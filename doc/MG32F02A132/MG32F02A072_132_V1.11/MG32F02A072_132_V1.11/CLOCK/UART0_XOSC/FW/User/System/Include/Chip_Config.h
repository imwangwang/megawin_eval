/**
 ******************************************************************************
 *
 * @file        Chip_Config.H
 *
 * @brief       Chip Configuration Definitions
 *				
 * @par         Project
 *              Mx92G8z
 * @version     V0.01
 * @date        2017/03/29
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 */

#ifndef _Chip_Config_H
#define _Chip_Config_H
#define _Chip_Config_H_VER        0.01        /*!< File Version */

//*** <<< Use Configuration Wizard in Context Menu >>> *** 

// <o0> Internal Flash Size <135168=>128K + 4K <73728=>64K + 8K <40960=>32K + 8K
// <o1> Internal RAM Size <16384=>16K <8192=>8K <4096=>4K
// <o2> Crystal Frequency. max 50000000Hz <0-50000000>
// <o3> External Clock Frequency. max 50000000Hz <0-50000000>
// <q4> Debug Function
#define IROM_Size       ((uint32_t)73728)       /*!< Internal Flash Size */
#define IRAM_Size       ((uint32_t)16384)       /*!< Internal SRAM Size */
#define CrystalFreq     ((uint32_t)12000000)    /*!< External Crystal Frequency(Hz)     */
#define ExtFreq         ((uint32_t)24000000)    /*!< External Input Clock Frequency(Hz) */

#define _Debug          0                       /*!< 0 = Disable, 1 = Enable, Code Debug Setting */

// <<< end of configuration section >>>
#endif

#ifdef __cplusplus
 extern "C" {
#endif

#if _Debug
#define Debug(format, args...) printf("[%s:%d] "format"\n\r", __FILE__, __LINE__, ##args)     /*!< Code Debug for "printf" */
#else
#define Debug(args...)

#ifdef __cplusplus
}
#endif

#endif

