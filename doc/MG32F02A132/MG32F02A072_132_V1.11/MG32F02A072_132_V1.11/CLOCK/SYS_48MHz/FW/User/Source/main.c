/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				该Demo为配置主时钟为PLLO:48MHz的例程。
 *				
 *				注意：时钟配置错误会导致MCU无法启动，届时需要使用
 *				ICP Programmer进行烧写固件，且需要在断电重新上电
 *				的时候点击烧写。
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
	
  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1
	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_PLLO);				//时钟源48MHz

	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);
  ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 // LED指示灯IO
	GPIO_PinMode_Config(PINE(14),&PINX_InitStruct); 					 // LED指示灯IO
  GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 // LED指示灯IO
	
	PE13=PE14=PE15 = 0;
	
	while(1);
}
