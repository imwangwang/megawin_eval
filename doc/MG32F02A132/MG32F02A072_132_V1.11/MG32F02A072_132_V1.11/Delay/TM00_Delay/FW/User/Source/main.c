/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				利用TM00做毫秒延时。
 *				此demo旨在利用设置定时器的周期和预分频参数，
 *				了解定时器设置的原理。
 *				
 *				注意：
 *   			若用户想延时1ms，也就是说让定时器每ms溢出，
 *				且CK_TM00_PR是12MHz。
 *				那么总时钟为12M*1ms = 12000
 *				因此用户可设置"clock prescaler"=100 和 "MainCounter"=120
 *
 *
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */

#include "MG32x02z_DRV.H"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

//  <h> Delay Period Setting 
//  <o0> Clock Prescaler (1~256)  <1-256>
//  <o1> Main Counter (1~256) <1-256>

//if user wants to delay 1ms and CK_TM00_PR is 12MHz.
//The Total clocks is 12M*1ms = 12000.
//User can set "clock prescaler"=100 and "MainCounter"=120 .   
#define Simple_Time_Prescaler       100
#define Simple_Time_MainCounter     120

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{   
	
    UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
    MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
    ProtectModuleReg(MEMprotect);

    UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1
	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
		
	/* Configure peripheral clock */
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);

    ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	void TM00_Delay_Init (void)
功能描述:	TM00初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void TM00_Delay_Init(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;

    UnProtectModuleReg(CSCprotect);
    CSC_PeriphProcessClockSource_Config(CSC_TM00_CKS, CK_APB);
    CSC_PeriphOnModeClock_Config(CSC_ON_TM00, ENABLE);					  // Enable TM00 module clock
    ProtectModuleReg(CSCprotect);
	
    // ----------------------------------------------------
    // 1.initial TimeBase structure 
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_Period = Simple_Time_MainCounter - 1; 
    TM_TimeBase_InitStruct.TM_Prescaler = Simple_Time_Prescaler - 1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
    
    TM_TimeBase_Init(TM00, &TM_TimeBase_InitStruct);
		
	TM_ClearFlag(TM00, TMx_TOF);
    
    // ----------------------------------------------------
    // 3.Start TM00 
    TM_Timer_Cmd(TM00, ENABLE);

    // ----------------------------------------------------
    // 4.until TOF flag event (polling)
    while(TM_GetSingleFlagStatus(TM00, TMx_TOF) == DRV_UnHappened);
    TM_ClearFlag(TM00, TMx_TOF);        // clear TOF flag

		
}

/***********************************************************************************
函数名称:	void delay_ms (int i)
功能描述:	毫秒延时处理程序
输入参数:	int i
返回参数:	  
*************************************************************************************/
void delay_ms(int i)
{
	
	for(;i>0;i--)
	{
		while(TM_GetSingleFlagStatus(TM00, TMx_TOF) == DRV_UnHappened);		//等待定时器溢出
        TM_ClearFlag(TM00, TMx_TOF);        // clear TOF flag
	}
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
  GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 // LED指示灯IO
	TM00_Delay_Init();
    while(1)
    {
		delay_ms(1000);
		PE15=~PE15;
    }
}

