

/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"
#include "Type.h"
#include "UserDefine.h"
#include "MG32x02z__Common_DRV.h"
#include "Stdio.h"			// for printf

#define _CK_IHRCO_		0
#define _CK_XOSC_		1
#define _CK_SEL_		_CK_IHRCO_

#define SYS_CLOCK	48.000				// sysclk =48MHz
#define PRINTF_URTX	URT3				// URT0 for printf

#define URT3_TX_PB14	0
#define URT3_TX_PD8		1				
#define URT3_TX_PE12	2

#define URT3_RX_PB13	0
#define URT3_RX_PD9		1				
#define URT3_RX_PE13	2

#define URT3_TX_USE		URT3_TX_PB14
#define URT3_RX_USE		URT3_RX_PB13


#define URT1_TX_PC8		0
#define URT1_TX_PC5		1				// 与仿真脚共享, 用于URT0_TXD时,将不能仿真
#define URT1_TX_PD4		2
#define URT1_TX_PE2		3

#define URT1_RX_PC9		0
#define URT1_RX_PC4		1				// 与仿真脚共享, 用于URT0_RXD时,将不能仿真
#define URT1_RX_PD5		2
#define URT1_RX_PE3		3

#define URT1_TX_USE		URT1_TX_PC8
#define URT1_RX_USE		URT1_RX_PC9


#define URT0_TX_PB8		0
#define URT0_TX_PC5		1				// 与仿真脚共享, 用于URT0_TXD时,将不能仿真
#define URT0_TX_PC10	2
#define URT0_TX_PE0		3

#define URT0_RX_PB9		0
#define URT0_RX_PC4		1				// 与仿真脚共享, 用于URT0_RXD时,将不能仿真
#define URT0_RX_PC11	2
#define URT0_RX_PE1		3

#define URT0_TX_USE		URT0_TX_PC10
#define URT0_RX_USE		URT0_RX_PC11

#define UART0_RX_BUF_SIZE		1048	//SOI+CID+LEN+ADDR+DATA+LRC+EOI按照DATA量1024字节算的话总数最大是1048字节，小于等于该量的通信长度均可
#define UART3_RX_BUF_SIZE		1048
#define UARTX_RX_BUF_SIZE		1048	//可为串口0或3



#define  SecetKeyLenth  8


u8 Uar0RxBuf[UART0_RX_BUF_SIZE];		//串口0接收缓冲
u8 Uart0RxIn;							//串口0接收位
u8 Uart0RxOut;							//串口0接收读取位

u8 Uar3RxBuf[UART3_RX_BUF_SIZE];		//串口3接收缓冲
u8 Uart3RxIn;							//串口3接收位
u8 Uart3RxOut;							//串口3接收读取位

u8 UarxRxBuf[UARTX_RX_BUF_SIZE];		//协议中使用的实际串口接收缓存
u8 UartxRxIn;							//协议中使用的实际串口接收位
u8 UartxRxOut;							//协议中使用的实际串口接收读取位


u16 ISPTime =500;  					//500ms ISP启动窗口
u8 ISPTimeCounting =1;				//ISP窗口倒计时使能：1：使能   			  0：禁用 
u8 WriteEnable =0;					//写使能	：1：使能  			0：禁用
u8 ResetEnable =0;					//复位使能：1：使能  				0：禁用
u8 CheckOK =0;						//校验帧校验状态：1：使能					  0：禁用
u16 BMSDCount =0;					//数据帧数量（单位：字节）
u8 ProcUart =0;						//协议中串口号定义
u8 UartConfirm =0;					//判断是否已确定协议使用的串口


u8 RxCID1;							//接收CID1帧内容
u8 TxCID1;							//发送CID1帧内容
u8 RxCID2;							//接收CID2帧内容
u8 TxCID2;							//发送CID2帧内容
u8 RxCID3;							//接收CID3帧内容
u8 TxCID3;							//发送CID3帧内容

WordTypeDef BMSRxLen;				//接收LEN帧内容
WordTypeDef BMSTxLen;				//发送LEN帧内容
WordTypeDef BMSRxADDR;				//接收ADDR帧内容
WordTypeDef BMSTxADDR;				//发送ADDR帧内容
u8 BMSRxData[1024]={0};				//接收DATA帧内容
u8 BMSTxData[1024]={0};				//发送DATA帧内容
WordTypeDef BMSLRCRxData;			//接收LRC帧内容
WordTypeDef BMSLRCTxData;			//发送LRC帧内容
u8 SecretKey[SecetKeyLenth]={ 0xAA,  0xA5,  0x5A,  0xA8,  0x8A,  0x58,  0x85,  0x78 };		//解密用密钥



u8	SysStatus;		//系统状态枚举
enum{
	SYS_IDLE,
	SYS_WAIT_3A_2,
	SYS_WAIT_3A_3,
	SYS_WAIT_3A_4,
	SYS_WAIT_CID1,
	SYS_WAIT_CID2,
	SYS_WAIT_CID3,
	SYS_WAIT_LEN_H,
	SYS_WAIT_LEN_L,
	SYS_WAIT_ADDR_H,
	SYS_WAIT_ADDR_L,
	SYS_WAIT_DATA,
	SYS_WAIT_LRC_H,
	SYS_WAIT_LRC_L,
	SYS_WAIT_0D_1,
	SYS_WAIT_0D_2,
	SYS_WAIT_0D_3,
	SYS_WAIT_0D_4,
	SYS_CHECKSUM,
	SYS_HANDLE,
	SYS_SENDCMD

};

enum {
	DFU_ENTER = 0x88,
	DFU_DOWNLOAD = 0x80
};

u8 DFUCmd;
DWordTypeDef DFUAddr;
DWordTypeDef DFUSize;
WordTypeDef DFUDataChksum;
WordTypeDef DFUCmdChksum;
WordTypeDef DFUPageCnt;
u8 DFUData[64];
u8 DFUInx;

u8 OverTime;

u8 LedTime;
#define SetOverTime(x)	OverTime=x

#define FLASH_AP_STARTADDRESS	0x18000000
#define FLASH_AP_PAGE_CNT		100

/*
*************************************************************************************
* Interrupt Handler
*
*************************************************************************************
*/

/***********************************************************************************
函数名称:	void SysTick_Handler(void)
功能描述:	系统Tick 中断处理入口
输入参数:	
返回参数:	  
*************************************************************************************/
void SysTick_Handler(void)
{
    //to do......
	IncTick();
	if(OverTime!=0) OverTime--;
	if(LedTime!=0) LedTime--;
	if(ISPTime!=0 && ISPTimeCounting ==1) ISPTime--;
}


 /***********************************************************************************
 函数名称:	 void URT0_IRQ(void)
 功能描述:	 URT0 中断处理
 输入参数:	 
 返回参数:	   
 *************************************************************************************/
 void URT0_IRQ(void)
 {
     uint32_t URT_Flag;
     uint32_t URT_IT_Msk;
     
     URT_Flag = URT_GetITAllFlagStatus(URT0);
     URT_IT_Msk = URT_GetITStatus(URT0);
     
     if((URT_Flag & URT_STA_UGF_mask_w) && (URT_IT_Msk & URT_INT_UG_IE_mask_w))
     {
        
         if((URT_Flag & URT_STA_SADRF_mask_w) && (URT_IT_Msk & URT_INT_SADR_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URT0,URT_IT_SADR);
         }
         if((URT_Flag & URT_STA_BRTF_mask_w) && (URT_IT_Msk & URT_INT_BRT_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URT0,URT_IT_BRT);
         }
         if((URT_Flag & URT_STA_TMOF_mask_w) && (URT_IT_Msk & URT_INT_TMO_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URT0,URT_IT_TMO);
         }
         URT_ClearITFlag(URT0,URT_IT_UG);
     }
     if((URT_Flag & URT_STA_RXF_mask_w) && (URT_IT_Msk & URT_INT_RX_IE_mask_w))
     {
         
         //---when RX Data Register have data---//
         
         // To do ......
         if(UartConfirm ==0)
		{
			Uar0RxBuf[Uart0RxIn] = URT_GetRXData(URT0);
         	UarxRxBuf[UartxRxIn] = Uar0RxBuf[Uart0RxIn];
         	if(Uar0RxBuf[Uart0RxIn] == ':')
         	{
				URT_ITEA_Cmd(URT3, DISABLE);
				//URT_Cmd(URT3,DISABLE);
				ProcUart = 0;
				UartConfirm =1;
	         }
	         Uart0RxIn++;
	         UartxRxIn++;
	         if(Uart0RxIn>= UART0_RX_BUF_SIZE)
	         {
				Uart0RxIn=0;	
	         }
	         if(UartxRxIn>= UARTX_RX_BUF_SIZE)
	         {
				UartxRxIn=0;	
	         }
		}
		else if(UartConfirm ==1)
		{
			UarxRxBuf[UartxRxIn] = URT_GetRXData(URT0);
			UartxRxIn++;
			if(UartxRxIn>= UARTX_RX_BUF_SIZE)
			{
			   UartxRxIn=0;    
			}
		}
         
         URT_ClearITFlag(URT0,URT_IT_RX);
     }
     if((URT_Flag & URT_STA_TXF_mask_w) && (URT_IT_Msk & URT_INT_TX_IE_mask_w))
     {
         // To do ......
         
         URT_ClearITFlag(URT0,URT_IT_TX);
     }
     if((URT_Flag & URT_STA_LSF_mask_w) && (URT_IT_Msk & URT_INT_LS_IE_mask_w))
     {
         if((URT_Flag & URT_STA_IDLF_mask_w) && (URT_IT_Msk & URT_INT_IDL_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT0,URT_IT_IDL);
         }
         if((URT_Flag & URT_STA_CTSF_mask_w) && (URT_IT_Msk & URT_INT_CTS_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT0,URT_IT_CTS);
         }
         URT_ClearITFlag(URT0,URT_IT_LS);
     }
     if((URT_Flag & URT_STA_ERRF_mask_w) && (URT_IT_Msk & URT_INT_ERR_IE_mask_w))
     {
         
         if((URT_Flag & URT_STA_PEF_mask_w) && (URT_IT_Msk & URT_INT_PE_IE_mask_w))
         {
             //To do......
            
             URT_ClearITFlag(URT0,URT_IT_PE);
         }
         if((URT_Flag & URT_STA_ROVRF_mask_w) && (URT_IT_Msk & URT_INT_ROVR_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT0,URT_IT_ROVR);
         }
         if((URT_Flag & URT_STA_RXTMOF_mask_w) && (URT_IT_Msk & URT_INT_RXTMO_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT0,URT_IT_RXTMO);
         }
         if((URT_Flag & URT_STA_IDTMOF_mask_w) && (URT_IT_Msk & URT_INT_IDTMO_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT0,URT_IT_IDTMO);
         }

     }
     if((URT_Flag & URT_STA_TCF_mask_w) && (URT_IT_Msk & URT_INT_TC_IE_mask_w))
     {
         //To do......
         
         URT_ClearITFlag(URT0, URT_IT_TC);
     }
}

/***********************************************************************************
函数名称:	void URT3_IRQ(void)
功能描述:	URT3 中断处理
输入参数:	
返回参数:	  
*************************************************************************************/
void URT3_IRQ(void)
 {
     uint32_t URT_Flag;
     uint32_t URT_IT_Msk;
     
     
     URT_Flag = URT_GetITAllFlagStatus(URT3);
     URT_IT_Msk = URT_GetITStatus(URT3);
     
     if((URT_Flag & URT_STA_UGF_mask_w) && (URT_IT_Msk & URT_INT_UG_IE_mask_w))
     {
        
         if((URT_Flag & URT_STA_SADRF_mask_w) && (URT_IT_Msk & URT_INT_SADR_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URT3,URT_IT_SADR);
         }
         if((URT_Flag & URT_STA_BRTF_mask_w) && (URT_IT_Msk & URT_INT_BRT_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URT3,URT_IT_BRT);
         }
         if((URT_Flag & URT_STA_TMOF_mask_w) && (URT_IT_Msk & URT_INT_TMO_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URT3,URT_IT_TMO);
         }
         URT_ClearITFlag(URT3,URT_IT_UG);
     }
     if((URT_Flag & URT_STA_RXF_mask_w) && (URT_IT_Msk & URT_INT_RX_IE_mask_w))
     {
		  if(UartConfirm ==0)
		 {
			 Uar3RxBuf[Uart3RxIn] = URT_GetRXData(URT3);
			 UarxRxBuf[UartxRxIn] = Uar3RxBuf[Uart3RxIn];
			 if(Uar3RxBuf[Uart3RxIn] == ':')
			 {
				 URT_ITEA_Cmd(URT0, DISABLE);
				 //URT_Cmd(URT0,DISABLE);
				 ProcUart = 3;
				 UartConfirm =1;
			  }
			  Uart3RxIn++;
			  UartxRxIn++;
			  if(Uart3RxIn>= UART3_RX_BUF_SIZE)
			  {
				 Uart3RxIn=0;	 
			  }
			  if(UartxRxIn>= UARTX_RX_BUF_SIZE)
			  {
				 UartxRxIn=0;	 
			  }
		 }
		 else if(UartConfirm ==1)
		 {
			 UarxRxBuf[UartxRxIn] = URT_GetRXData(URT3);
			 UartxRxIn++;
			 if(UartxRxIn>= UARTX_RX_BUF_SIZE)
			 {
				UartxRxIn=0;	
			 }
		 }
         
         URT_ClearITFlag(URT3,URT_IT_RX);

     }
     if((URT_Flag & URT_STA_TXF_mask_w) && (URT_IT_Msk & URT_INT_TX_IE_mask_w))
     {
         // To do ......
         
         URT_ClearITFlag(URT3,URT_IT_TX);
     }
     if((URT_Flag & URT_STA_LSF_mask_w) && (URT_IT_Msk & URT_INT_LS_IE_mask_w))
     {
         if((URT_Flag & URT_STA_IDLF_mask_w) && (URT_IT_Msk & URT_INT_IDL_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT3,URT_IT_IDL);
         }
         if((URT_Flag & URT_STA_CTSF_mask_w) && (URT_IT_Msk & URT_INT_CTS_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT3,URT_IT_CTS);
         }
         URT_ClearITFlag(URT3,URT_IT_LS);
     }
     if((URT_Flag & URT_STA_ERRF_mask_w) && (URT_IT_Msk & URT_INT_ERR_IE_mask_w))
     {
         
         if((URT_Flag & URT_STA_PEF_mask_w) && (URT_IT_Msk & URT_INT_PE_IE_mask_w))
         {
             //To do......
            
             URT_ClearITFlag(URT3,URT_IT_PE);
         }
         if((URT_Flag & URT_STA_ROVRF_mask_w) && (URT_IT_Msk & URT_INT_ROVR_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT3,URT_IT_ROVR);
         }
         if((URT_Flag & URT_STA_RXTMOF_mask_w) && (URT_IT_Msk & URT_INT_RXTMO_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT3,URT_IT_RXTMO);
         }
         if((URT_Flag & URT_STA_IDTMOF_mask_w) && (URT_IT_Msk & URT_INT_IDTMO_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URT3,URT_IT_IDTMO);
         }

     }
     if((URT_Flag & URT_STA_TCF_mask_w) && (URT_IT_Msk & URT_INT_TC_IE_mask_w))
     {
         //To do......
         
         URT_ClearITFlag(URT3, URT_IT_TC);
     }
}



/***********************************************************************************
函数名称:	void URT0_IRQHandler(void)
功能描述:	URT0 中断处理入口
输入参数:	
返回参数:	  
*************************************************************************************/
void URT0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(URT0_IRQn);      
    if(IRQ_ID & EXIC_SRC5_ID20_urt0_b0)
    {
        URT0_IRQ();
    }
}

/***********************************************************************************
函数名称:	void URT123_IRQHandler(void)
功能描述:	URT0 中断处理入口
输入参数:	
返回参数:	  
*************************************************************************************/
void URT123_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(URT123_IRQn);      
    if(IRQ_ID & EXIC_SRC5_ID21_urt3_b1)
    {
        URT3_IRQ();
    }
}


/*
*************************************************************************************
*/ 



/*
*************************************************************************************
*  Init device
*
*************************************************************************************
*/

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
    
	
    UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
    MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
    ProtectModuleReg(MEMprotect);

    UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

#if (_CK_SEL_==_CK_IHRCO_)	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO


	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_PLLO);	
#else
	
	/* CK_HS selection */
	CSC_XOSCGain_Select(Gain_Medium);		
	CSC_PeriphOnModeClock_Config(CSC_ON_PortC, ENABLE);
	CSC_XOSC_Cmd(ENABLE);					// Enable XOSC
	while(CSC_GetSingleFlagStatus(CSC_XOSCF) == DRV_Normal);
	CSC_ClearFlag(CSC_XOSCF);
	
	CSC_MissingClockDetectionDuration_Select(MCD_Duration_125us);
	CSC_MissingClockDetection_Cmd(ENABLE);

	CSC_CK_HS_Select(HS_CK_XOSC);
	
	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=65M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_PLLO);	
#endif

    ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	void TICK_Init (void)
功能描述:	系统滴答时钟初始化 
输入参数:	
返回参数:	  
*************************************************************************************/
void TICK_Init (void)
{
	InitTick(SYS_CLOCK*1000000,0);
	NVIC_EnableIRQ(SysTick_IRQn);
}

/***********************************************************************************
函数名称:	void GPIO_Init(void)
功能描述:	通用IO口初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void GPIO_Init(void)
{
	PIN_InitTypeDef PINX_InitStruct;
    
	//==Set GPIO Clock
    UnProtectModuleReg(CSCprotect);
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);						// Enable PortE Clock
    ProtectModuleReg(CSCprotect);
    
	//==Set GPIO init
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 		// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse

	PINX_InitStruct.PINX_Alternate_Function  = PE13_AF_GPE13;				// Pin AFS = GPIO
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 		// 

	PINX_InitStruct.PINX_Alternate_Function  = PE14_AF_GPE14;				// Pin AFS = GPIO
	GPIO_PinMode_Config(PINE(14),&PINX_InitStruct); 					 		// 

	PINX_InitStruct.PINX_Alternate_Function  = PE15_AF_GPE15;				// Pin AFS = GPIO
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 	// 
  
}

/***********************************************************************************
函数名称:	void URT0_Init(void)
功能描述:	UART0初始化 
		  TXD(PC10),RXD(PBC11)
		  8,n,1 19200bps@48MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void URT0_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    
	PIN_InitTypeDef PINX_InitStruct;

	//==Set URT0 Clock
    UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
 	CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);						// Enable UART0 Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);						// Enable PortB Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortC,ENABLE);						// Enable PortC Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);						// Enable PortE Clock
    ProtectModuleReg(CSCprotect);
    
	//==Set GPIO init
	//PB8 PPO TX ,PB9 ODO RX
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_High;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	
#if (URT0_TX_USE == URT0_TX_PB8)	
	PINX_InitStruct.PINX_Alternate_Function  = PB8_AF_URT0_TX;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD at PB8
#endif
#if (URT0_TX_USE == URT0_TX_PC5)
	PINX_InitStruct.PINX_Alternate_Function  = PC5_AF_URT0_TX;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINC(5),&PINX_InitStruct); 					 		// TXD at PC5
#endif
#if (URT0_TX_USE == URT0_TX_PC10)
	PINX_InitStruct.PINX_Alternate_Function  = PC10_AF_URT0_TX;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINC(10),&PINX_InitStruct); 					 	// TXD at PC10
#endif
#if (URT0_TX_USE == URT0_TX_PE0)
	PINX_InitStruct.PINX_Alternate_Function  = PE0_AF_URT0_TX;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINE(0),&PINX_InitStruct); 					 		// TXD at PE0
#endif
	
#if (URT0_RX_USE == URT0_RX_PB9)	
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PB9_AF_URT0_RX;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD at PB9
#endif
#if (URT0_RX_USE == URT0_RX_PC4)
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PC4_AF_URT0_RX;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINC(4),&PINX_InitStruct); 					 		// RXD at PC4
#endif
#if (URT0_RX_USE == URT0_RX_PC11)
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PC11_AF_URT0_RX;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINC(11),&PINX_InitStruct); 					 	// RXD at PC11
#endif
#if (URT0_RX_USE == URT0_RX_PE1)
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PE1_AF_URT0_RX;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINE(1),&PINX_InitStruct); 					 		// RXD at PE1
#endif

    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                	//Set PSR
    URT_BRG.URT_BaudRateCounterReload = 99;	                	//Set RLR
    URT_BaudRateGenerator_Config(URT0, &URT_BRG);		    	//BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URT0, ENABLE);	            	//Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URT0, URT_TXClock_Internal);		//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URT0, URT_RXClock_Internal);		//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URT0, 24);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URT0, 24);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URT0, URT_RXSMP_3TIME);
    URT_TX_Cmd(URT0, ENABLE);	                            	//Enable TX
    URT_RX_Cmd(URT0, ENABLE);	                            	//Enable RX
    
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URT0, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URT0, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URT0, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URT0, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URT0, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URT0, 0);
    
    //=====Enable URT Interrupt=====//
    URT_IT_Config(URT0, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URT0, ENABLE);
    NVIC_EnableIRQ(URT0_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URT0, ENABLE);
		
}

/***********************************************************************************
函数名称:	void URT1_Init(void)
功能描述:	UART1初始化 
		  8,n,1 115200bps@48MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void URT1_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    
	PIN_InitTypeDef PINX_InitStruct;

	//==Set URT0 Clock
    UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_UART1_CKS, CK_APB);
 	CSC_PeriphOnModeClock_Config(CSC_ON_UART1,ENABLE);						// Enable UART0 Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortC,ENABLE);						// Enable PortC Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortD,ENABLE);						// Enable PortD Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);						// Enable PortE Clock
    ProtectModuleReg(CSCprotect);
    
	//==Set GPIO init
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_High;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	
#if (URT1_TX_USE == URT1_TX_PC8)	
	PINX_InitStruct.PINX_Alternate_Function  = PC8_AF_URT1_TX;				// Pin AFS = URT1_TX
	GPIO_PinMode_Config(PINC(8),&PINX_InitStruct); 					 		// TXD at PC8
#endif
#if (URT1_TX_USE == URT1_TX_PC5)
	PINX_InitStruct.PINX_Alternate_Function  = PC5_AF_URT1_TX;				// Pin AFS = URT1_TX
	GPIO_PinMode_Config(PINC(5),&PINX_InitStruct); 					 		// TXD at PC5
#endif
#if (URT1_TX_USE == URT1_TX_PD4)
	PINX_InitStruct.PINX_Alternate_Function  = PD4_AF_URT1_TX;				// Pin AFS = URT1_TX
	GPIO_PinMode_Config(PINC(10),&PINX_InitStruct); 					 	// TXD at PD4
#endif
#if (URT1_TX_USE == URT1_TX_PE2)
	PINX_InitStruct.PINX_Alternate_Function  = PE2_AF_URT1_TX;				// Pin AFS = URT1_TX
	GPIO_PinMode_Config(PINE(2),&PINX_InitStruct); 					 		// TXD at PE2
#endif
	
#if (URT1_RX_USE == URT1_RX_PC9)	
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PC9_AF_URT1_RX;				// Pin AFS = URT1_RX
	GPIO_PinMode_Config(PINC(9),&PINX_InitStruct); 					 		// RXD at PC9
#endif
#if (URT1_RX_USE == URT1_RX_PC4)
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PC4_AF_URT1_RX;				// Pin AFS = URT1_RX
	GPIO_PinMode_Config(PINC(4),&PINX_InitStruct); 					 		// RXD at PC4
#endif
#if (URT1_RX_USE == URT1_RX_PD5)
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PD5_AF_URT1_RX;				// Pin AFS = URT1_RX
	GPIO_PinMode_Config(PIND(5),&PINX_InitStruct); 					 		// RXD at PD5
#endif
#if (URT1_RX_USE == URT1_RX_PE3)
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PE3_AF_URT1_RX;				// Pin AFS = URT1_RX
	GPIO_PinMode_Config(PINE(3),&PINX_InitStruct); 					 		// RXD at PE3
#endif

    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                	//Set PSR
    URT_BRG.URT_BaudRateCounterReload = 99;	                	//Set RLR
    URT_BaudRateGenerator_Config(URT1, &URT_BRG);		    	//BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URT1, ENABLE);	            	//Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URT1, URT_TXClock_Internal);		//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URT1, URT_RXClock_Internal);		//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URT1, 24);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URT1, 24);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URT1, URT_RXSMP_3TIME);
    URT_TX_Cmd(URT1, ENABLE);	                            	//Enable TX
    URT_RX_Cmd(URT1, ENABLE);	                            	//Enable RX
    
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URT1, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URT1, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URT1, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URT1, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URT1, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URT1, 0);
    
    //=====Enable URT Interrupt=====//
    //URT_IT_Config(URT1, URT_IT_RX, ENABLE);
    //URT_ITEA_Cmd(URT1, ENABLE);
    //NVIC_EnableIRQ(URT123_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URT1, ENABLE);
		
}

/***********************************************************************************
函数名称:	void URT3_Init(void)
功能描述:	UART3初始化 
		  TXD(PC10),RXD(PBC11)
		  8,n,1 19200bps@48MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void URT3_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    
	PIN_InitTypeDef PINX_InitStruct;

	//==Set URT3 Clock
    UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_UART3_CKS, CK_APB);
 	CSC_PeriphOnModeClock_Config(CSC_ON_UART3,ENABLE);						// Enable UART0 Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);						// Enable PortB Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortD,ENABLE);						// Enable PortC Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);						// Enable PortE Clock
    ProtectModuleReg(CSCprotect);
    
	//==Set GPIO init
	//PB8 PPO TX ,PB9 ODO RX
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_High;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	
#if (URT3_TX_USE == URT3_TX_PB14)	
	PINX_InitStruct.PINX_Alternate_Function  = PB14_AF_URT3_TX;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINB(14),&PINX_InitStruct); 					 		// TXD at PB8
#endif
#if (URT3_TX_USE == URT3_TX_PD8)
	PINX_InitStruct.PINX_Alternate_Function  = PD8_AF_URT3_TX;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PIND(8),&PINX_InitStruct); 					 		// TXD at PC5
#endif
#if (URT3_TX_USE == URT3_TX_PE12)
	PINX_InitStruct.PINX_Alternate_Function  = PE12_AF_URT3_TX;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINE(12),&PINX_InitStruct); 					 	// TXD at PC10
#endif
	
#if (URT3_RX_USE == URT3_RX_PB13)	
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PB13_AF_URT3_RX;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINB(13),&PINX_InitStruct); 					 		// RXD at PB9
#endif
#if (URT3_RX_USE == URT3_RX_PD9)
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PD9_AF_URT3_RX;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PIND(9),&PINX_InitStruct); 					 		// RXD at PC4
#endif
#if (URT3_RX_USE == URT3_RX_PE13)
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PE13_AF_URT3_RX;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 	// RXD at PC11
#endif

    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                	//Set PSR
    URT_BRG.URT_BaudRateCounterReload = 99;	                	//Set RLR
    URT_BaudRateGenerator_Config(URT3, &URT_BRG);		    	//BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URT3, ENABLE);	            	//Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URT3, URT_TXClock_Internal);		//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URT3, URT_RXClock_Internal);		//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URT3, 24);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URT3, 24);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URT3, URT_RXSMP_3TIME);
    URT_TX_Cmd(URT3, ENABLE);	                            	//Enable TX
    URT_RX_Cmd(URT3, ENABLE);	                            	//Enable RX
    
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URT3, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URT3, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URT3, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URT3, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URT3, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URT3, 0);
    
    //=====Enable URT Interrupt=====//
    URT_IT_Config(URT3, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URT3, ENABLE);
    NVIC_EnableIRQ(URT123_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URT3, ENABLE);
		
}


/*
*************************************************************************************
*/ 



/*
*************************************************************************************
*  Function
*
*************************************************************************************
*/
/***********************************************************************************
函数名称:	int fputc(int ch,FILE *f)
功能描述: 串口输出, 用于printf函数
输入参数:	
返回参数:	  
*************************************************************************************/
int fputc(int ch,FILE *f)
{
	
	URT_SetTXData(PRINTF_URTX,1,ch);
	while(URT_GetITSingleFlagStatus(PRINTF_URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(PRINTF_URTX,URT_IT_TC);
	
	return ch;
}

/***********************************************************************************
函数名称:	void SendByte(u32 ch)
功能描述: 串口输出
输入参数:	
返回参数:	  
*************************************************************************************/
void SendByte(u32 ch)
{
	if(ProcUart ==0)
	{
		URT_SetTXData(URT0,1,ch);
		while(URT_GetITSingleFlagStatus(URT0,URT_IT_TC)==DRV_UnHappened);
		URT_ClearITFlag(URT0,URT_IT_TC);
	}
	else if(ProcUart ==3)
	{
		URT_SetTXData(URT3,1,ch);
		while(URT_GetITSingleFlagStatus(URT3,URT_IT_TC)==DRV_UnHappened);
		URT_ClearITFlag(URT3,URT_IT_TC);
	}

}

/***********************************************************************************
函数名称:	DRV_Return MEM_FlashAPPageErase(uint32_t StartPageAddress, uint32_t PageQuantity)
功能描述: 擦除AP区
输入参数:	
返回参数:	  
*************************************************************************************/
DRV_Return MEM_FlashAPPageErase(uint32_t StartPageAddress, uint32_t PageQuantity)
{
    DRV_Return lDRV_Return = DRV_Success;
    uint32_t lCount;
    uint32_t *lptrDest_addr;

    // Check IHRCO Enable
    if(CSC->CR0.MBIT.IHRCO_EN == 0)
        return DRV_Failure;

    // Check flash page erase address alignment
    if(((uint32_t)StartPageAddress & 0x03FF) != 0)
        return DRV_Failure;

    if(((StartPageAddress) < 0x18000000) || ((StartPageAddress) >= 0x1A000000))
        return DRV_Failure;

	if(PageQuantity >=128)
		return DRV_Failure;

    lptrDest_addr = (uint32_t *)StartPageAddress;
    lCount = 0;
		
    __MEM_UnProtect();
    __MEM_Enable();
    __MEM_Access_Enable(MEM_ACCESS_AP_WRITE);
    __MEM_SetWriteMode(APErase);
    __MEM_MultipleWriteUnProtect();

    do{
        // Mem Flag Clear.
        MEM->STA.B[0] = (MEM_STA_WPEF_mask_b0 | MEM_STA_EOPF_mask_b0);
        __ISB();
        *lptrDest_addr = 0xFFFFFFFF;
				
        if((MEM->STA.B[0] & (MEM_STA_WPEF_mask_b0 | MEM_STA_EOPF_mask_b0)) != MEM_STA_EOPF_mask_b0)
        {
            lDRV_Return = DRV_Failure;
            break;
        }
				
				lptrDest_addr +=0x100;//1 page=1024bytes,1 address=32bitdata=4bytes,so,1 page address offset=1024/4=0x100

    }while(++lCount < PageQuantity);
		
		

    __MEM_UnProtect();
    __MEM_MultipleWriteProtect();
    __MEM_SetWriteMode(None);
    __MEM_Access_Disable(MEM_ACCESS_AP_WRITE);
    __MEM_Disable();
    __MEM_Protect();

    return lDRV_Return;
}

/***********************************************************************************
函数名称:	void Flash_Read_para(uint32_t addr_start, uint16_t len, uint32_t *pdat)
功能描述: 读取flash数据        		addr_start：读取数据的起始地址				len：数据长度 			*pdat：指向读取数据的指针
输入参数:	uint32_t addr_start, uint16_t len, uint32_t *pdat
返回参数:	  
*************************************************************************************/
void Flash_Read_para(uint32_t addr_start, uint16_t len, uint32_t *pdat)
{
	u8 i=0;
	for(i=0;i<len;i++)
	{
	*pdat = *(__IO u8*)(addr_start);
	addr_start++;
	pdat++;
	}
		
}



/***********************************************************************************
函数名称:	DRV_Return MEM_FlashAPProgram(uint32_t StartAddress, uint32_t DataStartAddress, uint32_t Length)
功能描述: 写AP区
输入参数:	
返回参数:	  
*************************************************************************************/
DRV_Return MEM_FlashAPProgram(uint32_t StartAddress, uint32_t DataStartAddress, uint32_t Length)
{

    DRV_Return lDRV_Return = DRV_Success;
    uint32_t lCount;
    uint32_t *lptrSrc_addr;
    uint32_t *lptrDest_addr;
    

    // Check IHRCO Enable
    if(CSC->CR0.MBIT.IHRCO_EN == 0)
        return DRV_Failure;

    // Check write flash address alignment
		    if(((StartAddress & 0x03) != 0) && ((DataStartAddress & 0x03) != 0))
        return DRV_Failure;

    if(((StartAddress) < 0x18000000) || ((StartAddress) >= 0x1A000000))
        return DRV_Failure;

    lptrSrc_addr = (uint32_t *)DataStartAddress;
    lptrDest_addr = (uint32_t *)StartAddress;
    lCount = 0;

    __MEM_UnProtect();
    __MEM_Enable();
    __MEM_Access_Enable(MEM_ACCESS_AP_WRITE);
    __MEM_SetWriteMode(APProgram);
    __MEM_MultipleWriteUnProtect();

    do{
        // Mem Flag Clear.
        MEM->STA.B[0] = (MEM_STA_WPEF_mask_b0 | MEM_STA_EOPF_mask_b0);
        __ISB();
        *lptrDest_addr = *lptrSrc_addr;

        if((MEM->STA.B[0] & (MEM_STA_WPEF_mask_b0 | MEM_STA_EOPF_mask_b0)) != MEM_STA_EOPF_mask_b0)
        {
            lDRV_Return = DRV_Failure;
            break;
        }
        lptrSrc_addr ++;
        lptrDest_addr ++;
    }while(++lCount < Length);

    __MEM_UnProtect();
    __MEM_MultipleWriteProtect();
    __MEM_SetWriteMode(None);
    __MEM_Access_Disable(MEM_ACCESS_AP_WRITE);
    __MEM_Disable();
    __MEM_Protect();

    return lDRV_Return;
}


/*
*************************************************************************************
*/ 



/***********************************************************************************
函数名称:	void UncodeService(uint8_t *file,uint16_t len)
功能描述: 对上位机发出的DATA进行解密
输入参数:	uint8_t *file,uint16_t len
返回参数:	  
*************************************************************************************/
void UncodeService(uint8_t *file,uint16_t len)
{
	uint16_t index;
	uint8_t  temp1,temp2;
	for(index=0;index<len;index++)
	{
		temp2=file[index];
		temp2=temp2^SecretKey[index%SecetKeyLenth];
		
		temp1=temp2;
		temp1>>=4; temp2<<=4;
		temp2 |=temp1;
		
		file[index]=temp2;
	}
}

/***********************************************************************************
函数名称:	void SendCMD(u8 CID1,u8 CID2,u8 CID3,u8 TxData1)
功能描述: 向上位机专门发送			出错指令
输入参数:	u8 CID1,u8 CID2,u8 CID3,u8 TxData1
返回参数:	  
*************************************************************************************/
void SendCMD(u8 CID1,u8 CID2,u8 CID3,u8 TxData1)
{
	WordTypeDef CheckSum;

	CheckSum.W = CID1+CID2+CID3+0x0002+TxData1;
	CheckSum.W = ~(CheckSum.W);
	CheckSum.W = CheckSum.W+0x0001;
	
	SendByte(':');					//发送起点帧
	SendByte(':');
	SendByte(':');	
	SendByte(':');

	SendByte(CID1);					//发送CID帧
	SendByte(CID2);
	SendByte(CID3);

	SendByte(0x00);					//发送LEN帧
	SendByte(0x02);

	SendByte(0x00);					//发送ADDR帧:00 00
	SendByte(0x00);

	SendByte(0x00);					//发送Data帧
	SendByte(TxData1);
				
	SendByte(CheckSum.B.BHigh);		//发送LRC帧
	SendByte(CheckSum.B.BLow);

	SendByte(0x0D);					//发送结束帧
	SendByte(0x0D);
	SendByte(0x0D);
	SendByte(0x0D);
	
}

/***********************************************************************************
函数名称:	void UpgradeProc(void)
功能描述: 更新协议
输入参数:	
返回参数:	  
*************************************************************************************/
void UpgradeProc(void)
{
	u8 RxData;
	u8 ProgramCheck =0;
	WordTypeDef RxCheckSum;

	if(UartxRxIn!=UartxRxOut && UartConfirm ==1)				//若有一个字节信息过来则进行接收
	{
		RxData=UarxRxBuf[UartxRxOut];
		UartxRxOut++;
		if(UartxRxOut>=UARTX_RX_BUF_SIZE)
		{
			UartxRxOut=0;
		}
		switch(SysStatus)
		{
			case SYS_IDLE:
				if(RxData == ':')			//遇到协议起点则开始进行协议接收
				{
					BMSLRCRxData.W = 0x0000;
					RxCheckSum.W = 0x0000;
					SysStatus ++;
					ISPTimeCounting =0;		//禁用超时复位
					//printf(":1 received!!\n");
				}
				else 
				{
					SysStatus = SYS_IDLE;
				}
			break;
					
			case SYS_WAIT_3A_2:
				if(RxData == ':')
				{
					SysStatus ++;
					//printf(":2 received!!\n");
				}
				else 
				{
					SysStatus = SYS_IDLE;
				}
			break;

			case SYS_WAIT_3A_3:
				if(RxData == ':')
				{
					SysStatus ++;
					//printf(":3 received!!\n");
				}
				else 
				{
					SysStatus = SYS_IDLE;

				}
			break;
			
			case SYS_WAIT_3A_4:
				if(RxData == ':')
				{
					SysStatus ++;
					//printf(":4 received!!\n");
				}
				else 
				{
					SysStatus = SYS_IDLE;
				}
			break;	

			case SYS_WAIT_CID1:
				if(RxData == 'W')
				{
					RxCID1 = RxData;
					SysStatus ++;
					RxCheckSum.W += RxCID1;		
					//printf("CID1 received!!\n");
				}
				else if(RxData == 'R')
				{
					RxCID1 = RxData;
					SysStatus ++;
					RxCheckSum.W += RxCID1;
					//printf("CID1 received!!\n");
				}
				else
				{
					SysStatus = SYS_IDLE;
				}
				
			break;	
				
			case SYS_WAIT_CID2:
				if(RxData == 'E')
				{
					WriteEnable = 1;
					RxCID2 = RxData;
					SysStatus ++;
					RxCheckSum.W += RxCID2;
					//printf("CID2 received!!\n");
				}
				else if(RxData == 'F')
				{
					RxCID2 = RxData;
					SysStatus ++;
					RxCheckSum.W += RxCID2;
					//printf("CID2 received!!\n");
				}
				else
				{
					SysStatus = SYS_IDLE;
					SendCMD(RxCID1, RxCID2, 'N', 0x01);  //帧和校验错误
				}
				
			break;	
				
			case SYS_WAIT_CID3:
				if(RxData == 'D')
				{
					RxCID3 = RxData;
					SysStatus ++;
					RxCheckSum.W += RxCID3;   			//校验数据增加
					//printf("CID3 received!!\n");
				}
				else if(RxData == 'L')
				{
					RxCID3 = RxData;
					SysStatus = SYS_WAIT_ADDR_H; 		//由于该CID状态下少了两个字节信息帧，为了不影响流程，伪造两个字节长度数据,并跳过LEN信息帧以继续接收协议
					BMSRxLen.W = 0x0002;
					RxCheckSum.W += RxCID3;   			//校验数据增加
					//printf("CID3 received!!\n");
				}
				else if(RxData == 'G')
				{
					RxCID3 = RxData;
					SysStatus = SYS_WAIT_ADDR_H; 		//由于该CID状态下少了两个字节信息帧，为了不影响流程，伪造两个字节长度数据,并跳过LEN信息帧以继续接收协议
					BMSRxLen.W = 0x0002;
					RxCheckSum.W += RxCID3;   			//校验数据增加
					//printf("CID3 received!!\n");
				}
				else if(RxData == 'B')
				{
					RxCID3 = RxData;
					SysStatus ++;
					RxCheckSum.W += RxCID3;   			//校验数据增加
					//printf("CID3 received!!\n");
				}
				else if(RxData == 'N')
				{
					RxCID3 = RxData;
					SysStatus ++;
					RxCheckSum.W += RxCID3;   			//校验数据增加
					//printf("CID3 received!!\n");
				}
				else if(RxData == 'E')
				{
					RxCID3 = RxData;
					SysStatus = SYS_WAIT_ADDR_H; 		//由于该CID状态下少了两个字节信息帧，为了不影响流程，伪造两个字节长度数据,并跳过LEN信息帧以继续接收协议
					BMSRxLen.W = 0x0002;
					RxCheckSum.W += RxCID3;   			//校验数据增加
					//printf("CID3 received!!\n");
				}
				else
				{
					SysStatus = SYS_IDLE;
				}
				
			break;	

			case SYS_WAIT_LEN_H:
				BMSRxLen.B.BHigh = RxData;		
				SysStatus++;
				RxCheckSum.W += BMSRxLen.B.BHigh;   			//校验数据增加
				//printf("LENH received!!\n");
			break;
				
			case SYS_WAIT_LEN_L:
				BMSRxLen.B.BLow = RxData;
				SysStatus++;
				BMSDCount = BMSRxLen.W;
				RxCheckSum.W += BMSRxLen.B.BLow;   			//校验数据增加				
				//printf("LENL received!!\n");
			break;

			case SYS_WAIT_ADDR_H:
				BMSRxADDR.B.BHigh = RxData;
				SysStatus++;
				RxCheckSum.W += BMSRxADDR.B.BHigh;   			//校验数据增加
				//printf("ADDRH received!!\n");
			break;
				
			case SYS_WAIT_ADDR_L:
				BMSRxADDR.B.BLow = RxData;
				SysStatus++;
				BMSDCount =0;
				RxCheckSum.W += BMSRxADDR.B.BLow;   			//校验数据增加
				//printf("ADDRL received!!\n");
			break;

			case SYS_WAIT_DATA:
			
				BMSRxData[BMSDCount] = RxData;					//将数据存入缓冲，用于更新AP
				RxCheckSum.W += BMSRxData[BMSDCount];   			//校验数据增加
				BMSDCount++;
				if(BMSDCount >= BMSRxLen.W)						//接收到完整数据后进入下一状态
				{
					SysStatus++;
					BMSDCount = 0;
				}
				
				//printf("DATA received!!\n");
			break;

			case SYS_WAIT_LRC_H:
				BMSLRCRxData.B.BHigh = RxData;	
				SysStatus++;
				//printf("LRCH received!!\n");
			break;	

			case SYS_WAIT_LRC_L:
				BMSLRCRxData.B.BLow = RxData;
				SysStatus++;
				//printf("LRCL received!!\n");
			break;
				
			case SYS_WAIT_0D_1:
				if(RxData == 0x0D)
				{
					SysStatus ++;
					//printf("0D1 received!!\n");
					
				}
				else 
				{
					SysStatus = SYS_IDLE;
				}
			break;	

			case SYS_WAIT_0D_2:
				if(RxData == 0x0D)
				{
					SysStatus ++;
					//printf("0D2 received!!\n");
					
				}
				else 
				{
					SysStatus = SYS_IDLE;
				}
			break;	

			case SYS_WAIT_0D_3:
				if(RxData == 0x0D)
				{
					SysStatus ++;
					//printf("0D3 received!!\n");
					
				}
				else 
				{
					SysStatus = SYS_IDLE;
				}
			break;	

			case SYS_WAIT_0D_4:
				if(RxData == 0x0D)
				{
					SysStatus ++;
					//printf("0D4 received!!\n");
					
				}
				else 
				{
					SysStatus = SYS_IDLE;
				}
			

			case SYS_CHECKSUM:
				RxCheckSum.W = ~(RxCheckSum.W) +1;		//运算接收帧校验（即取反后+1）
				if(RxCheckSum.W == BMSLRCRxData.W)		//比对16位数据均相同，则进入处理
				{
					SysStatus++;
					CheckOK =1;							//使能返回校验正确
				}
				else
				{
					SysStatus = SYS_IDLE;
					SendCMD(RxCID1, RxCID2, 'N', 0x01);//校验错误
					break;
				}
			
			case SYS_HANDLE:						//校验成功后进行处理，该过程不应时间过长
				TxCID1 = RxCID1;					//准备发送通信的3个CID
				TxCID2 = RxCID2;
				TxCID3 = 'B';
			
				if(RxCID1 == 'R')
				{
					if(RxCID3 == 'L')			//询问包长，读命令使用
					{
						BMSTxLen.W = 0x0002;
						BMSTxADDR.W = 0x0000;
						BMSTxData[0] = 0x04;	//MG32F02A每页为1024字节，即0x400，因此每次接收1024字节数据即可
						BMSTxData[1] = 0x00;
					}
					
					if(RxCID3 == 'G')			//上位机请求复位
					{	
						ResetEnable =1;			//使能复位功能
						BMSTxLen.W = 0x0002;
						BMSTxADDR.W = 0x0000;
						BMSTxData[0] = 0x04;
						BMSTxData[1] = 0x00;
					}
					
				}

				if(RxCID1 == 'W')
				{
					if(RxCID3 == 'E')			//上位机请求数据访问
					{
						WriteEnable =1;			//使能数据读写功能
						BMSTxLen.W = 0x0002;
						BMSTxADDR.W = 0x0000;
						BMSTxData[0] = 0x04;
						BMSTxData[1] = 0x00;
						
					}

					if(RxCID3 == 'G')			//上位机请求复位
					{
						ResetEnable =1;
					}

					if(RxCID3 == 'D' && WriteEnable == 1)   	//进行数据操作
					{
						DFUPageCnt.W=BMSRxLen.W/1024;			//MG32F02A每页为1024字节，计算出一个包的数据有多少页
						if((BMSRxLen.W&0x3FF)!=0)				
						{
							DFUPageCnt.W++;						//不满一页的按照1页增加
						}

						UncodeService(BMSRxData,1024);			//数据缓冲区数据解密
						
						if(MEM_FlashAPPageErase(FLASH_AP_STARTADDRESS+(BMSRxADDR.W*16),DFUPageCnt.W)==DRV_Success)		//AP区页擦除，*16是因为解压缩传输地址
						{
							if(MEM_FlashAPProgram(FLASH_AP_STARTADDRESS+(BMSRxADDR.W*16),(uint32_t)(BMSRxData), ((sizeof(BMSRxData)) / 4))==DRV_Success)//AP区写数据
							{
								ProgramCheck = 1;		//擦除及写入均成功使能回复传输无异常
							}
						}
					}
		
				}

				SysStatus++;

			case SYS_SENDCMD:		//发送回传协议
				if(RxCID3 =='L'||RxCID3 =='E'||RxCID3=='G')	//发送DATA，返回有效数据包长
				{
					BMSTxData[0] = 0x04;
					BMSTxData[1] = 0x00;
				}
				else
				{
					BMSTxData[0] = 0x00;
					if(CheckOK)				//校验正常
					{
						if(ProgramCheck ==1)	//AP区写入正常
						{
							BMSTxData[1] = 0x00;
						}
						CheckOK =0;
					}

					else
					{
						BMSTxData[1] = 0x01;
					}
				}
				
				BMSLRCTxData.W = TxCID1+TxCID2+TxCID3+BMSTxLen.W+BMSTxData[0]+BMSTxData[1];
				BMSLRCTxData.W = ~(BMSLRCTxData.W);
				BMSLRCTxData.W = BMSLRCTxData.W+0x0001;		//运算发送协议校验帧
				
				SendByte(':');		//发送起点帧
				SendByte(':');
				SendByte(':');	
				SendByte(':');

				SendByte(TxCID1);	//发送CID帧
				SendByte(TxCID2);
				SendByte(TxCID3);

				SendByte(BMSTxLen.B.BHigh);		//发送LEN帧
				SendByte(BMSTxLen.B.BLow);

				SendByte(0x00);				//发送ADDR帧:00 00
				SendByte(0x00);

				SendByte(BMSTxData[0]);		//发送Data帧
				SendByte(BMSTxData[1]);
				
				SendByte(BMSLRCTxData.B.BHigh);		//发送LRC帧
				SendByte(BMSLRCTxData.B.BLow);

				SendByte(0x0D);		//发送结束帧
				SendByte(0x0D);
				SendByte(0x0D);
				SendByte(0x0D);
				
				if(ResetEnable ==1)		//若前面使能复位功能，则进行复位
				{
					__MEM_UnProtect();
					__MEM_SetBootSelect(AP_Space);
					__MEM_Protect();
					
					RST->KEY.H[0] = 0xA217;
					RST->CR0.B[0] |= RST_CR0_SW_EN_mask_h0;
								
					__ISB();
					
					RST->WE.W |= RST_WE_SW_WE_mask_w;
					PE13=1;
					//__NVIC_SystemReset();
					__ISB();

				}
				SysStatus = SYS_IDLE;
			break;	
						
			
				
		}
		
		
	}
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	CSC_Init();
	GPIO_Init();
	TICK_Init();
	URT0_Init();
	URT3_Init();
	
	while(1)
    {
    	if(LedTime==0)
    	{
			PE14=!PE14;
			LedTime=100;
		}
		if(ISPTime ==0)		//500ms超时复位，AP启动
		{
			__MEM_UnProtect();
			__MEM_SetBootSelect(AP_Space);
			__MEM_Protect();
					
			RST->KEY.H[0] = 0xA217;
			RST->CR0.B[0] |= RST_CR0_SW_EN_mask_h0;
								
			__ISB();
					
			RST->WE.W |= RST_WE_SW_WE_mask_w;
			PE13=1;
					//__NVIC_SystemReset();
			__ISB();
		}

		UpgradeProc();		//轮询更新协议

    }
   
}
