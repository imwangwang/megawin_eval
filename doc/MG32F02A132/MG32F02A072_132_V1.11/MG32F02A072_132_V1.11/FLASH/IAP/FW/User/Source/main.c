

/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "MG32x02z_DRV.H"
#include "Type.h"
#include "UserDefine.h"
#include <stdio.h>

#include "Sample_MEM_FlashAccess.h"
extern  void Sample_MEM_FlashIAPAccess(void);



void URT0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
	uint32_t URT_Flag;
	uint32_t URT_IT_Msk;
	
	uint32_t data;
    
    
    IRQ_ID = EXIC_GetITSourceID(URT0_IRQn);      
    if(IRQ_ID & EXIC_SRC5_ID20_urt0_b0)
    {
		
		
		URT_Flag = URT_GetITAllFlagStatus(URT0);
		URT_IT_Msk = URT_GetITStatus(URT0);
		
		if((URT_Flag & URT_STA_RXF_mask_w) && (URT_IT_Msk & URT_INT_RX_IE_mask_w))
		{
			
			//---when RX Data Register have data---//
			data = URT_GetRXData(URT0);
			URT_SetTXData(URT0, 1, data);
			
			// To do ......
			URT_ClearITFlag(URT0,URT_IT_RX);
		}
    }
}

void SysTick_Handler(void)
{
    //to do......
	IncTick();
}


void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
    
	
    UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
    MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
    ProtectModuleReg(MEMprotect);

    UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO


	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_PLLO);	


	/* Configure ICKO function */
		
	/* Configure peripheral clock */
	CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
 	CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortC,ENABLE);
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);
	
	CSC_PeriphProcessClockSource_Config(CSC_I2C0_CKS, CK_APB);
	CSC_PeriphOnModeClock_Config(CSC_ON_I2C0, ENABLE);					  // Enable IIC0 module clock

 	CSC_PeriphOnModeClock_Config(CSC_ON_DMA,ENABLE);
	

    ProtectModuleReg(CSCprotect);
    
}


int fputc(int ch,FILE *f)
{
	
	URT_SetTXData(URT0,1,ch);
	while(URT_GetITSingleFlagStatus(URT0,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URT0,URT_IT_TC);
	
	return ch;
}

void UartSendByte(int ch)
{
	
	URT_SetTXData(URT0,1,ch);
	while(URT_GetITSingleFlagStatus(URT0,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URT0,URT_IT_TC);
	
}

void InitGPIO(void)
{
	PIN_InitTypeDef PINX_InitStruct;
    
    
	//==Set GPIO init
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 		// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse

	PINX_InitStruct.PINX_Alternate_Function  = PE13_AF_GPE13;				// Pin AFS = GPIO
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 		// 

	PINX_InitStruct.PINX_Alternate_Function  = PE14_AF_GPE14;				// Pin AFS = GPIO
	GPIO_PinMode_Config(PINE(14),&PINX_InitStruct); 					 		// 

	PINX_InitStruct.PINX_Alternate_Function  = PE15_AF_GPE15;				// Pin AFS = GPIO
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 	// 
  



}

/***********************************************************************************
void Sample_URT0_Init(void)
		  TXD(PB8),RXD(PB9)
		  8,n,1 115200bps@12MHz

  
*************************************************************************************/
void InitUART0(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    
	PIN_InitTypeDef PINX_InitStruct;
	//==Set GPIO init
	//PB8 PPO TX ,PB9 ODO RX
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function  = PB8_AF_URT0_TX;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD at PB8

	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = PB9_AF_URT0_RX;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD at PB9

    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                	//Set PSR
    URT_BRG.URT_BaudRateCounterReload = 12;	                	//Set RLR
    URT_BaudRateGenerator_Config(URT0, &URT_BRG);		    	//BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URT0, ENABLE);	            	//Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URT0, URT_TXClock_Internal);		//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URT0, URT_RXClock_Internal);		//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URT0, 31);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URT0, 31);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URT0, URT_RXSMP_3TIME);
    URT_TX_Cmd(URT0, ENABLE);	                            	//Enable TX
    URT_RX_Cmd(URT0, ENABLE);	                            	//Enable RX
    
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URT0, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URT0, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URT0, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URT0, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URT0, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URT0, 0);
    
    //=====Enable URT Interrupt=====//
    //URT_IT_Config(URTX, URT_IT_RX, ENABLE);
    //URT_ITEA_Cmd(URTX, ENABLE);
    //NVIC_EnableIRQ(URT0_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URT0, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}


int main()
{

	u32 temp;
	u32 addr;
	
	CSC_Init();
	InitGPIO();
	InitUART0();

	
	InitTick(48000000,0);
	NVIC_EnableIRQ(SysTick_IRQn);
	PE13=1;
	PE14=1;
	PE15=1;
	printf("Hello!\n");
	
    MEM_SetIAPSize(4096);
	
	PE13=0;
	Sample_MEM_FlashIAPPageErase(0x1A000000, 1);
	PE13=1;
	PE14=0;
    Sample_MEM_FlashIAPSingleProgram(0x1A000800UL, 0x89ABCDEFUL);
	PE14=1;
    Sample_MEM_FlashIAPSingleProgram(0x1A000804UL, 0x76543210UL);
	PE14=0;
    Sample_MEM_FlashIAPSingleProgram(0x1A000808UL, 0xFEDCBA98UL);
	PE14=1;
    Sample_MEM_FlashIAPSingleProgram(0x1A00080CUL, 0x01234567UL);
	PE14=0;
 //   Sample_MEM_FlashIAPProgram(0x1A000000UL, (uint32_t)(&Value0), (sizeof(Value0) / 4));
	PE14=1;
	//Sample_MEM_FlashIAPAccess();
	
	addr=0x1A000800UL;
	
	temp = *(__IO u8*)(addr);

    printf("addr:0x%x, data:0x%x\r\n", addr, temp);		//Read 1byte one time
	
	temp = *(__IO u16*)(addr);

    printf("addr:0x%x, data:0x%x\r\n", addr, temp);		//Read 2byte one time
	temp = *(__IO u32*)(addr);

    printf("addr:0x%x, data:0x%x\r\n", addr, temp);		//Read 4byte one time

	while(1)
    {
		Delay(100);
		PE15=!PE15;
		
    }
}




















