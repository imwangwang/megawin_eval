/**
 *******************************************************************************
 *
 * @file        MG32x02z_SYS_DRV.H
 *
 * @brief       This file contains all the functions prototypes for the SYS 
 *              firmware library.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 *******************************************************************************
 * @par 		Disclaimer 
 * The Demo software is provided "AS IS" without any warranty, either 
 * expressed or implied, including, but not limited to, the implied warranties 
 * of merchantability and fitness for a particular purpose. The author will 
 * not be liable for any special, incidental, consequential or indirect 
 * damages due to loss of data or any other reason. 
 * These statements agree with the world wide and local dictated laws about 
 * authorship and violence against these laws. 
 *******************************************************************************
 *******************************************************************************
 */ 

#ifndef _MG32x02z_SYS_DRV_H
/*!< _MG32x02z_SYS_DRV_H */ 
#define _MG32x02z_SYS_DRV_H 



#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_SYS.h"


/**
 * @name	Function announce
 *   		
 */ 
///@{  
void SYS_ITEA_Cmd (FunctionalState NewState);
uint32_t SYS_GetChipMID (void);
uint8_t SYS_ReadGeneralReg (void);
void SYS_WriteGeneralReg (uint8_t SYS_Value);

#if defined(MG32F02A032)
uint32_t SYS_ReadBackupReg (void);
void SYS_WriteBackupReg (uint32_t SYS_Value);
#endif
///@}


#endif  //_MG32x02z_SYS_DRV_H

