





/**
 ******************************************************************************
 *
 * @file        MG32x02z_IRQ_Init.c
 * @brief       The IRQ Init C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *   
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */



#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_IRQ_Init.h"
#include "MG32x02z_CM0_DRV.h"
 
 /**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */ 
void IRQ_Init(void)
{
    #if NMI_IRQHandler_En      ==1 
        NVIC_EnableIRQ(NMI_IRQn);
    #endif
    #if HardFault_IRQHandler_En==1 
        NVIC_EnableIRQ(HardFault_IRQn);
    #endif
    #if SVC_IRQHandler_En      ==1 
        NVIC_EnableIRQ(SVC_IRQn);
    #endif 
    #if PendSV_IRQHandler_En   ==1 
        NVIC_EnableIRQ(PendSV_IRQn);
    #endif 
    #if SysTick_IRQHandler_En  ==1 
        CM0_SysTickIT_Cmd(ENABLE);
    #endif 
    #if WWDT_IRQHandler_En     ==1
        NVIC_EnableIRQ(WWDT_IRQn);
    #endif
    #if SYS_IRQHandler_En      ==1 
        NVIC_EnableIRQ(SYS_IRQn); 
    #endif
    #if EXINT0_IRQHandler_En   ==1 
        NVIC_EnableIRQ(EXINT0_IRQn); 
    #endif
    #if EXINT1_IRQHandler_En   ==1 
        NVIC_EnableIRQ(EXINT1_IRQn);
    #endif 
    #if EXINT2_IRQHandler_En   ==1 
        NVIC_EnableIRQ(EXINT2_IRQn); 
    #endif
    #if EXINT3_IRQHandler_En   ==1 
        NVIC_EnableIRQ(EXINT3_IRQn);
    #endif 
    #if COMP_IRQHandler_En     ==1 
        NVIC_EnableIRQ(COMP_IRQn);
    #endif 
    #if DMA_IRQHandler_En      ==1
        NVIC_EnableIRQ(DMA_IRQn);
    #endif
    #if ADC_IRQHandler_En      ==1 
        NVIC_EnableIRQ(ADC_IRQn);        
    #endif
    #if DAC_IRQHandler_En      ==1
        NVIC_EnableIRQ(DAC_IRQn);
    #endif
    #if TM0x_IRQHandler_En     ==1 
        NVIC_EnableIRQ(TM0x_IRQn); 
    #endif
    #if TM10_IRQHandler_En     ==1 
        NVIC_EnableIRQ(TM10_IRQn);
    #endif 
    #if TM1x_IRQHandler_En     ==1 
        NVIC_EnableIRQ(TM1x_IRQn);
    #endif 
    #if TM20_IRQHandler_En     ==1 
        NVIC_EnableIRQ(TM20_IRQn); 
    #endif
    #if TM2x_IRQHandler_En     ==1 
        NVIC_EnableIRQ(TM2x_IRQn); 
    #endif
    #if TM3x_IRQHandler_En     ==1 
        NVIC_EnableIRQ(TM3x_IRQn); 
    #endif
    #if URT0_IRQHandler_En     ==1 
        NVIC_EnableIRQ(URT0_IRQn);
    #endif   
    #if URT123_IRQHandler_En   ==1 
        NVIC_EnableIRQ(URT123_IRQn);
    #endif 
    #if SPI0_IRQHandler_En     ==1 
        NVIC_EnableIRQ(SPI0_IRQn);
    #endif 
    #if I2C0_IRQHandler_En     ==1 
        NVIC_EnableIRQ(I2C0_IRQn);
    #endif 
    #if I2Cx_IRQHandler_En     ==1 
        NVIC_EnableIRQ(I2Cx_IRQn); 
    #endif    
}
