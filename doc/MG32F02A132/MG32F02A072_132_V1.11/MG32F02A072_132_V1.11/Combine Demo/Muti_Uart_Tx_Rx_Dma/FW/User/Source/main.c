/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用DMA传输数据到URT0输出。
 *				此demo会在串口打印Hello之后将Sendbuf
 *				内容使用DMA输出到串口输出。
 *				注意：
 *
 *
 *
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */

#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_URT_DRV.H"
#include "MG32x02z_DRV.H"
#include <stdio.h>


#define URTX URT2
#define MYBINARYIMAGE2_LENGTH 20

uint8_t SendBuf0[MYBINARYIMAGE2_LENGTH]={1,2,3,4,5,6,7,8,9};//DMA0发送缓冲
uint8_t SendBuf1[MYBINARYIMAGE2_LENGTH]={1,2,3,4,5,6,7,8,9};//DMA1发送缓冲
uint8_t SendBuf2[MYBINARYIMAGE2_LENGTH]={1,2,3,4,5,6,7,8,9};//DMA2发送缓冲

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

//if user wants to delay 1ms and CK_TM00_PR is 12MHz.
//The Total clocks is 12M*1ms = 12000.
//User can set "clock prescaler"=100 and "MainCounter"=120 .   
#define Simple_Time_Prescaler       100
#define Simple_Time_MainCounter     120
#define TM10_Simple_Time_Prescaler       1000
#define TM10_Simple_Time_MainCounter     120

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
    
	
    UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
    MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
    ProtectModuleReg(MEMprotect);

    UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO


	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
	
    ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	int fputc(int ch,FILE *f)
功能描述:	printf函数重定向
输入参数:	int ch,FILE *f
返回参数:	ch  
*************************************************************************************/
int fputc(int ch,FILE *f)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
	return ch;
}

/***********************************************************************************
函数名称:	void UartSendByte(int n,int ch)
功能描述:	Uart发送函数
输入参数:	int n,int ch
返回参数:	
*************************************************************************************/
void UartSendByte(int n,int ch)
{
	if(n==0)
	{
	URT_SetTXData(URT0,1,ch);
	while(URT_GetITSingleFlagStatus(URT0,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URT0,URT_IT_TC);
	}
	else if(n==1)
	{
	URT_SetTXData(URT1,1,ch);
	while(URT_GetITSingleFlagStatus(URT1,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URT1,URT_IT_TC);
	}
	else if(n==2)
	{
	URT_SetTXData(URT2,1,ch);
	while(URT_GetITSingleFlagStatus(URT2,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URT2,URT_IT_TC);
	}
	
}

/***********************************************************************************
函数名称:	void URT0_Init(void)
功能描述:	UART0初始化 
		  TXD(PB8),RXD(PB9)
		  8,n,1 9600bps@12MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void URT0_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
	PIN_InitTypeDef PINX_InitStruct;

	UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
	CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);
	CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);
	ProtectModuleReg(CSCprotect);
    
	//==Set GPIO init
	//PB8 PPO TX ,PB9 ODO RX
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD at PB8

	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD at PB9
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                	//Set PSR
    URT_BRG.URT_BaudRateCounterReload = 49;	                	//Set RLR
    URT_BaudRateGenerator_Config(URT0, &URT_BRG);		    	//BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URT0, ENABLE);	            	//Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URT0, URT_TXClock_Internal);		//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URT0, URT_RXClock_Internal);		//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URT0, 24);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URT0, 24);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URT0, URT_RXSMP_3TIME);
    URT_TX_Cmd(URT0, ENABLE);	                            	//Enable TX
    URT_RX_Cmd(URT0, ENABLE);	                            	//Enable RX
    
    
 
    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URT0, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URT0, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URT0, URT_DataLine_2);
    
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URT0, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URT0, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URT0, 0);
    
    //=====Enable URT Interrupt=====//
    URT_IT_Config(URT0, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URT0, ENABLE);
    NVIC_EnableIRQ(URT0_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URT0, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}

void URT0_IRQHandler(void)    			//串口0中断
{
	
	uint32_t URT_Flag;
	uint32_t URT_IT_Msk;
	uint32_t data;
		
	URT_Flag = URT_GetITAllFlagStatus(URT0);
	URT_IT_Msk = URT_GetITStatus(URT0);
	
	if((URT_Flag & URT_STA_RXF_mask_w) && (URT_IT_Msk & URT_INT_RX_IE_mask_w))
	{
		u32 data=0;
		//---when RX Data Register have data---//
		data = URT_GetRXData(URT0);
		URT_SetTXData(URT0, 1, data);
		// To do ......
		URT_ClearITFlag(URT0,URT_IT_RX);
	}


}

void URT1_IRQ(void)    			//串口1中断
{
	uint32_t URT_Flag;
	uint32_t URT_IT_Msk;
	uint32_t data;
	
	
	URT_Flag = URT_GetITAllFlagStatus(URT1);
	URT_IT_Msk = URT_GetITStatus(URT1);


	if((URT_Flag & URT_STA_RXF_mask_w) && (URT_IT_Msk & URT_INT_RX_IE_mask_w))
	{
		u32 data=0;
		//---when RX Data Register have data---//
		data = URT_GetRXData(URT1);
		URT_SetTXData(URT1, 1, data);
		
		// To do ......
		URT_ClearITFlag(URT1,URT_IT_RX);
	}


}

void URT2_IRQ(void)    			//串口2中断
{
	uint32_t URT_Flag;
	uint32_t URT_IT_Msk;
	uint32_t data;
	
	
	URT_Flag = URT_GetITAllFlagStatus(URT2);
	URT_IT_Msk = URT_GetITStatus(URT2);


	if((URT_Flag & URT_STA_RXF_mask_w) && (URT_IT_Msk & URT_INT_RX_IE_mask_w))
	{
		u32 data=0;
		//---when RX Data Register have data---//
		data = URT_GetRXData(URT2);
		URT_SetTXData(URT2, 1, data);
		
		// To do ......
		URT_ClearITFlag(URT2,URT_IT_RX);
	}


}


void URT123_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(URT123_IRQn);      
    if(IRQ_ID & EXIC_SRC5_ID21_urt1_b1)
    {
        URT1_IRQ();
    }
    if( IRQ_ID & EXIC_SRC5_ID21_urt2_b1)
    {
        URT2_IRQ();
    }

}




/***********************************************************************************
函数名称:	void URT1_Init(void)
功能描述:	UART1初始化 
		  TXD(PC8),RXD(PC9)
		  8,n,1 9600bps@12MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void URT1_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
	PIN_InitTypeDef PINX_InitStruct;

	UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_UART1_CKS, CK_APB);
	CSC_PeriphOnModeClock_Config(CSC_ON_UART1,ENABLE);
	CSC_PeriphOnModeClock_Config(CSC_ON_PortC,ENABLE);
	ProtectModuleReg(CSCprotect);
    
	//==Set GPIO init
	//PB8 PPO TX ,PB9 ODO RX
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function  = 4;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINC(8),&PINX_InitStruct); 					 		// TXD at PB8

	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = 4;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINC(9),&PINX_InitStruct); 					 		// RXD at PB9
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                	//Set PSR
    URT_BRG.URT_BaudRateCounterReload = 49;	                	//Set RLR
    URT_BaudRateGenerator_Config(URT1, &URT_BRG);		    	//BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URT1, ENABLE);	            	//Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URT1, URT_TXClock_Internal);		//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URT1, URT_RXClock_Internal);		//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URT1, 24);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URT1, 24);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URT1, URT_RXSMP_3TIME);
    URT_TX_Cmd(URT1, ENABLE);	                            	//Enable TX
    URT_RX_Cmd(URT1, ENABLE);	                            	//Enable RX
    
    
 
    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URT1, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URT1, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URT1, URT_DataLine_2);
    
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URT1, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URT1, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URT1, 0);
    
    //=====Enable URT Interrupt=====//
    URT_IT_Config(URT1, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URT1, ENABLE);
    NVIC_EnableIRQ(URT123_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URT1, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}

/***********************************************************************************
函数名称:	void URT2_Init(void)
功能描述:	UART2初始化 
		  TXD(PB2),RXD(PB3)
		  8,n,1 9600bps@12MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void URT2_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
	PIN_InitTypeDef PINX_InitStruct;

	UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_UART2_CKS, CK_APB);
	CSC_PeriphOnModeClock_Config(CSC_ON_UART2,ENABLE);
	CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);
	ProtectModuleReg(CSCprotect);
    
	//==Set GPIO init
	//PB8 PPO TX ,PB9 ODO RX
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function  = 4;				// Pin AFS = URT0_TX
	GPIO_PinMode_Config(PINB(2),&PINX_InitStruct); 					 		// TXD at PB8

	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
	PINX_InitStruct.PINX_Alternate_Function  = 4;				// Pin AFS = URT0_RX
	GPIO_PinMode_Config(PINB(3),&PINX_InitStruct); 					 		// RXD at PB9
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                	//Set PSR
    URT_BRG.URT_BaudRateCounterReload = 49;	                	//Set RLR
    URT_BaudRateGenerator_Config(URT2, &URT_BRG);		    	//BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URT2, ENABLE);	            	//Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URT2, URT_TXClock_Internal);		//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URT2, URT_RXClock_Internal);		//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URT2, 24);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URT2, 24);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URT2, URT_RXSMP_3TIME);
    URT_TX_Cmd(URT2, ENABLE);	                            	//Enable TX
    URT_RX_Cmd(URT2, ENABLE);	                            	//Enable RX
    
    
 
    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URT2, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URT2, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URT2, URT_DataLine_2);
    
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URT2, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URT2, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URT2, 0);
    
    //=====Enable URT Interrupt=====//
    URT_IT_Config(URT2, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URT2, ENABLE);
    NVIC_EnableIRQ(URT123_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URT2, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}



/***********************************************************************************
函数名称:	void DMA0_Init (void)
功能描述:	DMA0初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void DMA0_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;
	 
	 UnProtectModuleReg(CSCprotect);
	 CSC_PeriphOnModeClock_Config(CSC_ON_DMA,ENABLE);
	 ProtectModuleReg(CSCprotect);

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = ENABLE;
        DMATestPattern.DestDINCSel = DISABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_MEM_Read;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_URT0_TX;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = MYBINARYIMAGE2_LENGTH;				//传输数据量，单位为字节，并不是多少个数据包
    
        // source/destination config
        DMATestPattern.DMASourceAddr = (uint8_t *)&SendBuf0;
        DMATestPattern.DMADestinationAddr = &URT0->TDAT;

		 DMA_Base_Init(&DMATestPattern);
}

/***********************************************************************************
函数名称:	void DMA1_Init (void)
功能描述:	DMA1初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void DMA1_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;
	 
	 UnProtectModuleReg(CSCprotect);
	 CSC_PeriphOnModeClock_Config(CSC_ON_DMA,ENABLE);
	 ProtectModuleReg(CSCprotect);

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel1, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel1;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = ENABLE;
        DMATestPattern.DestDINCSel = DISABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_MEM_Read;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_URT1_TX;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = MYBINARYIMAGE2_LENGTH;				//传输数据量，单位为字节，并不是多少个数据包
    
        // source/destination config
        DMATestPattern.DMASourceAddr = (uint8_t *)&SendBuf1;
        DMATestPattern.DMADestinationAddr = &URT1->TDAT;

		 DMA_Base_Init(&DMATestPattern);
}

/***********************************************************************************
函数名称:	void DMA2_Init (void)
功能描述:	DMA2初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void DMA2_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;
	 
	 UnProtectModuleReg(CSCprotect);
	 CSC_PeriphOnModeClock_Config(CSC_ON_DMA,ENABLE);
	 ProtectModuleReg(CSCprotect);

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel2, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel2;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = ENABLE;
        DMATestPattern.DestDINCSel = DISABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_MEM_Read;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_URT2_TX;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = MYBINARYIMAGE2_LENGTH;				//传输数据量，单位为字节，并不是多少个数据包
    
        // source/destination config
        DMATestPattern.DMASourceAddr = (uint8_t *)&SendBuf2;
        DMATestPattern.DMADestinationAddr = &URT2->TDAT;

		 DMA_Base_Init(&DMATestPattern);
}


/***********************************************************************************
函数名称:	void TM00_Delay_Init (void)
功能描述:	TM00初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void TM00_Delay_Init(void)
{  
		TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;

		UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_TM00_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_TM00, ENABLE);					  // Enable TM00 module clock
		
		ProtectModuleReg(CSCprotect);
	
    // ----------------------------------------------------
    // 1.initial TimeBase structure 
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_Period = Simple_Time_MainCounter - 1; 
    TM_TimeBase_InitStruct.TM_Prescaler = Simple_Time_Prescaler - 1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
    
    TM_TimeBase_Init(TM00, &TM_TimeBase_InitStruct);
		
		TM_ClearFlag(TM00, TMx_TOF);
    
    // ----------------------------------------------------
    // 3.Start TM00 
    TM_Timer_Cmd(TM00, ENABLE);

    // ----------------------------------------------------
    // 4.until TOF flag event (polling)
    while(TM_GetSingleFlagStatus(TM00, TMx_TOF) == DRV_UnHappened);
    TM_ClearFlag(TM00, TMx_TOF);        // clear TOF flag

	TM_IT_Config(TM00,TMx_TIE_IE, ENABLE);
	TM_ITEA_Cmd(TM00, ENABLE);
	NVIC_EnableIRQ(TM0x_IRQn);

	
		
}

/***********************************************************************************
函数名称:	void TM10_Delay_Init (void)
功能描述:	TM10初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void TM10_Delay_Init(void)
{  
		TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;

		UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_TM10_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_TM10, ENABLE);					  // Enable TM01 module clock
		ProtectModuleReg(CSCprotect);
	
    // ----------------------------------------------------
    // 1.initial TimeBase structure 
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_Period = TM10_Simple_Time_MainCounter - 1; 
    TM_TimeBase_InitStruct.TM_Prescaler = TM10_Simple_Time_Prescaler - 1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
    
    TM_TimeBase_Init(TM10, &TM_TimeBase_InitStruct);
		
		TM_ClearFlag(TM10, TMx_TOF);
    
    // ----------------------------------------------------
    // 3.Start TM10
    TM_Timer_Cmd(TM10, ENABLE);

    // ----------------------------------------------------
    // 4.until TOF flag event (polling)
    while(TM_GetSingleFlagStatus(TM10, TMx_TOF) == DRV_UnHappened);
    TM_ClearFlag(TM10, TMx_TOF);        // clear TOF flag

	TM_IT_Config(TM10,TMx_TIE_IE, ENABLE);
	TM_ITEA_Cmd(TM10, ENABLE);
	NVIC_EnableIRQ(TM10_IRQn);
		
}


void TM0x_IRQHandler(void)				//1毫秒中断
{
	if(TM_GetSingleFlagStatus(TM00, TMx_TOF) == DRV_Happened)
	{
		//中断内容
		TM_ClearFlag(TM00, TMx_TOF);        // clear TOF flag
	}
	
	
}

void TM10_IRQHandler(void)				//10毫秒中断
{
	if(TM_GetSingleFlagStatus(TM10, TMx_TOF) == DRV_Happened)
	{
		//中断内容
		TM_ClearFlag(TM10, TMx_TOF);        // clear TOF flag
	}
	
	
}




/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	int i;
	CSC_Init();
	URT0_Init();			//串口0初始化
    URT1_Init();			//串口1初始化
	URT2_Init();			//串口2初始化
	
	TM00_Delay_Init();   	//1ms中断初始化
	TM10_Delay_Init();	    //10ms中断初始化

	DMA0_Init();    //使能串口0 DMA发送
	DMA1_Init();    //使能串口1 DMA发送
	DMA2_Init();    //使能串口2 DMA发送
	
//	printf("Hello!\n");
//	UartSendByte(0,0x48);
//	UartSendByte(1,0x48);
//	UartSendByte(2,0x48);

	while(1)
	{
	  i++;
    if(i>=500000)
    {
			i=0;
			strcpy(SendBuf0,"i am source 0");
			strcpy(SendBuf1,"i am source 1");
			strcpy(SendBuf2,"i am source 2");
			DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
			DMA_ClearFlag(DMA, DMA_FLAG_CH1_TCF);
			DMA_ClearFlag(DMA, DMA_FLAG_CH2_TCF);
			URT_TXDMA_Cmd(URT0, ENABLE);
			URT_TXDMA_Cmd(URT1, ENABLE);
			URT_TXDMA_Cmd(URT2, ENABLE);
			DMA_StartRequest(DMAChannel0);
			DMA_StartRequest(DMAChannel1);
			DMA_StartRequest(DMAChannel2);
       
			while (DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_TCF) == DRV_UnHappened);
			DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
			while (DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH1_TCF) == DRV_UnHappened);
			DMA_ClearFlag(DMA, DMA_FLAG_CH1_TCF);
			while (DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH2_TCF) == DRV_UnHappened);
			DMA_ClearFlag(DMA, DMA_FLAG_CH2_TCF);
		

		}
	}

}




















