

// <<< Use Configuration Wizard in Context Menu >>>
//<o0> URT BaudRateGenerator clock source frequence is(2000000-48000000) <2000000-48000000>
//<o1> Baudrate is(600-256000) <600-6000000>
#define URT_BDCLK_SOURCE     25000000
#define URT_BR_VALUE         460800
// <<< end of configuration section >>>



#include "MG32x02z.H"
#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_URT_DRV.H"
#include "MG32x02z_GPIO_DRV.H"



/**
 *****************************************************************************************
 * @fn      UART_TXRXBaudRate_Configuration
 * @brief   URT interrupts all enable or disable
 * @details  
 * @param   URTx          : where x can be (0 to 3) to select the UART peripheral
 * @param   URT_Frequence : PROC frequence value (ex 12MHz =  12000000)
 * @param   URTBaudRate   : TX and RX baudrate value (ex 9600)
 * @return        
 * @bug         
 * @note    1.The RX and TX clock source is from uart internal clock in the function.
 *          2.The interanl clock is from PROC (APB or System clock according CSC setting)
 *          3.The baud rate is about the value of URTBaudRate.
 *****************************************************************************************
 */
DRV_Return URT_TXRXBaudRate_Configuration(URT_Struct* URTX , uint32_t URT_Frequence , uint32_t URTBaudRate )
{
	int8_t   i,j,k;
	uint32_t tmp;
	uint8_t  TXOverSamplingSampleNumber;
    uint8_t  RXOverSamplingSampleNumber;
	URT_BRG_TypeDef URT_BRGStruct;

	tmp = URT_Frequence / URTBaudRate;											
	//================TX and RX oversamplig value===================
	j = 0;
	for(i=8;i<32;i++)
	{
		k = tmp % i;
		if(k==0)
		{
			TXOverSamplingSampleNumber = (i - 1);
			RXOverSamplingSampleNumber = (i - 1);
			break;
		}
		else
		{
			if((i-k)>j)
			{
				j = i - k;
				TXOverSamplingSampleNumber = (i - 1);
				RXOverSamplingSampleNumber = (i - 1);
			}
		}
	}
	
	//==========prescaler and baudrate counter reload value============
	tmp = tmp / (TXOverSamplingSampleNumber  + 1);
	j = 0;
	for(i=2;i<16;i++)															
	{
		k = tmp % i;
		if(k==0)
		{
			URT_BRGStruct.URT_PrescalerCounterReload = (i - 1);
			break;
		}
		else
		{
			if((i-k)>j)
			{
				j = (i - k);
				URT_BRGStruct.URT_PrescalerCounterReload = (i - 1);
			}
		}
	}
	tmp = tmp / (URT_BRGStruct.URT_PrescalerCounterReload + 1);
	if(tmp==0)
	{
		return(DRV_Failure);												    //TX and RX baudrate can't setting.
	}
	URT_BRGStruct.URT_BaudRateCounterReload = (tmp - 1);
	
	
	    
    //----------------Baud Rate Generator Configuration----------------
    URT_BRGStruct.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRGStruct.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BaudRateGenerator_Config(URTX,&URT_BRGStruct);
    
    //----------------RX / TX Baud Rate Configuration------------------
    URT_TXClockSource_Select(URTX , URT_TXClock_Internal);
    URT_RXClockSource_Select(URTX , URT_RXClock_Internal);
    URT_RXOverSamplingMode_Select( URTX, URT_RXSMP_3TIME);
    URT_TXOverSamplingSampleNumber_Select( URTX, TXOverSamplingSampleNumber);
	URT_RXOverSamplingSampleNumber_Select( URTX, RXOverSamplingSampleNumber);
	URT_BaudRateGenerator_Cmd(URTX , ENABLE);

	return(DRV_Success);
}

/**
 *****************************************************************************************
 * @fn      UART_Inital
 * @brief   UART inital
 * @details  
 * @param   URTx          : where x can be (0 to 3) to select the UART peripheral
 * @param   
 * @param   
 * @return        
 * @bug         
 * @note    
 *****************************************************************************************
 */
DRV_Return URT_Init(URT_Struct* URTX)
{
    DRV_Return URT_Init_Result;
    
	URT_Data_TypeDef URT_Datastruct;

    
    //---------Data character configuration-----------
    URT_Datastruct.URT_TX_DataLength = URT_DataLength_8;
    URT_Datastruct.URT_RX_DataLength = URT_DataLength_8;
    URT_Datastruct.URT_TX_DataOrder  = URT_DataTyped_LSB;
    URT_Datastruct.URT_RX_DataOrder  = URT_DataTyped_LSB;
    URT_Datastruct.URT_TX_Parity     = URT_Parity_No;
    URT_Datastruct.URT_RX_Parity     = URT_Parity_No;
    URT_Datastruct.URT_TX_StopBits   = URT_StopBits_1_0;
    URT_Datastruct.URT_RX_StopBits   = URT_StopBits_1_0;
    URT_Datastruct.URT_TX_DataInverse= DISABLE;
    URT_Datastruct.URT_RX_DataInverse= DISABLE;
    URT_DataCharacter_Config(URTX,&URT_Datastruct);   
    
    
    //---------------Mode setting----------------------
    URT_Mode_Select( URTX , URT_URT_mode);                                                   // Normal URT mode
    URT_DataLine_Select(URTX,URT_DataLine_2);                                                // Data bus is 2 line ( RX and TX)
	
    //----------------Data control---------------------
    URT_RXShadowBufferThreshold_Select(URTX,URT_RXTH_1BYTE );                                // RX shadowBuffer level is 1 byte.
    
    //------------Baudrate configuration--------------
    URT_Init_Result = URT_TXRXBaudRate_Configuration(URTX,URT_BDCLK_SOURCE,URT_BR_VALUE);

    if(URT_Init_Result==DRV_Success)
    {
        
       URT_TX_Cmd(URTX,ENABLE);
       URT_RX_Cmd(URTX,ENABLE);
	   URT_Cmd(URTX,ENABLE);
    }
    return(URT_Init_Result);
}


































