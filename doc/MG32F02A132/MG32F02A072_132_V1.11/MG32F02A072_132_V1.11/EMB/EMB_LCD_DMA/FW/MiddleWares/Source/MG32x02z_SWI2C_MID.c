#include "MG32F02A_MID_SWI2C.h"



/*
//=============================================================================
//
//=============================================================================
*/
void SWI2Cdelay(void)                  // for 100KHz I2C: SCL High/Low duration = 5us/5us
{                                      // 5us= 60mc for @12MHz
                                       // ! NOPs v.s. Clock rates of S/W I2C
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
     __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
}



/*
//=============================================================================
//
//=============================================================================
*/
void SWI2C_Init(void)
{
    SWI2C_SCL2H();
    // SWI2C_SCL2L();

    // GPIO Config SDA & SCL OpenDrain
    PINB(12)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(13)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
}



void SWI2C2_Init(void)
{
    SWI2C2_SCL2H();
    // SWI2C2_SCL2L();

    // GPIO Config SDA & SCL OpenDrain
    PINB(12)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(13)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
}



/*
//=============================================================================
//
//=============================================================================
*/
void SWI2C_Slave_Detection_Start(void)
{
    SWI2C_SCL2H();

    // Bus is Logic-High
    WAIT_SWI2C_SCL2H();
    WAIT_SWI2C_SDA2H();

    // Detection_Start
    WAIT_SWI2C_SDA2L();
    WAIT_SWI2C_SCL2L();

    // Keep SCL on Low
    SWI2C_SCL2L();
}



void SWI2C2_Slave_Detection_Start(void)
{
    SWI2C2_SCL2H();

    // Bus is Logic-High
    WAIT_SWI2C2_SCL2H();
    WAIT_SWI2C2_SDA2H();

    // Detection_Start
    WAIT_SWI2C2_SDA2L();
    WAIT_SWI2C2_SCL2L();

    // Keep SCL on Low
    SWI2C2_SCL2L();
}



/*
//=============================================================================
//
//=============================================================================
*/
void SWI2C_Slave_Detection_Stop(void)
{
    WAIT_SWI2C_SDA2L();
    SWI2C_SCL2H();
    WAIT_SWI2C_SCL2H();
    WAIT_SWI2C_SDA2H();
}



void SWI2C2_Slave_Detection_Stop(void)
{
    WAIT_SWI2C2_SDA2L();
    SWI2C2_SCL2H();
    WAIT_SWI2C2_SCL2H();
    WAIT_SWI2C2_SDA2H();
}



/*
//=============================================================================
//
//=============================================================================
*/
//Slave Transmitter
//
//             MST          MST           MST  MST
//             rcv          rcv           rcv  snd
//             D7           D6            D0   ACK
//         SLV |        SLV |         SLV |    |       SLV
//         snd |        snd |         snd |    |       rcv
//         D7  |        D6  |         D0  |    |       ACK
//                                              
//             ����Ŀ       ����Ŀ        ����Ŀ       ����Ŀ
// SCL  ...�����1   �   �����2   �..  �����8   �   �����9   �...
//
//       

uint8_t SWI2C_Slave_DataByte_Transmit(uint8_t data_transmit)
{
    uint8_t bit_count = 8;
    uint8_t ACK_bit = 0;
    do{
        // Slave send data bit
        if((data_transmit & 0x80) == 0x80)
        {
            SWI2C_SDA2H();
            WAIT_SWI2C_SDA2H();
        }
        else
        {
            SWI2C_SDA2L();
            WAIT_SWI2C_SDA2L();
        }
        SWI2C_SCL2H();
        WAIT_SWI2C_SCL2H();
        WAIT_SWI2C_SCL2L();
        SWI2C_SCL2L();
        data_transmit = data_transmit << 1;
    }while(-- bit_count != 0);

    SWI2C_SDA2H();                              // release SDA

    /* receive ACK from Master/Receiver */
    SWI2C_SCL2H();
    WAIT_SWI2C_SCL2H();                         // wait SCL 0->1 ;* 9th clock

    if(SWI2C_SDA() != 0)                        // Slave receive ACK bit
        ACK_bit = SWI2C_NACK;
    else
        ACK_bit = SWI2C_ACK;

    WAIT_SWI2C_SCL2L();                         // wait SCL 1->0 ;*
    SWI2C_SCL2L();                              // !! S/W stretch SCL's Low

    return ACK_bit;
}



uint8_t SWI2C2_Slave_DataByte_Transmit(uint8_t data_transmit)
{
    uint8_t bit_count = 8;
    uint8_t ACK_bit = 0;
    do{
        // Slave send data bit
        if((data_transmit & 0x80) == 0x80)
        {
            SWI2C2_SDA2H();
            WAIT_SWI2C2_SDA2H();
        }
        else
        {
            SWI2C2_SDA2L();
            WAIT_SWI2C2_SDA2L();
        }
        SWI2C2_SCL2H();
        WAIT_SWI2C2_SCL2H();
        WAIT_SWI2C2_SCL2L();
        SWI2C2_SCL2L();
        data_transmit = data_transmit << 1;
    }while(-- bit_count != 0);

    SWI2C2_SDA2H();                              // release SDA

    /* receive ACK from Master/Receiver */
    SWI2C2_SCL2H();
    WAIT_SWI2C2_SCL2H();                         // wait SCL 0->1 ;* 9th clock

    if(SWI2C2_SDA() != 0)                        // Slave receive ACK bit
        ACK_bit = SWI2C2_NACK;
    else
        ACK_bit = SWI2C2_ACK;

    WAIT_SWI2C2_SCL2L();                         // wait SCL 1->0 ;*
    SWI2C2_SCL2L();                              // !! S/W stretch SCL's Low

    return ACK_bit;
}



/*
//=============================================================================
//
//=============================================================================
*/
//Slave Receiver
//
//        MST       MST          MST                  MST
//        snd       snd          snd                  rcv
//        D7        D6           D0                   ACK
//        |    SLV  |       SLV  |        SLV     SLV  |
//        |    rcv  |       rcv  |        rcv     snd  |
//        |    D7   |       D6   |        D0      ACK  |
//                                              
//             ����Ŀ       ����Ŀ        ����Ŀ       ����Ŀ
// SCL  ...�����1   �   �����2   �..  �����8   �   �����9   �...
//
//       

uint8_t SWI2C_Slave_DataByte_Receive(uint8_t ACK_bit)
{
    uint8_t bit_count = 8; 
    uint8_t i2c_data = 0;

    do{                                         // next_bit_from_Master
        i2c_data = i2c_data << 1;

        SWI2C_SCL2H();
        WAIT_SWI2C_SCL2H();                     // wait SCL 0->1

        if(SWI2C_SDA())                         // Slave receive data bit
            i2c_data |= 0x01;
        else
            i2c_data &= 0xFE;

        WAIT_SWI2C_SCL2L();                     // wait SCL 1->0
        SWI2C_SCL2L();
    }while(-- bit_count != 0);

    /* return ACK or NOT ACK to Master/Transmitter */
    if(ACK_bit != 0)                                 // Slave return ACK or NOT ACK
    {
        SWI2C_SDA2H();
        WAIT_SWI2C_SDA2H();
    }
    else
    {
        SWI2C_SDA2L();
        WAIT_SWI2C_SDA2L();
    }

    SWI2C_SCL2H();
    WAIT_SWI2C_SCL2H();                       // wait SCL 0->1 ;* 9th clock
    WAIT_SWI2C_SCL2L();                       // wait SCL 1->0 ;*
    SWI2C_SCL2L();                            // !! S/W stretch SCL's Low

    SWI2C_SDA2H();                            // Slave release SDA

    // data_receive = i2c_data;
    return i2c_data;
}



uint8_t SWI2C2_Slave_DataByte_Receive(uint8_t ACK_bit)
{
    uint8_t bit_count = 8; 
    uint8_t i2c_data = 0;

    do{                                         // next_bit_from_Master
        i2c_data = i2c_data << 1;

        SWI2C2_SCL2H();
        WAIT_SWI2C2_SCL2H();                     // wait SCL 0->1

        if(SWI2C2_SDA())                         // Slave receive data bit
            i2c_data |= 0x01;
        else
            i2c_data &= 0xFE;

        WAIT_SWI2C2_SCL2L();                     // wait SCL 1->0
        SWI2C2_SCL2L();
    }while(-- bit_count != 0);

    /* return ACK or NOT ACK to Master/Transmitter */
    if(ACK_bit != 0)                                 // Slave return ACK or NOT ACK
    {
        SWI2C2_SDA2H();
        WAIT_SWI2C2_SDA2H();
    }
    else
    {
        SWI2C2_SDA2L();
        WAIT_SWI2C2_SDA2L();
    }

    SWI2C2_SCL2H();
    WAIT_SWI2C2_SCL2H();                       // wait SCL 0->1 ;* 9th clock
    WAIT_SWI2C2_SCL2L();                       // wait SCL 1->0 ;*
    SWI2C2_SCL2L();                            // !! S/W stretch SCL's Low

    SWI2C2_SDA2H();                            // Slave release SDA

    // data_receive = i2c_data;
    return i2c_data;
}



/*
//=============================================================================
//
//=============================================================================
*/
//Slave Receiver
//
//        MST       MST          MST                  MST
//        snd       snd          snd                  rcv
//        D7        D6           D0                   ACK
//        |    SLV  |       SLV  |        SLV     SLV  |
//        |    rcv  |       rcv  |        rcv     snd  |
//        |    D7   |       D6   |        D0      ACK  |
//                                              
//             ����Ŀ       ����Ŀ        ����Ŀ       ����Ŀ
// SCL  ...�����1   �   �����2   �..  �����8   �   �����9   �...
//
//       

uint8_t SWI2C_Slave_AddressByte_Receive(uint8_t Address_8bit)
{
    uint8_t bit_count = 8; 
    uint8_t i2c_data = 0;
    uint8_t Slave_Addr = (Address_8bit & 0xFE);

    do{                                         // next_bit_from_Master
        i2c_data = i2c_data << 1;

        SWI2C_SCL2H();
        WAIT_SWI2C_SCL2H();                     // wait SCL 0->1

        if(SWI2C_SDA())                         // Slave receive data bit
            i2c_data |= 0x01;
        else
            i2c_data &= 0xFE;

        WAIT_SWI2C_SCL2L();                     // wait SCL 1->0
        SWI2C_SCL2L();
    }while(-- bit_count != 0);

//return ACK or NOT ACK to Master/Transmitter
    if((i2c_data & 0xFE) == Slave_Addr)// Slave return ACK or NOT ACK
    {
        SWI2C_SDA2L();
        WAIT_SWI2C_SDA2L();                     // wait SDA 1->0
    }
    else
    {
        SWI2C_SDA2H();
        WAIT_SWI2C_SDA2H();                     // wait SDA 0->1
    }

    SWI2C_SCL2H();
    WAIT_SWI2C_SCL2H();                       // wait SCL 0->1 ;* 9th clock
    WAIT_SWI2C_SCL2L();                       // wait SCL 1->0 ;*
    SWI2C_SCL2L();                            // !! S/W stretch SCL's Low

    SWI2C_SDA2H();                            // Slave release SDA

    // data_receive = i2c_data;
    return i2c_data;
}



uint8_t SWI2C2_Slave_AddressByte_Receive(uint8_t Address_8bit)
{
    uint8_t bit_count = 8; 
    uint8_t i2c_data = 0;
    uint8_t Slave_Addr = (Address_8bit & 0xFE);

    do{                                         // next_bit_from_Master
        i2c_data = i2c_data << 1;

        SWI2C2_SCL2H();
        WAIT_SWI2C2_SCL2H();                     // wait SCL 0->1

        if(SWI2C2_SDA())                         // Slave receive data bit
            i2c_data |= 0x01;
        else
            i2c_data &= 0xFE;

        WAIT_SWI2C2_SCL2L();                     // wait SCL 1->0
        SWI2C2_SCL2L();
    }while(-- bit_count != 0);

//return ACK or NOT ACK to Master/Transmitter
    if((i2c_data & 0xFE) == Slave_Addr)// Slave return ACK or NOT ACK
    {
        SWI2C2_SDA2L();
        WAIT_SWI2C2_SDA2L();                     // wait SDA 1->0
    }
    else
    {
        SWI2C2_SDA2H();
        WAIT_SWI2C2_SDA2H();                     // wait SDA 0->1
    }

    SWI2C2_SCL2H();
    WAIT_SWI2C2_SCL2H();                       // wait SCL 0->1 ;* 9th clock
    WAIT_SWI2C2_SCL2L();                       // wait SCL 1->0 ;*
    SWI2C2_SCL2L();                            // !! S/W stretch SCL's Low

    SWI2C2_SDA2H();                            // Slave release SDA

    // data_receive = i2c_data;
    return i2c_data;
}



/*
//=============================================================================
//
//=============================================================================
*/
void SWI2C_Master_Output_Start(void)
{
    SWI2C_SDA2H();
    WAIT_SWI2C_SDA2H();                         // wait SDA 0->1

    SWI2C_SCL2H();
    WAIT_SWI2C_SCL2H();                         // wait SCL 0->1

    SWI2C_SDA2L();
    WAIT_SWI2C_SDA2L();                         // wait SDA 1->0
    SWI2Cdelay();

    SWI2C_SCL2L();
    WAIT_SWI2C_SCL2L();                         // wait SCL 1->0
    SWI2Cdelay();
}



void SWI2C2_Master_Output_Start(void)
{
    SWI2C2_SDA2H();
    WAIT_SWI2C2_SDA2H();                         // wait SDA 0->1

    SWI2C2_SCL2H();
    WAIT_SWI2C2_SCL2H();                         // wait SCL 0->1

    SWI2C2_SDA2L();
    WAIT_SWI2C2_SDA2L();                         // wait SDA 1->0
    SWI2Cdelay();

    SWI2C2_SCL2L();
    WAIT_SWI2C2_SCL2L();                         // wait SCL 1->0
    SWI2Cdelay();
}



/*
//=============================================================================
//
//=============================================================================
*/
void SWI2C_Master_Output_Stop(void)
{
    SWI2C_SDA2L();
    WAIT_SWI2C_SDA2L();                         // wait SDA 1->0

    SWI2Cdelay();

    SWI2C_SCL2H();
    WAIT_SWI2C_SCL2H();                         // wait SCL 0->1

    SWI2Cdelay();

    SWI2C_SDA2H();
    WAIT_SWI2C_SDA2H();                         // wait SDA 0->1

    SWI2Cdelay();
}



void SWI2C2_Master_Output_Stop(void)
{
    SWI2C2_SDA2L();
    WAIT_SWI2C2_SDA2L();                         // wait SDA 1->0

    SWI2Cdelay();

    SWI2C2_SCL2H();
    WAIT_SWI2C2_SCL2H();                         // wait SCL 0->1

    SWI2Cdelay();

    SWI2C2_SDA2H();
    WAIT_SWI2C2_SDA2H();                         // wait SDA 0->1

    SWI2Cdelay();
}



/*
//=============================================================================
//
//=============================================================================
*/
//Master Transmitter
//
//         MST          MST           MST              MST
//         snd          snd           snd              rcv
//         D7           D6            D0               ACK
//         |   SLV      |   SLV       |   SLV  SLV     |
//         |   rcv      |   rcv       |   rcv  snd     |
//         |   D7       |   D6        |   D0   ACK     |
//                                              
//             ����Ŀ       ����Ŀ        ����Ŀ       ����Ŀ
// SCL  ...�����1   �   �����2   �..  �����8   �   �����9   �...
//
//       
//       clr SI

uint8_t SWI2C_Master_DataByte_Transmit(uint8_t data_transmit)
{
    uint8_t ACK_bit = 0;
    uint8_t bit_count = 8;
    
    do{
        if((data_transmit & 0x80) != 0)
        {
            SWI2C_SDA2H();                      // Master send data bit
            WAIT_SWI2C_SDA2H();                 // wait SDA 0->1
        }
        else
        {
            SWI2C_SDA2L();                      // Master send data bit
            WAIT_SWI2C_SDA2L();                 // wait SDA 1->0
        }
        SWI2Cdelay();
 
        SWI2C_SCL2H();                          // SCL 0->1
        WAIT_SWI2C_SCL2H();                     // wait SCL 0->1

        SWI2Cdelay();
        // SWI2Cdelay();

        SWI2C_SCL2L();                          // SCL 1->0
        WAIT_SWI2C_SCL2L();
        // SWI2Cdelay();
        data_transmit = data_transmit << 1;
    }while(-- bit_count != 0);

    SWI2C_SDA2H();                          // SDA Release
    SWI2Cdelay();    
    /* receive ACK from Slave/Receiver */
    
    SWI2C_SCL2H();                              // SCL 0->1
    WAIT_SWI2C_SCL2H();                         // !! S/W wait until H/W Slave release SCL

    if(SWI2C_SDA() != 0)                        // Master receive ACK bit
        ACK_bit = SWI2C_NACK;
    else
        ACK_bit = SWI2C_ACK;
    
    SWI2Cdelay();
    // SWI2Cdelay();    
    
    SWI2C_SCL2L();                              // SCL 1->0
    WAIT_SWI2C_SCL2L();                         // !! S/W wait until H/W Slave SCL to Low

    // SWI2Cdelay();    

    return ACK_bit;
}



uint8_t SWI2C2_Master_DataByte_Transmit(uint8_t data_transmit)
{
    uint8_t ACK_bit = 0;
    uint8_t bit_count = 8;
    
    do{
        if((data_transmit & 0x80) != 0)
        {
            SWI2C2_SDA2H();                      // Master send data bit
            WAIT_SWI2C2_SDA2H();                 // wait SDA 0->1
        }
        else
        {
            SWI2C2_SDA2L();                      // Master send data bit
            WAIT_SWI2C2_SDA2L();                 // wait SDA 1->0
        }
        SWI2Cdelay();
 
        SWI2C2_SCL2H();                          // SCL 0->1
        WAIT_SWI2C2_SCL2H();                     // wait SCL 0->1

        SWI2Cdelay();
        // SWI2Cdelay();

        SWI2C2_SCL2L();                          // SCL 1->0
        WAIT_SWI2C2_SCL2L();
        // SWI2Cdelay();
        data_transmit = data_transmit << 1;
    }while(-- bit_count != 0);

    SWI2C2_SDA2H();                          // SDA Release
    SWI2Cdelay();    
    /* receive ACK from Slave/Receiver */
    
    SWI2C2_SCL2H();                              // SCL 0->1
    WAIT_SWI2C2_SCL2H();                         // !! S/W wait until H/W Slave release SCL

    if(SWI2C2_SDA() != 0)                        // Master receive ACK bit
        ACK_bit = SWI2C2_NACK;
    else
        ACK_bit = SWI2C2_ACK;
    
    SWI2Cdelay();
    // SWI2Cdelay();    
    
    SWI2C2_SCL2L();                              // SCL 1->0
    WAIT_SWI2C2_SCL2L();                         // !! S/W wait until H/W Slave SCL to Low

    // SWI2Cdelay();    

    return ACK_bit;
}




/*
//=============================================================================
//
//=============================================================================
*/
// Master Receiver
// 
//              MST          MST           MST  MST
//              rcv          rcv           rcv  snd
//              D7           D6            D0   ACK
//        SLV   |    SLV     |    SLV      |    |       SLV
//        snd   |    snd     |    snd      |    |       rcv
//        D7    |    D6      |    D0       |    |       ACK
//                                               
//              ����Ŀ       ����Ŀ        ����Ŀ       ����Ŀ
//  SCL  ...�����1   �   �����2   �..  �����8   �   �����9   �...
// 
//       

uint8_t SWI2C_Master_DataByte_Receive(uint8_t ACK_bit)
{
    uint8_t data_receive = 0;
    uint8_t bit_count = 8;
    
    do{
        data_receive = data_receive << 1;

        SWI2Cdelay();
        //SWI2Cdelay();

        SWI2C_SCL2H();                          // SCL 0->1
        WAIT_SWI2C_SCL2H();                     // !! S/W wait until H/W Slave release SCL

        if(SWI2C_SDA() != 0)                    // Master receive data bit
            data_receive |= 1;
        else
            data_receive &= 0xFE;

        SWI2Cdelay();
        //SWI2Cdelay();

        SWI2C_SCL2L();                          // SCL 1->0
    }while(-- bit_count != 0);

    /* return ACK or NOT ACK to Slave/Transmitter */

    if(ACK_bit != 0)                                 // Master return ACK or NOT ACK
    {
        SWI2C_SDA2H(); 
        WAIT_SWI2C_SDA2H();
    }
    else 
    {
        SWI2C_SDA2L(); 
        WAIT_SWI2C_SDA2L();
    }
    SWI2Cdelay();
    //SWI2Cdelay();

    SWI2C_SCL2H();                              // SCL 0->1 ;* 9th clock
    WAIT_SWI2C_SCL2H();                         // !! S/W wait until H/W Slave release SCL
    SWI2Cdelay();
    //SWI2Cdelay();
    
    SWI2C_SCL2L();                              // SCL 1->0 ;*
    SWI2Cdelay();
    //SWI2Cdelay();
    SWI2C_SDA2H();                              // Slave release SDA

    return data_receive;
}



uint8_t SWI2C2_Master_DataByte_Receive(uint8_t ACK_bit)
{
    uint8_t data_receive = 0;
    uint8_t bit_count = 8;
    
    do{
        data_receive = data_receive << 1;

        SWI2Cdelay();
        //SWI2Cdelay();

        SWI2C2_SCL2H();                          // SCL 0->1
        WAIT_SWI2C2_SCL2H();                     // !! S/W wait until H/W Slave release SCL

        if(SWI2C2_SDA() != 0)                    // Master receive data bit
            data_receive |= 1;
        else
            data_receive &= 0xFE;

        SWI2Cdelay();
        //SWI2Cdelay();

        SWI2C2_SCL2L();                          // SCL 1->0
    }while(-- bit_count != 0);

    /* return ACK or NOT ACK to Slave/Transmitter */

    if(ACK_bit != 0)                                 // Master return ACK or NOT ACK
    {
        SWI2C2_SDA2H(); 
        WAIT_SWI2C2_SDA2H();
    }
    else 
    {
        SWI2C2_SDA2L(); 
        WAIT_SWI2C2_SDA2L();
    }
    SWI2Cdelay();
    //SWI2Cdelay();

    SWI2C2_SCL2H();                              // SCL 0->1 ;* 9th clock
    WAIT_SWI2C2_SCL2H();                         // !! S/W wait until H/W Slave release SCL
    SWI2Cdelay();
    //SWI2Cdelay();
    
    SWI2C2_SCL2L();                              // SCL 1->0 ;*
    SWI2Cdelay();
    //SWI2Cdelay();
    SWI2C2_SDA2H();                              // Slave release SDA

    return data_receive;
}



/*
//=============================================================================
//
//=============================================================================
*/
//Master Transmitter
//
//         MST          MST           MST              MST
//         snd          snd           snd              rcv
//         D7           D6            D0               ACK
//         |   SLV      |   SLV       |   SLV  SLV     |
//         |   rcv      |   rcv       |   rcv  snd     |
//         |   D7       |   D6        |   D0   ACK     |
//                                              
//             ����Ŀ       ����Ŀ        ����Ŀ       ����Ŀ
// SCL  ...�����1   �   �����2   �..  �����8   �   �����9   �...
//
//       
//       clr SI

uint8_t SWI2C_Master_DataByte_Transfer_Virtual(uint8_t data_transmit, uint8_t ACK_bit)
{
    uint8_t bit_count = 8;
    
    do{
        if((data_transmit & 0x80) != 0)
        {
            SWI2C_SDA2H();                      // Master send data bit
            WAIT_SWI2C_SDA2H();                 // wait SDA 0->1
        }
        else
        {
            SWI2C_SDA2L();                      // Master send data bit
            WAIT_SWI2C_SDA2L();                 // wait SDA 1->0
        }
        SWI2Cdelay();
 
        SWI2C_SCL2H();                          // SCL 0->1
        WAIT_SWI2C_SCL2H();                     // wait SCL 0->1

        SWI2Cdelay();
        // SWI2Cdelay();

        SWI2C_SCL2L();                          // SCL 1->0
        WAIT_SWI2C_SCL2L();
        // SWI2Cdelay();
        data_transmit = data_transmit << 1;
    }while(-- bit_count != 0);

    if(ACK_bit == 0x00)
    {
        SWI2C_SDA2L();                          // SDA Release
        WAIT_SWI2C_SDA2L();                     // wait SDA 1->0
    }
    else
    {
        SWI2C_SDA2H();                          // SDA Release
        WAIT_SWI2C_SDA2H();                     // wait SDA 0->1
    }

    SWI2Cdelay();    
   
    SWI2C_SCL2H();                              // SCL 0->1
    WAIT_SWI2C_SCL2H();                         // !! S/W wait until H/W Slave release SCL
    
    SWI2Cdelay();
    // SWI2Cdelay();    
    
    SWI2C_SCL2L();                              // SCL 1->0
    WAIT_SWI2C_SCL2L();                         // !! S/W wait until H/W Slave SCL to Low

    SWI2Cdelay();    
    SWI2C_SDA2H();                              // SDA Release
    // WAIT_SWI2C_SDA2H();                         // wait SDA 0->1
 
    return ACK_bit;
}


