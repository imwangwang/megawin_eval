/**
 *******************************************************************************
 * @file        MG32x02z_SPI_MID.h
 *
 * @brief       The SPI middle code h file
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/02/21
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * #001_Hades_
 *  >> 
 *
 @endif
 *******************************************************************************
 */

#include "MG32x02z_Common_MID.h"


#ifndef _MG32x02z_SPI_MID_H
/*!< _MG32x02z_SPI_MID_H */ 
#define _MG32x02z_SPI_MID_H


/*! @name 
*/
/* @{ */ 
//#define nCS			PB0
#define nCS			PD6
#define Dummy_Data  0xFFFFFFFF
/* @} */


/*! @enum   MDI_SPI_TIAD_Enum
    @brief  SPIx transfer interface and direction.
*/
typedef enum
{
    MID_SPI_StanardSpi              = 0x00,
    MDI_SPI_1lineDataIn             = 0x10,
    MDI_SPI_1lineDataOut            = 0x14,
    MDI_SPI_2linesSeparateDataIn    = 0x20,
    MDI_SPI_2linesSeparateDataOut   = 0x24,
    MDI_SPI_2linesCopyDataIn        = 0x28,
    MDI_SPI_2linesCopyDataOut       = 0x2C,
    MDI_SPI_4linesSeparateDataIn    = 0x30,
    MDI_SPI_4linesSeparateDataOut   = 0x34,
    MDI_SPI_4linesCopyDataIn        = 0x38,
    MDI_SPI_4linesCopyDataOut       = 0x3C,
    MDI_SPI_4linesDuplicateDataOut  = 0x44,
    MDI_SPI_8linesDataIn            = 0x50,
    MDI_SPI_8linesDataOut           = 0x54
}MDI_SPI_TIAD_Enum;


/**
 * @name	Function announce
 *   		
 */ 
///@{ 
//void MID_Flash_Read_ID(void);
uint32_t MID_Flash_Read_ID (void);

void MID_Flash_Write_Enable(void);
void MID_Flash_Check_Busy(void);

void MID_Flash_Page_Program(uint32_t Addr, uint8_t *DataSource, uint16_t Length);
///@}


#endif  // _MG32x02z_SPI_MID_H


