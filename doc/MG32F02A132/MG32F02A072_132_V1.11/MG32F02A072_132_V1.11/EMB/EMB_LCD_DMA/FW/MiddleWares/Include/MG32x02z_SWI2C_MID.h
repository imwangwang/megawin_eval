#ifndef __Mx32F02A_MID_SWI2C_H
#define __Mx32F02A_MID_SWI2C_H

#ifdef __cplusplus
 extern "C" {
#endif


#include "MG32F02A_MID_Common.h"



// software I2C to test the hardware I2C
#define SWI2C_SCL()       (GPIOB->IN.H[0] & PX_IN_IN12_mask_h0)
#define SWI2C_SDA()       (GPIOB->IN.H[0] & PX_IN_IN13_mask_h0)

//#define SCL_MASK()      PX_IN_IN12_mask_h0
//#define SDA_MASK()      PX_IN_IN13_mask_h0

#define SWI2C_SCL2L()         GPIOB->SC.H[1] = PX_SC_CLR12_mask_h1
#define SWI2C_SCL2H()         GPIOB->SC.H[0] = PX_SC_SET12_mask_h0
#define SWI2C_SDA2L()         GPIOB->SC.H[1] = PX_SC_CLR13_mask_h1
#define SWI2C_SDA2H()         GPIOB->SC.H[0] = PX_SC_SET13_mask_h0

#define WAIT_SWI2C_SCL2L()    while((GPIOB->IN.H[0] & PX_IN_IN12_mask_h0) != 0)
#define WAIT_SWI2C_SCL2H()    while((GPIOB->IN.H[0] & PX_IN_IN12_mask_h0) == 0)
#define WAIT_SWI2C_SDA2L()    while((GPIOB->IN.H[0] & PX_IN_IN13_mask_h0) != 0)
#define WAIT_SWI2C_SDA2H()    while((GPIOB->IN.H[0] & PX_IN_IN13_mask_h0) == 0)



// software I2C to test the hardware I2C
#define SWI2C2_SCL()       (GPIOB->IN.H[0] & PX_IN_IN2_mask_h0)
#define SWI2C2_SDA()       (GPIOB->IN.H[0] & PX_IN_IN3_mask_h0)

//#define SCL_MASK()      PX_IN_IN12_mask_h0
//#define SDA_MASK()      PX_IN_IN13_mask_h0

#define SWI2C2_SCL2L()         GPIOB->SC.H[1] = PX_SC_CLR2_mask_h1
#define SWI2C2_SCL2H()         GPIOB->SC.H[0] = PX_SC_SET2_mask_h0
#define SWI2C2_SDA2L()         GPIOB->SC.H[1] = PX_SC_CLR3_mask_h1
#define SWI2C2_SDA2H()         GPIOB->SC.H[0] = PX_SC_SET3_mask_h0

#define WAIT_SWI2C2_SCL2L()    while((GPIOB->IN.H[0] & PX_IN_IN2_mask_h0) != 0)
#define WAIT_SWI2C2_SCL2H()    while((GPIOB->IN.H[0] & PX_IN_IN2_mask_h0) == 0)
#define WAIT_SWI2C2_SDA2L()    while((GPIOB->IN.H[0] & PX_IN_IN3_mask_h0) != 0)
#define WAIT_SWI2C2_SDA2H()    while((GPIOB->IN.H[0] & PX_IN_IN3_mask_h0) == 0)



//extern unsigned char bdata SW_I2C_Control;
//extern bit ACK_bit;                     // 0:Active, 1:Not Active



#define     SWI2C_ACK       0x00
#define     SWI2C_NACK      0x01
#define     SWI2C2_ACK      0x00
#define     SWI2C2_NACK     0x01



void SWI2Cdelay(void);



void SWI2C_Init(void);
void SWI2C2_Init(void);



void SWI2C_Slave_Detection_Start(void);
void SWI2C2_Slave_Detection_Start(void);
void SWI2C_Slave_Detection_Stop(void);
void SWI2C2_Slave_Detection_Stop(void);



uint8_t SWI2C_Slave_DataByte_Transmit(uint8_t data_transmit);
uint8_t SWI2C2_Slave_DataByte_Transmit(uint8_t data_transmit);
uint8_t SWI2C_Slave_DataByte_Receive(uint8_t ACK_bit);
uint8_t SWI2C2_Slave_DataByte_Receive(uint8_t ACK_bit);
uint8_t SWI2C_Slave_AddressByte_Receive(uint8_t Address_8bit);
uint8_t SWI2C2_Slave_AddressByte_Receive(uint8_t Address_8bit);



void SWI2C_Master_Output_Start(void);
void SWI2C2_Master_Output_Start(void);
void SWI2C_Master_Output_Stop(void);
void SWI2C2_Master_Output_Stop(void);



uint8_t SWI2C_Master_DataByte_Transmit(uint8_t data_transmit);
uint8_t SWI2C2_Master_DataByte_Transmit(uint8_t data_transmit);
uint8_t SWI2C_Master_DataByte_Receive(uint8_t ACK_bit);
uint8_t SWI2C2_Master_DataByte_Receive(uint8_t ACK_bit);



uint8_t SWI2C_Master_DataByte_Transfer_Virtual(uint8_t data_transmit, uint8_t ACK_bit);



#ifdef __cplusplus
}
#endif

#endif


