
/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __MG32x02z_Module_Function_H
#define __MG32x02z_Module_Function_H

#ifdef __cplusplus
 extern "C" {
#endif 



/* Includes -----------------------------------------------------------------*/

/* Exported types -----------------------------------------------------------*/

/* Exported constants -------------------------------------------------------*/

/* Exported macro -----------------------------------------------------------*/

/* Exported functions -------------------------------------------------------*/



#ifdef __cplusplus
}
#endif

#endif

/******************* (C) COPYRIGHT 2018 Megawin ************ END OF FILE *****/    


