/**
 ******************************************************************************
 *
 * @file        MG32x02z_TM_MID.c
 *
 * @brief       This file provides firmware functions to manage the following 
 *              functionalities of the TM peripheral:
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/02/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *>>
 *--
 *--
 *>>
 *>>
 *
 @endif 
 ******************************************************************************
 */ 




#include "MG32x02z_TM_MID.H"



/**
 * @name	Initial/Deinitial TM with TM_HandleTypeDef
 *   		
 */ 
///@{ 
/**
 *******************************************************************************
 * @brief       Initial TM with TM_HandleTypeDef.
 * @param[in]   htm : pointer to a TM_HandleTypeDef
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    TM_HandleTypeDef htm;
 
    MID_TM_PWM_Init(&TM_Handle);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_PWM_Init(TM_HandleTypeDef *htm)
{
  if(htm->State == MID_TM_STATE_RESET)
  {
    /* Allocate lock resource and initialize it */
    htm->Lock = MID_UnLocked;
    
    /* Init the low level hardware : GPIO, CLOCK, NVIC and DMA */
//    MID_TM_PWM_MspInit(htm);
  }

  /* Set the TM state */
  htm->State= MID_TM_STATE_BUSY;

  /* Init the base time for the PWM */
  TM_TimeBase_Init(htm->Instance, &htm->TimeBaseInit);

  /* Initialize the TM state*/
  htm->State= MID_TM_STATE_READY;

  return MID_Success;
}

/**
 *******************************************************************************
 * @brief       Fills each TM_HandleTypeDef member with its default value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    MID_TM_TimeBaseInitTypeDef htm;
 
    MID_TM_PWM_Init(&htm);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_PWM_DeInit(TM_HandleTypeDef *htm)
{
    
    htm->State = MID_TM_STATE_BUSY;

    /* Disable the TM Peripheral Clock */
    TM_Timer_Cmd(htm->Instance, DISABLE);

    /* DeInit the low level hardware: GPIO, CLOCK, NVIC and DMA */
    // Use define this function call
//    MID_TM_PWM_MspDeInit(htm);

    /* Change TM state */
    htm->State = MID_TM_STATE_RESET;

    /* Release Lock */
    __MID_UNLOCK(htm);

    return MID_Success;
}
///@}



/**
 * @name	PWM output config
 *   		
 */ 
///@{ 
/**
 *******************************************************************************
 * @brief       Deinitializes the TM.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param[in]   sConfig : PWM configuration structure
 * @param       Channel : TM Channels to be enabled
 *              This parameter can be one of the following values:
 *  @arg        MID_TM_Channel0: TM Channel 0 selected
 *  @arg        MID_TM_Channel1: TM Channel 1 selected
 *  @arg        MID_TM_Channel2: TM Channel 2 selected
 *  @arg        MID_TM_Channel3: TM Channel 3 selected
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    TM_HandleTypeDef htm;
    TM_OC_InitTypeDef sConfig;
    
    // initial struct htm and sConfig 
    // to do ...
    
    // 
    MID_TM_PWM_ConfigChannel(&htm, &sConfig, MID_TM_Channel0);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_PWM_ConfigChannel(TM_HandleTypeDef *htm, TM_OC_InitTypeDef* sConfig, MID_TM_ChannelSelectDef Channel)
{
    __MID_LOCK(htm);

    htm->State = MID_TM_STATE_BUSY;

    switch (Channel)
    {
    case MID_TM_Channel0:
        {
            /* Configure the Channel 0 in PWM mode */
            TM_CH0Function_Config(htm->Instance, sConfig->OCMode);

            /* Set the reload value for channel0 */
            TM_SetCC0A(htm->Instance, sConfig->ReloadValue);
            TM_SetCC0B(htm->Instance, sConfig->ReloadValue);

            /* Configure the OC00 Output pin */
            TM_InverseOC0z_Cmd(htm->Instance, sConfig->OCxInverse);
            TM_OC0zOutputState_Init(htm->Instance, sConfig->OCxIdleState);
            TM_OC00Output_Cmd(htm->Instance, sConfig->OCxEnable);
            
            /* Configure the OC0N Output pin */
            TM_InverseOC0N_Cmd(htm->Instance, sConfig->OCNInverse);
            TM_OC0NOutput_Cmd(htm->Instance, sConfig->OCNEnable);
            
            /* Configure the OC0H Output pin */
            TM_InverseOC0H_Cmd(htm->Instance, sConfig->OCHInverse);
            TM_OC0HOutputState_Init(htm->Instance, sConfig->OCHIdleState);
        }
        break;

    case MID_TM_Channel1:
        {
            /* Configure the Channel 1 in PWM mode */
            TM_CH1Function_Config(htm->Instance, sConfig->OCMode);

            /* Set the reload value for channel1 */
            TM_SetCC1A(htm->Instance, sConfig->ReloadValue);
            TM_SetCC1B(htm->Instance, sConfig->ReloadValue);

            /* Configure the OC10 Output pin */
            TM_InverseOC1z_Cmd(htm->Instance, sConfig->OCxInverse);
            TM_OC1zOutputState_Init(htm->Instance, sConfig->OCxIdleState);
            TM_OC10Output_Cmd(htm->Instance, sConfig->OCxEnable);
            
            /* Configure the OC1N Output pin */
            TM_InverseOC1N_Cmd(htm->Instance, sConfig->OCNInverse);
            TM_OC1NOutput_Cmd(htm->Instance, sConfig->OCNEnable);
            
            /* Configure the OC1H Output pin */
            TM_InverseOC1H_Cmd(htm->Instance, sConfig->OCHInverse);
            TM_OC1HOutputState_Init(htm->Instance, sConfig->OCHIdleState);
        }
        break;

    case MID_TM_Channel2:
        {
            /* Configure the Channel 2 in PWM mode */
            TM_CH2Function_Config(htm->Instance, sConfig->OCMode);

            /* Set the reload value for channel2 */
            TM_SetCC2A(htm->Instance, sConfig->ReloadValue);
            TM_SetCC2B(htm->Instance, sConfig->ReloadValue);

            /* Configure the OC2 Output pin */
            TM_InverseOC2_Cmd(htm->Instance, sConfig->OCxInverse);
            TM_OC2OutputState_Init(htm->Instance, sConfig->OCxIdleState);
            TM_OC2Output_Cmd(htm->Instance, sConfig->OCxEnable);
            
            /* Configure the OC2N Output pin */
            TM_InverseOC2N_Cmd(htm->Instance, sConfig->OCNInverse);
            TM_OC2NOutput_Cmd(htm->Instance, sConfig->OCNEnable);
            
            /* Configure the OC2H Output pin */
            TM_InverseOC2H_Cmd(htm->Instance, sConfig->OCHInverse);
            TM_OC2HOutputState_Init(htm->Instance, sConfig->OCHIdleState);
        }
        break;

    case MID_TM_Channel3:
        {
            /* Configure the Channel 3 in PWM mode */
            TM_CH3Function_Config(htm->Instance, sConfig->OCMode);

            /* Set the reload value for channel3 */
            TM_SetCC3A(htm->Instance, sConfig->ReloadValue);
            TM_SetCC3B(htm->Instance, sConfig->ReloadValue);

            /* Configure the OC3 Output pin */
            TM_InverseOC3_Cmd(htm->Instance, sConfig->OCxInverse);
            TM_OC3OutputState_Init(htm->Instance, sConfig->OCxIdleState);
            TM_OC3Output_Cmd(htm->Instance, sConfig->OCxEnable);
            
            /* Configure the OC3H Output pin */
            TM_InverseOC3H_Cmd(htm->Instance, sConfig->OCHInverse);
            TM_OC3HOutputState_Init(htm->Instance, sConfig->OCHIdleState);
        }
        break;

    default:
        break;
    }

    htm->State = MID_TM_STATE_READY;

    __MID_UNLOCK(htm);

    return MID_Success;
}

/**
 *******************************************************************************
 * @brief       Starts the PWM signal generation.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    MID_TM_PWM_Start(htm);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_PWM_Start(TM_HandleTypeDef *htm)
{
    /* Enable the Timer Capture/compare channel */
    TM_Timer_Cmd(htm->Instance, ENABLE);

    /* Return function status */
    return MID_Success;
}

/**
 *******************************************************************************
 * @brief       Stop the PWM signal generation.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    MID_TM_PWM_Start(htm, ENABLE);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_PWM_Stop(TM_HandleTypeDef *htm, FunctionalState NewState)
{
    /* Enable the Timer Capture/compare channel */
    TM_Timer_Cmd(htm->Instance, DISABLE);

    /* Return function status */
    return MID_Success;
}
///@}



/**
 * @name	PWM interrupt config
 *   		
 */ 
///@{ 
/**
 *******************************************************************************
 * @brief       Starts the PWM signal generation in interrupt mode..
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       Channel : TM Channels to be enabled
 *              This parameter can be one of the following values:
 *  @arg        MID_TM_Channel0: TM Channel 0 selected
 *  @arg        MID_TM_Channel1: TM Channel 1 selected
 *  @arg        MID_TM_Channel2: TM Channel 2 selected
 *  @arg        MID_TM_Channel3: TM Channel 3 selected
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    MID_TM_PWM_Start(htm);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_PWM_Start_IT(TM_HandleTypeDef *htm, MID_TM_ChannelSelectDef Channel)
{
    switch(Channel)
    {
    case MID_TM_Channel0:
        TM_IT_Config(htm->Instance, TMx_CC0_IE, ENABLE);
        break;
    case MID_TM_Channel1:
        TM_IT_Config(htm->Instance, TMx_CC1_IE, ENABLE);
        break;
    case MID_TM_Channel2:
        TM_IT_Config(htm->Instance, TMx_CC2_IE, ENABLE);
        break;
    case MID_TM_Channel3:
        TM_IT_Config(htm->Instance, TMx_CC3_IE, ENABLE);
        break;
    case MID_TM_ChannelAll:
        TM_IT_Config(htm->Instance, (TMx_CC0_IE | TMx_CC1_IE | TMx_CC2_IE /*| TMx_CC3_IE*/ | TMx_TIE_IE), ENABLE);
        break;
    default:
        break;
    }
    
    TM_ITEA_Cmd(htm->Instance, ENABLE);
    
    /* Enable the Timer NVIC interrupt */
    // please reference "MG32x02z_IRQ_Init.c" 
//    if(htm->Instance == TM00)
//        NVIC_EnableIRQ(TM0x_IRQn);
//    if(htm->Instance == TM01)
//        NVIC_EnableIRQ(TM0x_IRQn);
//    if(htm->Instance == TM10)
//        NVIC_EnableIRQ(TM10_IRQn);
//    if(htm->Instance == TM16)
//        NVIC_EnableIRQ(TM1x_IRQn);
//    if(htm->Instance == TM20)
//        NVIC_EnableIRQ(TM20_IRQn);
//    if(htm->Instance == TM26)
//        NVIC_EnableIRQ(TM2x_IRQn);
    if(htm->Instance == TM36)
        NVIC_EnableIRQ(TM3x_IRQn);

    /* Return function status */
    return MID_Success;
}

/**
 *******************************************************************************
 * @brief       Stop the PWM signal generation in interrupt mode.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       Channel : TM Channels to be enabled
 *              This parameter can be one of the following values:
 *  @arg        MID_TM_Channel0: TM Channel 0 selected
 *  @arg        MID_TM_Channel1: TM Channel 1 selected
 *  @arg        MID_TM_Channel2: TM Channel 2 selected
 *  @arg        MID_TM_Channel3: TM Channel 3 selected
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    MID_TM_PWM_Start(htm, ENABLE);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_PWM_Stop_IT(TM_HandleTypeDef *htm, MID_TM_ChannelSelectDef Channel)
{
    switch(Channel)
    {
    case MID_TM_Channel0:
        TM_IT_Config(htm->Instance, TMx_CC0_IE, DISABLE);
        break;
    case MID_TM_Channel1:
        TM_IT_Config(htm->Instance, TMx_CC1_IE, DISABLE);
        break;
    case MID_TM_Channel2:
        TM_IT_Config(htm->Instance, TMx_CC2_IE, DISABLE);
        break;
    case MID_TM_Channel3:
        TM_IT_Config(htm->Instance, TMx_CC3_IE, DISABLE);
        break;
    case MID_TM_ChannelAll:
        TM_IT_Config(htm->Instance, (TMx_CC0_IE | TMx_CC1_IE | TMx_CC2_IE | TMx_CC3_IE | TMx_TIE_IE), DISABLE);
        break;
    default:
        break;
    }
    
    TM_ITEA_Cmd(htm->Instance, DISABLE);
    
    /* Disable the Timer NVIC interrupt */
//    if(htm->Instance == TM00)
//        NVIC_DisableIRQ(TM0x_IRQn);
//    if(htm->Instance == TM01)
//        NVIC_DisableIRQ(TM0x_IRQn);
//    if(htm->Instance == TM10)
//        NVIC_DisableIRQ(TM10_IRQn);
//    if(htm->Instance == TM16)
//        NVIC_DisableIRQ(TM1x_IRQn);
//    if(htm->Instance == TM20)
//        NVIC_DisableIRQ(TM20_IRQn);
//    if(htm->Instance == TM26)
//        NVIC_DisableIRQ(TM2x_IRQn);
//    if(htm->Instance == TM36)
//        NVIC_DisableIRQ(TM3x_IRQn);

    /* Return function status */
    return MID_Success;
}
///@}



/**
 * @name	Accept/Reject reload duty cycle
 *   		
 */ 
///@{ 
/**
 *******************************************************************************
 * @brief       Accept update the PWM/OC register.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateOC_Start(htm);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_AcceptUpdateOC(TM_HandleTypeDef *htm)
{
    /* Accept update OC/PWM register when TM overflow */
    TM_ReloadFromCCxB_Cmd(htm->Instance, TM_SimultaneouslyReload);
    
    /* Return function status */
    return MID_Success;
}

/**
 *******************************************************************************
 * @brief       Reject update the PWM/OC register.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateOC_Start(htm);
 * @endcode
 * @bug              
 *******************************************************************************
 */
MID_StatusTypeDef MID_TM_RejectUpdateOC(TM_HandleTypeDef *htm)
{
    /* Refuse update OC/PWM register when TM overflow */
    TM_ReloadFromCCxB_Cmd(htm->Instance, TM_StopReload);
    
    /* Return function status */
    return MID_Success;
}
///@}




/**
 * @name	Set channel0 duty cycle
 *   		
 */ 
///@{ 
/**
 *******************************************************************************
 * @brief       Update channel0's PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		MID_StatusTypeDef
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel0(htm, 32768);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel0(TM_HandleTypeDef *htm, uint16_t ReloadV)
{
    /* Seting the next reload value of channel0 when TM overflow */
    TM_SetCC0B(htm->Instance, ReloadV);
    
}

/**
 *******************************************************************************
 * @brief       Update channel0's OC0H PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel0H(htm, 128);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel0H(TM_HandleTypeDef *htm, uint8_t ReloadV)
{
    /* Seting the next reload value of channel0H when TM overflow */
    htm->Instance->CC0B.B[1] = ReloadV;

}

/**
 *******************************************************************************
 * @brief       Update channel0's OC0L PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel0L(htm, 128);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel0L(TM_HandleTypeDef *htm, uint8_t ReloadV)
{
    /* Seting the next reload value of channel0H when TM overflow */
    htm->Instance->CC0B.B[0] = ReloadV;
    
}
///@}




/**
 * @name	Set channel1 duty cycle
 *   		
 */ 
///@{ 
/**
 *******************************************************************************
 * @brief       Update channel1's PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel1(htm, 32768);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel1(TM_HandleTypeDef *htm, uint16_t ReloadV)
{
    /* Seting the next reload value of channel1 when TM overflow */
    TM_SetCC0B(htm->Instance, ReloadV);
    
}

/**
 *******************************************************************************
 * @brief       Update channel1's OC1H PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel1H(htm, 128);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel1H(TM_HandleTypeDef *htm, uint8_t ReloadV)
{
    /* Seting the next reload value of channel0H when TM overflow */
    htm->Instance->CC1B.B[1] = ReloadV;
    
}

/**
 *******************************************************************************
 * @brief       Update channel1's OC1L PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel1L(htm, 128);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel1L(TM_HandleTypeDef *htm, uint8_t ReloadV)
{
    /* Seting the next reload value of channel0H when TM overflow */
    htm->Instance->CC1B.B[0] = ReloadV;
    
}
///@}



/**
 * @name	Set channel2 duty cycle
 *   		
 */ 
///@{ 
/**
 *******************************************************************************
 * @brief       Update channel2's PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel2(htm, 32768);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel2(TM_HandleTypeDef *htm, uint16_t ReloadV)
{
    /* Seting the next reload value of channel0 when TM overflow */
    TM_SetCC2B(htm->Instance, ReloadV);
    
}

/**
 *******************************************************************************
 * @brief       Update channel2's OC2H PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel2H(htm, 128);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel2H(TM_HandleTypeDef *htm, uint8_t ReloadV)
{
    /* Seting the next reload value of channel0H when TM overflow */
    htm->Instance->CC2B.B[1] = ReloadV;
    
}

/**
 *******************************************************************************
 * @brief       Update channel2's OC2L PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel2L(htm, 128);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel2L(TM_HandleTypeDef *htm, uint8_t ReloadV)
{
    /* Seting the next reload value of channel0H when TM overflow */
    htm->Instance->CC2B.B[0] = ReloadV;
    
}
///@}



/**
 * @name	Set channel3 duty cycle
 *   		
 */ 
///@{ 
/**
 *******************************************************************************
 * @brief       Update channel3's PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel3(htm, 32768);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel3(TM_HandleTypeDef *htm, uint16_t ReloadV)
{
    /* Seting the next reload value of channel1 when TM overflow */
    TM_SetCC3B(htm->Instance, ReloadV);
    
}

/**
 *******************************************************************************
 * @brief       Update channel3's OC3H PWM/OC register with reload value.
 * @param[in]   htm : pointer to a TM_HandleTypeDef.
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel3H(htm, 128);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel3H(TM_HandleTypeDef *htm, uint8_t ReloadV)
{
    /* Seting the next reload value of channel0H when TM overflow */
    htm->Instance->CC3B.B[1] = ReloadV;
    
}

/**
 *******************************************************************************
 * @brief       Update channel3's OC3L PWM/OC register with reload value.
 * @param[in]   htm : TM_HandleTypeDef
 * @param       ReloadV : the reload value into the Capture/Compare Register
 * @return		None
 * @note 
 * @par         Example
 * @code
    MID_TM_UpdateChannel3L(htm, 128);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void MID_TM_UpdateChannel3L(TM_HandleTypeDef *htm, uint8_t ReloadV)
{
    /* Seting the next reload value of channel0H when TM overflow */
    htm->Instance->CC3B.B[0] = ReloadV;
    
}
///@}




