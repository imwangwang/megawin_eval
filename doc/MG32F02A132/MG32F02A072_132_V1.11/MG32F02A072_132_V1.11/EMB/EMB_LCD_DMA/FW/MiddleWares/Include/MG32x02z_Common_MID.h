/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MG32x02z_COMMON_MID_H
#define __MG32x02z_COMMON_MID_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "MG32x02z.h"
#include "MG32x02z_DRV.h"
#include <stdio.h>

/* Exported types ------------------------------------------------------------*/

/** 
  * @brief  MID Status structures definition  
  */  
typedef enum 
{
  MID_Success  = 0x00U,
  MID_Failure  = 0x01U,
  MID_Busy     = 0x02U,
  MID_Timeout  = 0x03U
} MID_StatusTypeDef;

/** 
  * @brief  MID Lock structures definition  
  */
typedef enum 
{
  MID_UnLocked = 0x00,
  MID_Locked   = 0x01  
} MID_LockTypeDef;



/* Exported constants -------------------------------------------------------*/



/* Exported macro ------------------------------------------------------------*/
#define __MID_LOCK(__HANDLE__)                                           \
                                do{                                        \
                                    if((__HANDLE__)->Lock == MID_Locked)   \
                                    {                                      \
                                       return MID_Busy;                    \
                                    }                                      \
                                    else                                   \
                                    {                                      \
                                       (__HANDLE__)->Lock = MID_Locked;    \
                                    }                                      \
                                  }while (0)


#define __MID_UNLOCK(__HANDLE__)                                          \
                                  do{                                       \
                                      (__HANDLE__)->Lock = MID_UnLocked;    \
                                    }while (0)



/* Exported functions -------------------------------------------------------*/



#ifdef __cplusplus
}
#endif

#endif /* __MG32x02z_MID_COMMON */



/******************* (C) COPYRIGHT 2018 Megawin ************ END OF FILE *****/  


