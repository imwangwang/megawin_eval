//*** <<< Use Configuration Wizard in Context Menu >>> ***
// <o5> Execute Mode   <1=>AP Mode <2=>ISP Mode <3=>SRAM Mode <4=>EMB Mode <10=>AP Mode All Enable
//
// <o6>  AP Code Size (Byte)
// <o7>  ISP Code Size (Byte)
// <o8>  SRAM Code Size (Byte)
// <o25> Flash Total Size (KByte)
// <o31> ISP Space Size (KByte)
// <o34> IAP Space Size (KByte)
// <o37> AP Space Size (KByte)
//

//*** <<< end of configuration section >>>    ***

#define AP_Mode                 1
#define ISP_Mode                2
#define SRAM_Mode               3
#define EMB_Mode                4
#define AP_Mode_All_Enable      10

#define Execution_Mode          1           // 1 = AP Mode  ISP 4KB / IAP 64KB / AP 64KB
                                            // 2 = ISP Mode ISP 24KB / IAP 54KB / AP 54KB
                                            // 3 = SRAM Mode ISP 4KB / IAP 64KB / AP 64KB
                                            // 4 = EMB Mode
                                            // 10 = AP Mode All Enable  ISP 4KB / IAP 64KB / AP 64KB
#define APCodeSize              0x6EAC
#define ISPCodeSize             0x0000      // ISP Mode 0x5BA4
#define SRAMCodeSize            0x0000

#define EMB_Low_Boundary        0x60000000
#define EMB_Size                0x80000000

#define PReg_Low_Boundary       0x40000000
#define PReg_Size               0x20000000

#define SRAM_Low_Boundary       0x20000000
#define SRAM_Size               16*1024     // 0x1000 Byte = 16384 Byte = 16KB

#define OB2_Low_Boundary        0x1FF30000
#define OB2_Size                1*1024      // 0x400    Byte = 1024     Byte = 1KB

#define OB1_Low_Boundary        0x1FF20000
#define OB1_Size                1*1024      // 0x400    Byte = 1024     Byte = 1KB

#define OB0_Low_Boundary        0x1FF10000
#define OB0_Size                1*1024      // 0x400    Byte = 1024     Byte = 1KB

#define ISPD_Low_Boundary       0x1FF00000
#define ISPD_Size               1*1024      // 0x400    Byte = 1024     Byte = 1KB

#define Flash_Size              132*1024    // 0x21000  Byte = 135168   Byte = 128K + 4K

#define ISP_Low_Boundary        0x1C000000
#define ISP_Flash_Size          8*1024      // 0x000    Byte = 0        Byte = 0KB

#define IAP_Low_Boundary        0x1A000000
#define IAP_Flash_Size          32*1024     // 0x65536  Byte = 1024     Byte = 64KB

#define AP_Low_Boundary         0x18000000
#define AP_Flash_Size           32*1024     // 0x65536  Byte = 1024     Byte = 70KB

#define ISP_Low_Boundary_Alias1 (AP_Low_Boundary + AP_Flash_Size + IAP_Flash_Size)
#define IAP_Low_Boundary_Alias1 (AP_Low_Boundary + AP_Flash_Size)


//#if Execution_Mode==AP_Mode
    #define AP_Mode_AP_Low_Boundary_Alias   (0x00000000)
    #define AP_Mode_IAP_Low_Boundary_Alias  (AP_Mode_AP_Low_Boundary_Alias + AP_Flash_Size)
    #define AP_Mode_ISP_Low_Boundary_Alias  (AP_Mode_AP_Low_Boundary_Alias + AP_Flash_Size + IAP_Flash_Size)
    #define AP_Mode_Free_LowBoundary_Alias  (AP_Mode_AP_Low_Boundary_Alias + AP_Flash_Size + IAP_Flash_Size + ISP_Flash_Size)

//#elif Execution_Mode==ISP_Mode
    #define ISP_Mode_ISP_Low_Boundary_Alias  ((uint32_t)0x00000000)
    #define ISP_Mode_Free_LowBoundary_Alias  (ISP_Mode_ISP_Low_Boundary_Alias + ISP_Flash_Size)

//#elif Execution_Mode==SRAM_Mode
    #define SRAM_Mode_SRAM_Low_Boundary_Alias ((uint32_t)0x00000000)
    #define SRAM_Mode_Free_LowBoundary_Alias  (SRAM_Mode_SRAM_Low_Boundary_Alias + SRAM_Size)

//#elif Execution_Mode==EMB_Mode
    #define EMB_Mode_EMB_Low_Boundary_Alias  ((uint32_t)0x00000000)

//#endif



#include "MG32x02z_hal_Common.h"



/**
 *          Flag
 *          MEM Flag State
 */ 
///@{ 
#define     MEM_FLAG_ISPSEF     ((uint32_t)0x00020000)  //
#define     MEM_FLAG_IAPSEF     MEM_STA_IAPSEF_mask_w   //
#define     MEM_FLAG_RPEF       MEM_STA_RPEF_mask_w     //
#define     MEM_FLAG_WPEF       MEM_STA_WPEF_mask_w     //
#define     MEM_FLAG_IAEF       MEM_STA_IAEF_mask_w     //
#define     MEM_FLAG_EOPF       MEM_STA_EOPF_mask_w     //
#define     MEM_FLAG_FBUSYF     MEM_STA_FBUSYF_mask_w   //
///@}



/**
 *          Reset
 *          MEM Error Reset
 */ 
///@{
#define     MEM_RST_MASK        (MEM_INT_RPE_RE_mask_w | MEM_INT_WPE_RE_mask_w | MEM_INT_IAE_RE_mask_w)
#define     MEM_RST_RPE         MEM_INT_RPE_RE_mask_w   //
#define     MEM_RST_WPE         MEM_INT_WPE_RE_mask_w   //
#define     MEM_RST_IAE         MEM_INT_IAE_RE_mask_w   //
///@}



/**
 *          Interrupt
 *          MEM Interrupt
 */ 
///@{ 
#define     MEM_IT_RPE          MEM_INT_RPE_IE_mask_w   //
#define     MEM_IT_WPE          MEM_INT_WPE_IE_mask_w   //
#define     MEM_IT_IAE          MEM_INT_IAE_IE_mask_w   //
#define     MEM_IT_EOP          MEM_INT_EOP_IE_mask_w   //
#define     MEM_IT_IEA          MEM_INT_IEA_mask_w      //
///@}



/**
 *          WaitState
 *          Flash Access Time, Flash Wait State
 */ 
///@{

#define     MEM_FWAIT_Zero      MEM_CR0_FWAIT_zero_w    //   0Hz > Sysclk >= 25MHz
#define     MEM_FWAIT_One       MEM_CR0_FWAIT_one_w     // 25MHz > Sysclk >= 50MHz
#define     MEM_FWAIT_Two       MEM_CR0_FWAIT_two_w     // 50MHz > Sysclk >= 75MHz
///@}


/**
 *          Boot Select
 *          Boot From AP Flash / ISP Flash / SRAM SameTime Alias to 0x00000000
 */ 
///@{ 
#define     MEM_BOOT_MS_AP      MEM_CR0_BOOT_MS_application_flash_w //  AP Flash
#define     MEM_BOOT_MS_ISP     MEM_CR0_BOOT_MS_boot_flash_w        //  ISP Flash
#define     MEM_BOOT_MS_SRAM    MEM_CR0_BOOT_MS_embedded_sram_w     //  Internal Flash
///@}


/**
 *          Access Mode
 *          Erase / Program
 */ 
///@{ 
#define     STANDBY             MEM_CR0_MDS_no_w     
#define     FLASH_WRITE         MEM_CR0_MDS_write_w     //
#define     FLASH_PROGRAM       MEM_CR0_MDS_write_w     //
#define     FLASH_ERASE         MEM_CR0_MDS_erase_w     //
#define     ISP_FLASH_WRITE     0x00000050              //
#define     ISP_FLASH_PROGRAM   0x00000050              //
#define     ISP_FLASH_ERASE     0x00000060              //
#define     OB_FLASH_WRITE      0x00000050              //
#define     OB_FLASH_PROGRAM    0x00000050              //
#define     OB_FLASH_ERASE      0x00000060              //
///@}


/**
 *          Access Block
 *          Erase / Program
 */ 
///@{ 
#define     AP_WRITE            MEM_CR1_AP_WEN_mask_w       //
#define     IAP_WRITE           MEM_CR1_IAP_WEN_mask_w      //
#define     IAP_EXEC            MEM_CR1_IAP_EXEC_mask_w     //
#define     ISP_READ            MEM_CR1_ISP_REN_mask_w      //
#define     ISP_WRITE           MEM_CR1_ISP_WEN_mask_w      //
#define     ISPD_READ           MEM_CR1_ISPD_REN_mask_w     //
#define     ISPD_WRITE          MEM_CR1_ISPD_WEN_mask_w     //  
#define     OB_WRITE            MEM_CR1_OB_WEN_mask_w       // 
///@}



#define __HAL_MEM_GET_FLAG(__FLAG__)            ((((MEM->STA.W) & (__FLAG__)) == (__FLAG__)) ? SET : CLR)
#define __HAL_MEM_GET_ALL_FLAG()                MEM->STA.W
#define __HAL_MEM_CLEAR_FLAG(__FLAG__)          (MEM->STA.W = (__FLAG__))

#define __HAL_MEM_UNPROTECT()                   MEM->KEY.H[0] = 0xA217
#define __HAL_MEM_PROTECT()                     MEM->KEY.H[0] = 0x0000
#define __HAL_MEM_GET_PROTECT_STATUS()          MEM->KEY.H[0]

#define __HAL_MEM_ENABLE()                      MEM->CR0.W |= MEM_CR0_EN_mask_w
#define __HAL_MEM_DISABLE()                     MEM->CR0.W &= ~MEM_CR0_EN_mask_w

#define __HAL_MEM_BOOT_SELECT(__SELECT__)       MEM->CR0.W = (MEM->CR0.W & (~MEM_CR0_BOOT_MS_mask_w)) | (__SELECT__)
#define __HAL_MEM_GET_BOOT_SELECT()             (MEM->CR0.W & MEM_CR0_BOOT_MS_mask_w)

#define __HAL_MEM_FWAIT(__WAIT__)               MEM->CR0.W = (MEM->CR0.W & (~MEM_CR0_FWAIT_mask_w)) | (__WAIT__)
#define __HAL_MEM_GET_FWAIT_STATUS()            (MEM->CR0.W & MEM_CR0_FWAIT_mask_w)

#define __HAL_MEM_ENABLE_HIGH_SPEED()           MEM->CR0.W |= MEM_CR0_HSP_EN_mask_w
#define __HAL_MEM_DISABLE_HIGH_SPEED()          MEM->CR0.W &= (~MEM_CR0_HSP_EN_mask_w)

#define __HAL_MEM_ENABLE_HOLD()                 MEM->CR0.W |= MEM_CR0_HOLD_mask_w
#define __HAL_MEM_DISABLE_HOLD()                MEM->CR0.W &= ~MEM_CR0_HOLD_mask_w

#define __HAL_MEM_IAP_SIZE_LOCK()               MEM->CR0.W &= ~MEM_CR0_IAP_AEN_mask_w
#define __HAL_MEM_GET_IAP_SIZE_LOCK_STATUS()    (((MEM->CR0.W & MEM_CR0_IAP_AEN_mask_w) != 0) ? SET : CLR)

#define __HAL_MEM_IAP_SIZE_CONFIG(__SIZE__)     MEM->IAPSZ.B[1] = (__SIZE__)
#define __HAL_MEM_GET_IAP_SIZE()                MEM->IAPSZ.B[1]

#define __HAL_MEM_ENABLE_IAP_CODE_EXECUTION()   MEM->CR1.W |= MEM_CR1_IAP_EXEC_mask_w
#define __HAL_MEM_DISABLE_IAP_CODE_EXECUTION()  MEM->CR1.W &= ~ MEM_CR1_IAP_EXEC_mask_w
#define __HAL_MEM_GET_IAP_CODE_EXECUTION_STATUS()    (MEM->CR1.W & MEM_CR1_IAP_EXEC_mask_w)

#define __HAL_MEM_ENABLE_RESET(__RESET__)       MEM->INT.W |= (__RESET__)
#define __HAL_MEM_DISABLE_RESET(__RESET__)      MEM->INT.W &= (__RESET__)
#define __HAL_MEM_RESET_GET_ALL_SOURCE()        (MEM->INT.W & MEM_RST_MASK)
#define __HAL_MEM_GET_RESET_SOURCE(__RESET__)   (((MEM->INT.H[1] & (__RESET__)) == (__RESET__)) ? SET : CLR)

#define __HAL_MEM_ENABLE_HARDFAULT()            MEM->CR0.W |= MEM_CR0_HF_EN_mask_w
#define __HAL_MEM_DISABLE_HARDFAULT()           MEM->CR0.W &= ~MEM_CR0_HF_EN_mask_w
#define __HAL_MEM_GET_HARDFAULT_STATUS()        (((MEM->CR0.W & MEM_CR0_HF_EN_mask_w) != 0)? SET : CLR) 

#define __HAL_MEM_ENABLE_IT(__INTERRUPT__)      MEM->INT.W |= (__INTERRUPT__)
#define __HAL_MEM_DISABLE_IT(__INTERRUPT__)     MEM->INT.W &= (~(__INTERRUPT__))
#define __HAL_MEM_GET_IT_SOURCE(__INTERRUPT__)  (((MEM->INT.W & (__INTERRUPT__)) == (__INTERRUPT__)) ? SET : CLR)

#define __HAL_MEM_ENABLE_IT_ALL()               MEM->INT.W |= MEM_IT_IEA
#define __HAL_MEM_DISABLE_IT_ALL()              MEM->INT.W &= (~MEM_IT_IEA)

#define __HAL_MEM_ENABLE_ACCESS(__BLOCK__)      MEM->CR1.W |= (__BLOCK__)
#define __HAL_MEM_DISABLE_ACCESS(__BLOCK__)     MEM->CR1.W &= ~(__BLOCK__)
#define __HAL_MEM_GET_ACCESS_STATE(__BLOCK__)   (((MEM->CR1.W & (__BLOCK__)) == (__BLOCK__)) ? SET : CLR)
#define __HAL_MEM_GET_ALL_ACCESS_STATUS()       MEM->CR1.W

#define __HAL_MEM_ACCESS_MODE(__MODE__)         MEM->CR0.W = (MEM->CR0.W & (~MEM_CR0_MDS_mask_w)) | (__MODE__)

#define __HAL_MEM_SEQUENCE_KEY(__SKEY__)        MEM->SKEY.B[0] = 0x46; MEM->SKEY.B[0] = (__SKEY__)
#define __HAL_MEM_SINGLE_SEQUENCE_KEY()         MEM->SKEY.B[0] = 0x46; MEM->SKEY.B[0] = 0xB9
#define __HAL_MEM_MULTIPLE_SEQUENCE_KEY()       MEM->SKEY.B[0] = 0x46; MEM->SKEY.B[0] = 0xBE
#define __HAL_MEM_SEQUENCE_KEY_LOCK()           MEM->SKEY.B[0] = 0
#define __HAL_MEM_GET_SEQUENCE_KEY_STATUS()     MEM->SKEY.B[0]

#define __HAL_MEM_UNPROTECT2()                  MEM->KEY.H[1] = 0xA217
#define __HAL_MEM_PROTECT2()                    MEM->KEY.H[1] = 0x0000
#define __HAL_MEM_GET_PROTECT2_STATUS()         MEM->KEY.H[1]

#define __HAL_MEM_SEQUENCE_KEY2(__SKEY2__)      MEM->SKEY.H[1] = 0x9867; MEM->SKEY.H[1] = (__SKEY2__)
#define __HAL_MEM_ISP_SEQUENCE_KEY2(__SKEY2__)  MEM->SKEY.H[1] = 0x9867; MEM->SKEY.H[1] = (__SKEY2__)
#define __HAL_MEM_ISP_SINGLE_SEQUENCE_KEY2()    MEM->SKEY.H[1] = 0x9867; MEM->SKEY.H[1] = 0xB955
#define __HAL_MEM_ISP_MULTIPLE_SEQUENCE_KEY2()  MEM->SKEY.H[1] = 0x9867; MEM->SKEY.H[1] = 0xBEAA
#define __HAL_MEM_SEQUENCE_KEY2_LOCK()          MEM->SKEY.H[1] = 0x0000
#define __HAL_MEM_GET_SEQUENCE_KEY2_STATUS()    MEM->SKEY.H[1]

#define __HAL_MEM_OB_SEQUENCE_KEY2(__SKEY2__)   MEM->SKEY.H[1] = 0xC33C; MEM->SKEY.H[1] = (__SKEY2__)
#define __HAL_MEM_OB_SINGLE_SEQUENCE_KEY2()     MEM->SKEY.H[1] = 0xC33C; MEM->SKEY.H[1] = 0xB9AA
#define __HAL_MEM_OB_MULTIPLE_SEQUENCE_KEY2()   MEM->SKEY.H[1] = 0xC33C; MEM->SKEY.H[1] = 0xBE55

#define __HAL_MEM_ENABLE_OB1_WRITE()            MEM->MCR.W |= MEM_MCR_OB1_WEN_mask_w
#define __HAL_MEM_DISABLE_OB1_WRITE()           MEM->MCR.W &= ~MEM_MCR_OB1_WEN_mask_w



void HAL_MEM_Lock_Flash_Access(void);

void HAL_MEM_AP_Single_Program(uint32_t* Addr,uint32_t Value);
void HAL_MEM_AP_Single_Erase(uint32_t* Addr);
void HAL_MEM_AP_Multiple_Program(void);
void HAL_MEM_AP_Multiple_Erase(void);

void HAL_MEM_IAP_Single_Program(uint32_t* Addr,uint32_t Value);
void HAL_MEM_IAP_Single_Erase(uint32_t* Addr);
void HAL_MEM_IAP_Multiple_Program(void);
void HAL_MEM_IAP_Multiple_Erase(void);

void HAL_MEM_ISP_Read(void);
void HAL_MEM_ISP_Single_Program(uint32_t* Addr,uint32_t Value);
void HAL_MEM_ISP_Single_Erase(uint32_t* Addr);
void HAL_MEM_ISP_Multiple_Program(void);
void HAL_MEM_ISP_Multiple_Erase(void);

void HAL_MEM_ISPD_Read(void);
void HAL_MEM_ISPD_Single_Program(uint32_t* Addr,uint32_t Value);
void HAL_MEM_ISPD_Single_Erase(uint32_t* Addr);
void HAL_MEM_ISPD_Multiple_Program(void);
void HAL_MEM_ISPD_Multiple_Erase(void);

void HAL_MEM_AP_Single_Program_Dis(uint32_t* Addr,uint32_t Value);
void HAL_MEM_AP_Single_Erase_Dis(uint32_t* Addr);
void HAL_MEM_AP_Multiple_Program_Dis(void);
void HAL_MEM_AP_Multiple_Erase_Dis(void);

void HAL_MEM_IAP_Single_Program_Dis(uint32_t* Addr,uint32_t Value);
void HAL_MEM_IAP_Single_Erase_Dis(uint32_t* Addr);
void HAL_MEM_IAP_Multiple_Program_Dis(void);
void HAL_MEM_IAP_Multiple_Erase_Dis(void);

void HAL_MEM_ISP_Read_Dis(void);
void HAL_MEM_ISP_Single_Program_Dis(uint32_t* Addr,uint32_t Value);
void HAL_MEM_ISP_Single_Erase_Dis(uint32_t* Addr);
void HAL_MEM_ISP_Multiple_Program_Dis(void);
void HAL_MEM_ISP_Multiple_Erase_Dis(void);

void HAL_MEM_ISPD_Read_Dis(void);
void HAL_MEM_ISPD_Single_Program_Dis(uint32_t* Addr,uint32_t Value);
void HAL_MEM_ISPD_Single_Erase_Dis(uint32_t* Addr);
void HAL_MEM_ISPD_Multiple_Program_Dis(void);
void HAL_MEM_ISPD_Multiple_Erase_Dis(void);

void HAL_MEM_OB_Single_Program(uint32_t* Addr,uint32_t Value);
void HAL_MEM_OB_Single_Erase(uint32_t* Addr);
void HAL_MEM_OB_Multiple_Program(void);
void HAL_MEM_OB_Multiple_Erase(void);

void HAL_MEM_OB_Single_Program_Dis(uint32_t* Addr,uint32_t Value);
void HAL_MEM_OB_Single_Erase_Dis(uint32_t* Addr);
void HAL_MEM_OB_Multiple_Program_Dis(void);
void HAL_MEM_OB_Multiple_Erase_Dis(void);


