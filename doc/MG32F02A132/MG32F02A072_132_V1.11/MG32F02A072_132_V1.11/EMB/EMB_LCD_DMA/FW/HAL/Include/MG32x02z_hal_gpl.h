#ifndef __Mx92G8z_GPL_MID_H
#define __Mx92G8z_GPL_MID_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "Mx92G8z.h"
//#include "Mx92G8z_Common_MID.h"

#define GPL_DMA_MASK       GPL_CR0_DMA_EN_mask_w
#define GPL_DMA_DISABLE    GPL_CR0_DMA_EN_disable_w
#define GPL_DMA_ENABLE     GPL_CR0_DMA_EN_enable_w
#define IS_GPL_DMA(ENABLE) (((ENABLE) == GPL_DMA_MASK) || \
                             (ENABLE) == GPL_DMA_DISABLE) || \
                            ((ENABLE) == GPL_DMA_ENABLE))

     

#define GPL_PARITY_MASK          GPL_CR0_PAR_POL_mask_w
#define GPL_PARITY_EVEN         GPL_CR0_PAR_POL_even_w
#define GPL_PARITY_ODD           GPL_CR0_PAR_POL_odd_w
#define IS_GPL_PARITY(SELECT) (((SELECT) == GPL_PARITY_MASK) || \
                                (SELECT) == GPL_PARITY_EVENT) || \
                               ((SELECT) == GPL_PARITY_ODD))



#define GPL_INVERSE_MASK       GPL_CR0_IN_INV_mask_w
#define GPL_INVERSE_DISABLE    GPL_CR0_IN_INV_disable_w
#define GPL_INVERSE_ENABLE     GPL_CR0_IN_INV_enable_w
#define IS_GPL_INVERSE(INVERSE) (((INVERSE) == GPL_INVERSE_MASK) || \
                                  (INVERSE) == GPL_INVERSE_DISABLE) || \
                                 ((INVERSE) == GPL_INVERSE_ENABLE))



#define GPL_BYTE_ORDER_CHANGE_MASK       GPL_CR0_BEND_EN_mask_w
#define GPL_BYTE_ORDER_CHANGE_DISABLE    GPL_CR0_BEND_EN_disable_w
#define GPL_BYTE_ORDER_CHANGE_ENABLE     GPL_CR0_BEND_EN_enable_w
#define IS_GPL_BYTE_ORDER_CHANGE(BYTE_ORDER) (((BYTE_ORDER) == GPL_BYTE_ORDER_CHANGE_MASK) || \
                                               (BYTE_ORDER) == GPL_BYTE_ORDER_CHANGE_DISABLE) || \
                                              ((BYTE_ORDER) == GPL_BYTE_ORDER_CHANGE_ENABLE))



#define GPL_BEFER_BIT_ORDER_MASK         GPL_CR0_BREV_MDS_mask_w
#define GPL_BEFER_BIT_ORDER_DISABLE      GPL_CR0_BREV_MDS_disable_w
#define GPL_BEFER_BIT_ORDER_8BITS        GPL_CR0_BREV_MDS_8bit_w
#define GPL_BEFER_BIT_ORDER_16BITS       GPL_CR0_BREV_MDS_16bit_w
#define GPL_BEFER_BIT_ORDER_32BITS       GPL_CR0_BREV_MDS_32bit_w
#define IS_GPL_BEFER_BIT_ORDER(SELECT) (((SELECT) == GPL_BEFER_BIT_ORDER_MASK) || \
                                         (SELECT) == GPL_BEFER_BIT_ORDER_DISABLE) || \
                                         (SELECT) == GPL_BEFER_BIT_ORDER_8BITS) || \
                                         (SELECT) == GPL_BEFER_BIT_ORDER_16BITS) || \
                                        ((SELECT) == GPL_BEFER_BIT_ORDER_32BITS))



#define GPL_CRC_MODE_MASK        GPL_CR1_CRC_MDS_mask_w
#define GPL_CRC_MODE_CCITT16     GPL_CR1_CRC_MDS_ccitt16_w  /* 0x1021 */
#define GPL_CRC_MODE_CRC8        GPL_CR1_CRC_MDS_crc8_w     /* 0x07 */
#define GPL_CRC_MODE_CRC16       GPL_CR1_CRC_MDS_crc16_w    /* 0x8005 */
#define GPL_CRC_MODE_CRC32       GPL_CR1_CRC_MDS_crc32_w    /* 0x04C11DB7 */
#define IS_GPL_CRC_MODE(SELECT) (((SELECT) == GPL_CRC_MODE_MASK) || \
                                  (SELECT) == GPL_CRC_MODE_CCITT16) || \
                                  (SELECT) == GPL_CRC_MODE_CRC8) || \
                                  (SELECT) == GPL_CRC_MODE_CRC16) || \
                                 ((SELECT) == GPL_CRC_MODE_CRC32))



#define GPL_CRC_DATA_SIZE_MASK       GPL_CR1_CRC_DSIZE_mask_w
#define GPL_CRC_DATA_SIZE_8BITS      GPL_CR1_CRC_DSIZE_8bit_w
#define GPL_CRC_DATA_SIZE_16BITS     GPL_CR1_CRC_DSIZE_16bit_w
#define GPL_CRC_DATA_SIZE_32BITS     GPL_CR1_CRC_DSIZE_32bit_w
#define IS_GPL_CRC_DATA_SIZE(SELECT) (((SELECT) == GPL_CRC_DATA_SIZE_MASK) || \
                                       (SELECT) == GPL_CRC_DATA_SIZE_8BITS) || \
                                       (SELECT) == GPL_CRC_DATA_SIZE_16BITS) || \
                                      ((SELECT) == GPL_CRC_DATA_SIZE_32BITS))



#define GPL_CRC_MASK       GPL_CR1_CRC_EN_mask_w
#define GPL_CRC_DISABLE    GPL_CR1_CRC_EN_disable_w
#define GPL_CRC_ENABLE     GPL_CR1_CRC_EN_enable_w
#define IS_GPL_CRC(CRC) (((CRC) == GPL_CRC_MASK) || \
                          (CRC) == GPL_CRC_DISABLE) || \
                         ((CRC) == GPL_CRC_ENABLE))



#define GPL_AFTER_BIT_ORDER_MASK         GPL_CR1_CRC_BREV_mask_w
#define GPL_AFTER_BIT_ORDER_DISABLE      GPL_CR1_CRC_BREV_disable_w
#define GPL_AFTER_BIT_ORDER_8BITS        GPL_CR1_CRC_BREV_8bit_w
#define GPL_AFTER_BIT_ORDER_16BITS       GPL_CR1_CRC_BREV_16bit_w
#define GPL_AFTER_BIT_ORDER_32BITS       GPL_CR1_CRC_BREV_32bit_w
#define IS_GPL_AFTER_BIT_ORDER(SELECT) (((SELECT) == GPL_AFTER_BIT_ORDER_MASK) || \
                                         (SELECT) == GPL_AFTER_BIT_ORDER_DISABLE) || \
                                         (SELECT) == GPL_AFTER_BIT_ORDER_8BITS) || \
                                         (SELECT) == GPL_AFTER_BIT_ORDER_16BITS) || \
                                        ((SELECT) == GPL_AFTER_BIT_ORDER_32BITS))



#define __HAL_GPL_GetParityCheck8                           GPL->STA.B[1]
#define __HAL_GPL_GetParityCheck16                          GPL->STA.B[2]
#define __HAL_GPL_GetParityCheck32                          GPL->STA.B[3]

#define __HAL_GPL_DMA_Cmd(__CMD__)                          GPL->CR0.W = (GPL->CR0.W & (~GPL_DMA_MASK)) | (__CMD__)
#define __HAL_GPL_DMA_Enable()                              GPL->CR0.W = (GPL->CR0.W | GPL_DMA_MASK)
#define __HAL_GPL_DMA_Disable()                             GPL->CR0.W = (GPL->CR0.W & (~GPL_DMA_MASK))

#define __HAL_GPL_DataInput(__DATA__)                       GPL->DIN.W = (uint32_t)(__DATA__)

#define __HAL_GPL_ParityCheck_Select(__SELECT__)            GPL->CR0.W = (GPL->CR0.W & (~GPL_PARITY_MASK)) | (__SELECT__)

#define __HAL_GPL_Inverse_Cmd(__INVERSE__)                  GPL->CR0.W = (GPL->CR0.W & (~GPL_INVERSE_MASK)) | (__INVERSE__)
#define __HAL_GPL_Inverse_Enable()                          GPL->CR0.W = (GPL->CR0.W | GPL_INVERSE_MASK)
#define __HAL_GPL_Inverse_Disable()                         GPL->CR0.W = (GPL->CR0.W & (~GPL_INVERSE_MASK))

#define __HAL_GPL_ByteOrderChange_Cmd(__BYTE_ORDER__)       GPL->CR0.W = (GPL->CR0.W & (~GPL_BYTE_ORDER_CHANGE_MASK)) | (__BYTE_ORDER__)
#define __HAL_GPL_ByteOrderChange_Enable()                  GPL->CR0.W = (GPL->CR0.W | GPL_BYTE_ORDER_CHANGE_MASK)
#define __HAL_GPL_ByteOrderChange_Disable()                 GPL->CR0.W = (GPL->CR0.W & (~GPL_BYTE_ORDER_CHANGE_MASK))

#define __HAL_GPL_BeferBitOrderChange_Select(__SELECT__)    GPL->CR0.W = (GPL->CR0.W & (~GPL_BEFER_BIT_ORDER_MASK)) | (__SELECT__)

#define __HAL_GPL_CRC_Mode_Select(__SELECT__)               GPL->CR1.W = (GPL->CR1.W & (~GPL_CRC_MODE_MASK)) | (__SELECT__)
#define __HAL_GPL_CRC_Data_Size_Select(__SELECT__)          GPL->CR1.W = (GPL->CR1.W & (~GPL_CRC_DATA_SIZE_MASK)) | (__SELECT__)

#define __HAL_GPL_CRC_Cmd(__CMD__)                          GPL->CR1.W = (GPL->CR1.W & (~GPL_CRC_MASK)) | (__CMD__)
#define __HAL_GPL_CRC_Enable()                              GPL->CR1.W = (GPL->CR1.W | GPL_CRC_MASK)
#define __HAL_GPL_CRC_Disable()                             GPL->CR1.W = (GPL->CR1.W & (~GPL_CRC_MASK))

#define __HAL_GPL_CRC_Initial(__VALUE__)                    GPL->CRCINIT.W = (__VALUE__)

#define __HAL_GPL_AfterBitOrderChange_Select(__SELECT__)    GPL->CR1.W = (GPL->CR1.W & (~GPL_AFTER_BIT_ORDER_MASK)) | (__SELECT__)

#define __HAL_GPL_DataOutput()                              GPL->DOUT.W

#endif

