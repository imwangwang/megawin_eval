#include <MG32x02z_HAL_DMA.h>



void HAL_DMA_IRQHandler(DMA_HandleTypeDef *DMAx)
{



}



void HAL_DMA_MspInit(DMA_HandleTypeDef *DMAx)
{
    /* USER CODE BEGIN EMB_MspInit 0 */

    /* USER CODE END EMB_MspInit 0 */

    // GPIO Config

    // Clock Enable
    __HAL_UNPROTECT_MODULE(CSC);
    CSC->AHB.MBIT.DMA_EN = 1;
    __HAL_PROTECT_MODULE(CSC);

    // SoftWare Trigger HardWare Reset
//    UnProtectModuleReg(RSTprotect);
//    ProtectModuleReg(RSTprotect);

    /* Peripheral interrupt init*/
    // NVIC_ClearPendingIRQ(DMA_IRQn);
    // NVIC_SetPriority(DMA_IRQn, 1);
    // NVIC_EnableIRQ(DMA_IRQn);
        
    /* USER CODE BEGIN EMB_MspInit 1 */

    /* USER CODE END EMB_MspInit 1 */
}



void HAL_DMA_MspDeInit(DMA_HandleTypeDef *DMAx)
{
    /* USER CODE BEGIN EMB_MspInit 0 */

    /* USER CODE END EMB_MspInit 0 */

    // GPIO Config

    // Clock Enable
    __HAL_UNPROTECT_MODULE(CSC);
    CSC->AHB.MBIT.DMA_EN = 0;
    __HAL_PROTECT_MODULE(CSC);

    // SoftWare Trigger HardWare Reset
//    UnProtectModuleReg(RSTprotect);
//    ProtectModuleReg(RSTprotect);

    /* Peripheral interrupt init*/
    // NVIC_ClearPendingIRQ(DMA_IRQn);
    // NVIC_SetPriority(DMA_IRQn, 1);
    // NVIC_EnableIRQ(DMA_IRQn);
        
    /* USER CODE BEGIN EMB_MspInit 1 */

    /* USER CODE END EMB_MspInit 1 */
}



HAL_StatusTypeDef HAL_DMA_Init(DMA_HandleTypeDef *DMAx)
{
    /* Check the EMB handle allocation */
    if(DMAx == NULL)
    {
        return HAL_ERROR;
    }

    if((DMAx->State.W & DMA_STATE_INITREADY) == 0)
    {
        __HAL_UNLOCK(DMAx);

        /* Init the low level hardware : GPIO, CLOCK, CORTEX...etc */
        HAL_DMA_MspInit(DMAx);
    }

    __HAL_DMA_DISABLE(DMAx);
    __HAL_DMA_DISABLE_IT(DMAx, (DMA_IT_IEA | DMA_IT_GPL_CEIE));
    __HAL_DMA_CLEAR_FLAG(DMAx, DMA_FLAG_ALL);

    DMAx->Instance->CR0.W = DMAx->Init.OperationMode.W;

    __HAL_DMA_ENABLE(DMAx);

    DMAx->State.Bit.InitReady = 1;
    DMAx->Event = DMAx->State.W;
    DMAx->Control.W = 0;

    return HAL_OK;
}



HAL_StatusTypeDef HAL_DMA_DeInit(DMA_HandleTypeDef *DMAx)
{
    /* Check the EMB handle allocation */
    if(DMAx == NULL)
    {
        return HAL_ERROR;
    }

    /* Check the EMB handle allocation */
//    if(DMAx->State != 0)
//    {
//        return HAL_ERROR;
//    }

    __HAL_DMA_DISABLE(DMAx);

    __HAL_DMA_DISABLE_IT(DMAx, (DMA_IT_IEA | DMA_IT_GPL_CEIE));
    __HAL_DMA_CLEAR_FLAG(DMAx, (0xFFFFFFFF));

    if((DMAx->State.W & DMA_STATE_INITREADY) == 0)
    {
        /* Init the low level hardware : GPIO, CLOCK, CORTEX...etc */
        HAL_DMA_MspInit(DMAx);
        DMAx->State.Bit.InitReady = 1;
        DMAx->Event = 0;
    }

    DMAx->Instance->CR0.W = DMAx->Init.OperationMode.W;

    __HAL_DMA_ENABLE(DMAx);

    DMAx->Event = DMA_EVENT_INITREADY;

    return HAL_OK;
}



HAL_StatusTypeDef HAL_DMA_GPL_ChannelSelect(DMA_HandleTypeDef *DMAx, uint32_t ChannelSelect)
{
    __HAL_DMA_GPL_CHANNEL_SELECT(DMAx, ChannelSelect);
    return HAL_OK;
}



void HAL_DMA_CHx_IRQHandler(DMA_CHx_HandleTypeDef *DMA_CHx)
{



}



void HAL_DMA_CHx_MspInit(DMA_CHx_HandleTypeDef *DMA_CHx)
{
    /* USER CODE BEGIN EMB_MspInit 0 */

    /* USER CODE END EMB_MspInit 0 */

    // GPIO Config

    // Clock Enable
    __HAL_UNPROTECT_MODULE(CSC);
    CSC->AHB.MBIT.DMA_EN = 1;
    __HAL_PROTECT_MODULE(CSC);

    // SoftWare Trigger HardWare Reset
//    UnProtectModuleReg(RSTprotect);
//    ProtectModuleReg(RSTprotect);

    /* Peripheral interrupt init*/
    // NVIC_ClearPendingIRQ(DMA_IRQn);
    // NVIC_SetPriority(DMA_IRQn, 1);
    // NVIC_EnableIRQ(DMA_IRQn);
        
    /* USER CODE BEGIN EMB_MspInit 1 */

    /* USER CODE END EMB_MspInit 1 */
}



HAL_StatusTypeDef HAL_DMA_CHx_Init(DMA_CHx_HandleTypeDef *DMA_CHx)
{
    /* Check the EMB handle allocation */
    if(DMA_CHx == NULL)
    {
        return HAL_ERROR;
    }

    __HAL_DMA_CHx_DISABLE(DMA_CHx);

    __HAL_DMA_CHx_DISABLE(DMA_CHx);
    __HAL_DMA_CHx_DISABLE_IT(DMA_CHx, (DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE));
    __HAL_DMA_CHx_CLEAR_FLAG(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    if((DMA_CHx->State.W & DMA_STATE_INITREADY) == 0)
    {
        /* Init the low level hardware : GPIO, CLOCK, CORTEX...etc */
        //HAL_DMA_MspInit(DMAx);
        DMA_CHx->State.Bit.InitReady = 1;
        DMA_CHx->Event = 0;
    }

    if((DMA_CHx->State.W & DMA_STATE_INITREADY) == 0)
    {
        /* Init the low level hardware : GPIO, CLOCK, CORTEX...etc */
        HAL_DMA_CHx_MspInit(DMA_CHx);
        DMA_CHx->State.Bit.InitReady = 1;
        DMA_CHx->Event = 0;
    }

//    DMA_CHx->Instance->CHxA.W = DMA_CHx->Init.OperationMode.W[0] & ;
//    DMA_CHx->Instance->CHxB.W = DMA_CHx->Init.OperationMode.W[1] & ;

    __HAL_DMA_CHx_ENABLE(DMA_CHx);

    DMA_CHx->Event = DMA_EVENT_INITREADY;

    return HAL_OK;
}




/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_Initial(void)
{
    __DMA_Disable(DMA);

    __DMA_GPL_Channel_Select(DMA, DMA_GPL_DISABLE);
    // __DMA_GPL_Channel_Select(DMA, DMA_GPL_CHANNEL_0);
    // __DMA_GPL_Channel_Select(DMA, DMA_GPL_CHANNEL_1);
    // __DMA_GPL_Channel_Select(DMA, DMA_GPL_CHANNEL_2);

    __DMA_Channel_Priority(DMA, DMA_PRIORITY_ROUND);
    // __DMA_Channel_Priority(DMA, DMA_PRIORITY_LEVEL);

    __DMA_Enable(DMA);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

//    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

//    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    // __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));
    __DMA_CHx_Clear_Flag(DMA_CHx, DMA_CHx_FLAG_TCF);

//    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

//    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_PL0(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_PL1(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_1);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_PL2(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_2);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_PL3(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_3);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

//    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

//    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_TXDET);
    // __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_DET_ADDR(DMA_CHx, 0x20003C00);
    __DMA_Channel_DINC_Disable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_ChannelNonDisable(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    //__DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

//    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

//    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_TXDET);
    // __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_DET_ADDR(DMA_CHx, 0x20003C00);
    __DMA_Channel_DINC_Disable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    //__DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    // __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));
    __DMA_CHx_Clear_Flag(DMA_CHx, DMA_CHx_FLAG_TCF);

//    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

//    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_TXDET);
    // __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_DET_ADDR(DMA_CHx, 0x20003C00);
    __DMA_Channel_DINC_Disable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_PL0(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));
    
    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_TXDET);
    // __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_DET_ADDR(DMA_CHx, 0x20003C00);
    __DMA_Channel_DINC_Disable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_PL1(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));
    
    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_1);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_TXDET);
    // __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_DET_ADDR(DMA_CHx, 0x20003C00);
    __DMA_Channel_DINC_Disable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_PL2(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));
    
    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_2);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_TXDET);
    // __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_DET_ADDR(DMA_CHx, 0x20003C00);
    __DMA_Channel_DINC_Disable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_PL3(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));
    
    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_3);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_CHx_SRC_Memory);
    __DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    __DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_TXDET);
    // __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_DET_ADDR(DMA_CHx, 0x20003C00);
    __DMA_Channel_DINC_Disable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

//    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));
    __DMA_CHx_Clear_Flag(DMA_CHx, DMA_CHx_FLAG_TCF);
//    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_4);

//    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);
    
    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);
    
    __DMA_Channel_LOOP_Disable(DMA_CHx);
    
    __DMA_Channel_HOLD_Disable(DMA_CHx);
    
    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);
    
    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_RXSRC);
    // __DMA_Channel_SRC_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_SRC_ADDR(DMA_CHx, 0x20003800);
    __DMA_Channel_SINC_Disable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_DET_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_PL0(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_4);
    
    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);
    
    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);
    
    __DMA_Channel_LOOP_Disable(DMA_CHx);
    
    __DMA_Channel_HOLD_Disable(DMA_CHx);
    
    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);
    
    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_RXSRC);
    // __DMA_Channel_SRC_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_SRC_ADDR(DMA_CHx, 0x20003800);
    __DMA_Channel_SINC_Disable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_DET_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_PL1(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_4);
    
    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_1);
    
    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);
    
    __DMA_Channel_LOOP_Disable(DMA_CHx);
    
    __DMA_Channel_HOLD_Disable(DMA_CHx);
    
    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);
    
    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_RXSRC);
    // __DMA_Channel_SRC_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_SRC_ADDR(DMA_CHx, 0x20003800);
    __DMA_Channel_SINC_Disable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_DET_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_PL2(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_4);
    
    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_2);
    
    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);
    
    __DMA_Channel_LOOP_Disable(DMA_CHx);
    
    __DMA_Channel_HOLD_Disable(DMA_CHx);
    
    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);
    
    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_RXSRC);
    // __DMA_Channel_SRC_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_SRC_ADDR(DMA_CHx, 0x20003800);
    __DMA_Channel_SINC_Disable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_DET_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DMA_TX_Test(void)
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_PL3(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_4);
    
    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_3);
    
    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);
    
    __DMA_Channel_LOOP_Disable(DMA_CHx);
    
    __DMA_Channel_HOLD_Disable(DMA_CHx);
    
    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);
    
    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SRC(DMA_CHx, DMA_RXSRC);
    // __DMA_Channel_SRC_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_SRC_ADDR(DMA_CHx, 0x20003800);
    __DMA_Channel_SINC_Disable(DMA_CHx);

    __DMA_Channel_DET(DMA_CHx, DMA_CHx_DET_Memory);
    __DMA_Channel_DET_ADDR(DMA_CHx, DET_ADDR);
    __DMA_Channel_DINC_Enable(DMA_CHx);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



///**
// *******************************************************************************
// * @brief       ?????(Template)
// *
// *              ????,??????
// * @details     ?????
// * @param[out]	var1            : var1???
// * @param[in]   var2            : var2???
// *  @arg\b      enum1/value1    : enum1/value1???(bold font)
// *  @arg\b      enum2/value2    : enum2/value2???(bold font)
// *      \n      - Enumerator-1  : Enumerator-1 ???.
// *      \n      - Enumerator-2  : Enumerator-2 ???.
// * @param[in,out]  	var3    		: var3???
// *  @arg        enum1/value1    : enum1/value1???(normal font)
// *  @arg        enum2/value2    : enum2/value2???(normal font)
// *  @arg        enum3/value3    : enum3/value3???(normal font)
// * @par         Refer
// *              ???????
// *      \n      global_var1     : global var1 ???
// *      \n      global_var2     : global var2 ???
// *      \n      global_var3     : global var3 ???
// * @return      No/??0/??1/... ???
// * @return      0=??
// * @return      1=?? 
// * @see         global_var1 : global var1 ???
// * @see         global_var2 : global var2 ???
// * @see         global_var3
// * @exception   ????????,???none
// * @note        
// * @par         Example
// * @code
//                [Example Code here start ...]    
//                ...
//                [Example Code here end   ...]                     
// * @endcode
// * @par         Modify
// *              void DMA_TX_Test(void)
// * @bug          
// *******************************************************************************
// */
//HAL_StatusTypeDef HAL_DMA_CH0_Memory_To_Memory(uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH)  
//{
//    __DMA_Channel_0_Request_Disable(DMA);
//    __DMA_Channel_0_Disable(DMA);
//    __DMA_Channel_0_Disable_IT(DMA, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

//    __DMA_CLEAR_FLAG(DMA, DMA_FLAG_CH0_ERRF | DMA_FLAG_CH0_THF | DMA_FLAG_CH0_TCF);

//    __DMA_Channel_0_Burst_Size(DMA, DMA_CHx_BSIZE_4);

//    __DMA_Channel_0_Priority_Level(DMA, DMA_CHx_PLS_LEVEL_0);

//    __DMA_Channel_0_ADSEL(DMA, DMA_CHx_ADSEL_NORMAL);

//    __DMA_Channel_0_LOOP_Disable(DMA);

//    __DMA_Channel_0_HOLD_Disable(DMA);

//    __DMA_Channel_0_Ext_REQ_Mode(DMA, DMA_CHx_XMDS_DISABLE);

//    __DMA_Channel_0_Ext_PIN(DMA, DMA_CHx_XPIN_TRG0);

//    __DMA_Channel_0_SRC(DMA, DMA_CHx_SRC_Memory);
//    __DMA_Channel_0_SRC_ADDR(DMA, SRC_ADDR);
//    __DMA_Channel_0_SINC_ENABLE(DMA);

//    __DMA_Channel_0_DET(DMA, DMA_CHx_SRC_Memory);
//    __DMA_Channel_0_DET_ADDR(DMA, DET_ADDR);
//    __DMA_Channel_0_DINC_ENABLE(DMA);

//    __DMA_Channel_0_Data_Lenth(DMA, DATA_LANTH);

//    __DMA_Channel_0_Enable_IT(DMA, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

//    __DMA_Channel_0_Enable(DMA);
//    __DMA_Channel_0_Request_Enable(DMA);

//    return HAL_OK;
//}



///**
// *******************************************************************************
// * @brief       ?????(Template)
// *
// *              ????,??????
// * @details     ?????
// * @param[out]	var1            : var1???
// * @param[in]   var2            : var2???
// *  @arg\b      enum1/value1    : enum1/value1???(bold font)
// *  @arg\b      enum2/value2    : enum2/value2???(bold font)
// *      \n      - Enumerator-1  : Enumerator-1 ???.
// *      \n      - Enumerator-2  : Enumerator-2 ???.
// * @param[in,out]  	var3    		: var3???
// *  @arg        enum1/value1    : enum1/value1???(normal font)
// *  @arg        enum2/value2    : enum2/value2???(normal font)
// *  @arg        enum3/value3    : enum3/value3???(normal font)
// * @par         Refer
// *              ???????
// *      \n      global_var1     : global var1 ???
// *      \n      global_var2     : global var2 ???
// *      \n      global_var3     : global var3 ???
// * @return      No/??0/??1/... ???
// * @return      0=??
// * @return      1=?? 
// * @see         global_var1 : global var1 ???
// * @see         global_var2 : global var2 ???
// * @see         global_var3
// * @exception   ????????,???none
// * @note        
// * @par         Example
// * @code
//                [Example Code here start ...]    
//                ...
//                [Example Code here end   ...]                     
// * @endcode
// * @par         Modify
// *              void DMA_TX_Test(void)
// * @bug          
// *******************************************************************************
// */
//HAL_StatusTypeDef HAL_DMA_CH1_TX_Memory_To_Periphery_PL0(uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH)  
//{
//    __DMA_Channel_1_Request_Disable(DMA);
//    __DMA_Channel_1_Disable(DMA);
//    __DMA_Channel_1_Disable_IT(DMA, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

//    __DMA_CLEAR_FLAG(DMA, DMA_FLAG_CH1_ERRF | DMA_FLAG_CH1_THF | DMA_FLAG_CH1_TCF);

//    __DMA_Channel_1_Burst_Size(DMA, DMA_CHx_BSIZE_1);

//    __DMA_Channel_1_Priority_Level(DMA, DMA_CHx_PLS_LEVEL_1);

//    __DMA_Channel_1_ADSEL(DMA, DMA_CHx_ADSEL_NORMAL);

//    __DMA_Channel_1_LOOP_Disable(DMA);

//    __DMA_Channel_1_HOLD_Disable(DMA);

//    __DMA_Channel_1_Ext_REQ_Mode(DMA, DMA_CHx_XMDS_DISABLE);

//    __DMA_Channel_1_Ext_PIN(DMA, DMA_CHx_XPIN_TRG0);

//    __DMA_Channel_1_SRC(DMA, DMA_CHx_SRC_Memory);
//    __DMA_Channel_1_SRC_ADDR(DMA, SRC_ADDR);
//    __DMA_Channel_1_SINC_ENABLE(DMA);

//    __DMA_Channel_1_DET(DMA, DMA_TXDET);
//    // __DMA_Channel_0_DET_ADDR(DMA, 0x20003C00);
//    __DMA_Channel_1_DINC_DISABLE(DMA);

//    __DMA_Channel_1_Data_Lenth(DMA, DATA_LANTH);

//    __DMA_Channel_1_Enable_IT(DMA, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

//    __DMA_Channel_1_Enable(DMA);
//    __DMA_Channel_1_Request_Enable(DMA);

//    return HAL_OK;
//}



///**
// *******************************************************************************
// * @brief       ?????(Template)
// *
// *              ????,??????
// * @details     ?????
// * @param[out]	var1            : var1???
// * @param[in]   var2            : var2???
// *  @arg\b      enum1/value1    : enum1/value1???(bold font)
// *  @arg\b      enum2/value2    : enum2/value2???(bold font)
// *      \n      - Enumerator-1  : Enumerator-1 ???.
// *      \n      - Enumerator-2  : Enumerator-2 ???.
// * @param[in,out]  	var3    		: var3???
// *  @arg        enum1/value1    : enum1/value1???(normal font)
// *  @arg        enum2/value2    : enum2/value2???(normal font)
// *  @arg        enum3/value3    : enum3/value3???(normal font)
// * @par         Refer
// *              ???????
// *      \n      global_var1     : global var1 ???
// *      \n      global_var2     : global var2 ???
// *      \n      global_var3     : global var3 ???
// * @return      No/??0/??1/... ???
// * @return      0=??
// * @return      1=?? 
// * @see         global_var1 : global var1 ???
// * @see         global_var2 : global var2 ???
// * @see         global_var3
// * @exception   ????????,???none
// * @note        
// * @par         Example
// * @code
//                [Example Code here start ...]    
//                ...
//                [Example Code here end   ...]                     
// * @endcode
// * @par         Modify
// *              void DMA_TX_Test(void)
// * @bug          
// *******************************************************************************
// */
//HAL_StatusTypeDef HAL_DMA_CH2_RX_Periphery_To_Memory_PL0(uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH)  
//{
//    __DMA_Channel_2_Request_Disable(DMA);
//    __DMA_Channel_2_Disable(DMA);
//    __DMA_Channel_2_Disable_IT(DMA, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

//    __DMA_CLEAR_FLAG(DMA, DMA_FLAG_CH2_ERRF | DMA_FLAG_CH2_THF | DMA_FLAG_CH2_TCF);

//    __DMA_Channel_2_Burst_Size(DMA, DMA_CHx_BSIZE_4);
//    
//    __DMA_Channel_2_Priority_Level(DMA, DMA_CHx_PLS_LEVEL_2);
//    
//    __DMA_Channel_2_ADSEL(DMA, DMA_CHx_ADSEL_NORMAL);
//    
//    __DMA_Channel_2_LOOP_Disable(DMA);
//    
//    __DMA_Channel_2_HOLD_Disable(DMA);
//    
//    __DMA_Channel_2_Ext_REQ_Mode(DMA, DMA_CHx_XMDS_DISABLE);
//    
//    __DMA_Channel_2_Ext_PIN(DMA, DMA_CHx_XPIN_TRG0);

//    __DMA_Channel_2_SRC(DMA, DMA_RXSRC);
//    // __DMA_Channel_1_SRC_ADDR(DMA, 0x20003800);
//    __DMA_Channel_2_SINC_Disable(DMA);

//    __DMA_Channel_2_DET(DMA, DMA_CHx_DET_Memory);
//    __DMA_Channel_2_DET_ADDR(DMA, DET_ADDR);
//    __DMA_Channel_2_DINC_Enable(DMA);

//    __DMA_Channel_2_Data_Lenth(DMA, DATA_LANTH);

//    __DMA_Channel_2_Enable_IT(DMA, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

//    __DMA_Channel_2_Enable(DMA);
//    __DMA_Channel_2_Request_Enable(DMA);

//    return HAL_OK;
//}



HAL_StatusTypeDef HAL_DMA_Periphery_To_Periphery(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint8_t DMA_TXDET, uint16_t DATA_LANTH)
{
    __DMA_Channel_Request_Disable(DMA_CHx);
    __DMA_Channel_Disable(DMA_CHx);
    __DMA_Channel_Disable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_CHx_Clear_Flag(DMA_CHx, (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF));

//    __DMA_Channel_Burst_Size(DMA_CHx, DMA_CHx_BSIZE_1);

//    __DMA_Channel_Priority_Level(DMA_CHx, DMA_CHx_PLS_LEVEL_0);

    __DMA_Channel_ADSEL(DMA_CHx, DMA_CHx_ADSEL_NORMAL);

    __DMA_Channel_LOOP_Disable(DMA_CHx);

    __DMA_Channel_HOLD_Disable(DMA_CHx);

    __DMA_Channel_Ext_REQ_Mode(DMA_CHx, DMA_CHx_XMDS_DISABLE);

    __DMA_Channel_Ext_PIN(DMA_CHx, DMA_CHx_XPIN_TRG0);

    __DMA_Channel_SINC_Disable(DMA_CHx);
    __DMA_Channel_SRC(DMA_CHx, DMA_RXSRC);
    //__DMA_Channel_SRC_ADDR(DMA_CHx, SRC_ADDR);
    //__DMA_Channel_SINC_Enable(DMA_CHx);

    __DMA_Channel_DINC_Disable(DMA_CHx);
    __DMA_Channel_DET(DMA_CHx, DMA_TXDET);
    // __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)
    // __DMA_Channel_DET_ADDR(DMA_CHx, 0x20003C00);

    __DMA_Channel_Data_Lenth(DMA_CHx, DATA_LANTH);

    __DMA_Channel_Enable_IT(DMA_CHx, DMA_CHx_IT_EIE | DMA_CHx_IT_HIE | DMA_CHx_IT_CIE);

    __DMA_Channel_Enable(DMA_CHx);
    __DMA_Channel_Request_Enable(DMA_CHx);

    return HAL_OK;
}



















