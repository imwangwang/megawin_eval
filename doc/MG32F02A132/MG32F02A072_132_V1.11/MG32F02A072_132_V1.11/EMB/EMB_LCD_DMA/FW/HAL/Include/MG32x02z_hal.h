/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MG32x02z_HAL_H
#define __MG32x02z_HAL_H

#ifdef __cplusplus
 extern "C"{
#endif

/* Includes ------------------------------------------------------------------*/
#include "MG32x02z_hal_common.h"
//#include "Legacy/MG32x02z_hal_LEGACY.h"
//#include "MG32x02z_hal_mem.h"
//#include "MG32x02z_hal_emb.h"
//#include "MG32x02z_hal_swemb.h"
//#include "MG32x02z_hal_gpl.h"
#include "MG32x02z_hal_dma.h"
#include "MG32x02z_hal_i2c.h"


/* Exported types ------------------------------------------------------------*/




#ifdef __cplusplus
}
#endif

#endif /* ___MG32x02z_HAL_H */

/************************ (C) COPYRIGHT Megawin *****END OF FILE****/


