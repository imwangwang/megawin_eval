#ifndef __MG32x02z_HAL_I2C_H
#define __MG32x02z_HAL_I2C_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "MG32x02z_HAL_Common.h"

#define BM_MR_MT_ST_DataByte            4   /* =0:4Byte, =1:1Byte, =2:2Byte, =3:3Byte, >=4:4Byte */

/** @addtogroup STM32F0xx_HAL_Driver
  * @{
  */

/** @addtogroup I2C
  * @{
  */ 

/* Exported types ------------------------------------------------------------*/ 
/** @defgroup I2C_Exported_Types I2C Exported Types
  * @{
  */

/** @defgroup I2C_Timing Configuration_Structure_definition I2C Timing Configuration Structure definition
  * @brief  I2C Timing Configuration Structure definition    
  * @{
  */
typedef union{
    __IO uint32_t W;
    __IO uint16_t H[2];
    __IO uint8_t B[4];
    struct{
        __IO uint32_t                :1;     //[0] 
        __IO uint32_t                :1;     //[1] 
        __IO uint32_t  CK_SEL        :2;     //[3..2] I2C internal clock CK_I2C source select.
                                        //0x0 = PROC : CK_I2C_PR process clock from CSC
                                        //0x1 = Reserved
                                        //0x2 = TM00_TRGO
                                        //0x3 = Reserved
        __IO uint32_t  CK_DIV        :3;     //[6..4] I2C internal clock CK_I2C_INT input divider
        __IO uint32_t                :1;     //[7] 
        __IO uint32_t  CK_PSC        :3;     //[10..8] I2C internal clock CK_I2C_INT prescaler
        __IO uint32_t                :1;     //[11] 
        __IO uint32_t  TMO_CKS       :1;     //[12] I2C timeout clock source select.
                                        //0 = CK_UT
                                        //1 = PSC64 (CK_I2C_PSC divided by 64)
        __IO uint32_t                :3;     //[15..13] 
        __IO uint32_t  LPT           :5;     //[20..16]  Low Period
        __IO uint32_t                :3;     //[23..21] 
        __IO uint32_t  HPT           :5;     //[28..24]  High Period
        __IO uint32_t                :3;     //[31..29]
    }Bits;
}I2C_TimingTypeDef;

/**
  * @}
  */

/** @defgroup I2C_Configuration_Structure_definition I2C Configuration Structure definition
  * @brief  I2C Configuration Structure definition    
  * @{
  */
typedef struct
{
    __IO I2C_TimingTypeDef Timing;           /*!< Specifies the I2C_TIMINGR_register value.
                                          This parameter calculated by referring to I2C initialization 
                                          section in Reference manual */

    __IO uint32_t OperationMode;             /*!< operation mode select. */

    __IO uint32_t BufferMode;                /*!< data buffer enable bit. */

    __IO uint32_t PreDrive;                  /*!< pre-drive time select for both SCL and SDA by CK_I2Cx clock time.*/

    __IO uint32_t SDAFBDH;                   /*!< SDA first bit drive high enable when data transmitted. 
                                          This bit is no effect and disabled when I2Cx_PDRV_SEL=0. */

    __IO uint32_t GeneralCallMode;           /*!< Specifies if general call mode is selected.
                                          This parameter can be a value of @ref I2C_general_call_addressing_mode. */

    __IO uint32_t DualAddressMode;           /*!< Specifies if dual addressing mode is selected.
                                          This parameter can be a value of @ref I2C_dual_addressing_mode */

    __IO uint32_t StretchMode;               /*!< Specifies if nostretch mode is selected.
                                          This parameter can be a value of @ref I2C_nostretch_mode */

    __IO uint16_t OwnAddress1;               /*!< Specifies the first device own address.
                                          This parameter can be a 7-bit or 10-bit address. */

    __IO uint16_t OwnAddress2;               /*!< Specifies the second device own address if dual addressing mode is selected
                                          This parameter can be a 7-bit address. */
}I2C_InitTypeDef;

/**
  * @}
  */

/** @defgroup HAL_Event_structure_definition HAL state structure definition
  * @brief  HAL Event structure definition  
  * @{
  */
#define I2C_EVENT_RESET                  0UL        /*!< I2C not yet initialized or disabled */
#define I2C_EVENT_TRANSFER_DONE         (1UL << 0)  /*!< I2C Master/Slave Transmit/Receive finished */
#define I2C_EVENT_TRANSFER_INCOMPLETE   (1UL << 1)  /*!< I2C Master/Slave Transmit/Receive incomplete transfer */
#define I2C_EVENT_GENERAL_CALL          (1UL << 2)  /*!< I2C General Call indication (cleared on start of next Slave operation) */
#define I2C_EVENT_ARBITRATION_LOST      (1UL << 3)  /*!< I2C Master lost arbitration (cleared on start of next Master operation) */
#define I2C_EVENT_BUS_ERROR             (1UL << 4)  /*!< I2C Bus error detected (cleared on start of next Master/Slave operation) */
#define I2C_EVENT_TIMEOUT               (1UL << 5)  /*!< I2C Timeout state */
#define I2C_EVENT_ADDR_NACK             (1UL << 6)  /*!< I2C Address not acknowledged from Slave */
#define I2C_EVENT_DATA_NACK             (1UL << 7)  /*!< I2C Data not acknowledged from Transmit */
#define I2C_EVENT_RESTART               (1UL << 8)  /*!< I2C Received Bus ReStart */
#define I2C_EVENT_STOP                  (1UL << 9)  /*!< I2C Received Bus Stop */
#define I2C_EVENT_BRXOVR                (1UL << 10) /*!< I2C Receive Buffer Over */
#define I2C_EVENT_BTXOVR                (1UL << 11) /*!< I2C Transmit Buffer Over */
#define I2C_EVENT_SRXOVR                (1UL << 12) /*!< I2C Slave Stretching Receive Over */
#define I2C_EVENT_STXOVR                (1UL << 13) /*!< I2C Slave Stretching Transmit Over */
//#define I2C_EVENT_NONE14                (1UL << 14) /*!<  */
//#define I2C_EVENT_NONE15                (1UL << 15) /*!<  */
//#define I2C_EVENT_NONE16                (1UL << 16) /*!<  */
//#define I2C_EVENT_NONE17                (1UL << 17) /*!<  */
//#define I2C_EVENT_NONE18                (1UL << 18) /*!<  */
//#define I2C_EVENT_NONE19                (1UL << 19) /*!<  */
//#define I2C_EVENT_NONE20                (1UL << 20) /*!<  */
//#define I2C_EVENT_SLAVE_RECEIVE         (1UL << 21) /*!< I2C Slave Receive operation requested */
//#define I2C_EVENT_SLAVE_TRANSMIT        (1UL << 22) /*!< I2C Slave Transmit operation requested */
//#define I2C_EVENT_MASTER_RECEIVE        (1UL << 23) /*!< I2C Master Receive operation requested */
//#define I2C_EVENT_MASTER_TRANSMIT       (1UL << 24) /*!< I2C Master Transmit operation requested */
#define I2C_EVENT_DIRECTION               (1UL << 25) /*!< I2C Transfer Direction operation requested 0=Transmitter, 1=Receiver */
#define I2C_EVENT_DIRECTION_TRAN           0UL        /*!< Direction: 0=Transmitter, 1=Receiver */
#define I2C_EVENT_DIRECTION_REC           (1UL << 25) /*!< Direction: 0=Transmitter, 1=Receiver */
#define I2C_EVENT_MASTER                  (1UL << 26) /*!< I2C Master operation requested */
#define I2C_EVENT_MASTER_SLAVE             0UL
#define I2C_EVENT_TXBUFFUNSTABLE          (1UL << 27)  /*!<  */
#define I2C_EVENT_RXBUFFUNSTABLE          (1UL << 28)  /*!< Bus Idle */
#define I2C_EVENT_BUFFMODE                (1UL << 29) /*!< I2C BuffMode: 0=ByteMode/FirmWareMode, 1=8-Bytes Buffer Mode */
#define I2C_EVENT_BUSY                    (1UL << 30) /*!< I2C internal process is ongoing */
#define I2C_EVENT_INITREADY               (1UL << 31) /*!< I2C initialized Ready */
/**
  * @}
  */


/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define I2C_STATE_TRANSFER_INCOMPLETE   (1UL << 1)  /*!< I2C Master/Slave Transmit/Receive incomplete transfer */
#define I2C_STATE_GENERALCALL           (1UL << 2)  /*!< General Call indication (cleared on start of next Slave operation) */
#define I2C_STATE_ARBITRATION_LOST      (1UL << 3)  /*!< Master lost arbitration (cleared on start of next Master operation) */
#define I2C_STATE_BUS_ERROR             (1UL << 4)  /*!< Bus error detected (cleared on start of next Master/Slave operation) */
#define I2C_STATE_TIMEOUT               (1UL << 5)  /*!< TimeOut */
#define I2C_STATE_ADDR_NACK             (1UL << 6)  /*!< Address State Received NACK */
#define I2C_STATE_DATA_NACK             (1UL << 7)  /*!< Data State Received NACK */
#define I2C_STATE_RESTART               (1UL << 8)  /*!< Received Bus ReStart */
#define I2C_STATE_STOP                  (1UL << 9)  /*!< Received Bus Stop */
#define I2C_STATE_BRxOVR                (1UL << 10)  /*!<  */
#define I2C_STATE_BTxOVR                (1UL << 11)  /*!<  */
#define I2C_STATE_SRxOVR                (1UL << 12)  /*!<  */
#define I2C_STATE_STxOVR                (1UL << 13)  /*!<  */
//#define I2C_STATE_NONE14                (1UL << 14)  /*!<  */
//#define I2C_STATE_NONE15                (1UL << 15)  /*!<  */
//#define I2C_STATE_NONE16                (1UL << 16)  /*!<  */
//#define I2C_STATE_NONE17                (1UL << 17)  /*!<  */
//#define I2C_STATE_NONE18                (1UL << 18)  /*!<  */
//#define I2C_STATE_NONE19                (1UL << 19)  /*!<  */
//#define I2C_STATE_NONE20                (1UL << 20)  /*!<  */
//#define I2C_STATE_SRx                   (1UL << 21)  /*!<  */
#define I2C_STATE_TXDMABUSY             (1UL << 22)  /*!<  */
#define I2C_STATE_RXDMABUSY             (1UL << 23)  /*!<  */
//#define I2C_STATE_NONE24                (0UL << 24)  /*!<  */
#define I2C_STATE_DIRECTION             (1UL << 25)  /*!< Direction: 0=Transmitter, 1=Receiver */
#define I2C_STATE_DIRECTION_TRAN         0UL         /*!< Direction: 0=Transmitter, 1=Receiver */
#define I2C_STATE_DIRECTION_REC         (1UL << 25)  /*!< Direction: 0=Transmitter, 1=Receiver */
#define I2C_STATE_MASTER                (1UL << 26)  /*!< Master: 0=Slave, 1=Master */
#define I2C_STATE_MASTER_SLAVE           0UL
#define I2C_STATE_TXBUFFUNSTABLE        (1UL << 27)  /*!<  */
#define I2C_STATE_RXBUFFUNSTABLE        (1UL << 28)  /*!< Bus Idle */
#define I2C_STATE_BUFFMODE              (1UL << 29)  /*!< BuffMode: 0=ByteMode/FirmWareMode, 1=8-Bytes Buffer Mode */
#define I2C_STATE_BUSY                  (1UL << 30)  /*!< Busy */
#define I2C_STATE_INITREADY             (1UL << 31)  /*!<  */

typedef union
{
    __IO uint32_t    W;
    __IO uint16_t    H[2];
    __IO uint8_t     B[4];
    struct{
        __IO uint8_t                    :1; ///< bit0
        __IO uint8_t    Incomplete      :1; ///< bit1
        __IO uint8_t    GeneralCall     :1; ///< bit2 General Call indication (cleared on start of next Slave operation)
        __IO uint8_t    ArbitrationLost :1; ///< bit3 Master lost arbitration (cleared on start of next Master operation)
        __IO uint8_t    BusError        :1; ///< bit4 Bus error detected (cleared on start of next Master/Slave operation)
        __IO uint8_t    TimeOut         :1; ///< bit5 TimeOut
        __IO uint8_t    AddrNACK        :1; ///< bit6 Address State Received NACK, Only By Master Transmit
        __IO uint8_t    DataNACK        :1; ///< bit7 Data State Received NACK, Only By Master Transmit
        __IO uint8_t    Restart         :1; ///< bit8 Received Bus ReStart
        __IO uint8_t    Stop            :1; ///< bit9 Received Bus Stop
        __IO uint8_t    BRxOVR          :1; ///< bit10
        __IO uint8_t    BTxOVR          :1; ///< bit11
        __IO uint8_t    SRxOVR          :1; ///< bit12
        __IO uint8_t    STxOVR          :1; ///< bit13
        __IO uint8_t                    :1; ///< bit14
        __IO uint8_t                    :1; ///< bit15
        __IO uint8_t                    :1; ///< bit16
        __IO uint8_t                    :1; ///< bit17
        __IO uint8_t                    :1; ///< bit18
        __IO uint8_t                    :1; ///< bit19
        __IO uint8_t                    :1; ///< bit20
        __IO uint8_t                    :1; ///< bit21
        __IO uint8_t    TxDMABusy       :1; ///< bit22
        __IO uint8_t    RxDMABusy       :1; ///< bit23
        __IO uint8_t                    :1; ///< bit24 
        __IO uint8_t    Direction       :1; ///< bit25 Direction: 0=Transmitter, 1=Receiver
        __IO uint8_t    Master          :1; ///< bit26 Master: 0=Slave, 1=Master
        __IO uint8_t    TxBuffUnStable  :1; ///< bit27 
        __IO uint8_t    RxBuffUnStable  :1; ///< bit28 
        __IO uint8_t    BuffMode        :1; ///< bit29 BuffMode: 0=ByteMode/FirmWareMode, 1=8-Bytes Buffer Mode
        __IO uint8_t    Busy            :1; ///< bit30 Busy flag
        __IO uint8_t    InitReady       :1; ///< bit31
    }Bit;
}I2C_StateTypeDef;
/**
  * @}
  */



/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define I2C_CONTROL_ENDTYPE_NEXT        0   /*!< I2C Master End of Transmission Follow */
#define I2C_CONTROL_ENDTYPE_RESTART     1   /*!< I2C Master End of Transmission Follow */
#define I2C_CONTROL_ENDTYPE_STOP        2   /*!< I2C Master End of Transmission Follow */
#define I2C_CONTROL_ENDTYPE_STOPSTART   3   /*!< I2C Master End of Transmission Follow */
#define I2C_CONTROL_ADDRNACKNEXT        1   /*!< Master Transfer Address NACK */
#define I2C_CONTROL_DATANACKNEXT        1   /*!< Transmint Data NACK */
#define I2C_CONTROL_RXBUFF_EMPTY        1   /*!< Receive Buffer Stable 0=Not Empty, 1=Empty */
#define I2C_CONTROL_TXDATA_READY        1   /*!< Transmit Buffer Stable 0=UnDone, 1=DataReady */
#define I2C_CONTROL_NONE7               1   /*!<  */
#define I2C_CONTROL_RXDMA_DISABLE       0   /*!<  */
#define I2C_CONTROL_RXDMACHANNEL0       1   /*!<  */
#define I2C_CONTROL_RXDMACHANNEL1       2   /*!<  */
#define I2C_CONTROL_RXDMACHANNEL2       3   /*!<  */
#define I2C_CONTROL_TXDMA_DISABLE       0   /*!<  */
#define I2C_CONTROL_TXDMACHANNEL0       1   /*!<  */
#define I2C_CONTROL_TXDMACHANNEL1       2   /*!<  */
#define I2C_CONTROL_TXDMACHANNEL2       3   /*!<  */

typedef union
{
    __IO uint32_t    W;
    __IO uint16_t    H[2];
    __IO uint8_t     B[4];
    struct{
        __IO uint8_t    EndType         :2; ///< bit1:0 I2C Master End of Transmission Follow 0=Next, 1=RepeatStart, 2=Stop, 3=StopStart
        __IO uint8_t    AddrNACKNext    :1; ///< bit2
        __IO uint8_t    DataNACKNext    :1; ///< bit3
        __IO uint8_t    RxBuffEmpty     :1; ///< bit4 Empty Stabel
        __IO uint8_t    TxDataReady     :1; ///< bit5 Data Ready Stable
        __IO uint8_t                    :1; ///< bit6
        __IO uint8_t                    :1; ///< bit7
        __IO uint8_t    RXDMAChannel    :4; ///< bit11:8 0:Disable DMA Channel Source
        __IO uint8_t    TXDMAChannel    :4; ///< bit15:12 0:Disable DMA Channel Sorece
        __IO uint8_t                    :8; ///< bit23:16
        __IO uint8_t                    :8; ///< bit31:24
    }Bit;
}I2C_ControlTypeDef;

/**
  * @}
  */



/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define I2C_STATE_NONE              0UL

/**
  * @}
  */

/** @defgroup HAL_state_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

typedef enum
{
  I2C_TRANSFEREND_NEXT              = 0,     /*!< I2C not yet initialized or disabled         */
  I2C_TRANSFEREND_REPEATSTART       = 1,     /*!< I2C initialized and ready for use           */
  I2C_TRANSFEREND_STOP              = 2,     /*!< I2C not yet initialized or disabled         */
  I2C_TRANSFEREND_STOPSTART         = 3,     /*!< I2C internal process is ongoing             */                                                                        
}I2C_TransferEndTypeDef;

/**
  * @}
  */

/** @defgroup Special_structure_definition Special structure definition
  * @brief  Special structure definition  
  * @{
  */

typedef enum{
    I2C_SPECIAL_NONE = 0,
    I2C_SPECIAL_AddrACK_DataNACK = 1,
    I2C_SPECIAL_ALOSF = 2,
    I2C_SPECIAL_BUSERROR = 3,
}I2C_SpecialTypedef;

/**
  * @}
  */

/** @defgroup I2C_handle_Structure_definition I2C handle Structure definition 
  * @brief  I2C handle Structure definition   
  * @{
  */

typedef struct
{

    __IO I2C_Struct        *Instance;      /*!< I2C registers base address */
    __IO I2C_InitTypeDef    Init;           /*!< I2C communication parameters */

    __IO uint32_t           Trials;         /*!< I2C Master Mode Address Trials */
    __IO uint32_t           Trial_Time;     /*!< I2C Master Mode Address Trials Time */

    __IO I2C_ControlTypeDef Control;        /*!< I2C communication Control */
    __IO I2C_StateTypeDef   State;          /*!< I2C communication State */
    __IO uint32_t           Event;          /*!< I2C communication Event */

    __IO uint8_t            *pTxBuffPtr;    /*!< Pointer to I2C Transmit transfer buffer */
    __IO uint8_t            *pRxBuffPtr;    /*!< Pointer to I2C Received transfer buffer */

    __IO uint32_t           TxferLenth;     /*!< I2C Transmit transfer lenth      */
    __IO uint32_t           RxferLenth;     /*!< I2C Received transfer lenth      */

    __IO uint32_t           TxferCount;     /*!< I2C Transmit transfer counter    */
    __IO uint32_t           RxferCount;     /*!< I2C Received Receive counter     */
    __IO uint32_t           RxShadowCount;  /*!< I2C Received Shadow counter by BufferMode */

    __IO uint8_t            DeviceAddr;     /*!< I2C Master Trnafer Device Address */
    __IO uint8_t            SlaveRecAddr;   /*!< I2C Slave Received Match Address */
    __IO uint8_t            SlaveTraAddr;   /*!< I2C Slave Transmiter Address */
    __IO uint8_t            NextSlaveAddr;  /*!< I2C Slave Mode Next Receiv Address */

    __IO I2C_SpecialTypedef Special;
    __IO uint32_t           SpecialNumber;
    __IO uint32_t           SpecialCount;
}I2C_HandleTypeDef;
/**
  * @}
  */

/**
  * @}
  */  

/* Exported constants --------------------------------------------------------*/

/** @defgroup I2C_Exported_Constants I2C Exported Constants
  * @{
  */



/** @defgroup I2C_TIMING_CK_SEL I2C internal clock CK_I2C source select.
  * @{
  */
#define I2C_TIMING_CK_SEL_PROC          0
#define I2C_TIMING_CK_SEL_TM00_TRGO     2

#define I2C_TIMING_CK_SEL(SELECT)       (((SELECT) == I2C_TIMING_CK_SEL_PROC) || \
                                         ((SELECT) == I2C_TIMING_CK_SEL_PROC))
/**
  * @}
  */


/** @defgroup I2C_TIMING_CK_DIV I2C internal clock CK_I2C source select.
  * @{
  */
#define I2C_TIMING_CK_DIV_1             0
#define I2C_TIMING_CK_DIV_2             1
#define I2C_TIMING_CK_DIV_4             2
#define I2C_TIMING_CK_DIV_8             3
#define I2C_TIMING_CK_DIV_16            4
#define I2C_TIMING_CK_DIV_32            5
#define I2C_TIMING_CK_DIV_64            6
#define I2C_TIMING_CK_DIV_128           7

#define I2C_TIMING_CK_DIV(SELECT)       (((SELECT) == I2C_TIMING_CK_DIV_1) || \
                                         ((SELECT) == I2C_TIMING_CK_DIV_2) || \
                                         ((SELECT) == I2C_TIMING_CK_DIV_4) || \
                                         ((SELECT) == I2C_TIMING_CK_DIV_8) || \
                                         ((SELECT) == I2C_TIMING_CK_DIV_16) || \
                                         ((SELECT) == I2C_TIMING_CK_DIV_32) || \
                                         ((SELECT) == I2C_TIMING_CK_DIV_64) || \
                                         ((SELECT) == I2C_TIMING_CK_DIV_128))
/**
  * @}
  */

/** @defgroup I2C_TIMING_CK_CKS I2C timeout clock select
  * @{
  */
#define I2C_TIMING_CK_CKS_CK_UT         0
#define I2C_TIMING_CK_CKS_PSC64         1

#define I2C_TIMING_CK_CKS(SELECT)       (((SELECT) == I2C_TIMING_CK_CKS_CK_UT) || \
                                         ((SELECT) == I2C_TIMING_CK_CKS_PSC64))

/**
  * @}
  */

/** @defgroup I2C_Operation_mode I2C Operation mode Select
  * @{
  */
#define I2C_OPERATION_MASTER_SLAVE          I2C_CR0_MDS_i2c_w
#define I2C_OPERATION_MULTI_MASTER_SLAVE    I2C_CR0_MDS_i2c_w
#define I2C_OPERATION_Monitor_SLAVE         I2C_CR0_MDS_monitor_w

#define I2C_OPERATION(OPERATION)        (((OPERATION) == I2C_OPERATION_MASTER_SLAVE) || \
                                         ((OPERATION) == I2C_OPERATION_Monitor_SLAVE) || \
                                         ((OPERATION) == I2C_OPERATION_Multi_Mater))
/**
  * @}
  */

/** @defgroup I2C_dual_addressing_mode I2C dual addressing mode
  * @{
  */

#define I2C_DUALADDRESS_DISABLE         ((uint32_t)0x00000000)
#define I2C_DUALADDRESS_ENABLE          I2C_CR0_SADR2_EN_mask_w

#define IS_I2C_DUAL_ADDRESS(ADDRESS)    (((ADDRESS) == I2C_DUALADDRESS_DISABLED) || \
                                         ((ADDRESS) == I2C_DUALADDRESS_ENABLED))
/**
  * @}
  */

/** @defgroup I2C_general_call_addressing_mode I2C general call addressing mode
  * @{
  */
#define I2C_GENERALCALL_DISABLE         ((uint32_t)0x00000000)
#define I2C_GENERALCALL_ENABLE          I2C_CR0_GC_EN_mask_w

#define IS_I2C_GENERAL_CALL(CALL)       (((CALL) == I2C_GENERALCALL_DISABLED) || \
                                         ((CALL) == I2C_GENERALCALL_ENABLED))
/**
  * @}
  */

/** @defgroup I2C_stretch_mode I2C stretch mode
  * @{
  */
#define I2C_STRETCH_DISABLE             I2C_CR0_SCLS_DIS_mask_w
#define I2C_STRETCH_ENABLE              ((uint32_t)0x00000000)

#define IS_I2C_NO_STRETCH(STRETCH)      (((STRETCH) == I2C_NOSTRETCH_DISABLED) || \
                                         ((STRETCH) == I2C_NOSTRETCH_ENABLED))
/**
  * @}
  */

/** @defgroup I2C_Buff_mode I2C Buff mode
  * @{
  */
#define I2C_BUFFMODE_DISABLE            ((uint32_t)0x00000000)
#define I2C_BUFFMODE_ENABLE             I2C_CR0_BUF_EN_mask_w

#define IS_I2C_BUFFMODE(BUFFMODE)       (((BUFFMODE) == I2C_BUFFMODE_DISABLE) || \
                                         ((BUFFMODE) == I2C_BUFFMODE_ENABLE))
/**
  * @}
  */

/** @defgroup I2C_PreDrive Mode I2C PreDrive Mode
  * @{
  */
#define I2C_PREDRIVE_0T                 I2C_CR0_PDRV_SEL_0t_w
#define I2C_PREDRIVE_1T                 I2C_CR0_PDRV_SEL_1t_w
#define I2C_PREDRIVE_2T                 I2C_CR0_PDRV_SEL_2t_w
#define I2C_PREDRIVE_3T                 I2C_CR0_PDRV_SEL_3t_w

#define IS_I2C_PREDRIV(PREDRIV)         (((PREDRIV) == I2C_CR0_PDRV_SEL_0t_w) || \
                                         ((PREDRIV) == I2C_CR0_PDRV_SEL_1t_w) || \
                                         ((PREDRIV) == I2C_CR0_PDRV_SEL_2t_w) || \
                                         ((PREDRIV) == I2C_CR0_PDRV_SEL_3t_w))
/**
  * @}
  */

/** @defgroup I2C_SDAFBDH Mode I2C SDA Fist Bit Drive High Enable
  * @{
  */
#define I2C_SDAFBDH_DISABLE             ((uint32_t)0x00000000)
#define I2C_SDAFBDH_ENABLE              I2C_CR0_SFBD_EN_mask_w

#define IS_I2C_SDAFBDH(SDAFBDH)         (((SDAFBDH) == I2C_SDAFBDH_DISABLE) || \
                                         ((SDAFBDH) == I2C_SDAFBDH_ENABLE))
/**
  * @}
  */

/** @defgroup I2C_StartStopMode_definition I2C StartStopMode definition
  * @{
  */

#define  I2C_NO_STARTSTOP               (uint32_t)(I2C_CR2_STA_LCK_mask_w | I2C_CR2_STO_LCK_mask_w)
#define  I2C_GENERATE_STOP              (uint32_t)(I2C_CR2_STO_mask_w | I2C_CR2_STA_LCK_mask_w | I2C_CR2_STO_LCK_mask_w)
#define  I2C_GENERATE_START             (uint32_t)(I2C_CR2_STA_mask_w | I2C_CR2_STA_LCK_mask_w | I2C_CR2_STO_LCK_mask_w)
#define  I2C_GENERATE_REPEATSTART       (uint32_t)(I2C_CR2_STA_mask_w | I2C_CR2_STO_mask_w | I2C_CR2_STA_LCK_mask_w | I2C_CR2_STO_LCK_mask_w)

#define  I2C_GENERATE_PreSTOP           (uint32_t)(I2C_CR2_CMD_TC_mask_w | I2C_CR2_STO_mask_w | I2C_CR2_STO_LCK_mask_w)
#define  I2C_GENERATE_PreSTART          (uint32_t)(I2C_CR2_CMD_TC_mask_w | I2C_CR2_STA_mask_w | I2C_CR2_STA_LCK_mask_w)
#define  I2C_GENERATE_preREPEATSTART    (uint32_t)(I2C_CR2_CMD_TC_mask_w | I2C_CR2_STA_mask_w | I2C_CR2_STO_mask_w | I2C_CR2_STA_LCK_mask_w | I2C_CR2_STO_LCK_mask_w)

#define  IS_TRANSFER_REQUEST(REQUEST)   (((REQUEST) == I2C_GENERATE_STOP)        || \
                                         ((REQUEST) == I2C_GENERATE_START)  || \
                                         ((REQUEST) == I2C_GENERATE_REPEATSTART) || \
                                         ((REQUEST) == I2C_GENERATE_PreSTOP) || \
                                         ((REQUEST) == I2C_GENERATE_PreSTART) || \
                                         ((REQUEST) == I2C_GENERATE_preREPEATSTART) || \
                                         ((REQUEST) == I2C_NO_STARTSTOP))

/**
  * @}
  */

/** @defgroup I2C_Interrupt_configuration_definition I2C Interrupt configuration definition
  * @brief I2C Interrupt definition
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX  : Interrupt control mask
  * @{
  */
#define I2C_INT_STPSTR                    I2C_INT_STPSTR_IE_mask_w
#define I2C_INT_WUP                       I2C_INT_WUP_IE_mask_w
#define I2C_INT_TMOUT                     I2C_INT_TMOUT_IE_mask_w
#define I2C_INT_ERR                       I2C_INT_ERR_IE_mask_w
#define I2C_INT_BUF                       I2C_INT_BUF_IE_mask_w
#define I2C_INT_EVENT                     I2C_INT_EVENT_IE_mask_w
#define I2C_INT_IEA                       I2C_INT_IEA_mask_w

/**
  * @}
  */

/** @defgroup I2C_Flag_definition I2C Flag definition
  * @brief I2C Interrupt definition
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX  : Interrupt control mask
  * @{
  */ 

#define I2C_FLAG_BUSYF                    I2C_STA_BUSYF_mask_w         /* busy flag */
#define I2C_FLAG_EVENTF                   I2C_STA_EVENTF_mask_w        /* status event interrupt Flag */
#define I2C_FLAG_BUFF                     I2C_STA_BUFF_mask_w          /* buffer mode event flag. */
#define I2C_FLAG_ERRF                     I2C_STA_ERRF_mask_w          /* error interrupt flag for invalid no ack, bus arbitration lost bus error or data overrun error. */
#define I2C_FLAG_TMOUTF                   I2C_STA_TMOUTF_mask_w        /* time-out detect flag. */
#define I2C_FLAG_WUPF                     I2C_STA_WUPF_mask_w          /* wakeup from STOP mode flag. */
#define I2C_FLAG_RXF                      I2C_STA_RXF_mask_w           /* Receive data register not empty. */
#define I2C_FLAG_TXF                      I2C_STA_TXF_mask_w           /* Transmit data register empty. */
#define I2C_FLAG_RSTRF                    I2C_STA_RSTRF_mask_w         /* repeat start asserted flag. */
#define I2C_FLAG_STOPF                    I2C_STA_STOPF_mask_w         /* stop detection flag. */
#define I2C_FLAG_CNTF                     I2C_STA_CNTF_mask_w          /* buffer count I2Cx_BUF_CNT empty status. */
#define I2C_FLAG_ERRCF                    I2C_STA_ERRCF_mask_w         /* error event clear control flag. */
#define I2C_FLAG_SADRF                    I2C_STA_SADRF_mask_w         /* slave mode slave address matched or master received mode slave address asserted flag. */
#define I2C_FLAG_SLAF                     I2C_STA_SLAF_mask_w          /* slave mode detection status */
#define I2C_FLAG_MSTF                     I2C_STA_MSTF_mask_w          /* master mode detection status. */
#define I2C_FLAG_RWF                      I2C_STA_RWF_mask_w           /* read or write transfer direction status. */
#define I2C_FLAG_TCF                      I2C_STA_TCF_mask_w           /* transfer complete flag. */
#define I2C_FLAG_STPSTRF                  ((uint32_t)0x00020000)       /* I2Cx Stop or Start detection flag. */
#define I2C_FLAG_TXRF                     I2C_STA_TXRF_mask_w          /* transmit data register remained status. */
#define I2C_FLAG_ROVRF                    I2C_STA_ROVRF_mask_w         /* data buffer receive overrun error flag. */
#define I2C_FLAG_TOVRF                    I2C_STA_TOVRF_mask_w         /* data buffer transmit underrun error flag. */
#define I2C_FLAG_NACKF                    I2C_STA_NACKF_mask_w         /* Not Acknowledge received error flag. */
#define I2C_FLAG_ALOSF                    I2C_STA_ALOSF_mask_w         /* bus arbitration lost error flag. */
#define I2C_FLAG_BERRF                    I2C_STA_BERRF_mask_w         /* bus error flag for invalid Stop/Start state */

/**
  * @}
  */


/** @defgroup I2C_OWNER_Addressing
* @{
  */

#define I2C_OWNER1_ADDRESS                I2C_CR0_SADR_EN_mask_w
#define I2C_OWNER2_ADDRESS                I2C_CR0_SADR2_EN_mask_w

/**
  * @}
  */

/**
  * @}
  */

/* Exported macros -----------------------------------------------------------*/

/** @defgroup I2C_Exported_Macros I2C Exported Macros
  * @{
  */

/** @brief  Reset I2C handle state
  * @param  __HANDLE__: I2C handle.
  * @retval None
  */
#define __HAL_I2C_RESET_HANDLE_STATE(__HANDLE__) ((__HANDLE__)->State = HAL_I2C_STATE_RESET)

/** @brief  Enables or disables the specified I2C interrupts.
  * @param  __HANDLE__: specifies the I2C Handle.
  *         This parameter can be I2Cx where x: 1 or 2 to select the I2C peripheral.
  * @param  __INTERRUPT__: specifies the interrupt source to enable or disable.
  *         This parameter can be one of the following values:
  *            @arg I2C_INT_WUPI: Errors interrupt enable
  *            @arg I2C_INT_TMOUTI: Transfer complete interrupt enable
  *            @arg I2C_INT_ERRI: STOP detection interrupt enable
  *            @arg I2C_INT_BUFI : NACK received interrupt enable
  *            @arg I2C_INT_EVENTI: Address match interrupt enable
  *            @arg I2C_INT_IEA: RX interrupt enable
  *
  * @retval None
  */
#define __HAL_I2C_ENABLE_IT(__HANDLE__, __INTERRUPT__)   ((__HANDLE__)->Instance->INT.W |= (__INTERRUPT__))
#define __HAL_I2C_DISABLE_IT(__HANDLE__, __INTERRUPT__)  ((__HANDLE__)->Instance->INT.W &= (~(__INTERRUPT__)))

/** @brief  Checks if the specified I2C interrupt source is enabled or disabled.
  * @param  __HANDLE__: specifies the I2C Handle.
  *         This parameter can be I2Cx where x: 1 or 2 to select the I2C peripheral.
  * @param  __INTERRUPT__: specifies the I2C interrupt source to check.
  *         This parameter can be one of the following values:
  *            @arg I2C_INT_STPSTR:
  *            @arg I2C_INT_WUP: 
  *            @arg I2C_INT_TMOUT: 
  *            @arg I2C_INT_ERR: Errors interrupt enable
  *            @arg I2C_INT_BUF: 
  *            @arg I2C_INT_EVENT: 
  *            @arg I2C_INT_IEA: 
  *   
  * @retval The new state of __IT__ (TRUE or FALSE).
  */
#define __HAL_I2C_GET_IT_SOURCE(__HANDLE__, __INTERRUPT__) ((((__HANDLE__)->Instance->INT.W & (__INTERRUPT__)) == (__INTERRUPT__)) ? SET : RESET)

/** @brief  Checks whether the specified I2C flag is set or not.
  * @param  __HANDLE__: specifies the I2C Handle.
  *         This parameter can be I2Cx where x: 1 or 2 to select the I2C peripheral.
  * @param  __FLAG__: specifies the flag to check.
  *         This parameter can be one of the following values:
  *            @arg I2C_FLAG_BUSYF:
  *            @arg I2C_FLAG_EVENTF:
  *            @arg I2C_FLAG_BUFF:
  *            @arg I2C_FLAG_ERRF:
  *            @arg I2C_FLAG_TMOUTF:
  *            @arg I2C_FLAG_WUPF:
  *            @arg I2C_FLAG_RXF:
  *            @arg I2C_FLAG_TXF:
  *            @arg I2C_FLAG_RSTRF:
  *            @arg I2C_FLAG_STOPF:
  *            @arg I2C_FLAG_CNTF:
  *            @arg I2C_FLAG_ERRCF:
  *            @arg I2C_FLAG_SADRF:
  *            @arg I2C_FLAG_SLAF:
  *            @arg I2C_FLAG_MSTF:
  *            @arg I2C_FLAG_RWF:
  *            @arg I2C_FLAG_TCF:
  *            @arg I2C_FLAG_STPSTRF:
  *            @arg I2C_FLAG_TXRF:
  *            @arg I2C_FLAG_ROVRF:
  *            @arg I2C_FLAG_TOVRF:
  *            @arg I2C_FLAG_NACKF:
  *            @arg I2C_FLAG_ALOSF:
  *            @arg I2C_FLAG_BERRF:
  *
  * @retval The new state of __FLAG__ (TRUE or FALSE).
  */
#define __HAL_I2C_GET_FLAG(__HANDLE__, __FLAG__) ((((__HANDLE__)->Instance->STA.W) & (__FLAG__)) == (__FLAG__))
#define __HAL_I2C_CLEAR_FLAG(__HANDLE__, __FLAG__) ((__HANDLE__)->Instance->STA.W = (__FLAG__))


/** @brief  Clears the I2C pending flags which are cleared by writing 1 in a specific bit.
  * @param  __HANDLE__: specifies the I2C Handle.
  *         This parameter can be I2Cx where x: 1 or 2 to select the I2C peripheral.
  * @param  __OWNER__: specifies the flag to clear.
  *         This parameter can be any combination of the following values:
  *            @arg I2C_OWNER1_ADDRESS :
  *            @arg I2C_OWNER2_ADDRESS :
  *
  * @retval None
  */
#define __HAL_I2C_OWNADDRESS_ENABLE(__HANDLE__, __OWNER__)      ((__HANDLE__)->Instance->CR0.W |= (__OWNER__))
#define __HAL_I2C_OWNADDRESS_DISABLE(__HANDLE__, __OWNER__)     ((__HANDLE__)->Instance->CR0.W &= (~(__OWNER__)))

#define __HAL_I2C_EVENT_CODE(__HANDLE__)                        ((__HANDLE__)->Instance->STA2.B[0] & 0xF8)

#define __HAL_I2C_GET_EVENTF(__HANDLE__)                        ((__HANDLE__)->Instance->STA2.W & I2C_STA2_EVENTF2_happened_w)
                                                                //((__HANDLE__)->Instance->STA.W & I2C_STA_EVENTF_happened_w))

#define __HAL_I2C_CLEAR_EVENTF(__HANDLE__)                      ((__HANDLE__)->Instance->STA2.W = I2C_STA2_EVENTF2_happened_w)
                                                                //((__HANDLE__)->Instance->STA.W & I2C_STA_EVENTF_happened_w))


#define __HAL_I2C_ENABLE(__HANDLE__)                            ((__HANDLE__)->Instance->CR0.W |=  I2C_CR0_EN_mask_w)
#define __HAL_I2C_DISABLE(__HANDLE__)                           ((__HANDLE__)->Instance->CR0.W &=  ~I2C_CR0_EN_mask_w)

#define __HAL_I2C_RESET_CR2(__HANDLE__)                         ((__HANDLE__)->Instance->CR2 &= (uint32_t)~((uint32_t)(I2C_CR2_SADD | I2C_CR2_HEAD10R | I2C_CR2_NBYTES | I2C_CR2_RELOAD | I2C_CR2_RD_WRN)))

#define __HAL_I2C_GENERATE_START(__ADDMODE__,__ADDRESS__)       (((__ADDMODE__) == I2C_ADDRESSINGMODE_7BIT) ? (uint32_t)((((uint32_t)(__ADDRESS__) & (I2C_CR2_SADD)) | (I2C_CR2_START) | (I2C_CR2_AUTOEND)) & (~I2C_CR2_RD_WRN)) : \
                                                                                                          (uint32_t)((((uint32_t)(__ADDRESS__) & (I2C_CR2_SADD)) | (I2C_CR2_ADD10) | (I2C_CR2_START)) & (~I2C_CR2_RD_WRN)))

#define __HAL_I2C_DMA_TX_ENABLE(__HANDLE__)                     ((__HANDLE__)->Instance->CR0.W |=  I2C_CR0_DMA_TXEN_mask_w)
#define __HAL_I2C_DMA_TX_DISABLE(__HANDLE__)                    ((__HANDLE__)->Instance->CR0.W &=  ~I2C_CR0_DMA_TXEN_mask_w)

#define __HAL_I2C_DMA_RX_ENABLE(__HANDLE__)                     ((__HANDLE__)->Instance->CR0.W |=  I2C_CR0_DMA_RXEN_mask_w)
#define __HAL_I2C_DMA_RX_DISABLE(__HANDLE__)                    ((__HANDLE__)->Instance->CR0.W &=  ~I2C_CR0_DMA_RXEN_mask_w)

#define IS_I2C_OWNER1_ADDRESS(OWNER1)                           ((OWNER1) <= (uint32_t)0x000003FF)
#define IS_I2C_OWNER2_ADDRESS(OWNER2)                           ((OWNER2) <= (uint16_t)0x00FF)

/**
  * @}
  */ 

/* Include I2C HAL Extended module */
//#include "stm32f0xx_hal_i2c_ex.h"

/* Exported functions --------------------------------------------------------*/
/** @addtogroup I2C_Exported_Functions
  * @{
  */

/** @addtogroup I2C_Exported_Functions_Group1 Initialization and de-initialization functions
  * @{
  */

/* Initialization/de-initialization functions  **********************************/
HAL_StatusTypeDef HAL_I2C_Init(I2C_HandleTypeDef *I2Cx);
HAL_StatusTypeDef HAL_I2C_DeInit (I2C_HandleTypeDef *I2Cx);
void HAL_I2C_MspInit(I2C_HandleTypeDef *I2Cx);
void HAL_I2C_MspDeInit(I2C_HandleTypeDef *I2Cx);

/**
  * @}
  */ 

/** @addtogroup I2C_Exported_Functions_Group2 Input and Output operation functions
  * @{
  */
   
/* IO operation functions  *****************************************************/

 /******* Blocking mode: Polling */
HAL_StatusTypeDef HAL_I2C_BuffModeMasterTransmit(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_BuffModeMasterReceive(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_BuffModeSlaveTransmit(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_BuffModeSlaveReceive(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_BuffModeSlaveReceiveGC(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);

HAL_StatusTypeDef HAL_I2C_FWModeMasterTransmit(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_FWModeMasterReceive(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_FWModeSlaveTransmit(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_FWModeSlaveReceive(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_FWModeSlaveReceveGC(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout);

// HAL_StatusTypeDef HAL_I2C_Mem_Write(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
// HAL_StatusTypeDef HAL_I2C_Mem_Read(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size, uint32_t Timeout);
HAL_StatusTypeDef HAL_I2C_IsDeviceReady(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint32_t Trials, uint32_t Timeout);

 /******* Non-Blocking mode: Interrupt */
HAL_StatusTypeDef HAL_I2C_BuffModeMasterTransmitIT(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_BuffModeMasterReceiveIT(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth);
// HAL_StatusTypeDef HAL_I2C_BuffModeSlaveTransmitIT(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_BuffModeSlaveTransmitIT(I2C_HandleTypeDef *I2Cx, uint8_t OwnAddress, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_BuffModeSlaveReceiveIT(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth);

HAL_StatusTypeDef HAL_I2C_ByteModeMasterTransmitIT(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_ByteModeMasterReceiveIT(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth);
// HAL_StatusTypeDef HAL_I2C_ByteModeSlaveTransmitIT(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_ByteModeSlaveTransmitIT(I2C_HandleTypeDef *I2Cx, uint8_t OwnAddress, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_ByteModeSlaveReceiveIT(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_ByteModeSlaveReceiveGCIT(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth);

// HAL_StatusTypeDef I2C_Mem_Write_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size);
// HAL_StatusTypeDef I2C_Mem_Read_IT(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size);

 /******* Non-Blocking mode: DMA */
HAL_StatusTypeDef HAL_I2C_ByteModeMasterTransmitDMA(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_ByteModeMasterReceiveDMA(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_ByteModeSlaveTransmitDMA(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth);
HAL_StatusTypeDef HAL_I2C_ByteModeSlaveReceiveDMA(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth);
// HAL_StatusTypeDef I2C_ByteModeMemWriteDMA(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Lenth);
// HAL_StatusTypeDef I2C_ByteModeMemReadDMA(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Lenth);

 /******* I2C IRQHandler and Callbacks used in non blocking modes (Interrupt and DMA) */
void HAL_I2C_ByteMode_IRQHandler(I2C_HandleTypeDef *I2Cx);
void HAL_I2C_ByteMode_STPSTRF_IRQHandler(I2C_HandleTypeDef *I2Cx);
void HAL_I2C_Error_IRQHandler(I2C_HandleTypeDef *I2Cx);
void HAL_I2C_BUFF_IRQHandler(I2C_HandleTypeDef *I2Cx);
void HAL_I2C_WakeUp_IRQHandler(I2C_HandleTypeDef *I2Cx);
void HAL_I2C_TimeOut_IRQHandler(I2C_HandleTypeDef *I2Cx);

//void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c);
//void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c);
//void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hi2c);
//void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c);
//void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c);
//void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c);
//void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c);

/**
  * @}
  */ 

/** @addtogroup I2C_Exported_Functions_Group3 Peripheral State and Errors functions
  * @{
  */

/* Peripheral State and Errors functions  **************************************/
HAL_StatusTypeDef HAL_I2C_GetState(I2C_HandleTypeDef *hi2c);
uint32_t             HAL_I2C_GetError(I2C_HandleTypeDef *hi2c);

/**
  * @}
  */ 


#ifdef __cplusplus
}
#endif

//=============================================================================

/** @defgroup Start / Stop / Acknowledge Enable / Disable.
  * @brief  Start / Stop / Acknowledge Enable / Disable.
  * @{
  */

/** @brief  Start / Stop / Acknowledge Enable / Disable.
  */
#define __HAL_I2C_PreSet_ASSERT_ACKNOWLEDGE_CLR(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~I2C_CR2_AA_mask_b0) | (I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PreSet_ASSERT_ACKNOWLEDGE_SET(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = ((__HANDLE__)->CR2.B[0] | (I2C_CR2_AA_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Set_ASSERT_ACKNOWLEDGE_CLR(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~(I2C_CR2_AA_mask_b0 | I2C_CR2_CMD_TC_mask_b0)) | I2C_CR2_AA_LCK_mask_b0)
#define __HAL_I2C_Set_ASSERT_ACKNOWLEDGE_SET(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~I2C_CR2_CMD_TC_mask_b0) | (I2C_CR2_AA_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PAA_0(__HANDLE__)     __HAL_I2C_PreSet_ASSERT_ACKNOWLEDGE_CLR(__HANDLE__)
#define __HAL_I2C_PAA_1(__HANDLE__)     __HAL_I2C_PreSet_ASSERT_ACKNOWLEDGE_SET(__HANDLE__)
#define __HAL_I2C_AA_0(__HANDLE__)      __HAL_I2C_Set_ASSERT_ACKNOWLEDGE_CLR(__HANDLE__)
#define __HAL_I2C_AA_1(__HANDLE__)      __HAL_I2C_Set_ASSERT_ACKNOWLEDGE_SET(__HANDLE__)

#define __HAL_I2C_PreSet_START_CLR(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~I2C_CR2_STA_mask_b0) | (I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0))
#define __HAL_I2C_PreSet_START_SET(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = ((__HANDLE__)->CR2.B[0] | (I2C_CR2_STA_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0))
#define __HAL_I2C_Set_START_CLR(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~(I2C_CR2_STA_mask_b0 | I2C_CR2_CMD_TC_mask_b0)) | I2C_CR2_STA_LCK_mask_b0)
#define __HAL_I2C_Set_START_SET(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~I2C_CR2_CMD_TC_mask_b0) | (I2C_CR2_STA_mask_b0 | I2C_CR2_STA_LCK_mask_b0))
#define __HAL_I2C_PSTA_0(__HANDLE__)    __HAL_I2C_PreSet_START_CLR(__HANDLE__)
#define __HAL_I2C_PSTA_1(__HANDLE__)    __HAL_I2C_PreSet_START_SET(__HANDLE__)
#define __HAL_I2C_STA_0(__HANDLE__)     __HAL_I2C_Set_START_CLR(__HANDLE__)
#define __HAL_I2C_STA_1(__HANDLE__)     __HAL_I2C_Set_START_SET(__HANDLE__)

#define __HAL_I2C_PreSet_STOP_CLR(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~I2C_CR2_STO_mask_b0) | (I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STO_LCK_mask_b0))
#define __HAL_I2C_PreSet_STOP_SET(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = ((__HANDLE__)->CR2.B[0] | (I2C_CR2_STO_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STO_LCK_mask_b0))
#define __HAL_I2C_Set_STOP_CLR(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~(I2C_CR2_STO_mask_b0 | I2C_CR2_CMD_TC_mask_b0)) | I2C_CR2_STO_LCK_mask_b0)
#define __HAL_I2C_Set_STOP_SET(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]& ~I2C_CR2_CMD_TC_mask_b0) | (I2C_CR2_STO_mask_b0 | I2C_CR2_STO_LCK_mask_b0))
#define __HAL_I2C_PSTO_0(__HANDLE__)    __HAL_I2C_PreSet_STOP_CLR(__HANDLE__)
#define __HAL_I2C_PSTO_1(__HANDLE__)    __HAL_I2C_PreSet_STOP_SET(__HANDLE__)
#define __HAL_I2C_STO_0(__HANDLE__)     __HAL_I2C_Set_STOP_CLR(__HANDLE__)
#define __HAL_I2C_STO_1(__HANDLE__)     __HAL_I2C_Set_STOP_SET(__HANDLE__)

#define __HAL_I2C_PreSet_STA_STO_AA_111(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = ((__HANDLE__)->CR2.B[0]|(I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PreSet_STA_STO_AA_110(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_AA_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PreSet_STA_STO_AA_101(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_STO_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PreSet_STA_STO_AA_100(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PreSet_STA_STO_AA_011(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_STA_mask_b0)))|(I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PreSet_STA_STO_AA_010(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_STA_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STO_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PreSet_STA_STO_AA_001(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0)))|(I2C_CR2_AA_mask_b0 | I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_PreSet_STA_STO_AA_000(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))

#define __HAL_I2C_Set_STA_STO_AA_111(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Set_STA_STO_AA_110(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Set_STA_STO_AA_101(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STO_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Set_STA_STO_AA_100(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Set_STA_STO_AA_011(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_mask_b0)))|(I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Set_STA_STO_AA_010(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STO_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Set_STA_STO_AA_001(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0)))|(I2C_CR2_AA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Set_STA_STO_AA_000(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))

#define __HAL_I2C_PAction_111(__HANDLE__) __HAL_I2C_PreSet_STA_STO_AA_111(__HANDLE__)
#define __HAL_I2C_PAction_110(__HANDLE__) __HAL_I2C_PreSet_STA_STO_AA_110(__HANDLE__)
#define __HAL_I2C_PAction_101(__HANDLE__) __HAL_I2C_PreSet_STA_STO_AA_101(__HANDLE__)
#define __HAL_I2C_PAction_100(__HANDLE__) __HAL_I2C_PreSet_STA_STO_AA_100(__HANDLE__)
#define __HAL_I2C_PAction_011(__HANDLE__) __HAL_I2C_PreSet_STA_STO_AA_011(__HANDLE__)
#define __HAL_I2C_PAction_010(__HANDLE__) __HAL_I2C_PreSet_STA_STO_AA_010(__HANDLE__)
#define __HAL_I2C_PAction_001(__HANDLE__) __HAL_I2C_PreSet_STA_STO_AA_001(__HANDLE__)
#define __HAL_I2C_PAction_000(__HANDLE__) __HAL_I2C_PreSet_STA_STO_AA_000(__HANDLE__)

#define __HAL_I2C_Action_111(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Action_110(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Action_101(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STO_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Action_100(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Action_011(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_mask_b0)))|(I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Action_010(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STO_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Action_001(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0)))|(I2C_CR2_AA_mask_b0 | I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
#define __HAL_I2C_Action_000(__HANDLE__) (__HANDLE__)->Instance->CR2.B[0] = (((__HANDLE__)->CR2.B[0]&(~(I2C_CR2_CMD_TC_mask_b0 | I2C_CR2_STA_mask_b0 | I2C_CR2_STO_mask_b0 | I2C_CR2_AA_mask_b0)))|(I2C_CR2_STA_LCK_mask_b0 | I2C_CR2_STO_LCK_mask_b0 | I2C_CR2_AA_LCK_mask_b0))
/**
  * @}
  */



#define I2C_CLK_CK_PSC_0_w              ((uint32_t) 0x00000000)
#define I2C_CLK_CK_PSC_1_w              ((uint32_t) 0x00000100)
#define I2C_CLK_CK_PSC_2_w              ((uint32_t) 0x00000200)
#define I2C_CLK_CK_PSC_3_w              ((uint32_t) 0x00000300)
#define I2C_CLK_CK_PSC_4_w              ((uint32_t) 0x00000400)
#define I2C_CLK_CK_PSC_5_w              ((uint32_t) 0x00000500)
#define I2C_CLK_CK_PSC_6_w              ((uint32_t) 0x00000600)
#define I2C_CLK_CK_PSC_7_w              ((uint32_t) 0x00000700)
  
////=============================================================================

/**
 extern }
  */ 

#endif

