#ifndef __MG32x02z_HAL_EMB_H
#define __MG32x02z_HAL_EMB_H

#ifdef __cplusplus
 extern "C" {
#endif



/* Includes ------------------------------------------------------------------*/
//#include "stdio.h"
//#include "string.h"
//#include "System_Config.h"
//#include "main.h"
//#include "Mx92G8z__Common_MID.h"
#include "MG32x02z_HAL_Common.h"


/** @defgroup I2C_Timing Configuration_Structure_definition I2C Timing Configuration Structure definition
  * @brief  I2C Timing Configuration Structure definition    
  * @{
  */
typedef union{
    uint32_t W[2];
    uint16_t H[4];
    uint8_t B[8];
    struct{
                                        // for EMB CR0
        uint8_t                 :1;     //[0]
        uint8_t     WEN         :1;     //[1] EMB write enable. (for flash memory)
        uint8_t     BW_EN       :1;     //[2] EMB bus byte write enable.
        uint8_t     SYNC_EN     :1;     //[3] EMB synchronous transaction enable. 0 = Async, 1 = Sync. 
        uint8_t                 :2;     //[5..4]
        uint8_t                 :1;     //[6]
        uint8_t                 :1;     //[7]
        uint8_t     BUS_DSIZE   :1;     //[8] EMB bus access data size. 0x0 = 8-bit, 0x1 = 16-bit.
        uint8_t                 :1;     //[9] 
        uint8_t     BUS_MDS     :1;     //[10] EMB bus address and data bus mode select. 0 = Multiplex, 1 = Separated
        uint8_t     ADR_TWO     :1;     //[11] EMB two address phase timing mode enable.
        uint8_t     ADR_SEL     :2;     //[13..12] EMB bus address range select.
        uint8_t                 :1;     //[14]
        uint8_t                 :1;     //[15]
        uint8_t     CE_MDS      :2;     //[17..16] EMB MCE signal mode select.
                                        //         0x0 = CE : chip enable signal
                                        //         0x1 = ALE : same as ALE timing
                                        //         0x2 = ALE2 : 2nd phase address latch enable
        uint8_t                 :1;     //[18]
        uint8_t                 :1;     //[19]
        uint8_t                 :1;     //[20]
        uint8_t                 :3;     //[23..21]
        uint8_t     ALE2_MDS    :1;     //[24] EMB MALE2 signal mode select. When 
                                        //     0x0 = ALE2 : 2nd phase address latch enable
                                        //     0x1 = ALE : same as ALE timing
        uint8_t                 :1;     //[25]
        uint8_t     OE_CTL      :1;     //[26] EMB MOE control timing select.
                                        //     0x0 = TOGGLE : high-to-low change
                                        //     0x1 = LOW : drive low during read access
        uint8_t     WE_CTL      :1;     //[27]
                                        //     0x0 = TOGGLE : high-to-low change
                                        //     0x1 = LOW : drive low during read access
        uint8_t                 :3;     //[30..28]
        uint8_t     DMA_EN      :1;     //[31] EMB DMA Enable

                                        // for EMB CR1
        uint8_t     CE_INV      :1;     //[0] 
        uint8_t     ALE_INV     :1;     //[1]
        uint8_t     ALE2_INV    :1;     //[2]
        uint8_t     CLK_INV     :1;     //[3]
        uint8_t                 :1;     //[4]
        uint8_t                 :3;     //[7..5]
        uint8_t     MA_SWAP     :1;     //[8]
        uint8_t     MAD_SWAP    :1;     //[9] 
        uint8_t     MAD_BSWAP   :1;     //[10] 
        uint8_t                 :1;     //[11] 
        uint8_t     MAM1_SEL    :2;     //[13..12] EMB MA-1 signal output pin select.
                                        //         0x0 = No : not output MA-1 signal
                                        //         0x1 = MAD15
                                        //         0x2 = BW1
                                        //         0x3 = ALE2
        uint8_t                 :2;     //[15..14] 
        uint8_t     CE_SWEN     :1;     //[16] EMB MCE signal output software control enable bit. 
        uint8_t     CE_SWO      :1;     //[17] EMB MCE signal software control output data bit.
        uint8_t     ALE_SWEN    :1;     //[18] EMB MALE signal output software control enable bit. 
        uint8_t     ALE_SWO     :1;     //[19] EMB MALE signal software control output data bit.
        uint8_t     ALE2_SWEN   :1;     //[20] EMB MALE2 signal output software control enable bit. 
        uint8_t     ALE2_SWO    :1;     //[21] EMB MALE2 signal software control output data bit.
        uint8_t     BW0_SWEN    :1;     //[22] EMB BW0 signal output software control enable bit. 
        uint8_t     BW0_SWO     :1;     //[23] EMB BW0 signal software control output data bit.
        uint8_t     BW1_SWEN    :1;     //[24] EMB BW0 signal output software control enable bit. 
        uint8_t     BW1_SWO     :1;     //[25] EMB BW0 signal software control output data bit.
        uint8_t                 :2;     //[27..26]
        uint8_t     OE_MUX      :2;     //[29..28] MOE output signal select.
                                        //         0x0 = OE : EMB Output Enable signal
                                        //         0x1 = TM10 : TM10_CKO
                                        //         0x2 = TM16 : TM16_CKO
                                        //         0x3 = TM20 : TM20_CKO
        uint8_t     WE_MUX      :2;     //[31..30] MWE output signal select.
                                        //         0x0 = WE : EMB Write Enable signal
                                        //         0x1 = TM10 : TM10_CKO
                                        //         0x2 = TM16 : TM16_CKO
                                        //         0x3 = TM20 : TM20_CKO
    }Bits;
}EMB_OperationModeTypeDef;

/**
  * @}
  */



/** @defgroup I2C_Timing Configuration_Structure_definition I2C Timing Configuration Structure definition
  * @brief  I2C Timing Configuration Structure definition    
  * @{
  */
typedef union{
    uint32_t W[2];
    uint16_t H[4];
    uint8_t B[8];
    struct{
                                        // for EMB CLK
        uint8_t                 :4;     //[3..0]
        uint8_t     CK_PSC      :3;     //[6..4]    
        uint8_t                 :1;     //[7]
        uint8_t                 :8;     //[15..8]
        uint8_t                 :8;     //[23..16]
        uint8_t                 :8;     //[31..24]

                                        // for EMB CR2
        uint8_t     ALES        :1;     //[0] EMB SRAM/NOR bus ALE/ALE2 setup time.
        uint8_t                 :3;     //[3..1]
        uint8_t     ALEW        :3;     //[6..4] EMB SRAM/NOR bus ALE/ALE2 pulse width.
        uint8_t                 :1;     //[7]
        uint8_t     ALEH        :1;     //[8] EMB SRAM/NOR bus ALE/ALE2 hold time.
        uint8_t                 :3;     //[11..9]
        uint8_t     ACCS        :1;     //[12] EMB SRAM/NOR bus data access setup time. 
        uint8_t                 :3;     //[15..13]
        uint8_t     ACCW        :4;     //[19..16] EMB SRAM/NOR bus data access time.
        uint8_t     ACCH        :1;     //[20] EMB SRAM/NOR bus data access hold time. 
        uint8_t                 :3;     //[23..21] 
        uint8_t     IDLE        :2;     //[25..24] EMB SRAM/NOR bus idle time between two successive cycles. 
        uint8_t                 :2;     //[27..26]
        uint8_t                 :4;     //[31..28]
    }Bits;
}EMB_TimingTypeDef;

/**
  * @}
  */



/** @defgroup I2C_Configuration_Structure_definition I2C Configuration Structure definition
  * @brief  I2C Configuration Structure definition    
  * @{
  */
typedef struct
{
    EMB_OperationModeTypeDef OperationMode;  /*!<                                            */  

    EMB_TimingTypeDef Timing;           /*!< Specifies the I2C_TIMINGR_register value.
                                          This parameter calculated by referring to I2C initialization 
                                          section in Reference manual */
}EMB_InitTypeDef;

/**
  * @}
  */



/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define EMB_CONTROL_NONE0           (1UL << 0) /*!<  */
#define EMB_CONTROL_NONE1           (1UL << 1) /*!<  */
#define EMB_CONTROL_NONE2           (1UL << 2) /*!<  */
#define EMB_CONTROL_NONE3           (1UL << 3) /*!<  */
#define EMB_CONTROL_NONE4           (1UL << 4) /*!<  */
#define EMB_CONTROL_NONE5           (1UL << 5) /*!<  */
#define EMB_CONTROL_NONE6           (1UL << 6) /*!<  */
#define EMB_CONTROL_NONE7           (1UL << 7) /*!<  */
#define EMB_CONTROL_NONE8           (1UL << 8) /*!<  */
#define EMB_CONTROL_NONE9           (1UL << 9) /*!<  */
#define EMB_CONTROL_NONE10          (1UL << 10) /*!< */
#define EMB_CONTROL_NONE11          (1UL << 11) /*!< */
#define EMB_CONTROL_NONE12          (1UL << 12) /*!< */
#define EMB_CONTROL_NONE13          (1UL << 13) /*!<  */
#define EMB_CONTROL_NONE14          (1UL << 14) /*!<  */
#define EMB_CONTROL_Pending         (1UL << 15) /*!< After the data transfer Pending */

typedef union
{
    uint32_t    W;
    uint16_t    H[2];
    uint8_t     B[4];
    struct{
        uint8_t                     :1; ///< bit0
        uint8_t                     :1; ///< bit1
        uint8_t                     :1; ///< bit2
        uint8_t                     :1; ///< bit3
        uint8_t                     :1; ///< bit4
        uint8_t                     :1; ///< bit5
        uint8_t                     :1; ///< bit6
        uint8_t                     :1; ///< bit7
        uint8_t                     :1; ///< bit8
        uint8_t                     :1; ///< bit9
        uint8_t                     :1; ///< bit10
        uint8_t                     :1; ///< bit11
        uint8_t                     :1; ///< bit12
        uint8_t                     :1; ///< bit13
        uint8_t                     :1; ///< bit14
        uint8_t    Pending          :1; ///< bit15 After the data transfer Pending
        uint16_t   Trials           :16; ///< bit16 Master Mode Device Slave NACK, Trials Number. 
    }Bit;
}EMB_ControlTypeDef;

/**
  * @}
  */



/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define EMB_STATE_TRANSFER_INCOMPLETE   (1UL << 1)  /*!< I2C Master/Slave Transmit/Receive incomplete transfer */
//#define EMB_STATE_NONE2                 (1UL << 2)  /*!<  */
//#define EMB_STATE_NONE3                 (1UL << 3)  /*!<  */
//#define EMB_STATE_NONE4                 (1UL << 4)  /*!<  */
//#define EMB_STATE_NONE5                 (1UL << 5)  /*!<  */
//#define EMB_STATE_NONE6                 (1UL << 6)  /*!<  */
//#define EMB_STATE_NONE7                 (1UL << 7)  /*!<  */
//#define EMB_STATE_NONE8                 (1UL << 8)  /*!<  */
//#define EMB_STATE_NONE9                 (1UL << 9)  /*!<  */
//#define EMB_STATE_NONE10                (1UL << 10)  /*!<  */
//#define EMB_STATE_NONE11                (1UL << 11)  /*!<  */
//#define EMB_STATE_NONE12                (1UL << 12)  /*!<  */
//#define EMB_STATE_NONE13                (1UL << 13)  /*!<  */
//#define EMB_STATE_NONE14                (1UL << 14)  /*!<  */
//#define EMB_STATE_NONE15                (1UL << 15)  /*!<  */
//#define EMB_STATE_NONE16                (1UL << 16)  /*!<  */
//#define EMB_STATE_NONE17                (1UL << 17)  /*!<  */
//#define EMB_STATE_NONE18                (1UL << 18)  /*!<  */
//#define EMB_STATE_NONE19                (1UL << 19)  /*!<  */
//#define EMB_STATE_NONE20                (1UL << 20)  /*!<  */
//#define EMB_STATE_NONE21                (1UL << 21)  /*!<  */
//#define EMB_STATE_NONE22                (1UL << 22)  /*!<  */
//#define EMB_STATE_NONE23                (1UL << 23)  /*!<  */
//#define EMB_STATE_NONE24                (1UL << 24)  /*!<  */
//#define EMB_STATE_NONE25                (1UL << 25)  /*!<  */
//#define EMB_STATE_NONE26                (1UL << 26)  /*!<  */
//#define EMB_STATE_NONE27                (1UL << 27)  /*!<  */
//#define EMB_STATE_NONE28                (1UL << 28)  /*!<  */
//#define EMB_STATE_NONE29                (1UL << 29)  /*!<  */
#define EMB_STATE_BUSY                  (1UL << 30)  /*!< Busy */
#define EMB_STATE_INITREADY             (1UL << 31)  /*!<  */

typedef union
{
    uint32_t    W;
    uint16_t    H[2];
    uint8_t     B[4];
    struct{
        uint8_t                     :1; ///< bit0
        uint8_t     Incomplete      :1; ///< bit1
        uint8_t                     :1; ///< bit2
        uint8_t                     :1; ///< bit3
        uint8_t                     :1; ///< bit4
        uint8_t                     :1; ///< bit5
        uint8_t                     :1; ///< bit6
        uint8_t                     :1; ///< bit7
        uint8_t                     :1; ///< bit8
        uint8_t                     :1; ///< bit9
        uint8_t                     :1; ///< bit10
        uint8_t                     :1; ///< bit11
        uint8_t                     :1; ///< bit12
        uint8_t                     :1; ///< bit13
        uint8_t                     :1; ///< bit14
        uint8_t                     :1; ///< bit15
        uint8_t                     :1; ///< bit16
        uint8_t                     :1; ///< bit17
        uint8_t                     :1; ///< bit18
        uint8_t                     :1; ///< bit19
        uint8_t                     :1; ///< bit20
        uint8_t                     :1; ///< bit21
        uint8_t                     :1; ///< bit22
        uint8_t                     :1; ///< bit23
        uint8_t                     :1; ///< bit24 
        uint8_t                     :1; ///< bit25
        uint8_t                     :1; ///< bit26
        uint8_t                     :1; ///< bit27 
        uint8_t                     :1; ///< bit28 
        uint8_t                     :1; ///< bit29 BuffMode: 0=ByteMode/FirmWareMode, 1=8-Bytes Buffer Mode
        uint8_t     Busy            :1; ///< bit30 Busy flag
        uint8_t     InitReady       :1; ///< bit31
    }Bit;
}EMB_StateTypeDef;
/**
  * @}
  */



/** @defgroup HAL_Event_structure_definition HAL state structure definition
  * @brief  HAL Event structure definition  
  * @{
  */
#define EMB_EVENT_RESET                 0UL        /*!< I2C not yet initialized or disabled */
#define EMB_EVENT_TRANSFER_DONE         (1UL << 0)  /*!< I2C Master/Slave Transmit/Receive finished */
#define EMB_EVENT_TRANSFER_INCOMPLETE   (1UL << 1)  /*!< I2C Master/Slave Transmit/Receive incomplete transfer */
//#define EMB_EVENT_                      (1UL << 2)  /*!<  */
//#define EMB_EVENT_                      (1UL << 3)  /*!<  */
//#define EMB_EVENT_                      (1UL << 4)  /*!<  */
//#define EMB_EVENT_                      (1UL << 5)  /*!<  */
//#define EMB_EVENT_                      (1UL << 6)  /*!<  */
//#define EMB_EVENT_                      (1UL << 7)  /*!<  */
//#define EMB_EVENT_                      (1UL << 8)  /*!<  */
//#define EMB_EVENT_                      (1UL << 9)  /*!<  */
//#define EMB_EVENT_                      (1UL << 10) /*!<  */
//#define EMB_EVENT_                      (1UL << 11) /*!<  */
//#define EMB_EVENT_                      (1UL << 12) /*!<  */
//#define EMB_EVENT_                      (1UL << 13) /*!<  */
//#define EMB_EVENT_NONE14                (1UL << 14) /*!<  */
//#define EMB_EVENT_NONE15                (1UL << 15) /*!<  */
//#define EMB_EVENT_NONE16                (1UL << 16) /*!<  */
//#define EMB_EVENT_NONE17                (1UL << 17) /*!<  */
//#define EMB_EVENT_NONE18                (1UL << 18) /*!<  */
//#define EMB_EVENT_NONE19                (1UL << 19) /*!<  */
//#define EMB_EVENT_NONE20                (1UL << 20) /*!<  */
//#define EMB_EVENT_                      (1UL << 21) /*!<  */
//#define EMB_EVENT_                      (1UL << 22) /*!<  */
//#define EMB_EVENT_                      (1UL << 23) /*!<  */
//#define EMB_EVENT_                      (1UL << 24) /*!<  */
//#define EMB_EVENT_                      (1UL << 25) /*!<  */
//#define EMB_EVENT_                      (1UL << 26) /*!<  */
//#define EMB_EVENT_                      (1UL << 27)  /*!<  */
//#define EMB_EVENT_                      (1UL << 28)  /*!<  */
//#define EMB_EVENT_                      (1UL << 29) /*!<  */
#define EMB_EVENT_BUSY                  (1UL << 30) /*!< I2C internal process is ongoing */
#define EMB_EVENT_INITREADY             (1UL << 31) /*!< I2C initialized Ready */
/**
  * @}
  */



/** @defgroup Special_structure_definition Special structure definition
  * @brief  Special structure definition  
  * @{
  */

typedef enum{
    EMB_SPECIAL_NONE = 0,
}EMB_SpecialTypedef;

/**
  * @}
  */



/** @defgroup I2C_handle_Structure_definition I2C handle Structure definition 
  * @brief  I2C handle Structure definition   
  * @{
  */

typedef struct
{

    EMB_Struct              *Instance;      /*!< EMB registers base address */
    __IO EMB_InitTypeDef    Init;           /*!< EMB communication parameters */

    __IO EMB_ControlTypeDef Control;        /*!< EMB communication Control */
    __IO EMB_StateTypeDef   State;          /*!< EMB communication State */
    __IO uint32_t           Event;          /*!< EMB communication Event */

    EMB_SpecialTypedef      Special;
    __IO uint32_t           SpecialNumber;
    __IO uint32_t           SpecialCount;
}EMB_HandleTypeDef;
/**
  * @}
  */



/** @defgroup EMB_Flag_definition EMB Flag definition
  * @brief EMB Interrupt definition
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX  : Interrupt control mask
  * @{
  */ 

#define EMB_FLAG_BUSYF                    EMB_STA_BUSYF_mask_w         /* busy flag */
#define EMB_FLAG_BWEF                     EMB_STA_BWEF_mask_w          /* status event interrupt Flag */
#define EMB_FLAG_WPEF                     EMB_STA_WPEF_mask_w          /* buffer mode event flag. */

/**
  * @}
  */



/** @defgroup EMB_Interrupt_configuration_definition EMB Interrupt configuration definition
  * @brief EMB Interrupt definition
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX  : Interrupt control mask
  * @{
  */

#define EMB_INT_WPE                       EMB_INT_WPE_IE_mask_w
#define EMB_INT_BWE                       EMB_INT_BWE_IE_mask_w
#define EMB_INT_IEA                       EMB_INT_IEA_mask_w

/**
  * @}
  */



/** @brief  Checks whether the specified EMB flag is set or not.
  * @param  __HANDLE__: specifies the EMB Handle.
  *         This parameter can be EMB where x: 1 or 2 to select the I2C peripheral.
  * @param  __FLAG__: specifies the flag to check.
  *         This parameter can be one of the following values:
  *            @arg EMB_FLAG_BUSYF:
  *            @arg EMB_FLAG_BWEF:
  *            @arg EMB_FLAG_WPEF:
  *
  * @retval The new state of __FLAG__ (TRUE or FALSE).
  */
#define __HAL_EMB_GET_FLAG(__HANDLE__, __FLAG__) ((((__HANDLE__)->Instance->STA.W) & (__FLAG__)) == (__FLAG__))
#define __HAL_EMB_CLEAR_FLAG(__HANDLE__, __FLAG__) ((__HANDLE__)->Instance->STA.W = (__FLAG__))



/** @brief  Checks if the specified I2C interrupt source is enabled or disabled.
  * @param  __HANDLE__: specifies the I2C Handle.
  *         This parameter can be I2Cx where x: 1 or 2 to select the I2C peripheral.
  * @param  __INTERRUPT__: specifies the I2C interrupt source to check.
  *         This parameter can be one of the following values:
  *            @arg EMB_INT_WPE:
  *            @arg EMB_INT_BWE:
  *            @arg EMB_INT_IEA:
  *   
  * @retval The new state of __IT__ (TRUE or FALSE).
  */
#define __HAL_EMB_ENABLE_IT(__HANDLE__, __INTERRUPT__)   ((__HANDLE__)->Instance->INT.W |= (__INTERRUPT__))
#define __HAL_EMB_DISABLE_IT(__HANDLE__, __INTERRUPT__)  ((__HANDLE__)->Instance->INT.W &= (~(__INTERRUPT__)))
#define __HAL_EMB_GET_IT_SOURCE(__HANDLE__, __INTERRUPT__) ((((__HANDLE__)->Instance->INT.W & (__INTERRUPT__)) == (__INTERRUPT__)) ? SET : RESET)



#define __HAL_EMB_DMA_ENABLE(__HANDLE__)                     ((__HANDLE__)->Instance->CR0.W |=  EMB_CR0_DMA_EN_mask_w)
#define __HAL_EMB_DMA_DISABLE(__HANDLE__)                    ((__HANDLE__)->Instance->CR0.W &=  ~EMB_CR0_DMA_EN_mask_w)



#define __HAL_EMB_ENABLE(__HANDLE__)                            ((__HANDLE__)->Instance->CR0.W |=  EMB_CR0_EN_mask_w)
#define __HAL_EMB_DISABLE(__HANDLE__)                           ((__HANDLE__)->Instance->CR0.W &=  ~EMB_CR0_EN_mask_w)



void HAL_EMB_IRQHandler(EMB_HandleTypeDef *EMBx);

HAL_StatusTypeDef HAL_EMB_MAD_PPO(EMB_HandleTypeDef *EMBx);
HAL_StatusTypeDef HAL_EMB_MAD_ODO(EMB_HandleTypeDef *EMBx);
HAL_StatusTypeDef HAL_EMB_MAD_DIN(EMB_HandleTypeDef *EMBx);

void HAL_EMB_MspInit(EMB_HandleTypeDef *EMBx);
HAL_StatusTypeDef HAL_EMB_Init(EMB_HandleTypeDef *EMBx);



HAL_StatusTypeDef HAL_EMB_0ALE_8bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx);
HAL_StatusTypeDef HAL_EMB_0ALE_8bit_SRAM_CY7C1362C(EMB_HandleTypeDef *EMBx);



HAL_StatusTypeDef HAL_EMB_0ALE_16bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx);
HAL_StatusTypeDef HAL_EMB_0ALE_16bit_SRAM_CY7C1362C(EMB_HandleTypeDef *EMBx);



HAL_StatusTypeDef HAL_EMB_1ALE_24ADR_16bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx);



HAL_StatusTypeDef HAL_EMB_1ALE_30ADR_16bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx);
HAL_StatusTypeDef HAL_EMB_1ALE_30ADR_16bit_SRAM_CY7C1362C(EMB_HandleTypeDef *EMBx);



HAL_StatusTypeDef HAL_EMB_2ALE_30ADR_16bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx);
HAL_StatusTypeDef HAL_EMB_2ALE_30ADR_16bit_SRAM_CY62167EV30LL_1(EMB_HandleTypeDef *EMBx);
HAL_StatusTypeDef HAL_EMB_2ALE_30ADR_16bit_SRAM_CY7C1632C(EMB_HandleTypeDef *EMBx);


#ifdef __cplusplus
}
#endif

#endif

