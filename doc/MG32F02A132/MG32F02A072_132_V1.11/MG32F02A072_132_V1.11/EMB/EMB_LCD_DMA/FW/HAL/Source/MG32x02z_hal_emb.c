


// <<< Use Configuration Wizard in Context Menu >>>
//  <o0> Select EMB Pin Config

//*** <<< end of configuration section >>>    ***

#define EMB_Pin_Define     12


#include <MG32x02z_HAL_EMB.h>

void HAL_EMB_IRQHandler(EMB_HandleTypeDef *EMBx)
{



}



void HAL_EMB_MspInit(EMB_HandleTypeDef *EMBx)
{
    /* USER CODE BEGIN EMB_MspInit 0 */

    /* USER CODE END EMB_MspInit 0 */

    // GPIO Config

#if EMB_Pin_Define == 11 // WorkStation
    // MADV
    // PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MALE
    PINB(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    // PINE(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MOE
    PINB(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PIND(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    // PINE(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MWE
    PINB(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    // PIND(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MCE
    PINB(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    // PIND(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MCLK
    //X PINC(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PIND(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MBW0
    PINC(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(12)->CR.W = PX_CR_IOM_qb_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MBW1
    PINC(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(13)->CR.W = PX_CR_IOM_qb_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MALE2
    PINE(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINB(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    // PIND(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    // PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MAD[0:7]
    PINB(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MAD[8:15]
    PINC(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MA[0:15]
//    PIND(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PIND(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
#endif

#if EMB_Pin_Define == 12 // 16MA 16MD SRAM ASYNC CY62167EV30LL
 
    // MCLK
    PIND(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MCE
    PINB(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MWE
    PINB(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MOE
    PINB(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PIND(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINE(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MBW0
    PINE(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MBW1
    PINE(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //PINC(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w | PX_CR_LCK_un_locked_w;

    // MALE
    PINB(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //PINC(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w | PX_CR_LCK_un_locked_w;
    PINE(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MALE2
    PIND(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINE(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MAD[0:7]
    PINB(8)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(10)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(12)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(14)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(1)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(3)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(9)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(11)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MAD[8:15]
    PINB(9)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(11)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(13)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(15)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(2)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(8)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(10)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(12)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MA[0:15] Port A Bug MAD[0:15]
    PINA(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
#endif


#if EMB_Pin_Define == 13 // 16MA 16MD SRAM 

    // MCLK
    PIND(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MCE
    PINB(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MWE
    PINB(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MOE
    PINB(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MBW0
    PINE(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(7)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MBW1
    PINE(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MALE
    PINB(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MALE2
    PINE(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MADV
    // PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MRDY
    PINE(15)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_disable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MAD[0:7]
    PINB(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINB(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MAD[8:15]
    PINC(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MA[0:15] Port A Bug MAD[0:15]
//    PINA(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
//    PINA(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
#endif

#if EMB_Pin_Define == 19 // 16MA 16MD SRAM ASYNC CY62167EV30LL
 
    // MCLK
    PIND(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MCE
    PINB(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MWE
    PINB(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MOE
    PINB(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MBW0
    PINE(12)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(7)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MBW1
    PINE(13)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MALE
    PINB(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINE(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MALE2
    PINE(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MADV
    // PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MAD[0:7]
    PIND(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MAD[8:15]
    PINB(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINB(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    PINB(8)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    PINC(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINC(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MA[0:15] Port A Bug MAD[0:15]
    PINA(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
#endif

#if EMB_Pin_Define == 20 // MA0 ~ 15 GPIOA, MAD 0 ~ 7 GPIOD, MAD8~10 GPIOE, MCLK / MCE / MOE / MWE / MALE2 GPIOD, MOE MALE GPIOE

    // MCLK
    //x PINC(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PIND(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MCE
    //xPINB(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //xPINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PIND(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MWE
    //x PINB(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PIND(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MOE
    //x PINB(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PIND(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MBW0
    PINE(12)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(7)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MBW1
    PINE(13)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    // PINC(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MALE
    //x PINB(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    //x PINE(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINE(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MALE2
    //PINE(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PIND(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    // MAD[0:7]
    PIND(7)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(10)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(9)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(8)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(2)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(5)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(4)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;
    PIND(3)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af9_w;

    //MAD[8:15]
    PINC(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINC(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

    // MA[0:15] Port A Bug MAD[0:15]
    PINA(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;
    PINA(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af8_w;

#endif

    // Clock Enable
    __HAL_UNPROTECT_MODULE(CSC);
    CSC->AHB.MBIT.EMB_EN = 1;
    __HAL_PROTECT_MODULE(CSC);

    // SoftWare Trigger HardWare Reset
    __HAL_UNPROTECT_MODULE(RST);
    RST->AHB.MBIT.EMB_EN = 1;
    __NOP();
    RST->AHB.MBIT.EMB_EN = 0;
    __HAL_PROTECT_MODULE(RST);

//    __HAL_EMB_CLEAR_FLAG(EMBx, (EMB_FLAG_BUSYF | EMB_FLAG_BWEF | EMB_FLAG_WPEF));
    EMB->STA.W = (EMB_FLAG_BUSYF | EMB_FLAG_BWEF | EMB_FLAG_WPEF);

    /* Peripheral interrupt init*/
    // NVIC_ClearPendingIRQ(EMB_IRQn);
    // NVIC_SetPriority(EMB_IRQn, 1);
    // NVIC_EnableIRQ(EMB_IRQn);
        
    /* USER CODE BEGIN EMB_MspInit 1 */

    /* USER CODE END EMB_MspInit 1 */
}



HAL_StatusTypeDef HAL_EMB_Init(EMB_HandleTypeDef *EMBx)
{
    /* Check the EMB handle allocation */
    if(EMBx == NULL)
    {
        return HAL_ERROR;
    }

    __HAL_EMB_DISABLE(EMBx);

    __HAL_EMB_DMA_DISABLE(EMBx);
    __HAL_EMB_DISABLE_IT(EMBx, (EMB_INT_WPE | EMB_INT_BWE | EMB_INT_IEA));
    __HAL_EMB_CLEAR_FLAG(EMBx, (EMB_FLAG_BUSYF | EMB_FLAG_BWEF | EMB_FLAG_WPEF));

//    if((EMBx->State.W & EMB_STATE_INITREADY) == 0)
//    {
        /* Init the low level hardware : GPIO, CLOCK, CORTEX...etc */
        HAL_EMB_MspInit(EMBx);
        EMBx->State.Bit.InitReady = 1;
        EMBx->Event = 0;
//    }

    EMBx->Instance->CLK.W = EMBx->Init.Timing.W[0];
    EMBx->Instance->CR0.W = EMBx->Init.OperationMode.W[0];
    EMBx->Instance->CR1.W = EMBx->Init.OperationMode.W[1];
    EMBx->Instance->CR2.W = EMBx->Init.Timing.W[1];

    __HAL_EMB_ENABLE(EMBx);

    EMBx->Event = EMB_EVENT_INITREADY;

    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_0ALE_8bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x00;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 1;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 0;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 0;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 1;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 0;
    EMBx->Init.OperationMode.Bits.DMA_EN = 0;

    EMBx->Init.OperationMode.Bits.CLK_INV = 0;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 0;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 1;
    EMBx->Init.Timing.Bits.ALEH = 0;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_0ALE_8bit_SRAM_CY7C1362C(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x00;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 1;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 0;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 0;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 1;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 0;
    EMBx->Init.OperationMode.Bits.DMA_EN = 0;

    EMBx->Init.OperationMode.Bits.CLK_INV = 0;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 0;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 1;
    EMBx->Init.Timing.Bits.ALEH = 0;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_0ALE_16bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x00;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 0;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 0;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 1;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 1;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 0;
    EMBx->Init.OperationMode.Bits.DMA_EN = 0;

    EMBx->Init.OperationMode.Bits.CLK_INV = 1;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 0;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 1;
    EMBx->Init.Timing.Bits.ALEH = 0;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);
    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_0ALE_16bit_SRAM_CY7C1362C(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));
    GPIOA->OUT.B[1] = 0x00;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 0;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 0;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 1;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 1;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 0;
    EMBx->Init.OperationMode.Bits.DMA_EN = 0;

    EMBx->Init.OperationMode.Bits.CLK_INV = 1;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 0;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 1;
    EMBx->Init.Timing.Bits.ALEH = 0;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);
    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_1ALE_24ADR_16bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x01;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 1;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 0;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 1;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 0;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 1;
    EMBx->Init.OperationMode.Bits.DMA_EN = 1;
    EMBx->Init.OperationMode.Bits.CLK_INV = 0;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 1;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 1;
    EMBx->Init.Timing.Bits.ALEH = 1;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_1ALE_30ADR_16bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x01;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 0;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 0;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 1;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 0;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 2;
    EMBx->Init.OperationMode.Bits.DMA_EN = 1;
    EMBx->Init.OperationMode.Bits.CLK_INV = 1;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 0;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 0;
    EMBx->Init.Timing.Bits.ALEH = 1;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_1ALE_30ADR_16bit_SRAM_CY7C1362C(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x01;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 1;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 0;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 1;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 0;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 2;
    EMBx->Init.OperationMode.Bits.DMA_EN = 1;
    EMBx->Init.OperationMode.Bits.CLK_INV = 1;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 1;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 1;
    EMBx->Init.Timing.Bits.ALEH = 1;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_2ALE_30ADR_16bit_SRAM_CY62167EV30LL(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x02;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 0;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 1;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 1;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 0;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 2;
    EMBx->Init.OperationMode.Bits.DMA_EN = 1;
    EMBx->Init.OperationMode.Bits.CLK_INV = 1;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 0;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 0;
    EMBx->Init.Timing.Bits.ALEH = 1;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_2ALE_30ADR_16bit_SRAM_CY62167EV30LL_1(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x02;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 0;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 1;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 1;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 0;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 2;
    EMBx->Init.OperationMode.Bits.DMA_EN = 1;
    EMBx->Init.OperationMode.Bits.CLK_INV = 1;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 1;
    EMBx->Init.Timing.Bits.ACCH = 1;
    EMBx->Init.Timing.Bits.ACCW = 2;
    EMBx->Init.Timing.Bits.ACCS = 1;
    EMBx->Init.Timing.Bits.ALEH = 1;
    EMBx->Init.Timing.Bits.ALEW = 2;
    EMBx->Init.Timing.Bits.ALES = 1;

    HAL_EMB_Init(EMBx);

    return HAL_OK;
}



HAL_StatusTypeDef HAL_EMB_2ALE_30ADR_16bit_SRAM_CY7C1632C(EMB_HandleTypeDef *EMBx)
{
    //printf("CR0 Addr is 0x%8X \n\r", (uint32_t)&(EMBx->Instance->CR0.W));

    GPIOA->OUT.B[1] = 0x02;

    EMBx->Instance = EMB;
    EMBx->Init.OperationMode.W[0] = 0;
    EMBx->Init.OperationMode.W[1] = 0;
    EMBx->Init.Timing.W[0] = 0;
    EMBx->Init.Timing.W[1] = 0;

    EMBx->Init.OperationMode.Bits.WEN = 1;
    EMBx->Init.OperationMode.Bits.BW_EN = 1;
    EMBx->Init.OperationMode.Bits.ADR_TWO = 1;
    EMBx->Init.OperationMode.Bits.BUS_DSIZE = 1;
    EMBx->Init.OperationMode.Bits.BUS_MDS = 0;
    EMBx->Init.OperationMode.Bits.ADR_SEL = 2;
    EMBx->Init.OperationMode.Bits.DMA_EN = 1;
    EMBx->Init.OperationMode.Bits.CLK_INV = 1;

    EMBx->Init.Timing.Bits.CK_PSC = 0;
    EMBx->Init.Timing.Bits.IDLE = 0;
    EMBx->Init.Timing.Bits.ACCH = 0;
    EMBx->Init.Timing.Bits.ACCW = 1;
    EMBx->Init.Timing.Bits.ACCS = 0;
    EMBx->Init.Timing.Bits.ALEH = 1;
    EMBx->Init.Timing.Bits.ALEW = 0;
    EMBx->Init.Timing.Bits.ALES = 0;

    HAL_EMB_Init(EMBx);

    return HAL_OK;
}


