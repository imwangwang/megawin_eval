/**
 ******************************************************************************
 *
 * @file        Mx92G8z_I2C_MID.C
 *
 * @brief       This is the C code format example file.
 *
 * @par         Project
 *              FA217
 * @version     V0.1
 * @date        2016/04/14
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *#001_CTK_20140116 		// bugNum_Authour_Date 
 *>>Bug1 description 
 *--Bug1 sub-description 
 *--
 *--
 *>>
 *>>
 *#002_WJH_20140119 
 *>>Bug2 description 
 *--Bug2 sub-description 
 *#003_HWT_20140120 
 *>>Bug3 description 
 *
 @endif 
 ******************************************************************************
 */ 

#include "MG32x02z_HAL.h"

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DRV_I2C0_IRQHandler(void)
 * @bug          
 *******************************************************************************
 */
void I2C_ByteMode_STPSTRF_IRQHandler(I2C_HandleTypeDef *I2Cx)
{
    if(__HAL_I2C_GET_FLAG(I2Cx, I2C_FLAG_STPSTRF))
    {
        __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_STPSTRF | I2C_FLAG_STOPF | I2C_FLAG_RSTRF);
        if((I2Cx->State.W & I2C_STATE_BUSY) != 0)
        {
            printf("STPSTRF \n\r");
            if((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) == (I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE))
            {   // FWSR
                if(I2Cx->RxferCount == 0)
                {
                    I2Cx->Control.Bit.RxBuffEmpty = 1;
                    I2Cx->State.W = (I2Cx->State.W & I2C_EVENT_INITREADY) | (I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE);
                    I2Cx->SlaveRecAddr = 0xFF;
                    __HAL_I2C_AA_1();
                }
                else
                {
                    if(I2Cx->RxferCount < I2Cx->RxferLenth)
                        I2Cx->Event |= I2C_EVENT_TRANSFER_INCOMPLETE;

                    // I2Cx->Control.Bit.RxBuffStable = 0;
                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;
                    I2Cx->State.W = (I2Cx->State.W & I2C_EVENT_INITREADY)
                                    | (I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE);
                    __HAL_I2C_AA_1();
                }
            }

            if((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) == (I2C_STATE_DIRECTION_TRAN | I2C_STATE_MASTER_SLAVE))
            {   //FWST
                if(I2Cx->RxferCount == 0)
                {
                    I2Cx->Control.Bit.TxDataReady = 1;
                    I2Cx->State.W = (I2Cx->State.W & I2C_EVENT_INITREADY) | (I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE);
                    I2Cx->SlaveRecAddr = 0xFF;
                    __HAL_I2C_AA_1();
                }
                else
                {
                    if(I2Cx->TxferCount < I2Cx->TxferLenth)
                        I2Cx->Event |= I2C_EVENT_TRANSFER_INCOMPLETE;
                    // I2Cx->Control.Bit.TxBuffStable = 0;
                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;
                    I2Cx->State.W = (I2Cx->State.W & I2C_EVENT_INITREADY)
                                    | (I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE);
                    __HAL_I2C_AA_1();
                }
            }
        }
    }
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DRV_I2C0_IRQHandler(void)
 * @bug          
 *******************************************************************************
 */
void I2C_ByteMode_IRQHandler(I2C_HandleTypeDef *I2Cx)
{
//    __printf("State = 0x%8X",I2Cx->State.W);
//    __printf("Event = 0x%8X",I2Cx->Event);

    // if(__HAL_I2C_GET_FLAG(I2Cx,I2C_FLAG_EVENTF)) /* OLD */
    if(__HAL_I2C_GET_EVENTF(I2Cx) == 0) /* NEW */
    {
        return;
    }

    __HAL_Debug("EVENT Code = 0x%X",__HAL_I2C_EVENT_CODE(I2Cx));
    printf("State = 0x%X\n\r",__HAL_I2C_EVENT_CODE(I2Cx));
    printf("TxLen = %d, TxCnt = %d\n\r", I2Cx->TxferLenth, I2Cx->TxferCount);
    printf("RxLen = %d, RxCnt = %d\n\r", I2Cx->RxferLenth, I2Cx->RxferCount);

    //printf("EVENT Code = 0x%X \n\r",__HAL_I2C_EVENT_CODE(I2Cx));
    if(__HAL_I2C_EVENT_CODE(I2Cx) < 0x60)
    {   /* Mater Transmit & Receive */
        switch(__HAL_I2C_EVENT_CODE(I2Cx))
        {
            case 0x00:  /*!< I2C Bus error */
                I2Cx->State.W |= I2C_STATE_BUS_ERROR;
                I2Cx->Event = I2Cx->State.W; 
                if((I2Cx->State.W & I2C_STATE_DIRECTION) == I2C_STATE_DIRECTION_REC)
                {
                    if(I2Cx->RxferCount < I2Cx->RxferLenth)
                        I2Cx->Event |= I2C_EVENT_TRANSFER_INCOMPLETE;
                }
                else
                {
                    if(I2Cx->TxferCount < I2Cx->TxferLenth)
                        I2Cx->Event |= I2C_EVENT_TRANSFER_INCOMPLETE;
                }
                I2Cx->Event |= I2C_EVENT_TRANSFER_DONE;
                I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;
                __HAL_I2C_STO_1();
                break;

            case 0x08:  /*!< START transmitted */
            case 0x10:  /*!< MT MR, A repeated START command has been transmitted */
                __HAL_I2C_Action_001();
                I2Cx->State.W = ((I2Cx->State.W & (~(I2C_STATE_MASTER))) | (I2C_STATE_BUSY | I2C_STATE_MASTER));
                I2Cx->Instance->SBUF.B[0] = I2Cx->DeviceAddr;
                break;

            case 0x20:  /*!< MT, SLA+W transmitted, no ACK received */
                I2Cx->State.W |= I2C_STATE_ADDR_NACK;
                if(!I2Cx->Control.Bit.AddrNACKNext)
                {
                    __HAL_I2C_STO_1();
                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;
                    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC;
                    break;
                }
            case 0x18:  /*!< MT, SLA+W transmitted, ACK received */
                I2Cx->State.W = ((I2Cx->State.W & (~(I2C_STATE_DIRECTION | I2C_STATE_MASTER))) | (I2C_STATE_MASTER | I2C_STATE_DIRECTION_TRAN | I2C_STATE_BUSY));
                I2Cx->Event = I2Cx->State.W;
                I2Cx->TxferCount = 0;
                goto MT_ACK;

            case 0x30:  /*!< MT, Data byte in I2CDAT has been transmitted, NACK has been received */
                I2Cx->State.W |= I2C_STATE_DATA_NACK;
                if(!I2Cx->Control.Bit.DataNACKNext)
                {
                    __HAL_I2C_STO_1();
                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;
                    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC;
                    break;
                }
                __HAL_Debug("NACK Continue");
            case 0x28:  /*!< MT, Data transmitted, ACK received */
            MT_ACK:
                if(I2Cx->Control.Bit.TXDMAChannel != I2C_CONTROL_TXDMA_DISABLE )
                {
                    if((I2Cx->State.W & I2C_STATE_TXDMABUSY) == 0)
                    {
                        switch(I2Cx->Control.Bit.EndType){
                            case I2C_TRANSFEREND_NEXT:
                                __HAL_I2C_PAction_001();
                                break;
                            case I2C_TRANSFEREND_REPEATSTART:
                                __HAL_I2C_PAction_101();
                                break;
                            case I2C_TRANSFEREND_STOP:
                                __HAL_I2C_PAction_011();
                                break;
                            case I2C_TRANSFEREND_STOPSTART:
                                __HAL_I2C_PAction_111();
                                break;
                        }

                        I2Cx->State.W = ((I2Cx->State.W & (~(I2C_STATE_DIRECTION | I2C_STATE_MASTER))) | (I2C_STATE_MASTER | I2C_STATE_DIRECTION_TRAN | I2C_STATE_TXDMABUSY | I2C_STATE_BUSY));
                        I2Cx->TxferCount = 0;

                        I2Cx->Instance->CR0.W |= I2C_CR0_DMA_TXEN_enable_w;
                        __ISB();
                        return;
                    }

                    if((I2Cx->Instance->CR0.MBIT.DMA_TXEN == 0) && ((I2Cx->State.W & I2C_STATE_TXDMABUSY) != 0))
                    {
                        I2Cx->State.W &= (~I2C_STATE_TXDMABUSY);
                        I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;
                        I2Cx->TxferCount = I2Cx->TxferLenth;
                        I2Cx->Control.Bit.TxDataReady = 0;
                        if (I2Cx->Control.Bit.EndType != I2C_TRANSFEREND_STOP) 
                            goto I2C_FirmWare_Handler_End;
                    }
                }

                if(I2Cx->TxferCount < I2Cx->TxferLenth)
                {
                    I2Cx->Instance->SBUF.B[0] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                    I2Cx->TxferCount ++;
                }
                else
                {
                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;
                    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC;
                    I2Cx->Control.Bit.TxDataReady = 0;

                    switch(I2Cx->Control.Bit.EndType)
                    {
                        case I2C_TRANSFEREND_NEXT:
                            __HAL_Debug("NextTransfer");
                            __HAL_I2C_Action_001();
                            goto I2C_FirmWare_Handler_End;

                        case I2C_TRANSFEREND_REPEATSTART:
                            __HAL_Debug("RePeatStart, 0x%X",I2Cx->Instance->CR2.W);
                            __HAL_I2C_Action_101();
                            goto I2C_FirmWare_Handler_End;

                        case I2C_TRANSFEREND_STOP:
                            __HAL_I2C_Action_011();
                            __HAL_Debug("Stop, 0x%X",I2Cx->Instance->CR2.W);
                            break;

                        case I2C_TRANSFEREND_STOPSTART:
                            __HAL_Debug("StopStart, 0x%X",I2Cx->Instance->CR2.W);
                            __HAL_I2C_Action_111();
                            goto I2C_FirmWare_Handler_End;
                    }
                }
                __HAL_I2C_AA_1();
                break;

            case 0x38:  /*!< MT, Arbitration lost in Address+W or Data byte */
                        /*!< MR, Arbitration lost in Address+R or Data byte */
                        /*!< MR, Arbitration lost in NACK bit */
                I2Cx->State.W |= I2C_STATE_ARBITRATION_LOST;
                I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;
                I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;
                __HAL_I2C_Action_001();
                break;

            case 0x40:  /*!< MR, SLA+R transmitted, ACK received */
                if(I2Cx->Control.Bit.RXDMAChannel != I2C_CONTROL_RXDMA_DISABLE)
                    goto MR_ACK;

                I2Cx->State.W = ((I2Cx->State.W & (~(I2C_STATE_DIRECTION | I2C_STATE_MASTER))) | (I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC | I2C_STATE_BUSY));
                I2Cx->RxferCount = 0;
                I2Cx->Control.Bit.RxBuffEmpty = 0;

                __HAL_I2C_AA_1();
                if((I2Cx->RxferCount >= I2Cx->RxferLenth) && (I2Cx->Control.Bit.EndType != I2C_TRANSFEREND_NEXT))
                    __HAL_I2C_AA_0();

                break;

            case 0x50:  /*!< MR, Data received, ACK returned */
                MR_ACK:
                __HAL_I2C_AA_1();
                if(I2Cx->Control.Bit.RXDMAChannel != I2C_CONTROL_RXDMA_DISABLE )
                {
                    if((I2Cx->State.W & I2C_STATE_RXDMABUSY) == 0)
                    {
                        I2Cx->Control.Bit.RxBuffEmpty = 0;
                        
                        switch(I2Cx->Control.Bit.EndType){
                            case I2C_TRANSFEREND_NEXT:
                                __HAL_I2C_PAction_001();
                                break;
                            case I2C_TRANSFEREND_REPEATSTART:
                                __HAL_I2C_PAction_101();
                                break;
                            case I2C_TRANSFEREND_STOP:
                                __HAL_I2C_PAction_011();
                                break;
                            case I2C_TRANSFEREND_STOPSTART:
                                __HAL_I2C_PAction_111();
                                break;
                        }

                        I2Cx->State.W = ((I2Cx->State.W & (~(I2C_STATE_DIRECTION | I2C_STATE_MASTER))) | (I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC | I2C_STATE_RXDMABUSY | I2C_STATE_BUSY));
                        I2Cx->Event = I2Cx->State.W;
                        I2Cx->RxferCount = 0;
                        
                        I2Cx->Instance->CR0.W |= I2C_CR0_DMA_RXEN_enable_w;
                        __ISB();
                        return;
                    }

                    if((I2Cx->Instance->CR0.MBIT.DMA_RXEN == 0) && ((I2Cx->State.W & I2C_STATE_RXDMABUSY) != 0))
                    {
                        I2Cx->State.W &= (~I2C_STATE_RXDMABUSY);
                        I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;
                        I2Cx->RxferCount = I2Cx->RxferLenth;
                        I2Cx->Control.Bit.RxBuffEmpty = 0;
                        if (I2Cx->Control.Bit.EndType != I2C_TRANSFEREND_STOP) 
                            goto I2C_FirmWare_Handler_End;
                    }
                }

                I2Cx->Control.Bit.RxBuffEmpty = 0;
                I2Cx->pRxBuffPtr[I2Cx->RxferCount] = I2Cx->Instance->SBUF.B[0];
                I2Cx->RxferCount ++;
                if((I2Cx->RxferCount >= (I2Cx->RxferLenth - 1)) && (I2Cx->Control.Bit.EndType != I2C_TRANSFEREND_NEXT) )
                    __HAL_I2C_AA_0();
                break;

            case 0x48:  /*!< MR, SLA+R transmitted, no ACK received */
                I2Cx->State.W = (I2Cx->State.W & (~(I2C_STATE_DIRECTION | I2C_STATE_MASTER))) 
                                | (I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC | I2C_STATE_ADDR_NACK);
                goto FMMR_END;
            case 0x58:  /*!< MR, Data received, no ACK returned */
                I2Cx->State.W = (I2Cx->State.W & (~(I2C_STATE_DIRECTION | I2C_STATE_MASTER))) 
                                | (I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC | I2C_STATE_DATA_NACK); 
                I2Cx->Control.Bit.RxBuffEmpty = 0;
                I2Cx->pRxBuffPtr[I2Cx->RxferCount] = I2Cx->Instance->SBUF.B[0];
                I2Cx->RxferCount ++;
            FMMR_END:
                I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;
                if(I2Cx->RxferCount < I2Cx->RxferLenth)
                    I2Cx->Event |= I2C_EVENT_TRANSFER_INCOMPLETE;

                I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;

                switch(I2Cx->Control.Bit.EndType)
                {
                    case I2C_TRANSFEREND_NEXT:
                        __HAL_Debug("NextTransfer");
                        __HAL_I2C_Action_001();
                        goto I2C_FirmWare_Handler_End;

                    case I2C_TRANSFEREND_REPEATSTART:
                        __HAL_Debug("RePeatStart, 0x%X",I2Cx->Instance->CR2.W);
                        __HAL_I2C_Action_101();
                        goto I2C_FirmWare_Handler_End;

                    case I2C_TRANSFEREND_STOP:
                        __HAL_I2C_Action_011();
                        __HAL_Debug("Stop, 0x%X",I2Cx->Instance->CR2.W);
                        break;

                    case I2C_TRANSFEREND_STOPSTART:
                        __HAL_Debug("StopStart, 0x%X",I2Cx->Instance->CR2.W);
                        __HAL_I2C_Action_111();
                        goto I2C_FirmWare_Handler_End;
                }
                break;

            default:
                break;
        }
    }
    else
    {   /* Slave Transmit & Receive */
        switch(__HAL_I2C_EVENT_CODE(I2Cx))
        {
            case 0x60:  /*!< SR, Own Address+W has been received, ACK has been returned */
            case 0x68:  /*!< SR, Arbitration lost in Address+RorW as master, Own Address+W has been received, ACK has been returned */
            case 0x70:  /*!< FA217 SRGC, General Call Address+W has been received, ACK has been returned */
            case 0x78:  /*!< FA217 SRGC, General Call Address+W has been received, NACK has been returned */
                if((!I2Cx->Control.Bit.RxBuffEmpty) || ((I2Cx->State.W & (I2C_STATE_MASTER | I2C_STATE_DIRECTION)) != (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC)) )
                {
                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;
                    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_RXBUFFUNSTABLE | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);
                    I2Cx->NextSlaveAddr = I2Cx->Instance->SBUF.B[0];
    
                    printf("FWSR. Buffer UnStable = 0x%X \n\r",I2Cx->SlaveRecAddr);
                    goto I2C_FirmWare_Handler_End;
                }
                I2Cx->State.W = (I2Cx->State.W & (~(I2C_STATE_MASTER | I2C_STATE_DIRECTION | I2C_STATE_RXBUFFUNSTABLE)))
                                | (I2C_STATE_BUSY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);

                if((__HAL_I2C_EVENT_CODE(I2Cx) == 0x68) || (__HAL_I2C_EVENT_CODE(I2Cx) == 0x78))
                    I2Cx->State.W |= I2C_STATE_ARBITRATION_LOST;

                if((__HAL_I2C_EVENT_CODE(I2Cx) == 0x70) || (__HAL_I2C_EVENT_CODE(I2Cx) == 0x78))
                    I2Cx->State.W |= I2C_STATE_GENERALCALL;

                I2Cx->NextSlaveAddr = 0xFF;
                I2Cx->SlaveRecAddr = (I2Cx->Instance->SBUF.B[0] & 0XFE);
                I2Cx->RxferCount = 0;

                if(I2Cx->Control.Bit.RXDMAChannel != I2C_CONTROL_RXDMA_DISABLE)
                {
                    __HAL_I2C_AA_1();
                    // __HAL_I2C_Action_001();
                    // I2Cx->Instance->CR0.MBIT.DMA_RXEN = 1;
                    __HAL_I2C_DMA_RX_ENABLE(I2Cx);
                    __HAL_PAA_0();
                    // __HAL_I2C_PAction_000();
                    goto I2C_FirmWare_Handler_End;
                }

                __HAL_I2C_AA_1();
                if(I2Cx->RxferCount >= I2Cx->RxferLenth)
                    __HAL_I2C_AA_0();
                
                break;

            case 0x80:  /*!< SR, Previously addressed with Own Address, Data byte has been received, ACK has been returned */
            case 0x90:  /*!< FA217 SRGC, Previously addressed with General Call Address, Data byte has been received, ACK has been returned */
                I2Cx->Control.Bit.RxBuffEmpty = 0;   /* Received Data 0=UnStable, 1=Stable */
                I2Cx->pRxBuffPtr[I2Cx->RxferCount] = I2Cx->Instance->SBUF.B[0];
                I2Cx->RxferCount ++;

                __HAL_I2C_AA_1();
                if(I2Cx->RxferCount >= I2Cx->RxferLenth)
                    __HAL_I2C_AA_0();

//#if !WorkStation && C1Chip
//                /* Bug Data and Clock Sync Release, may be Generate is STOP Signal. */
//                if(I2Cx->Instance == I2C0)
//                {
//                    GPIOB->SCR2.MBIT.SC10 = 0;
//                    PINB(10)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
//                }
//                else if(I2Cx->Instance == I2C1)
//                {
//                    GPIOC->SCR2.MBIT.SC10 = 0;
//                    PINC(10)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
//                }
//                __NOP();
//                __NOP();
//                __NOP();
//                __NOP();
//                __NOP();
//                __NOP();
//                __NOP();
//                __NOP();
//#endif
                break;

            case 0x88: /*!< SR, Data received , no ACK returned */
            case 0x98: /*!< SRGC, Data received General call, no ACK returned */
                I2Cx->Control.Bit.RxBuffEmpty = 0;   /* Received Data 0=UnStable, 1=Stable */
                I2Cx->pRxBuffPtr[I2Cx->RxferCount] = I2Cx->Instance->SBUF.B[0];
                I2Cx->RxferCount ++;
                goto SlaveReceive_End;

            case 0xA0: /*!< SR, STOP / ReStart received while addressed */
                if((I2Cx->Control.Bit.RXDMAChannel != I2C_CONTROL_RXDMA_DISABLE) && (I2Cx->Instance->CR0.MBIT.DMA_RXEN))
                {
                    DMA_CHx_Struct *DMA_CHx_Tmp;

                    __HAL_I2C_DMA_RX_DISABLE(I2Cx);

                    // Check DMA Channel
                    if(__DMA_Get_Channel_SRC(DMA_CH0) == DMA_CHx_SRC_I2C0)
                        DMA_CHx_Tmp = DMA_CH0; 

                    if(__DMA_Get_Channel_SRC(DMA_CH1) == DMA_CHx_SRC_I2C0)
                        DMA_CHx_Tmp = DMA_CH0; 

                    if(__DMA_Get_Channel_SRC(DMA_CH2) == DMA_CHx_SRC_I2C0)
                        DMA_CHx_Tmp = DMA_CH0; 

                    I2Cx->RxferCount = __DMA_Get_CHx_Data_Lenth(DMA_CHx_Tmp) - __DMA_Get_CHx_Data_Count(DMA_CHx_Tmp) - 1;

                    __DMA_Channel_Request_Disable(DMA_CHx_Tmp);
                    __DMA_Channel_Disable(DMA_CHx_Tmp);

                    // Check DMA Channel Count
                    if(I2Cx->RxferCount == 0)
                    {
                        __DMA_Channel_Enable(DMA_CHx_Tmp);
                        __DMA_Channel_Request_Enable(DMA_CHx_Tmp);
                        __HAL_I2C_DMA_RX_ENABLE(I2Cx);
                    }                         
                }

                // if((I2Cx->RxferCount == 0) && ((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) == (I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE)))
                if(I2Cx->RxferCount == 0)                    {
                    __HAL_Debug("SR_A0_0");
                    I2Cx->Control.Bit.RxBuffEmpty = 1;
                    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);
                    I2Cx->SlaveRecAddr = 0xFF;
                    __HAL_I2C_AA_1();
                    break;
                }

            SlaveReceive_End:
                I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;
                if(I2Cx->RxferCount < I2Cx->RxferLenth)
                {
                    I2Cx->Event |= I2C_EVENT_TRANSFER_INCOMPLETE;
                }
                goto Slave_End;

            case 0xB0:  /*!< ST, Arbitration lost SLA+R */
                    I2Cx->State.W |= I2C_STATE_ARBITRATION_LOST;
            case 0xA8:  /*!< ST, SLA+R received, ACK returned */
                if(((I2Cx->Control.Bit.TXDMAChannel != I2C_CONTROL_TXDMA_DISABLE) && ((I2Cx->State.W & I2C_STATE_TXDMABUSY) != 0)) ||
                   ((I2Cx->Control.Bit.TXDMAChannel != I2C_CONTROL_RXDMA_DISABLE) && ((I2Cx->State.W & I2C_STATE_RXDMABUSY) != 0)) ||
                   ((I2Cx->Control.Bit.TXDMAChannel == I2C_CONTROL_TXDMA_DISABLE) && (((I2Cx->State.W & I2C_STATE_BUSY) != 0) || ((I2Cx->Instance->STA.W & I2C_FLAG_BUSYF) != 0))))
                {
                    I2Cx->State.W &= ~(I2C_STATE_TXDMABUSY | I2C_STATE_RXDMABUSY);
                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;
                    I2Cx->Control.Bit.TxDataReady = 0;

                    I2Cx->NextSlaveAddr = I2Cx->Instance->SAC.B[0];
                    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_TXBUFFUNSTABLE | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_TRAN);
                    printf("FWST. Buffer UnStable, Next Address = 0x%X \n\r",I2Cx->SlaveRecAddr);
                    goto I2C_FirmWare_Handler_End;
                }

                I2Cx->SlaveTraAddr = I2Cx->Instance->SBUF.B[0];
                I2Cx->TxferCount = 0;

                I2Cx->State.W = ((I2Cx->State.W & (~(I2C_STATE_MASTER | I2C_STATE_DIRECTION )))
                              | (I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_TRAN));

                if (I2Cx->Control.Bit.TxDataReady == 0)
                {
                    I2Cx->State.W |= I2C_STATE_TXBUFFUNSTABLE;
                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;
                    printf("FWST. Buffer UnStable, Next Address = 0x%X \n\r",I2Cx->SlaveRecAddr);
                    goto I2C_FirmWare_Handler_End;
                }

            case 0xB8:  /*!< ST, Data transmitted, ACK received */
                if(I2Cx->Control.Bit.TXDMAChannel != I2C_CONTROL_TXDMA_DISABLE)
                {
                    if((I2Cx->State.W & I2C_STATE_TXDMABUSY) == 0)
                    {
                        switch(I2Cx->Control.Bit.EndType){
                            case I2C_TRANSFEREND_NEXT:
                                __HAL_I2C_PAction_001();
                                break;
                            case I2C_TRANSFEREND_REPEATSTART:
                                __HAL_I2C_PAction_100();
                                break;
                            case I2C_TRANSFEREND_STOP:
                                __HAL_I2C_PAction_010();
                                break;
                            case I2C_TRANSFEREND_STOPSTART:
                                __HAL_I2C_PAction_110();
                                break;
                        }

                        I2Cx->TxferCount = 0;
                        I2Cx->State.W |= I2C_STATE_TXDMABUSY;
                        I2Cx->Instance->CR0.W |= I2C_CR0_DMA_TXEN_enable_w;
                        __ISB();
                        return;
                    }
                    //if(((I2Cx->State.W & I2C_STATE_TXDMABUSY) != 0) && (I2Cx->Instance->CR0.MBIT.DMA_TXEN == 0))
                    else
                    {
                        switch(I2Cx->Control.Bit.TXDMAChannel)
                        { 
                            case I2C_CONTROL_TXDMACHANNEL0:
                                I2Cx->TxferCount = DMA_CH0->CHxCNT.H[0];
                                break;
                            case I2C_CONTROL_TXDMACHANNEL1:
                                I2Cx->TxferCount = DMA_CH1->CHxCNT.H[0];
                                break;
                            case I2C_CONTROL_TXDMACHANNEL2:
                                I2Cx->TxferCount = DMA_CH2->CHxCNT.H[0];
                                break;
                        }
                        I2Cx->TxferCount = (I2Cx->TxferLenth - I2Cx->TxferCount);
                        if(I2Cx->TxferCount != 0)
                            I2Cx->State.W |= I2C_EVENT_TRANSFER_INCOMPLETE;

                        I2Cx->State.W &= (~I2C_STATE_TXDMABUSY);
                        I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;
                        I2Cx->Control.Bit.TxDataReady = 0;

                        if((I2Cx->TxferCount ==0) && (I2Cx->Control.Bit.EndType == I2C_TRANSFEREND_NEXT))
                            goto I2C_FirmWare_Handler_End;
                        else
                            __HAL_I2C_AA_1();
                        break;
                    }
                }

                I2Cx->Instance->SBUF.B[0] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                I2Cx->TxferCount ++;
                __HAL_I2C_AA_1();
                if((I2Cx->TxferCount >= I2Cx->TxferLenth) && (I2Cx->Control.Bit.EndType != I2C_TRANSFEREND_NEXT)) 
                    __HAL_I2C_AA_0();
                break;

            case 0xC8: /*!< ST, Last data transmitted, ACK received */
                I2Cx->State.W |= I2C_STATE_DATA_NACK;

            case 0xC0: /*!< ST, Data transmitted, no ACK received */
                I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;

                if(I2Cx->TxferCount < I2Cx->TxferLenth)
                    I2Cx->Event |= I2C_EVENT_TRANSFER_INCOMPLETE;

            Slave_End:
                I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);
                __HAL_I2C_AA_1();
                break;

            case 0xF8:  /*!< MT MR ST SR, Reset or STOP command */
                if((I2Cx->RxferCount == 0) && ((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) == (I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE)))
                {
                    __HAL_Debug("SR_A0_0");
                    I2Cx->Control.Bit.RxBuffEmpty = 1;
                    // I2Cx->State.W = (I2Cx->State.W & I2C_EVENT_INITREADY) | (I2C_STATE_DIRECTION_REC | I2C_STATE_MASTER_SLAVE);
                    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);
                    I2Cx->SlaveRecAddr = 0xFF;
                }
                __HAL_I2C_AA_1();
                break;

            default:
                break;
        }
    }
    // __HAL_I2C_CLEAR_FLAG(I2Cx,I2C_FLAG_EVENTF); /* OLD */
    __HAL_I2C_CLEAR_EVENTF(I2Cx); /* NEW */

    /* Bug Data and Clock Sync Release, may be Generate is STOP Signal. */
//#if !WorkStation && C1Chip
//    PINB(10)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af2_w;
////    PINC(10)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af2_w;
//#endif
    __HAL_Debug("Event End");
    return;

I2C_FirmWare_Handler_End:
    if(I2Cx->Instance == I2C0)
        NVIC_DisableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_DisableIRQ (I2Cx_IRQn);

    __HAL_Debug("Reserved IRQ End");
}


/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DRV_I2C0_IRQHandler(void)
 * @bug          
 *******************************************************************************
 */
void I2C_Error_IRQHandler(I2C_HandleTypeDef *I2Cx)
{
    // if(I2Cx->HW_State_Reg & I2C_STA_ERRF_mask_w)
    if(__HAL_I2C_GET_FLAG(I2Cx,I2C_FLAG_ERRF))
    {
        // I2Cx->HW_State_Reg &= ~I2C_STA_ERRF_mask_w;
        __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_ERRF);
        __HAL_Debug("ERRF");

        // if(I2Cx->HW_State_Reg & I2C_STA_ALOSF_mask_w)
        if(I2Cx->Instance->STA.W & I2C_STA_ALOSF_mask_w)
        {
            I2Cx->State.Bit.ArbitrationLost = 1;
            I2Cx->Event |= I2C_EVENT_ARBITRATION_LOST;
            __HAL_Debug("ALOSF");

            /* Clear ALOSF Flag */
            __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_ALOSF);
#if WorkStation
            printf("ALOSF END");
#endif
        }

        // if(I2Cx->HW_State_Reg & I2C_STA_BERRF_mask_w)
        if((I2Cx->Instance->STA.W & I2C_STA_BERRF_mask_w) != 0)
        {
            I2Cx->State.Bit.BuffMode = 1;
            __HAL_Debug("BERRF");

            /* Clear BERRF Flag */
            __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_BERRF);
        }

        // if(I2Cx->HW_State_Reg & I2C_STA_NACKF_mask_w)
        if((I2Cx->Instance->STA.W & I2C_STA_NACKF_mask_w) != 0)
        {
            if((I2Cx->Instance->STA.MBIT.MSTF) && (I2Cx->Instance->STA.MBIT.SADRF))
            {
                I2Cx->State.Bit.AddrNACK = 1;
                __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_SADRF);
            }
            else
            {
                I2Cx->State.Bit.DataNACK = 1;
            }
            __HAL_Debug("NACKF");

            /* Clear NACKF Flag */
            __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_NACKF);
        }

        // if(I2Cx->HW_State_Reg & I2C_STA_TOVRF_mask_w)
        if((I2Cx->Instance->STA.W & I2C_STA_TOVRF_mask_w) != 0)
        {
            I2Cx->State.Bit.STxOVR = 1;
            
            __HAL_Debug("TOVRF");

            /* Clear TOVRF Flag */
            __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_TOVRF);
        }

        // if(I2Cx->HW_State_Reg & I2C_STA_ROVRF_mask_w)
        if((I2Cx->Instance->STA.W & I2C_STA_ROVRF_mask_w) != 0)
        {
            I2Cx->State.Bit.SRxOVR = 1;
            __HAL_Debug("ROVRF");

            /* Clear ROVRF Flag */
            __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_ROVRF);
        }

        I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;

        __HAL_I2C_DISABLE_IT(I2Cx, I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA);
        __HAL_Debug("End ERROR");
    }
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DRV_I2C0_IRQHandler(void)
 * @bug          
 *******************************************************************************
 */
void I2C_BUFF_IRQHandler(I2C_HandleTypeDef *I2Cx)
{
    union{
        uint32_t W;
        uint8_t B[4];
    }lTranDataTemp;
    uint8_t lI2C_DAT_CNT = 0;
    uint8_t lI2C_DAT_DIFF = 0;

    if(__HAL_I2C_GET_FLAG(I2Cx,I2C_FLAG_BUFF))
    {
        __HAL_Debug("BUFF");

        //if(((I2Cx->Instance->STA.W & I2C_STA_SADRF_mask_w) != 0) && ((I2Cx->State.W & I2C_STATE_BUSY) != I2C_STATE_BUSY))
        if((I2Cx->Instance->STA.W & I2C_STA_SADRF_mask_w) != 0)
        {
            __HAL_Debug("SADRF");
            if(I2Cx->Instance->STA.MBIT.SLAF && ((I2Cx->State.W & I2C_STATE_BUSY) == 0))
            {   // Slave Mode
                __HAL_Debug("Slave");
                if(I2Cx->Instance->STA.MBIT.RWF)
                {   // Transmit Mode
                    __HAL_Debug("ST");
                    if((!I2Cx->Control.Bit.TxDataReady) || ((I2Cx->SlaveTraAddr & 0xFE) != (I2Cx->Instance->SAC.B[0] & 0xFE)))
                    {
                        I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;
                        I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_BUFFMODE | I2C_STATE_RXBUFFUNSTABLE | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_TRAN);
                        I2Cx->NextSlaveAddr = I2Cx->Instance->SAC.B[0];

                        if(I2Cx->Instance == I2C0)
                            NVIC_DisableIRQ (I2C0_IRQn);
                        else if (I2Cx->Instance == I2C1)
                            NVIC_DisableIRQ (I2Cx_IRQn);

                        printf("Slave Tx Buffer UnStable, I2C_Control = 0x%X, I2C_SlaTranAddr = 0x%X, I2C_SAC = 0x%X \n\r", I2Cx->Control.W, I2Cx->SlaveTraAddr, I2Cx->Instance->SAC.B[0]);
                        goto I2C_BuffMode_Handler_End;
                    }
                    I2Cx->TxferCount = 0;
                    I2Cx->State.W = ((I2Cx->State.W & (~(I2C_STATE_MASTER | I2C_STATE_DIRECTION | I2C_STATE_RXBUFFUNSTABLE)))|(I2C_STATE_INITREADY | I2C_STATE_BUFFMODE | I2C_STATE_BUSY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_TRAN));
                    if(I2Cx->TxferCount < I2Cx->TxferLenth)
                    {
                        lI2C_DAT_CNT = 0;
                    #if BM_MR_MT_ST_DataByte == 0 || BM_MR_MT_ST_DataByte >= 4
                        do{
                            lTranDataTemp.B[lI2C_DAT_CNT] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                            lI2C_DAT_CNT ++;
                            /* Suggest 1.Write I2C_BUF_CNT "and" Data Buff Trigger Operation */
                            /*         2.First Write I2C_DAT, I2C_STA_TXF 1 >> 0 >> 1, and Trigger I2C_STA_BUFF to "1" */
                            I2Cx->TxferCount ++;
                        }while((lI2C_DAT_CNT < 4) && (I2Cx->TxferCount < I2Cx->TxferLenth));

                        __HAL_PAA_1();
                        if(I2Cx->TxferLenth <= 4)
                            __HAL_PAA_0();
                    #endif

                    #if BM_MR_MT_ST_DataByte == 1
                        lTranDataTemp.B[lI2C_DAT_CNT] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                        lI2C_DAT_CNT ++;
                        I2Cx->TxferCount ++;

                        PAA_1
                        if(I2Cx->TxferLenth <= 1)
                            __HAL_PAA_0();
                    #endif

                    #if BM_MR_MT_ST_DataByte == 2
                        do{
                            lTranDataTemp.B[lI2C_DAT_CNT] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                            lI2C_DAT_CNT ++;
                            I2Cx->TxferCount ++;
                        }while((lI2C_DAT_CNT < 2) && (I2Cx->TxferCount < I2Cx->TxferLenth));

                        PAA_1
                        if(I2Cx->TxferLenth <= 2)
                            __HAL_PAA_0();
                    #endif

                    #if BM_MR_MT_ST_DataByte == 3
                        do{
                            lTranDataTemp.B[lI2C_DAT_CNT] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                            lI2C_DAT_CNT ++;
                            I2Cx->TxferCount ++;
                        }while((lI2C_DAT_CNT < 3) && (I2Cx->TxferCount < I2Cx->TxferLenth));

                        PAA_1
                        if(I2Cx->TxferLenth <= 3)
                            __HAL_PAA_0();
                    #endif

                        I2Cx->Instance->CR2.MBIT.BUF_CNT = lI2C_DAT_CNT;  /*!< Output Data */
                        I2Cx->Instance->DAT.W = lTranDataTemp.W;
                        /* TXF Auto Clear By Write I2C_DAT */
                        __HAL_Debug("Write Buff and Data Count");
                    }
                }
                else
                {   // Received mode
                    __HAL_Debug("SR");
                    if(!I2Cx->Control.Bit.RxBuffEmpty)
                    {
                        I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE | I2C_EVENT_TRANSFER_INCOMPLETE;
                        I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_BUFFMODE | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);
                        I2Cx->NextSlaveAddr = I2Cx->Instance->SAC.B[0];

                        if(I2Cx->Instance == I2C0)
                            NVIC_DisableIRQ (I2C0_IRQn);
                        else if (I2Cx->Instance == I2C1)
                            NVIC_DisableIRQ (I2Cx_IRQn);

                        printf("Slave Rx Buffer UnStable, I2C_Control = 0x%X, I2C_SlaRecAddr = 0x%X, I2C_SAC = 0x%X \n\r", I2Cx->Control.W, I2Cx->SlaveRecAddr, I2Cx->Instance->SAC.B[0]);
                        goto I2C_BuffMode_Handler_End;
                    }
                    I2Cx->SlaveRecAddr = I2Cx->Instance->SAC.B[0];
                    I2Cx->RxferCount = 0;
                    I2Cx->State.W = ((I2Cx->State.W & (~(I2C_STATE_MASTER | I2C_STATE_DIRECTION)))|(I2C_STATE_INITREADY | I2C_STATE_BUFFMODE | I2C_STATE_BUSY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC));
                }
            }

            // Master Receive Mode
            // if((I2Cx->Instance->STA.MBIT.MSTF) && (I2Cx->Instance->STA.MBIT.RWF)
            // if((((I2Cx->Instance->STA.W & (I2C_STA_MSTF_mask_w | I2C_STA_RWF_mask_w)) == (I2C_STA_MSTF_mask_w | I2C_STA_RWF_mask_w))) && ((I2Cx->State.W & (I2C_STATE_MASTER | I2C_STATE_DIRECTION)) == (I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC)))
            if(((I2Cx->Instance->STA.W & (I2C_STA_MSTF_mask_w | I2C_STA_RWF_mask_w)) == (I2C_STA_MSTF_mask_w | I2C_STA_RWF_mask_w)))
            {   // I2Cx->HW_State_Reg &= ~I2C_STA_BUFF_mask_w;
                __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_BUFF);
                I2Cx->State.W = ((I2Cx->State.W &(~(I2C_STATE_MASTER | I2C_STATE_DIRECTION))) | (I2C_STATE_INITREADY | I2C_STATE_BUFFMODE | I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC | I2C_STATE_BUSY));
                __HAL_Debug("MR");
                if(I2Cx->RxShadowCount < I2Cx->RxferLenth)
                {
                    do{
                        __HAL_Debug("Buffer Count");

                    #if BM_MR_MT_ST_DataByte == 0 || BM_MR_MT_ST_DataByte >= 4
                        if((I2Cx->RxferLenth - I2Cx->RxShadowCount) > 4)
                        {
                            I2Cx->Instance->CR2.MBIT.BUF_CNT = 4;
                            I2Cx->RxShadowCount += 4;
                        }
                    #endif
                    #if BM_MR_MT_ST_DataByte == 1
                        if((I2Cx->RxferLenth - I2Cx->RxShadowCount) > 1)
                        {
                            I2Cx->Instance->CR2.MBIT.BUF_CNT = 1;
                            I2Cx->RxShadowCount += 1;
                        }
                    #endif
                    #if BM_MR_MT_ST_DataByte == 2
                        if((I2Cx->RxferLenth - I2Cx->RxShadowCount) > 2)
                        {
                            I2Cx->Instance->CR2.MBIT.BUF_CNT = 2;
                            I2Cx->RxShadowCount += 2;
                        }
                    #endif
                    #if BM_MR_MT_ST_DataByte == 3
                        if((I2Cx->RxferLenth - I2Cx->RxShadowCount) > 3)
                        {
                            I2Cx->Instance->CR2.MBIT.BUF_CNT = 3;
                            I2Cx->RxShadowCount += 3;
                        }
                    #endif
                        else
                        {
                            __HAL_PAA_0();
                            I2Cx->Instance->CR2.MBIT.BUF_CNT = (I2Cx->RxferLenth - I2Cx->RxShadowCount);
                            I2Cx->RxShadowCount += (I2Cx->RxferLenth - I2Cx->RxShadowCount);
                            switch(I2Cx->Control.Bit.EndType)
                            {
                                case 0:
                                    __HAL_PSTO_1();
                                    __HAL_Debug("Stop");
                                    break;

                                case 1:
                                    __HAL_PSTA_1();
                                    __HAL_Debug("RePeatStart");
                                    break;

                                case 2:
                                    __HAL_I2C_PAction_110();
                                    __HAL_Debug("PreStopStart");
                                    break;
                                
                                default:
                                    __HAL_Debug("NextTransfer");
                                    break;
                            }
                        }
                    }while((I2Cx->Instance->STA.MBIT.CNTF) && (I2Cx->RxShadowCount < I2Cx->RxferLenth));
                }
            }
            // I2Cx->HW_State_Reg &= ~(I2C_STA_BUFF_mask_w | I2C_STA_SADRF_mask_w);
            __HAL_Debug("Clear SADRF\n\r");
            __HAL_I2C_CLEAR_FLAG(I2Cx, (I2C_FLAG_BUFF | I2C_FLAG_SADRF));
I2C_BuffMode_Handler_End:
        __NOP();
        }

        // if(I2Cx->HW_State_Reg & I2C_STA_RXF_mask_w)
        if((I2Cx->Instance->STA.W & I2C_STA_RXF_mask_w) && ((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_BUSY)) == (I2C_STATE_DIRECTION_REC | I2C_STATE_BUSY)))
        {
            // I2Cx->HW_State_Reg &= ~I2C_STA_BUFF_mask_w;
            __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_BUFF);
            __HAL_Debug("RXF");

            if((I2Cx->Instance->STA.W & (I2C_STA_MSTF_mask_w | I2C_STA_RWF_mask_w)) == (I2C_STA_MSTF_mask_w | I2C_STA_RWF_mask_w))
            {
                __HAL_Debug("1");
                if((I2Cx->RxShadowCount < I2Cx->RxferLenth ) && I2Cx->Instance->STA.MBIT.CNTF)
                {
                    __HAL_Debug("2");
                #if BM_MR_MT_ST_DataByte == 0 || BM_MR_MT_ST_DataByte >= 4
                    if((I2Cx->RxferLenth - I2Cx->RxShadowCount) > 4)
                    {
                        __HAL_Debug("3");
                        I2Cx->Instance->CR2.MBIT.BUF_CNT = 4;
                        I2Cx->RxShadowCount += 4;
                    }
                #endif
                #if BM_MR_MT_ST_DataByte == 1
                    if((I2Cx->RxferLenth - I2Cx->RxShadowCount) > 1)
                    {
                        __HAL_Debug("3");
                        I2Cx->Instance->CR2.MBIT.BUF_CNT = 1;
                        I2Cx->RxShadowCount += 1;
                    }
                #endif
                #if BM_MR_MT_ST_DataByte == 2
                    if((I2Cx->RxferLenth - I2Cx->RxShadowCount) > 2)
                    {
                        __HAL_Debug("3");
                        I2Cx->Instance->CR2.MBIT.BUF_CNT = 2;
                        I2Cx->RxShadowCount += 2;
                    }
                #endif
                #if BM_MR_MT_ST_DataByte == 3
                    if((I2Cx->RxferLenth - I2Cx->RxShadowCount) > 3)
                    {
                        __HAL_Debug("3");
                        I2Cx->Instance->CR2.MBIT.BUF_CNT = 3;
                        I2Cx->RxShadowCount += 3;
                    }
                #endif

                    else
                    {
                        __HAL_PAA_0();
                        __HAL_Debug("4");
                        I2Cx->Instance->CR2.MBIT.BUF_CNT = (I2Cx->RxferLenth - I2Cx->RxShadowCount);
                        I2Cx->RxShadowCount += (I2Cx->RxferLenth - I2Cx->RxShadowCount);
                        switch(I2Cx->Control.Bit.EndType)
                        {
                            case 0:
                                __HAL_PSTO_1();
                                __HAL_Debug("PreStop, 0x%X",I2Cx->Instance->CR2.W);
                                break;

                            case 1:
                                __HAL_PSTA_1();
                                __HAL_Debug("PreRePeatStart, 0x%X",I2Cx->Instance->CR2.W);
                                break;

                            case 2:
                                __HAL_I2C_PAction_110();
                                __HAL_Debug("PreStopStart, 0x%X",I2Cx->Instance->CR2.W);
                                break;

                            default:
                                __HAL_Debug("NextTransfer");
                                break;
                        }
                    }
                }
//                else
//                {
//                    __HAL_Debug("Buffer Count 0");
//                    I2Cx->Instance->CR2.MBIT.BUF_CNT = 0;
//                }
            }

            if(I2Cx->RxferCount < I2Cx->RxferLenth)
            {
////                Delay(20);
                __HAL_Debug("5");
                lI2C_DAT_CNT = 0;

                if((I2Cx->State.W & I2C_STATE_MASTER) == I2C_STATE_MASTER) 
                {
                #if BM_MR_MT_ST_DataByte == 0 || BM_MR_MT_ST_DataByte >= 4
                    lI2C_DAT_DIFF = 4;
                    if((I2Cx->RxferLenth - I2Cx->RxferCount) < 4)
                #endif

                #if BM_MR_MT_ST_DataByte == 1
                    lI2C_DAT_DIFF = 1;
                    if((I2Cx->RxferLenth - I2Cx->RxferCount) < 1)
                #endif

                #if BM_MR_MT_ST_DataByte == 2
                    lI2C_DAT_DIFF = 2;
                    if((I2Cx->RxferLenth - I2Cx->RxferCount) < 2)
                #endif

                #if BM_MR_MT_ST_DataByte == 3
                    lI2C_DAT_DIFF = 3;
                    if((I2Cx->RxferLenth - I2Cx->RxferCount) < 3)
                #endif
                    {
                        lI2C_DAT_DIFF = (I2Cx->RxferLenth - I2Cx->RxferCount);
                        __HAL_Debug("6");
                    }
                }

                if((I2Cx->State.W & I2C_STATE_MASTER) == I2C_STATE_MASTER_SLAVE) 
                {
                    lI2C_DAT_DIFF = 4;
                    if((I2Cx->RxferLenth - I2Cx->RxferCount) < 4)
                    {
                        lI2C_DAT_DIFF = (I2Cx->RxferLenth - I2Cx->RxferCount);
                        __HAL_Debug("6");
                    }
                }

                // if((I2Cx->HW_State_Reg & I2C_STA_SLAF_mask_w) && (I2Cx->HW_State_Reg & I2C_STA_RWF_mask_w))
                if((I2Cx->Instance->STA.W & (I2C_STA_SLAF_mask_w | I2C_STA_RWF_mask_w)) == I2C_STA_SLAF_mask_w) // Slave Reciver
                {
                    lI2C_DAT_DIFF = I2Cx->Instance->CR2.MBIT.BUF_CNT;
                    __HAL_Debug("7");
                }

                lTranDataTemp.W = I2Cx->Instance->DAT.W;
#if !WorkStation && !FPGA && !C0
                __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_RXF);
#endif
                // I2Cx->HW_State_Reg &= ~I2C_STA_RXF_mask_w;

//                __HAL_Debug("BUF_CNT = 0x%X, DAT = 0x%8X", lI2C_DAT_DIFF, I2Cx->Instance->DAT.W);
    
                do{
                    __HAL_Debug("9");
                    //__HAL_Debug("RxferCount = 0x%d, lI2C DAT CNT = 0x%d",I2Cx->RxferCount,lI2C_DAT_CNT);
                    if(I2Cx->RxferCount < I2Cx->RxferLenth)
                    {
                        __HAL_Debug("A");
                        I2Cx->pRxBuffPtr[I2Cx->RxferCount] = lTranDataTemp.B[lI2C_DAT_CNT];
                        // printf("RxferCount = 0x%X, pData = 0x%X \n\r",I2Cx->RxferCount,I2Cx->pRxBuffPtr[I2Cx->RxferCount]);
                        I2Cx->RxferCount ++;
                        I2Cx->Control.Bit.RxBuffEmpty = 0;
                    }
                    else
                    {
                        __HAL_Debug("B");
                        I2Cx->State.Bit.SRxOVR = 1;
                        I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_INCOMPLETE | I2C_EVENT_TRANSFER_DONE;
                        break;
                    }
                }while(++lI2C_DAT_CNT < lI2C_DAT_DIFF);    
            }
//            else
//                I2Cx->Event = I2Cx->State.W;
//                I2Cx->Event |= I2C_EVENT_TRANSFER_INCOMPLETE | I2C_EVENT_RECEIVED_OVER;
            __HAL_Debug("RXFEND");
        }

        if((I2Cx->Instance->STA.W & I2C_STA_TXF_mask_w) && ((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_BUSY)) == (I2C_STATE_DIRECTION_TRAN | I2C_STATE_BUSY)))
        {
            __HAL_Debug("TXF");
            __printf("1>>");
            if((I2Cx->Instance->STA.W & (I2C_STA_MSTF_mask_w | I2C_STA_SLAF_mask_w | I2C_STA_RWF_mask_w)) == I2C_STA_MSTF_mask_w)
            {
                __printf("2>>");
                I2Cx->State.W = (I2Cx->State.W | (I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_BUFFMODE | I2C_STATE_MASTER | I2C_STATE_DIRECTION_TRAN));
            }

            // I2Cx->HW_State_Reg &= ~I2C_STA_BUFF_mask_w;
            __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_BUFF);

            if(I2Cx->TxferCount < I2Cx->TxferLenth)
            {
                __printf("3>>");
                lI2C_DAT_CNT = 0;  // Clear DAT Count

            #if BM_MR_MT_ST_DataByte == 0 || BM_MR_MT_ST_DataByte >= 4
                do{
                    lTranDataTemp.B[lI2C_DAT_CNT] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                    lI2C_DAT_CNT ++;
                    /* Suggest 1.Write I2C_BUF_CNT "and" Data Buff Trigger Operation */
                    /*         2.First Write I2C_DAT, I2C_STA_TXF 1 >> 0 >> 1, and Trigger I2C_STA_BUFF to "1" */
                    I2Cx->TxferCount ++;
                }while((lI2C_DAT_CNT < 4) && (I2Cx->TxferCount < I2Cx->TxferLenth));
            #endif

            #if BM_MR_MT_ST_DataByte == 1
                lTranDataTemp.B[lI2C_DAT_CNT] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                lI2C_DAT_CNT ++;
                I2Cx->TxferCount ++;
            #endif

            #if BM_MR_MT_ST_DataByte == 2
                do{
                    lTranDataTemp.B[lI2C_DAT_CNT] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                    lI2C_DAT_CNT ++;
                    I2Cx->TxferCount ++;
                }while((lI2C_DAT_CNT < 2) && (I2Cx->TxferCount < I2Cx->TxferLenth));
            #endif

            #if BM_MR_MT_ST_DataByte == 3
                do{
                    lTranDataTemp.B[lI2C_DAT_CNT] = I2Cx->pTxBuffPtr[I2Cx->TxferCount];
                    lI2C_DAT_CNT ++;
                    I2Cx->TxferCount ++;
                }while((lI2C_DAT_CNT < 3) && (I2Cx->TxferCount < I2Cx->TxferLenth));
            #endif

                if(I2Cx->TxferCount >= I2Cx->TxferLenth)
                {
                    __HAL_PAA_0();
                    __printf("5>>");
                    if(I2Cx->Instance->STA.W & I2C_STA_MSTF_mask_w)
                    {
                        __printf("6>>");
                        switch(I2Cx->Control.Bit.EndType)
                        {
                            case 0:
                                __HAL_PSTO_1();
                                __HAL_Debug("PreStop, 0x%X",I2Cx->Instance->CR2.W);
                                break;

                            case 1:
                                __HAL_PSTA_1();
                                __HAL_Debug("PreRePeatStart, 0x%X",I2Cx->Instance->CR2.W);
                                break;

                            case 2:
                                __HAL_I2C_PAction_110();
                                __HAL_Debug("PreStopStart, 0x%X",I2Cx->Instance->CR2.W);
                                break;

                            default:
                                __HAL_Debug("NextTransfer");
                                break;
                        }
                    }
                }

                __HAL_Debug("BUF_CNT = 0x%X \n\r", lI2C_DAT_CNT);
                I2Cx->Instance->CR2.MBIT.BUF_CNT = lI2C_DAT_CNT;  /*!< Output Data */
                I2Cx->Instance->DAT.W = lTranDataTemp.W;
                /* Clear TXF Flag */
//                __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_TXF);
//                ??? TXF Auto Clear By Write I2C_DAT ?
                __printf("7");
                //__HAL_Debug("Write Buff and Data Count");
            }
            else
            {
                __HAL_PAA_0();
//                I2Cx->Instance->CR2.MBIT.BUF_CNT = 0;  /*!< Output Data */
//                __printf("8");
            }
            __printf("\n\r");
        }

        if(((I2Cx->Instance->STA.W & (I2C_STA_STOPF_mask_w | I2C_STA_RSTRF_mask_w)) != 0) && ((I2Cx->Instance->STA.W & (I2C_STA_RXF_mask_w)) == 0) && ((I2Cx->Instance->STA.W & (I2C_STA_TXF_mask_w)) != 0))
        {
            // I2Cx->HW_State_Reg &= ~I2C_STA_BUFF_mask_w;
            __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_BUFF);
            __HAL_Debug("STOPF / RSTRF");

            if((I2Cx->State.W & (I2C_STATE_MASTER | I2C_STATE_DIRECTION | I2C_STATE_BUSY)) == (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC | I2C_STATE_BUSY))
            {
                if(I2Cx->RxferCount == 0)
                {
                    // I2Cx->Event &= ~I2C_EVENT_SLAVE_RECEIVE;
                    I2Cx->State.W = I2C_EVENT_INITREADY | I2C_EVENT_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;
                    __HAL_Debug("RxCount0");
                }
                else
                {
                    __HAL_PAA_0();
                    __HAL_I2C_AA_1();

                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;

                    if(I2Cx->RxferCount < I2Cx->RxferLenth)
                        I2Cx->Event |=  I2C_EVENT_TRANSFER_INCOMPLETE;

                    I2Cx->State.W = I2C_EVENT_INITREADY | I2C_EVENT_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;

                    /* Clear STOP Flag */
                    // NVIC_DisableIRQ(I2C0_IRQn);
                    // __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA));
                    __HAL_Debug("SR Done");
                }
            }

            if((I2Cx->State.W & (I2C_STATE_MASTER | I2C_STATE_DIRECTION | I2C_STATE_BUSY)) == (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_TRAN | I2C_STATE_BUSY))
            {
                if(I2Cx->TxferCount == 0)
                {
                    // I2Cx->Event &= ~I2C_EVENT_SLAVE_TRANSMIT;
                    I2Cx->State.W = I2C_EVENT_INITREADY | I2C_EVENT_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;
                    __HAL_Debug("TxCount0");
                }
                else
                {
                    __HAL_PAA_0();
                    __HAL_I2C_AA_1();

                    I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;

                    if(I2Cx->TxferCount < I2Cx->TxferLenth)
                        I2Cx->Event |=  I2C_EVENT_TRANSFER_INCOMPLETE;

                    I2Cx->State.W = I2C_EVENT_INITREADY | I2C_EVENT_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;

                    /* Clear STOP Flag */
                    NVIC_DisableIRQ(I2C0_IRQn);
                    // __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA));
                    __HAL_Debug("ST Done");
                }
            }

            if((I2Cx->State.W & (I2C_STATE_MASTER | I2C_STATE_DIRECTION | I2C_STATE_BUSY)) == (I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC | I2C_STATE_BUSY))
            {
                __HAL_PAA_0();
                __HAL_I2C_AA_1();

                I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;

                if(I2Cx->RxferCount < I2Cx->RxferLenth)
                    I2Cx->Event |=  I2C_EVENT_TRANSFER_INCOMPLETE;

                I2Cx->State.W = I2C_EVENT_INITREADY | I2C_EVENT_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;

                /* Clear STOP Flag */
                // NVIC_DisableIRQ(I2C0_IRQn);
                // __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA));
                __HAL_Debug("MR Done");
            }

            if((I2Cx->State.W & (I2C_STATE_MASTER | I2C_STATE_DIRECTION | I2C_STATE_BUSY)) == (I2C_STATE_MASTER | I2C_STATE_DIRECTION_TRAN | I2C_STATE_BUSY))
            {
                __HAL_PAA_0();
                __HAL_I2C_AA_1();

                I2Cx->Event = I2Cx->State.W | I2C_EVENT_TRANSFER_DONE;

                if(I2Cx->TxferCount < I2Cx->TxferLenth)
                    I2Cx->Event |=  I2C_EVENT_TRANSFER_INCOMPLETE;

                I2Cx->State.W = I2C_EVENT_INITREADY | I2C_EVENT_DIRECTION_REC | I2C_STATE_MASTER_SLAVE;

                /* Clear STOP Flag */
                // NVIC_DisableIRQ(I2C0_IRQn);
                // __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA));
                __HAL_Debug("MT Done");
            }

            if(__HAL_I2C_GET_FLAG(I2Cx, I2C_FLAG_STOPF))
            {
                __HAL_Debug("STOPF");
                __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_STOPF);
            }

            if(__HAL_I2C_GET_FLAG(I2Cx, I2C_FLAG_RSTRF))
            {
                __HAL_Debug("RSTRF");
                __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_RSTRF);
            }
        }

        /* Clear BUFF Flag */
        __HAL_Debug("End BUFF");

    }
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DRV_I2C0_IRQHandler(void)
 * @bug          
 *******************************************************************************
 */
void I2C_WakeUp_IRQHandler(I2C_HandleTypeDef *I2Cx)
{
    uint8_t Addr_Temp;
    if((__HAL_I2C_GET_FLAG(I2Cx,I2C_FLAG_WUPF)) && 
      ((I2Cx->Instance->INT.W & I2C_INT_WUP_IE_mask_w) != 0) && 
      ((I2Cx->Instance->INT.W & I2C_INT_IEA_mask_w) != 0))
    {
        __HAL_Debug("I2C Wake-Up");

        I2Cx->NextSlaveAddr = I2Cx->Instance->SAC.B[0];
        Addr_Temp = I2Cx->Instance->SBUF.B[0];
        SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;  // Config __WFI goto STOP mode

        if(I2Cx->Instance == I2C0)
            PW->WKSTP1.W &= ~PW_WKSTP1_WKSTP_I2C0_mask_w;
        else if (I2Cx->Instance == I2C1)
            PW->WKSTP1.W &= ~PW_WKSTP1_WKSTP_I2C1_mask_w;

        __HAL_I2C_DISABLE_IT(I2Cx, I2C_INT_WUP);
        __HAL_I2C_CLEAR_FLAG(I2Cx,I2C_FLAG_WUPF);

        printf("I2C Wake-Up");

        __HAL_I2C_CLEAR_FLAG(I2Cx,I2C_FLAG_SADRF);

        __HAL_Debug("End WUPF");
    }
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              void DRV_I2C0_IRQHandler(void)
 * @bug          
 *******************************************************************************
 */
void I2C_TimeOut_IRQHandler(I2C_HandleTypeDef *I2Cx)
{
    if(__HAL_I2C_GET_FLAG(I2Cx,I2C_FLAG_TMOUTF))
    {
        I2Cx->State.Bit.TimeOut = 1;
        __HAL_Debug("TMOUTF");

        /* Clear TMOUTF Flag */
        __HAL_I2C_CLEAR_FLAG(I2Cx, I2C_FLAG_TMOUTF);
        __HAL_Debug("End I2C TimeOut Handler");
    }
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
void I2C_CMD(I2C_Struct* I2Cx,FunctionalState State)
{
    __printf("I2Cx_CMD \n\r");
    if(State == DISABLE)
        I2Cx->CR0.B[0] |= I2C_CR0_EN_mask_b0;
    else
        I2Cx->CR0.B[0] &= ~I2C_CR0_EN_mask_b0;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
void I2C_ITEA_CMD(I2C_Struct* I2Cx,FunctionalState State)
{
    __printf("I2Cx_ITEA_CMD \n\r");
    if(State == DISABLE)
        I2Cx->INT.B[0] |= I2C_INT_IEA_mask_b0;
    else
        I2Cx->INT.B[0] &= ~I2C_INT_IEA_mask_b0;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
void I2C_MspInit(I2C_HandleTypeDef *I2Cx)
{
    __HAL_Debug("I2C_MspInit");
    if(I2Cx->Instance == I2C0)
    {
        /* USER CODE BEGIN I2C0_MspInit 0 */

        /* USER CODE END I2C0_MspInit 0 */

        // GPIO Config
#if C0Chip && FPGA
        PINB(8)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af2_w;
        PINB(9)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af2_w;
#else
        PINB(10)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af2_w;
        PINB(11)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af2_w;
#endif

        // Clock Enable
        UnProtectModuleReg(CSCprotect);
        CSC->APB0.MBIT.I2C0_EN = 1;
        ProtectModuleReg(CSCprotect);

        // SoftWare Trigger HardWare Reset
        UnProtectModuleReg(RSTprotect);
        RST->APB0.MBIT.I2C0_EN = 1;
        __NOP();
        RST->APB0.MBIT.I2C0_EN = 0;
        ProtectModuleReg(RSTprotect);

        __HAL_I2C_CLEAR_FLAG(I2Cx,I2C_FLAG_BUFF);   

        /* Peripheral interrupt init*/
        // NVIC_ClearPendingIRQ(I2C0_IRQn);
        // NVIC_SetPriority(I2C0_IRQn, 1);
        // NVIC_EnableIRQ(I2C0_IRQn);
        
        /* USER CODE BEGIN I2C0_MspInit 1 */

        /* USER CODE END I2C0_MspInit 1 */
    }
    else if(I2Cx->Instance == I2C1)
    {
        /* USER CODE BEGIN I2C1_MspInit 0 */

        /* USER CODE END I2C1_MspInit 0 */
        
        // GPIO Config
        PINC(10)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af2_w;
        PINC(11)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af2_w;

        // Clock Enable
        UnProtectModuleReg(CSCprotect);
        CSC->APB0.MBIT.I2C0_EN = 1;
        ProtectModuleReg(CSCprotect);

        // SoftWare Trigger HardWare Reset
        UnProtectModuleReg(RSTprotect);
        RST->APB0.MBIT.I2C1_EN = 1;
        __NOP();
        RST->APB0.MBIT.I2C1_EN = 0;
        ProtectModuleReg(RSTprotect);

        __HAL_I2C_CLEAR_FLAG(I2Cx,(I2C_FLAG_BUFF | I2C_FLAG_TXF));   

        /* Peripheral interrupt init*/
        // NVIC_ClearPendingIRQ(I2Cx_IRQn);
        // NVIC_SetPriority(I2Cx_IRQn, 1);
        // NVIC_EnableIRQ(I2Cx_IRQn);

        /* USER CODE BEGIN I2C1_MspInit 1 */

        /* USER CODE END I2C1_MspInit 1 */
    }
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_Init(I2C_HandleTypeDef *I2Cx)
{
    /* Check the I2C handle allocation */
    if(I2Cx == NULL)
    {
        return HAL_ERROR;
    }
    
    __HAL_Debug("I2C_Init");
    // __printf("Timing = 0x%8X\n\r",I2Cx->Init.Timing.H[1]);

    /* Disable the selected I2C peripheral */
    __HAL_I2C_DISABLE(I2Cx);

    // #define __HAL_I2C_DISABLE_IT(__HANDLE__, __INTERRUPT__)  ((__HANDLE__)->Instance->INT.W &= (~(__INTERRUPT__)))
    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_BUF | I2C_INT_IEA));

    if(I2Cx->Instance == I2C0)
        NVIC_DisableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_DisableIRQ (I2Cx_IRQn);

    if((I2Cx->State.W & I2C_STATE_INITREADY) == 0)
    {
        /* Init the low level hardware : GPIO, CLOCK, CORTEX...etc */
        I2C_MspInit(I2Cx);
        I2Cx->State.W = 0;
        I2Cx->Event = 0;
    }

    /*---------------------------- I2Cx TIMINGR Configuration ------------------*/
    /* Configure I2Cx: Frequency range */
    I2Cx->Instance->CLK.H[0] = I2Cx->Init.Timing.H[0];
    I2Cx->Instance->CR1.H[0] = I2Cx->Init.Timing.H[1];

    /*---------------------------- I2Cx OAR1 Configuration ---------------------*/
    I2Cx->Instance->CR0.W &= ~I2C_CR0_SADR_EN_mask_w;
    if(I2Cx->Init.OwnAddress1 != 0)
    {
        I2Cx->Instance->SADR.B[0] = I2Cx->Init.OwnAddress1;
    }

    /*---------------------------- I2Cx OAR2 Configuration ---------------------*/
    I2Cx->Instance->CR0.W &= ~I2C_CR0_SADR2_EN_mask_w;
    if((I2Cx->Init.OwnAddress2 != 0) && (I2Cx->Init.DualAddressMode != 0))    
    {
        I2Cx->Instance->SADR.B[1] = I2Cx->Init.OwnAddress2;
    }
    else
    {
        I2Cx->Init.DualAddressMode = 0x00000000;
    }

    /*---------------------------- I2Cx CR1 Configuration ----------------------*/
    I2Cx->Instance->CR0.W = (I2Cx->Init.DualAddressMode
                           | I2Cx->Init.GeneralCallMode
                           | I2Cx->Init.StretchMode
                           | I2Cx->Init.BufferMode
                           | I2Cx->Init.OperationMode
                           | I2Cx->Init.PreDrive
                           | I2Cx->Init.SDAFBDH
                           | I2C_CR0_SADR_EN_mask_w);

    I2Cx->Event = I2C_EVENT_INITREADY;
    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC;

//    // #define __HAL_I2C_DISABLE_IT(__HANDLE__, __INTERRUPT__)  ((__HANDLE__)->Instance->INT.W &= (~(__INTERRUPT__)))
//    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_BUF | I2C_INT_IEA));

//    /* Enable EVENT, IEA Interrupt*/
//    if(I2Cx->Init.BufferMode != 0)
//    {
//        // #define __HAL_I2C_ENABLE_IT(__HANDLE__, __INTERRUPT__)   ((__HANDLE__)->Instance->INT.W |= (__INTERRUPT__))
//        __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA);
//    }
//    else
//    {
//        // #define __HAL_I2C_ENABLE_IT(__HANDLE__, __INTERRUPT__)   ((__HANDLE__)->Instance->INT.W |= (__INTERRUPT__))
//        __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_EVENT | I2C_INT_IEA);
//    }

    __HAL_Debug("I2Cx Initial Ready");

    /* Enable the selected I2C peripheral */
    __HAL_I2C_ENABLE(I2Cx);

    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_IsDeviceReady(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint32_t Trials, uint32_t Timeout)
{
//    uint32_t ltickstart = 0;
    __IO uint32_t lI2C_Trials = 0;
    uint8_t lReadyState = 0; /* is "0" Slave Device is Not Ready, "1" Slave Device is Ready */

    __HAL_Debug("          I2C_IsDeviceReady ?");

    if((I2Cx->State.W & I2C_STATE_INITREADY) == 0)
    {
        return(HAL_ERROR);
    }
    
    if(((I2Cx->Instance->STA.B[0] & I2C_STA_BUSYF_mask_b0) != 0) || (I2Cx->State.Bit.Busy != 0))
    {
        return(HAL_ERROR);
    }

    // Disable
    I2Cx->Instance->CR0.W &= (~I2C_CR0_EN_mask_w);

    __HAL_I2C_DISABLE_IT(I2Cx,(I2C_INT_IEA | I2C_INT_BUF | I2C_INT_ERR | I2C_INT_EVENT | I2C_INT_TMOUT | I2C_INT_WUP));

    /* Change Mode to FirmWare Mode  */
    I2Cx->Instance->CR0.B[0] &= ~I2C_CR0_BUF_EN_mask_b0;

    /* Clear Flag */
    // I2Cx->Instance->STA.W = (~(I2C_STA_TXF_mask_w | I2C_STA_EVENTF_mask_w)); /* OLD */
    __HAL_I2C_CLEAR_FLAG(I2Cx, (~(I2C_FLAG_EVENTF | I2C_FLAG_TXF))); /* NEW */

    // Enable
    I2Cx->Instance->CR0.W |= I2C_CR0_EN_enable_w;

    I2Cx->Event |= I2C_EVENT_BUSY;
    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_MASTER | I2C_STATE_DIRECTION_TRAN;
    I2Cx->Event = I2Cx->State.W;

    do{
        __HAL_STA_1();
        // while((I2Cx->Instance->STA.B[0] & I2C_STA_EVENTF_mask_b0) == 0); /* OLD */
        // while(__HAL_I2C_GET_EVENTF(I2Cx) == 0); /* NEW */
        while((__HAL_I2C_GET_EVENTF(I2Cx) == 0) || (__HAL_I2C_GET_FLAG(I2Cx, I2C_FLAG_EVENTF) == 0)); /* Test */
        __HAL_STA_0();

        if(__HAL_I2C_EVENT_CODE(I2Cx) != 0x08)
        {
            return(HAL_ERROR); /* Bus Busy to Slave EVENT 0xA8 0x60 0x70 */ 
        }
        else // 
        {
            __HAL_Debug("            Start");
            I2Cx->Instance->SBUF.B[0] = (uint8_t)DevAddress;
            // I2Cx->Instance->STA.B[0] = I2C_STA_EVENTF_mask_b0; /* OLD */
            __HAL_I2C_CLEAR_EVENTF(I2Cx); /* NEW */

            // while(__HAL_I2C_GET_EVENTF(I2Cx) == 0); /* NEW */
            while((__HAL_I2C_GET_EVENTF(I2Cx) == 0) || (__HAL_I2C_GET_FLAG(I2Cx, I2C_FLAG_EVENTF) == 0)); /* Test */

            if(__HAL_I2C_EVENT_CODE(I2Cx) == 0x18)
            {
                lReadyState = 1; /* "1" Slave Device is Ready */
                __HAL_Debug("            Addr ACK");

//        #if !WorkStation 
//            #if _Debug || GPrintf
//                Delay(27);
//            #endif
//        #endif
//                Delay(3);
            }
            else
            {
                __HAL_Debug("            Addr NACK");
                lReadyState = 0; /* "0" Slave Device is Not Ready */
                lI2C_Trials ++;
            }

            __HAL_I2C_STO_1();
            I2Cx->Instance->STA.B[0] = I2C_STA_EVENTF_mask_b0; /* OLD */
            // __HAL_I2C_CLEAR_EVENTF(I2Cx); /* NEW */
            while(__HAL_I2C_EVENT_CODE(I2Cx) != 0xF8);
            __HAL_I2C_STO_0();
        }

        if(lReadyState == 0)
        {
            /* Delay Time */
            Delay(3);
        }
    }while((lI2C_Trials < Trials) && (lReadyState == 0));

    /* Change Mode to FirmWare Mode  */
    if(I2Cx->Init.BufferMode != 0)
    {
        I2Cx->Instance->CR0.B[0] |= I2C_CR0_BUF_EN_mask_b0;
    }
    __HAL_I2C_CLEAR_FLAG(I2Cx,(~(I2C_FLAG_TXF | I2C_FLAG_EVENTF)));

    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC;
    I2Cx->Event = I2Cx->State.W;

    __HAL_Debug("          I2C_IsDeviceReady End");

    if((lI2C_Trials < Trials) && (lReadyState == 1))
        return(HAL_OK);
    else
        return(HAL_ERROR);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeMasterTransmit(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
        return(HAL_ERROR);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeMasterReceive(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_BuffMode_MR");

    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeSlaveTransmit(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_BuffMode_ST");

    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeSlaveReceive(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_BuffMode_SR");

    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeSlaveReceiveGC(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_BuffMode_SRGC");

    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef  I2C_FWModeMasterTransmit(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_FWMode_MT");
    
    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_FWModeMasterReceive(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_FWMode_MR");
    
    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_FWModeSlaveTransmit(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_FWMode_ST");
    
    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_FWModeSlaveReceive(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_FWMode_SR");
    
    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_FWModeSlaveReceveGC(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth, uint32_t Timeout)
{
    __HAL_Debug("I2Cx_FWMode_SRGC");
    
    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeMasterTransmitIT(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
{
    __HAL_Debug("I2C_BuffModeMasterTransmit_IT");
    if((pData == NULL ) || (Lenth == 0)) 
    {
        return HAL_ERROR;
    }

    if((!(I2Cx->State.W & I2C_STATE_INITREADY)) || (I2Cx->State.W & I2C_STATE_BUSY) || (I2Cx->Control.Bit.TxDataReady))
    {
        __printf("Event = 0x%8X",I2Cx->Event);
        return HAL_ERROR;
    }

    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_EVENT | I2C_INT_IEA));

//    if(I2Cx->Instance == I2C0)
//        NVIC_DisableIRQ (I2C0_IRQn);
//    else if (I2Cx->Instance == I2C1)
//        NVIC_DisableIRQ (I2Cx_IRQn);

    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_MASTER | I2C_STATE_DIRECTION_TRAN;
    I2Cx->Event = I2C_EVENT_INITREADY;

    __HAL_Debug("I2C_FWModeMasterTransmit_IT");

    I2Cx->Control.Bit.TxDataReady = 1;
    I2Cx->Control.Bit.EndType = 0; // STOP
    I2Cx->pTxBuffPtr = pData;
    I2Cx->TxferLenth = Lenth;
    I2Cx->TxferCount = 0;
    I2Cx->DeviceAddr = (DevAddress & 0xFE);

    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA));

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    /* Write Slave Addrss to I2C DAT    */
    I2Cx->Instance->CR2.MBIT.BUF_CNT = 1;  /*!< Output Slave Addr */
    I2Cx->Instance->DAT.W = (uint32_t)(DevAddress & 0xFE);

    // Check Internal and BusState
    if((I2Cx->State.W & I2C_STATE_RESTART) != 0)
    {
        printf("ReStart");
        I2Cx->Instance->STA.W = I2C_STA_RSTRF_mask_w;    /*!< Clear Repeat Start Flag */
    }
    else
    {
        // Check SDA & SCL Pin on Logic High
        __HAL_STA_1();
    }

    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeMasterReceiveIT(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
{
    if((pData == NULL ) || (Lenth == 0))
    {
        return HAL_ERROR;
    }

    if((I2Cx->Instance->STA.MBIT.BUSYF) || (I2Cx->State.Bit.Busy) || (!I2Cx->State.Bit.InitReady))
    {
        __HAL_Debug("to Busy");
        return(HAL_ERROR);
    }

    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_EVENT | I2C_INT_IEA));

//    if(I2Cx->Instance == I2C0)
//        NVIC_DisableIRQ (I2C0_IRQn);
//    else if (I2Cx->Instance == I2C1)
//        NVIC_DisableIRQ (I2Cx_IRQn);

    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC);
    I2Cx->Event = I2C_EVENT_INITREADY;

    __HAL_Debug("I2C_BuffModeMasterReceive_IT");

    I2Cx->pRxBuffPtr = pData;
    I2Cx->RxferLenth = Lenth;
    I2Cx->RxferCount = 0;
    I2Cx->RxShadowCount = 0;
    I2Cx->Control.Bit.RxBuffEmpty = 1;

    __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA);

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    /* Write Slave Addrss to I2C DAT    */
    I2Cx->Instance->CR2.MBIT.BUF_CNT = 1;  /*!< Output Slave Addr */
    I2Cx->Instance->DAT.W = (uint32_t)(DevAddress | 0x01);

    // Check Internal and BusState
    if((I2Cx->State.W & I2C_STATE_RESTART) != 0)
    {
        __HAL_Debug("ReStart");
        I2Cx->Instance->STA.W = I2C_STA_RSTRF_mask_w;    /*!< Clear Repeat Start Flag */
    }
    else
    {
        // Check SDA & SCL Pin on Logic High
        __HAL_STA_1();
    }

    return(HAL_OK);
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeSlaveTransmitIT(I2C_HandleTypeDef *I2Cx, uint8_t OwnAddress, uint8_t *pData, uint16_t Lenth)
{
    if((pData == NULL) || (Lenth == 0))
    {
        return HAL_ERROR;
    }

    if((I2Cx->State.W & (I2C_STATE_INITREADY)) != I2C_STATE_INITREADY) 
    {
        __printf("0.Event = 0x%8X",I2Cx->Event);
        return HAL_ERROR;
    }
    else
    {
        if((I2Cx->State.W & I2C_STATE_BUSY) == I2C_STATE_BUSY)
        {
            if((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) != (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_TRAN))
            {   
                __printf("1.Event = 0x%8X",I2Cx->Event);
                return HAL_ERROR;
            }
        }
    }

    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_EVENT | I2C_INT_IEA));

//    if(I2Cx->Instance == I2C0)
//        NVIC_DisableIRQ (I2C0_IRQn);
//    else if (I2Cx->Instance == I2C1)
//        NVIC_DisableIRQ (I2Cx_IRQn);

    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC;
    I2Cx->Event = I2C_EVENT_INITREADY;

    __HAL_Debug("I2C_FWModeSlaveTransmitIT");

    I2Cx->SlaveTraAddr = (OwnAddress & 0xFE);
    I2Cx->pTxBuffPtr = pData;
    I2Cx->TxferLenth = Lenth;
    I2Cx->TxferCount = 0;
    I2Cx->Control.Bit.TxDataReady = 1;
    I2Cx->Control.Bit.EndType = 0; // STOP

    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA);

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    __HAL_I2C_AA_1();

    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_BuffModeSlaveReceiveIT(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth)
{
//    if((pData == NULL) || (Lenth == 0))
    if(pData == NULL)
    {
        return HAL_ERROR;
    }

    if((I2Cx->State.W & (I2C_STATE_INITREADY)) != I2C_STATE_INITREADY) 
    {
        __printf("Event = 0x%8X",I2Cx->Event);
        return HAL_ERROR;
    }
    else
    {
        if((I2Cx->State.W & I2C_STATE_BUSY) == I2C_STATE_BUSY)
        {
            if((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) != (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC))
            {   
                __printf("Event = 0x%8X",I2Cx->Event);
                return HAL_ERROR;
            }
        }
    }

    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_EVENT | I2C_INT_IEA));

//    if(I2Cx->Instance == I2C0)
//        NVIC_DisableIRQ (I2C0_IRQn);
//    else if (I2Cx->Instance == I2C1)
//        NVIC_DisableIRQ (I2Cx_IRQn);

    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);
    I2Cx->Event = (I2C_EVENT_INITREADY);

    __HAL_Debug("I2C_FWModeSlaveReceiveIT");

    I2Cx->pRxBuffPtr = pData;
    I2Cx->RxferLenth = Lenth;
    I2Cx->RxferCount = 0;
    I2Cx->Control.Bit.RxBuffEmpty = 1;
    I2Cx->Control.Bit.EndType = 0; // STOP

    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_BUF | I2C_INT_ERR | I2C_INT_IEA);

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    __HAL_I2C_AA_1();

    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_ByteModeMasterTransmitIT(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
{
    if((pData == NULL) || (Lenth == 0))
    {
        return HAL_ERROR;
    }

    if((!(I2Cx->State.W & I2C_STATE_INITREADY)) || (I2Cx->State.W & I2C_STATE_BUSY) || (I2Cx->Control.Bit.TxDataReady))
    {
        __printf("Event = 0x%8X",I2Cx->Event);
        return HAL_ERROR;
    }

    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_EVENT | I2C_INT_IEA));

//    if(I2Cx->Instance == I2C0)
//        NVIC_DisableIRQ (I2C0_IRQn);
//    else if (I2Cx->Instance == I2C1)
//        NVIC_DisableIRQ (I2Cx_IRQn);

    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_MASTER | I2C_STATE_DIRECTION_TRAN;
    I2Cx->Event = I2C_EVENT_INITREADY;

    __HAL_Debug("I2C_FWModeMasterTransmit_IT");

    I2Cx->Control.Bit.TxDataReady = 1;
    I2Cx->Control.Bit.EndType = 0; // STOP
    I2Cx->pTxBuffPtr = pData;
    I2Cx->TxferLenth = Lenth;
    I2Cx->TxferCount = 0;
    I2Cx->DeviceAddr = (DevAddress & 0xFE);

    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_EVENT | I2C_INT_IEA);

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    I2Cx->Event = I2C_EVENT_INITREADY;

    __HAL_STA_1();
    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_ByteModeMasterReceiveIT(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
{
    if((pData == NULL) || (Lenth == 0))
    {
        return HAL_ERROR;
    }

    if((I2Cx->State.W & (I2C_STATE_INITREADY)) != I2C_STATE_INITREADY) 
    {
        __printf("Event = 0x%8X",I2Cx->Event);
        return HAL_ERROR;
    }
    else
    {
        if((I2Cx->State.W & I2C_STATE_BUSY) == I2C_STATE_BUSY)
        {
            if((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) != (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC))
            {   
                __printf("Event = 0x%8X",I2Cx->Event);
                return HAL_ERROR;
            }
        }
    }

    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_EVENT | I2C_INT_IEA));

//    if(I2Cx->Instance == I2C0)
//        NVIC_DisableIRQ (I2C0_IRQn);
//    else if (I2Cx->Instance == I2C1)
//        NVIC_DisableIRQ (I2Cx_IRQn);

    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC);
    I2Cx->Event = I2C_EVENT_INITREADY;

    __HAL_Debug("I2C_FWModeMasterReceiveIT");

    I2Cx->pRxBuffPtr = pData;
    I2Cx->RxferLenth = Lenth;
    I2Cx->RxferCount = 0;
    I2Cx->RxShadowCount = 0;
    I2Cx->Control.Bit.RxBuffEmpty = 1;

    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_EVENT | I2C_INT_IEA);

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    I2Cx->Event = I2C_EVENT_INITREADY;

    I2Cx->DeviceAddr = (DevAddress | 0x01);

    __HAL_STA_1();
    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */

HAL_StatusTypeDef I2C_ByteModeSlaveTransmitIT(I2C_HandleTypeDef *I2Cx, uint8_t OwnAddress, uint8_t *pData, uint16_t Lenth)
{
    if((pData == NULL) || (Lenth == 0))
    {
        printf("Data is NULL \n\r");
        return HAL_ERROR;
    }

    if((I2Cx->State.W & (I2C_STATE_INITREADY)) != I2C_STATE_INITREADY) 
    {
        printf("I2C Not Initial \n\r");
        return HAL_ERROR;
    }
    else
    {
        if((I2Cx->State.W & I2C_STATE_BUSY) != I2C_STATE_BUSY)
            I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC;
        else
        {
            if((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) != (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_TRAN))
            {   
                __printf("Event = 0x%8X",I2Cx->Event);
                return HAL_ERROR;
            }
        }
    }
    __HAL_Debug("I2C_FWModeSlaveTransmitIT");

    I2Cx->SlaveTraAddr = (OwnAddress & 0xFE);
    I2Cx->pTxBuffPtr = pData;
    I2Cx->TxferLenth = Lenth;
    I2Cx->TxferCount = 0;
    I2Cx->Control.Bit.TxDataReady = 1;
    I2Cx->Control.Bit.EndType = 0; // STOP

//    __HAL_Debug("Event = 0x%8X",I2Cx->Event);
//    __HAL_Debug("State = 0x%8X",I2Cx->State.W);

    I2Cx->Event = (I2C_EVENT_INITREADY);

    __HAL_I2C_AA_1();
    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_BUF | I2C_INT_IEA));
    // __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_EVENT | I2C_INT_WUP | I2C_INT_STPSTR | I2C_INT_IEA));
    __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_EVENT | I2C_INT_IEA);
    
    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_ByteModeSlaveReceiveIT(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth)
{
//    if((pData == NULL) || (Lenth == 0))
    if(pData == NULL)
    {
        printf("Data is NULL \n\r");
        return HAL_ERROR;
    }

    if((I2Cx->State.W & (I2C_STATE_INITREADY)) != I2C_STATE_INITREADY) 
    {
        printf("Initial Not Ready \n\r");
        return HAL_ERROR;
    }
    else
    {
        if((I2Cx->State.W & I2C_STATE_BUSY) == I2C_STATE_BUSY)
        {
            if((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) != (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC))
            {   
                printf("Event = 0x%8X",I2Cx->Event);
                return HAL_ERROR;
            }
        }
    }

    __HAL_Debug("I2C_FWModeSlaveReceiveIT");

    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);
    I2Cx->pRxBuffPtr = pData;
    I2Cx->RxferLenth = (Lenth - 1);
    I2Cx->RxferCount = 0;
    I2Cx->Control.Bit.RxBuffEmpty = 1;
    I2Cx->Control.Bit.EndType = 0; // STOP

    I2Cx->Event = (I2C_EVENT_INITREADY);

    __HAL_I2C_AA_1();
    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_BUF | I2C_INT_IEA));
    // __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_EVENT | I2C_INT_WUP | I2C_INT_STPSTR | I2C_INT_IEA);
    __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_EVENT | I2C_INT_IEA);

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_ByteModeMasterTransmitDMA(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
{
    if((pData == NULL) || (Lenth == 0))
    {
        return HAL_ERROR;
    }

    if((I2Cx->State.W & I2C_STATE_INITREADY) == 0)
    {
        if((I2Cx->Control.Bit.TxDataReady == 0) || 
          ((I2Cx->Control.Bit.TXDMAChannel != I2C_CONTROL_TXDMA_DISABLE) && ((I2Cx->State.W & I2C_STATE_TXDMABUSY) != 0)) || 
          ((I2Cx->Control.Bit.TXDMAChannel == I2C_CONTROL_TXDMA_DISABLE) && (((I2Cx->State.W & I2C_STATE_BUSY) != 0) || ((I2Cx->Instance->STA.W & I2C_FLAG_BUSYF) != 0))))
        {
            __printf("Event = 0x%8X",I2Cx->Event);
            return HAL_ERROR;
        }
    }

    // DMA Check

    // Disable Interrupt
    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_EVENT | I2C_INT_IEA));

    __HAL_Debug("I2C_FWModeMasterTransmit_DMA");

    I2Cx->Control.Bit.TxDataReady = 1;
    I2Cx->pTxBuffPtr = pData;
    I2Cx->TxferLenth = Lenth;
    I2Cx->TxferCount = 0;
    I2Cx->DeviceAddr = (DevAddress & 0xFE);

    /* Send Start Signel */
    if(((I2Cx->State.W & I2C_STATE_BUSY) == 0) && ((I2Cx->Instance->STA.W & I2C_FLAG_BUSYF) == 0))
        __HAL_I2C_Action_101();

    I2Cx->State.W = I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_MASTER | I2C_STATE_DIRECTION_TRAN;
    I2Cx->Event = I2C_EVENT_INITREADY;
    // __HAL_I2C_CLEAR_EVENTF(I2Cx); /* NEW */

    // __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_WUP | I2C_INT_EVENT | I2C_INT_IEA));
    __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_EVENT | I2C_INT_IEA));

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    I2Cx->Event = I2C_EVENT_INITREADY;

    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_ByteModeMasterReceiveDMA(I2C_HandleTypeDef *I2Cx, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
{
    if((pData == NULL) || (Lenth == 0))
    {
        return HAL_ERROR;
    }

    if((I2Cx->State.W & I2C_STATE_INITREADY) == 0)
    {
        if((I2Cx->Control.Bit.RxBuffEmpty == 0) || 
          ((I2Cx->Control.Bit.RXDMAChannel != I2C_CONTROL_RXDMA_DISABLE) && ((I2Cx->State.W & I2C_STATE_RXDMABUSY) != 0)) || 
          ((I2Cx->Control.Bit.RXDMAChannel == I2C_CONTROL_RXDMA_DISABLE) && (((I2Cx->State.W & I2C_STATE_BUSY) != 0) || ((I2Cx->Instance->STA.W & I2C_FLAG_BUSYF) != 0))))
        {
            __printf("Event = 0x%8X",I2Cx->Event);
            return HAL_ERROR;
        }
    }

    // DMA Check

    // Disable Interrupt
    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_EVENT | I2C_INT_IEA));

    __HAL_Debug("I2C_FWModeMasterReceiver_DMA");

    I2Cx->Control.Bit.RxBuffEmpty = 1;
    I2Cx->pRxBuffPtr = pData;
    I2Cx->RxferLenth = Lenth;
    I2Cx->RxferCount = 0;
    I2Cx->DeviceAddr = (DevAddress | 0x01);

    /* Send Start Signel */
    if(((I2Cx->State.W & I2C_STATE_BUSY) == 0) && ((I2Cx->Instance->STA.W & I2C_FLAG_BUSYF) == 0))
        __HAL_I2C_Action_101();

    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_BUSY | I2C_STATE_MASTER | I2C_STATE_DIRECTION_REC);
    I2Cx->Event = I2C_EVENT_INITREADY;
//    __HAL_I2C_CLEAR_EVENTF(I2Cx); /* NEW */

    // __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_WUP | I2C_INT_EVENT | I2C_INT_IEA));
    __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_EVENT | I2C_INT_IEA));

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    I2Cx->Event = I2C_EVENT_INITREADY;

    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */

HAL_StatusTypeDef I2C_ByteModeSlaveTransmitDMA(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth)
{
    if((pData == NULL) || (Lenth == 0))
    {
        return HAL_ERROR;
    }

    if(((I2Cx->State.W & I2C_STATE_INITREADY) == 0) ||
        (I2Cx->Control.Bit.TxDataReady == 0) || 
       ((I2Cx->Control.Bit.TXDMAChannel != I2C_CONTROL_TXDMA_DISABLE) && ((I2Cx->State.W & I2C_STATE_TXDMABUSY) != 0)) || 
       ((I2Cx->Control.Bit.TXDMAChannel == I2C_CONTROL_TXDMA_DISABLE) && (((I2Cx->State.W & I2C_STATE_BUSY) != 0) || ((I2Cx->Instance->STA.W & I2C_FLAG_BUSYF) != 0))))
    {
            __printf("Event = 0x%8X",I2Cx->Event);
            return HAL_ERROR;
    }

    __HAL_Debug("I2C_FWModeSlaveTransmitDMA");

    I2Cx->Control.Bit.TxDataReady = 1;
    I2Cx->pTxBuffPtr = pData;
    I2Cx->TxferLenth = Lenth;
    I2Cx->TxferCount = 0;

//    __HAL_Debug("Event = 0x%8X",I2Cx->Event);
//    __HAL_Debug("State = 0x%8X",I2Cx->State.W);

    I2Cx->Event = (I2C_EVENT_INITREADY);

    __HAL_I2C_AA_1();

    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_BUF | I2C_INT_IEA));

    // __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_EVENT | I2C_INT_WUP | I2C_INT_STPSTR | I2C_INT_IEA));
    __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_WUP | I2C_INT_EVENT | I2C_INT_IEA));
    
    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    return HAL_OK;
}

/**
 *******************************************************************************
 * @brief       ?????(Template)
 *
 *              ????,??????
 * @details     ?????
 * @param[out]	var1            : var1???
 * @param[in]   var2            : var2???
 *  @arg\b      enum1/value1    : enum1/value1???(bold font)
 *  @arg\b      enum2/value2    : enum2/value2???(bold font)
 *      \n      - Enumerator-1  : Enumerator-1 ???.
 *      \n      - Enumerator-2  : Enumerator-2 ???.
 * @param[in,out]  	var3    		: var3???
 *  @arg        enum1/value1    : enum1/value1???(normal font)
 *  @arg        enum2/value2    : enum2/value2???(normal font)
 *  @arg        enum3/value3    : enum3/value3???(normal font)
 * @par         Refer
 *              ???????
 *      \n      global_var1     : global var1 ???
 *      \n      global_var2     : global var2 ???
 *      \n      global_var3     : global var3 ???
 * @return      No/??0/??1/... ???
 * @return      0=??
 * @return      1=?? 
 * @see         global_var1 : global var1 ???
 * @see         global_var2 : global var2 ???
 * @see         global_var3
 * @exception   ????????,???none
 * @note        
 * @par         Example
 * @code
                [Example Code here start ...]    
                ...
                [Example Code here end   ...]                     
 * @endcode
 * @par         Modify
 *              int FuncExample(int var1, char *var2)  
 * @bug          
 *******************************************************************************
 */
HAL_StatusTypeDef I2C_ByteModeSlaveReceiveDMA(I2C_HandleTypeDef *I2Cx, uint8_t *pData, uint16_t Lenth)
{
//    if((pData == NULL) || (Lenth == 0))
    if(pData == NULL)
    {
        printf("Data is NULL \n\r");
        return HAL_ERROR;
    }

    if((I2Cx->State.W & (I2C_STATE_INITREADY)) != I2C_STATE_INITREADY) 
    {
        printf("Initial Not Ready \n\r");
        return HAL_ERROR;
    }
    else
    {
        if((I2Cx->State.W & I2C_STATE_BUSY) == I2C_STATE_BUSY)
        {
            if((I2Cx->State.W & (I2C_STATE_DIRECTION | I2C_STATE_MASTER)) != (I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC))
            {   
                printf("Event = 0x%8X",I2Cx->Event);
                return HAL_ERROR;
            }
        }
    }

    __HAL_Debug("I2C_FWModeSlaveReceiveDMA");

    I2Cx->State.W = (I2C_STATE_INITREADY | I2C_STATE_MASTER_SLAVE | I2C_STATE_DIRECTION_REC);
    I2Cx->pRxBuffPtr = pData;
    I2Cx->RxferLenth = Lenth;
    I2Cx->RxferCount = 0;
    // I2Cx->Control.Bit.RXDMAEnable = 1;
    I2Cx->Control.Bit.RxBuffEmpty = 1;
    I2Cx->Control.Bit.EndType = 0; // STOP

    I2Cx->Event = (I2C_EVENT_INITREADY);

    __HAL_I2C_AA_1();
    /* Enable EVENT, IEA Interrupt*/
    __HAL_I2C_DISABLE_IT(I2Cx, (I2C_INT_ERR | I2C_INT_WUP | I2C_INT_TMOUT | I2C_INT_BUF | I2C_INT_IEA));
    // __HAL_I2C_ENABLE_IT(I2Cx, I2C_INT_EVENT | I2C_INT_WUP | I2C_INT_STPSTR | I2C_INT_IEA);
    __HAL_I2C_ENABLE_IT(I2Cx, (I2C_INT_WUP | I2C_INT_EVENT | I2C_INT_IEA));

    if(I2Cx->Instance == I2C0)
        NVIC_EnableIRQ (I2C0_IRQn);
    else if (I2Cx->Instance == I2C1)
        NVIC_EnableIRQ (I2Cx_IRQn);

    return HAL_OK;
}




