#include "MG32x02z_HAL_SWEMB.h"



#define DataWidthSelect         1       // 0:8bits  1:16bits 

#define __HAL_MAddress(__Addr16__)    GPIOA->OUT.H[0] = (__Addr16__)     

#define SWEMB_MAD_0_15_H        GPIOB->SC.H[0] = 0xFF00;\
                                GPIOC->SC.H[0] = 0x1F0E;

#define MCPortB                 GPIOB->SC.W
#define MCPortE                 GPIOB->SC.W

#define MCLK_H                  PX_SC_SET0_mask_w           // PD0
#define MCLK_L                  PX_SC_CLR0_mask_w
#define MCLK_L_H                GPIOD->SC.W = PX_SC_CLR0_mask_w; \
                                __ISB(); \
                                GPIOD->SC.W = PX_SC_SET0_mask_w; \
                                __ISB(); \
                                GPIOD->SC.W = PX_SC_CLR0_mask_w; \
                                __ISB(); \
                                GPIOD->SC.W = PX_SC_SET0_mask_w; \
                                __ISB(); \
                                GPIOD->SC.W = PX_SC_CLR0_mask_w; \
                                __ISB(); \
                                GPIOD->SC.W = PX_SC_SET0_mask_w; \
                                __ISB();

#define MCE_En                  PX_SC_CLR7_mask_w           // PB7
#define MCE_Dis                 PX_SC_SET7_mask_w

#define MWE_En                  PX_SC_CLR6_mask_w           // PB6
#define MWE_Dis                 PX_SC_SET6_mask_w

#define MOE_En                  PX_SC_CLR5_mask_w           // PB5
#define MOE_Dis                 PX_SC_SET5_mask_w

#define MLB_En                  PX_SC_CLR12_mask_w          // PE12
#define MLB_Dis                 PX_SC_SET12_mask_w

#define MHB_En                  PX_SC_CLR13_mask_w          // PE13
#define MHB_Dis                 PX_SC_SET13_mask_w

#define __HAL_MHB_Dis_MLB_Dis   GPIOE->SC.W = PX_SC_SET13_mask_w | PX_SC_SET12_mask_w
#define __HAL_MHB_Dis_MLB_En    GPIOE->SC.W = PX_SC_SET13_mask_w | PX_SC_CLR12_mask_w
#define __HAL_MHB_En_MLB_Dis    GPIOE->SC.W = PX_SC_CLR13_mask_w | PX_SC_SET12_mask_w
#define __HAL_MHB_En_MLB_En     GPIOE->SC.W = PX_SC_CLR13_mask_w | PX_SC_CLR12_mask_w

#define MALE_En                 PX_SC_CLR4_mask_w           // PB4
#define MALE_Dis                PX_SC_SER4_mask_w

#define MALE2_En                PX_SC_CLR14_mask_w          // PE14
#define MALE2_Dis               PX_SC_SER14_mask_w



HAL_StatusTypeDef HAL_SWEMB_MspInit(void)
{
    // MCLK
    PIND(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MCE
    PINB(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    //x PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MWE
    PINB(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    //x PINE(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MOE
    PINB(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level0_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    //x PINE(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MBW0
    PINE(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    // PINC(7)->CR.W = PX_CR_IOM_odo_w | PX_CR_HS_enable_w | PX_CR_PU_enable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MBW1
    PINE(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    // PINC(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MALE
    // PINB(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    //x PINE(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MALE2
    // PINE(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MADV
    // PINE(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MRDY
    // PINE(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MA[0:15] Port A Bug MAD[0:15]
    PINA(0)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(4)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(5)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(6)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(7)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINA(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

    // MAD[0:7]
    PINB(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

  #if DataWidthSelect == 1
    // MAD[8:15]
    PINB(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
  #endif

    SWEMB_MAD_0_15_H
    __ISB();

    return HAL_OK;
}

HAL_StatusTypeDef HAL_SWEMB_Init(void)
{
    HAL_SWEMB_MspInit();
    MCLK_L_H
    MCPortB = MCE_Dis | MWE_Dis;
    __HAL_MHB_Dis_MLB_Dis;
    MCLK_L_H
    return HAL_OK;
}



HAL_StatusTypeDef HAL_SWEMB_Init_CE_BW_Aways_0(void)
{
    HAL_SWEMB_MspInit();
    MCPortB = MCE_En | MWE_Dis | MOE_Dis;
    __HAL_MHB_En_MLB_En;
    __ISB();
    return HAL_OK;
}



HAL_StatusTypeDef HAL_SWEMB_Write_Config(void)
{
    // MAD[0:7]
    PINB(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(14)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(1)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(3)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

#if DataWidthSelect == 1
    // MAD[8:15]
    PINB(9)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(11)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(13)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(15)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(2)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(8)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(10)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(12)->CR.W = PX_CR_IOM_ppo_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
#endif
    return HAL_OK;
}

HAL_StatusTypeDef HAL_SWEMB_Read_Config(void)
{
    // MAD[0:7]
    PINB(8)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(10)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(12)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(14)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(1)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(3)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(9)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(11)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;

#if DataWidthSelect == 1
    // MAD[8:15]
    PINB(9)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(11)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(13)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINB(15)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(2)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(8)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(10)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
    PINC(12)->CR.W = PX_CR_IOM_din_w | PX_CR_HS_enable_w | PX_CR_PU_disable_w | PX_CR_INV_disable_w | PX_CR_ODC_level2_w | PX_CR_FDIV_bypass_w | PX_CR_AFS_af0_w;
#endif
    return HAL_OK;
}

#if DataWidthSelect == 0
HAL_StatusTypeDef HAL_Data_Write_Output(uint8_t WData)
#endif

#if DataWidthSelect == 1
HAL_StatusTypeDef HAL_Data_Write_Output(uint16_t WData)
#endif
{
    union{
        uint32_t W;
        uint16_t H[2];
        uint8_t  B[4];
    }Port_B_SC;
    
    union{
        uint32_t W;
        uint16_t H[2];
    }Port_C_SC;

    Port_B_SC.W = 0;
    Port_C_SC.W = 0;

#if EMB_Pin_Define == 12
    if((WData & 0x0001) != 0)
        Port_B_SC.H[0] |= 0x0100;   // Port B 8
    else
        Port_B_SC.H[1] |= 0x0100;

    if((WData & 0x0002) != 0)
        Port_B_SC.H[0] |= 0x0400;   // Port B 10
    else
        Port_B_SC.H[1] |= 0x0400;

    if((WData & 0x0004) != 0)
        Port_B_SC.H[0] |= 0x1000;   // Port B 12
    else
        Port_B_SC.H[1] |= 0x1000;

    if((WData & 0x0008) != 0)
        Port_B_SC.H[0] |= 0x4000;   // Port B 14
    else
        Port_B_SC.H[1] |= 0x4000;

    if((WData & 0x0010) != 0)
        Port_C_SC.H[0] |= 0x0002;   // Port C 1
    else
        Port_C_SC.H[1] |= 0x0002;

    if((WData & 0x0020) != 0)
        Port_C_SC.H[0] |= 0x0008;   // Port C 3
    else
        Port_C_SC.H[1] |= 0x0008;

    if((WData & 0x0040) != 0)
        Port_C_SC.H[0] |= 0x0200;   // Port C 9
    else
        Port_C_SC.H[1] |= 0x0200;

    if((WData & 0x0080) != 0)
        Port_C_SC.H[0] |= 0x0800;   // Port C 11
    else
        Port_C_SC.H[1] |= 0x0800;

  #if DataWidthSelect == 1
    if((WData & 0x0100) != 0)
        Port_B_SC.H[0] |= 0x0200;   // Port B 9
    else
        Port_B_SC.H[1] |= 0x0200;

    if((WData & 0x0200) != 0)
        Port_B_SC.H[0] |= 0x0800;   // Port B 11
    else
        Port_B_SC.H[1] |= 0x0800;

    if((WData & 0x0400) != 0)
        Port_B_SC.H[0] |= 0x2000;   // Port B 13
    else
        Port_B_SC.H[1] |= 0x2000;

    if((WData & 0x0800) != 0)
        Port_B_SC.H[0] |= 0x8000;   // Port B 15
    else
        Port_B_SC.H[1] |= 0x8000;

    if((WData & 0x1000) != 0)
        Port_C_SC.H[0] |= 0x0004;   // Port C 2
    else
        Port_C_SC.H[1] |= 0x0004;

    if((WData & 0x2000) != 0)
        Port_C_SC.H[0] |= 0x0100;   // Port C 8
    else
        Port_C_SC.H[1] |= 0x0100;

    if((WData & 0x4000) != 0)
        Port_C_SC.H[0] |= 0x0400;   // Port C 10
    else
        Port_C_SC.H[1] |= 0x0400;

    if((WData & 0x8000) != 0)
        Port_C_SC.H[0] |= 0x1000;   // Port C 12
    else
        Port_C_SC.H[1] |= 0x1000;
  #endif
#endif

#if EMB_Pin_Define == 13
    Port_B_SC.B[1] = (uint8_t)WData;
    Port_B_SC.B[3] = (uint8_t)(~WData);

    if((WData & 0x0100) != 0)
        Port_C_SC.H[0] |= 0x0002;   // Port C 1
    else
        Port_C_SC.H[1] |= 0x0002;

    if((WData & 0x0200) != 0)
        Port_C_SC.H[0] |= 0x0004;   // Port C 2
    else
        Port_C_SC.H[1] |= 0x0004;

    if((WData & 0x0400) != 0)
        Port_C_SC.H[0] |= 0x0008;   // Port C 3
    else
        Port_C_SC.H[1] |= 0x0008;

    if((WData & 0x0800) != 0)
        Port_C_SC.H[0] |= 0x0100;   // Port C 8
    else
        Port_C_SC.H[1] |= 0x0100;

    if((WData & 0x1000) != 0)
        Port_C_SC.H[0] |= 0x0200;   // Port C 9
    else
        Port_C_SC.H[1] |= 0x0200;

    if((WData & 0x2000) != 0)
        Port_C_SC.H[0] |= 0x0400;   // Port C 10
    else
        Port_C_SC.H[1] |= 0x0400;

    if((WData & 0x4000) != 0)
        Port_C_SC.H[0] |= 0x0800;   // Port C 11
    else
        Port_C_SC.H[1] |= 0x0800;

    if((WData & 0x8000) != 0)
        Port_C_SC.H[0] |= 0x1000;   // Port C 12
    else
        Port_C_SC.H[1] |= 0x1000;

#endif
    GPIOB->SC.W = Port_B_SC.W;
    GPIOC->SC.W = Port_C_SC.W;

    SWEMB_MAD_0_15_H
    __ISB();

    return HAL_OK;
}

#if DataWidthSelect == 0
uint8_t HAL_Data_Read_Input(void)
#endif

#if DataWidthSelect == 1
uint16_t HAL_Data_Read_Input(void)
#endif
{
    volatile uint16_t Port_B_IN;
    volatile uint16_t Port_C_IN;
    volatile uint16_t SWEMB_Temp_R = 0x0000;
    SWEMB_MAD_0_15_H
    __ISB();

    Port_B_IN = GPIOB->IN.H[0];
    Port_C_IN = GPIOC->IN.H[0];

#if EMB_Pin_Define == 12
    if((Port_B_IN & 0x0100) != 0) // PB8
        SWEMB_Temp_R |= 0x0001;

    if((Port_B_IN & 0x0400) != 0) // PB10
        SWEMB_Temp_R |= 0x0002;

    if((Port_B_IN & 0x1000) != 0) // PB12
        SWEMB_Temp_R |= 0x0004;

    if((Port_B_IN & 0x4000) != 0) // PB14
        SWEMB_Temp_R |= 0x0008;

    if((Port_C_IN & 0x0002) != 0) // PC1
        SWEMB_Temp_R |= 0x0010;

    if((Port_C_IN & 0x0008) != 0) // PC3
        SWEMB_Temp_R |= 0x0020;

    if((Port_C_IN & 0x0200) != 0) // PC9
        SWEMB_Temp_R |= 0x0040;

    if((Port_C_IN & 0x0800) != 0) // PC11
        SWEMB_Temp_R |= 0x0080;

  #if DataWidthSelect == 0
    return((uint8_t)SWEMB_Temp_R);
  #endif

  #if DataWidthSelect == 1
    if((Port_B_IN & 0x0200) != 0) // PB9
        SWEMB_Temp_R |= 0x0100;

    if((Port_B_IN & 0x0800) != 0) // PB11
        SWEMB_Temp_R |= 0x0200;

    if((Port_B_IN & 0x2000) != 0) // PB13
        SWEMB_Temp_R |= 0x0400;

    if((Port_B_IN & 0x8000) != 0) // PB15
        SWEMB_Temp_R |= 0x0800;

    if((Port_C_IN & 0x0004) != 0) // PC2
        SWEMB_Temp_R |= 0x1000;

    if((Port_C_IN & 0x0100) != 0) // PC8
        SWEMB_Temp_R |= 0x2000;

    if((Port_C_IN & 0x0400) != 0) // PC10
        SWEMB_Temp_R |= 0x4000;

    if((Port_C_IN & 0x1000) != 0) // PC12
        SWEMB_Temp_R |= 0x8000;

//    printf("ReadData_HalfWord 0x%4X \n\r", SWEMB_Temp_R);
    return((uint16_t)SWEMB_Temp_R);
  #endif
#endif

#if EMB_Pin_Define == 13
    SWEMB_Temp_R |= (uint8_t)(Port_B_IN >> 8);
    SWEMB_Temp_R & 0x00FF;

    if((Port_C_IN & 0x0002) != 0) // PC1
        SWEMB_Temp_R |= 0x0100;

    if((Port_C_IN & 0x0004) != 0) // PC2
        SWEMB_Temp_R |= 0x0200;

    if((Port_C_IN & 0x0008) != 0) // PC3
        SWEMB_Temp_R |= 0x0400;

    if((Port_C_IN & 0x0100) != 0) // PC8
        SWEMB_Temp_R |= 0x0800;

    if((Port_C_IN & 0x0200) != 0) // PC9
        SWEMB_Temp_R |= 0x1000;

    if((Port_C_IN & 0x0400) != 0) // PC10
        SWEMB_Temp_R |= 0x2000;

    if((Port_C_IN & 0x0800) != 0) // PC11
        SWEMB_Temp_R |= 0x4000;

    if((Port_C_IN & 0x1000) != 0) // PC12
        SWEMB_Temp_R |= 0x8000;
#endif
    return((uint16_t)SWEMB_Temp_R);
}

HAL_StatusTypeDef HAL_SWEMB_Byte_Write(uint32_t Address, uint8_t Data)
{
//    MCPortB = MCE_Dis | MWE_Dis;
//    __HAL_MHB_Dis_MLB_Dis;
//    __ISB();
#if DataWidthSelect == 0
    __HAL_MAddress((uint16_t)Address);
    HAL_Data_Write_Output(Data)
    MCPortB = MCE_En | MWE_En | MOE_Dis;
    __HAL_MHB_Dis_MLB_En;
#else
    __HAL_MAddress((uint16_t)((Address >> 1) & 0x7FFFFFFF));
    // HAL_Data_Write_Output(((((uint16_t)Data) << 8) & 0xFF00) | (0x00FF & ((uint16_t)Data)));

    MCPortB = MCE_En | MWE_En;

    if((Address & 0x0001) == 0)
    {
        HAL_Data_Write_Output(0x00FF & ((uint16_t)Data));
        __HAL_MHB_Dis_MLB_En;
    }
    else
    {
        HAL_Data_Write_Output((((uint16_t)Data) << 8) & 0xFF00);
        __HAL_MHB_En_MLB_Dis;
    }
#endif
    __ISB();
    MCLK_L_H
    __HAL_MHB_Dis_MLB_Dis;
    __ISB();
    MCPortB = MCE_Dis | MWE_Dis | MOE_Dis; 
    __ISB();
    HAL_Data_Write_Output(0xFFFF);
    return HAL_OK;
}

HAL_StatusTypeDef HAL_SWEMB_HalfWord_Write(uint32_t Address, uint16_t Data)
{
//    MCPortB = MCE_Dis | MWE_Dis;
//    __HAL_MHB_Dis_MLB_Dis;
//    __ISB();
#if DataWidthSelect == 0
    volatile union{
        uint8_t B[4];
        uint16_t H[2];
        uint32_t W;
    }lu_Temp;

    if((Address & 0x01) != 0)
        return HAL_ERROR;

    lu_Temp.H[0] = Data;

    __HAL_MAddress((uint16_t)Address);
    HAL_Data_Write_Output(lu_Temp.B[0])
    MCPortB = MCE_En | MWE_En | MOE_Dis;
    __HAL_MHB_Dis_MLB_En;

    __ISB();
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MWE_Dis;

    __HAL_MAddress((uint16_t)(Address | 0x0001));
    HAL_Data_Write_Output(lu_Temp.B[1])
    MCPortB = MCE_En | MWE_En | MOE_Dis;
    __HAL_MHB_Dis_MLB_En;

#else

    if((Address & 0x01) != 0)
        return HAL_ERROR;

    __HAL_MAddress((uint16_t)((Address >> 1) & 0x7FFFFFFF));
    HAL_Data_Write_Output(0xFFFF & ((uint16_t)Data));
    MCPortB = MCE_En;
    MCPortB = MWE_En | MOE_Dis;
    __HAL_MHB_En_MLB_En;

#endif
    __ISB();
    MCLK_L_H
    __HAL_MHB_Dis_MLB_Dis;
    __ISB();
    MCPortB = MWE_Dis | MOE_Dis;
    MCPortB = MCE_Dis;

    return HAL_OK;
}


HAL_StatusTypeDef HAL_SWEMB_HalfWord_Write_CE_BW_Aways_0(uint32_t Address, uint16_t Data)
{
    if((Address & 0x01) != 0)
        return HAL_ERROR;

    __HAL_MAddress((uint16_t)((Address >> 1) & 0x7FFFFFFF));
    HAL_Data_Write_Output(0xFFFF & ((uint16_t)Data));
    MCPortB = MWE_En | MOE_Dis;
    __ISB();
    MCPortB = MWE_Dis | MOE_Dis;
    return HAL_OK;
}


HAL_StatusTypeDef HAL_SWEMB_Word_Write(uint32_t Address, uint32_t Data)
{
    volatile union{
        uint8_t B[4];
        uint16_t H[2];
        uint32_t W;
    }lu_Temp;


    if((Address & 0x03) != 0)
        return HAL_ERROR;

//    MCPortB = MCE_Dis | MWE_Dis;
//    __HAL_MHB_Dis_MLB_Dis;
//    __ISB();

    lu_Temp.W = Data;

#if DataWidthSelect == 0
    __HAL_MAddress((uint16_t)Address);
    HAL_Data_Write_Output(lu_Temp.B[0])
    MCPortB = MCE_En | MWE_En | MOE_Dis;
    __HAL_MHB_Dis_MLB_En;

    __ISB();
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MWE_Dis;

    __HAL_MAddress((uint16_t)(Address | 0x0001));
    HAL_Data_Write_Output(lu_Temp.B[1])
    __HAL_MHB_Dis_MLB_En;
    MCPortB = MCE_En | MWE_En | MOE_Dis;
    __ISB();
    MCPortB = MCE_Dis | MWE_Dis;
    __HAL_MHB_Dis_MLB_Dis;

    __HAL_MAddress((uint16_t)(Address | 0x0010));
    HAL_Data_Write_Output(lu_Temp.B[2])
    __HAL_MHB_Dis_MLB_En;
    MCPortB = MCE_En | MWE_En | MOE_Dis;
    __ISB();
    MCPortB = MCE_Dis | MWE_Dis;
    __HAL_MHB_Dis_MLB_Dis;

    __HAL_MAddress((uint16_t)(Address | 0x0011));
    HAL_Data_Write_Output(lu_Temp.B[3])
    __HAL_MHB_Dis_MLB_En;
    MCPortB = MCE_En | MWE_En | MOE_Dis;

#else
    __HAL_MAddress((uint16_t)((Address >> 1) & 0x7FFFFFFF));
    HAL_Data_Write_Output(lu_Temp.H[0]);
    MCPortB = MCE_En | MWE_En | MOE_Dis;
    __HAL_MHB_En_MLB_En;
    __ISB();

    MCLK_L_H

    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MWE_Dis;
    __ISB();

    __HAL_MAddress((uint16_t)(((Address >> 1) & 0x7FFFFFFF) | 0x00000001));
    HAL_Data_Write_Output(lu_Temp.H[1]);
    MCPortB = MCE_En | MWE_En | MOE_Dis;
    __HAL_MHB_En_MLB_En;

#endif
    __ISB();
    MCLK_L_H
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MWE_Dis | MOE_Dis;
    HAL_Data_Write_Output(0xFFFF);

    return HAL_OK;
}

uint8_t HAL_SWEMB_Byte_Read(uint32_t Address)
{
    union{
        uint8_t B[4];
        uint16_t H[2];
        uint32_t W;
    }lu_Temp;

#if DataWidthSelect == 0
    __HAL_MAddress((uint16_t)(Address ));
    HAL_Data_Write_Output(0xFFFF);
    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_Dis_MLB_En;
    __ISB();
    lu_Temp.B[0] = HAL_Data_Read_Input;

    __ISB();
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MWE_Dis | MWE_Dis;

    return lu_Temp.B[0];
#else
    __HAL_MAddress((uint16_t)((Address >> 1) & 0x7FFFFFFF));
    HAL_Data_Write_Output(0xFFFF);

    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_En_MLB_En;
    __ISB();

    MCLK_L_H

    lu_Temp.H[0] = HAL_Data_Read_Input();

    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;

//    printf("ReadData_Byte 0x%4X \n\r", lu_Temp.H[0]);
    if((Address & 0x0001) == 0)
        return(lu_Temp.B[0]);
    else
        return(lu_Temp.B[1]);
#endif
}

uint16_t HAL_SWEMB_HalfWord_Read(uint32_t Address)
{
    volatile union{
        uint8_t B[4];
        uint16_t H[2];
        uint32_t W;
    }lu_Temp;

    if((Address & 0x01) != 0)
        return HAL_ERROR;

#if DataWidthSelect == 0
    HAL_Data_Write_Output(0xFFFF);

    __HAL_MAddress((uint16_t)(Address ));
    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_Dis_MLB_En;
    __ISB();
    lu_Temp.B[0] = HAL_Data_Read_Input;
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;

    __HAL_MAddress((uint16_t)(Address));
    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_Dis_MLB_En;
    __ISB();
    lu_Temp.B[1] = HAL_Data_Read_Input;
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;

    return lu_Temp.H[0];
#else
    __HAL_MAddress((uint16_t)((Address >> 1) & 0x7FFFFFFF));
    HAL_Data_Write_Output(0xFFFF);

    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_En_MLB_En;
    __ISB();

    MCLK_L_H

    lu_Temp.H[0] = HAL_Data_Read_Input();

    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;

//    printf("ReadData_HalfWord 0x%4X \n\r", lu_Temp.H[0]);
    return(lu_Temp.H[0]);
#endif
}


uint16_t HAL_SWEMB_HalfWord_Read_CE_BW_Aways_0(uint32_t Address)
{
    volatile union{
        uint8_t B[4];
        uint16_t H[2];
        uint32_t W;
    }lu_Temp;

    __HAL_MAddress((uint16_t)((Address >> 1) & 0x7FFFFFFF));
    HAL_Data_Write_Output(0xFFFF);

    MCPortB = MOE_En | MWE_Dis;
    __ISB();
    lu_Temp.H[0] = HAL_Data_Read_Input();
    MCPortB = MOE_Dis | MWE_Dis;
//    printf("ReadData_HalfWord 0x%4X \n\r", lu_Temp.H[0]);
    return(lu_Temp.H[0]);
}


uint32_t HAL_SWEMB_Word_Read(uint32_t Address)
{
    volatile union{
        uint8_t B[4];
        uint16_t H[2];
        uint32_t W;
    }lu_Temp;

    if((Address & 0x03) != 0)
        return HAL_ERROR;

#if DataWidthSelect == 0
    HAL_Data_Write_Output(0xFFFF);

    __HAL_MAddress((uint16_t)(Address ));
    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_Dis_MLB_En;
    __ISB();
    lu_Temp.B[0] = HAL_Data_Read_Input;
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;

    __HAL_MAddress((uint16_t)(Address));
    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_Dis_MLB_En;
    __ISB();
    lu_Temp.B[1] = HAL_Data_Read_Input;
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;

    __HAL_MAddress((uint16_t)(Address));
    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_Dis_MLB_En;
    __ISB();
    lu_Temp.B[2] = HAL_Data_Read_Input;
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;

    __HAL_MAddress((uint16_t)(Address));
    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_Dis_MLB_En;
    __ISB();
    lu_Temp.B[3] = HAL_Data_Read_Input;
    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;

    return lu_Temp.W;
#else
    
    HAL_Data_Write_Output(0xFFFF);

    __HAL_MAddress((uint16_t)((Address >> 1) & 0x7FFFFFFF));
    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_En_MLB_En;
    __ISB();

    MCLK_L_H

    lu_Temp.H[0] = HAL_Data_Read_Input();

    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;
    __ISB();
    
    __HAL_MAddress((uint16_t)(((Address | 0x00000002) >> 1) & 0x7FFFFFFF));

    MCPortB = MCE_En | MOE_En | MWE_Dis;
    __HAL_MHB_En_MLB_En;
    __ISB();

    MCLK_L_H

    lu_Temp.H[1] = HAL_Data_Read_Input();

    __HAL_MHB_Dis_MLB_Dis;
    MCPortB = MCE_Dis | MOE_Dis | MWE_Dis;
//    printf("ReadData_Word 0x%8X \n\r", lu_Temp.W);
    return(lu_Temp.W);
#endif 
}



