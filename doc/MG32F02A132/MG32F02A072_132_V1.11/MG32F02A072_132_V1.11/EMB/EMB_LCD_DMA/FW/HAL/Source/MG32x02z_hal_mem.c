
#include <MG32x02z_HAL_MEM.h>



/**
 *******************************************************************************
 * @fn          HAL_MEM_Lock_Flash_Access
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_Lock_Flash_Access(void)
{
    MEM->CR1.W &= MEM_CR1_ISPD_WEN_mask_w | MEM_CR1_ISPD_REN_mask_w | MEM_CR1_ISP_WEN_mask_w | MEM_CR1_ISP_REN_mask_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
    MEM->CR0.MBIT.EN = 1;
    MEM->CR0.W |= MEM_CR0_EN_mask_w;

    MEM->CR0.W &= MEM_CR0_EN_mask_w;

}



/**
 *******************************************************************************
 * @fn          HAL_MEM_AP_Single_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_AP_Single_Program(uint32_t* Addr,uint32_t Value)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W |= MEM_CR1_AP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;

    *Addr = Value;

    MEM->CR1.W &= ~MEM_CR1_AP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}



/**
 *******************************************************************************
 * @fn          HAL_MEM_AP_Single_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_AP_Single_Erase(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W |= MEM_CR1_AP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;

    *Addr = 0x00000000;

    MEM->CR1.W &= ~MEM_CR1_AP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}



/**
 *******************************************************************************
 * @fn          HAL_MEM_AP_Multiple_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_AP_Multiple_Program(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W |= MEM_CR1_AP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}



/**
 *******************************************************************************
 * @fn          MEM_AP_Multiple_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_AP_Multiple_Erase(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W |= MEM_CR1_AP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}



/**
 *******************************************************************************
 * @fn          MEM_IAP_Single_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_IAP_Single_Program(uint32_t* Addr, uint32_t Value)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W |= MEM_CR1_IAP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;

    *Addr = Value;

    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR1.W &= ~MEM_CR1_IAP_WEN_enable_w;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}



/**
 *******************************************************************************
 * @fn          MEM_IAP_Single_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_IAP_Single_Erase(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= (MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0);
    MEM->CR1.W |= MEM_CR1_IAP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;

    *Addr = 0x00000000;

    MEM->CR1.W &= ~MEM_CR1_IAP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_IAP_Multiple_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_IAP_Multiple_Program(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W |= MEM_CR1_IAP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_IAP_Multiple_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_IAP_Multiple_Erase(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= (MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0);
    MEM->CR1.W |= MEM_CR1_IAP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Read
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Read(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    // AP Mode > ISP Mode Set ISPD REN > AP Mode
    MEM->CR1.W |= MEM_CR1_ISP_REN_enable_w;
    MEM->KEY.MBIT.KEY = 0;
    MEM->KEY.MBIT.KEY2 = 0;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Single_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Single_Program(uint32_t* Addr, uint32_t Value)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= (MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0 | 0x40);
    MEM->CR1.W |= MEM_CR1_ISP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0x9867;
    MEM->SKEY.MBIT.SKEY2 = 0xB955;

    *Addr = Value;

//    MEM->CR1.W &= ~MEM_CR1_ISP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Single_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Single_Erase(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= (MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0 | 0x40);
    MEM->CR1.W |= MEM_CR1_ISP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0x9867;
    MEM->SKEY.MBIT.SKEY2 = 0xB955;

    *Addr = 0x00000000;

//    MEM->CR1.W &= ~MEM_CR1_ISP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Multiple_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Multiple_Program(void)
{
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0 | 0x40;
    MEM->CR1.W |= MEM_CR1_ISP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0x9867;
    MEM->SKEY.MBIT.SKEY2 = 0xBEAA;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Multiple_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Multiple_Erase(void)
{
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0 | 0x40;
    MEM->CR1.W |= MEM_CR1_ISP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0x9867;
    MEM->SKEY.MBIT.SKEY2 = 0xBEAA;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Read
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Read(void)
{
    // AP Mode > ISP Mode Set ISPD REN > AP Mode
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR1.W |= MEM_CR1_ISPD_REN_enable_w;
    MEM->KEY.MBIT.KEY = 0;
    MEM->KEY.MBIT.KEY2 = 0;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Single_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Single_Program(uint32_t* Addr, uint32_t Value)
{
    // AP Mode > ISP Mode Set ISPD REN / WEN > AP Mode
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W |= MEM_CR1_ISPD_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;
    
    *Addr = Value;

//    MEM->CR1.W &= ~MEM_CR1_ISPD_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Single_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Single_Erase(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W |= MEM_CR1_ISPD_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;
    
    *Addr = 0x00000000;

//    MEM->CR1.W &= ~MEM_CR1_ISPD_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Multiple_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Multiple_Program(void)
{
    // AP Mode > ISP Mode Set ISPD REN / WEN > AP Mode
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W |= MEM_CR1_ISPD_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Multiple_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Multiple_Erase(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W |= MEM_CR1_ISPD_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_OB_Single_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_OB_Single_Program(uint32_t* Addr, uint32_t Value)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0 | 0x40;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR1.W |= MEM_CR1_OB_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0xC33C;
    MEM->SKEY.MBIT.SKEY2 = 0xB9AA;

    *Addr = Value;

    MEM->CR1.W &= ~MEM_CR1_OB_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_OB_Single_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_OB_Single_Erase(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0 | 0x40;
    MEM->CR1.W |= MEM_CR1_OB_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0xC33C;
    MEM->SKEY.MBIT.SKEY2 = 0xB9AA;

    *Addr = 0x00000000;

    MEM->CR1.W &= ~MEM_CR1_OB_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_OB_Multiple_Program
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_OB_Multiple_Program(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0 | 0x40;
    MEM->CR1.W |= MEM_CR1_OB_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0xC33C;
    MEM->SKEY.MBIT.SKEY2 = 0xBE55;
}

/**
 *******************************************************************************
 * @fn          MEM_OB_Multiple_Erase
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_OB_Multiple_Erase(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0 | 0x40;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR1.W |= MEM_CR1_OB_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0xC33C;
    MEM->SKEY.MBIT.SKEY2 = 0xBE55;
}



/**
 *******************************************************************************
 * @fn          MEM_AP_Single_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_AP_Single_Program_Dis(uint32_t* Addr, uint32_t Value)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W &= ~MEM_CR1_AP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;

    *Addr = Value;

    MEM->CR1.W &= ~MEM_CR1_AP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_AP_Single_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_AP_Single_Erase_Dis(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W &= ~MEM_CR1_AP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;

    *Addr = 0x00000000;

    MEM->CR1.W &= ~MEM_CR1_AP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_AP_Multiple_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_AP_Multiple_Program_Dis(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W &= ~MEM_CR1_AP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_AP_Multiple_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_AP_Multiple_Erase_Dis(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W &= ~MEM_CR1_AP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_IAP_Single_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_IAP_Single_Program_Dis(uint32_t* Addr, uint32_t Value)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W &= ~MEM_CR1_IAP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;

    *Addr = Value;

    MEM->CR1.W &= ~MEM_CR1_IAP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_IAP_Single_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_IAP_Single_Erase_Dis(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W &= ~MEM_CR1_IAP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;

    *Addr = 0x00000000;

    MEM->CR1.W &= ~MEM_CR1_IAP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_IAP_Multiple_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_IAP_Multiple_Program_Dis(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W &= ~MEM_CR1_IAP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_IAP_Multiple_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_IAP_Multiple_Erase_Dis(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W &= ~MEM_CR1_IAP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Read_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Read_Dis(void)
{
    // AP Mode > ISP Mode Set ISPD REN > AP Mode
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR1.W &= ~MEM_CR1_ISP_REN_enable_w;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Single_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Single_Program_Dis(uint32_t* Addr, uint32_t Value)
{
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0 | 0x40;
    MEM->CR1.W &= ~MEM_CR1_ISP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0x9867;
    MEM->SKEY.MBIT.SKEY2 = 0xB955;

    *Addr = Value;

    MEM->CR1.W &= ~MEM_CR1_ISP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Single_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Single_Erase_Dis(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0 | 0x40;
    MEM->CR1.W &= ~MEM_CR1_ISP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0x9867;
    MEM->SKEY.MBIT.SKEY2 = 0xB955;

    *Addr = 0x00000000;

    MEM->CR1.W &= ~MEM_CR1_ISP_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Multiple_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Multiple_Program_Dis(void)
{
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0 | 0x40;
    MEM->CR1.W &= ~MEM_CR1_ISP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0x9867;
    MEM->SKEY.MBIT.SKEY2 = 0xBEAA;
}

/**
 *******************************************************************************
 * @fn          MEM_ISP_Multiple_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISP_Multiple_Erase_Dis(void)
{
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0 | 0x40;
    MEM->CR1.W &= ~MEM_CR1_ISP_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0x9867;
    MEM->SKEY.MBIT.SKEY2 = 0xBEAA;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Read_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Read_Dis(void)
{
    // AP Mode > ISP Mode Set ISPD REN > AP Mode
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR1.W &= ~MEM_CR1_ISPD_REN_enable_w;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Single_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Single_Program_Dis(uint32_t* Addr, uint32_t Value)
{
    // AP Mode > ISP Mode Set ISPD REN / WEN > AP Mode
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W &= ~MEM_CR1_ISPD_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;
    
    *Addr = Value;

    MEM->CR1.W &= ~MEM_CR1_ISPD_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Single_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Single_Erase_Dis(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W &= ~MEM_CR1_ISPD_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xB9;
    
    *Addr = 0x00000000;

    MEM->CR1.W &= ~MEM_CR1_ISPD_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Multiple_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Multiple_Program_Dis(void)
{
    // AP Mode > ISP Mode Set ISPD REN / WEN > AP Mode
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0;
    MEM->CR1.W &= ~MEM_CR1_ISPD_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_ISPD_Multiple_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_ISPD_Multiple_Erase_Dis(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0;
    MEM->CR1.W &= ~MEM_CR1_ISPD_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY = 0x46;
    MEM->SKEY.MBIT.SKEY = 0xBE;
}

/**
 *******************************************************************************
 * @fn          MEM_OB_Single_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_OB_Single_Program_Dis(uint32_t* Addr, uint32_t Value)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0 | 0x40;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR1.W &= ~MEM_CR1_OB_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0xC33C;
    MEM->SKEY.MBIT.SKEY2 = 0xB9AA;

    *Addr = Value;

    MEM->CR1.W &= ~MEM_CR1_OB_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_OB_Single_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_OB_Single_Erase_Dis(uint32_t* Addr)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0 | 0x40;
    MEM->CR1.W &= ~MEM_CR1_OB_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0xC33C;
    MEM->SKEY.MBIT.SKEY2 = 0xB9AA;

    *Addr = 0x00000000;

    MEM->CR1.W &= ~MEM_CR1_OB_WEN_enable_w;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->KEY.MBIT.KEY2 = 0x0000;
    MEM->KEY.MBIT.KEY = 0x0000;
}

/**
 *******************************************************************************
 * @fn          MEM_OB_Multiple_Program_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_OB_Multiple_Program_Dis(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_write_b0 | 0x40;
    MEM->CR1.W &= ~MEM_CR1_OB_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0xC33C;
    MEM->SKEY.MBIT.SKEY2 = 0xBE55;
}

/**
 *******************************************************************************
 * @fn          MEM_OB_Multiple_Erase_Dis
 * @brief       
 * @details     
 * @param       
 *      @arg    
 * @return      
 * @bug         
 * @note        
 *******************************************************************************
 */
void HAL_MEM_OB_Multiple_Erase_Dis(void)
{
    MEM->KEY.MBIT.KEY = 0xA217;
    MEM->CR0.B[0] &= ~MEM_CR0_MDS_mask_b0;
    MEM->CR0.B[0] |= MEM_CR0_EN_mask_b0 | MEM_CR0_MDS_erase_b0 | 0x40;
    MEM->KEY.MBIT.KEY2 = 0xA217;
    MEM->CR1.W &= ~MEM_CR1_OB_WEN_enable_w;
    MEM->SKEY.MBIT.SKEY2 = 0xC33C;
    MEM->SKEY.MBIT.SKEY2 = 0xBE55;
}















