#ifndef __MG32x02z_HAL_SWEMB_H
#define __MG32x02z_HAL_SWEMB_H

#ifdef __cplusplus
 extern "C" {
#endif



#include "MG32x02z_HAL_Common.h"

HAL_StatusTypeDef HAL_SWEMB_Read_Config(void);
HAL_StatusTypeDef HAL_SWEMB_Write_Config(void);

HAL_StatusTypeDef HAL_SWEMB_MspInit(void);
HAL_StatusTypeDef HAL_SWEMB_Init(void);
HAL_StatusTypeDef HAL_SWEMB_Init_CE_BW_Aways_0(void);

HAL_StatusTypeDef HAL_SWEMB_Byte_Write(uint32_t Address, uint8_t Data);
HAL_StatusTypeDef HAL_SWEMB_HalfWord_Write(uint32_t Address, uint16_t Data);
HAL_StatusTypeDef HAL_SWEMB_Word_Write(uint32_t Address, uint32_t Data);

uint8_t HAL_SWEMB_Byte_Read(uint32_t Address);
uint16_t HAL_SWEMB_HalfWord_Read(uint32_t Address);
uint32_t HAL_SWEMB_Word_Read(uint32_t Address);

HAL_StatusTypeDef HAL_SWEMB_HalfWord_Write_CE_BW_Aways_0(uint32_t Address, uint16_t Data);
uint16_t HAL_SWEMB_HalfWord_Read_CE_BW_Aways_0(uint32_t Address);

#ifdef __cplusplus
}
#endif

#endif


