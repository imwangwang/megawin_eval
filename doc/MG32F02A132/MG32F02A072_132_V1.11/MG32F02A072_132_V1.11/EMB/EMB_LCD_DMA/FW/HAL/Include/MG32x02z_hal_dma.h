#ifndef __MG32x02z_HAL_DMA_H
#define __MG32x02z_HAL_DMA_H

#ifdef __cplusplus
 extern "C" {
#endif



/* Includes ------------------------------------------------------------------*/
#include "MG32x02z_hal_Common.h"

/** @defgroup DMA OperationMode Configuration Structure definition.
  * @brief    DMA OperationMode Configuration Structure definition    
  * @{
  */
typedef union{
    uint32_t W;
    uint16_t H[2];
    uint8_t B[4];
    struct{
                                         // for DMA CR0
        uint8_t                 :1;      //[0]
        uint8_t     PRI_MDS     :1;      //[1] DMA Channel priority mode select.
                                         //    0 = Round : control by Round Robin method
                                         //    1 = Level : control by Channel priority level
        uint8_t                 :2;      //[3..2]
        uint8_t     GPL_CHS     :2;      //[5..4] DMA Channel select for extra GPL function. 
                                         //       0x0 = Disable : no any Channel with GPL function
                                         //       0x1 = CH0
                                         //       0x2 = CH1
                                         //       0x3 = CH2
        uint8_t                 :2;      //[7..6]
        uint8_t                 :8;      //[15..8]
        uint8_t                 :8;      //[23..16]
        uint8_t                 :8;      //[31..24]
    }Bits;
}DMA_OperationModeTypeDef;

/**
  * @}
  */



/** @defgroup DMA Configuration Structure definition
  * @brief  DMA Configuration Structure definition    
  * @{
  */
typedef struct
{
    DMA_OperationModeTypeDef OperationMode;  /*!<                                            */  
}DMA_InitTypeDef;

/**
  * @}
  */



/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define DMA_CONTROL_NONE0           (1UL << 0) /*!<  */
#define DMA_CONTROL_NONE1           (1UL << 1) /*!<  */
#define DMA_CONTROL_NONE2           (1UL << 2) /*!<  */
#define DMA_CONTROL_NONE3           (1UL << 3) /*!<  */
#define DMA_CONTROL_NONE4           (1UL << 4) /*!<  */
#define DMA_CONTROL_NONE5           (1UL << 5) /*!<  */
#define DMA_CONTROL_NONE6           (1UL << 6) /*!<  */
#define DMA_CONTROL_NONE7           (1UL << 7) /*!<  */
#define DMA_CONTROL_NONE8           (1UL << 8) /*!<  */
#define DMA_CONTROL_NONE9           (1UL << 9) /*!<  */
#define DMA_CONTROL_NONE10          (1UL << 10) /*!< */
#define DMA_CONTROL_NONE11          (1UL << 11) /*!< */
#define DMA_CONTROL_NONE12          (1UL << 12) /*!< */
#define DMA_CONTROL_NONE13          (1UL << 13) /*!<  */
#define DMA_CONTROL_NONE14          (1UL << 14) /*!<  */
#define DMA_CONTROL_Pending         (1UL << 15) /*!< After the data transfer Pending */

typedef union
{
    uint32_t    W;
    uint16_t    H[2];
    uint8_t     B[4];
    struct{
        uint8_t                     :1; ///< bit0
        uint8_t                     :1; ///< bit1
        uint8_t                     :1; ///< bit2
        uint8_t                     :1; ///< bit3
        uint8_t                     :1; ///< bit4
        uint8_t                     :1; ///< bit5
        uint8_t                     :1; ///< bit6
        uint8_t                     :1; ///< bit7
        uint8_t                     :1; ///< bit8
        uint8_t                     :1; ///< bit9
        uint8_t                     :1; ///< bit10
        uint8_t                     :1; ///< bit11
        uint8_t                     :1; ///< bit12
        uint8_t                     :1; ///< bit13
        uint8_t                     :1; ///< bit14
        uint8_t    Pending          :1; ///< bit15 After the data transfer Pending
        uint16_t   Trials           :16; ///< bit16 Master Mode Device Slave NACK, Trials Number. 
    }Bit;
}DMA_ControlTypeDef;

/**
  * @}
  */



/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define DMA_STATE_ALL                     0xC0000001
#define DMA_STATE_INCOMPLETE              (1UL << 1)  /*!< I2C Master/Slave Transmit/Receive incomplete transfer */
//#define DMA_STATE_NONE2                 (1UL << 2)  /*!<  */
//#define DMA_STATE_NONE3                 (1UL << 3)  /*!<  */
//#define DMA_STATE_NONE4                 (1UL << 4)  /*!<  */
//#define DMA_STATE_NONE5                 (1UL << 5)  /*!<  */
//#define DMA_STATE_NONE6                 (1UL << 6)  /*!<  */
//#define DMA_STATE_NONE7                 (1UL << 7)  /*!<  */
//#define DMA_STATE_NONE8                 (1UL << 8)  /*!<  */
//#define DMA_STATE_NONE9                 (1UL << 9)  /*!<  */
//#define DMA_STATE_NONE10                (1UL << 10)  /*!<  */
//#define DMA_STATE_NONE11                (1UL << 11)  /*!<  */
//#define DMA_STATE_NONE12                (1UL << 12)  /*!<  */
//#define DMA_STATE_NONE13                (1UL << 13)  /*!<  */
//#define DMA_STATE_NONE14                (1UL << 14)  /*!<  */
//#define DMA_STATE_NONE15                (1UL << 15)  /*!<  */
//#define DMA_STATE_NONE16                (1UL << 16)  /*!<  */
//#define DMA_STATE_NONE17                (1UL << 17)  /*!<  */
//#define DMA_STATE_NONE18                (1UL << 18)  /*!<  */
//#define DMA_STATE_NONE19                (1UL << 19)  /*!<  */
//#define DMA_STATE_NONE20                (1UL << 20)  /*!<  */
//#define DMA_STATE_NONE21                (1UL << 21)  /*!<  */
//#define DMA_STATE_NONE22                (1UL << 22)  /*!<  */
//#define DMA_STATE_NONE23                (1UL << 23)  /*!<  */
//#define DMA_STATE_NONE24                (1UL << 24)  /*!<  */
//#define DMA_STATE_NONE25                (1UL << 25)  /*!<  */
//#define DMA_STATE_NONE26                (1UL << 26)  /*!<  */
//#define DMA_STATE_NONE27                (1UL << 27)  /*!<  */
//#define DMA_STATE_NONE28                (1UL << 28)  /*!<  */
//#define DMA_STATE_NONE29                (1UL << 29)  /*!<  */
#define DMA_STATE_BUSY                  (1UL << 30)  /*!< Busy */
#define DMA_STATE_INITREADY             (1UL << 31)  /*!<  */

typedef union
{
    uint32_t    W;
    uint16_t    H[2];
    uint8_t     B[4];
    struct{
        uint8_t                     :1; ///< bit0
        uint8_t     Incomplete      :1; ///< bit1
        uint8_t                     :1; ///< bit2
        uint8_t                     :1; ///< bit3
        uint8_t                     :1; ///< bit4
        uint8_t                     :1; ///< bit5
        uint8_t                     :1; ///< bit6
        uint8_t                     :1; ///< bit7
        uint8_t                     :1; ///< bit8
        uint8_t                     :1; ///< bit9
        uint8_t                     :1; ///< bit10
        uint8_t                     :1; ///< bit11
        uint8_t                     :1; ///< bit12
        uint8_t                     :1; ///< bit13
        uint8_t                     :1; ///< bit14
        uint8_t                     :1; ///< bit15
        uint8_t                     :1; ///< bit16
        uint8_t                     :1; ///< bit17
        uint8_t                     :1; ///< bit18
        uint8_t                     :1; ///< bit19
        uint8_t                     :1; ///< bit20
        uint8_t                     :1; ///< bit21
        uint8_t                     :1; ///< bit22
        uint8_t                     :1; ///< bit23
        uint8_t                     :1; ///< bit24 
        uint8_t                     :1; ///< bit25
        uint8_t                     :1; ///< bit26
        uint8_t                     :1; ///< bit27 
        uint8_t                     :1; ///< bit28 
        uint8_t                     :1; ///< bit29 BuffMode: 0=ByteMode/FirmWareMode, 1=8-Bytes Buffer Mode
        uint8_t     Busy            :1; ///< bit30 Busy Flag
        uint8_t     InitReady       :1; ///< bit31
    }Bit;
}DMA_StateTypeDef;
/**
  * @}
  */



/** @defgroup HAL_Event_structure_definition HAL state structure definition
  * @brief  HAL Event structure definition  
  * @{
  */
#define DMA_EVENT_RESET                 0UL        /*!< I2C not yet initialized or disabled */
#define DMA_EVENT_TRANSFER_DONE         (1UL << 0)  /*!< I2C Master/Slave Transmit/Receive finished */
#define DMA_EVENT_TRANSFER_INCOMPLETE   (1UL << 1)  /*!< I2C Master/Slave Transmit/Receive incomplete transfer */
//#define DMA_EVENT_                      (1UL << 2)  /*!<  */
//#define DMA_EVENT_                      (1UL << 3)  /*!<  */
//#define DMA_EVENT_                      (1UL << 4)  /*!<  */
//#define DMA_EVENT_                      (1UL << 5)  /*!<  */
//#define DMA_EVENT_                      (1UL << 6)  /*!<  */
//#define DMA_EVENT_                      (1UL << 7)  /*!<  */
//#define DMA_EVENT_                      (1UL << 8)  /*!<  */
//#define DMA_EVENT_                      (1UL << 9)  /*!<  */
//#define DMA_EVENT_                      (1UL << 10) /*!<  */
//#define DMA_EVENT_                      (1UL << 11) /*!<  */
//#define DMA_EVENT_                      (1UL << 12) /*!<  */
//#define DMA_EVENT_                      (1UL << 13) /*!<  */
//#define DMA_EVENT_NONE14                (1UL << 14) /*!<  */
//#define DMA_EVENT_NONE15                (1UL << 15) /*!<  */
//#define DMA_EVENT_NONE16                (1UL << 16) /*!<  */
//#define DMA_EVENT_NONE17                (1UL << 17) /*!<  */
//#define DMA_EVENT_NONE18                (1UL << 18) /*!<  */
//#define DMA_EVENT_NONE19                (1UL << 19) /*!<  */
//#define DMA_EVENT_NONE20                (1UL << 20) /*!<  */
//#define DMA_EVENT_                      (1UL << 21) /*!<  */
//#define DMA_EVENT_                      (1UL << 22) /*!<  */
//#define DMA_EVENT_                      (1UL << 23) /*!<  */
//#define DMA_EVENT_                      (1UL << 24) /*!<  */
//#define DMA_EVENT_                      (1UL << 25) /*!<  */
//#define DMA_EVENT_                      (1UL << 26) /*!<  */
//#define DMA_EVENT_                      (1UL << 27)  /*!<  */
//#define DMA_EVENT_                      (1UL << 28)  /*!<  */
//#define DMA_EVENT_                      (1UL << 29) /*!<  */
#define DMA_EVENT_BUSY                  (1UL << 30) /*!< I2C internal process is ongoing */
#define DMA_EVENT_INITREADY             (1UL << 31) /*!< I2C initialized Ready */
/**
  * @}
  */



/** @defgroup Special_structure_definition Special structure definition
  * @brief  Special structure definition  
  * @{
  */

typedef enum{
    DMA_SPECIAL_NONE = 0,
}DMA_SpecialTypedef;

/**
  * @}
  */



/** @defgroup I2C_handle_Structure_definition I2C handle Structure definition 
  * @brief  I2C handle Structure definition   
  * @{
  */

typedef struct
{

    DMA_Struct              *Instance;      /*!< DMA registers base address */
    __IO DMA_InitTypeDef    Init;           /*!< DMA communication parameters */

    HAL_LockTypeDef         Lock;           /*!< DMA locking object */

    __IO uint32_t           Event;          /*!< DMA communication User Event */
    __IO DMA_ControlTypeDef Control;        /*!< DMA communication User Control */
    __IO DMA_StateTypeDef   State;          /*!< DMA communication HW State */

    DMA_SpecialTypedef      Special;
    __IO uint32_t           SpecialNumber;
    __IO uint32_t           SpecialCount;
}DMA_HandleTypeDef;
/**
  * @}
  */



/** @defgroup DMA_CHx OperationMode Configuration_Structure_definition I2C Timing Configuration Structure definition
  * @brief  DMA_CHx OperationMode Configuration Structure definition    
  * @{
  */
typedef union{
    uint32_t W[2];
    uint16_t H[4];
    uint8_t B[8];
    struct{
                                        // for DMA_CHx CHxA
        uint8_t                 :1;     //[0]
        uint8_t     HOLD        :1;     //[1] DMA Channel operation hold Enable.
        uint8_t     LOOP        :1;     //[2] DMA Channel destination or peripheral transfer address auto increased Enable.
        uint8_t     ADSEL       :1;     //[3] DMA Channel address increased mode select.
        uint8_t                 :4;     //[7..4]
        uint8_t     BUS_XMDS    :2;     //[9..8] DMA Channel external pin trigger request mode select.
                                        //       0x0 = Disable : Disable external request pin input.
                                        //       0x1 = Single : single request mode.
                                        //       0x2 = Block : block request mode.
                                        //       0x3 = Demand : demand request mode(active high).
        uint8_t     BUS_PLS     :2;     //[11..10] DMA Channel priority level select.
                                        //       0x0 = LV0 : lowest priority
                                        //       0x1 = LV1 : normal priority
                                        //       0x2 = LV2 : high priority
                                        //       0x3 = LV3 : highest priority
        uint8_t     BSIZE       :2;     //[13..12] DMA Channel transfer burst size.
                                        //       0x0 = One
                                        //       0x1 = Two
                                        //       0x2 = Reserved
                                        //       0x3 = Four
        uint8_t                 :1;     //[14]
        uint8_t     REQUEST     :1;     //[15] DMA Channel data transfer request Enable. 
        uint8_t                 :1;     //[16]
        uint8_t     CIE         :1;     //[17] DMA Channel transfer complete interrupt Enable.
        uint8_t     HIE         :1;     //[18] DMA Channel transfer half interrupt Enable.
        uint8_t     EIE         :1;     //[19] DMA Channel transfer error interrupt Enable.
        uint8_t                 :4;     //[23..20]
        uint8_t                 :1;     //[24]
        uint8_t                 :1;     //[25] DMA Channel-0 transfer complete Flag.
        uint8_t                 :1;     //[26] DMA Channel-0 transfer half Flag. 
        uint8_t                 :1;     //[27] DMA Channel-0 transfer error Flag.
        uint8_t                 :4;     //[31..28]

                                        // for DMA_CHx CHxB
        uint8_t     SRC         :4;     //[3..0] DMA Channel transfer peripheral source select.
        uint8_t                 :4;     //[7..4]
        uint8_t     DET         :4;     //[11..8] DMA Channel transfer peripheral destination select. 
        uint8_t                 :4;     //[15..12]
        uint8_t     SINC        :1;     //[16] DMA source or memory transfer address auto increased Enable.
        uint8_t     DINC        :1;     //[17] DMA destination or peripheral transfer address auto increased Enable.
        uint8_t     SSYNC       :1;     //[18] DMA source process synchronization Enable bit.
        uint8_t     DSYNC       :1;     //[19] DMA destination process synchronization Enable bit.
        uint8_t                 :4;     //[23..20]
        uint8_t     XPIN        :1;     //[24]
        uint8_t                 :7;     //[31..25]
    }Bits;
}DMA_CHx_OperationModeTypeDef;

/**
  * @}
  */



/** @defgroup I2C_Configuration_Structure_definition I2C Configuration Structure definition
  * @brief  I2C Configuration Structure definition    
  * @{
  */
typedef struct
{
    __IO DMA_CHx_OperationModeTypeDef   OperationMode;  /*!<                                            */  
    __IO uint32_t        SRC_ADDR;
    __IO uint32_t        DET_ADDR;
    __IO uint16_t        Lenth;
}DMA_CHx_InitTypeDef;

/**
  * @}
  */



/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define DMA_CHx_CONTROL_NONE0           (1UL << 0) /*!<  */
#define DMA_CHx_CONTROL_NONE1           (1UL << 1) /*!<  */
#define DMA_CHx_CONTROL_NONE2           (1UL << 2) /*!<  */
#define DMA_CHx_CONTROL_NONE3           (1UL << 3) /*!<  */
#define DMA_CHx_CONTROL_NONE4           (1UL << 4) /*!<  */
#define DMA_CHx_CONTROL_NONE5           (1UL << 5) /*!<  */
#define DMA_CHx_CONTROL_NONE6           (1UL << 6) /*!<  */
#define DMA_CHx_CONTROL_NONE7           (1UL << 7) /*!<  */
#define DMA_CHx_CONTROL_NONE8           (1UL << 8) /*!<  */
#define DMA_CHx_CONTROL_NONE9           (1UL << 9) /*!<  */
#define DMA_CHx_CONTROL_NONE10          (1UL << 10) /*!< */
#define DMA_CHx_CONTROL_NONE11          (1UL << 11) /*!< */
#define DMA_CHx_CONTROL_NONE12          (1UL << 12) /*!< */
#define DMA_CHx_CONTROL_NONE13          (1UL << 13) /*!<  */
#define DMA_CHx_CONTROL_NONE14          (1UL << 14) /*!<  */
#define DMA_CHx_CONTROL_Pending         (1UL << 15) /*!< After the data transfer Pending */

typedef union
{
    uint32_t    W;
    uint16_t    H[2];
    uint8_t     B[4];
    struct{
        uint8_t                     :1; ///< bit0
        uint8_t                     :1; ///< bit1
        uint8_t                     :1; ///< bit2
        uint8_t                     :1; ///< bit3
        uint8_t                     :1; ///< bit4
        uint8_t                     :1; ///< bit5
        uint8_t                     :1; ///< bit6
        uint8_t                     :1; ///< bit7
        uint8_t                     :1; ///< bit8
        uint8_t                     :1; ///< bit9
        uint8_t                     :1; ///< bit10
        uint8_t                     :1; ///< bit11
        uint8_t                     :1; ///< bit12
        uint8_t                     :1; ///< bit13
        uint8_t                     :1; ///< bit14
        uint8_t    Pending          :1; ///< bit15 After the data transfer Pending
        uint16_t   Trials           :16; ///< bit16 Master Mode Device Slave NACK, Trials Number. 
    }Bit;
}DMA_CHx_ControlTypeDef;

/**
  * @}
  */



/** @defgroup HAL_State_structure_definition HAL state structure definition
  * @brief  HAL State structure definition  
  * @{
  */

#define DMA_CHx_STATE_TRANSFER_INCOMPLETE   (1UL << 1)  /*!< I2C Master/Slave Transmit/Receive incomplete transfer */
//#define DMA_CHx_STATE_NONE2                 (1UL << 2)  /*!<  */
//#define DMA_CHx_STATE_NONE3                 (1UL << 3)  /*!<  */
//#define DMA_CHx_STATE_NONE4                 (1UL << 4)  /*!<  */
//#define DMA_CHx_STATE_NONE5                 (1UL << 5)  /*!<  */
//#define DMA_CHx_STATE_NONE6                 (1UL << 6)  /*!<  */
//#define DMA_CHx_STATE_NONE7                 (1UL << 7)  /*!<  */
//#define DMA_CHx_STATE_NONE8                 (1UL << 8)  /*!<  */
//#define DMA_CHx_STATE_NONE9                 (1UL << 9)  /*!<  */
//#define DMA_CHx_STATE_NONE10                (1UL << 10)  /*!<  */
//#define DMA_CHx_STATE_NONE11                (1UL << 11)  /*!<  */
//#define DMA_CHx_STATE_NONE12                (1UL << 12)  /*!<  */
//#define DMA_CHx_STATE_NONE13                (1UL << 13)  /*!<  */
//#define DMA_CHx_STATE_NONE14                (1UL << 14)  /*!<  */
//#define DMA_CHx_STATE_NONE15                (1UL << 15)  /*!<  */
//#define DMA_CHx_STATE_NONE16                (1UL << 16)  /*!<  */
//#define DMA_CHx_STATE_NONE17                (1UL << 17)  /*!<  */
//#define DMA_CHx_STATE_NONE18                (1UL << 18)  /*!<  */
//#define DMA_CHx_STATE_NONE19                (1UL << 19)  /*!<  */
//#define DMA_CHx_STATE_NONE20                (1UL << 20)  /*!<  */
//#define DMA_CHx_STATE_NONE21                (1UL << 21)  /*!<  */
//#define DMA_CHx_STATE_NONE22                (1UL << 22)  /*!<  */
//#define DMA_CHx_STATE_NONE23                (1UL << 23)  /*!<  */
//#define DMA_CHx_STATE_NONE24                (1UL << 24)  /*!<  */
//#define DMA_CHx_STATE_NONE25                (1UL << 25)  /*!<  */
//#define DMA_CHx_STATE_NONE26                (1UL << 26)  /*!<  */
//#define DMA_CHx_STATE_NONE27                (1UL << 27)  /*!<  */
//#define DMA_CHx_STATE_NONE28                (1UL << 28)  /*!<  */
//#define DMA_CHx_STATE_NONE29                (1UL << 29)  /*!<  */
#define DMA_CHx_STATE_BUSY                  (1UL << 30)  /*!< Busy */
#define DMA_CHx_STATE_INITREADY             (1UL << 31)  /*!<  */

typedef union
{
    uint32_t    W;
    uint16_t    H[2];
    uint8_t     B[4];
    struct{
        uint8_t                     :1; ///< bit0
        uint8_t     Incomplete      :1; ///< bit1
        uint8_t                     :1; ///< bit2
        uint8_t                     :1; ///< bit3
        uint8_t                     :1; ///< bit4
        uint8_t                     :1; ///< bit5
        uint8_t                     :1; ///< bit6
        uint8_t                     :1; ///< bit7
        uint8_t                     :1; ///< bit8
        uint8_t                     :1; ///< bit9
        uint8_t                     :1; ///< bit10
        uint8_t                     :1; ///< bit11
        uint8_t                     :1; ///< bit12
        uint8_t                     :1; ///< bit13
        uint8_t                     :1; ///< bit14
        uint8_t                     :1; ///< bit15
        uint8_t                     :1; ///< bit16
        uint8_t                     :1; ///< bit17
        uint8_t                     :1; ///< bit18
        uint8_t                     :1; ///< bit19
        uint8_t                     :1; ///< bit20
        uint8_t                     :1; ///< bit21
        uint8_t                     :1; ///< bit22
        uint8_t                     :1; ///< bit23
        uint8_t                     :1; ///< bit24 
        uint8_t                     :1; ///< bit25
        uint8_t                     :1; ///< bit26
        uint8_t                     :1; ///< bit27 
        uint8_t                     :1; ///< bit28 
        uint8_t                     :1; ///< bit29 BuffMode: 0=ByteMode/FirmWareMode, 1=8-Bytes Buffer Mode
        uint8_t     Busy            :1; ///< bit30 Busy Flag
        uint8_t     InitReady       :1; ///< bit31
    }Bit;
}DMA_CHx_StateTypeDef;
/**
  * @}
  */



/** @defgroup HAL_Event_structure_definition HAL state structure definition
  * @brief  HAL Event structure definition  
  * @{
  */
#define DMA_CHx_EVENT_RESET                 0UL        /*!< I2C not yet initialized or disabled */
#define DMA_CHx_EVENT_TRANSFER_DONE         (1UL << 0)  /*!< I2C Master/Slave Transmit/Receive finished */
#define DMA_CHx_EVENT_TRANSFER_INCOMPLETE   (1UL << 1)  /*!< I2C Master/Slave Transmit/Receive incomplete transfer */
//#define DMA_CHx_EVENT_                      (1UL << 2)  /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 3)  /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 4)  /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 5)  /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 6)  /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 7)  /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 8)  /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 9)  /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 10) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 11) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 12) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 13) /*!<  */
//#define DMA_CHx_EVENT_NONE14                (1UL << 14) /*!<  */
//#define DMA_CHx_EVENT_NONE15                (1UL << 15) /*!<  */
//#define DMA_CHx_EVENT_NONE16                (1UL << 16) /*!<  */
//#define DMA_CHx_EVENT_NONE17                (1UL << 17) /*!<  */
//#define DMA_CHx_EVENT_NONE18                (1UL << 18) /*!<  */
//#define DMA_CHx_EVENT_NONE19                (1UL << 19) /*!<  */
//#define DMA_CHx_EVENT_NONE20                (1UL << 20) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 21) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 22) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 23) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 24) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 25) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 26) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 27) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 28) /*!<  */
//#define DMA_CHx_EVENT_                      (1UL << 29) /*!<  */
#define DMA_CHx_EVENT_BUSY                  (1UL << 30) /*!< I2C internal process is ongoing */
#define DMA_CHx_EVENT_INITREADY             (1UL << 31) /*!< I2C initialized Ready */
/**
  * @}
  */



/** @defgroup Special_structure_definition Special structure definition
  * @brief  Special structure definition  
  * @{
  */

typedef enum{
    DMA_CHx_SPECIAL_NONE = 0,
}DMA_CHx_SpecialTypedef;

/**
  * @}
  */



/** @defgroup I2C_handle_Structure_definition I2C handle Structure definition 
  * @brief  I2C handle Structure definition   
  * @{
  */

typedef struct
{

    DMA_CHx_Struct              *Instance;      /*!< DMA_CHx registers base address */
    __IO DMA_CHx_InitTypeDef    Init;           /*!< DMA_CHx communication parameters */

    __IO DMA_CHx_ControlTypeDef Control;        /*!< DMA_CHx communication Control */
    __IO DMA_CHx_StateTypeDef   State;          /*!< DMA_CHx communication State */
    __IO uint32_t               Event;          /*!< DMA_CHx communication Event */

    HAL_LockTypeDef             Lock;            /*!< DMA locking object */

    DMA_CHx_SpecialTypedef      Special;
    __IO uint32_t               SpecialNumber;
    __IO uint32_t               SpecialCount;
}DMA_CHx_HandleTypeDef;

/**
  * @}
  */



/** @defgroup DMA_Flag_definition DMA Flag definition
  * @brief I2C Interrupt definition
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX  : Interrupt control mask
  * @{
  */ 
#define DMA_FLAG_ALL                      0x80000FFFUL
#define DMA_FLAG_GPL_CEF                  (0x1UL << 31)
#define DMA_FLAG_CH2_ERRF                 DMA_STA_CH2_ERRF_mask_w   /* DMA Channel-2 transfer error Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH2_THF                  DMA_STA_CH2_THF_mask_w    /* DMA Channel-2 transfer half Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH2_TCF                  DMA_STA_CH2_TCF_mask_w    /* DMA Channel-2 transfer complete Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH2_GIF                  DMA_STA_CH2_GIF_mask_w    /* DMA Channel-2 global interrupt Flag. This bit will be set if any of other Channel event interrupt Flag is set. */
#define DMA_FLAG_CH1_ERRF                 DMA_STA_CH1_ERRF_mask_w   /* DMA Channel-1 transfer error Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH1_THF                  DMA_STA_CH1_THF_mask_w    /* DMA Channel-1 transfer half Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH1_TCF                  DMA_STA_CH1_TCF_mask_w    /* DMA Channel-1 transfer complete Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH1_GIF                  DMA_STA_CH1_GIF_mask_w    /* DMA Channel-1 global interrupt Flag. This bit will be set if any of other Channel event interrupt Flag is set. */
#define DMA_FLAG_CH0_ERRF                 DMA_STA_CH0_ERRF_mask_w   /* DMA Channel-0 transfer error Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH0_THF                  DMA_STA_CH0_THF_mask_w    /* DMA Channel-0 transfer half Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH0_TCF                  DMA_STA_CH0_TCF_mask_w    /* DMA Channel-0 transfer complete Flag. (set by hardware and clear by software writing 1) */
#define DMA_FLAG_CH0_GIF                  DMA_STA_CH0_GIF_mask_w    /* DMA Channel-0 global interrupt Flag. This bit will be set if any of other Channel event interrupt Flag is set. */

/**
  * @}
  */

/** @defgroup DMA GPL Select
  * @brief DMA GPL Select
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
//#define DMA_IT_GPL_CEIE                DMA_INT_GPL_CEIE_enable_b3
#define DMA_IT_IEA                     DMA_INT_IEA_enable_b0

/**
  * @}
  */



/** @defgroup DMA GPL Select
  * @brief DMA GPL Select
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_GPL_DISABLE                 DMA_CR0_GPL_CHS_disable_b0
#define DMA_GPL_CHANNEL_0               DMA_CR0_GPL_CHS_ch0_b0
#define DMA_GPL_CHANNEL_1               DMA_CR0_GPL_CHS_ch1_b0
#define DMA_GPL_CHANNEL_2               DMA_CR0_GPL_CHS_ch2_b0

/**
  * @}
  */



/** @defgroup DMA Chennel Enable / Disable
  * @brief DMA GPL Select
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHANNEL_ENALBE_0            DMA_CR0_CH0_ENB_enable_b0
#define DMA_CHANNEL_ENALBE_1            DMA_CR0_CH1_ENB_enable_b0
#define DMA_CHANNEL_ENALBE_2            DMA_CR0_CH2_ENB_enable_b0

/**
  * @}
  */



/** @defgroup DMA Channel priority mode select.
  * @brief DMA Channel priority mode select.
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_PRIORITY_ROUND              DMA_CR0_PRI_MDS_round_b0
#define DMA_PRIORITY_LEVEL              DMA_CR0_PRI_MDS_level_b0

/**
  * @}
  */



//=============================================================================
/** @defgroup DMA_Flag_definition DMA Flag definition
  * @brief I2C Interrupt definition
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX  : Interrupt control mask
  * @{
  */ 
#define DMA_CHx_FLAG_ERRF               DMA_CHxA_CHx_ERR2F_mask_b3   /* DMA Channel-0 transfer error Flag. (set by hardware and clear by software writing 1) */
#define DMA_CHx_FLAG_THF                DMA_CHxA_CHx_TH2F_mask_b3    /* DMA Channel-0 transfer half Flag. (set by hardware and clear by software writing 1) */
#define DMA_CHx_FLAG_TCF                DMA_CHxA_CHx_TC2F_mask_b3    /* DMA Channel-0 transfer complete Flag. (set by hardware and clear by software writing 1) */

/**
  * @}
  */



/** @defgroup DMA Channel Transfer Interrupt Enable/Disable.
  * @brief DMA Channel Transfer Interrupt 
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHx_IT_EIE                  DMA_CHxA_CHx_EIE_mask_b2
#define DMA_CHx_IT_HIE                  DMA_CHxA_CHx_HIE_mask_b2
#define DMA_CHx_IT_CIE                  DMA_CHxA_CHx_CIE_mask_b2

/**
  * @}
  */



/** @defgroup DMA transfer burst size.
  * @brief DMA Channel Transfer Interrupt 
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHx_BSIZE_MASK              DMA_CHxA_CHx_BSIZE_mask_b1
#define DMA_CHx_BSIZE_1                 DMA_CHxA_CHx_BSIZE_one_b1
#define DMA_CHx_BSIZE_2                 DMA_CHxA_CHx_BSIZE_two_b1
#define DMA_CHx_BSIZE_4                 DMA_CHxA_CHx_BSIZE_four_b1

/**
  * @}
  */



/** @defgroup DMA Channel priority level select.
  * @brief DMA Channel Transfer Interrupt 
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHx_PLS_MASK                DMA_CHxA_CHx_PLS_mask_b1

#define DMA_CHx_PLS_LEVEL_0             DMA_CHxA_CHx_PLS_lv0_b1
#define DMA_CHx_PLS_LEVEL_1             DMA_CHxA_CHx_PLS_lv1_b1
#define DMA_CHx_PLS_LEVEL_2             DMA_CHxA_CHx_PLS_lv2_b1
#define DMA_CHx_PLS_LEVEL_3             DMA_CHxA_CHx_PLS_lv3_b1

#define DMA_CHx_PLS_LOWSET              DMA_CHxA_CHx_PLS_lv0_b1
#define DMA_CHx_PLS_MIDDLE              DMA_CHxA_CHx_PLS_lv1_b1
#define DMA_CHx_PLS_HIGH                DMA_CHxA_CHx_PLS_lv2_b1
#define DMA_CHx_PLS_HIGHSET             DMA_CHxA_CHx_PLS_lv3_b1
/**
  * @}
  */



/** @defgroup DMA Channel external pin trigger request mode select. 
  * @brief DMA Channel external pin trigger request mode select. 
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHx_XMDS_MASK               DMA_CHxA_CHx_XMDS_mask_b1

#define DMA_CHx_XMDS_DISABLE            DMA_CHxA_CHx_XMDS_disable_b1
#define DMA_CHx_XMDS_SINGLE             DMA_CHxA_CHx_XMDS_single_b1
#define DMA_CHx_XMDS_BLOCK              DMA_CHxA_CHx_XMDS_block_b1
#define DMA_CHx_XMDS_DENAND             DMA_CHxA_CHx_XMDS_demand_b1

/**
  * @}
  */



/** @defgroup DMA address increased mode select.
  * @brief DMA Channel external pin trigger request mode select. 
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHx_ADSEL_MASK               DMA_CHxA_CHx_ADSEL_mask_b0
#define DMA_CHx_ADSEL_NORMAL             DMA_CHxA_CHx_ADSEL_normal_b0
#define DMA_CHx_ADSEL_SKIP3              DMA_CHxA_CHx_ADSEL_skip3_b0



/** @defgroup DMA Channel external trigger pin select.
  * @brief DMA Channel external pin trigger request mode select. 
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHx_XPIN_MASK                DMA_CHxB_CHx_XPIN_mask_b3
#define DMA_CHx_XPIN_TRG0                DMA_CHxB_CHx_XPIN_trg0_b3
#define DMA_CHx_XPIN_TRG1                DMA_CHxB_CHx_XPIN_trg1_b3



/** @defgroup DMA Channel transfer peripheral destination select. 
  * @brief DMA Channel external pin trigger request mode select. 
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHx_DET_MASK                0xFF
#define DMA_CHx_DET_Memory              0x00
#define DMA_CHx_DET_DAC0                0x01
#define DMA_CHx_DET_I2C0                0x02
#define DMA_CHx_DET_I2C1                0x03
#define DMA_CHx_DET_URT0                0x04
#define DMA_CHx_DET_URT1                0x05
#define DMA_CHx_DET_URT2                0x06
#define DMA_CHx_DET_URT3                0x07
#define DMA_CHx_DET_SPI0                0x08
#define DMA_CHx_DET_GPL                 0x0B
#define DMA_CHx_DET_TM36_CC0B           0x0C
#define DMA_CHx_DET_TM36_CC1B           0x0D
#define DMA_CHx_DET_TM36_CC2B           0x0E
#define DMA_CHx_DET_TM36_IC3            0x0F



/** @defgroup DMA Channel transfer peripheral source select. 
  * @brief DMA Channel external pin trigger request mode select. 
  *        Elements values convention: 0xXXXXXXXX
  *           - XXXXXXXX 
  * @{
  */
#define DMA_CHx_SRC_MASK                0xFF
#define DMA_CHx_SRC_Memory              0x00
#define DMA_CHx_SRC_ADC0                0x01
#define DMA_CHx_SRC_I2C0                0x02
#define DMA_CHx_SRC_I2C1                0x03
#define DMA_CHx_SRC_URT0                0x04
#define DMA_CHx_SRC_URT1                0x05
#define DMA_CHx_SRC_URT2                0x06
#define DMA_CHx_SRC_URT3                0x07
#define DMA_CHx_SRC_SPI0                0x08
#define DMA_CHx_SRC_TM36_IC3            0x0F



//=====================================================================================================================
/** @brief  Checks if the specified I2C interrupt source is enabled or disabled.
  * @param  __HANDLE__: specifies the I2C Handle.
  *         This parameter can be I2Cx where x: 1 or 2 to select the I2C peripheral.
  * @param  __INTERRUPT__: specifies the I2C interrupt source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_INT_GPL_CEIE:
  *            @arg DMA_INT_IEA:
  *   
  * @retval The new state of __IT__ (TRUE or FALSE).
  */
#define __HAL_DMA_ENABLE_IT(__DMAx__, __INTERRUPT__)         ((__DMAx__)->Instance->CR0.W |= (__INTERRUPT__))
#define __HAL_DMA_DISABLE_IT(__DMAx__, __INTERRUPT__)        ((__DMAx__)->Instance->CR0.W &= (~(__INTERRUPT__)))
#define __HAL_DMA_GET_IT_SOURCE(__DMAx__, __INTERRUPT__)     ((((__DMAx__)->Instance->CR0.W & (__INTERRUPT__)) == (__INTERRUPT__)) ? SET : RESET)



/** @brief  Checks if the specified DMA GPL Channel Select.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __CHx__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_FLAG_ALL:
  *            @arg DMA_FLAG_GPL_CEF:
  *            @arg DMA_FLAG_CH2_ERRF: 
  *            @arg DMA_FLAG_CH2_THF: 
  *            @arg DMA_FLAG_CH2_TCF:
  *            @arg DMA_FLAG_CH2_GIF:
  *            @arg DMA_FLAG_CH1_ERRF: 
  *            @arg DMA_FLAG_CH1_THF: 
  *            @arg DMA_FLAG_CH1_TCF:
  *            @arg DMA_FLAG_CH1_GIF:
  *            @arg DMA_FLAG_CH0_ERRF: 
  *            @arg DMA_FLAG_CH0_THF: 
  *            @arg DMA_FLAG_CH0_TCF:
  *            @arg DMA_FLAG_CH0_GIF:
  *   
  * @retval 
  */
#define __HAL_DMA_GET_FLAG_STATE(__DMAx__, __FLAG__)    (((__DMAx__)->Instance->STA.W) & (__FLAG__))
#define __HAL_DMA_GET_FLAG(__DMAx__, __FLAG__)          ((((__DMAx__)->Instance->STA.W) & (__FLAG__)) == (__FLAG__))
#define __HAL_DMA_CLEAR_FLAG(__DMAx__, __FLAG__)        ((__DMAx__)->Instance->STA.W = (__FLAG__))

#define __DMA_Get_Flag_State(__DMAx__, __FLAG__)    (((__DMAx__)->STA.W) & (__FLAG__))
#define __DMA_Get_Flag(__DMAx__, __FLAG__)          ((((__DMAx__)->STA.W) & (__FLAG__)) == (__FLAG__))
#define __DMA_Clear_Flag(__DMAx__, __FLAG__)        ((__DMAx__)->STA.W = (__FLAG__))



/** @brief  Checks if the specified DMA GPL Channel Select.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __CHx__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_STATE_ALL:
  *            @arg DMA_STATE_INCOMPLETE:
  *            @arg DMA_STATE_BUSY: 
  *            @arg DMA_STATE_INITREADY: 
  *
  * @retval 
  */
#define __HAL_DMA_GET_STATE(__DMAx__, __STATE__)        (((__DMAx__)->Instance->STA.W) & (__FLAG__))



/** @brief  Checks if the specified DMA GPL Channel Select.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __CHx__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_GPL_DISABLE:
  *            @arg DMA_GPL_CHANNEL_0: 
  *            @arg DMA_GPL_CHANNEL_1: 
  *            @arg DMA_GPL_CHANNEL_2:
  *   
  * @retval 
  */
#define __HAL_DMA_GPL_CHANNEL_SELECT(__DMAx__, __CHx__)  (__DMAx__)->Instance->CR0.W = (((__DMAx__)->Instance->CR0.W &(~DMA_CR0_GPL_CHS_mask_w)) | (__CHx__))

#define __DMA_GPL_Channel_Select(__DMAx__, __CHx__)  (__DMAx__)->CR0.B[0] = (((__DMAx__)->CR0.B[0] &(~DMA_CR0_GPL_CHS_mask_b0)) | (__CHx__))



///** @brief  DMA Channel priority mode select. 
//  * @param  __DMAx__: specifies the DMA.
//  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
//  * @param  __MDS__: specifies the DMA GPL source to check.
//  *         This parameter can be one of the following values:
//  *            @arg DMA_PRIORITY_ROUND:
//  *            @arg DMA_PRIORITY_LEVEL: 
//  *   
//  * @retval 
//  */
#define __HAL_DMA_CHANNEL_PRIORITY(__DMAx__, __MDS__)   (__DMAx__)->Instance->CR0.W = (((__DMAx__)->Instance->CR0.W &(~DMA_CR0_PRI_MDS_mask_w)) | (__MDS__))
#define __DMA_Channel_Priority(__DMAx__, __MDS__)   (__DMAx__)->CR0.B[0] = (((__DMAx__)->CR0.B[0] &(~DMA_CR0_PRI_MDS_mask_b0)) | (__MDS__))



/** @brief  DMA Channel priority mode select. 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  *   
  * @retval 
  */
#define __HAL_DMA_ENABLE(__DMAx__)      (__DMAx__)->Instance->CR0.W |= DMA_CR0_EN_enable_w
#define __HAL_DMA_DISABLE(__DMAx__)     (__DMAx__)->Instance->CR0.W &= ~DMA_CR0_EN_enable_w

#define __DMA_Enable(__DMAx__)          (__DMAx__)->CR0.B[0] |= DMA_CR0_EN_enable_b0
#define __DMA_Disable(__DMAx__)         (__DMAx__)->CR0.B[0] &= ~DMA_CR0_EN_enable_b0


/** @brief  Checks if the specified DMA GPL Channel Select.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __CHx__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHANNEL_ENALBE_0:
  *            @arg DMA_CHANNEL_ENALBE_1: 
  *            @arg DMA_CHANNEL_ENALBE_2: 
  *   
  * @retval 
  */
#define __HAL_DMA_Channel_Enable(__DMAx__, __CHx__)  (__DMAx__)->CR0.W |= (__CHx__)
#define __HAL_DMA_Channel_Disable(__DMAx__, __CHx__)  (__DMAx__)->CR0.W &= ~(__CHx__)

#define __DMA_Channel_Enable(__DMAxCHx__)           (__DMAxCHx__)->CHxA.B[0] |= DMA_CHxA_CHx_EN_enable_b0
#define __DMA_Channel_Disable(__DMAxCHx__)          (__DMAxCHx__)->CHxA.B[0] &= ~DMA_CHxA_CHx_EN_mask_b0
#define __DMA_Channel_0_Enable(__DMAx__)            (__DMAx__)->CH0A.B[0] |= DMA_CH0A_CH0_EN_enable_b0
#define __DMA_Channel_0_Disable(__DMAx__)           (__DMAx__)->CH0A.B[0] &= ~DMA_CH0A_CH0_EN_mask_b0
#define __DMA_Channel_1_Enable(__DMAx__)            (__DMAx__)->CH1A.B[0] |= DMA_CH1A_CH1_EN_enable_b0
#define __DMA_Channel_1_Disable(__DMAx__)           (__DMAx__)->CH1A.B[0] &= ~DMA_CH1A_CH1_EN_mask_b0
#define __DMA_Channel_2_Enable(__DMAx__)            (__DMAx__)->CH2A.B[0] |= DMA_CH2A_CH2_EN_enable_b0
#define __DMA_Channel_2_Disable(__DMAx__)           (__DMAx__)->CH2A.B[0] &= ~DMA_CH2A_CH2_EN_mask_b0



//=====================================================================================================================


/** @brief  DMA channel data transfer request Enable / Disable 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  *   
  * @retval 
  */
#define __DMA_Channel_Request_Enable(__DMAxCHx__)   (__DMAxCHx__)->CHxA.B[1] |= DMA_CHxA_CHx_REQ_mask_b1
#define __DMA_Channel_Request_Disable(__DMAxCHx__)  (__DMAxCHx__)->CHxA.B[1] &= ~DMA_CHxA_CHx_REQ_mask_b1
#define __DMA_Channel_0_Request_Enable(__DMAx__)    (__DMAx__)->CH0A.B[1] |= DMA_CH0A_CH0_REQ_mask_b1
#define __DMA_Channel_0_Request_Disable(__DMAx__)   (__DMAx__)->CH0A.B[1] &= ~DMA_CH0A_CH0_REQ_mask_b1
#define __DMA_Channel_1_Request_Enable(__DMAx__)    (__DMAx__)->CH1A.B[1] |= DMA_CH1A_CH1_REQ_mask_b1
#define __DMA_Channel_1_Request_Disable(__DMAx__)   (__DMAx__)->CH1A.B[1] &= ~DMA_CH1A_CH1_REQ_mask_b1
#define __DMA_Channel_2_Request_Enable(__DMAx__)    (__DMAx__)->CH2A.B[1] |= DMA_CH2A_CH2_REQ_mask_b1
#define __DMA_Channel_2_Request_Disable(__DMAx__)   (__DMAx__)->CH2A.B[1] &= ~DMA_CH2A_CH2_REQ_mask_b1



/** @brief  DMA transfer burst size.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __BSIZE__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_BSIZE_1:
  *            @arg DMA_CHx_BSIZE_2: 
  *            @arg DMA_CHx_BSIZE_4: 
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_Burst_Size(__DMAxCHx__, __BSIZE__) (__DMAxCHx__)->Instance->CHxA.W = (((__DMAxCHx__)->Instance->CHxA.W &(~DMA_CHx_BSIZE_MASK)) | (__BSIZE__))
#define __HAL_DMA_Channel_Burst_Size(__DMAxCHx__, __BSIZE__) (__DMAxCHx__)->Instance->CHxA.W = (((__DMAxCHx__)->Instance->CHxA.W &(~DMA_CHx_BSIZE_MASK)) | (__BSIZE__))

#define __DMA_Channel_Burst_Size(__DMAxCHx__, __BSIZE__) (__DMAxCHx__)->CHxA.B[1] = (((__DMAxCHx__)->CHxA.B[1] &(~DMA_CHx_BSIZE_MASK)) | (__BSIZE__))
#define __DMA_Channel_0_Burst_Size(__DMAx__, __BSIZE__) (__DMAx__)->CH0A.B[1] = (((__DMAx__)->CH0A.B[1] &(~DMA_CHx_BSIZE_MASK)) | (__BSIZE__))
#define __DMA_Channel_1_Burst_Size(__DMAx__, __BSIZE__) (__DMAx__)->CH1A.B[1] = (((__DMAx__)->CH1A.B[1] &(~DMA_CHx_BSIZE_MASK)) | (__BSIZE__))
#define __DMA_Channel_2_Burst_Size(__DMAx__, __BSIZE__) (__DMAx__)->CH2A.B[1] = (((__DMAx__)->CH2A.B[1] &(~DMA_CHx_BSIZE_MASK)) | (__BSIZE__))



/** @brief  DMA Channel external trigger pin select.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __SELECT__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_XPIN_TRG0:
  *            @arg DMA_CHx_XPIN_TRG1:
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_Ext_PIN(__DMAxCHx__, __SELECT__) (__DMAxCHx__)->Instance->CHxB.W = (((__DMAxCHx__)->Instance->CHxB.W &(~DMA_CHx_XPIN_MASK)) | (__SELECT__))
#define __HAL_DMA_Channel_Ext_PIN(__DMAxCHx__, __SELECT__) (__DMAxCHx__)->Instance->CHxB.W = (((__DMAxCHx__)->Instance->CHxB.W &(~DMA_CHx_XPIN_MASK)) | (__SELECT__))

#define __DMA_Channel_Ext_PIN(__DMAxCHx__, __SELECT__) (__DMAxCHx__)->CHxB.B[3] = (((__DMAxCHx__)->CHxB.B[3] &(~DMA_CHx_XPIN_MASK)) | (__SELECT__))
#define __DMA_Channel_0_Ext_PIN(__DMAx__, __SELECT__)  (__DMAx__)->CH0B.B[3] = (((__DMAx__)->CH0B.B[3] &(~DMA_CHx_XPIN_MASK)) | (__SELECT__))
#define __DMA_Channel_1_Ext_PIN(__DMAx__, __SELECT__)  (__DMAx__)->CH1B.B[3] = (((__DMAx__)->CH1B.B[3] &(~DMA_CHx_XPIN_MASK)) | (__SELECT__))
#define __DMA_Channel_2_Ext_PIN(__DMAx__, __SELECT__)  (__DMAx__)->CH2B.B[3] = (((__DMAx__)->CH2B.B[3] &(~DMA_CHx_XPIN_MASK)) | (__SELECT__))



/** @brief  DMA Channel external pin trigger request mode select.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __XMDS__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_XMDS_DISABLE:
  *            @arg DMA_CHx_XMDS_SINGLE:
  *            @arg DMA_CHx_XMDS_BLOCK:
  *            @arg DMA_CHx_XMDS_DENAND: 
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_Ext_REQ_Mode(__DMAxCHx__, __XMDS__); (__DMAxCHx__)->Instance->CHxA.W = (((__DMAxCHx__)->Instance->CHxA.W &(~DMA_CHx_XMDS_MASK)) | (__XMDS__))
#define __HAL_DMA_Channel_Ext_REQ_Mode(__DMAxCHx__, __XMDS__); (__DMAxCHx__)->Instance->CHxA.W = (((__DMAxCHx__)->Instance->CHxA.W &(~DMA_CHx_XMDS_MASK)) | (__XMDS__))

#define __DMA_Channel_Ext_REQ_Mode(__DMAxCHx__, __XMDS__); (__DMAxCHx__)->CHxA.B[1] = (((__DMAxCHx__)->CHxA.B[1] &(~DMA_CHx_XMDS_MASK)) | (__XMDS__))
#define __DMA_Channel_0_Ext_REQ_Mode(__DMAx__, __XMDS__); (__DMAx__)->CH0A.B[1] = (((__DMAx__)->CH0A.B[1] &(~DMA_CHx_XMDS_MASK)) | (__XMDS__))
#define __DMA_Channel_1_Ext_REQ_Mode(__DMAx__, __XMDS__); (__DMAx__)->CH1A.B[1] = (((__DMAx__)->CH1A.B[1] &(~DMA_CHx_XMDS_MASK)) | (__XMDS__))
#define __DMA_Channel_2_Ext_REQ_Mode(__DMAx__, __XMDS__); (__DMAx__)->CH2A.B[1] = (((__DMAx__)->CH2A.B[1] &(~DMA_CHx_XMDS_MASK)) | (__XMDS__))



/** @brief  DMA address increased mode select.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __ADSEL__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_ADSEL_NORMAL:
  *            @arg DMA_CHx_ADSEL_SKIP3:
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_ADSEL(__DMAxCHx__, __ADSEL__); (__DMAxCHx__)->Instance->CHxA.W = (((__DMAxCHx__)->Instance->CHxA.W &(~DMA_CHx_ADSEL_MASK)) | (__ADSEL__))
#define __HAL_DMA_Channel_ADSEL(__DMAxCHx__, __ADSEL__); (__DMAxCHx__)->Instance->CHxA.W = (((__DMAxCHx__)->Instance->CHxA.W &(~DMA_CHx_ADSEL_MASK)) | (__ADSEL__))

#define __DMA_Channel_ADSEL(__DMAxCHx__, __ADSEL__); (__DMAxCHx__)->CHxA.B[0] = (((__DMAxCHx__)->CHxA.B[0] &(~DMA_CHx_ADSEL_MASK)) | (__ADSEL__))
#define __DMA_Channel_0_ADSEL(__DMAx__, __ADSEL__); (__DMAx__)->CH0A.B[0] = (((__DMAx__)->CH0A.B[0] &(~DMA_CHx_ADSEL_MASK)) | (__ADSEL__))
#define __DMA_Channel_1_ADSEL(__DMAx__, __ADSEL__); (__DMAx__)->CH1A.B[0] = (((__DMAx__)->CH1A.B[0] &(~DMA_CHx_ADSEL_MASK)) | (__ADSEL__))
#define __DMA_Channel_2_ADSEL(__DMAx__, __ADSEL__); (__DMAx__)->CH2A.B[0] = (((__DMAx__)->CH2A.B[0] &(~DMA_CHx_ADSEL_MASK)) | (__ADSEL__))



/** @brief  DMA destination or peripheral transfer address auto increased Enable.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_LOOP_Enable(__DMAxCHx__)     (__DMAxCHx__)->Instance->CHxA.W |= DMA_CHxA_CHx_LOOP_enable_w
#define __HAL_DMA_CHx_LOOP_Disable(__DMAxCHx__)    (__DMAxCHx__)->Instance->CHxA.W &= ~DMA_CHxA_CHx_LOOP_mask_w
#define __HAL_DMA_Channel_LOOP_Enable(__DMAxCHx__)     (__DMAxCHx__)->Instance->CHxA.W |= DMA_CHxA_CHx_LOOP_enable_w
#define __HAL_DMA_Channel_LOOP_Disable(__DMAxCHx__)    (__DMAxCHx__)->Instance->CHxA.W &= ~DMA_CHxA_CHx_LOOP_mask_w

#define __DMA_Channel_LOOP_Enable(__DMAxCHx__)     (__DMAxCHx__)->CHxA.B[0] |= DMA_CHxA_CHx_LOOP_enable_b0
#define __DMA_Channel_LOOP_Disable(__DMAxCHx__)    (__DMAxCHx__)->CHxA.B[0] &= ~DMA_CHxA_CHx_LOOP_mask_b0
#define __DMA_Channel_0_LOOP_Enable(__DMAx__)      (__DMAx__)->CH0A.B[0] |= DMA_CH0A_CH0_LOOP_enable_b0
#define __DMA_Channel_0_LOOP_Disable(__DMAx__)     (__DMAx__)->CH0A.B[0] &= ~DMA_CH0A_CH0_LOOP_mask_b0
#define __DMA_Channel_1_LOOP_Enable(__DMAx__)      (__DMAx__)->CH1A.B[0] |= DMA_CH1A_CH1_LOOP_enable_b0
#define __DMA_Channel_1_LOOP_Disable(__DMAx__)     (__DMAx__)->CH1A.B[0] &= ~DMA_CH1A_CH1_LOOP_mask_b0
#define __DMA_Channel_2_LOOP_Enable(__DMAx__)      (__DMAx__)->CH2A.B[0] |= DMA_CH2A_CH2_LOOP_enable_b0
#define __DMA_Channel_2_LOOP_Disable(__DMAx__)     (__DMAx__)->CH2A.B[0] &= ~DMA_CH2A_CH2_LOOP_mask_b0



/** @brief  DMA destination or peripheral transfer address auto increased Enable.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_DINC_ENABLE(__DMAxCHx__)  (__DMAxCHx__)->Instance->CHxB.W = (__DMAxCHx__)->Instance->CHxB.W |= DMA_CHxB_CHx_DINC_enable_w
#define __HAL_DMA_CHx_DINC_DISABLE(__DMAxCHx__) (__DMAxCHx__)->Instance->CHxB.W = (__DMAxCHx__)->Instance->CHxB.W &= ~DMA_CHxB_CHx_DINC_mask_w
#define __HAL_DMA_Channel_DINC_ENABLE(__DMAxCHx__)  (__DMAxCHx__)->Instance->CHxB.W = (__DMAxCHx__)->Instance->CHxB.W |= DMA_CHxB_CHx_DINC_enable_w
#define __HAL_DMA_Channel_DINC_DISABLE(__DMAxCHx__) (__DMAxCHx__)->Instance->CHxB.W = (__DMAxCHx__)->Instance->CHxB.W &= ~DMA_CHxB_CHx_DINC_mask_w

#define __DMA_Channel_DINC_Enable(__DMAxCHx__)  (__DMAxCHx__)->CHxB.B[2] = (__DMAxCHx__)->CHxB.B[2] |= DMA_CHxB_CHx_DINC_enable_b2
#define __DMA_Channel_DINC_Disable(__DMAxCHx__) (__DMAxCHx__)->CHxB.B[2] = (__DMAxCHx__)->CHxB.B[2] &= ~DMA_CHxB_CHx_DINC_mask_b2
#define __DMA_Channel_0_DINC_Enable(__DMAx__)   (__DMAx__)->CH0B.B[2] = (__DMAx__)->CH0B.B[2] |= DMA_CH0B_CH0_DINC_enable_b2
#define __DMA_Channel_0_DINC_Disable(__DMAx__)  (__DMAx__)->CH0B.B[2] = (__DMAx__)->CH0B.B[2] &= ~DMA_CH0B_CH0_DINC_mask_b2
#define __DMA_Channel_1_DINC_Enable(__DMAx__)   (__DMAx__)->CH1B.B[2] = (__DMAx__)->CH1B.B[2] |= DMA_CH1B_CH1_DINC_enable_b2
#define __DMA_Channel_1_DINC_Disable(__DMAx__)  (__DMAx__)->CH1B.B[2] = (__DMAx__)->CH1B.B[2] &= ~DMA_CH1B_CH1_DINC_mask_b2
#define __DMA_Channel_2_DINC_Enable(__DMAx__)   (__DMAx__)->CH2B.B[2] = (__DMAx__)->CH2B.B[2] |= DMA_CH2B_CH2_DINC_enable_b2
#define __DMA_Channel_2_DINC_Disable(__DMAx__)  (__DMAx__)->CH2B.B[2] = (__DMAx__)->CH2B.B[2] &= ~DMA_CH2B_CH2_DINC_mask_b2



/** @brief  DMA source or memory transfer address auto increased Enable. 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_SINC_ENABLE(__DMAxCHx__)  (__DMAxCHx__)->Instance->CHxB.W = (__DMAxCHx__)->Instance->CHxB.W |= DMA_CHxB_CHx_SINC_enable_w
#define __HAL_DMA_CHx_SINC_DISABLE(__DMAxCHx__) (__DMAxCHx__)->Instance->CHxB.W = (__DMAxCHx__)->Instance->CHxB.W &= ~DMA_CHxB_CHx_SINC_mask_w
#define __HAL_DMA_Channel_SINC_ENABLE(__DMAxCHx__)  (__DMAxCHx__)->Instance->CHxB.W = (__DMAxCHx__)->Instance->CHxB.W |= DMA_CHxB_CHx_SINC_enable_w
#define __HAL_DMA_Channel_SINC_DISABLE(__DMAxCHx__) (__DMAxCHx__)->Instance->CHxB.W = (__DMAxCHx__)->Instance->CHxB.W &= ~DMA_CHxB_CHx_SINC_mask_w

#define __DMA_Channel_SINC_Enable(__DMAxCHx__)  (__DMAxCHx__)->CHxB.B[2] = (__DMAxCHx__)->CHxB.B[2] |= DMA_CHxB_CHx_SINC_enable_b2
#define __DMA_Channel_SINC_Disable(__DMAxCHx__) (__DMAxCHx__)->CHxB.B[2] = (__DMAxCHx__)->CHxB.B[2] &= ~DMA_CHxB_CHx_SINC_mask_b2
#define __DMA_Channel_0_SINC_Enable(__DMAx__)   (__DMAx__)->CH0B.B[2] = (__DMAx__)->CH0B.B[2] |= DMA_CH0B_CH0_SINC_enable_b2
#define __DMA_Channel_0_SINC_Disable(__DMAx__)  (__DMAx__)->CH0B.B[2] = (__DMAx__)->CH0B.B[2] &= ~DMA_CH0B_CH0_SINC_mask_b2
#define __DMA_Channel_1_SINC_Enable(__DMAx__)   (__DMAx__)->CH1B.B[2] = (__DMAx__)->CH1B.B[2] |= DMA_CH1B_CH1_SINC_enable_b2
#define __DMA_Channel_1_SINC_Disable(__DMAx__)  (__DMAx__)->CH1B.B[2] = (__DMAx__)->CH1B.B[2] &= ~DMA_CH1B_CH1_SINC_mask_b2
#define __DMA_Channel_2_SINC_Enable(__DMAx__)   (__DMAx__)->CH2B.B[2] = (__DMAx__)->CH2B.B[2] |= DMA_CH2B_CH2_SINC_enable_b2
#define __DMA_Channel_2_SINC_Disable(__DMAx__)  (__DMAx__)->CH2B.B[2] = (__DMAx__)->CH2B.B[2] &= ~DMA_CH2B_CH2_SINC_mask_b2



/** @brief  DMA Channel transfer peripheral destination select. 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __SELECT__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_DET_Memory:
  *            @arg DMA_CHx_DET_DAC0:
  *            @arg DMA_CHx_DET_I2C0:
  *            @arg DMA_CHx_DET_I2C1:
  *            @arg DMA_CHx_DET_URT0:
  *            @arg DMA_CHx_DET_URT1:
  *            @arg DMA_CHx_DET_URT2:
  *            @arg DMA_CHx_DET_URT3:
  *            @arg DMA_CHx_DET_SPI0:
  *            @arg DMA_CHx_DET_GPL:
  *            @arg DMA_CHx_DET_TM36_CC0B:
  *            @arg DMA_CHx_DET_TM36_CC1B:
  *            @arg DMA_CHx_DET_TM36_CC2B:
  *
  * @retval 
  */
#define __HAL_DMA_CHx_DET(__DMAxCHx__, __SELECT__) (__DMAxCHx__)->Instance->CHxB.W = (((__DMAxCHx__)->Instance->CHxB.W &(~DMA_CHx_DET_MASK)) | (__SELECT__))
#define __HAL_DMA_Channel_DET(__DMAxCHx__, __SELECT__) (__DMAxCHx__)->Instance->CHxB.W = (((__DMAxCHx__)->Instance->CHxB.W &(~DMA_CHx_DET_MASK)) | (__SELECT__))

#define __DMA_Channel_DET(__DMAxCHx__, __SELECT__) (__DMAxCHx__)->CHxB.B[1] = (__SELECT__)
#define __DMA_Channel_0_DET(__DMAx__, __SELECT__) (__DMAx__)->CH0B.B[1] = (__SELECT__)
#define __DMA_Channel_1_DET(__DMAx__, __SELECT__) (__DMAx__)->CH1B.B[1] = (__SELECT__)
#define __DMA_Channel_2_DET(__DMAx__, __SELECT__) (__DMAx__)->CH2B.B[1] = (__SELECT__)



/** @brief  DMA Channel transfer peripheral destination select. 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __SELECT__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_SRC_Memory
  *            @arg DMA_CHx_SRC_ADC0
  *            @arg DMA_CHx_SRC_I2C0
  *            @arg DMA_CHx_SRC_I2C1
  *            @arg DMA_CHx_SRC_URT0
  *            @arg DMA_CHx_SRC_URT1
  *            @arg DMA_CHx_SRC_URT2
  *            @arg DMA_CHx_SRC_URT3
  *            @arg DMA_CHx_SRC_SPI0
  *            @arg DMA_CHx_SRC_TM36_IC3
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_SRC(__DMAxCHx__, __SELECT__)      (__DMAxCHx__)->Instance->CHxB.W = (((__DMAxCHx__)->Instance->CHxB.W &(~DMA_CHx_SRC_MASK)) | (__SELECT__))
#define __HAL_DMA_GET_CHx_SRC(__DMAxCHx__)              (((__DMAxCHx__)->Instance->CHxB.W & DMA_CHx_SRC_MASK))
#define __HAL_DMA_Channel_SRC(__DMAxCHx__, __SELECT__)  (__DMAxCHx__)->Instance->CHxB.W = (((__DMAxCHx__)->Instance->CHxB.W &(~DMA_CHx_SRC_MASK)) | (__SELECT__))
#define __HAL_DMA_GET_Channel_SRC(__DMAxCHx__)          (((__DMAxCHx__)->Instance->CHxB.W & DMA_CHx_SRC_MASK))

#define __DMA_Channel_SRC(__DMAxCHx__, __SELECT__) (__DMAxCHx__)->CHxB.B[0] = (__SELECT__)
#define __DMA_Get_Channel_SRC(__DMAxCHx__) (((__DMAxCHx__)->CHxB.B[0] & DMA_CHx_SRC_MASK))
#define __DMA_Channel_0_SRC(__DMAx__, __SELECT__) (__DMAx__)->CH0B.B[0] = (__SELECT__)
#define __DMA_Channel_1_SRC(__DMAx__, __SELECT__) (__DMAx__)->CH1B.B[0] = (__SELECT__)
#define __DMA_Channel_2_SRC(__DMAx__, __SELECT__) (__DMAx__)->CH2B.B[0] = (__SELECT__)



/** @brief  DMA transfer data count initial number. 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __LENTH__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *   
  * @retval 
  */
#define __HAL_DMA_GET_CHx_Data_Lenth(__DMAxCHx__) (__DMAxCHx__)->Instance->CHxNUM.H[0]
#define __HAL_DMA_GET_Channel_Data_Lenth(__DMAxCHx__) (__DMAxCHx__)->Instance->CHxNUM.H[0]
#define __HAL_DMA_CHx_Data_Lenth(__DMAxCHx__, __LENTH__) (__DMAxCHx__)->Instance->CHxNUM.H[0] = (__LENTH__)
#define __HAL_DMA_Channel_Data_Lenth(__DMAxCHx__, __LENTH__) (__DMAxCHx__)->Instance->CHxNUM.H[0] = (__LENTH__)

#define __DMA_Get_CHx_Data_Lenth(__DMAxCHx__) (__DMAxCHx__)->CHxNUM.H[0]
#define __DMA_Get_Channel_Data_Lenth(__DMAxCHx__) (__DMAxCHx__)->CHxNUM.H[0]
#define __DMA_CHx_Data_Lenth(__DMAxCHx__, __LENTH__) (__DMAxCHx__)->CHxNUM.H[0] = (__LENTH__)
#define __DMA_Channel_Data_Lenth(__DMAxCHx__, __LENTH__) (__DMAxCHx__)->CHxNUM.H[0] = (__LENTH__)
#define __DMA_Channel_0_Data_Lenth(__DMAx__, __LENTH__) (__DMAx__)->CH0NUM.H[0] = (__LENTH__)
#define __DMA_Channel_1_Data_Lenth(__DMAx__, __LENTH__) (__DMAx__)->CH1NUM.H[0] = (__LENTH__)
#define __DMA_Channel_2_Data_Lenth(__DMAx__, __LENTH__) (__DMAx__)->CH2NUM.H[0] = (__LENTH__)



/** @brief  DMA source or memory transfer start address. 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __ADDRESS__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_SRC_ADDR(__DMAxCHx__, __ADDRESS__) (__DMAxCHx__)->Instance->CHxSSA.W = (__ADDRESS__)
#define __HAL_DMA_Channel_SRC_ADDR(__DMAxCHx__, __ADDRESS__) (__DMAxCHx__)->Instance->CHxSSA.W = (__ADDRESS__)

#define __DMA_CHx_SRC_ADDR(__DMAxCHx__, __ADDRESS__) (__DMAxCHx__)->CHxSSA.W = (__ADDRESS__)
#define __DMA_Channel_SRC_ADDR(__DMAxCHx__, __ADDRESS__) (__DMAxCHx__)->CHxSSA.W = (__ADDRESS__)
#define __DMA_Channel_0_SRC_ADDR(__DMAx__, __ADDRESS__) (__DMAx__)->CH0SSA.W = (__ADDRESS__)
#define __DMA_Channel_1_SRC_ADDR(__DMAx__, __ADDRESS__) (__DMAx__)->CH1SSA.W = (__ADDRESS__)
#define __DMA_Channel_2_SRC_ADDR(__DMAx__, __ADDRESS__) (__DMAx__)->CH2SSA.W = (__ADDRESS__)



/** @brief  DMA destination or peripheral transfer start address.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __ADDRESS__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_DET_ADDR(__DMAxCHx__, __ADDRESS__)        (__DMAxCHx__)->Instance->CHxDSA.W = (__ADDRESS__)
#define __HAL_DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__)    (__DMAxCHx__)->Instance->CHxDSA.W = (__ADDRESS__)

#define __DMA_CHx_DET_ADDR(__DMAxCHx__, __ADDRESS__) (__DMAxCHx__)->CHxDSA.W = (__ADDRESS__)
#define __DMA_Channel_DET_ADDR(__DMAxCHx__, __ADDRESS__) (__DMAxCHx__)->CHxDSA.W = (__ADDRESS__)
#define __DMA_Channel_0_DET_ADDR(__DMAx__, __ADDRESS__) (__DMAx__)->CH0DSA.W = (__ADDRESS__)
#define __DMA_Channel_1_DET_ADDR(__DMAx__, __ADDRESS__) (__DMAx__)->CH1DSA.W = (__ADDRESS__)
#define __DMA_Channel_2_DET_ADDR(__DMAx__, __ADDRESS__) (__DMAx__)->CH2DSA.W = (__ADDRESS__)



/** @brief  DMA Channel priority level select.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __PLevel__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_PLS_LEVEL_0:
  *            @arg DMA_CHx_PLS_LEVEL_1: 
  *            @arg DMA_CHx_PLS_LEVEL_2: 
  *            @arg DMA_CHx_PLS_LEVEL_3: 
  *            @arg DMA_CHx_PLS_LOWSET: 
  *            @arg DMA_CHx_PLS_MIDDLE: 
  *            @arg DMA_CHx_PLS_HIGH: 
  *            @arg DMA_CHx_PLS_HIGHSET: 
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_Priority_Level(__DMAxCHx__, __PLevel__);      (__DMAxCHx__)->Instance->CHxA.W = (((__DMAxCHx__)->Instance->CHxA.W &(~DMA_CHx_PLS_MASK)) | (__PLevel__))
#define __HAL_DMA_Channel_Priority_Level(__DMAxCHx__, __PLevel__);  (__DMAxCHx__)->Instance->CHxA.W = (((__DMAxCHx__)->Instance->CHxA.W &(~DMA_CHx_PLS_MASK)) | (__PLevel__))

#define __DMA_Channel_Priority_Level(__DMAxCHx__, __PLevel__); (__DMAxCHx__)->CHxA.B[1] = (((__DMAxCHx__)->CHxA.B[1] &(~DMA_CHx_PLS_MASK)) | (__PLevel__))
#define __DMA_Channel_0_Priority_Level(__DMAx__, __PLevel__); (__DMAx__)->CH0A.B[1] = (((__DMAx__)->CH0A.B[1] &(~DMA_CHx_PLS_MASK)) | (__PLevel__))
#define __DMA_Channel_1_Priority_Level(__DMAx__, __PLevel__); (__DMAx__)->CH1A.B[1] = (((__DMAx__)->CH1A.B[1] &(~DMA_CHx_PLS_MASK)) | (__PLevel__))
#define __DMA_Channel_2_Priority_Level(__DMAx__, __PLevel__); (__DMAx__)->CH2A.B[1] = (((__DMAx__)->CH2A.B[1] &(~DMA_CHx_PLS_MASK)) | (__PLevel__))


/** @brief  Checks if the specified I2C interrupt source is enabled or disabled.
  * @param  __HANDLE__: specifies the I2C Handle.
  *         This parameter can be I2Cx where x: 1 or 2 to select the I2C peripheral.
  * @param  __INTERRUPT__: specifies the I2C interrupt source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_IT_EIE:
  *            @arg DMA_CHx_IT_HIE:
  *            @arg DMA_CHx_IT_CIE:
  *   
  * @retval The new state of __IT__ (TRUE or FALSE).
  */
#define __HAL_DMA_CHx_ENABLE_IT(__DMAxCHx__, __INTERRUPT__)         ((__DMAxCHx__)->Instance->CHxA.B[2] |= (__INTERRUPT__))
#define __HAL_DMA_CHx_DISABLE_IT(__DMAxCHx__, __INTERRUPT__)        ((__DMAxCHx__)->Instance->CHxA.B[2] &= (~(__INTERRUPT__)))
#define __HAL_DMA_CHx_GET_IT_SOURCE(__DMAxCHx__, __INTERRUPT__)     ((((__DMAxCHx__)->Instance->CHxA.B[2] & (__INTERRUPT__)) == (__INTERRUPT__)) ? SET : RESET)
#define __HAL_DMA_CHANNEL_ENABLE_IT(__DMAxCHx__, __INTERRUPT__)     (__DMAxCHx__)->Instance->CHxA.B[2] |= (__INTERRUPT__)
#define __HAL_DMA_CHANNEL_DISABLE_IT(__DMAxCHx__, __INTERRUPT__)    (__DMAxCHx__)->Instance->CHxA.B[2] &= ~(__INTERRUPT__)
#define __HAL_DMA_CHANNEL_GET_IT_SOURCE(__DMAxCHx__, __INTERRUPT__) ((((__DMAxCHx__)->Instance->CH0A.B[2] & (__INTERRUPT__)) == (__INTERRUPT__)) ? SET : RESET)

#define __DMA_Channel_Enable_IT(__DMAxCHx__, __INTERRUPT__)   (__DMAxCHx__)->CHxA.B[2] |= (__INTERRUPT__)
#define __DMA_Channel_Disable_IT(__DMAxCHx__, __INTERRUPT__)  (__DMAxCHx__)->CHxA.B[2] &= ~(__INTERRUPT__)
#define __DMA_Channel_0_Enable_IT(__DMAx__, __INTERRUPT__)    (__DMAx__)->CH0A.B[2] |= (__INTERRUPT__)
#define __DMA_Channel_0_Disable_IT(__DMAx__, __INTERRUPT__)   (__DMAx__)->CH0A.B[2] &= ~(__INTERRUPT__)
#define __DMA_Channel_1_Enable_IT(__DMAx__, __INTERRUPT__)    (__DMAx__)->CH1A.B[2] |= (__INTERRUPT__)
#define __DMA_Channel_1_Disable_IT(__DMAx__, __INTERRUPT__)   (__DMAx__)->CH1A.B[2] &= ~(__INTERRUPT__)
#define __DMA_Channel_2_Enable_IT(__DMAx__, __INTERRUPT__)    (__DMAx__)->CH2A.B[2] |= (__INTERRUPT__)
#define __DMA_Channel_2_Disable_IT(__DMAx__, __INTERRUPT__)   (__DMAx__)->CH2A.B[2] &= ~(__INTERRUPT__)



/** @brief  Checks if the specified DMA GPL Channel Select.
  * @param  __DMAxCHx__: specifies the DMA Channel.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __CHx__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *            @arg DMA_CHx_FLAG_ERRF: 
  *            @arg DMA_CHx_FLAG_THF: 
  *            @arg DMA_CHx_FLAG_TCF:
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_GET_ALL_FLAG(__DMAxCHx__)             (((__DMAxCHx__)->Instance->CHxA.B[3]) & (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF))
#define __HAL_DMA_CHx_GET_FLAG(__DMAxCHx__, __FLAG__)       ((((__DMAxCHx__)->Instance->CHxA.B[3]) & (__FLAG__)) == (__FLAG__))
#define __HAL_DMA_CHx_CLEAR_FLAG(__DMAxCHx__, __FLAG__)     ((__DMAxCHx__)->Instance->CHxA.B[3] = (__FLAG__))
#define __HAL_DMA_CHANNEL_GET_ALL_FLAG(__DMAxCHx__)         (((__DMAxCHx__)->Instance->CHxA.B[3]) & (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF))
#define __HAL_DMA_CHANNEL_GET_FLAG(__DMAxCHx__, __FLAG__)   ((((__DMAxCHx__)->Instance->CHxA.B[3]) & (__FLAG__)) == (__FLAG__))
#define __HAL_DMA_CHANNEL_CLEAR_FLAG(__DMAxCHx__, __FLAG__) ((__DMAxCHx__)->Instance->CHxA.B[3] = (__FLAG__))

#define __DMA_CHx_Get_All_Flag(__DMAxCHx__) (((__DMAxCHx__)->CHxA.B[3]) & (DMA_CHx_FLAG_ERRF | DMA_CHx_FLAG_THF | DMA_CHx_FLAG_TCF))
#define __DMA_CHx_Get_Flag(__DMAxCHx__, __FLAG__)   ((((__DMAxCHx__)->CHxA.B[3]) & (__FLAG__)) == (__FLAG__))
#define __DMA_CHx_Clear_Flag(__DMAxCHx__, __FLAG__) ((__DMAxCHx__)->CHxA.B[3] = (__FLAG__))

#define __DMA_CHx_WaitComplete(__DMAxCHx__)     while((((__DMAxCHx__)->CHxA.B[3]) & DMA_CHx_FLAG_TCF) == 0)

/** @brief  DMA Channel operation Enable.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_ENABLE(__DMAxCHx__)       (__DMAxCHx__)->Instance->CHxA.B[0] |= DMA_CHxA_CHx_EN_enable_b0
#define __HAL_DMA_CHx_DISABLE(__DMAxCHx__)      (__DMAxCHx__)->Instance->CHxA.B[0] &= ~DMA_CHxA_CHx_EN_mask_b0
#define __HAL_DMA_CHANNEL_ENABLE(__DMAxCHx__)   (__DMAxCHx__)->Instance->CHxA.B[0] |= DMA_CHxA_CHx_EN_enable_b0
#define __HAL_DMA_CHANNEL_DISABLE(__DMAxCHx__)  (__DMAxCHx__)->Instance->CHxA.B[0] &= ~DMA_CHxA_CHx_EN_mask_b0



/** @brief  DMA Channel data transfer request Enable / Disable 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_REQUEST_ENABLE(__DMAxCHx__)       (__DMAxCHx__)->Instance->CHxA.B[1] |= DMA_CH0A_CH0_REQ_mask_b1
#define __HAL_DMA_CHx_REQUEST_DISABLE(__DMAxCHx__)      (__DMAxCHx__)->Instance->CHxA.B[1] &= ~DMA_CH0A_CH0_REQ_mask_b1
#define __HAL_DMA_CHANNEL_REQUEST_ENABLE(__DMAxCHx__)   (__DMAxCHx__)->Instance->CHxA.B[1] |= DMA_CH0A_CH0_REQ_mask_b1
#define __HAL_DMA_CHANNEL_REQUEST_DISABLE(__DMAxCHx__)  (__DMAxCHx__)->Instance->CHxA.B[1] &= ~DMA_CH0A_CH0_REQ_mask_b1



/** @brief  DMA Channel operation hold Enable.
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  *   
  * @retval 
  */
#define __HAL_DMA_CHx_HOLD_ENABLE(__DMAxCHx__)          (__DMAxCHx__)->Instance->CHxA.B[0] |= DMA_CHxA_CHx_HOLD_enable_b0
#define __HAL_DMA_CHx_HOLD_DISABLE(__DMAxCHx__)         (__DMAxCHx__)->Instance->CHxA.B[0] &= ~DMA_CHxA_CHx_HOLD_mask_b0
#define __HAL_DMA_CHANNEL_HOLD_ENABLE(__DMAxCHx__)      (__DMAxCHx__)->Instance->CHxA.B[0] |= DMA_CHxA_CHx_HOLD_enable_b0
#define __HAL_DMA_CHANNEL_HOLD_DISABLE(__DMAxCHx__)     (__DMAxCHx__)->Instance->CHxA.B[0] &= ~DMA_CHxA_CHx_HOLD_mask_b0

#define __DMA_Channel_HOLD_Enable(__DMAxCHx__)     (__DMAxCHx__)->CHxA.B[0] |= DMA_CHxA_CHx_HOLD_enable_b0
#define __DMA_Channel_HOLD_Disable(__DMAxCHx__)    (__DMAxCHx__)->CHxA.B[0] &= ~DMA_CHxA_CHx_HOLD_mask_b0
#define __DMA_Channel_0_HOLD_Enable(__DMAx__)      (__DMAx__)->CH0A.B[0] |= DMA_CH0A_CH0_HOLD_enable_b0
#define __DMA_Channel_0_HOLD_Disable(__DMAx__)     (__DMAx__)->CH0A.B[0] &= ~DMA_CH0A_CH0_HOLD_mask_b0
#define __DMA_Channel_1_HOLD_Enable(__DMAx__)      (__DMAx__)->CH1A.B[0] |= DMA_CH1A_CH1_HOLD_enable_b0
#define __DMA_Channel_1_HOLD_Disable(__DMAx__)     (__DMAx__)->CH1A.B[0] &= ~DMA_CH1A_CH1_HOLD_mask_b0
#define __DMA_Channel_2_HOLD_Enable(__DMAx__)      (__DMAx__)->CH2A.B[0] |= DMA_CH2A_CH2_HOLD_enable_b0
#define __DMA_Channel_2_HOLD_Disable(__DMAx__)     (__DMAx__)->CH2A.B[0] &= ~DMA_CH2A_CH2_HOLD_mask_b0



/** @brief  DMA transfer data count initial number. 
  * @param  __DMAx__: specifies the DMA.
  *         This parameter can be DMAx where x: 1 or 2 to select the DMA peripheral.
  * @param  __LENTH__: specifies the DMA GPL source to check.
  *         This parameter can be one of the following values:
  *   
  * @retval 
  */
#define __HAL_DMA_GET_CHx_DATA_COUNT(__DMAxCHx__) (__DMAxCHx__)->Instance->CHxCNT.H[0]
#define __HAL_DMA_GET_Channel_DATA_COUNT(__DMAxCHx__) (__DMAxCHx__)->Instance->CHxCNT.H[0]

#define __DMA_Get_CHx_Data_Count(__DMAxCHx__) (__DMAxCHx__)->CHxCNT.H[0]
#define __DMA_Get_Channel_Data_Count(__DMAxCHx__) (__DMAxCHx__)->CHxCNT.H[0]
#define __DMA_Get_Channel_0_Data_Count(__DMAx__) (__DMAx__)->CHxCNT.H[0]
#define __DMA_Get_Channel_1_Data_Count(__DMAx__) (__DMAx__)->CHxCNT.H[0]
#define __DMA_Get_Channel_2_Data_Count(__DMAx__) (__DMAx__)->CHxCNT.H[0]



//=============================================================================

void HAL_DMA_IRQHandler(DMA_HandleTypeDef *DMAx);

void HAL_DMA_MspInit(DMA_HandleTypeDef *DMAx);
void HAL_DMA_MspDeInit(DMA_HandleTypeDef *DMAx);

HAL_StatusTypeDef HAL_DMA_Init(DMA_HandleTypeDef *DMAx);
HAL_StatusTypeDef HAL_DMA_DeInit(DMA_HandleTypeDef *DMAx);

//HAL_StatusTypeDef HAL_DMA_ConfigChannel(DMA_HandleTypeDef *DMAx);

//HAL_StatusTypeDef HAL_DMA_Start(DMA_HandleTypeDef *DMAx);
//HAL_StatusTypeDef HAL_DMA_Stop(DMA_HandleTypeDef *DMAx);
//HAL_StatusTypeDef HAL_DMA_Abort(DMA_HandleTypeDef *DMAx);

HAL_StatusTypeDef HAL_DMA_Start_IT(DMA_HandleTypeDef *DMAx);
HAL_StatusTypeDef HAL_DMA_Stop_IT(DMA_HandleTypeDef *DMAx);
HAL_StatusTypeDef HAL_DMA_Abort_IT(DMA_HandleTypeDef *DMAx);

HAL_StatusTypeDef HAL_DMA_ProcessCpItCallback(DMA_HandleTypeDef *DMAx);
HAL_StatusTypeDef HAL_DMA_ErrorCallback(DMA_HandleTypeDef *DMAx);

HAL_StatusTypeDef HAL_DMA_GetState(DMA_HandleTypeDef *DMAx);
HAL_StatusTypeDef HAL_DMA_GetError(DMA_HandleTypeDef *DMAx);

HAL_StatusTypeDef HAL_DMA_GPL_ChannelSelect(DMA_HandleTypeDef *DMAx, uint32_t ChannelSelect);


//=============================================================================

void HAL_DMA_CHx_IRQHandler(DMA_CHx_HandleTypeDef *DMA_CHx);

void HAL_DMA_CHx_MspInit(DMA_CHx_HandleTypeDef *DMA_CHx);
void HAL_DMA_CHx_MspDeInit(DMA_CHx_HandleTypeDef *DMA_CHx);

HAL_StatusTypeDef HAL_DMA_CHx_Init(DMA_CHx_HandleTypeDef *DMA_CHx);
HAL_StatusTypeDef HAL_DMA_CHx_DeInit(DMA_CHx_HandleTypeDef *DMA_CHx);

HAL_StatusTypeDef HAL_DMA_CHx_Config(DMA_CHx_HandleTypeDef *DMA_CHx);

HAL_StatusTypeDef HAL_DMA_CHx_Start(DMA_CHx_HandleTypeDef *DMA_CHx);
HAL_StatusTypeDef HAL_DMA_CHx_Stop(DMA_CHx_HandleTypeDef *DMA_CHx);
HAL_StatusTypeDef HAL_DMA_CHx_Abort(DMA_CHx_HandleTypeDef *DMA_CHx);

HAL_StatusTypeDef HAL_DMA_CHx_Start_IT(DMA_CHx_HandleTypeDef *DMA_CHx);
HAL_StatusTypeDef HAL_DMA_CHx_Stop_IT(DMA_CHx_HandleTypeDef *DMA_CHx);
HAL_StatusTypeDef HAL_DMA_CHx_Abort_IT(DMA_CHx_HandleTypeDef *DMA_CHx);

HAL_StatusTypeDef HAL_DMA_CHx_ProcessCpItCallback(DMA_CHx_HandleTypeDef *DMA_CHx);
HAL_StatusTypeDef HAL_DMA_CHx_ErrorCallback(DMA_CHx_HandleTypeDef *DMA_CHx);

HAL_StatusTypeDef HAL_DMA_CHx_GetState(DMA_CHx_HandleTypeDef *DMA_CHx);
HAL_StatusTypeDef HAL_DMA_CHx_GetError(DMA_CHx_HandleTypeDef *DMA_CHx);

//=============================================================================



HAL_StatusTypeDef HAL_DMA_Initial(void);



HAL_StatusTypeDef HAL_DMA_Memory_To_Memory(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_PL0(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_PL1(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_PL2(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_Memory_To_Memory_PL3(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH);



HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_ChannelNonDisable(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_PL0(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_PL1(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_PL2(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_TX_Memory_To_Periphery_PL3(DMA_CHx_Struct *DMA_CHx, uint32_t SRC_ADDR, uint8_t DMA_TXDET, uint16_t DATA_LANTH);



HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_PL0(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_PL1(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_PL2(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);
HAL_StatusTypeDef HAL_DMA_RX_Periphery_To_Memory_PL3(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);



HAL_StatusTypeDef HAL_DMA_Periphery_To_Periphery(DMA_CHx_Struct *DMA_CHx, uint8_t DMA_RXSRC, uint8_t DMA_TXDET, uint16_t DATA_LANTH);



//HAL_StatusTypeDef HAL_DMA_CH0_Memory_To_Memory(uint32_t SRC_ADDR, uint32_t DET_ADDR, uint16_t DATA_LANTH);
//HAL_StatusTypeDef HAL_DMA_CH0_TX_Memory_To_Periphery(uint32_t SRC_ADDR, uint32_t DMA_TXDET, uint16_t DATA_LANTH);
//HAL_StatusTypeDef HAL_DMA_CH0_RX_Periphery_To_Memory(uint32_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);
//HAL_StatusTypeDef HAL_DMA_CH1_TX_Memory_To_Periphery(uint32_t SRC_ADDR, uint32_t DMA_TXDET, uint16_t DATA_LANTH);
//HAL_StatusTypeDef HAL_DMA_CH1_RX_Periphery_To_Memory(uint32_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);
//HAL_StatusTypeDef HAL_DMA_CH2_TX_Memory_To_Periphery(uint32_t SRC_ADDR, uint32_t DMA_TXDET, uint16_t DATA_LANTH);
//HAL_StatusTypeDef HAL_DMA_CH2_RX_Periphery_To_Memory(uint32_t DMA_RXSRC, uint32_t DET_ADDR, uint16_t DATA_LANTH);



#ifdef __cplusplus
}
#endif

#endif     


