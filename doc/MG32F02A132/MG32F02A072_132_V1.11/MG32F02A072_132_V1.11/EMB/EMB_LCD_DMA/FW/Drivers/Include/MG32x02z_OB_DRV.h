/**
 ******************************************************************************
 *
 * @file        MG32x02z_OB_DRV.h
 *
 * @brief       This is the C code include format example file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V0.1
 * @date        2018/02/26
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *#001_Name_Date                // bugNum_Authour_Date 
 *>>Bug1 description 
 *--Bug1 sub-description 
 *--
 *--
 *>>
 *>>
 *#002_Name_Date
 *>>Bug2 description 
 *--Bug2 sub-description 
 *#003_Name_Date
 *>>Bug3 description 
 *
 @endif 
 ******************************************************************************
 */
#ifndef _MG32x02z_OB_DRV_H
#define _MG32x02z_OB_DRV_H

#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_MEM_DRV.H"

/******************************************************************************
*   Define
******************************************************************************/ 
#define OB_Size                        1*1024      // 0x400    Byte = 1024     Byte = 1KB

#define OB_Flash_Start_Address         (0x1FF10000)

//=============================================================================
// Flash Read Write Enable
/**
 * @name	
 *   		OB Write Enable
 */ 
///@{  
#define MEM_OB_WRITE           MEM_CR1_OB_WEN_mask_h0

///@}


/******************************************************************************
*   Enum
******************************************************************************/ 
// Flash Access Mode
typedef enum {
    OBErase = 0x6,
    OBProgram = 0x5
}OBWrtieMode;

// Single Write Access or Multiple Write  sequential key 
typedef enum {
    OBSingleSKey = 0xB9AA,
    OBMultipleSKey = 0xBE55,
}OBUnProtectSKey;

typedef struct{
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR00;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR01;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR02;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR03;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR04;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR05;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR06;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR07;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR08;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR09;
}OR0_Struct;



/******************************************************************************
*   Struct
******************************************************************************/


/******************************************************************************
*   Function
******************************************************************************/ 

void MEM_OBWriteMode_Config(OBWrtieMode Mode);
void MEM_OBUnProtectWriteSequentialKey(OBUnProtectSKey SKEY);

DRV_Return MEM_OBWrite_Cmd(FunctionalState State);
void MEM_OBSinglePageErase_Cmd(void);
void MEM_OBMultiplePageErase_Cmd(void);
void MEM_OBSingleProgram_Cmd(void);
void MEM_OBMultipleProgram_Cmd(void);



#endif

