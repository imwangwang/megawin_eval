/**
 ******************************************************************************
 *
 * @file        MG32x02z_OB1_DRV.c
 *
 * @brief       This is the C code format example file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V0.1
 * @date        2018/02/26
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 	Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History:
 @endif 
 ******************************************************************************
 */ 

#include "MG32x02z_OB1_DRV.h"

/**
 *******************************************************************************
 * @brief       OB1 Flash Write Function Enable / Disable
 * @details     
 * @param[in]	State
 *  @arg\b      ENABLE : OB Write Enable
 *  @arg\b      DISABLE : OB Write Disable
 * @return      DRV_Success
 * @return      DRV_Failure
 * @exception   This register is only able to change when boots from ISP mode.
 * @note        
 * @par         Example
 * @code
                MEM_OBFlashWrite_Cmd(ENABLE); // OB Write and Read Enable
                MEM_OBFlashWrite_Cmd(DISABLE); // OB Write Disable
 * @endcode
 * @par         Modify
 *              DRV_Return MEM_OBFlashWrite_Cmd(FunctionalState State)
 * @bug         
 *******************************************************************************
 */
DRV_Return MEM_OB1Write_Cmd(FunctionalState State)
{
    UnProtectModuleReg(MEMprotect);
    if(State == ENABLE)
    {
        MEM_Cmd(ENABLE);
        MEM->MCR.B[0] |= MEM_OB1_WRITE; 

        if((MEM->MCR.B[0] & MEM_OB1_WRITE) != 1)
        {
            ProtectModuleReg(MEMprotect);
            return DRV_Failure;
        }
    }
    else
    {
        MEM_Cmd(ENABLE);
        MEM->MCR.B[0] &= ~MEM_OB1_WRITE;
    }
    ProtectModuleReg(MEMprotect);
    return DRV_Success;
}


