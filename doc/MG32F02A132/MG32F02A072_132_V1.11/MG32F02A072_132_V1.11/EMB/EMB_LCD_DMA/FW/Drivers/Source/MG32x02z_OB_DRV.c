/**
 ******************************************************************************
 *
 * @file        MG32x02z_OB_DRV.c
 *
 * @brief       This is the C code format example file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V0.1
 * @date        2018/02/26
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History:
 @endif 
 ******************************************************************************
 */ 

#include "MG32x02z_OB_DRV.h"



/**
 *******************************************************************************
 * @brief       OB Write Access Mode Config
 * @details     
 * @details     Flash Page Erase
 *      \n      Flash Word Program
 * @param[in]   Mode
 *  @arg\b      None          : None Opration.
 *  @arg\b      OBErase       : AP Flash Page Erase.
 *  @arg\b      OBProgram     : AP Flash Word Program.
 * @return      No
 * @exception   None
 * @note        
 * @par         Example
 * @code
                MEM_OBWriteMode_Config(None);
                MEM_OBWriteMode_Config(OBErase);
                MEM_OBWriteMode_Config(OBProgram);
 * @endcode
 * @par         Modify
 *              void MEM_OBWriteMode_Config(OBWrtieMode Mode)
 * @bug         
 *******************************************************************************
 */
void MEM_OBWriteMode_Config(OBWrtieMode Mode)
{
    MEM->CR0.MBIT.MDS = Mode;
}



/**
 *******************************************************************************
 * @brief       OB Flash UnProtect Sequential Key
 * @details     
 * @param[in]   SKEY : OB UnProtect SKey
 *  @arg\b      OBSingleSKey : OB Space Single Page Erase or Single Word Program
 *  @arg\b      OBMultipleSKey : OB Space Multiple Page Erase or Multiple Word Program
 * @return      No
 * @exception   none
 * @note        
 * @par         Example
 * @code
                MEM_OBUnProtectWriteSequentialKey(OBSingleSKey); // OB Space Single Page Erase or Single Word Program
                MEM_OBUnProtectWriteSequentialKey(OBMultipleSKey); // OB Space Multiple Page Erase or Multiple Word Program
 * @endcode
 * @par         Modify
 *              void MEM_OBUnProtectWriteSequentialKey(OBUnProtectSKey SKEY)
 * @bug         
 *******************************************************************************
 */
void MEM_OBUnProtectWriteSequentialKey(OBUnProtectSKey SKEY)
{
    switch(SKEY)
    {
        case OBSingleSKey:
        case OBMultipleSKey:
            MEM->SKEY.MBIT.SKEY2 = 0xC33C;
            MEM->SKEY.MBIT.SKEY2 = SKEY;
            break;
    }
}



/**
 *******************************************************************************
 * @brief       OB Flash Write Function Enable / Disable
 * @details     
 * @param[in]	State
 *  @arg\b      ENABLE : OB Write Enable
 *  @arg\b      DISABLE : OB Write Disable
 * @return      DRV_Success
 * @return      DRV_Failure
 * @exception   This register is only able to change when boots from ISP mode.
 * @note        
 * @par         Example
 * @code
                MEM_OBWrite_Cmd(ENABLE); // OB Write and Read Enable
                MEM_OBWrite_Cmd(DISABLE); // OB Write Disable
 * @endcode
 * @par         Modify
 *              DRV_Return MEM_OBWrite_Cmd(FunctionalState State)
 * @bug         
 *******************************************************************************
 */
DRV_Return MEM_OBWrite_Cmd(FunctionalState State)
{
    UnProtectModuleReg(MEMprotect);
    if(State == ENABLE)
    {
        MEM_Cmd(ENABLE);
        MEM_Access_Cmd(MEM_OB_WRITE, ENABLE);

        if(MEM_GetFlagStatus(MEM_OB_WRITE) == DRV_Failure)
        {
            ProtectModuleReg(MEMprotect);
            return DRV_Failure;
        }
    }
    else
    {
        MEM_Cmd(ENABLE);
        MEM_Access_Cmd(MEM_OB_WRITE, DISABLE);
    }
    ProtectModuleReg(MEMprotect);
    return DRV_Success;
}



/**
 *******************************************************************************
 * @brief       MEM_OBSinglePageErase_Cmd
 * @details     
 * @return		None
 * @exception   None
 * @note        
 * @par         Example
 * @code
                MEM_OBSinglePageErase_Cmd();
 * @endcode
 * @par         Modify
 *              void MEM_OBSinglePageErase_Cmd(void)
 * @bug 
 *******************************************************************************
 */
void MEM_OBSinglePageErase_Cmd(void)
{
    UnProtectModuleReg(MEMprotect);
    MEM_OBWriteMode_Config(OBErase);
    MEM_OBUnProtectWriteSequentialKey(OBSingleSKey);
    ProtectModuleReg(MEMprotect);
}



/**
 *******************************************************************************
 * @brief       MEM_OBMultiplePageErase_Cmd
 * @details     
 * @return		None
 * @exception   None
 * @note        
 * @par         Example
 * @code
                MEM_OBMultiplePageErase_Cmd();
 * @endcode
 * @par         Modify
 *              void MEM_OBMultiplePageErase_Cmd(void)
 * @bug 
 *******************************************************************************
 */
void MEM_OBMultiplePageErase_Cmd(void)
{
    UnProtectModuleReg(MEMprotect);
    MEM_OBWriteMode_Config(OBErase);
    MEM_OBUnProtectWriteSequentialKey(OBMultipleSKey);
    ProtectModuleReg(MEMprotect);
}



/**
 *******************************************************************************
 * @brief       MEM_OBSingleProgram_Cmd.
 * @details     
 * @return		None
 * @exception   None
 * @note        
 * @par         Example
 * @code
                MEM_OBSingleProgram_Cmd();
 * @endcode
 * @par         Modify
 *              void MEM_OBSingleProgram_Cmd(void)
 * @bug 
 *******************************************************************************
 */
void MEM_OBSingleProgram_Cmd(void)
{
    UnProtectModuleReg(MEMprotect);
    MEM_OBWriteMode_Config(OBProgram);
    MEM_OBUnProtectWriteSequentialKey(OBSingleSKey);
    ProtectModuleReg(MEMprotect);
}

/**
 *******************************************************************************
 * @brief       MEM_OBMultipleProgram_Cmd.
 * @details     
 * @return		None
 * @exception   None
 * @note        
 * @par         Example
 * @code
                MEM_OBMultipleProgram_Cmd();
 * @endcode
 * @par         Modify
 *              void MEM_OBSingleProgram_Cmd(void)
 * @bug 
 *******************************************************************************
 */
void MEM_OBMultipleProgram_Cmd(void)
{
    UnProtectModuleReg(MEMprotect);
    MEM_OBWriteMode_Config(OBProgram);
    MEM_OBUnProtectWriteSequentialKey(OBMultipleSKey);
    ProtectModuleReg(MEMprotect);
}





