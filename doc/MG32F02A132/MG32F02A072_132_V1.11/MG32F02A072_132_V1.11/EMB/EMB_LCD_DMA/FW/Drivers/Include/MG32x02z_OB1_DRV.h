/**
 ******************************************************************************
 *
 * @file        MG32x02z_OB1_DRV.h
 *
 * @brief       This is the C code include format example file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V0.1
 * @date        2016/04/14
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *#001_Name_Date                // bugNum_Authour_Date 
 *>>Bug1 description 
 *--Bug1 sub-description 
 *--
 *--
 *>>
 *>>
 *#002_Name_Date
 *>>Bug2 description 
 *--Bug2 sub-description 
 *#003_Name_Date
 *>>Bug3 description 
 *
 @endif 
 ******************************************************************************
 */
#ifndef _MG32x02z_OB1_DRV_H
#define _MG32x02z_OB1_DRV_H

#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_MEM_DRV.H"
#include "MG32x02z_OB_DRV.H"

/******************************************************************************
*   Define
******************************************************************************/ 
#define OB1_Size                        1*1024      // 0x400    Byte = 1024     Byte = 1KB

#define OB1_Flash_Start_Address         (0x1FF20000)

//=============================================================================
// Flash Read Write Enable
/**
 * @name	
 *   		OB1 Enable
 */ 
///@{  
#define MEM_OB1_WRITE          MEM_MCR_OB1_WEN_mask_b0
#define OB1Write               MEM_MCR_OB1_WEN_mask_b0
///@}



/******************************************************************************
*   OB1 Register
******************************************************************************/ 
    
typedef struct{
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR10;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR11;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR12;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR13;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR14;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR15;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR16;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR17;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR18;
    union{
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
    }OR19;
}OR1_Struct;


/******************************************************************************
*   Struct
******************************************************************************/


/******************************************************************************
*   Function
******************************************************************************/ 

#define MEM_OB1WriteMode_Config                 MEM_OBWriteMode_Config
#define MEM_OB1UnProtectWriteSequentialKey      MEM_OBUnProtectWriteSequentialKey

#define MEM_OB1SinglePageErase_Config           MEM_OBSinglePageErase_Config
#define MEM_OB1MultiplePageErase_Config         MEM_OBMultiplePageErase_Config
#define MEM_OB1SingleProgram_Config             MEM_OBSingleProgram_Config
#define MEM_OB1MultipleProgram_Config           MEM_OBMultipleProgram_Config

#define MEM_OB1_Load                            MEM_OB_Load
#define MEM_OB1_Store                           MEM_OB_Store
#define MEM_OB1_Modify                          MEM_OB_Modify

DRV_Return MEM_OB1Write_Cmd(FunctionalState State);

#endif

