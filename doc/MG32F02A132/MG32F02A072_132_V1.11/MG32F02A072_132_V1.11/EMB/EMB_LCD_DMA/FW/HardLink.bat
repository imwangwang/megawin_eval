del /Q/F/S Reg_INC
del /Q/F/S Drivers
del /Q/F/S MiddleWares
del /Q/F/S HAL
del /Q/F/S SWIF
del /Q/F/S IEC60730

rmdir /Q/S Reg_INC
rmdir /Q/S Drivers
rmdir /Q/S MiddleWares
rmdir /Q/S HAL
rmdir /Q/S SWIF
rmdir /Q/S IEC60730

mklink /H /D /J Reg_INC ..\..\__Reg_INC
mklink /H /D /J Drivers ..\..\__Drivers
mklink /H /D /J MiddleWares ..\..\__MiddleWares
mklink /H /D /J HAL ..\..\__HAL
mklink /H /D /J SWIF ..\..\__SWIF
mklink /H /D /J IEC60730 ..\..\__IEC60730

attrib -h Integration.bat
attrib +h HardLink.bat


