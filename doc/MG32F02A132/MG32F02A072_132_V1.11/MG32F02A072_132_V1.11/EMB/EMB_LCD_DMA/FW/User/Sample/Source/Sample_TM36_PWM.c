/**
  ******************************************************************************
 *
 * @file        Sample_TM36_PWM.c
 *
 * @brief       Output 3-channels PWM of TM36
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/12/13
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "MG32x02z_TM_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***
//  <h> TM36 counter period
//      <o0> prescaler counter range (1~65536)  <1-65536>
//      <o1> main counter range (1~65536) <1-65536>
//  </h>
#define TM36_PrescalerCounter_Range 1
#define TM36_MainCounter_Range      65536

//  <h> Duty cycle setting 
//      <o0> Channel0 match point (1~65536) <1-65536>
//      <o1> Channel1 match point (1~65536) <1-65536>
//      <o2> Channel2 match point (1~65536) <1-65536>
//  </h>
#define TM36_Channel0_MatchPoint    100
#define TM36_Channel1_MatchPoint    1000
#define TM36_Channel2_MatchPoint    10000

//*** <<< end of configuration section >>>    ***

/**
 *******************************************************************************
 * @brief	    Output 3-channels PWM of TM36 
 * @details     1.initial & modify TimeBase parameter 
 *    \n        2.config TM36 channel0/1/2 function 
 *    \n        3.Enable TM36 channel 0 Output
 *    \n        4.set match point (Duty cycle %) for PWM channel0
 *    \n        5.Enable TM36 channel 1 Output
 *    \n        6.set match point (Duty cycle %) for PWM channel1
 *    \n        7.Enable TM36 channel 2 Output
 *    \n        8.set match point (Duty cycle %) for PWM channel2
 *    \n        9.select Edge Align
 *    \n        10.clear all flag
 *    \n        11.Enable Timer  
 * @note        The period = (prescaler*main counter)/12MHz (CK_TM36_PR=12MHz) (s)
 *              User can update duty cycle by replace CCxB register.
 *******************************************************************************
 */
void Sample_TM36_PWM(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
     

    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->TM36 = Enable
    //Configure Peripheral On Mode Clock->Port C = Enable
    //Configure Peripheral On Mode Clock->Port D = Enable
	
    //==Set GPIO init 
    //TM36_OC00 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOC->PC0
    //GPIO port initial is 0xFFFF
    //PC0 mode is PPO
    //PC0 function TM36_OC00
	
    //==Set GPIO init 
    //TM36_OC10 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOC->PC2
    //GPIO port initial is 0xFFFF
    //PC2 mode is PPO
    //PC2 function TM36_OC10

    //==Set GPIO init 
    //TM36_OC2 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOD->PD0
    //GPIO port initial is 0xFFFF
    //PD0 mode is PPO
    //PD0 function TM36_OC2

	 
    TM_DeInit(TM36);
    
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_MainClockDirection =TM_UpCount;
    TM_TimeBase_InitStruct.TM_Period = TM36_MainCounter_Range - 1; 
    TM_TimeBase_InitStruct.TM_Prescaler = TM36_PrescalerCounter_Range - 1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
    
    TM_TimeBase_Init(TM36, &TM_TimeBase_InitStruct);
   
    // ----------------------------------------------------
    // 2.config TM36 channel0/1/2 function 
    TM_CH0Function_Config(TM36, TM_16bitPWM);
    TM_CH1Function_Config(TM36, TM_16bitPWM);
    TM_CH2Function_Config(TM36, TM_16bitPWM);
    
    // ----------------------------------------------------
    // 3.Enable TM36 channel 0 Output (just output TM36_OC00)
    TM_OC00Output_Cmd(TM36,ENABLE);    
    TM_InverseOC0z_Cmd(TM36, DISABLE);
    TM_OC0zOutputState_Init(TM36, CLR);
    
    TM_OC0NOutput_Cmd(TM36, DISABLE);
    TM_InverseOC0N_Cmd(TM36, DISABLE);
    // ----------------------------------------------------
    // 4.set match point (Duty cycle %) for PWM channel0
    TM_SetCC0A(TM36,TM36_Channel0_MatchPoint);        
    TM_SetCC0B(TM36,TM36_Channel0_MatchPoint);		// reload value when overflow

    // ----------------------------------------------------
    // 5.TM36 channel 1 Output config (just output TM36_OC10)
    TM_OC10Output_Cmd(TM36,ENABLE);    
    TM_InverseOC1z_Cmd(TM36, DISABLE);
    TM_OC1zOutputState_Init(TM36, CLR);
    
    TM_OC1NOutput_Cmd(TM36, DISABLE);
    TM_InverseOC1N_Cmd(TM36, DISABLE);
    // ----------------------------------------------------
    // 6.set match point (Duty cycle %) for PWM channel1
    TM_SetCC1A(TM36,TM36_Channel1_MatchPoint);        
    TM_SetCC1B(TM36,TM36_Channel1_MatchPoint);		// reload value when overflow

    // ----------------------------------------------------
    // 7.TM36 channel 2 Output config (just output TM36_OC2)
    TM_OC2Output_Cmd(TM36,ENABLE);    
    TM_InverseOC2_Cmd(TM36, DISABLE);
    TM_OC2OutputState_Init(TM36, CLR);
    
    TM_OC2NOutput_Cmd(TM36, DISABLE);
    TM_InverseOC2N_Cmd(TM36, DISABLE);
    // ----------------------------------------------------
    // 8.set match point (Duty cycle %) for PWM channel2
    TM_SetCC2A(TM36,TM36_Channel2_MatchPoint);        
    TM_SetCC2B(TM36,TM36_Channel2_MatchPoint);		// reload value when overflow

    // ----------------------------------------------------
    // 9.select Edge Align
    TM_AlignmentMode_Select(TM36, TM_EdgeAlign);
    
    // ----------------------------------------------------
    // 10.clear flag
    TM_ClearFlag(TM36, TMx_CF0A | TMx_CF1A | TMx_CF2A);
    
    // ----------------------------------------------------
    // 11.Timer enable
    TM_Timer_Cmd(TM36,ENABLE);
    
    return;
}




