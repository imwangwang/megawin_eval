/**
  ******************************************************************************
 *
 * @file        Sample_TM16_AutoStop.c
 *
 * @brief       Control the number of TM16_CKO
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "MG32x02z_TM_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***

//  <h> ClockOut Setting (Output GPIOb Pin2)
//      <o0> Period width (clks) (1~65536) <1-65536>
//      <o1> Clock out times (1~65536)  <1-65536>
//  </h>
#define Simple_CKO_Period           12000
#define Simple_CKO_Number           1000

//*** <<< end of configuration section >>>    ***

/**
 *******************************************************************************
 * @brief	    Control the number of toggle of TM16_CKO
 * @details     1.initial & modify TimeBase parameter 
 *    \n        2.clear TOF flag
 *    \n        3.Clock Out config (initial state='1' & enable clock out)
 *    \n        4.Enable Auto Stop 
 *    \n        5.Start TM16 
 * @note        If user wants to output 1000 times of 1ms period.
 *              User can set "pulse width (clks)" to 12000 (if CK_TM16_PR is 12MHz).
 *              And Set "clock out times" to 1000 
 *               
 *******************************************************************************
 */
void Sample_TM16_AutoStop(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
     

    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->TM16 = Enable
    //Configure Peripheral On Mode Clock->Port B = Enable
	
    //==Set GPIO init 
    //TM16_CKO pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->PB2
    //GPIO port initial is 0xFFFF
    //PB2 mode is PPO
    //PB2 function TM16_CKO
	 
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_Period = Simple_CKO_Number - 1; 
    TM_TimeBase_InitStruct.TM_Prescaler = Simple_CKO_Period - 1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
    
    TM_TimeBase_Init(TM16, &TM_TimeBase_InitStruct);
    
    // ----------------------------------------------------
    // 2.clear TOF flag
    TM_ClearFlag(TM16, TMx_TOF);
    
    // ----------------------------------------------------
    // 3.Clock Out config (initial state='1' & enable clock out) 
    TM_ClockOutSource_Select(TM16, PrescalerCKO);
    TM_ClockOut_Cmd(TM16, ENABLE);
    TM_CKOOutputState_Init(TM16, CLR);
    
    // ----------------------------------------------------
    // 4.Enable Auto Stop 
    TM_AutoStop_Cmd(TM16, ENABLE);
    
    // ----------------------------------------------------
    // 5.Start TM16 
    TM_Timer_Cmd(TM16, ENABLE);
    
    return;
}


