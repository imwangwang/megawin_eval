/**
  ******************************************************************************
 *
 * @file        Sample_CMP_Init.c
 *
 * @brief       CMP initial
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "MG32x02z_CMP_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***
//  <e0.0> CMP0 Config
//      <o0.8..10> Analog input positive channel selection <0=> IVREF <1=> CMP0_I0 (PA8) <2=> CMP0_I1 (PA9) <3=> CMP_C0 (PB0) <4=> CMP_C1 (PB1) <5=> LDO
//      <o0.12..14> Analog input NEGATIVE channel selection <0=> IVREF <1=> CMP0_I0 (PA8) <2=> CMP0_I1 (PA9) <3=> CMP_C0 (PB0) <4=> CMP_C1 (PB1) <5=> LDO
//      <o0.3> Response time <0=> Fast <1=> Slow (Low Power)
//      <o0.16> Inverse analog comparator polarity <0=> Normal (Positive) <1=> Inverse (Negative)
//      <o0.17> Inverse analog comparator output pin <0=> Disable <1=> Inverse to Pin
//      <o0.18..19> Analog comparator's clock filter <0=> Bypass <1=> CMP_CK <2=> TM00_TRGO <3=> TM01_TRGO
//      <o0.20..21> Analog comparator clock filter divider <0=> DIV1 <1=> DIV2 <2=> DIV4 <3=>DIV8
//  </e>
#define Sample_AC0_Config          0x00002101

//  <e0.0> CMP1 Config
//      <o0.8..10> Analog input positive channel selection <0=> IVREF2 <1=> CMP1_I0 (PA10) <2=> CMP1_I1 (PA11) <3=> CMP_C0 (PB0) <4=> CMP_C1 (PB1) <5=> LDO
//      <o0.12..14> Analog input NEGATIVE channel selection <0=> IVREF2 <1=> CMP1_I0 (PA10) <2=> CMP1_I1 (PA11) <3=> CMP_C0 (PB0) <4=> CMP_C1 (PB1) <5=> LDO
//      <o0.3> Response time <0=> Fast <1=> Slow (Low Power)
//      <o0.16> Inverse analog comparator polarity <0=> Normal (Positive) <1=> Inverse (Negative)
//      <o0.17> Inverse analog comparator output pin <0=> Disable <1=> Inverse to Pin
//      <o0.18..19> Analog comparator's clock filter <0=> Bypass <1=> CMP_CK <2=> TM00_TRGO <3=> TM01_TRGO
//      <o0.20..21> Analog comparator clock filter divider <0=> DIV1 <1=> DIV2 <2=> DIV4 <3=>DIV8
//  </e>
#define Sample_AC1_Config          0x00000001

//  <e0.0> CMP2 Config
//      <o0.8..10> Analog input positive channel selection <0=> IVREF2 <1=> CMP2_I0 (PA12) <2=> CMP2_I1 (PA13) <3=> CMP_C0 (PB0) <4=> CMP_C1 (PB1) <5=> LDO
//      <o0.12..14> Analog input NEGATIVE channel selection <0=> IVREF2 <1=> CMP2_I0 (PA12) <2=> CMP2_I1 (PA13) <3=> CMP_C0 (PB0) <4=> CMP_C1 (PB1) <5=> LDO
//      <o0.3> Response time <0=> Fast <1=> Slow (Low Power)
//      <o0.16> Inverse analog comparator polarity <0=> Normal (Positive) <1=> Inverse (Negative)
//      <o0.17> Inverse analog comparator output pin <0=> Disable <1=> Inverse to Pin
//      <o0.18..19> Analog comparator's clock filter <0=> Bypass <1=> CMP_CK <2=> TM00_TRGO <3=> TM01_TRGO
//      <o0.20..21> Analog comparator clock filter divider <0=> DIV1 <1=> DIV2 <2=> DIV4 <3=>DIV8
//  </e>
#define Sample_AC2_Config          0x00000000

//  <e0.0> CMP3 Config
//      <o0.8..10> Analog input positive channel selection <0=> IVREF2 <1=> CMP3_I0 (PA14) <2=> CMP3_I1 (PA15) <3=> CMP_C0 (PB0) <4=> CMP_C1 (PB1) <5=> LDO
//      <o0.12..14> Analog input NEGATIVE channel selection <0=> IVREF2 <1=> CMP3_I0 (PA14) <2=> CMP3_I1 (PA15) <3=> CMP_C0 (PB0) <4=> CMP_C1 (PB1) <5=> LDO
//      <o0.3> Response time <0=> Fast <1=> Slow (Low Power)
//      <o0.16> Inverse analog comparator polarity <0=> Normal (Positive) <1=> Inverse (Negative)
//      <o0.17> Inverse analog comparator output pin <0=> Disable <1=> Inverse to Pin
//      <o0.18..19> Analog comparator's clock filter <0=> Bypass <1=> CMP_CK <2=> TM00_TRGO <3=> TM01_TRGO
//      <o0.20..21> Analog comparator clock filter divider <0=> DIV1 <1=> DIV2 <2=> DIV4 <3=>DIV8
//  </e>
#define Sample_AC3_Config          0x00000000

//  <e0.0> IVREF Config
//      <o0.2..7> Analog comparator main internal reference (R-ladder:0~63) <0-64>
//  </e>
//  <e0.8> IVREF2 Config
//      <o0.10..15> Analog comparator main internal reference (R-ladder:0~63) <0-64>
//  </e>
#define Sample_ANA_Config           65021

//*** <<< end of configuration section >>>    ***

/**
 *******************************************************************************
 * @brief	    Config Comparator parameter 
 * @details     Config Comparator all parameter by Wizard
 * @note        User can polling CMPx state if comparator changed.
 *              ie. 
 *                  // polling AC0 rising edge event
 *                  if(CMP_GetSingleFlagStatus(CMP,AC0_RisingEdge_Flag) == DRV_Happened)
 *                  { 
 *                     CMP_ClearFlag(CMP, AC0_RisingEdge_Flag);  
 *                     // to do ... 
 *                  }
 *******************************************************************************
 */
void Sample_CMP_Init(void)
{  
    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->CMP = Enable
    //Configure Peripheral On Mode Clock->Port A = Enable (if use CMPx_I0, CMPx_I1)
    //Configure Peripheral On Mode Clock->Port B = Enable (if use CMP_C0, CMP_C1)
    //Configure Peripheral On Mode Clock->TM00 = Enable (if clock filter source from TM00)
    //Configure Peripheral On Mode Clock->TM01 = Enable (if clock filter source from TM01)
	
    //==Set GPIO init 
    //(CMP0) CMP0_I0/CMP0_I1 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOA->Pin8/9
    //GPIO port initial is 0xFFFF
    //Pin8, Pin9 mode is AIO
    //Pin8, Pin9 function GPA8, GPA9
	
    //==Set GPIO init 
    //(CMP1) CMP1_I0/CMP1_I1 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOA->Pin10/11
    //GPIO port initial is 0xFFFF
    //Pin10, Pin11 mode is AIO
    //Pin10, Pin11 function GPA10, GPA11
    
    //==Set GPIO init 
    //(CMP2) CMP2_I0/CMP2_I1 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOA->Pin12/13
    //GPIO port initial is 0xFFFF
    //Pin12, Pin13 mode is AIO
    //Pin12, Pin13 function GPA12, GPA13
    
    //==Set GPIO init 
    //(CMP3) CMP3_I0/CMP3_I1 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOA->Pin14/15
    //GPIO port initial is 0xFFFF
    //Pin14, Pin15 mode is AIO
    //Pin14, Pin15 function GPA14, GPA15
    
    //==Set GPIO init 
    //(CMP) CMP_C0/CMP_C1 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->Pin0/1
    //GPIO port initial is 0xFFFF
    //Pin0, Pin1 mode is AIO
    //Pin0, Pin1 function GPB0, GPB1
    
    
	//-------------- CMP0 (AC0) configuration -----------------------
    CMPAC0->CR.W = Sample_AC0_Config;
	//-------------- CMP1 (AC1) configuration -----------------------
    CMPAC1->CR.W = Sample_AC1_Config;
	//-------------- CMP2 (AC2) configuration -----------------------
    CMPAC2->CR.W = Sample_AC2_Config;
	//-------------- CMP3 (AC3) configuration -----------------------
    CMPAC3->CR.W = Sample_AC3_Config;

	//-------------- ANA configuration ------------------------------
    CMP->ANA.W = Sample_ANA_Config;
    
    return;
}
