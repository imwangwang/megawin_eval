

/**
 ******************************************************************************
 *
 * @file        MG32x02z__IRQHandler.c
 * @brief       The demo IRQHandler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */


#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_EXIC_DRV.H"
#include "MG32x02z__IRQHandler.H"
#include "LCDDEMO_RGBLED_API.h"


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void NMI_Handler(void)
{    
    //to do......
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */


void HardFault_Handler(void)
{

    
    //to do......
    
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void SVC_Handler(void)
{
    //to do......    
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void PendSV_Handler(void)
{
    //to do......    
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void SysTick_Handler(void)
{
    IncTick();
    //to do......
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void WWDT_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(WWDT_IRQn);
    if(IRQ_ID & EXIC_SRC0_ID0_wwdt_b0)
    {
        WWDT_IRQ();
    }
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void SYS_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(SYS_IRQn);

    if(IRQ_ID & EXIC_SRC0_ID1_iwdt_b1)
    {
        IWDT_IRQ();   
    }
    if(IRQ_ID & EXIC_SRC0_ID1_pw_b1)
    {
        PW_IRQ();
    }
    if(IRQ_ID & EXIC_SRC0_ID1_rtc_b1)
    {
        RTC_IRQ();
    }
    if(IRQ_ID & EXIC_SRC0_ID1_csc_b1)
    {
        CSC_IRQ();
    }
    if(IRQ_ID & EXIC_SRC0_ID1_apb_b1)
    {
        APB_IRQ();
    }
    if(IRQ_ID & EXIC_SRC0_ID1_mem_b1)
    {
        MEM_IRQ();
    }
}



/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void EXINT0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(EXINT0_IRQn);    
    if( IRQ_ID & EXIC_SRC0_ID3_exint0_b3)
    {
        EXINT0_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void EXINT1_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(EXINT1_IRQn);      
    if( IRQ_ID & EXIC_SRC1_ID4_exint1_b0)
    {
        EXINT1_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void EXINT2_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(EXINT2_IRQn);      
    if( IRQ_ID & EXIC_SRC1_ID5_exint2_b1)
    {
        EXINT2_IRQ();
    }
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void EXINT3_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(EXINT3_IRQn);      
    if( IRQ_ID & EXIC_SRC1_ID6_exint3_b2)
    {
        EXINT3_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void COMP_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(COMP_IRQn);      
    if( IRQ_ID & EXIC_SRC1_ID7_cmp_b3)
    {
        CMP_IRQ();
    }
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void DMA_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(DMA_IRQn);      
    if( IRQ_ID & EXIC_SRC2_ID8_dma_b0)
    {
        DMA_IRQ();
    }    
    
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void ADC_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    __nop();
    IRQ_ID = EXIC_GetITSourceID(ADC_IRQn);      
    if( IRQ_ID & EXIC_SRC2_ID10_adc_b2)
    {
        ADC_IRQ();
    }
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void DAC_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(DAC_IRQn);      
    if( IRQ_ID & EXIC_SRC2_ID11_dac_b3)
    {
        DAC_IRQ();
    }     
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void TM0x_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM0x_IRQn);     
    if( IRQ_ID & EXIC_SRC3_ID12_tm00_b0 )
    {
        TM00_IRQ();
    }
    if(IRQ_ID & EXIC_SRC3_ID12_tm01_b0)
    {
        TM01_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void TM10_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM10_IRQn);      
    if( IRQ_ID & EXIC_SRC3_ID13_tm10_b1)
    {
        TM10_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void TM1x_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM1x_IRQn);     
    if(IRQ_ID & EXIC_SRC3_ID14_tm16_b2)
    {
        TM16_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void TM20_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM20_IRQn);      
    if(IRQ_ID & EXIC_SRC3_ID15_tm20_b3)
    {
        TM20_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void TM2x_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM2x_IRQn);      
    if(IRQ_ID & EXIC_SRC4_ID16_tm26_b0)
    {
        TM26_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void TM3x_IRQHandler(void)
{
    API_RGBLED_SW();

}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void URT0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(URT0_IRQn);      
    if(IRQ_ID & EXIC_SRC5_ID20_urt0_b0)
    {
        URT0_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void URT123_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(URT123_IRQn);      
    if(IRQ_ID & EXIC_SRC5_ID21_urt1_b1)
    {
        URT1_IRQ();
    }
    if( IRQ_ID & EXIC_SRC5_ID21_urt2_b1)
    {
        URT2_IRQ();
    }
    if( IRQ_ID & EXIC_SRC5_ID21_urt3_b1)
    {
        //URT3_IRQ();
        API_RGB_Config();
        URT_ClearITFlag(URT3,URT_IT_TMO);
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void SPI0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(SPI0_IRQn);      
    if( IRQ_ID & EXIC_SRC6_ID24_spi0_b0)
    {
        SPI0_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void I2C0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(I2C0_IRQn);      
    if(IRQ_ID & EXIC_SRC7_ID28_i2c0_b0)
    {
        I2C0_IRQ();
    }
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void I2Cx_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(I2Cx_IRQn);      
    if( IRQ_ID & EXIC_SRC7_ID29_i2c1_b1)
    {
        I2C1_IRQ();
    }
}



















