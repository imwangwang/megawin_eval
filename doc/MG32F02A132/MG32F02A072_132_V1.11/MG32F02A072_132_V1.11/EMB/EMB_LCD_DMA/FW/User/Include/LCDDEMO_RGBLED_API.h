

/**
 ******************************************************************************
 *
 * @file        LCDDEMO_RGBLED_API.H
 *
 * @brief       This file provides firmware functions to manage the following 
 *              functionalities of the ADC peripheral:
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/02/12
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *>>
 *--
 *--
 *>>
 *>>
 *
 @endif 
 ******************************************************************************
 */ 


#ifndef _LCDDEMO_RGBLED_API_H

/*!< _LCDDEMO_RGBLED_API_H */ 
#define _LCDDEMO_RGBLED_API_H



#include "MG32x02z_TM_MID.H"


typedef enum
{
    RGB_OFF = 0,
    RGB_ON  = 1,
}RGB_Status;

typedef enum
{
    RGB_Mode_STATIC    = 0x00,
    RGB_Mode_PULSING   = 0x01,
    RGB_Mode_BREATH    = 0x02,
    RGB_Mode_SPECTRUM  = 0x03,
    RGB_Mode_OFF       = 0xFF,
}RGB_MODE_TYPE;

#define RGB_CTR_LEN        12
#define RGB_LEN             3
   #define RGB_COLOR_R_TABLE   0
   #define RGB_COLOR_G_TABLE   1
   #define RGB_COLOR_B_TABLE   2
   #define RGB_MAIN_STEP       3
   #define RGB_MAIN_STEP_CMP   4
   #define RGB_SUB_STEP        5
   #define RGB_SUB_STEP_CMP    6
   #define RGB_TEMPO_CNT       7
   #define RGB_TEMPO_CNT_CMP   8
   #define RGB_COLOR_R         9
   #define RGB_COLOR_G         10
   #define RGB_COLOR_B         11


typedef struct
{    
    RGB_Status    RGB_SWITCH;
    RGB_MODE_TYPE RGB_MODE;    
    uint8_t       RGB_CTR[RGB_CTR_LEN]; 
}RGB_TYPE;





extern RGB_TYPE   RGB1;
extern RGB_TYPE   RGB2;
extern RGB_TYPE   RGB3;


#define RGB_PORT  GPIOA
/*!< LCD demo board Software RGB LED1 - B */ 
#define RGB1_B    PA0
/*!< LCD demo board Software RGB LED1 - R */ 
#define RGB1_R    PA1
/*!< LCD demo board Software RGB LED1 - G */ 
#define RGB1_G    PA2
/*!< LCD demo board Software RGB LED2 - B */ 
#define RGB2_B    PA3
/*!< LCD demo board Software RGB LED2 - R */ 
#define RGB2_R    PA4
/*!< LCD demo board Software RGB LED2 - G */ 
#define RGB2_G    PA5
/*!< LCD demo board Software RGB LED1 - B */ 
#define RGB3_B    PA0
/*!< LCD demo board Software RGB LED1 - R */ 
#define RGB3_R    PA1
/*!< LCD demo board Software RGB LED1 - G */ 
#define RGB3_G    PA2


#define RGB_R         0
#define RGB_G         1
#define RGB_B         2


#define RGBX_OFF      1
#define RGBX_ON       0

#define RGB1_R_ON        RGB1_R = RGBX_ON;
#define RGB1_R_OFF       RGB1_R = RGBX_OFF;
#define RGB1_G_ON        RGB1_G = RGBX_ON;
#define RGB1_G_OFF       RGB1_G = RGBX_OFF;
#define RGB1_B_ON        RGB1_B = RGBX_ON;
#define RGB1_B_OFF       RGB1_B = RGBX_OFF;
#define RGB2_R_ON        RGB2_R = RGBX_ON;
#define RGB2_R_OFF       RGB2_R = RGBX_OFF;
#define RGB2_G_ON        RGB2_G = RGBX_ON;
#define RGB2_G_OFF       RGB2_G = RGBX_OFF;
#define RGB2_B_ON        RGB2_B = RGBX_ON;
#define RGB2_B_OFF       RGB2_B = RGBX_OFF;
#define RGB3_R_ON        RGB3_R = RGBX_ON;
#define RGB3_R_OFF       RGB3_R = RGBX_OFF;
#define RGB3_G_ON        RGB3_G = RGBX_ON;
#define RGB3_G_OFF       RGB3_G = RGBX_OFF;
#define RGB3_B_ON        RGB3_B = RGBX_ON;
#define RGB3_B_OFF       RGB3_B = RGBX_OFF;

#define RGB12_RGB_OFF    GPIOA->SC.H[0] = (PX_Pin_0|PX_Pin_1|PX_Pin_2|PX_Pin_3|PX_Pin_4|PX_Pin_5);
#define RGB12_RGB_ON     GPIOA->SC.H[1] = (PX_Pin_0|PX_Pin_1|PX_Pin_2|PX_Pin_3|PX_Pin_4|PX_Pin_5);
#define RGB1_RGB_OFF     GPIOA->SC.H[0] = (PX_Pin_0|PX_Pin_1|PX_Pin_2);
#define RGB1_RGB_ON      GPIOA->SC.H[1] = (PX_Pin_0|PX_Pin_1|PX_Pin_2);
#define RGB2_RGB_OFF     GPIOA->SC.H[0] = (PX_Pin_3|PX_Pin_4|PX_Pin_5);
#define RGB2_RGB_ON      GPIOA->SC.H[1] = (PX_Pin_3|PX_Pin_4|PX_Pin_5);
#define RGB3_RGB_OFF     API_UpdateRGB3_R(0); API_UpdateRGB3_G(0); API_UpdateRGB3_B(0); TM_ReloadFromCCxB_Cmd(TM26,TM_SimultaneouslyReload);


#define RGB1_OFF         RGB1.RGB_SWITCH = RGB_OFF;
#define RGB2_OFF         RGB2.RGB_SWITCH = RGB_OFF;
#define RGB3_OFF         RGB3.RGB_SWITCH = RGB_OFF; 



#define LED_RGB_ON    LED123.LED_STC_SWITCH = ON;
#define LED_RGB_OFF   LED123.LED_STC_SWITCH = OFF;



/** @brief  Update duty cycle.
    @retval None
  */
/*!< update TM36's (RGB1_R) */
#define API_UpdateRGB1_R(duty) MID_TM_UpdateChannel0H(&htm36, duty);    // update to OC/PWM reload register
/*!< update TM36's (RGB1_G) */
#define API_UpdateRGB1_G(duty) MID_TM_UpdateChannel1L(&htm36, duty);    // update to OC/PWM reload register
/*!< update TM36's (RGB1_B) */
#define API_UpdateRGB1_B(duty) MID_TM_UpdateChannel0L(&htm36, duty);    // update to OC/PWM reload register

/*!< update TM36's (RGB2_R) */
#define API_UpdateRGB2_R(duty) MID_TM_UpdateChannel2L(&htm36, duty);    // update to OC/PWM reload register
/*!< update TM36's (RGB2_G) */
#define API_UpdateRGB2_G(duty) MID_TM_UpdateChannel2H(&htm36, duty);    // update to OC/PWM reload register
/*!< update TM36's (RGB2_B) */
#define API_UpdateRGB2_B(duty) MID_TM_UpdateChannel1H(&htm36, duty);    // update to OC/PWM reload register

/*!< update TM26's OC0H (RGB3_R) */
#define API_UpdateRGB3_R(duty) MID_TM_UpdateChannel0H(&htm26, duty);    // update to OC/PWM reload register
/*!< update TM26's OC1H (RGB3_G) */
#define API_UpdateRGB3_G(duty) MID_TM_UpdateChannel1H(&htm26, duty);    // update to OC/PWM reload register
/*!< update TM26's OC1N (RGB3_B) */
#define API_UpdateRGB3_B(duty) MID_TM_UpdateChannel1L(&htm26, duty);    // update to OC/PWM reload register

/*!< Stop update PWM duty cycle */
#define API_StopUpdateDutyCycle(htm) MID_TM_RejectUpdateOC(htm);    
/*!< Start update PWM duty cycle */
#define API_StartUpdateDutyCycle(htm) MID_TM_AcceptUpdateOC(htm);    




/**
 * @name    Function announce
 * @brief   Config ADC base parameter.e.g. Clock/Resolution/Justified	
 */ 
///@{
void API_RGBLED_Init(void);
///@}

void API_RGBMode_Select(RGB_TYPE* RGBX, RGB_MODE_TYPE RGB_Mode);
void API_RGBColor_Select(RGB_TYPE* RGBX, uint8_t RGB_R_Param , uint8_t RGB_G_Param , uint8_t RGB_B_Param);
void API_RGB_Config(void);
void API_RGBLED_SW(void);




extern RGB_TYPE     RGB1;
extern RGB_TYPE     RGB2;
extern RGB_TYPE     RGB3;




#endif



