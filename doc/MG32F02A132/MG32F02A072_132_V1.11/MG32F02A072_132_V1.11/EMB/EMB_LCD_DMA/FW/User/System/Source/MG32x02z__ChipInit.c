

/**
 ******************************************************************************
 *
 * @file        MG32x02z__ChipInit.c
 * @brief       The demo ChipInit C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 * 
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */

#include "MG32x02z_ChipInit.h"


/**
 *******************************************************************************
 * @brief	    Chip inital.  
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void ChipInit(void)
{

    PW_Init();
    CSC_Init((uint32_t*) &CSC_InitConfig);
    RST_Init();
    GPIO_Init();
    
    //===Select inital outside Power & CSC & RST ====
    // From MG32x02z_ChipInit.h to select you need inital
    
    #if EXIC_Inital_En == 1
        EXIC_Init();
    #endif
    #if I2C0_Inital_En == 1
        I2C0_Init();
    #endif 
    #if I2C1_Inital_En == 1
        I2C1_Init();
    #endif 
    #if EMB_Inital_En == 1
        EMB_Initial();
    #endif
    #if MEME_Inital_En == 1
        MEM_Init();
    #endif
    #if GPL_Inital_En == 1
        GPL_Init();
    #endif
    #if IRQ_Inital_En == 1
        IRQ_Init();
    #endif
}








