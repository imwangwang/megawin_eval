
/**
 *******************************************************************************
 * @file        MG32x02z_MEM_IRQ.h
 *
 * @brief       The MEM Interrupt Request Handler H file
 *
 *              MG32x02z remote controller
 * @version     V1.00
 * @date        2016/10/25
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2015 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * #001_CTK_20140116 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 * --
 * >>
 * >>
 * #002_WJH_20140119
 * >> Bug2 description
 * -- Bug2 sub-description
 * #003_HWT_20140120
 * >> Bug3 description
 *
 @endif
 *******************************************************************************
 */



// <<< Use Configuration Wizard in Context Menu >>>


// <<< end of configuration section >>>    



#ifndef _MG32x02z_MEM_IRQ_H
#define _MG32x02z_MEM_IRQ_H
#define _MG32x02z_MEM_IRQ_H_VER                          0.01        /*!< File Version */

#ifdef __cplusplus
 extern "C" {
#endif

#if !(MG32x02z_H_VER == MG32x02z_MEM_IRQ_H_VER)
    #error "MG32x02z_MEM_H - Main/Module Version Mismatch !"
#endif


#include "MG32x02z_DRV.h"


void MEM_IRQ(void);


#ifdef __cplusplus
}
#endif

#endif


