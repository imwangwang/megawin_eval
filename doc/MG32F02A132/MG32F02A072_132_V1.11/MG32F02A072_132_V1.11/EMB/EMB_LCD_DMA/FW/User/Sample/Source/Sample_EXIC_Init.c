



/**
 ******************************************************************************
 *
 * @file        Sample_EXIC_Init.c
 * @brief       The demo EXIC'S inital sample C file
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */
#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_GPIO_DRV.H"
#include "MG32x02z_EXIC_DRV.H"



/**
*******************************************************************************
* @brief	   Initla all pin of Port A to be exic port. 
* @details     1.Port A inital.
*    \n        2.Set EXIC trigger mode.
*    \n        3.Set And Mask. 
*    \n        4.Set Or Mask.
*    \n        5.Clear All exic event flag of portA.
*    \n        6.Enable EXIC interrupt
*		 \n        7.Test
* @return      
* @exception   No
* @note        
* @par         Example
* @code
* @endcode             
*******************************************************************************
*/
void Sample_EXIC_Init(void)
{
    PIN_InitTypeDef  PINX_InitStruct;
    EXIC_TRGSTypeDef EXIC_TRGS;
    
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 11.0592M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->Port A = Enable if EXIC Port is PortA
    
    
    //====GPIO Inital==== 
    PINX_InitStruct.PINX_Pin                = PX_Pin_All;                     // Select Pin of the port to inital (PX_Pin_All is all pin of the port.)
    PINX_InitStruct.PINX_Mode								= PINX_Mode_OpenDrain_O;          // Select GPIO mode 
                                                                              //     1.QB: Quasi-Bidirection mode only for PC ) 
    PINX_InitStruct.PINX_PUResistant        = PINX_PUResistant_Enable;        // Select wether enable internal pull-up R or not.
    PINX_InitStruct.PINX_Speed              = PINX_Speed_Low;                 // Select wehter enable high speed mode
                                                                              //     1.(PINX_Speed_High mode only for PC0~3 , PD0~3 )    
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level0;           // Select output drive strength 
                                                                              //     1.(Level 0 & level 3 only for PE0~PE3)
    PINX_InitStruct.PINX_FilterDivider      = PINX_FilterDivider_Bypass;      // Select input filter divider.
    PINX_InitStruct.PINX_Inverse            = PINX_Inverse_Disable;           // Select input signal whether inverse or not.
                                                                              //     1.PINX_Inverse_Disable = L level or falling edge.
                                                                              //     2.PINX_Inverse_Enable  = H level or Rising edge.
    PINX_InitStruct.PINX_Alternate_Function = 0;                              // Select GPIO mode
    GPIO_PortMode_Config(IOMA,&PINX_InitStruct);                              // (Pin0 & Pin1) of PorA configuration
		GPIO_WritePort(GPIOA,0xFFFF); 																						// Initial PortA
    
    //====EXIC Inital====
    EXIC_TRGS.EXIC_Pin = EXIC_TRGS_ALL;                                       // The pin trigger mode select.
//    EXIC_TRGS.EXIC_Pin = EXIC_TRGS_PIN0 | EXIC_TRGS_PIN1 | EXIC_TRGS_PIN2;  //     1. You can selec any pin of the port.
    EXIC_TRGS.EXIC_TRGS_Mode = Edge;                                          // External interrupt pin edge/level trigger event select.
    EXIC_PxTriggerMode_Select(EXIC_PA,&EXIC_TRGS);                            // EXIC trigger mode configuration.
    
    EXIC_PxTriggerAndMask_Select(EXIC_PA , 0x0003);                           // EXIC_PA  AF trigger event select.
                                                                              //     1. Select PA_AF trigger pin(PA0, PA1).
                                                                              //     2. PA_AF is set if trigger event of select pin have to happen at same time(PA0 & PA1).  
    EXIC_PxTriggerOrMask_Select(EXIC_PA  , 0x000C);                           // EXIC_PA  OF trigger event select.
                                                                              //     1. Select PA_OF trigger pin(PA2, PA3).
                                                                              //     2. PA_OF is set if anyone trigger event of select pin happen(PA2 | PA3).   
    EXIC_ClearPxTriggerEventFlag(EXIC_PA, EXIC_PX_AllPIN );                   // Clear All pin of the port event flag.    
    
    EXIC_PxTriggerITEA_Cmd(EXIC_PA_IT , ENABLE);                              // Enable EXIC interrupt
    NVIC_EnableIRQ(EXINT0_IRQn);                                              //
    
    //===Interrupt handle====
    //Please refer to MG32x02z_EXIC_IRQ.c
		
		
		//=====Test====
		GPIO_WritePort(GPIOA,0xFFFC);	// into EXINT0(EXIC_PX_AF)	
		
		GPIO_WritePort(GPIOA,0xFFFF);
		GPIO_WritePort(GPIOA,0xFFF7);	// into EXINT0(EXIC_PX_OF)
		GPIO_WritePort(GPIOA,0xFFFF);
		GPIO_WritePort(GPIOA,0xFFFB); // into EXINT0(EXIC_PX_OF)
		
		
		GPIO_WritePort(GPIOA,0xFFFF);
		GPIO_WritePort(GPIOA,0xFFFE);	// do't into EXINT0
    
    
}



























