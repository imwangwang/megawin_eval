/**
  ******************************************************************************
 *
 * @file        Sample_SPI_Master_StandardSPI.c
 *
 * @brief       SPI master standard SPI sample code
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * #001_Hades_20180131
 *  >> File rename.
 *
 @endif
 *******************************************************************************
 */
 
#include "MG32x02z_DRV.h"

#define Dummy_Data 0xFFFFFFFF


/**
 *******************************************************************************
 * @brief  	    Sample SPI Master Standard SPI code
 * @details     1. Enable CSC to SPI clock
 *      \n      2. Default Initial SPI
 *      \n      3. Configure clock divider
 *      \n      4. Configure SPI data line, mode and data size...
 *      \n      5. Config SPI0 IO
 *      \n      6. Enable SPI
 *      \n      8. Read data
 *      \n      7. Send data
 *      \n      9. Disable SPI
 * @return	    	
 * @note        
 * @par         Example
 * @code
    Sample_SPI_MasterStandardSPI();
 * @endcode
 *******************************************************************************
 */
void Sample_SPI_MasterStandardSPI (void)
{
    PIN_InitTypeDef PINX_InitStruct;
    uint32_t RDAT;
    uint32_t TDAT;

    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12Mz
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    
    /*=== 1. Enable CSC to SPI clock ===*/
    //[A] When Use Wizard
    //Configure Peripheral On Mode Clock->SPI0 = Enable and Select SPI0_PR Source = CK_APB
    //Configure Peripheral On Mode Clock->Port B = Enable
    //[B] When Use Driver
//    UnProtectModuleReg(CSCprotect);                             // Unprotect CSC module
//    CSC_PeriphOnModeClock_Config(CSC_ON_SPI0, ENABLE);          // Enable SPI0 module clock
//    CSC_PeriphOnModeClock_Config(CSC_ON_PortB, ENABLE);		  // Enable PortB clock
//    CSC_PeriphProcessClockSource_Config(CSC_SPI0_CKS, CK_APB);  // CK_SPIx_PR = CK_APB = 12MHz
//    ProtectModuleReg(CSCprotect);                               // protect CSC module
       
    /*=== 2. Default Initial SPI ===*/
    SPI_DeInit(SPI0);
    
    /*=== 3. Configure clock divider ===*/                      // SPI clock = 1MHz
    SPI_Clock_Select(SPI0, SPI_CK_SPIx_PR);                     // CK_SPIx = CK_SPIx_PR
    SPI_PreDivider_Select(SPI0, SPI_PDIV_2);                    // PDIV outpu = CK_SPIx /2
    SPI_Prescaler_Select(SPI0, SPI_PSC_3);                      // Prescaler outpu = PDIV outpu /3
    SPI_Divider_Select(SPI0, SPI_DIV_2);                        // DIV outpu = PDIV outpu /2
    
    /*=== 4. Configure SPI data line, mode and data size... ===*/
    SPI_DataLine_Select(SPI0, SPI_Standard);                    // SPI data line standard SPI
    SPI_ModeAndNss_Select(SPI0, SPI_MasterWithNss);             // Master with NSS
    SPI_ClockPhase_Select(SPI0, SPI_LeadingEdge);               // CPHA = 0
    SPI_ClockPolarity_Select(SPI0, SPI_Low);                    // CPOL = 0
    SPI_FirstBit_Select(SPI0, SPI_MSB);                         // MSB first
    SPI_DataSize_Select(SPI0, SPI_8bits);                       // Data size 8bits
    
    /*=== 5. Config SPI0 IO ===*/
    PINX_InitStruct.PINX_Mode			    = PINX_Mode_PushPull_O;     // Pin select pusu pull mode
    PINX_InitStruct.PINX_PUResistant        = PINX_PUResistant_Enable;  // Enable pull up resistor
    PINX_InitStruct.PINX_Speed              = PINX_Speed_Low;           
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level0;     // Pin output driver full strength.
    PINX_InitStruct.PINX_FilterDivider      = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
    PINX_InitStruct.PINX_Inverse            = PINX_Inverse_Disable;     // Pin input data not inverse
    PINX_InitStruct.PINX_Alternate_Function = 2;                        // Pin AFS = 2
    
    GPIO_PinMode_Config(PINB(0),&PINX_InitStruct);                      // NSS setup at PB0
    GPIO_PinMode_Config(PINB(2),&PINX_InitStruct);                      // CLK setup at PB2
    GPIO_PinMode_Config(PINB(3),&PINX_InitStruct);                      // MOSI setup at PB3
    
    PINX_InitStruct.PINX_Mode				= PINX_Mode_OpenDrain_O;    // Setting open drain mode
    GPIO_PinMode_Config(PINB(1),&PINX_InitStruct);                      // MISO setup at PB1
    
    /*=== 6. Enable SPI ===*/
    SPI_Cmd(SPI0, ENABLE);                                              // Enable SPI
     
    /*=== 7. Read data ===*/
    // If you want get 1 byte, please send 1 byte 0x00.
    SPI_SetTxData(SPI0, SPI_3Byte, Dummy_Data);                         // Received 1 byte and send 1 byte the same time
    while(SPI_GetSingleFlagStatus(SPI0, SPI_RXF) == DRV_UnHappened);    // Wait RXF flag
    SPI_ClearFlag(SPI0, (SPI_TXF | SPI_RXF));                           // Clear TXF and RXF
    RDAT = SPI_GetRxData(SPI0);                                         // Get received data
    
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TCF flag
    SPI_ClearFlag(SPI0, SPI_TCF);                                       // Clear TCF

    /*=== 8. Send data ===*/ // Total transmit 5 bytes
    TDAT = RDAT + 0x04030201;
    SPI_SetTxData(SPI0, SPI_4Byte, TDAT);                               // Send 4 byte and received 4 byte the same time
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TXF) == DRV_UnHappened);    // Wait TXF flag
    SPI_ClearFlag(SPI0, (SPI_TXF | SPI_RXF));                           // Clear TXF and RXF
    
    SPI_SetTxData(SPI0, SPI_1Byte, 0x05);                               // Send 1 byte and received 1 byte the same time
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TCF flag
    SPI_ClearFlag(SPI0, SPI_TCF | SPI_TXF | SPI_RXF);                   // Clear TCF


    /*=== 9. Disable SPI ===*/
    while(SPI_GetNSSInputStatust(SPI0) == DRV_Low);                     // Wait SPI idle
    SPI_Cmd(SPI0, DISABLE);                                             // Disable SPI
}


//@del{
/**
 *******************************************************************************
 * @brief  	    Sample SPI Master Standard SPI code
 * @details  
 * @return	    	
 * @note        
 * @par         Example
 * @code
    Sample_SPI_MasterStandardSPI();
 * @endcode
 * @bug              
 *******************************************************************************
 */
void Sample_SPI_MasterStandard (void)
{
    PIN_InitTypeDef PINX_InitStruct;



    // Remember enable MG32x02z_CSC_Init.h Configure Peripheral On Mode Clock -> SPI
    UnProtectModuleReg(CSCprotect);                             // Unprotect CSC module
    CSC_PeriphOnModeClock_Config(CSC_ON_SPI0, ENABLE);          // Enable SPI0 module clock
    ProtectModuleReg(CSCprotect);                               // protect CSC module
    
    // DeInitial SPI
    SPI_DeInit(SPI0);
    
    // Interrupt Configure
    SPI_IT_Config(SPI0, SPI_INT_TX, ENABLE);                    // Enable SPI transmit data register empty can trigger interrupt
    SPI_IT_Config(SPI0, SPI_INT_TC, ENABLE);                    // Enable SPI transmission complete can trigger interrupt
    SPI_ITEA_Cmd(SPI0, ENABLE);                                 // Enable SPI0 interrupt all enable.
    NVIC_EnableIRQ(SPI0_IRQn);                                  // Enable NVIC SPI0
    // When you enable interrupt note
    // SPI_IRQ remember 

    // Setup clock divider
    UnProtectModuleReg(CSCprotect);                             // Unprotect CSC module
    CSC_PeriphProcessClockSource_Config(CSC_SPI0_CKS, CK_APB);  // CK_SPIx_PR = CK_APB = 12MHz
    ProtectModuleReg(CSCprotect);                               // protect CSC module
    SPI_Clock_Select(SPI0, SPI_CK_SPIx_PR);                     // CK_SPIx = CK_SPIx_PR
    SPI_PreDivider_Select(SPI0, SPI_PDIV_2);                    // PDIV outpu = CK_SPIx /2
    SPI_Prescaler_Select(SPI0, SPI_PSC_3);                      // Prescaler outpu = PDIV outpu /3
    SPI_Divider_Select(SPI0, SPI_DIV_2);                        // DIV outpu = PDIV outpu /2
    
    // Configure SPI
    SPI_DataLine_Select(SPI0, SPI_Standard);                    // SPI data line standard SPI
    SPI_ModeAndNss_Select(SPI0, SPI_MasterWithNss);             // Master with NSS
    SPI_ClockPhase_Select(SPI0, SPI_LeadingEdge);               // CPHA = 0
    SPI_ClockPolarity_Select(SPI0, SPI_Low);                    // CPOL = 0
    SPI_FirstBit_Select(SPI0, SPI_MSB);                         // MSB first
    SPI_DataSize_Select(SPI0, SPI_8bits);                       // Data size 8bits

    // Setup SPI keep work in SLEEP mode
    UnProtectModuleReg(CSCprotect);                             // Unprotect CSC module
    CSC_PeriphSleepModeClock_Config(CSC_SLP_SPI0, ENABLE);      // Enable SPI0 keep work in SLEEP mode.
    ProtectModuleReg(CSCprotect);                               // Protect CSC module
    
    // Config SPI0 IO
    PINX_InitStruct.PINX_Mode				= PINX_Mode_PushPull_O;
    PINX_InitStruct.PINX_PUResistant        = PINX_PUResistant_Enable;
    PINX_InitStruct.PINX_Speed              = PINX_Speed_Low;
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level0;
    PINX_InitStruct.PINX_FilterDivider      = PINX_FilterDivider_Bypass;
    PINX_InitStruct.PINX_Inverse            = PINX_Inverse_Disable;
    PINX_InitStruct.PINX_Alternate_Function = 2;                // Pin AFS = 2
    GPIO_PinMode_Config(PINB(0),&PINX_InitStruct);              // NSS at PB0
    
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level2;
    GPIO_PinMode_Config(PINB(2),&PINX_InitStruct);              // CLK at PB2
    GPIO_PinMode_Config(PINB(3),&PINX_InitStruct);              // MOSI at PB3
    
    PINX_InitStruct.PINX_Mode				= PINX_Mode_OpenDrain_O;
    GPIO_PinMode_Config(PINB(1),&PINX_InitStruct);              // MISO at PB1
    
    // Enable SPI module
    SPI_Cmd(SPI0, ENABLE);  
    
    // Send data
    SPI_SetTxData(SPI0, SPI_1Byte, 0x0F);                       // Send 1 byte
    
    // Read data
    // If you want get n byte, please send n byte 0xFF.
}
//@del}
