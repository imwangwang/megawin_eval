/**
  ******************************************************************************
 *
 * @file        Sample_IWDT_Init.c
 *
 * @brief       IWDT sample code
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * #001_Hades_20180131
 *  >> File rename.
 *
 @endif
 *******************************************************************************
 */
 
#include "MG32x02z_DRV.h"


/**
 *******************************************************************************
 * @brief  	    Sample IWDT init
 * @details     1. Enable CSC to IWDT clock
 *      \n      2. Configure IWDT clock
 *      \n      3. Enable IWDT module
 *      \n      4. Check flag action
 *      \n      5. Disable IWDT module
 * @return	    None	
 * @note        
 * @par         Example
 * @code
    Sample_IWDT_Init();
 * @endcode
 * @bug              
 *******************************************************************************
 */
void Sample_IWDT_Init (void)
{
    
    
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12Mz
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->IWDT = Enable

//    /*=== 1. Enable CSC to IWDT clock ===*/
//    UnProtectModuleReg(CSCprotect);                                 // Unprotect CSC module
//    CSC_PeriphOnModeClock_Config(CSC_ON_IWDT, ENABLE);              // Enable IWDT module clock
//    ProtectModuleReg(CSCprotect);                                   // protect CSC module
      
    /*=== 2. Configure IWDT clock ===*/
    UnProtectModuleReg(IWDTprotect);                                // Unprotect IWDT module
    IWDT_Divider_Select(IWDT_DIV_256);                              // DIV output = CK_IWDT /256
    
    /*=== 3. Enable IWDT module ===*/
    IWDT_Cmd(ENABLE);                                               // Enable IWDT module
    UnProtectModuleReg(IWDTprotect);                                // Protect IWDT module
    
    /*=== 4. Check flag action ===*/
    while(IWDT_GetSingleFlagStatus(IWDT_EW1F) == DRV_UnHappened);   // Wait IWDT early wakeup-1 happened
    IWDT_ClearFlag(IWDT_EW1F);                                      // Clear EW1F flag 
    
    while(IWDT_GetSingleFlagStatus(IWDT_EW0F) == DRV_UnHappened);   // Wait IWDT early wakeup-0 happened
    IWDT_ClearFlag(IWDT_EW0F);                                      // Clear EW0F flag
    
    while(IWDT_GetSingleFlagStatus(IWDT_TF) == DRV_UnHappened);     // Wait IWDT timer timeout happened
    IWDT_ClearFlag(IWDT_TF);                                        // Clear TF flag
    IWDT_RefreshCounter();                                          // Clear IWDT timer
    
    /*===  5. Disable IWDT module ===*/
    UnProtectModuleReg(IWDTprotect);                                // Unprotect IWDT module
    IWDT_Cmd(DISABLE);                                              // Disable IWDT module
    ProtectModuleReg(IWDTprotect);                                  // Protect IWDT module

}


//@del{
/**
 *******************************************************************************
 * @brief  	    Sample IWDT init
 * @details  
 * @return	    	
 * @note        
 * @par         Example
 * @code
    Sample_IWDT();
 * @endcode
 * @bug              
 *******************************************************************************
 */
void Sample_IWDT (void)
{
    // Remember enable MG32x02z_CSC_Init.h Configure Peripheral On Mode Clock -> IWDT
    UnProtectModuleReg(CSCprotect);                         // Unprotect CSC module
    CSC_PeriphOnModeClock_Config(CSC_ON_IWDT, ENABLE);      // Enable IWDT module clock
    ProtectModuleReg(CSCprotect);                           // protect CSC module
      
    UnProtectModuleReg(IWDTprotect);                        // Unprotect IWDT module
    // Interrupt Configure
    IWDT_IT_Config(IWDT_INT_EW1, ENABLE);                   // Enable IWDT early wakeup-1 can trigger interrupt
    IWDT_IT_Config(IWDT_INT_EW0, ENABLE);                   // Enable IWDT early wakeup-0 can trigger interrupt
    IWDT_IT_Config(IWDT_INT_TF, ENABLE);                    // Enable IWDT timer timeout can trigger interrupt
    SYS_ITEA_Cmd(ENABLE);                                   // Enable SYS_IEA
    NVIC_EnableIRQ(SYS_IRQn);                               // Enable NVIC SYS
    
    // Setup clock divider
    IWDT_Divider_Select(IWDT_DIV_256);                      // DIV output = CK_IWDT /256
    
    // Setup IWDT keep work in SLEEP mode
//    UnProtectModuleReg(CSCprotect);                         // Unprotect CSC module.
//    CSC_PeriphSleepModeClock_Config(CSC_SLP_IWDT, ENABLE);  // Enable IWDT keep work in SLEEP mode.
//    ProtectModuleReg(CSCprotect);                           // protect CSC module.
    
    // Setup IWDT keep work in STOP mode
//    UnProtectModuleReg(CSCprotect);                         // Unprotect CSC module.
//    CSC_PeriphStopModeClock_Config(CSC_STP_IWDT, ENABLE);   // Enable IWDT keep work in SLEEP mode.
//    ProtectModuleReg(CSCprotect);                           // protect CSC module.
//    IWDT_StopModeWakeUpEvent_Config(IWDT_EW1_WPEN, ENABLE); // Enable IWDT early wakeup-1 can trigger chip wake up in STOP mode.
//    IWDT_StopModeWakeUpEvent_Config(IWDT_EW0_WPEN, ENABLE); // Enable IWDT early wakeup-0 can trigger chip wake up in STOP mode.
//    IWDT_StopModeWakeUpEvent_Config(IWDT_TF_WPEN, ENABLE);  // Enable IWDT timer timeout can trigger chip wake up in STOP mode.
        
    // Enable IWDT module
    IWDT_Cmd(ENABLE);
    
    UnProtectModuleReg(IWDTprotect);                        // Protect IWDT module
    
    // Setup IWDT timer timeout trigger reset
//    UnProtectModuleReg(RSTprotect);
//    RST_CRstSource_Config(RST_IWDT_CE, ENABLE);             // Enable IWDT event cold reset trigger source.
//    RST_WRstSource_Config(RST_IWDT_WE, ENABLE);             // Enable IWDT event cold reset trigger source.
//    ProtectModuleReg(RSTprotect);
}
//@del}

