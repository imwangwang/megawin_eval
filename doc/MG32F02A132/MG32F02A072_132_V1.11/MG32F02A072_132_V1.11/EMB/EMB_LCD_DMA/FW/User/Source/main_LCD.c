/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

//#include "MG32x02z__Common_DRV.H"
//#include "MG32x02z_EXIC_DRV.H"
//#include "MG32x02z__IRQHandler.H"
#include "MG32x02z_MID.H"

#include "MG32x02z_GPIO_Init.h"
#include "MG32x02z_IRQ_Init.h"
#include "MG32x02z_I2C_Init.h"
#include "MG32x02z_EMB_Init.h"
#include "MG32x02z_MEM_Init.h"
#include "MG32x02z_GPL_Init.h"

#include "LCD.h"
#include "ltdc.h"
#include "ugui.h"
#include "LCDDEMO_RGBLED_API.h"
#include "LCDDEMO_KEY_API.H"
#include "API_Flash.H"

#include "Sample_I2C_ByteMode_Master.h"

//#incldue "meun.h"

extern DRV_Return URT_Init(URT_Struct* URTX);


//void ChipInit(void);
//void Sample_Code(void);
//extern UG_GUI gui;

#define TextCol 80
#define TextRow 20
uint32_t blue=0x07E0;

struct {
    struct{
        uint8_t Text[TextCol];
        uint8_t Font;
        uint8_t Length;
        uint16_t Color;
    }Text[TextRow];
    uint8_t ShowEnable;
    uint8_t AddIndex;
    uint16_t Accumulation;
}TextList;



void API_TextList_Init(void)
{
    uint8_t lx, ly;
    ly = 0;
    do{
        lx = 0;
        do{
            TextList.Text[ly].Text[lx] = 0x20;
        }while(++lx < TextCol);
        TextList.Text[ly].Color = 0;
        TextList.Text[ly].Font = 0;
        TextList.Text[ly].Length = 0;
    }while(++ly < TextRow);
    TextList.Accumulation = 0;
    TextList.AddIndex = 0;
    TextList.ShowEnable = 0;
}



void API_ShowTextList(void)
{
    uint16_t lYAxis = 0;
    uint8_t lShowIndex;
    uint8_t lShowCount = 0;
    if(TextList.ShowEnable == 0)
        return;
    LCD_Clear(BLACK);
    if(TextList.Accumulation < TextRow)
        lShowIndex = 0;
    else
        lShowIndex = TextList.AddIndex;
    do{
        LCD_ShowString(0, lYAxis, lcddev.width, 16, TextList.Text[0].Color, TextList.Text[lShowIndex].Font, TextList.Text[lShowIndex].Text);
        lShowIndex ++;
        if(lShowIndex >= TextRow)
            lShowIndex = 0;
        lYAxis += 16;
    }while(++lShowCount < TextRow);
}



void API_AddText2List(uint8_t *p, uint8_t Length, uint8_t Font, uint16_t Color)
{
    uint8_t *lpChar = p;
    uint8_t lCharCount = 0;
    do{
        TextList.Text[TextList.AddIndex].Text[lCharCount] = *lpChar;
        lpChar ++;
    }while(++lCharCount < TextCol);
    TextList.Text[TextList.AddIndex].Length = Length;
    TextList.Text[TextList.AddIndex].Font = Font;
    TextList.Text[TextList.AddIndex].Color = Color;

    TextList.AddIndex ++;
    if(TextList.AddIndex >= TextRow)
        TextList.AddIndex = 0;
    TextList.Accumulation ++;
    API_ShowTextList();
}


uint32_t TotalCount = 0;



struct{
    uint8_t Name[6];
    uint32_t Address;
    uint32_t Size;
    uint16_t Check;
}FlashIS;



union RBuffer{
    uint32_t W[512];
    uint16_t H[1024];
    uint8_t  B[2048];
}ReadDataBuffer;


void DMA_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = DISABLE;
        DMATestPattern.DestDINCSel = ENABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_SPI0_RX;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_MEM_Write;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = 1024;
    
        // source/destination config
        DMATestPattern.DMASourceAddr = &SPI0->RDAT;//(uint8_t *)&RcvBuf;
//        DMATestPattern.DMADestinationAddr = (uint32_t *)0x60000000;//(uint8_t *)&RcvBuf;
				
						 // Setting M2M simple parameter

        DMA_Base_Init(&DMATestPattern);
}



void DMAP_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = ENABLE;
        DMATestPattern.DestDINCSel = ENABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_MEM_Read;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_MEM_Write;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = 1024;
    
        // source/destination config
//        DMATestPattern.DMASourceAddr = &SPI0->RDAT;//(uint8_t *)&RcvBuf;
        DMATestPattern.DMADestinationAddr = (uint32_t *)0x60000000;//(uint8_t *)&RcvBuf;
				
						 // Setting M2M simple parameter

        DMA_Base_Init(&DMATestPattern);
}

void DMAU_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = DISABLE;
        DMATestPattern.DestDINCSel = ENABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_SPI0_RX;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_MEM_Write;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = 1024;
    
        // source/destination config
        DMATestPattern.DMASourceAddr = &SPI0->RDAT;//(uint8_t *)&RcvBuf;
        DMATestPattern.DMADestinationAddr = (uint32_t *)0x60000000;//(uint8_t *)&RcvBuf;
				
						 // Setting M2M simple parameter

        DMA_Base_Init(&DMATestPattern);
}

void LCD_Show_Picture(uint16_t nTh)
{
    uint32_t lBaseAddr = (nTh << 4);
    uint32_t lCount;
    uint32_t lAddr;
    union{
        uint16_t H[512];
        uint8_t B[1024];
    }lH_B;
    // Read Head 
    API_Flash_MultiBytesRead(0x0000000, (uint8_t *)&TotalCount, 0x4);

    API_Flash_MultiBytesRead(lBaseAddr, (uint8_t *)&FlashIS.Name, 6);
    API_Flash_MultiBytesRead((lBaseAddr + 6), (uint8_t *)&FlashIS.Address, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xA), (uint8_t *)&FlashIS.Size, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xE), (uint8_t *)&FlashIS.Check, 2);

    LCD_SetCursor(0, 0);		//?????? 
    LCD_WriteRAM_Prepare();	    //????GRAM
    __LCD_CS_CLR();\
    __LCD_RS_SET();\

    lAddr = FlashIS.Address;
    do{
        API_Flash_MultiBytesRead(lAddr, &lH_B.B[0], 1024);
        lCount = 0;
        do{
            if(lH_B.H[lCount] == 0xFFFF)
                __DATAOUT(0x0000);
            else
                __DATAOUT(lH_B.H[lCount]);
        }while(++ lCount < 512);
        lAddr += 1024;
    }while(lAddr < (FlashIS.Address + FlashIS.Size));
    __LCD_CS_SET();
}

void LCD_Show_Picturee(uint16_t nTh)
{
    uint32_t lBaseAddr = (nTh << 4);
    uint32_t lAddr;
    union{
        uint16_t H[512];
        uint8_t B[1024];
    }lH_B;
    // Read Head 
    API_Flash_MultiBytesRead(0x0000000, (uint8_t *)&TotalCount, 0x4);

    API_Flash_MultiBytesRead(lBaseAddr, (uint8_t *)&FlashIS.Name, 6);
    API_Flash_MultiBytesRead((lBaseAddr + 6), (uint8_t *)&FlashIS.Address, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xA), (uint8_t *)&FlashIS.Size, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xE), (uint8_t *)&FlashIS.Check, 2);

    LCD_SetCursor(0, 0);		//?????? 
    LCD_WriteRAM_Prepare();	    //????GRAM
    __LCD_CS_CLR();\
    __LCD_RS_SET();\

    lAddr = FlashIS.Address;
    do{
        API_Flash_MultiBytesReadd(lAddr, &lH_B.B[0], 1024);
       /* do{
            if(lH_B.H[lCount] == 0xFFFF)
                __DATAOUT(0x0000);
            else
                __DATAOUT(lH_B.H[lCount]);
        }while(++ lCount < 512);*/
				DMAP_Init();
				DMA_SetSourceAddress(DMAChannel0, &lH_B.B[0]);
				DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
				DMA_StartRequest(DMAChannel0);
				while (DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_TCF) == DRV_UnHappened);		
				DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
				DMA_Init();
        lAddr += 1024;
    }while(lAddr < (FlashIS.Address + FlashIS.Size));
    __LCD_CS_SET();
}

void LCD_Show_Pictureu(uint16_t nTh)
{
    uint32_t lBaseAddr = (nTh << 4);
    uint32_t lAddr;
    union{
        uint16_t H[512];
        uint8_t B[1024];
    }lH_B;
    // Read Head 
    API_Flash_MultiBytesRead(0x0000000, (uint8_t *)&TotalCount, 0x4);

    API_Flash_MultiBytesRead(lBaseAddr, (uint8_t *)&FlashIS.Name, 6);
    API_Flash_MultiBytesRead((lBaseAddr + 6), (uint8_t *)&FlashIS.Address, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xA), (uint8_t *)&FlashIS.Size, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xE), (uint8_t *)&FlashIS.Check, 2);
		
    LCD_SetCursor(0, 0);		//?????? 
    LCD_WriteRAM_Prepare();	    //????GRAM
    __LCD_CS_CLR();\
    __LCD_RS_SET();\
		
    lAddr = FlashIS.Address;
    do{
				DMAU_Init();
        API_Flash_MultiBytesReadu(lAddr, &lH_B.B[0], 1024);
         lAddr += 1024;
    }while(lAddr < (FlashIS.Address + FlashIS.Size));
    __LCD_CS_SET();
		DMA_Init();
}


void LCD_Show_Text(void)
{
    __LCD_BackLight_Off();
    LCD_Clear(BLACK);
    LCD_ShowString(0, 0, lcddev.width, 24, GBLUE, 24, (uint8_t *)"Megawin Company");
    LCD_ShowString(0, 24, lcddev.width, 24, BRED, 24, (uint8_t *)"ARM Cortex-M0 Series");
    LCD_ShowString(0, 48, lcddev.width, 24, YELLOW, 24, (uint8_t *)"Product MG32F02A");	
    LCD_ShowString(0, 72, lcddev.width, 24, GREEN, 24, (uint8_t *)"Show Features");
    LCD_ShowString(0, 96, lcddev.width, 24, BLUE, 24, (uint8_t *)"2018/02/17");

    LCD_ShowString(0, 144, lcddev.width, 16, WHITE, 16, (uint8_t *)"Megawin Company");	
    LCD_ShowString(0, 160, lcddev.width, 16, WHITE, 16, (uint8_t *)"ARM Cortex M0 Series");	
    LCD_ShowString(0, 176, lcddev.width, 16, WHITE, 16, (uint8_t *)"Product MG32F02A");
    LCD_ShowString(0, 192, lcddev.width, 16, WHITE, 16, (uint8_t *)"Show Features");
    LCD_ShowString(0, 208, lcddev.width, 16, WHITE, 16, (uint8_t *)"2018/02/17");

    LCD_ShowString(0, 240, lcddev.width, 12, WHITE, 12, (uint8_t *)"Megawin MG32F02A");	
    LCD_ShowString(0, 252, lcddev.width, 12, WHITE, 12, (uint8_t *)"ARM Cortex M0 Series");	
    LCD_ShowString(0, 264, lcddev.width, 12, WHITE, 12, (uint8_t *)"Product MG32F02A");
    LCD_ShowString(0, 276, lcddev.width, 12, WHITE, 12, (uint8_t *)"Show Features");
    LCD_ShowString(0, 288, lcddev.width, 12, WHITE, 12, (uint8_t *)"2018/02/17");
    __LCD_BackLight_On();
}



void LCD_Random_Text(void)
{   uint16_t lCount = 50;
    LCD_Clear(0xE71C);
    LCD_ShowString(0, 80, lcddev.width, 24, 0x001F, 24, (uint8_t *)"Megawin Company");
    LCD_ShowString(0, 104, lcddev.width, 24, 0x33E0, 24, (uint8_t *)"ARM Cortex-M0 Series");
    LCD_ShowString(0, 128, lcddev.width, 24, 0xF100, 24, (uint8_t *)"Product MG32F02A");
    Delay(1000);
    do{
        LCD_ShowString(rand() % 240, rand() % 320, lcddev.width, 24, rand() % 0xFFFF, 24, (uint8_t *)"Megawin Company");
        LCD_ShowString(rand() % 240, rand() % 320, lcddev.width, 24, rand() % 0xFFFF, 24, (uint8_t *)"ARM Cortex-M0 Series");
        LCD_ShowString(rand() % 240, rand() % 320, lcddev.width, 24, rand() % 0xFFFF, 24, (uint8_t *)"Product MG32F02A");	
    }while(-- lCount != 0);
}


void LCD_Random_Line(void)
{
    uint16_t lCount = 500;
    LCD_Clear(BLACK);
    do{
        LCD_DrawLine(rand() % 240, rand() % 320, rand() % 240, rand() % 320, rand() % 0xFFFF);
    }while(-- lCount != 0);
}

void LCD_Random_Rectangle(void)
{
    uint16_t lCount = 200;
    LCD_Clear(BLACK);
    do{
        LCD_DrawRectangle(rand() % 240, rand() % 320, rand() % 240, rand() % 320, rand() % 0xFFFF);
    }while(-- lCount != 0);
}

void LCD_Random_Fill(void)
{
    uint16_t lCount = 200;
    LCD_Clear(BLACK);
    do{
        LCD_Fill(rand() % 120, rand() % 160, rand() % 120, rand() % 160, rand() % 0xFFFF);
    }while(-- lCount != 0);
}

void LCD_Random_Circle(void)
{
    uint16_t lCount = 200;
    LCD_Clear(BLACK);
    do{
        LCD_Draw_Circle(rand() % 240, rand() % 320, rand() % 320, rand() % 0xFFFF);
    }while(-- lCount != 0);
}

void LCD_Random_Geometry(void)
{
    uint16_t lCount = 100;
    LCD_Clear(BLACK);
    do{
        LCD_DrawLine(rand() % 240, rand() % 320, rand() % 240, rand() % 320, rand() % 0xFFFF);
        LCD_DrawRectangle(rand() % 240, rand() % 320, rand() % 240, rand() % 320, rand() % 0xFFFF);
        LCD_Draw_Circle(rand() % 240, rand() % 320, rand() % 320, rand() % 0xFFFF);
    }while(-- lCount != 0);
}



typedef enum{
    LCD_Show_Logo = 1,
    LCD_Show_Text1,
//    LCD_Show_Text1 = 1,
    LCD_Show_GrayBar_1,
    LCD_Show_ColorBar1,
    LCD_Show_ColorBar2,
    LCD_Show_Line,
    LCD_Show_Rectangle,
    LCD_Show_Circle,
//    LCD_Show_Fill,
    LCD_Show_Geometry,
    LCD_Show_Text2,
    LCD_Show_Stop,
}LCD_Show_Typedef;



uint32_t            SPIFlashID;
uint32_t            ShowDelay = 0;
uint32_t            ShowDelayCapture;
MatrixKeyDef        key, oldkey;
RGB_MODE_TYPE       RGB1_Mode = RGB_Mode_SPECTRUM;
RGB_MODE_TYPE       RGB2_Mode = RGB_Mode_SPECTRUM;
RGB_MODE_TYPE       RGB3_Mode = RGB_Mode_SPECTRUM;
uint8_t             RGB1_light = 255;
uint8_t             RGB2_light = 255;
uint8_t             RGB3_light = 255;
uint8_t             DemoLoopEnable = 1;



void LCD_Show(uint8_t Template)
{
    switch(Template){
        case LCD_Show_Logo:
            SPIFlashID = MID_Flash_Read_ID();
            if((SPIFlashID == 0xFFFFFF) || (SPIFlashID == 0x00000000))
            {
                __LCD_BackLight_Off();
                LCD_Clear(BLACK);
                LCD_ShowString(0, 24, lcddev.width, 24, BRED, 24, (uint8_t *)"SPI Flash Not Ready");
                __LCD_BackLight_On();
            }
            else
                LCD_Show_Picture(1);
        break;
        case LCD_Show_GrayBar_1:
            LCD_GrayBar_1();
        break;
        case LCD_Show_ColorBar1:
            LCD_ColorBar_1();
        break;
        case LCD_Show_ColorBar2:
            LCD_ColorBar_2();
        break;
        case LCD_Show_Text1:
            LCD_Show_Text();
        break;
        case LCD_Show_Line:
            LCD_Random_Line();
        break;
        case LCD_Show_Rectangle:
            LCD_Random_Rectangle();
        break;
        case LCD_Show_Circle:
            LCD_Random_Circle();
        break;
//        case LCD_Show_Fill:
//            LCD_Random_Fill();
//        break;       
        case LCD_Show_Geometry:
            LCD_Random_Geometry();
        break;
        case LCD_Show_Text2:
            LCD_Random_Text();
        break;
        case LCD_Show_Stop:
        break;
    }
}


int main()
{
    uint8_t lcd_id[80];
		int Begin_Time,Finish_Time;
		double time;
    // Tick Initial
    // DRV_Return InitTick(uint32_t TickClock, uint32_t TickPriority)
    InitTick(48000000, 0);

    URT_Init(URT0);         // for Debug Information
    printf("%c[2J",27);
    printf("%c[H",27);

    API_Flash_Init();       /* Need include to ChipInit(); */
		DMA_Init();
    LCD_Init();
    IRQ_Init();
    printf("Initial Ready \n\r");

		while(1)
		{
			LCD_Clear(BLACK);
			Begin_Time = GetTick();
			LCD_Show_Picture(1); 																	//不使用DMA进行SPI FLASH的图片输出LCD
			Finish_Time = GetTick();
			time=Finish_Time-Begin_Time;
			time = 1000.0/time;
			sprintf((char *)lcd_id, "FLASH SPEED(Data from SPI):%.2lf Page/s", time);//输出刷图速度
			LCD_ShowString(0, 20, lcddev.width, 10, RED, 12, lcd_id);
			Delay(2000);
			 
			LCD_Clear(BLACK);
			Begin_Time = GetTick();
			LCD_Show_Picturee(1); 																	//使用DMA将SPI FLASH的图片输出到RAM，再用DMA从RAM输出到LCD
			Finish_Time = GetTick();
			time=Finish_Time-Begin_Time;
			time = 1000.0/time;
			sprintf((char *)lcd_id, "FLASH SPEED(Data from SPI):%.2lf Page/s", time);//输出刷图速度
			LCD_ShowString(0, 20, lcddev.width, 10, RED, 12, lcd_id);
			Delay(2000);
			 
			LCD_Clear(BLACK);
			Begin_Time = GetTick();
			LCD_Show_Pictureu(1); 																	//使用DMA将SPI FLASH的图片直接输出到LCD
			Finish_Time = GetTick();
			time=Finish_Time-Begin_Time;
			time = 1000.0/time;
			sprintf((char *)lcd_id, "FLASH SPEED(Data from SPI):%.2lf Page/s", time);//输出刷图速度
			LCD_ShowString(0, 20, lcddev.width, 10, RED, 12, lcd_id);
			Delay(2000);
		}
		
}


