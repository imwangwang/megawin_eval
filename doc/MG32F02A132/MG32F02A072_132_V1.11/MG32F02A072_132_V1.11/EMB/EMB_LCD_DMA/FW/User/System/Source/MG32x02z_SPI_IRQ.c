/**
 ******************************************************************************
 *
 * @file        MG32x02z_SPI_IRQ.c
 *
 * @brief       The demo code SPI Interrupt Request C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE 
 * Modify History:
 * #001_Hades_20180131
 *  >> File rename.
 *
 @endif
 *******************************************************************************
 */
 
#include "MG32x02z_SPI_DRV.h"


/**
 *******************************************************************************
 * @brief  	    SPI module IRQ
 * @details  
 * @return	    
 * @note
 * @bug              
 *******************************************************************************
 */
__weak void SPI0_IRQ (void)
{
    do
    {
        if(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_Happened)   
        {
            // SPI transmission complete happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_TCF);
        }
        
        if(SPI_GetSingleFlagStatus(SPI0, SPI_RXF) == DRV_Happened)   
        {
            // Receive data buffer high level happened.
            // To do...
            SPI_ClearFlag (SPI0,SPI_RXF);
        }
        
        if(SPI_GetSingleFlagStatus(SPI0, SPI_TXF) == DRV_Happened)   
        {
            // Transmit data buffer level low happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_TXF);
        }

//@del{        
//        if(SPI_GetSingleFlagStatus(SPI0, SPI_MODF) == DRV_Happened)   
//        {
//            // Mode detect fault happened.
//            // To do...
//            SPI_ClearFlag (SPI0, SPI_MODF);
//        }
//@del}
        
        if(SPI_GetSingleFlagStatus(SPI0, SPI_WEF) == DRV_Happened)   
        {
            // Slave mode write error happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_WEF);
        }
        
        if(SPI_GetSingleFlagStatus(SPI0, SPI_ROVRF) == DRV_Happened)   
        {
            // Receive overrun happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_ROVRF);
        }
        
        if(SPI_GetSingleFlagStatus(SPI0, SPI_TUDRF) == DRV_Happened)   
        {
            // Slave mode transmit underrun happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_TUDRF);
        }
        
    }while(SPI_GetAllFlagStatus(SPI0) != 0);
}



