
#include "stdio.h"
#include "string.h"
#include "MG32x02z_DRV.h"



volatile uint8_t gURT0_First = 0;
int fputc(int ch, FILE *f)
{
    if(gURT0_First == 0)
        gURT0_First = 1;
    else
        while(URT_GetITSingleFlagStatus(URT0, URT_IT_TC) == DRV_UnHappened);
    URT_ClearITFlag(URT0, URT_IT_TC);
    URT_SetTXData(URT0, 1, ch);
    return ch;
}

//volatile uint8_t gURT2_First = 0;
//int fputc(int ch, FILE *f)
//{
////    GPIOA->OUT.B[0] = ch;
//    if(gURT2_First == 0)
//        gURT2_First = 1;
//    else
//        while(URT_GetITSingleFlagStatus(URT2, URT_IT_TC) == DRV_UnHappened);

//    URT_ClearITFlag(URT2, URT_IT_TC);
//    URT_SetTXData(URT2, 1, ch);
//    return ch;
//}


//int fgetc(FILE *f)
//{
//    int ch;
//    while(URT0->STA.MBIT.RXF == 0);
//    ch = UART_RXData1BYTE_Read(URT0);
//    return ch;
//}
//#endif

//#if WorkStation
//  #if FPGA
//    int fputc(int ch, FILE *f)
//    {
//        GPIOA->OUT.B[0] = ch;
//        return ch;
//    }
//  #else
//    int fputc(int ch, FILE *f)
//    {
//        GPIOD->OUT.B[0] = ch;
//        return ch;
//    }
//  #endif
//#endif


