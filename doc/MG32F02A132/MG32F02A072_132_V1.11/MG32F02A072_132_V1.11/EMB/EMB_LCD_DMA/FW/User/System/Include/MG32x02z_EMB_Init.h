
/**
 ******************************************************************************
 *
 * @file        MG32x02z_EMB_Init.h
 *
 * @brief       MG32x02z EMB Initial header File
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2016/04/11
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *#001_CTK_20140116 		// bugNum_Authour_Date 
 *>>Bug1 description 
 *--Bug1 sub-description 
 *--
 *--
 *>>
 *>>
 *#002_WJH_20140119 
 *>>Bug2 description 
 *--Bug2 sub-description 
 *#003_HWT_20140120 
 *>>Bug3 description 
 *
 @endif 
 ******************************************************************************
 */ 

#include "MG32x02z_DRV.h"



void EMB_Initial(void);



