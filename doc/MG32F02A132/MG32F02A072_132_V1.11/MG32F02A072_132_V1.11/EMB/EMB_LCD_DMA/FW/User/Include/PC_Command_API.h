
/**
 ******************************************************************************
 *
 * @file        LCDDEMO_RGBLED_API.H
 *
 * @brief       This file provides firmware functions to manage the following 
 *              functionalities of the ADC peripheral:
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/02/12
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *>>
 *--
 *--
 *>>
 *>>
 *
 @endif 
 ******************************************************************************
 */ 
 
 
 
 
#ifndef _PC_COMMAND_API_H



#define COMMAND_HEAD            0xFF
#define GET_MPU_BUSY            0xE5
#define REG_WRITE_BYTE          0x10
#define REG_READ_BYTE           0x11
#define REG_WRITE_WORD          0x12
#define REG_READ_WORD           0x13
#define REG_WRITE_DWORD         0x14
#define REG_READ_DWORD          0x15
#define REG_WRITE_NBYTE         0x16
#define REG_READ_NBYTE          0x17
#define WRITE_MPU_BUFFER        0xE6
#define READ_MPU_BUFFER         0xE7
#define SEND_CHECKSUM           0x1C
#define GET_CHECKSUM            0x1D
#define SEND_USER_INFORMATION   0xE0

#define OUTPUT_HEAD
#define OUTPUT_BYTE             0x01
#define OUTPUT_WORD             0x02
#define OUTPUT_DWORD            0x04
#define OUTPUT_NBYTE            0xFF
#define OUTPUT_SYSTEM_STATUS    0x05
 
 
 
 #endif