
/**
 ******************************************************************************
 *
 * @file        MG32x02z_ChipInit.H
 *
 * @brief       By the file select you want to function inital.
 *   
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *>>
 *--
 *--
 *>>
 *>>
 *
 @endif 
 ******************************************************************************
 */

#ifndef _MG32x02z_ChipInit_H
 
#define _MG32x02z_ChipInit_H

// <<< Use Configuration Wizard in Context Menu >>>

//<q0>EXIC Inital
#define EXIC_Inital_En   0
//<h> I2C Inital
//<q0> I2C0 Inital 
//<q1> I2C1 Inital 
#define I2C0_Inital_En   0
#define I2C1_Inital_En   0
//</h>
//<q0> EMB Inital
#define EMB_Inital_En    0
//<q0> MEM_Inital
#define MEM_Inital_En    0
//<q0> GPL Inital 
#define GPL_Inital_En    0
//<q0> IRQ Inital    
#define IRQ_Inital_En    0



// <<< end of configuration section >>>    

#include "MG32x02z__Common_DRV.h"
#include "MG32x02z_CSC_Init.h"
#include "MG32x02z_PW_Init.h"
#include "MG32x02z_RST_Init.h"
#include "MG32x02z_GPIO_Init.h"
#include "MG32x02z_EXIC_Init.h"
#include "MG32x02z_IRQ_Init.h"
#include "MG32x02z_I2C_Init.h"
#include "MG32x02z_EMB_Init.h"
#include "MG32x02z_MEM_Init.h"
#include "MG32x02z_GPL_Init.h"

#endif
