
/**
 ******************************************************************************
 *
 * @file        Sample.H
 *
 * @brief       By the file select you want to sample code.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *>>
 *--
 *--
 *>>
 *>>
 *
 @endif 
 ******************************************************************************
 */ 


// <<< Use Configuration Wizard in Context Menu >>>

//<o0> Sample code marcro select   <0=> GPIO <1=> EXIC <2=> IWDT <3=> ADC <4=> DAC <5=> CMP 
//                                 <6=> TM0x <7=> TM1x <8=> TM2x <9=> TM3x
//                                 <10=>UARTx <11=> SPIx <12=> WWDT <13=> I2Cx
#define Sample_Code_Marcro_Select    0
    #define Sample_Code_Marcro_GPIO    0
    #define Sample_Code_Marcro_EXIC    1
    #define Sample_Code_Marcro_IWDT    2
    #define Sample_Code_Marcro_ADC     3
    #define Sample_Code_Marcro_DAC     4
    #define Sample_Code_Marcro_CMP     5
    #define Sample_Code_Marcro_TM0x    6
    #define Sample_Code_Marcro_TM1x    7
    #define Sample_Code_Marcro_TM2x    8
    #define Sample_Code_Marcro_TM3x    9
    #define Sample_Code_Marcro_URTx    10
    #define Sample_Code_Marcro_SPI     11
    #define Sample_Code_Marcro_WWDT    1
    #define Sample_Code_Marcro_I2Cx    13
//<h> GPIO sample code
    //<o0> GPIO sample code select   <0=> GPIO read and write inital
    #define Sample_Code_GPIO    0
        #define Sample_Code_GPIO_RWInit    0
//</h>

//<h> EXIC sample code
    //<o0> EXIC sample code select   <0=> EXIC inital
    #define Sample_Code_EXIC    0
        #define Sample_Code_EXIC_Init      0
//</h>

//<h> IWDT sample code
    //<o0> IWDT sample code select   <0=> IWDT inital
    #define Sample_Code_IWDT    0
        #define Sample_Code_IWDT_Init      0
//</h>

//<h> WWDT sample code
    //<o0> WWDT sample code select   <0=> WWDT Inital
    #define Sample_Code_WWDT    0
        #define Sample_Code_WWDT_Init      0
//</h>

//<h> ADC sample code
    //<o0> ADC sample code select    <0=> ADC conversion
    #define Sample_Code_ADC     0
        #define Sample_Code_ADC_CONV       0
//</h>

//<h> DAC sample code
    //<o0> DAC sample code select    <0=> DAC output
    #define Sample_Code_DAC     0
        #define Sample_Code_DAC_Output     0
//</h>

//<h> CMP sample code
    //<o0> CMP sample code select    <0=> CMP inital
    #define Sample_Code_CMP     0
        #define Sample_Code_CMP_Init       0
//</h>

//<h> TM0x sample code
    //<o0> TM0x sample code select   <0=> TM00 delay <1=> TM01 TRGO_UEV
    #define Sample_Code_TM0     1
        #define Sample_Code_TM00_Dealy      0
        #define Sample_Code_TM01_TRGO_UEV   1
//</h>


//<h> TM1x sample code
    //<o0> TM1x sample code select   <0=> TM10 clock output <1=> TM16 auto stop
    #define Sample_Code_TM1     1
        #define Sample_Code_TM10_CLKOUT     0
        #define Sample_Code_TM16_AutoStop   1
//</h>

//<h> TM2x sample code
    //<o0> TM2x sample code select   <0=> TM20 capture <1=> TM26 output compare
    #define Sample_Code_TM2     1
        #define Sample_Code_TM20_Capture    0
        #define Sample_Code_TM26_OutputCmp  1
//</h>
//<h> TM3x sample code
    //<o0> TM3x sample code select   <0=> TM36 PWM
    #define Sample_Code_TM3     0
        #define Sample_Code_TM36_PWM        0
//</h>

//<h> SPIx sample code 
    //<o0> SPIx sample code select   <0=> SPI0 slave (standard SPI) <1=> SPI0 master (standard SPI)
    #define Sample_Code_SPI     1
        #define Sample_Code_SPI0_Slave      0
        #define Sample_Code_SPI0_Master     1
//</h>

//<h> UARTx sample code
    //<o0> UARTx sample code select  <0=> URT0 inital   <1=> URT1 inital <2=> URT2 inital <3=> URT3 inital
    #define Sample_Code_URT     3
        #define Sample_Code_URT0_Init       0
        #define Sample_Code_URT1_Init       1
        #define Sample_Code_URT2_Init       2
        #define Sample_Code_URT3_Init       3
//</h>

//<h> I2Cx sample code
    //<o0> I2Cx sample code select <0=> I2C0_ByteMode_Master <1=> I2C1_ByteMode_Master
    #define Sample_Code_I2C     1
        #define Sample_Code_I2C0_ByteMode_Master    0
        #define Sample_Code_I2C1_ByteMode_Master    1
//</h>


// <<< end of configuration section >>> 



#if Sample_Code_Marcro_Select == Sample_Code_Marcro_I2Cx
    #include "Sample_I2C_ByteMode_Master.h"
#endif


/**
 * @name	GPIO sample code
 *   		
 */ 
///@{ 
void Sample_GPIO_RWInit(void);

///@}

/**
 * @name	EXIC sample code
 *   		
 */ 
///@{ 
void Sample_EXIC_Init(void);

///@}

/**
 * @name	IWDT sample code
 *   		
 */ 
///@{ 
void Sample_IWDT_Init (void);

///@}

/**
 * @name	WWDT sample code
 *   		
 */ 
///@{ 
void Sample_WWDT_Init (void);

///@}



/**
 * @name	ADC sample code
 *   		
 */ 
///@{ 
uint16_t Sample_ADC_Conversion(void);

///@}


/**
 * @name	DAC sample code
 *   		
 */ 
///@{ 
void Sample_DAC_Output(void);

///@}


/**
 * @name	CMP sample code
 *   		
 */ 
///@{ 
void Sample_CMP_Init(void);

///@}


/**
 * @name	TM0x sample code
 *   		
 */ 
///@{ 
void Sample_TM00_Delay(void);
void Sample_TM01_TRGO_UEV(void);

///@}


/**
 * @name	TM1X sample code
 *   		
 */ 
///@{ 
void Sample_TM10_ClockOut(void);
void Sample_TM16_AutoStop(void);
///@}


/**
 * @name	TM2X sample code
 *   		
 */ 
///@{ 
void Sample_TM20_Capture(void);
void Sample_TM26_OC(void);
///@}

/**
 * @name	TM3X sample code
 *   		
 */ 
///@{ 
void Sample_TM36_PWM(void);

///@}

/**
 * @name	SPIx sample code
 *   		
 */ 
///@{ 
void Sample_SPI_SlaveStandardSPI(void);
void Sample_SPI_MasterStandardSPI(void);
///@}



/**
 * @name	UARTx sample code
 *   		
 */ 
///@{ 
void Sample_URT0_Init(void);
void Sample_URT1_Init(void);
void Sample_URT2_Init(void);
void Sample_URT3_Init(void);
///@}












