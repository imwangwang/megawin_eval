#ifndef __DELAY_H
#define __DELAY_H 			   
#include "MG32x02z_DRV.H"  
//////////////////////////////////////////////////////////////////////////////////  
//本程序只供學習使用，未經作者許可，不得用於其它任何用途
//ALIENTEK STM32F407開發板
//使用SysTick的普通計數模式對延遲進行管理(支持ucosii)
//包括delay_us,delay_ms
//正點原子@ALIENTEK
//技術論壇:www.openedv.com
//修改日期:2014/5/2
//版本：V1.0
//版權所有，盜版必究。
//Copyright(C) 廣州市星翼電子科技有限公司 2014-2024
//All rights reserved
//********************************************************************************
//修改說明
//無
////////////////////////////////////////////////////////////////////////////////// 	 
void delay_init(uint8_t SYSCLK);
void delay_ms(uint16_t nms);
void delay_us(uint32_t nus);

#endif



