

/**
 ******************************************************************************
 *
 * @file        Sample_UART2_Init.c
 * @brief       The demo UART2'S inital sample C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */
 
 
 
#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_URT_DRV.H"

#define URTX    URT2

 /**
 *******************************************************************************
 * @brief	   Sample URT2 initial
 * @details     1. Set CSC inital.
 *     \n       2. Set GPIO inital.
 *     \n       3. Set RX/TX Baudrate.
 *     \n       4. Set data character.
 *     \n       5. Set URT2 mode.
 *     \n       6. Set Data line.
 *     \n       7. Set Data buffer control.
 *     \n       8. Enable URT2 interrupt.
 *     \n       9. Enable URT2 
 *     \n      10. Receive data into interrupt function.
 *     \n      11. Send data of receive.
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
void Sample_URT2_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    
    //==Set CSC init
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 11.0592M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->Port B/URT2 = Enable
    //Configure Peripheral On Mode Clock->URT2->Select URT2_PR Source = CK_APB(11.0592)
    
    //==Set GPIO init
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->Pin2/3
    //GPIO port initial is 0xFFFF
    //Pin2 mode is PPO/Pin3 mode is ODO
    //Pin2/3 pull-up resister Enable
    //Pin2/3 function URT2_TX/RX
    
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 3;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 5;	                //Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    //BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 3);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 3);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTX, ENABLE);	                            //Enable TX
    URT_RX_Cmd(URTX, ENABLE);	                            //Enable RX
    
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URTX, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URTX, URT_IDLEMode_No);
    URT_TXGaudTime_Select(URTX, 0);
    
    //=====Enable URT Interrupt=====//
    URT_IT_Cmd(URTX, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URTX, ENABLE);
    NVIC_EnableIRQ(URT123_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}



















