

#include <MG32x02z.h>
#include "MG32x02z_TM_MID.h"
#include "MG32x02z_ChipInit.h"
#include "MG32x02z_MEM_DRV.H"
#include "MG32x02z_ADC_MID.H"
#include "LCDDEMO_KEY_API.H"
#include "LCDDEMO_RGBLED_API.H"
#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_URT_DRV.H"

void ChipInit(void);
DRV_Return URT_Init(URT_Struct* URTX);


extern TM_HandleTypeDef    htm26, htm36;


int main(void)
{
    // ------------------------------------------------------------------------
    // use TM26 for PWM LED (H/W) model
    
    MatrixKeyDef        key;

    
    // ------------------------------------------------------------------------
    // MEM (Because AHB=48MHz)
    //__MEM_UnProtect();
//    __MEM_SetFlashWaitState(MEM_FWAIT_ONE);
    //__MEM_SetFlashWaitState(MEM_FWAIT_TWO);
    //__MEM_Protect();
    
    // CSC initial    
    ChipInit();
    
    RGB1_R = RGB1_G = RGB1_B = 1;
    RGB2_R = RGB2_G = RGB2_B = 1;
    
    // IRQ initial    
    IRQ_Init();
    
    
    
    
    
    
    
    
    // ------------------------------------------------------------------------
    // API_RGBLED_Init
    // ------------------------------------------------------------------------
    API_RGBLED_Init();
    
    // ------------------------------------------------------------------------
    // API_Key_Init function
    // ------------------------------------------------------------------------
    API_Key_Init();
    
    
    URT_Init(URT0);
    printf("initial ready\n\r");
    // ------------------------------------------------------------------------
    // wait for time (in main routine)
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // API_ReadKey function (period to call this)
    // ------------------------------------------------------------------------
    do{
    key = API_ReadKey();
    printf("    Key = %d\n\r", key);
    switch(key)
    {
        case SKey1:
            printf("    SKey1\n\r");
            // to do ...
            break;
        case SKey2:
            printf("    SKey2\n\r");
            // to do ...
            break;
        case SKey3: 
            printf("    SKey3\n\r");
            // to do ...
            break;
        case SKey4:
            printf("    SKey4\n\r");
            // to do ...
            break;
        case SKey5: 
            printf("    SKey5\n\r");
            // to do ...
            break;
        case SKey6:
            printf("    SKey6\n\r");
            // to do ...
            break;
        case SKey7: 
            printf("    SKey7\n\r");
            // to do ...
            break;
        case SKey8:
            printf("    SKey8\n\r");
            // to do ...
            break;
        case SKey9: 
            printf("    SKey9\n\r");
            // to do ...
            break;
        case SKey10:
            printf("    SKey10\n\r");
            // to do ...
            break;
        case SKey11:
            printf("    SKey11\n\r");
            // to do ...
            break;
        case SKey12:
            printf("    SKey12\n\r");
            // to do ...
            break;
        case PKey1: 
            printf("    PKey1\n\r");
            // to do ...
            break;
        case PKey2:
            printf("    PKey2\n\r");
            // to do ...
            break;
        case PKey3: 
            printf("    PKey3\n\r");
            // to do ...
            break;
        case PKey4:
            printf("    PKey4\n\r");
            // to do ...
            break;
        case PKey5: 
            printf("    PKey5\n\r");
            // to do ...
            break;
        case PKey6:
            printf("    PKey6\n\r");
            // to do ...
            break;
        default:
            break;
    }
    }while(1);

    // ------------------------------------------------------------------------
    // TM26 for H/W PWM RGB LED3
    // ------------------------------------------------------------------------
    {
        // Stop update PWM duty cycle
        API_StopUpdateDutyCycle(&htm26);
        
        // update H/W RGB LED (RGB3_R/G/B) 
        API_UpdateRGB3_R(128);                 
        API_UpdateRGB3_G(32); 
        API_UpdateRGB3_B(64);       

        // Enable update PWM duty cycle
        API_StartUpdateDutyCycle(&htm26);
    }

    // ------------------------------------------------------------------------
    // TM36 for S/W PWM RGB LED1/2
    // ------------------------------------------------------------------------
    {
        // Stop update PWM duty cycle
        API_StopUpdateDutyCycle(&htm36);
        
        // update duty cycle (RGB1_R/G/B)
        API_UpdateRGB1_R(20);      
        API_UpdateRGB1_G(40);      
        API_UpdateRGB1_B(80);      
        
        // update duty cycle (RGB2_R/G/B)
        API_UpdateRGB2_R(200);      
        API_UpdateRGB2_G(100);      
        API_UpdateRGB2_B(50);      

        // Enable update PWM duty cycle
        API_StartUpdateDutyCycle(&htm36);
    }
    
    
    
    
    
    
}


///**
// *******************************************************************************
// * @brief       replace fputc for printf sub routine
// * @param[in]   ch : specified the data transfer to UART
// * @param[in]   f : no used
// * @return		ch 
// *******************************************************************************
// */
//int fputc(int ch, FILE *f)
//{ 
//    URT_SetTXData(URT0, 1, ch);
//    while(URT_GetITSingleFlagStatus(URT0, URT_IT_TC) == DRV_UnHappened);   
//    URT_ClearITFlag(URT0, URT_IT_TC);
//    return ch;
//}


//// <<< Use Configuration Wizard in Context Menu >>>
////<o0> URT BaudRateGenerator clock source frequence is(2000000-48000000) <2000000-48000000>
////<o1> Baudrate is(600-256000) <600-256000>
//#define URT_BDCLK_SOURCE     48000000
//#define URT_BR_VALUE         115200
//// <<< end of configuration section >>>




///**
// *****************************************************************************************
// * @fn      UART_TXRXBaudRate_Configuration
// * @brief   URT interrupts all enable or disable
// * @details  
// * @param   URTx          : where x can be (0 to 3) to select the UART peripheral
// * @param   URT_Frequence : PROC frequence value (ex 12MHz =  12000000)
// * @param   URTBaudRate   : TX and RX baudrate value (ex 9600)
// * @return        
// * @bug         
// * @note    1.The RX and TX clock source is from uart internal clock in the function.
// *          2.The interanl clock is from PROC (APB or System clock according CSC setting)
// *          3.The baud rate is about the value of URTBaudRate.
// *****************************************************************************************
// */
//DRV_Return URT_TXRXBaudRate_Configuration(URT_Struct* URTX , uint32_t URT_Frequence , uint32_t URTBaudRate )
//{
//	int8_t   i,j,k;
//	uint32_t tmp;
//	uint8_t  TXOverSamplingSampleNumber;
//    uint8_t  RXOverSamplingSampleNumber;
//	URT_BRG_TypeDef URT_BRGStruct;
//    
//	
//	
//	
//	
//	tmp = URT_Frequence / URTBaudRate;											
//	//================TX and RX oversamplig value===================
//	j = 0;
//	for(i=8;i<32;i++)
//	{
//		k = tmp % i;
//		if(k==0)
//		{
//			TXOverSamplingSampleNumber = (i - 1);
//			RXOverSamplingSampleNumber = (i - 1);
//			break;
//		}
//		else
//		{
//			if((i-k)>j)
//			{
//				j = i - k;
//				TXOverSamplingSampleNumber = (i - 1);
//				RXOverSamplingSampleNumber = (i - 1);
//			}
//		}
//	}
//	
//	//==========prescaler and baudrate counter reload value============
//	tmp = tmp / (TXOverSamplingSampleNumber  + 1);
//	j = 0;
//	for(i=2;i<16;i++)															
//	{
//		k = tmp % i;
//		if(k==0)
//		{
//			URT_BRGStruct.URT_PrescalerCounterReload = (i - 1);
//			break;
//		}
//		else
//		{
//			if((i-k)>j)
//			{
//				j = (i - k);
//				URT_BRGStruct.URT_PrescalerCounterReload = (i - 1);
//			}
//		}
//	}
//	tmp = tmp / (URT_BRGStruct.URT_PrescalerCounterReload + 1);
//	if(tmp==0)
//	{
//		return(DRV_Failure);												    //TX and RX baudrate can't setting.
//	}
//	URT_BRGStruct.URT_BaudRateCounterReload = (tmp - 1);
//	
//	
//	    
//    
//    URT_BRGStruct.URT_InteranlClockSource = URT_BDClock_PROC;
//    URT_BRGStruct.URT_BaudRateMode = URT_BDMode_Separated;
//    URT_BaudRateGenerator_Config(URTX,&URT_BRGStruct);
//    
//    URT_TXClockSource_Select(URTX , URT_TXClock_Internal);
//    URT_RXClockSource_Select(URTX , URT_RXClock_Internal);
//    URT_RXOverSamplingMode_Select( URTX, URT_RXSMP_3TIME);
//    URT_TXOverSamplingSampleNumber_Select( URTX, TXOverSamplingSampleNumber);
//	URT_RXOverSamplingSampleNumber_Select( URTX, RXOverSamplingSampleNumber);
//	URT_BaudRateGenerator_Cmd(URTX , ENABLE);
//	

//	return(DRV_Success);
//}

///**
// *****************************************************************************************
// * @fn      UART_Inital
// * @brief   UART inital
// * @details  
// * @param   URTx          : where x can be (0 to 3) to select the UART peripheral
// * @param   
// * @param   
// * @return        
// * @bug         
// * @note    
// *****************************************************************************************
// */
//DRV_Return URT_Init(URT_Struct* URTX)
//{
//    DRV_Return URT_Init_Result;
//    
//	URT_Data_TypeDef URT_Datastruct;

//    
//    
//    URT_Datastruct.URT_TX_DataLength = URT_DataLength_8;
//    URT_Datastruct.URT_RX_DataLength = URT_DataLength_8;
//    URT_Datastruct.URT_TX_DataOrder  = URT_DataTyped_LSB;
//    URT_Datastruct.URT_RX_DataOrder  = URT_DataTyped_LSB;
//    URT_Datastruct.URT_TX_Parity     = URT_Parity_No;
//    URT_Datastruct.URT_RX_Parity     = URT_Parity_No;
//    URT_Datastruct.URT_TX_StopBits   = URT_StopBits_1_0;
//    URT_Datastruct.URT_RX_StopBits   = URT_StopBits_1_0;
//    URT_Datastruct.URT_TX_DataInverse= DISABLE;
//    URT_Datastruct.URT_RX_DataInverse= DISABLE;
//    URT_DataCharacter_Config(URTX,&URT_Datastruct);   
//    
//    
//    
//    URT_Mode_Select( URTX , URT_URT_mode);
//    URT_DataLine_Select(URTX,URT_DataLine_2);
//	URT_Init_Result = URT_TXRXBaudRate_Configuration(URTX,URT_BDCLK_SOURCE,URT_BR_VALUE);		

//    if(URT_Init_Result==DRV_Success)
//    {
//        
//       URT_TX_Cmd(URTX,ENABLE);
//       URT_RX_Cmd(URTX,ENABLE);
//	   URT_Cmd(URTX,ENABLE);
//    }
//    
//    return(URT_Init_Result);
//}


