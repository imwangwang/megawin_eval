


/**
 ******************************************************************************
 *
 * @file        Sample_GPIO_Init.c
 * @brief       The demo GPIO'S inital sample C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */
#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_GPIO_DRV.H"
 
 
/**
*******************************************************************************
* @brief	   Sample GPIOA initial
* @details     1. Port A Initial
*     \n       2. PortA read/write
*     \n       3. PA0 inital
*     \n       4. PA0 read/write
* @return      
* @exception   No
* @note
* @par         Example
* @code
* @endcode         
*******************************************************************************
*/
void Sample_GPIO_RWInit(void)
{
    uint16_t  ReadData;
    PIN_InitTypeDef PINX_InitStruct;
    
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 11.0592M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->Port A = Enable
 
	
    //===GPIO Port Inital====
    PINX_InitStruct.PINX_Pin                = PX_Pin_All;                              // Select Pin of the port to inital (PX_Pin_All is all pin of the port.)
    PINX_InitStruct.PINX_Mode								= PINX_Mode_OpenDrain_O;   // Select GPIO mode 
                                                                                       //     1.QB: Quasi-Bidirection mode only for PC ) 
    PINX_InitStruct.PINX_PUResistant        = PINX_PUResistant_Enable;                 // Select wether enable internal pull-up R or not.
    PINX_InitStruct.PINX_Speed              = PINX_Speed_Low;                          // Select wehter enable high speed mode
                                                                                       //     1.(PINX_Speed_High mode only for PC0~3 , PD0~3 )    
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level0;                    // Select output drive strength 
                                                                                       //     1.(Level 1 & level 3 only for PE0~PE3)
    PINX_InitStruct.PINX_FilterDivider      = PINX_FilterDivider_Bypass;               // Select input filter divider.
    PINX_InitStruct.PINX_Inverse            = PINX_Inverse_Disable;                    // Select input signal whether inverse or not.
    PINX_InitStruct.PINX_Alternate_Function = 0;                                       // Select GPIO mode
    GPIO_PortMode_Config(IOMA,&PINX_InitStruct);                                       // (Pin0 & Pin1) of PorA configuration
    GPIO_WritePort(GPIOA,0xFFFF);													   // Initial PortA
    
		//===PORT Read=====	
    ReadData = GPIO_ReadPort(GPIOA);                                                    // Read PortA Status
    
    //===PORT Write====    
    GPIO_WritePort(GPIOA,0x1234);                                                       // Write PortA = 0x1234 
		





    //===GPIO Pin Inital====
    PINX_InitStruct.PINX_Mode			    = PINX_Mode_OpenDrain_O;            // Select GPIO mode 
                                                                                //     1.QB: Quasi-Bidirection mode only for PC ) 
    PINX_InitStruct.PINX_PUResistant        = PINX_PUResistant_Enable;          // Select wether enable internal pull-up R or not.
    PINX_InitStruct.PINX_Speed              = PINX_Speed_Low;                   // Select wehter enable high speed mode
                                                                                //     1.(PINX_Speed_High mode only for PC0~3 , PD0~3 )
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level0;             // Select output drive strength 
                                                                                //     1.(Level 1 & level 3 only for PE0~PE3)
    PINX_InitStruct.PINX_FilterDivider      = PINX_FilterDivider_Bypass;        // Select input filter divider.
    PINX_InitStruct.PINX_Inverse            = PINX_Inverse_Disable;             // Select input signal whether inverse or not.
    PINX_InitStruct.PINX_Alternate_Function = 0;                                // Select GPIO mode
    GPIO_PinMode_Config(PINA(0),&PINX_InitStruct);                              // PA0 configuration
        
    //===PIN Read=====
    ReadData = PA0;                                                             // Read PA0 Status
    
    //===PIN Write====
    ReadData = 0;
    PA0 = ReadData;		                                                         // Write PA0 = 0;
}





