

/**
 ******************************************************************************
 *
 * @file        MG32x02z_TM36_IRQ.c
 * @brief       The TM36 Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/03/28
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */ 


// <<< Use Configuration Wizard in Context Menu >>>


// <<< end of configuration section >>>    



#include "MG32x02z_TM_DRV.h"






/**
 *******************************************************************************
 * @brief	    TM36 interrupt function.
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
__weak void TM36_IRQ(void)
{
    // IC/OC 
    if (TM_GetSingleFlagStatus(TM36, TMx_CF3A) == DRV_Happened)
    {
        // Timer IC3 falling edge flag/OC3 event sub flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_CF3A);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_CF2A) == DRV_Happened)
    {
        // Timer IC2 falling edge flag/OC2 event sub flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_CF2A);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_CF1A) == DRV_Happened)
    {
        // Timer IC1 falling edge flag/OC1 event sub flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_CF1A);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_CF0A) == DRV_Happened)
    {
        // Timer IC0 falling edge flag/OC0 event sub flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_CF0A);
    }
    
    //
    if (TM_GetSingleFlagStatus(TM36, TMx_CF3A) == DRV_Happened)
    {
        // Timer IC3 falling edge flag/OC3 event sub flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_CF3A);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_CF2A) == DRV_Happened)
    {
        // Timer IC2 falling edge flag/OC2 event sub flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_CF2A);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_CF1A) == DRV_Happened)
    {
        // Timer IC1 falling edge flag/OC1 event sub flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_CF1A);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_CF0A) == DRV_Happened)
    {
        // Timer IC0 falling edge flag/OC0 event sub flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_CF0A);
    }
    
    // 2nd / Main counter
    if (TM_GetSingleFlagStatus(TM36, TMx_TUF2) == DRV_Happened)
    {
        // 2nd Timer underflow flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_TUF2);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_TUF) == DRV_Happened)
    {
        // Main Timer underflow flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_TUF);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_TOF2) == DRV_Happened)
    {
        // 2nd Timer overflow flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_TOF2);
    }
    if (TM_GetSingleFlagStatus(TM36, TMx_TOF) == DRV_Happened)
    {
        // Main Timer overflow flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_TOF);
    }

    // external trigger
    if (TM_GetSingleFlagStatus(TM36, TMx_EXF) == DRV_Happened)
    {
        // Timer external trigger flag
        // To do...
        
        TM_ClearFlag (TM36, TMx_EXF);
    }
    
}



