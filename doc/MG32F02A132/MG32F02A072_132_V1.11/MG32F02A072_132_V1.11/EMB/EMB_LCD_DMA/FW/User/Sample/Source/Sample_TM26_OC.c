/**
  ******************************************************************************
 *
 * @file        Sample_TM26_OC.c
 *
 * @brief       Output 16-bit Output Compare function of TM26
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "MG32x02z_TM_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***
//  <h> TM26 counter period
//      <o0> prescaler counter range (1~65536)  <1-65536>
//      <o1> main counter range (1~65536) <1-65536>
//  </h>
#define TM26_PrescalerCounter_Range 1
#define TM26_MainCounter_Range      65536

//  <h> Match point (Output to TM26_OC00 = GPIOD Pin2)
//      <o0> First match point (1~65536) <1-65536>
//      <o1> Second match point (1~65536)  <1-65536>
//  </h>
#define TM26_1st_MatchPoint       15000
#define TM26_2nd_MatchPoint       32768

//*** <<< end of configuration section >>>    ***

/**
 *******************************************************************************
 * @brief	    Toggle when main counter match CC0A of TM26.
 *    \n        Then CC0A will reload from CC0B after main counter overflow
 * @details     1.initial & modify TimeBase parameter 
 *    \n        2.config TM26 channel 0 function
 *    \n        3.config TM26 channel 0 Output (TM26_OC00)
 *    \n        4.set output compare match point
 *    \n        5.clear all flag
 *    \n        6.Enable Timer
 * @note        The period = (prescaler*main counter)/12MHz (CK_TM26_PR=12MHz) (s)
 *******************************************************************************
 */
void Sample_TM26_OC(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
     

    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->TM26 = Enable
    //Configure Peripheral On Mode Clock->Port B = Enable
	
    //==Set GPIO init 
    //TM26_OC00 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOD->PD2
    //GPIO port initial is 0xFFFF
    //PD2 mode is PPO
    //PD2 function TM26_OC00
	
	 
    TM_DeInit(TM26);
    
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_MainClockDirection =TM_UpCount;
    TM_TimeBase_InitStruct.TM_Period = TM26_MainCounter_Range - 1; 
    TM_TimeBase_InitStruct.TM_Prescaler = TM26_PrescalerCounter_Range - 1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
    
    TM_TimeBase_Init(TM26, &TM_TimeBase_InitStruct);
   
    // ----------------------------------------------------
    // 2.config TM26 channel 0 function 
    TM_CH0Function_Config(TM26, TM_OutputCompare);
    
    // ----------------------------------------------------
    // 3.config TM26 channel 0 Output 
    TM_OC00Output_Cmd(TM26,ENABLE);    
    TM_InverseOC0z_Cmd(TM26, DISABLE);
    TM_OC0zOutputState_Init(TM26, CLR);

    // ----------------------------------------------------
    // 4.set output compare match point
    TM_SetCC0A(TM26,TM26_1st_MatchPoint);        
    TM_SetCC0B(TM26,TM26_2nd_MatchPoint);

    // ----------------------------------------------------
    // 5.clear all flag
    TM_ClearFlag(TM26, TMx_CF0A | TMx_CF0B);
    
    // ----------------------------------------------------
    // 6.Enable Timer
    TM_Timer_Cmd(TM26,ENABLE);
    
    return;
}




