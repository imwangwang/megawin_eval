/**
 ******************************************************************************
 *
 * @file        MG32x02z_SPI_IRQ.c
 *
 * @brief       The demo code SPI Interrupt Request C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.11
 * @date        2018/05/10
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */
 
#include "MG32x02z_SPI_DRV.h"
#include "MG32x02z_DRV.H"
#define SPI_NSS PB0 
/**
 *******************************************************************************
 * @brief  	    SPI module IRQ
 * @details  
 * @return	    
 * @note
 *******************************************************************************
 */
__weak void SPI0_IRQ (void)
{
    int x,y;
	  uint32_t RDAT;
//		do
 //   {

	/*		
			  if(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_Happened)   
        {
            // SPI transmission complete happened.
            // To do...
					
            SPI_ClearFlag (SPI0, SPI_TCF);
        }
    */    
        if(SPI_GetSingleFlagStatus(SPI0, SPI_RXF) == DRV_Happened)   
        {
					SPI_ClearFlag(SPI0, SPI_ALLF);// Clear flag
					SPI_TransferBidirection_Select (SPI0, SPI_BDIR_IN);
					SPI_ClearRxData(SPI0);				//Clear data which send and received by itself that can receive data sent by slave
					
					SPI_SetTxData(SPI0, SPI_1Byte, 0xFF);						 	// if data is 1 byte,please send 1byte,otherwise 0xFFFFFFFF,then you can get your data 
					PE14=0;
					while(SPI_GetSingleFlagStatus(SPI0, SPI_RXF) == DRV_UnHappened);	// Wait TCF flag
					
					SPI_ClearFlag(SPI0, (SPI_TXF | SPI_RXF));
					RDAT=SPI_GetRxData(SPI0);
					printf("     RX:0x%02X\n",RDAT);
	//				SPI_NSS = 1;                                                         // NSS = 1
        }
				
  /*      
        if(SPI_GetSingleFlagStatus(SPI0, SPI_TXF) == DRV_Happened)   
        {
            // Transmit data buffer level low happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_TXF);
        }

   
        if(SPI_GetSingleFlagStatus(SPI0, SPI_MODF) == DRV_Happened)   
        {
            // Mode detect fault happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_MODF);
        }

        
        if(SPI_GetSingleFlagStatus(SPI0, SPI_WEF) == DRV_Happened)   
        {
            // Slave mode write error happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_WEF);
        }
        
        if(SPI_GetSingleFlagStatus(SPI0, SPI_ROVRF) == DRV_Happened)   
        {
            // Receive overrun happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_ROVRF);
        }
        
        if(SPI_GetSingleFlagStatus(SPI0, SPI_TUDRF) == DRV_Happened)   
        {
            // Slave mode transmit underrun happened.
            // To do...
            SPI_ClearFlag (SPI0, SPI_TUDRF);
        }
 */       
//    }while(SPI_GetAllFlagStatus(SPI0) != 0);
}



