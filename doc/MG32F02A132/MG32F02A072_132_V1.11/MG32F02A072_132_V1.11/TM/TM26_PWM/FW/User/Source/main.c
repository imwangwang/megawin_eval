/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用TM26输出PWM波形到PE13
 *				和PE14。
 *				此demo会在PE13和PE14输出PWM波形，用户
 *				可设置TM_SetCCnA和TM_SetCCnB的第二个参数
 *				设置占空比。
 *				
 *				注意：
 *
 *
 *
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */

#include "MG32x02z_DRV.H"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

//if user wants to set PWM period 1ms and CK_TM00_PR is 12MHz.
//The Total clocks is 12M*1ms = 12000.
//User can set "clock prescaler"=100 and "pulse width"=120 .   
#define TM26_PrescalerCounter_Range 120
#define TM26_MainCounter_Range      100

/***********************************************************************************
函数名称:	void Sample_TM26_PWM (void)
功能描述:	TM26初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void Sample_TM26_PWM(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
    PIN_InitTypeDef PINX_InitStruct;
    
		UnProtectModuleReg(CSCprotect);
    CSC_PeriphProcessClockSource_Config(CSC_TM26_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_TM26, ENABLE);					  // Enable TIM26 module clock
		CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);					  // Enable Port E clock
		ProtectModuleReg(CSCprotect);

    TM_DeInit(TM26);

			//==Set GPIO init
		PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 								// Pin select Push Pull mode
		PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  				// Enable pull up resistor
		PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
		PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 						// Pin output driver full strength.
		PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;			// Pin input deglitch filter clock divider bypass
		PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 						// Pin input data not inverse

		PINX_InitStruct.PINX_Alternate_Function  = 7;														// Pin AFS = TIM26 OC
		GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 		  										// Set PE13 to TIM26 OC11
		GPIO_PinMode_Config(PINE(14),&PINX_InitStruct); 		  										// Set PE14 to TIM26 OC12
    
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_MainClockDirection =TM_UpCount;
    TM_TimeBase_InitStruct.TM_Period = TM26_MainCounter_Range-1; 
    TM_TimeBase_InitStruct.TM_Prescaler = TM26_PrescalerCounter_Range-1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;   
    TM_TimeBase_Init(TM26, &TM_TimeBase_InitStruct);
    // ----------------------------------------------------
    // 2.config TM26 channel 1 function 
    TM_CH1Function_Config(TM26, TM_16bitPWM);					//PWM模式
    // ----------------------------------------------------
    // 3.Enable TM26 channel 1 Output (just output TM26_OC11)
    TM_OC11Output_Cmd(TM26,ENABLE);    
    TM_InverseOC1z_Cmd(TM26, DISABLE);
    TM_OC1zOutputState_Init(TM26, CLR);
		// ----------------------------------------------------
		// 3.Enable TM26 channel 1 Output (just output TM26_OC12)
    TM_OC12Output_Cmd(TM26,ENABLE);    
    TM_InverseOC1z_Cmd(TM26, DISABLE);
    TM_OC1zOutputState_Init(TM26, CLR);
    // ----------------------------------------------------
    // 4.设置占空比(Duty cycle %) for PWM channel0.(设定范围值为1~MainCounter_Range的值）
    TM_SetCC1A(TM26,90);        
    TM_SetCC1B(TM26,90);		// reload value when overflow
    // ----------------------------------------------------
    // 9.select Edge Align
    TM_AlignmentMode_Select(TM26, TM_EdgeAlign);    
    // ----------------------------------------------------
    // 10.clear flag
    TM_ClearFlag(TM26, TMx_CF0A | TMx_CF1A | TMx_CF2A);   
    // ----------------------------------------------------
    // 11.Timer enable
    TM_Timer_Cmd(TM26,ENABLE);

}

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO
	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
		
	/* Configure peripheral clock */
 	ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{

	CSC_Init();
	Sample_TM26_PWM();
  while(1);
		
}
