/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用TM10做全计数器功能，并学会
 *				调整定时器频率。
 *				此demo会在定时器溢出时反转PE15 IO状态。
 *				且PE9为低电平时调整定时器周期。
 *				
 *				
 *				注意：
 *
 *
 *
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"
#include <stdio.h>
#include "TYPE.h"


#define URTX URT0

#define TM10_PRE_2nd_RELOAD			(u16)((u32)(200*1000*12.00/1-1)%65536)		// 200ms 200ms的溢出需要的计数器值就是：
																				//每12MHz的时间计数器值会+1,也就是1个T=1/f，那么200ms需要计数器加多少？就是(200*1000)/T   %65536是取高16位 /是低16位
#define TM10_MAIN_RELOAD			(u16)((u32)(200*1000*12.00/1-1)/65536)		// 200ms


/***********************************************************************************
函数名称:	void Sample_URT0_Init(void)
功能描述:	UART0初始化 
		  TXD(PB8),RXD(PB9)
		  8,n,1 115200bps@12MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void Sample_URT0_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    PIN_InitTypeDef PINX_InitStruct;
    
    UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);						// Enable UART0 Clock
		CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);						// Enable PortB Clock
    ProtectModuleReg(CSCprotect);

		PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
		PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
		PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
		PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
		PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
		PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_TX
		GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD at PB8

		PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_RX
		GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD at PB9  
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 3;	                //Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    //BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 25);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 25);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTX, ENABLE);	                            //Enable TX
    URT_RX_Cmd(URTX, ENABLE);	                            //Enable RX

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URTX, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URTX, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URTX, 0);
    
    //=====Enable URT Interrupt=====//
 //   URT_IT_Config(URTX, URT_IT_RX, ENABLE);
  //  URT_ITEA_Cmd(URTX, ENABLE);
  //  NVIC_EnableIRQ(URT0_IRQn);
		
    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}

/***********************************************************************************
函数名称:	int fputc(int ch,FILE *f)
功能描述:	printf函数重定向
输入参数:	int ch,FILE *f
返回参数:	ch  
*************************************************************************************/
int fputc(int ch,FILE *f)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
	return ch;
}

/***********************************************************************************
函数名称:	void UartSendByte(int ch)
功能描述:	Uart发送函数
输入参数:	int ch
返回参数:	
*************************************************************************************/
void UartSendByte(int ch)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
}

/***********************************************************************************
函数名称:	void TM10_IRQHandler(int ch)
功能描述:	TM10中断函数
输入参数:	void
返回参数:	
*************************************************************************************/
void TM10_IRQHandler(void)
{
	if (TM_GetSingleFlagStatus(TM10, TMx_TOF) == DRV_Happened)	   // Main Timer overflow flag
	{
		PE15=~PE15;
		TM_ClearFlag (TM10, TMx_TOF);
	}

}

/***********************************************************************************
函数名称:	void TM10_Init(void)
功能描述:	TM10初始化
输入参数:	void
返回参数:	
*************************************************************************************/
void TM10_Init(void)
{	
	TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
	 
	//==Set TM1x Clock
	UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_TM10_CKS, CK_APB);
	CSC_PeriphOnModeClock_Config(CSC_ON_TM10,ENABLE);						// Enable TM10 Clock
	ProtectModuleReg(CSCprotect);					
		
	TM_DeInit(TM10);
			
	// ----------------------------------------------------
	// Set TimeBase structure parameter
	TM_TimeBase_InitStruct.TM_CounterMode = Full_Counter;					// 全计数器模式
	TM_TimeBase_InitStruct.TM_MainClockSource = TM_CK_INT;					// 主计数器源: (全计数模式下,无需设置)
	TM_TimeBase_InitStruct.TM_2ndClockSource = TM_CK_INT;					// 预分频器(2nd计数器)计数器源: TM_CK_INT
	TM_TimeBase_InitStruct.TM_MainClockDirection = TM_UpCount;				// 主计数器向上计数 
	TM_TimeBase_InitStruct.TM_2ndClockDirection = TM_UpCount;				// 预分频器(2nd计数器)向上计数
		
	TM_TimeBase_InitStruct.TM_Prescaler = TM10_PRE_2nd_RELOAD;				// 预分频器(2nd计数器)重载值	
	TM_TimeBase_InitStruct.TM_Period = TM10_MAIN_RELOAD;					// 主计数器重载值
	TM_TimeBase_Init(TM10, &TM_TimeBase_InitStruct);
		 
	//== SET TM10 ClockOut
	TM_ClockOutSource_Select(TM10, MainCKO);
	TM_ClockOut_Cmd(TM10, ENABLE);	

	// ----------------------------------------------------
	// 2.clear TOF flag
	TM_ClearFlag(TM10, TMx_TOF);
			
	// ----------------------------------------------------
	// 3.Enable TM10/TM16 interrupt 
	TM_IT_Config(TM10,TMx_TIE_IE,ENABLE);									// 主计数器溢出中断使能
		
	TM_ITEA_Cmd(TM10,ENABLE);

	// ----------------------------------------------------
	// Timer enable
	TM_Timer_Cmd(TM10,ENABLE);
		
	// ----------------------------------------------------
	// NVIC Enable TM10/TM16
	NVIC_EnableIRQ(TM10_IRQn); 

	
}

/***********************************************************************************
函数名称:	void SetT1xReload(u32 dwFreq)
功能描述:	重设TIM10重载值
输入参数:	
返回参数:	  
*************************************************************************************/
void SetT1xReload(u32 dwFreq)
{
	DWordTypeDef dwT1Value;
	dwFreq=dwFreq*2;
	dwT1Value.DW=(48*1000000)/dwFreq-1;    //与设置TM1X重载值一样：设置重载值就是设置溢出计数值，从0开始到溢出值溢出需要的T就是我们的1/dwFreq，而
																					//每48MHz的时间计数器值+1，即1/48MHz时间计数器值+1，那么总的需要计数值就是T/系统频率时间，即 （1/dwFreq）/（1/48MHz） -1
	TM10->ARR.H[0]=dwT1Value.W.WHigh;				//数据手册278页：计数器为全计数模式或级联模式，时钟周期是TMX_ARR和PSARR设置
	TM10->PSARR.H[0]=dwT1Value.W.WLow;
}


/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{

  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
  CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
  CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO
	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	

	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);						// Enable PortE Clock
	
  ProtectModuleReg(CSCprotect);
    
}



/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	uint32_t i=0;
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();

	//==Set GPIO init
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push pull Output mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_High;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	
	PINX_InitStruct.PINX_Alternate_Function  = 0;			
	GPIO_PinMode_Config(PINE(14),&PINX_InitStruct);							//LED指示灯IO 
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct);							//LED指示灯IO 
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 	 	// Pin select open drain Output mode
	GPIO_PinMode_Config(PINE(9),&PINX_InitStruct);							//模拟按键IO
	PE15=1;
	Sample_URT0_Init();
	TM10_Init();
	printf("hello!\n");

	while(1)
	{
		if(PE9 == 0)			//按键按下
		{
			i+=50;
			SetT1xReload(i);
		}
	}
			
}
