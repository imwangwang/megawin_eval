
/**
 ******************************************************************************
 *
 * @file        MG32x02z_ADC_IRQ.c
 * @brief       The ADC Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/03/28
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */
// <<< Use Configuration Wizard in Context Menu >>>


// <<< end of configuration section >>>    



#include "MG32x02z_ADC_DRV.h"






/**
 *******************************************************************************
 * @brief	    ADC interrupt function.
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
__weak void ADC_IRQ(void)
{
    
    // accumulation 0/1/2
    if (ADC_GetSingleFlagStatus(ADC0, ADC_SUMOVRF) == DRV_Happened)
    {
        // ADC0 data sum-0,1,2  register overrun flag 
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_SUMOVRF);
		ADC_ClearSum0Flags(ADC0, ADC_SUMxUF);
		ADC_ClearSum1Flags(ADC0, ADC_SUMxUF);
		ADC_ClearSum2Flags(ADC0, ADC_SUMxUF);
    }
  
    if (ADC_GetSingleFlagStatus(ADC0, ADC_SUMCF) == DRV_Happened)
    {
        // ADC0 data sum-0,1,2  accumulation complete flag 
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_SUMCF);
    }

    if (ADC_GetSingleFlagStatus(ADC0, ADC_SUMOF) == DRV_Happened)
    {
        // ADC0 data sum-0,1,2 accumulation overflow or underflow flag
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_SUMOF);
    }

    
    // window detect
    if (ADC_GetSingleFlagStatus(ADC0, ADC_WDHF) == DRV_Happened)
    {
        // ADC0 voltage window detect outside high event flag
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_WDHF);
    }
  
    if (ADC_GetSingleFlagStatus(ADC0, ADC_WDIF) == DRV_Happened)
    {
        // ADC0 voltage window detect inside event flag 
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_WDIF);
    }

    if (ADC_GetSingleFlagStatus(ADC0, ADC_WDLF) == DRV_Happened)
    {
        // ADC0 voltage window detect outside low event flag  
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_WDLF);
    }

    // conversion overrun
    if (ADC_GetSingleFlagStatus(ADC0, ADC_OVRF) == DRV_Happened)
    {
        // ADC0 conversion overrun event flag   
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_OVRF);
    }
    
    // conversion end
    if (ADC_GetSingleFlagStatus(ADC0, ADC_ESCNVF) == DRV_Happened)
    {
        // ADC0 channel scan conversion end flag   
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_ESCNVF);
    }
    if (ADC_GetSingleFlagStatus(ADC0, ADC_E1CNVF) == DRV_Happened)
    {
        // ADC0 one-time conversion end flagg  
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_E1CNVF);
    }
    
    // sampling end
    if (ADC_GetSingleFlagStatus(ADC0, ADC_ESMPF) == DRV_Happened)
    {
        // ADC0 sampling end flag 
        // To do...
        
        ADC_ClearFlag (ADC0, ADC_ESMPF);
    }    


}



