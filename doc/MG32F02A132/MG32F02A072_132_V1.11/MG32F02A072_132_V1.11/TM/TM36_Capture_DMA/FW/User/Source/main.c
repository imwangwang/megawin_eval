/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用TM36输入捕获PWM波形。
 *				此demo会利用DMA捕获10个脉冲到数组中，
 *				并在串口打印这10个脉冲的计数器值。
 *				该Demo可与TM20_Capture感受使用DMA的区别。
 *				
 *				注意：
 *				TIM中仅TM36的IC3支持使用DMA捕获。
 *
 *
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define TM36_IC3_TriggerSrc       1
#define URTX URT0
uint16_t RcvBuf[10]={0};

/***********************************************************************************
函数名称:	void Sample_URT0_Init(void)
功能描述:	UART0初始化 
		  TXD(PB8),RXD(PB9)
		  8,n,1 115200bps@12MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void Sample_URT0_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    PIN_InitTypeDef PINX_InitStruct;

    UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);						// Enable UART0 Clock
		CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);						// Enable PortB Clock
    ProtectModuleReg(CSCprotect);

    PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
		PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
		PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
		PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
		PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
		PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_TX
		GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD at PB8

		PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_RX
		GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD at PB9  

    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 3;	                //Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    //BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 25);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 25);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTX, ENABLE);	                            //Enable TX
    URT_RX_Cmd(URTX, ENABLE);	                            //Enable RX

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URTX, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URTX, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URTX, 0);
    
    //=====Enable URT Interrupt=====//
 //   URT_IT_Config(URTX, URT_IT_RX, ENABLE);
  //  URT_ITEA_Cmd(URTX, ENABLE);
  //  NVIC_EnableIRQ(URT0_IRQn);
		
    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}

/***********************************************************************************
函数名称:	int fputc(int ch,FILE *f)
功能描述:	printf函数重定向
输入参数:	int ch,FILE *f
返回参数:	ch  
*************************************************************************************/
int fputc(int ch,FILE *f)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
	return ch;
}

/***********************************************************************************
函数名称:	void UartSendByte(int ch)
功能描述:	Uart发送函数
输入参数:	int ch
返回参数:	
*************************************************************************************/
void UartSendByte(int ch)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
}

/***********************************************************************************
函数名称:	void TM36_Capture_Init (void)
功能描述:	TM36输入捕获初始化
输入参数:	
返回参数:	
*************************************************************************************/
void TM36_Capture_Init(void)
{  
		TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
		PIN_InitTypeDef PINX_InitStruct;

		UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_TM36_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_TM36,ENABLE);						// Enable TM36 Clock
		CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);						// Enable PortB Clock
    ProtectModuleReg(CSCprotect);

		//==Set GPIO init
		PINX_InitStruct.PINX_Mode				 = PINX_Mode_Digital_I; 								// Pin select Digital input mode
		PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  				// Enable pull up resistor
		PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
		PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 						// Pin output driver full strength.
		PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;			// Pin input deglitch filter clock divider bypass
		PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 						// Pin input data not inverse
		PINX_InitStruct.PINX_Alternate_Function  = 6;														// Pin AFS = TIM36 IC
		GPIO_PinMode_Config(PINB(7),&PINX_InitStruct); 		  										// Set PB7 to TIM36 IC
	
    TM_DeInit(TM36);
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_MainClockDirection =TM_UpCount;
    TM_TimeBase_InitStruct.TM_Period = 65535; 
    TM_TimeBase_InitStruct.TM_Prescaler = 0;
		TM_TimeBase_InitStruct.TM_MainClockSource = TM_CK_INT;				
    TM_TimeBase_InitStruct.TM_2ndClockSource = TM_CK_INT;					
    TM_TimeBase_InitStruct.TM_CounterMode = Separate;
    
    TM_TimeBase_Init(TM36, &TM_TimeBase_InitStruct);  
    // ----------------------------------------------------
    // 2.config overwrite mode (keep data)
    TM_IC3OverWritten_Cmd(TM36, TM_Keep);    
    // ----------------------------------------------------
    // 3.config TM20 channel 0 function 
    TM_CH3Function_Config(TM36, TM_InputCapture);
    TM_IN3Source_Select(TM36, IC0);      			// TM36_IN3 from IC0 (PB7)               
    // ----------------------------------------------------	
#if (TM36_IC3_TriggerSrc == 1)      // Rising to Rising edge 
//    TM_TRGICounter_Select(TM36,TRGI_StartupRising);      //TM36不可加这行
//    TM_TRGIPrescaler_Select(TM36,TRGI_StartupRising);      //TM36不可加这行
    TM_IN3TriggerEvent_Select(TM36, IC_RisingEdge);
		TM_Counter_Cmd(TM36, ENABLE);
	  TM_Prescaler_Cmd(TM36, ENABLE);
 
#elif (TM20_IC0_TriggerSrc == 2)    // Rising to Falling edge 
    TM_TRGICounter_Select(TM20,TRGI_StartupRising);
    TM_TRGIPrescaler_Select(TM20,TRGI_StartupRising);
    TM_IN0TriggerEvent_Select(TM20, IC_FallingEdge);
    
#elif (TM20_IC0_TriggerSrc == 3)    // Falling to Falling edge 
    TM_TRGICounter_Select(TM20,TRGI_StartupFalling);
    TM_TRGIPrescaler_Select(TM20,TRGI_StartupFalling);
    TM_IN0TriggerEvent_Select(TM20, IC_FallingEdge);
    
#elif (TM20_IC0_TriggerSrc == 4)    // Falling to Rising edge
    TM_TRGICounter_Select(TM20,TRGI_StartupFalling);
    TM_TRGIPrescaler_Select(TM20,TRGI_StartupFalling);
    TM_IN0TriggerEvent_Select(TM20, IC_RisingEdge);
		
#elif (TM20_IC0_TriggerSrc == 5)    // Rising to Dual edge
    TM_TRGICounter_Select(TM20,TRGI_StartupRising);
    TM_TRGIPrescaler_Select(TM20,TRGI_StartupRising);
    TM_IN0TriggerEvent_Select(TM20, IC_DualEdge);
				
#endif   
    
    // ----------------------------------------------------
    // 5.clear all flag
    TM_ClearFlag(TM36, TMx_CF3A | TMx_CF3B);
    // ----------------------------------------------------
    // 6.enable Timer 
    TM_Timer_Cmd(TM36,ENABLE);
    
}

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{

  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO
	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
	
	/* Configure peripheral clock */
	
  ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	void DMA_Init (void)
功能描述:	DMA初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void DMA_Init(void)
{
	DMA_BaseInitTypeDef DMATestPattern;

	UnProtectModuleReg(CSCprotect);
	CSC_PeriphOnModeClock_Config(CSC_ON_DMA,ENABLE);						// Enable DMA module clock
    ProtectModuleReg(CSCprotect);

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = DISABLE;
        DMATestPattern.DestDINCSel = ENABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_TM36_IC3;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_MEM_Write;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_2Byte;           //输入捕获是16bit
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = 20;
    
        // source/destination config
        DMATestPattern.DMADestinationAddr = RcvBuf;
				
				DMA_Channel_Cmd(DMAChannel0, ENABLE);
				DMA_Base_Init(&DMATestPattern);
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	uint32_t j=0;
	CSC_Init();
	Sample_URT0_Init();
	TM36_Capture_Init();
	DMA_Init();
	
	printf("hello!\n");
	
	TM_DMAChannel_Cmd(TM36, TMx_DMA_IC3, ENABLE);
	DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
	
	DMA_StartRequest(DMAChannel0);
	while (DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_TCF) == DRV_UnHappened);
	DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
		
	for(j=0;j<10;j++)
	{
		printf("%d ",RcvBuf[j]);
	}
	TM_Timer_Cmd(TM36,DISABLE);
	return 0;	
			
}
