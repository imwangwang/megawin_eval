

/**
 ******************************************************************************
 *
 * @file        MG32x02z__IRQHandler.c
 * @brief       The demo IRQHandler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */


#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_EXIC_DRV.H"
#include "MG32x02z__IRQHandler.H"
#include "MG32x02z_IRQ_Init.h"

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if NMI_IRQHandler_En == 1
void NMI_Handler(void)
{    
    //to do......
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
// Hard Fault handler in C, with stack frame location and LR value 
// extracted from the assembly wrapper as input parameters 
#if HardFault_IRQHandler_En == 1
void hard_fault_handler_c(unsigned int * hardfault_args, unsigned lr_value) 
{
    static volatile unsigned int stacked_r0; 
    static volatile unsigned int stacked_r1; 
    static volatile unsigned int stacked_r2; 
    static volatile unsigned int stacked_r3; 
    static volatile unsigned int stacked_r12; 
    static volatile unsigned int stacked_lr; 
    static volatile unsigned int stacked_pc; 
    static volatile unsigned int stacked_psr; 
    stacked_r0 = ((unsigned long) hardfault_args[0]); 
    stacked_r1 = ((unsigned long) hardfault_args[1]); 
    stacked_r2 = ((unsigned long) hardfault_args[2]); 
    stacked_r3 = ((unsigned long) hardfault_args[3]); 
    stacked_r12 = ((unsigned long) hardfault_args[4]); 
    stacked_lr = ((unsigned long) hardfault_args[5]); 
    stacked_pc = ((unsigned long) hardfault_args[6]); 
    stacked_psr = ((unsigned long) hardfault_args[7]); 

    printf ("Hard fault handler\n\r"); 
    printf ("  R0 = 0x%X\n\r", stacked_r0); 
    printf ("  R1 = 0x%X\n\r", stacked_r1); 
    printf ("  R2 = 0x%X\n\r", stacked_r2); 
    printf ("  R3 = 0x%X\n\r", stacked_r3); 
    printf ("  R12 = 0x%X\n\r", stacked_r12); 
    printf ("  Stacked LR = 0x%X\n\r", stacked_lr); 
    printf ("  Stacked PC = 0x%X\n\r", stacked_pc); 
    printf ("  Stacked PSR = 0x%X\n\r", stacked_psr); 
    printf ("  Current LR = 0x%X\n\r", lr_value); 

    while(1); // endless loop 
}


// Hard Fault handler wrapper in assembly 
// It extracts the location of stack frame and passes it to handler 
// in C as a pointer.We also extract the LR value as second 
// parameter. 

__asm void HardFault_Handler(void) 
{
    MOVS    r0, #4 
    MOV     r1, LR 
    TST     r0, r1 
    BEQ     stacking_used_MSP 
    MRS     R0, PSP                                 ; first parameter - stacking was using PSP 
    B       get_LR_and_branch 
stacking_used_MSP 
    MRS     R0, MSP                                 ; first parameter - stacking was using MSP 
get_LR_and_branch 
    MOV     R1, LR                                  ; second parameter is LR current value 
    LDR     R2,=__cpp(hard_fault_handler_c) 
    BX      R2 
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if SVC_IRQHandler_En == 1
void SVC_Handler(void)
{
    //to do......    
}
#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if PendSV_IRQHandler_En == 1
void PendSV_Handler(void)
{
    //to do......    
}
#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if SysTick_IRQHandler_En==1
void SysTick_Handler(void)
{
    //to do......
	IncTick();
}

#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if WWDT_IRQHandler_En == 1
void WWDT_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(WWDT_IRQn);
    if(IRQ_ID & EXIC_SRC0_ID0_wwdt_b0)
    {
        WWDT_IRQ();
    }
}
#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if SYS_IRQHandler_En == 1
void SYS_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(SYS_IRQn);

    if(IRQ_ID & EXIC_SRC0_ID1_iwdt_b1)
    {
        IWDT_IRQ();   
    }
    if(IRQ_ID & EXIC_SRC0_ID1_pw_b1)
    {
        PW_IRQ();
    }
    if(IRQ_ID & EXIC_SRC0_ID1_rtc_b1)
    {
        RTC_IRQ();
    }
    if(IRQ_ID & EXIC_SRC0_ID1_csc_b1)
    {
        CSC_IRQ();
    }
    if(IRQ_ID & EXIC_SRC0_ID1_apb_b1)
    {
        APB_IRQ();
    }
    if(IRQ_ID & EXIC_SRC0_ID1_mem_b1)
    {
        MEM_IRQ();
    }
}

#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if EXINT0_IRQHandler_En == 1
void EXINT0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(EXINT0_IRQn);    
    if( IRQ_ID & EXIC_SRC0_ID3_exint0_b3)
    {
        EXINT0_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if EXINT1_IRQHandler_En == 1
void EXINT1_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(EXINT1_IRQn);      
    if( IRQ_ID & EXIC_SRC1_ID4_exint1_b0)
    {
        EXINT1_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if EXINT2_IRQHandler_En == 1
void EXINT2_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(EXINT2_IRQn);      
    if( IRQ_ID & EXIC_SRC1_ID5_exint2_b1)
    {
        EXINT2_IRQ();
    }
}
#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if EXINT3_IRQHandler_En == 1
void EXINT3_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(EXINT3_IRQn);      
    if( IRQ_ID & EXIC_SRC1_ID6_exint3_b2)
    {
        EXINT3_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if COMP_IRQHandler_En == 1
void COMP_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(COMP_IRQn);      
    if( IRQ_ID & EXIC_SRC1_ID7_cmp_b3)
    {
        CMP_IRQ();
    }
}
#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if DMA_IRQHandler_En == 1
void DMA_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(DMA_IRQn);      
    if( IRQ_ID & EXIC_SRC2_ID8_dma_b0)
    {
        DMA_IRQ();
    }    
    
}
#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if ADC_IRQHandler_En == 1
void ADC_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    __nop();
    IRQ_ID = EXIC_GetITSourceID(ADC_IRQn);      
    if( IRQ_ID & EXIC_SRC2_ID10_adc_b2)
    {
        ADC_IRQ();
    }
}
#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if DAC_IRQHandler_En == 1
void DAC_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(DAC_IRQn);      
    if( IRQ_ID & EXIC_SRC2_ID11_dac_b3)
    {
        DAC_IRQ();
    }     
}
#endif
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if TM0x_IRQHandler_En == 1
void TM0x_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM0x_IRQn);     
    if( IRQ_ID & EXIC_SRC3_ID12_tm00_b0 )
    {
        TM00_IRQ();
    }
    if(IRQ_ID & EXIC_SRC3_ID12_tm01_b0)
    {
        TM01_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if TM10_IRQHandler_En == 1
void TM10_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM10_IRQn);      
    if( IRQ_ID & EXIC_SRC3_ID13_tm10_b1)
    {
        TM10_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if TM1x_IRQHandler_En == 1
void TM1x_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM1x_IRQn);     
    if(IRQ_ID & EXIC_SRC3_ID14_tm16_b2)
    {
        TM16_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if TM20_IRQHandler_En == 1
void TM20_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM20_IRQn);      
    if(IRQ_ID & EXIC_SRC3_ID15_tm20_b3)
    {
        TM20_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if TM2x_IRQHandler_En == 1
void TM2x_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM2x_IRQn);      
    if(IRQ_ID & EXIC_SRC4_ID16_tm26_b0)
    {
        TM26_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if TM3x_IRQHandler_En == 1
void TM3x_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(TM3x_IRQn);      
    if(IRQ_ID & EXIC_SRC4_ID17_tm36_b1)
    {
        TM36_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if URT0_IRQHandler_En == 1
void URT0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(URT0_IRQn);      
    if(IRQ_ID & EXIC_SRC5_ID20_urt0_b0)
    {
        URT0_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if URT123_IRQHandler_En == 1
void URT123_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(URT123_IRQn);      
    if(IRQ_ID & EXIC_SRC5_ID21_urt1_b1)
    {
        URT1_IRQ();
    }
    if( IRQ_ID & EXIC_SRC5_ID21_urt2_b1)
    {
        URT2_IRQ();
    }
    if( IRQ_ID & EXIC_SRC5_ID21_urt3_b1)
    {
        URT3_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if SPI0_IRQHandler_En == 1
void SPI0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(SPI0_IRQn);      
    if( IRQ_ID & EXIC_SRC6_ID24_spi0_b0)
    {
        SPI0_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if I2C0_IRQHandler_En == 1
void I2C0_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(I2C0_IRQn);      
    if(IRQ_ID & EXIC_SRC7_ID28_i2c0_b0)
    {
        I2C0_IRQ();
    }
}
#endif

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#if I2Cx_IRQHandler_En == 1
void I2Cx_IRQHandler(void)
{
    uint8_t IRQ_ID;
    
    
    IRQ_ID = EXIC_GetITSourceID(I2Cx_IRQn);      
    if( IRQ_ID & EXIC_SRC7_ID29_i2c1_b1)
    {
        I2C1_IRQ();
    }
}
#endif


















