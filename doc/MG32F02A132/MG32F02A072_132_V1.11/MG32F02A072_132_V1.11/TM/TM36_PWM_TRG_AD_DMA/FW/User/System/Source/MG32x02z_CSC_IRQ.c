/**
 ******************************************************************************
 *
 * @file        MG32x02z_CSC_IRQ.c
 *
 * @brief       The demo code CSC Interrupt Request C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "MG32x02z_CSC_DRV.h"


/**
 *******************************************************************************
 * @brief  	    CSC module IRQ
 * @details  
 * @return	    
 * @note
 *******************************************************************************
 */
__weak  void CSC_IRQ (void)
{
    while(CSC_GetAllFlagStatus() != 0)
    {
        if((CSC_GetSingleFlagStatus(CSC_MCDF) & CSC_ALLF) == DRV_Happened)   
        {
            // XOSC missing clock detect failure happened.
            // To do...
            CSC_ClearFlag (CSC_MCDF);
        }

        if(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Happened)
        {
            // PLL clock is stable.
            // To do...
            CSC_ClearFlag (CSC_PLLF);
        }
        
        if(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Happened)
        {
            // IHRCO clock is stable.
            // To do...
            CSC_ClearFlag (CSC_IHRCOF);
        }
        
        if(CSC_GetSingleFlagStatus(CSC_ILRCOF) == DRV_Happened)
        {
            // ILRCO clock is stable.
            // To do...
            CSC_ClearFlag (CSC_ILRCOF);
        }
        
        if(CSC_GetSingleFlagStatus(CSC_XOSCF) == DRV_Happened)
        {
            // XOSC clock is stable.
            // To do...
            CSC_ClearFlag (CSC_XOSCF);
        }
    }
}



