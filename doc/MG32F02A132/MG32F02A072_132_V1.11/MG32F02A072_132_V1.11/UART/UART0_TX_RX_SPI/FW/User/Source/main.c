/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用UART作为SPI主机与SPI设备
 *				进行数据传输。
 *				此demo会通过UART的SPI主机接口
 *				发出0x30数据，并接收数据，接收
 *				到从机发回的0x50数据后PE15(LED)会闪烁
 *				注意：
 *				UART仅支持SPI主机，不支持从机。
 *
 *
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */

#include "MG32x02z_DRV.H"
#include <stdio.h>

#define URTX URT0

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
    
	
  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_11M0592Hz);			// IHRCO Sel 11.0592MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	

	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);
	
  ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	void URT0_SPIMode_Init (void)
功能描述:	UART0 SPI主机初始化 
		  MOSI(PB8),MISO(PB9),SPI_NSS(PB10),SPI_CLK(PC3)
		  
输入参数:	
返回参数:	  
*************************************************************************************/
void URT0_SPIMode_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    PIN_InitTypeDef PINX_InitStruct;

		UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);
		CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);	
		CSC_PeriphOnModeClock_Config(CSC_ON_PortC,ENABLE);
		ProtectModuleReg(CSCprotect);

		PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
		PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
		PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
		PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
		PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
		PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_TX
		GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD(MOSI) at PB8
		
		GPIO_PinMode_Config(PINB(10),&PINX_InitStruct); 					 		// URT0_NSS(SPI_NSS) at PB10
		
		GPIO_PinMode_Config(PINC(3),&PINX_InitStruct); 					 		// URT0_CLK(SPI_CLK) at PC3

		PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_RX
		GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD(MISO) at PB9  

    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 3;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 5;	                //Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    //BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 3);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 3);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTX, ENABLE);	                            //Enable TX
    URT_RX_Cmd(URTX, ENABLE);	                            //Enable RX
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_MSB;      //UART SPI Data Mode
    DataDef.URT_RX_DataOrder   = URT_DataTyped_MSB;      //UART SPI Data Mode
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_SYNC_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //==Set SPI Mode(CPHA = 0 , CPOL = 0)====//
    URT_CPHAMode_Select( URTX , URT_CPHA0_LeadEdge);
    URT_CPOLMode_Select( URTX , URT_CPOL0_Low );

    //=====Set Data Control=====//
    URT_TXGuardTime_Select(URTX, 0);
    
    
    //=====Enable CLK(SYSCLK)====//
    URT_CLKSignal_Cmd(URTX,ENABLE);
    
    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);

}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	int i;
	ctype            URT_RecData;
	PIN_InitTypeDef  PINX_InitStruct;
	CSC_Init();
	PINX_InitStruct.PINX_Mode 			   = PINX_Mode_PushPull_O;	   // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		   = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed			   = PINX_Speed_Low;		   
	PINX_InitStruct.PINX_OUTDrive 		   = PINX_OUTDrive_Level0;	   // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider	   = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			   = PINX_Inverse_Disable;	   // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						   // Pin AFS = 0
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 				   // 设置LED指示灯IO

	URT0_SPIMode_Init();
	
	while(1)
	{
	  i++;
    if(i>=500000)
    {
				i=0;
        URT_SetTXData(URTX,1,0x30);										//发送0x30指令到SPI从机设备
        while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
        URT_ClearITFlag(URTX,URT_IT_TC);
        URT_RecData.W = URT_GetRXData(URTX);      //接收数据字节量与发送的数据字节量相同
		    if(URT_RecData.W==0x50)
		    {
		       PE15=~PE15;   //若接收到的从机数据为0x50则PE15（LED）闪烁
		    } 
	   }
	}

}
