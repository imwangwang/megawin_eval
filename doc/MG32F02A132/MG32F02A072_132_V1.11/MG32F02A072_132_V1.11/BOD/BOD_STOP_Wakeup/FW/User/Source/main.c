/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				利用BOD1唤醒处于深度睡眠（STOP）状态的MCU
 *				此demo会在PE15（LED）闪烁5次后进入STOP模式，
 *				此时若供电电压向下低于3.7V时BOD1会唤醒MCU，
 *				并让MCU继续从睡眠处继续执行代码。
 *				可在BOD1_Init()的PW_BOD1Trigger_Select（）
 *				选择上升沿或下降沿触发，若要选择检测电压值，
 *				则在PW_BOD1Threshold_Select设置相应的电压值即可。
 *				
 *				
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"
#include "MG32x02z_ADC_DRV.h"
#include "MG32x02z_PW_DRV.h"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
    
	
  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

 	UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/
	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
		
	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);

  ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	void BOD1_Init()
功能描述:	BOD1初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void BOD1_Init()
{
	UnProtectModuleReg(PWprotect);
	NVIC_EnableIRQ(SYS_IRQn);                                    //使用sys_irqhandler需要使能sys_irq
	PW_StopModeLDO_Select(PW_LowPower_LDO);
	PW_OnModeLDO_Select(PW_Normal_LDO);
	PW_BOD1Threshold_Select (PW_BOD1_3V7);     					 //BOD1检测电压
	PW_BOD1Trigger_Select(PW_BODx_FallingEdge); 				 //触发沿为下降沿
	PW_BOD1_Cmd(ENABLE);
	PW_IntVoltageRef(ENABLE);							
	PW_PeriphStopModeContinuous_Config(PW_STPPO_BOD1, ENABLE);
	PW_PeriphStopModeWakeUp_Config(PW_WKSTP_BOD1, ENABLE);
	PW_IT_Config((PW_INT_WK | PW_INT_BOD1), ENABLE);
	PW_ClearFlag(PW_BOD1F);
	PW_ITEA_Cmd(ENABLE);
	ProtectModuleReg(PWprotect);
}

/***********************************************************************************
函数名称:	void SYS_IRQHandler(void)
功能描述:	SYS中断处理程序
输入参数:	
返回参数:	  
*************************************************************************************/
void SYS_IRQHandler(void)
{
  if(PW_GetSingleFlagStatus(PW_BOD1F) == DRV_Happened)
  {
		PE13=~PE13;
		PW_ClearFlag(PW_BOD1F);
  }
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	int i=0,j=0;
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();
	BOD1_Init();
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 // 设置LED指示灯IO
	GPIO_PinMode_Config(PINE(14),&PINX_InitStruct); 					 // 设置LED指示灯IO
  GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 // 设置LED指示灯IO
	while(1)
	{
		i++;
		if(i>=500000)
		{
			i=0;
			PE15=~PE15;
			j++;
			if(j>10)
			{
			  PE15=1;				
//				SCB->SCR  = (SCB_SCR_SLEEPDEEP_Msk);  //执行WFI指令前执行该指令可让MCU进入STOP模式
				__WFI();             //该指令为cmsis_arm.cc的指令， 执行该指令时MCU进入睡眠。
				j=0;
			}	 
		}
	}
}
