/**
 ******************************************************************************
 *
 * @file        MG32x02z_PW_IRQ.c
 *
 * @brief       The demo code PW Interrupt Request C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */
 
#include "MG32x02z_PW_DRV.h"
#include "MG32x02z_DRV.H"

/**
 *******************************************************************************
 * @brief  	    PW module IRQ
 * @details  
 * @return	    
 * @note
 *******************************************************************************
 */
__weak void PW_IRQ (void)
{
    while((PW_GetAllFlagStatus() & (PW_BOD0F | PW_BOD1F | PW_WKF)) != 0)
    {
        if(PW_GetSingleFlagStatus(PW_BOD0F) == DRV_Happened)
        {
            /* 
                When VDD voltage is less than BOD0 level. MCU cannot clear 
                BOD0F flag, until VDD voltage is greater than BOD0 level.
            */
            // When power on reset happened.
            // To do...
            PW_ClearFlag(PW_BOD0F);
        }
        
        if(PW_GetSingleFlagStatus(PW_BOD1F) == DRV_Happened)
        {
            // When Brown-Out detect BOD1 happened.
            // To do...
					PE15=0;
            PW_ClearFlag(PW_BOD1F);
        }
        
        if(PW_GetSingleFlagStatus(PW_WKF) == DRV_Happened)
        {
            // When system received wakeup event happened.
            // To do...
            PW_ClearFlag(PW_WKF);
        }
    }
}



