/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				利用EXIC唤醒处于深度睡眠（STOP）状态的MCU
 *				此demo会在PE15（LED）闪烁5次后进入STOP模式，
 *				使用（PA2或PA3）、（PA0和PA1）用低电平唤醒MCU，并让
 *				MCU继续从睡眠处继续执行代码。
 *				可在EXIC_Init()的EXIC_TRGS.EXIC_TRGS_Mode中
 *				选择边沿触发或电平触发，若要选择高电平或上升
 *				沿触发，则在PINX_InitStruct.PINX_Inverse设置
 *				反相即可。
 *				注意：STOP模式下强制使用Level模式触发
 *				
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */

#include "MG32x02z_DRV.H"
#include "MG32x02z_ADC_DRV.h"
#include "MG32x02z_PW_DRV.h"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
    
  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1
	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
		
	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);

 	ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	void EXIC_Init(void)
功能描述:	EXIC初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void EXIC_Init(void)
{
    PIN_InitTypeDef  PINX_InitStruct;
    EXIC_TRGSTypeDef EXIC_TRGS;

    UnProtectModuleReg(CSCprotect);
    CSC_PeriphOnModeClock_Config(CSC_ON_PortA,ENABLE);
    ProtectModuleReg(CSCprotect);

    //====GPIO Initial==== 
    PINX_InitStruct.PINX_Pin                = PX_Pin_All;                     // Select Pin of the port to initial (PX_Pin_All is all pin of the port.)
    PINX_InitStruct.PINX_Mode								= PINX_Mode_OpenDrain_O;          // Select GPIO mode 
                                                                              //     1.QB: Quasi-Bidirection mode only for PC ) 
    PINX_InitStruct.PINX_PUResistant        = PINX_PUResistant_Enable;        // Select wether enable internal pull-up R or not.
    PINX_InitStruct.PINX_Speed              = PINX_Speed_Low;                 // Select wehter enable high speed mode
                                                                              //     1.(PINX_Speed_High mode only for PC0~3 , PD0~3 )    
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level0;           // Select output drive strength 
                                                                              //     1.(Level 0 & level 3 only for PE0~PE3)
    PINX_InitStruct.PINX_FilterDivider      = PINX_FilterDivider_Bypass;      // Select input filter divider.
    PINX_InitStruct.PINX_Inverse            = PINX_Inverse_Disable;           // Select input signal whether inverse or not.
                                                                              //     1.PINX_Inverse_Disable = L level or falling edge.
                                                                              //     2.PINX_Inverse_Enable  = H level or Rising edge.
    PINX_InitStruct.PINX_Alternate_Function = 0;                              // Select GPIO mode
    GPIO_PortMode_Config(IOMA,&PINX_InitStruct);                              // (Pin0 & Pin1) of PorA configuration
		GPIO_WritePort(GPIOA,0xFFFF); 																						// Initial PortA
    
    //====EXIC Initial====                                    
    EXIC_TRGS.EXIC_Pin = EXIC_TRGS_PIN0 | EXIC_TRGS_PIN1 | EXIC_TRGS_PIN2 | EXIC_TRGS_PIN3;   //     1. You can selec any pin of the port.
    EXIC_TRGS.EXIC_TRGS_Mode = Level;                                          // External interrupt pin edge/level trigger event select.
    EXIC_PxTriggerMode_Select(EXIC_PA,&EXIC_TRGS);                            // EXIC trigger mode configuration.
    
    EXIC_PxTriggerAndMask_Select(EXIC_PA , 0x0003);                           // EXIC_PA  AF trigger event select.
                                                                              //     1. Select PA_AF trigger pin(PA0, PA1).
                                                                              //     2. PA_AF is set if trigger event of select pin have to happen at same time(PA0 & PA1).  
    EXIC_PxTriggerOrMask_Select(EXIC_PA  , 0x000C);                           // EXIC_PA  OF trigger event select.
                                                                              //     1. Select PA_OF trigger pin(PA2, PA3).
                                                                              //     2. PA_OF is set if anyone trigger event of select pin happen(PA2 | PA3).   
    EXIC_ClearPxTriggerEventFlag(EXIC_PA, EXIC_PX_AllPIN );                   // Clear All pin of the port event flag.    
    
    EXIC_PxTriggerITEA_Cmd(EXIC_PA_IT , ENABLE);                              // Enable EXIC interrupt
    NVIC_EnableIRQ(EXINT0_IRQn);                                              
    
    //===Interrupt handle====
    //Please refer to MG32x02z_EXIC_IRQ.c

}

/***********************************************************************************
函数名称:	void EXINT0_IRQHandler(void)
功能描述:	EXIC中断处理程序
输入参数:	
返回参数:	  
*************************************************************************************/
void EXINT0_IRQHandler(void)
{

	 PE14=~PE14;
	 EXIC_ClearPxTriggerEventFlag(EXIC_PA, EXIC_PX_AllPIN );
   EXIC_ClearPxTriggerITFlag(EXIC_PA_ITF,EXIC_PX_OF | EXIC_PX_AF);
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	int i=0,j=0;
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();
	EXIC_Init();
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
	GPIO_PinMode_Config(PINE(14),&PINX_InitStruct); 					 // 设置LED指示灯IO
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 // 设置LED指示灯IO

	while(1)
	{
		i++;
		if(i>=500000){
			i=0;
			PE15=~PE15;
			j++;
			if(j>10)
			{
			  PE15=1;				
				SCB->SCR  = (SCB_SCR_SLEEPDEEP_Msk);  //执行WFI指令前执行该指令可让MCU进入STOP模式
				__WFI();             //该指令为cmsis_arm.cc的指令， 执行该指令时MCU进入睡眠。
				j=0;
			}	 
		}
	}
}

