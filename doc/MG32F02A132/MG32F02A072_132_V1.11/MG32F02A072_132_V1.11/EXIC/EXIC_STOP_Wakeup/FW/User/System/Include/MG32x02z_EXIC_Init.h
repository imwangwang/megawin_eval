

/**
 ******************************************************************************
 *
 * @file        MG32x02z_EXIC_Init.H
 *
 * @brief       This file is to opition EXIC setting.
 *   
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 */


#ifndef _MG32x02z_EXIC_Init_H
#define _MG32x02z_EXIC_Init_H


// <<< Use Configuration Wizard in Context Menu >>>

//<e0> Enable EXIC PA initial.
    //<h> AND (KBI)
        //<o1.16..31>AND Mask  (0x0000 ~ 0xFFFF) <0x0000-0xFFFF>
        //<q5> AND unmatch enable
    //</h>
    //<h> OR (Interrupt)
        //<o1.0..15> OR  Mask  (0x0000 ~ 0xFFFF) <0x0000-0xFFFF>
    //</h>
    //<h> Trigger mode
        //<e2.1>  PA0
            //<o3.0..1> PIN0 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.3>  PA1
            //<o3.2..3> PIN1 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.5>  PA2
            //<o3.4..5> PIN2 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.7>  PA3
            //<o3.6..7> PIN3 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.9>  PA4
            //<o3.8..9> PIN4 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.11> PA5
            //<o3.10..11> PIN5 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.13> PA6
            //<o3.12..13> PIN6 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.15> PA7
            //<o3.14..15> PIN7 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.17> PA8
            //<o3.16..17> PIN8 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.19> PA9
            //<o3.18..19> PIN9 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.21> PA10
            //<o3.20..21> PIN10 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.23> PA11
            //<o3.22..23> PIN11 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.25> PA12
            //<o3.24..25> PIN12 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.27> PA13
            //<o3.26..27> PIN13 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.29> PA14
            //<o3.28..29> PIN14 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.31> PA15
            //<o3.30..31> PIN15 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
    //</h>
    //<q4> Interrupt enable
//</e>
#define MG32x02z_ExicPAInit_EN           1
#define MG32x02z_ExicPAInit_AndOrMask    0x00000010
#define MG32x02z_ExicPAInit_Mask         0x000002AA  
#define MG32x02z_ExicPAInit_Mode         0x6DB6DAAA
#define MG32x02z_ExicPAInit_INT          1
#define MG32x02z_ExicPAInit_AINV         0

//<e0> Enable EXIC PB initial.
    //<h> AND (KBI)
        //<o1.16..31>AND Mask  (0x0000 ~ 0xFFFF) <0x0000-0xFFFF>
        //<q5> AND unmatch enable
    //</h>
    //<h> OR (Interrupt)
        //<o1.0..15> OR  Mask  (0x0000 ~ 0xFFFF) <0x0000-0xFFFF>
    //</h>
    //<h> Trigger mode
        //<e2.1>  PB0
            //<o3.0..1> PIN0 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.3>  PB1
            //<o3.2..3> PIN1 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.5>  PB2
            //<o3.4..5> PIN2 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.7>  PB3
            //<o3.6..7> PIN3 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.9>  PB4
            //<o3.8..9> PIN4 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.11> PB5
            //<o3.10..11> PIN5 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.13> PB6
            //<o3.12..13> PIN6 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.15> PB7
            //<o3.14..15> PIN7 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.17> PB8
            //<o3.16..17> PIN8 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.19> PB9
            //<o3.18..19> PIN9 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.21> PB10
            //<o3.20..21> PIN10 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.23> PB11
            //<o3.22..23> PIN11 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.25> PB12
            //<o3.24..25> PIN12 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.27> PB13
            //<o3.26..27> PIN13 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.29> PB14
            //<o3.28..29> PIN14 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.31> PB15
            //<o3.30..31> PIN15 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>        
    //</h>
    //<q4> Interrupt enable
//</e>
#define MG32x02z_ExicPBInit_EN           0
#define MG32x02z_ExicPBInit_AndOrMask    0x00000000
#define MG32x02z_ExicPBInit_Mask         0x00000000  
#define MG32x02z_ExicPBInit_Mode         0x6DB6DB6D
#define MG32x02z_ExicPBInit_INT          0
#define MG32x02z_ExicPBInit_AINV         0

//<e0> Enable EXIC PC initial.
    //<h> AND (KBI)
        //<o1.16..31>AND Mask  (0x0000 ~ 0xFFFF) <0x0000-0xFFFF>
        //<q5> AND unmatch enable
    //</h>
    //<h> OR (Interrupt)
        //<o1.0..15> OR  Mask  (0x0000 ~ 0xFFFF) <0x0000-0xFFFF>
    //</h>
    //<h> Trigger mode
        //<e2.1>  PC0
            //<o3.0..1> PIN0 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.3>  PC1
            //<o3.2..3> PIN1 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.5>  PC2
            //<o3.4..5> PIN2 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.7>  PC3
            //<o3.6..7> PIN3 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.9>  PC4
            //<o3.8..9> PIN4 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.11> PC5
            //<o3.10..11> PIN5 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.13> PC6
            //<o3.12..13> PIN6 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.15> PC7
            //<o3.14..15> PIN7 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.17> PC8
            //<o3.16..17> PIN8 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.19> PC9
            //<o3.18..19> PIN9 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.21> PC10
            //<o3.20..21> PIN10 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.23> PC11
            //<o3.22..23> PIN11 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.25> PC12
            //<o3.24..25> PIN12 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.27> PC13
            //<o3.26..27> PIN13 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.29> PC14
            //<o3.28..29> PIN14 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>     
    //</h>
    //<q4> Interrupt enable
//</e>
#define MG32x02z_ExicPCInit_EN           0
#define MG32x02z_ExicPCInit_AndOrMask    0x00000000
#define MG32x02z_ExicPCInit_Mask         0x00000000  
#define MG32x02z_ExicPCInit_Mode         0x5DB6DB6D    
#define MG32x02z_ExicPCInit_INT          0
#define MG32x02z_ExicPCInit_AINV         0

//<e0> Enable EXIC PD initial.
    //<h> AND (KBI)
        //<o1.16..31>AND Mask  (0x0000 ~ 0xFFFF) <0x0000-0xFFFF>
        //<q5> AND unmatch enable
    //</h>
    //<h> OR (Interrupt)
        //<o1.0..15> OR  Mask  (0x0000 ~ 0xFFFF) <0x0000-0xFFFF>
    //</h>
    //<h> Trigger mode
        //<e2.1>  PD0
            //<o3.0..1> PIN0 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.3>  PD1
            //<o3.2..3> PIN1 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.5>  PD2
            //<o3.4..5> PIN2 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.7>  PD3
            //<o3.6..7> PIN3 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.9>  PD4
            //<o3.8..9> PIN4 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.11> PD5
            //<o3.10..11> PIN5 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.13> PD6
            //<o3.12..13> PIN6 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.15> PD7
            //<o3.14..15> PIN7 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.17> PD8
            //<o3.16..17> PIN8 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.19> PD9
            //<o3.18..19> PIN9 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.21> PD10
            //<o3.20..21> PIN10 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.23> PD11
            //<o3.22..23> PIN11 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.25> PD12
            //<o3.24..25> PIN12 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.27> PD13
            //<o3.26..27> PIN13 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.29> PD14
            //<o3.28..29> PIN14 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>
        //<e2.31> PD15
            //<o3.30..31> PIN15 trigger event is <1=>Level <2=>Edge <3=> Dual-edge
        //</e>   
    //</h>
    //<q4> Interrupt enable
//</e>
#define MG32x02z_ExicPDInit_EN           0
#define MG32x02z_ExicPDInit_AndOrMask    0x00000000
#define MG32x02z_ExicPDInit_Mask         0x00000000  
#define MG32x02z_ExicPDInit_Mode         0x6DB6DB6D  
#define MG32x02z_ExicPDInit_INT          0    
#define MG32x02z_ExicPDInit_AINV         0




// <<< end of configuration section >>>

void EXIC_Init(void);


#endif



