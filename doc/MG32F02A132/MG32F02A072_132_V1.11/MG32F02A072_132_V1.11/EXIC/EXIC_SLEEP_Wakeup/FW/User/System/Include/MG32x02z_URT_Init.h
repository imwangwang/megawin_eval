


/**
 ******************************************************************************
 *
 * @file        MG32x02z_URT_Init.H
 *
 * @brief       This file is to opition URT baudrate setting.
 *   
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/05/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 */




//*** <<< Use Configuration Wizard in Context Menu >>> ***
//<e0> Enable URT0 Initial
    //<o1> URT0 baudrate is (400-6000000)Hz<400-6000000>
//</e>
    #define URT0_INIT_EN     0
    #define URT0_INIT_BD     9600
//<e0> Enable URT1 Initial
    //<o1> URT1 baudrate is (400-6000000)Hz<400-6000000>
//</e>
    #define URT1_INIT_EN     0
    #define URT1_INIT_BD     9600
//<e0> Enable URT2 Initial
    //<o1> URT1 baudrate is (400-6000000)Hz<400-6000000>
//</e>
    #define URT2_INIT_EN     0
    #define URT2_INIT_BD     9600
//<e0> Enable URT3 Initial
    //<o1> URT1 baudrate is (400-6000000)Hz<400-6000000>
//</e>
    #define URT3_INIT_EN     0
    #define URT3_INIT_BD     9600


//*** <<< end of configuration section >>> ***

#include "MG32x02z_CSC_Init.h"
#include "MG32x02z_CSC.h"


#define URT_Freq_AHB         ON_CK_AHB_FREQ
#define URT_Freq_APB         ON_CK_APB_FREQ



#if (ON_CSC_APB0 & 0x00010000)  == 0 
    #define URT0_Freq         URT_Freq_APB
#else
    #define URT0_Freq         URT_Freq_AHB
#endif

#if (ON_CSC_APB0 & 0x00040000)  == 0
    #define URT1_Freq         URT_Freq_APB
#else
    #define URT1_Freq         URT_Freq_AHB
#endif

#if (ON_CSC_APB0 & 0x00100000)  == 0 
    #define URT2_Freq         URT_Freq_APB
#else
    #define URT2_Freq         URT_Freq_AHB
#endif

#if (ON_CSC_APB0 & 0x00400000)  == 0 
    #define URT3_Freq         URT_Freq_APB
#else
    #define URT3_Freq         URT_Freq_AHB
#endif




/**
 * @name	Function announce
 *   		
 */
///@{ 

DRV_Return URT_Init(void);

///@}



