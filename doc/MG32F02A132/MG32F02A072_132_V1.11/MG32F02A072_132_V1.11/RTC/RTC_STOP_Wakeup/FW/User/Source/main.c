/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				利用RTC唤醒处于深度睡眠（STOP）状态的MCU
 *				此demo会在PE15（LED）闪烁5次后进入STOP模式，
 *				RTC报警中断发生时唤醒MCU，并让MCU继续从睡眠
 *				处继续执行代码。
 *				RTC中断类型为系统中断，因此RTC的中断程序应写入
 *				SYS_IRQHandler(void)中。
 *
 *				注意：MG32F02A072/132不支持在STOP模式下继续使用
 *				XOSC，因此RTC时钟在STOP模式下不应使用XOSC时钟源
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */

#include "MG32x02z_DRV.H"
#include "MG32x02z_ADC_DRV.h"
#include "MG32x02z_PW_DRV.h"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
	
  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1
	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	

	/* Configure ICKO function */
		
	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);
  ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	void PWR_Init ()
功能描述:	电源策略初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void PWR_Init()
{
	UnProtectModuleReg(PWprotect);
	NVIC_EnableIRQ(SYS_IRQn);
	SYS_ITEA_Cmd(ENABLE);                        //use it to enable SYS_IRQHandler
	PW_StopModeLDO_Select(PW_LowPower_LDO);
	PW_OnModeLDO_Select(PW_Normal_LDO);
	PW_PeriphStopModeWakeUp_Config(PW_WKSTP_RTC, ENABLE);
	PW_IT_Config(PW_INT_WK , ENABLE);
	PW_ClearFlag(PW_INT_WK);
	PW_ITEA_Cmd(ENABLE);
	ProtectModuleReg(PWprotect);
}

/***********************************************************************************
函数名称:	SYS_IRQHandler(void)
功能描述:	系统中断
输入参数:	
返回参数:	  
*************************************************************************************/
void SYS_IRQHandler(void)
{
  if(PW_GetSingleFlagStatus(PW_WKF) == DRV_Happened)
  {
		PE13=~PE13;
    PW_ClearFlag(PW_WKF);
  }

}

/***********************************************************************************
函数名称:	SysTick_Handler(void)
功能描述:	系统时钟中断
输入参数:	
返回参数:	  
*************************************************************************************/
void SysTick_Handler(void)
{
    //to do......
	IncTick();
}

/***********************************************************************************
函数名称:	void RTC_Init (void)
功能描述:	RTC时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void RTC_Init (void)
{


    /*=== 1. Enable CSC to RTC clock ===*/
    UnProtectModuleReg(CSCprotect);                                 // Unprotect CSC module
		/* CK_LS setting */
#if defined (MG32F02A132) | (MG32F02A072)
		CSC_CK_LS_Select(LS_CK_ILRCO);   								 //XOSC is not supported in stop mode for 132/072
#else 
		CSC_XOSCGain_Select(Gain_Low);
		CSC_CK_LS_Select(LS_CK_XOSC);   
		CSC_PeriphOnModeClock_Config(CSC_ON_PortC,ENABLE);
		CSC_XOSC_Cmd(ENABLE);    									   //使能XOSC前必须先执行CSC_PeriphOnModeClock_Config(CSC_ON_PortC,ENABLE);
		CSC_MissingClockDetection_Cmd(DISABLE); // Disable MCD function (MCD not support XOSC 32.768KHz)
		CSC_GetSingleFlagStatus(CSC_XOSCF);     // 确认晶振(XOSC)振荡已稳定	
#endif

		CSC_PeriphSleepModeClock_Config(CSC_SLP_RTC, ENABLE);
		CSC_PeriphStopModeClock_Config(CSC_STP_RTC, ENABLE);
		CSC_PeriphOnModeClock_Config(CSC_ON_RTC, ENABLE);               // Enable RTC module clock
    
    ProtectModuleReg(CSCprotect);                                   // protect CSC module
      
    /*=== 2. Configure RTC clock ===*/
    UnProtectModuleReg(RTCprotect);                                 // Unprotect RTC module
    RTC_CLK_Select(RTC_CK_LS);                                      // RTC clock source = CK_LS
    RTC_PreDivider_Select(RTC_PDIV_4096);                           // PDIV output = RTC clock / 4096
    RTC_Divider_Select(RTC_DIV_8);                                  // DIV output = (RTC clock / 4096) / 8
    RTC_OutputSignal_Select(RTC_ALM);                                // RTC output = DIV otuput frequency
    
    /*=== 3. Set RTC timer value ===*/
    RTC_RCR_Mode_Select(RTC_RCR_MOD_ForceReload);                   // RTC switch to reload mode
    RTC_SetReloadReg(0);                                             // Set reload data
    RTC_TriggerStamp_SW();                                          // Trigger reload data update to RTC timer
    while(RTC_GetSingleFlagStatus(RTC_RCRF) == DRV_UnHappened);     // Waiting reload complete
    RTC_ClearFlag(RTC_ALLF);                                        // Clear flag
    
    /*=== 4. Update ALM value ===*/
    if(RTC_GetAlarmState() == DRV_True)                             // When alarm function enable
    {
        RTC_Alarm_Cmd(DISABLE);                                     // Disable alarm function
        while(RTC_GetSingleFlagStatus(RTC_RCRF) == DRV_UnHappened); // Waiting alarm function disable  
        RTC_ClearFlag(RTC_RCRF);                                    // Clear flag RCRF
    }
    RTC_SetAlarmCompareValue(10);                                   // 设定报警比较值，计数到达该值时触发报警
    RTC_Alarm_Cmd(ENABLE);                                          // Enable Alarm function
		
		RTC_StopModeWakeUpEvent_Config(RTC_PC_WPEN, ENABLE);		//使能停止模式下使用RTC唤醒MCU
		
//		RTC_IT_Config(RTC_INT_ALM,ENABLE);
//		RTC_ITEA_Cmd(ENABLE);
		PW_ClearFlag(PW_ALLF);
    RTC_ClearFlag(RTC_ALLF);
    /*=== 5. Enable RTC module ===*/
    RTC_Cmd (ENABLE);                                               // Enable RTC module
    ProtectModuleReg(RTCprotect);                                   // Protect RTC module

}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	int i=0,j=0;
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();
	PWR_Init();
	RTC_Init();
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 // 设置LED指示灯IO
	GPIO_PinMode_Config(PINE(14),&PINX_InitStruct); 					 // 设置LED指示灯IO
  GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 // 设置LED指示灯IO
	PINX_InitStruct.PINX_Alternate_Function = 2;
	GPIO_PinMode_Config(PINC(7),&PINX_InitStruct); 					 //  RTC_Out setup at PC7

	while(1)
	{
		i++;
		if(i>=500000)
		{
			i=0;
			PE15=~PE15;
			j++;
			if(j>10)
			{
			  PE15=1;				
				RTC_ClearFlag(RTC_ALLF);
				SCB->SCR  = (SCB_SCR_SLEEPDEEP_Msk);  //执行WFI指令前执行该指令可让MCU进入STOP模式
				__WFI();             //该指令为cmsis_arm.cc的指令， 执行该指令时MCU进入睡眠。
				j=0;
				UnProtectModuleReg(RTCprotect);
				RTC_SetReloadReg(0); 											// 设置重载值
				RTC_TriggerStamp_SW();											// Trigger reload data update to RTC timer
				while(RTC_GetSingleFlagStatus(RTC_RCRF) == DRV_UnHappened); 	// Waiting reload complete
				RTC_ClearFlag(RTC_ALLF);										// Clear flag
				ProtectModuleReg(RTCprotect);

			}	 

		}
	}
}
