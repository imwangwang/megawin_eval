/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用IIC做IIC主机，并进行发送和接收。
 *				
 *				
 *				
 *				注意：
 *				
 *				
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"
#include "iic.h"
#include <stdio.h>
#include "MG32x02z_I2C_DRV.h"

I2C_Struct* I2Cx;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define URTX URT0
#define MYBINARYIMAGE2_LENGTH 20

I2C_HandleTypeDef gSI2C0_Handle;
I2C_HandleTypeDef gSI2C1_Handle;

uint8_t SendBuf[MYBINARYIMAGE2_LENGTH]={0};
uint8_t const String0[] = {"Wel"};
uint8_t lDataBuffer[80];

/***********************************************************************************
函数名称:	void SysTick_Handler (void)
功能描述:	系统时钟处理程序
输入参数:	
返回参数:	  
*************************************************************************************/
void SysTick_Handler(void)
{
    //to do......
	IncTick();
}

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
    
  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO


	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
	
	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);
	
  ProtectModuleReg(CSCprotect);
    
}


/***********************************************************************************
函数名称:	void I2C0_ByteMode_Master_Init (void)
功能描述:	IIC主机初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void I2C0_ByteMode_Master_Init(void)
{
    I2C_InitTypeDef SI2C0_Init;

    SI2C0_Init.Instance = I2C0;
    SI2C0_Init.CK_I2C_PR_Frequency = 12000000;
    SI2C0_Init.SCL_Clock = 400000;    
    I2C_ByteMode_Init(&SI2C0_Init);

    gSI2C0_Handle.Instance = I2C0;
    printf("I2C_IsDeviceReady\n\r");
	
				
}

/***********************************************************************************
函数名称:	DRV_Return I2C_ByteMode_Init (I2C_InitTypeDef *SI2C)
功能描述:	IIC字节模式初始化
输入参数:	I2C_InitTypeDef *SI2C
返回参数:	DRV_Return 
*************************************************************************************/
DRV_Return I2C_ByteMode_Init(I2C_InitTypeDef *SI2C)
{

    uint16_t lI2C_Pre = 1;
    uint16_t lI2C_DIV = 1;
    uint16_t lI2C_HT_LT = 0;
    uint16_t lI2C_LT;
    uint16_t lI2C_HT;
		PIN_InitTypeDef PINX_InitStruct;
    //===== Check I2C Busy =====//
    if(__I2C_GetFlagStatus(SI2C->Instance, I2C_FLAG_BUSYF))
        return DRV_Busy;

    __I2C_Disable(SI2C->Instance);

    //===== GPIO Initial =====//
      //===== CSC for GPIO Config =====//
    UnProtectModuleReg(CSCprotect);                                             // Unprotect CSC module
    CSC_PeriphProcessClockSource_Config(CSC_I2C0_CKS, CK_APB);
 	CSC_PeriphOnModeClock_Config(CSC_ON_I2C0,ENABLE);
    CSC_PeriphOnModeClock_Config(CSC_ON_PortB, ENABLE);		                    // Enable PortB clock

    ProtectModuleReg(CSCprotect);                                               // protect CSC module

//      //===== GPIO Config =====//
    PINX_InitStruct.PINX_Mode	            = PINX_Mode_OpenDrain_O;            // Pin select pusu pull mode
    PINX_InitStruct.PINX_PUResistant        = PINX_PUResistant_Enable;         // Enable pull up resistor
    PINX_InitStruct.PINX_Speed              = PINX_Speed_High;           
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level0;             // Pin output driver full strength.
    PINX_InitStruct.PINX_FilterDivider      = PINX_FilterDivider_Bypass;        // Pin input deglitch filter clock divider bypass
    PINX_InitStruct.PINX_Inverse            = PINX_Inverse_Disable;             // Pin input data not inverse
    PINX_InitStruct.PINX_Alternate_Function = 2;                                // Pin AFS = 2

    if(SI2C->Instance == I2C0)
    {
        GPIO_PinMode_Config(PINB(10),&PINX_InitStruct);                         // I2C0 SCL setup at PB10
        GPIO_PinMode_Config(PINB(11),&PINX_InitStruct);                         // I2C0 SDA setup at PB11

 //       GPIO_PinMode_Config(PINC(4),&PINX_InitStruct);                          // I2C0 SCL setup at PC4
 //       GPIO_PinMode_Config(PINC(5),&PINX_InitStruct);                          // I2C0 SDA setup at PC5

 //       GPIO_PinMode_Config(PINC(8),&PINX_InitStruct);                          // I2C0 SCL setup at PC8
 //       GPIO_PinMode_Config(PINC(9),&PINX_InitStruct);                          // I2C0 SDA setup at PC9

 //       GPIO_PinMode_Config(PIND(5),&PINX_InitStruct);                          // I2C0 SCL setup at PD5
 //       GPIO_PinMode_Config(PIND(6),&PINX_InitStruct);                          // I2C0 SDA setup at PD6

 //       GPIO_PinMode_Config(PINE(0),&PINX_InitStruct);                          // I2C0 SCL setup at PE0
 //       GPIO_PinMode_Config(PINE(1),&PINX_InitStruct);                          // I2C0 SDA setup at PE1
    }

    if(SI2C->Instance == I2C1)
    {
        GPIO_PinMode_Config(PINC(10),&PINX_InitStruct);                         // I2C1 SCL setup at PC10
        GPIO_PinMode_Config(PINC(11),&PINX_InitStruct);                         // I2C1 SDA setup at PC11

        GPIO_PinMode_Config(PIND(8),&PINX_InitStruct);                          // I2C1 SCL setup at PD8
        GPIO_PinMode_Config(PIND(9),&PINX_InitStruct);                          // I2C1 SDA setup at PD9

        GPIO_PinMode_Config(PINE(2),&PINX_InitStruct);                          // I2C1 SCL setup at PE2
        GPIO_PinMode_Config(PINE(3),&PINX_InitStruct);                          // I2C1 SDA setup at PE3
    }

    //===== I2C Initial =====//
      //=== CSC for I2C Config ===//
    UnProtectModuleReg(CSCprotect);                                             // Unprotect CSC module
    if(SI2C->Instance == I2C0)
			
        CSC_PeriphOnModeClock_Config(CSC_ON_I2C0, ENABLE);                      // Enable I2C0 module clock
    if(SI2C->Instance == I2C1)
        CSC_PeriphOnModeClock_Config(CSC_ON_I2C1, ENABLE);                      // Enable I2C1 module clock
    ProtectModuleReg(CSCprotect);                                               // protect CSC module

      //=== I2C Config ===//
    //===== I2C Output Clock Config =====//
    // CK_I2C_PR
    // SCL Output Clock
    // HT + LT, <= 32 >=9, CK_I2C_PR / SCL Clock / Prescaler / DIV

    do{
        lI2C_HT_LT = SI2C->CK_I2C_PR_Frequency / SI2C->SCL_Clock / lI2C_Pre / lI2C_DIV;
        if((lI2C_HT_LT >= 32) || (lI2C_HT_LT <=9)) 
        {
            lI2C_Pre ++;
            if(lI2C_Pre > 8)
            {
                lI2C_Pre = 1;
                lI2C_DIV ++;
            }
        }
    }while((lI2C_HT_LT >= 32) || (lI2C_HT_LT <=9));

    lI2C_LT = (lI2C_HT_LT >> 1);
    lI2C_HT = lI2C_HT_LT - lI2C_LT;

    __I2C_SetClockSource(SI2C->Instance, I2C_CLK_SRC_PROC);
    __I2C_SetClockPrescaler(SI2C->Instance, lI2C_Pre - 1);
    __I2C_SetClockDivider(SI2C->Instance, lI2C_DIV - 1);
    __I2C_SetSCLHighTime(SI2C->Instance, lI2C_HT - 1);
    __I2C_SetSCLLowTime(SI2C->Instance, lI2C_HT - 1);

    //===== I2C Opration Mode Config =====//
    __I2C_GeneralCallAddress_Disable(SI2C->Instance);
    __I2C_SlaveAddressDetect_Disable(SI2C->Instance, (I2C_SADR_1 | I2C_SADR_2));
    
    //===== I2C Interrupt Config =====//
    if(SI2C->Instance == I2C0)
    {
        NVIC_EnableIRQ(I2C0_IRQn);
        NVIC_SetPriority(I2C0_IRQn, 1);                                         // Suggest SYSTICK Priority = 0
                                                                                //           Other Priority > 0
    }

    if(SI2C->Instance == I2C1)
    {
        NVIC_EnableIRQ(I2Cx_IRQn);
        NVIC_SetPriority(I2Cx_IRQn, 1);                                         // Suggest SYSTICK Priority = 0
                                                                                //           Other Priority > 0
    }

    __I2C_ITEA_Disable(SI2C->Instance);
    __I2C_IT_Disable(SI2C->Instance, (I2C_IT_TMOUT | I2C_IT_EVENT | I2C_IT_ERR | I2C_IT_BUF ));
		


    //===== I2C Timeout Config =====//
    __I2C_TMO_Disable(SI2C->Instance);
    __I2C_SetTimeOutClockSource(SI2C->Instance, I2C_TMO_CKS_DIV64);
    __I2C_SetTimeOutDetectionMode(SI2C->Instance, I2C_TMO_MDS_GENERAL);
    __I2C_SetTimeOutCount(SI2C->Instance, I2C_TMO_MDS_GENERAL);

    //===== I2C Enable =====//
    __I2C_Enable(SI2C->Instance);
	//	I2Cx->CR0.B[0] |= I2C_CR0_BUF_EN_enable_b0;
    return DRV_Success;
}

/***********************************************************************************
函数名称:	void I2C_Delay (uint32_t Delay)
功能描述:	IIC延时
输入参数:	uint32_t Delay
返回参数:	  
*************************************************************************************/
void I2C_Delay(uint32_t Delay)
{

	
}

/***********************************************************************************
函数名称:	DRV_Return I2C_IsDeviceReady(I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint32_t Trials)
功能描述:	IIC目标设备就绪检测
输入参数:	I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint32_t Trials
返回参数:	  DRV_Return 
*************************************************************************************/
DRV_Return I2C_IsDeviceReady(I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint32_t Trials)
{
//-----Check I2C Busy ------------------
    if(__I2C_GetFlagStatus(SI2C->Instance, I2C_FLAG_BUSYF) != CLR)
        return DRV_Busy;

//-----Set_Config-----------------------
    SI2C->State.W = 0;
    SI2C->SlaveAddress = DevAddress & 0xFFFE;
    SI2C->pData = 0x00000000;
    SI2C->Count = 0;
    SI2C->Control.W = I2C_XferNext_STOP;

//-----Start----------------------------
    do{
        SI2C->State.W = 0;
        Set_STA_STO_AA_100(SI2C->Instance);
        do{
            I2C_ByteMode_Handle(SI2C);
        }while(SI2C->State.MBit.Done == 0);

        if((SI2C->State.W & (I2C_State_BusError | \
                                   I2C_State_ArbitrationLost | \
                                   I2C_State_NACK)) == 0)
            return DRV_Success;

        I2C_Delay(1);
        
    }while(-- Trials > 0);

    return DRV_Failure;
}

/***********************************************************************************
函数名称:	void I2C_ByteMode_Handle (I2C_HandleTypeDef *SI2C)
功能描述:	IIC处理
输入参数:	I2C_HandleTypeDef *SI2C
返回参数:	  
*************************************************************************************/
void I2C_ByteMode_Handle(I2C_HandleTypeDef *SI2C)
{
    uint8_t lState;

    if(__I2C_GetStateFlag(SI2C->Instance) == 0)
        return;

    lState = __I2C_GetStatusCode(SI2C->Instance);
    switch(lState){
        case 0x00:  // Bus Error 
            SI2C->State.W |= I2C_State_BusError | \
                                   I2C_State_Done;;
            break;

        case 0x08:  // SLA+W sent and ACK received 
        case 0x10:  // Repeated start condition
            Set_STA_STO_AA_000(SI2C->Instance);
            __I2C_SendSBUF(SI2C->Instance, ((uint8_t)SI2C->SlaveAddress));
            break;

        case 0x18:  // MT SLA+W sent and ACK received
        case 0x28:  // MT DATA sent and ACK received
            if(SI2C->Count == 0)
            {
                Set_STA_STO_AA_010(SI2C->Instance);
                __I2C_ClearStateFlag(SI2C->Instance);
                while(__I2C_GetStatusCode(SI2C->Instance) == lState);
                lState = __I2C_GetStatusCode(SI2C->Instance);
                SI2C->State.W |= I2C_State_Done;
                return;
            }
            __I2C_SendSBUF(SI2C->Instance, ((uint8_t)*SI2C->pData));
            SI2C->pData ++;
            SI2C->Count --;
            break;

        case 0x20:  // MT SLA+W sent NACK received 
        case 0x30:  // MT DATA sent NACK received
        case 0x48:  // MR SLA+R sent NACK received
            Set_STA_STO_AA_010(SI2C->Instance);
            __I2C_ClearStateFlag(SI2C->Instance);
            while(__I2C_GetStatusCode(SI2C->Instance) == lState);
            lState = __I2C_GetStatusCode(SI2C->Instance);
            SI2C->State.W |= I2C_State_NACK | \
                                   I2C_State_Done;
            return;

        case 0x38:  // Arbitration lost
            Set_STA_STO_AA_000(SI2C->Instance);
            SI2C->State.W |= I2C_State_ArbitrationLost | \
                                   I2C_State_Done;
            break;

        case 0x40:  // SLA+R sent and ACK received
            Set_STA_STO_AA_001(SI2C->Instance);
        case 0x50:  // Data Received and ACK sent
            if(SI2C->Count <= 1)
                Set_STA_STO_AA_000(SI2C->Instance);
            *SI2C->pData = __I2C_ReceiveSBUF(SI2C->Instance);
            SI2C->Count--;
            SI2C->pData++;
            break;

        case 0x58:  // Data Received and NACK sent
            if((SI2C->Control.W & I2C_XferNext_Mask) == I2C_XferNext_RepeatStart)
                Set_STA_STO_AA_100(SI2C->Instance);
            else
                Set_STA_STO_AA_010(SI2C->Instance);

            SI2C->State.W |= I2C_State_Done;
            break;

//-------------------------------------
        case 0x60:  // Own SLA+W has bee Received ACK has been returned

        case 0x68:  // Arbitration lost in SLA+R/W as master,
                    // Own SLA+W has been Received ACK has bee returned

        case 0x70:  // General Call address has been received ACK has been returned

        case 0x78:  // Arbitration lost in SLA+R/W as master,
                    // General Call address has been received ACK has been returned

        case 0x80:  // Data byte has been received ACK has been return

        case 0x88:  // Data byte has been received Not ACK has been return

        case 0x90:  // Previously address with General Call address
                    // Data byte has been received ACK has been return

        case 0x98:  // Previously address with General Call address
                    // Data byte has been received Not ACK has been return

        case 0xA0:  // A STOP or repeated START has been received while will addressed as SLV/REC

        case 0xA8:  // Own SLA+R has bee Received ACK has been returned

        case 0xB0:  // Arbitration lost in SLA+R/W as master,
                    // Own SLA+R has been Received ACK has bee returned

        case 0xB8:  // Data byte in SIDAT has been transmitted ACK has been received

        case 0xC0:  // Data byte or Last data byte in SIDAT has been transmitted Not ACK has been received

        case 0xC8:  // Last Data byte in SIDAT has been transmitted ACK has been received

        case 0xD0: 
        case 0xD8:
        case 0xE0:
        case 0xE8:
        case 0xF0:
                    
        case 0xF8:  // Bus Idle

        default:
            break;
    }
    __I2C_ClearStateFlag(SI2C->Instance);
    while(__I2C_GetStateFlag(SI2C->Instance) != 0);
}

/***********************************************************************************
函数名称:	DRV_Return I2C_ByteMode_MasterTransmit(I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
功能描述:	IIC主机发送
输入参数:	I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth
返回参数:	  DRV_Return 
*************************************************************************************/
DRV_Return I2C_ByteMode_MasterTransmit(I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
{
//-----Check----------------------------
    if(__I2C_GetFlagStatus(SI2C->Instance, I2C_FLAG_BUSYF) != CLR)
        return DRV_Busy;

//-----Set_Config-----------------------
    SI2C->State.W = 0;
    SI2C->SlaveAddress = DevAddress & 0xFFFE;
    SI2C->pData = pData;
    SI2C->Count = Lenth;
    SI2C->Control.W = I2C_XferNext_STOP;

//-----Start----------------------------
    Set_STA_STO_AA_100(SI2C->Instance);
    do{
        I2C_ByteMode_Handle(SI2C);
    }while(SI2C->State.MBit.Done == 0);

    if((SI2C->State.W & (I2C_State_BusError | \
                               I2C_State_ArbitrationLost | \
                               I2C_State_NACK)) != 0)
        return DRV_Failure;

    return DRV_Success;
}

/***********************************************************************************
函数名称:	DRV_Return I2C_ByteMode_MasterReceive(I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
功能描述:	IIC主机接收
输入参数:	I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth
返回参数:	  DRV_Return 
*************************************************************************************/
DRV_Return I2C_ByteMode_MasterReceive(I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint8_t *pData, uint16_t Lenth)
{
//-----Check----------------------------
    if(__I2C_GetFlagStatus(SI2C->Instance, I2C_FLAG_BUSYF) != CLR)
        return DRV_Busy;

//-----Set_Config-----------------------
    SI2C->State.W = 0;
    SI2C->SlaveAddress = DevAddress | 0x0001;
    SI2C->pData = pData;
    SI2C->Count = Lenth;
    SI2C->Control.W = I2C_XferNext_STOP;

//-----Start----------------------------
    Set_STA_STO_AA_100(SI2C->Instance);
    do{
        I2C_ByteMode_Handle(SI2C);
    }while(SI2C->State.MBit.Done == 0);

    if((SI2C->State.W & (I2C_State_BusError | \
                               I2C_State_ArbitrationLost | \
                               I2C_State_NACK)) != 0)
        return DRV_Failure;

    return DRV_Success;
		
}

/***********************************************************************************
函数名称:	void Sample_URT0_Init(void)
功能描述:	UART0初始化 
		  TXD(PB8),RXD(PB9)
		  8,n,1 115200bps@12MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void Sample_URT0_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    PIN_InitTypeDef PINX_InitStruct;
    //==Set CSC init
    UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);						// Enable UART0 Clock
		CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);						// Enable PortB Clock
    ProtectModuleReg(CSCprotect);

		PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
		PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
		PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
		PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
		PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
		PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_TX
		GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD at PB8

		PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_RX
		GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD at PB9  
	
	
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 3;	                //Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    //BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 25);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 25);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTX, ENABLE);	                            //Enable TX
    URT_RX_Cmd(URTX, ENABLE);	                            //Enable RX
    
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URTX, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URTX, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URTX, 0);
    
    //=====Enable URT Interrupt=====//
    URT_IT_Config(URTX, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URTX, ENABLE);
    NVIC_EnableIRQ(URT0_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}

/***********************************************************************************
函数名称:	int fputc(int ch,FILE *f)
功能描述:	printf函数重定向
输入参数:	int ch,FILE *f
返回参数:	ch  
*************************************************************************************/
int fputc(int ch,FILE *f)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
	return ch;
}

/***********************************************************************************
函数名称:	void UartSendByte(int ch)
功能描述:	Uart发送函数
输入参数:	int ch
返回参数:	
*************************************************************************************/
void UartSendByte(int ch)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	u32 i;
	char RxData;
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 // D6 setup at PE15
	Sample_URT0_Init();
	printf("hello\n");
	I2C0_ByteMode_Master_Init();
	
	i=0;
    while(1)
    {
		   
    	i++;
    	if(i>=500000)
    	{
				i=0;
				PE15=0;

				if(I2C_IsDeviceReady(&gSI2C0_Handle, 0xA0, 10) != DRV_Success)
				printf("Target device is not ready\n\r");

				if(I2C_ByteMode_MasterTransmit(&gSI2C0_Handle, 0xA0, (uint8_t *)String0, (sizeof(String0)-1)) != DRV_Success)
				printf("Mater transmited data fail\n\r");
					
				if(I2C_ByteMode_MasterReceive(&gSI2C0_Handle, (uint16_t)0xA0, (uint8_t *)lDataBuffer, (sizeof(String0)-1)) != DRV_Success)
				printf("Mater received data fail\n\r");
				RxData=I2C_ReceiveSBUF(I2C0);
				printf("receive:0x%02X\n",RxData);
		PE15=1;			
			}
    }
}

