

/**
 ******************************************************************************
 *
 * @file        MG32x02z_IRQ_Init.h
 * @brief       The IRQ Init h file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *   
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#ifndef _MG32x02z_IRQ_Init_H
 
#define _MG32x02z_IRQ_Init_H


 // <<< Use Configuration Wizard in Context Menu >>>
 //<h> IRQHandler Configuration
     //<q0> NMI IRQHandler Enable 
     #define NMI_IRQHandler_En             0
     //<q0> HardFault IRQHandler Enable 
     #define HardFault_IRQHandler_En       0
     //<q0> SCV IRQHandler Enable 
     #define SVC_IRQHandler_En             0
     //<q0> PendSV IRQHandler Enable 
     #define PendSV_IRQHandler_En          0
     //<q0> SysTick IRQHandler Enable 
     #define SysTick_IRQHandler_En         0
     //<q0> WWDT IRQHandler Enable
     #define WWDT_IRQHandler_En            0
     //<q0> SYS IRQHandler Enable 
     #define SYS_IRQHandler_En             0
     //<q0> EXINT0 IRQHandler Enable 
     #define EXINT0_IRQHandler_En          0
     //<q0> EXINT1 IRQHandler Enable 
     #define EXINT1_IRQHandler_En          0
     //<q0> EXINT2 IRQHandler Enable 
     #define EXINT2_IRQHandler_En          0
     //<q0> EXINT3 IRQHandler Enable 
     #define EXINT3_IRQHandler_En          0
     //<q0> COMP IRQHandler Enable 
     #define COMP_IRQHandler_En            0
     //<q0> DMA IRQHandler Enable
     #define DMA_IRQHandler_En             0
     //<q0> ADC IRQHandler Enable 
     #define ADC_IRQHandler_En             0
     //<q0> DAC IRQHandler Enable
     #define DAC_IRQHandler_En             0
     //<q0> TM0x IRQHandler Enable 
     #define TM0x_IRQHandler_En            0
     //<q0> TM10 IRQHandler Enable 
     #define TM10_IRQHandler_En            0
     //<q0> TM1x IRQHandler Enable 
     #define TM1x_IRQHandler_En            0
     //<q0> TM20 IRQHandler Enable 
     #define TM20_IRQHandler_En            0
     //<q0> TM2x IRQHandler Enable 
     #define TM2x_IRQHandler_En            0
     //<q0> TM3x IRQHandler Enable 
     #define TM3x_IRQHandler_En            0
     //<q0> URT0 IRQHandler Enable 
     #define URT0_IRQHandler_En            0  
     //<q0> URT123 IRQHandler Enable 
     #define URT123_IRQHandler_En          0
     //<q0> SPI0 IRQHandler Enable 
     #define SPI0_IRQHandler_En            0
     //<q0> I2C0 IRQHandler Enable 
     #define I2C0_IRQHandler_En            1
     //<q0> I2Cx IRQHandler Enable 
     #define I2Cx_IRQHandler_En            0
 //</h>
 // <<< end of configuration section >>>




void IRQ_Init(void);


#endif




