/**
 ******************************************************************************
 *
 * @file        Sample_I2C_ByteMode_Master.h
 *
 * @brief       Sample I2C Byte Mode Master Head File 
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 */ 



#include "MG32x02z_DRV.h"


/**
 * @name	I2C Initialze TypeDef
 *          I2C Initialze TypeDef
 */ 
///@{
typedef struct
{
        I2C_Struct         *Instance;
        uint32_t            CK_I2C_PR_Frequency;
        uint32_t            SCL_Clock;
}I2C_InitTypeDef;
///@}


/** @defgroup I2C_Exported_Types I2C Exported Types
  * @{
  */

/**
 * @name	I2C Control TypeDef
 *          I2C Control TypeDef
 */ 
///@{
typedef enum{
    I2C_XferNext_Mask        = 0x00000003,
    I2C_XferNext_RepeatStart = 0x00000001,
    I2C_XferNext_STOP        = 0x00000002,
    I2C_XferNext_StopStart   = 0x00000003,
}I2C_XferNextTypeDef;

typedef union{
    uint32_t W;
    uint16_t H[2];
    uint8_t  B[4];
    struct{
        uint32_t XferNext           :2;
        uint32_t NACKNext           :1;
        uint32_t                    :29;
    }MBit;
}Sample_I2C_ControlTypeDef;
///@}



/**
 * @name	I2C State TypeDef
 *          I2C State TypeDef
 */ 
///@{
#define I2C_State_Done               (uint32_t)0x00000001
#define I2C_State_BusError           (uint32_t)0x00000002
#define I2C_State_ArbitrationLost    (uint32_t)0x00000004
#define I2C_State_NACK               (uint32_t)0x00000008

typedef union{
    uint32_t W;
    uint16_t H[2];
    uint8_t  B[4];
    struct{
        uint32_t Done               :1;
        uint32_t BusError           :1;
        uint32_t ArbitrationLost    :1;
        uint32_t NACK               :1;
        uint32_t                    :28;
    }MBit;
}Sample_I2C_StateTypeDef;
///@}



/**
 * @name	I2C Handle TypeDef
 *          I2C Handle TypeDef
 */ 
///@{
typedef struct{
    __IO I2C_Struct                 *Instance;
    __IO uint8_t                    *pData;
    __IO Sample_I2C_StateTypeDef    State;
    __IO Sample_I2C_ControlTypeDef  Control;
    __IO uint16_t                   Count;
    __IO uint8_t                    SlaveAddress;
}I2C_HandleTypeDef;
///@}

/**
  * @}
  */



void I2C0_ByteMode_Slave_Init(void);
//void I2C1_ByteMode_Master(void);

DRV_Return Sample_I2C_DeInit(I2C_InitTypeDef *SI2C);
DRV_Return I2C_ByteMode_Init(I2C_InitTypeDef *SI2C);

/******* Blocking mode: Polling */
DRV_Return I2C_IsDeviceReady(I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint32_t Trials);

//DRV_Return Sample_I2C_ByteMode_MasterTransmit(Sample_I2C_HandleTypeDef *SI2C, uint16_t SlaveAddress, uint8_t *pData, uint16_t Lenth);
//DRV_Return Sample_I2C_ByteMode_MasterReceive(Sample_I2C_HandleTypeDef *SI2C, uint16_t SlaveAddress, uint8_t *pData, uint16_t Lenth);

void I2C_ByteMode_Handle(I2C_HandleTypeDef *Sample_I2C);

