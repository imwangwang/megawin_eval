/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用SPI做SPI主机发送数据给SPI从机，
 *				并在中断中做SPI的接收，若接收到从机数据为0x50，
 *				则翻转PE15的IO状态。
 *				
 *				
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;


#define Dummy_Data 	0xFFFFFFFF
#define SPI_NSS 	PB0     // SPI_NSS

/***********************************************************************************
函数名称:	void SPI0_IRQHandler (void)
功能描述:	SPI0接收中断程序
输入参数:	
返回参数:	  
*************************************************************************************/
void SPI0_IRQHandler(void)
{
	if(SPI_GetSingleFlagStatus(SPI0, SPI_RXF) == DRV_Happened)	// Wait RXF flag
	{
	  uint32_t RDAT;
	  PE15=0;
  	RDAT = SPI_GetRxData(SPI0);         //获取从机接收的数据
	  if(RDAT == 0x50)
	  {
	    PE15=1;
	  }
	
	}
}

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;

  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
		
	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);

  ProtectModuleReg(CSCprotect);
}


/***********************************************************************************
函数名称:	void InitSPI0 (void)
功能描述:	SPI主机初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void InitSPI0(void)
{  
	
	PIN_InitTypeDef PINX_InitStruct;
	UnProtectModuleReg(CSCprotect);							  // Unprotect CSC module
	CSC_PeriphProcessClockSource_Config(CSC_SPI0_CKS, CK_APB);  // CK_SPIx_PR = CK_APB = 12MHz
	CSC_PeriphOnModeClock_Config(CSC_ON_SPI0, ENABLE);		  // Enable SPI0 module clock
	CSC_PeriphOnModeClock_Config(CSC_ON_PortB, ENABLE);		  // Enable PortB clock
	ProtectModuleReg(CSCprotect); 							  // protect CSC module
	   
	/*=== 2. Default Initial SPI ===*/
	SPI_DeInit(SPI0);
	
	/*=== 3. Configure clock divider ===*/						// SPI clock = 1MHz
	SPI_Clock_Select(SPI0, SPI_CK_SPIx_PR); 					// CK_SPIx = CK_SPIx_PR
	SPI_PreDivider_Select(SPI0, SPI_PDIV_2);					// PDIV outpu = CK_SPIx /2
	SPI_Prescaler_Select(SPI0, SPI_PSC_3);						// Prescaler outpu = PDIV outpu /3
	SPI_Divider_Select(SPI0, SPI_DIV_2);						// DIV outpu = PDIV outpu /2
	
	/*=== 4. Configure SPI data line, mode and data size... ===*/
	SPI_DataLine_Select(SPI0, SPI_Standard);			// SPI Standard mode 
	SPI_ModeAndNss_Select(SPI0, SPI_Master);					// Master
	SPI_ClockPhase_Select(SPI0, SPI_LeadingEdge);				// CPHA = 0
	SPI_ClockPolarity_Select(SPI0, SPI_Low);					// CPOL = 0
	SPI_FirstBit_Select(SPI0, SPI_MSB); 						// MSB first
	SPI_DataSize_Select(SPI0, SPI_8bits);						// Data size 8bits
	
	/*=== 5. Config SPI0 IO ===*/
	PINX_InitStruct.PINX_Mode				= PINX_Mode_PushPull_O; 	// Pin select pusu pull mode
	PINX_InitStruct.PINX_PUResistant		= PINX_PUResistant_Enable;	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed				= PINX_Speed_Low;			
	PINX_InitStruct.PINX_OUTDrive			= PINX_OUTDrive_Level1; 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider		= PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			= PINX_Inverse_Disable; 	// Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 2;						// Pin AFS = 2
	GPIO_PinMode_Config(PINB(2),&PINX_InitStruct);						// CLK setup at PB2
	
	PINX_InitStruct.PINX_OUTDrive			= PINX_OUTDrive_Level1; 	// Pin output drive strength 1/4
	GPIO_PinMode_Config(PINB(3),&PINX_InitStruct);						// MOSI setup at PB3
	
	PINX_InitStruct.PINX_Mode				= PINX_Mode_OpenDrain_O;    // Setting open drain mode
	PINX_InitStruct.PINX_Alternate_Function = 2;                // Pin AFS = 2
  GPIO_PinMode_Config(PINB(1),&PINX_InitStruct);              // MISO setup at PB1

	PINX_InitStruct.PINX_Alternate_Function = 0;						// Pin AFS = 0
	GPIO_PinMode_Config(PINB(0),&PINX_InitStruct);						// NSS setup at PB0

	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_Alternate_Function  = 0;						 	// Pin AFS = 0
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 	// LED指示灯IO
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 	// LED指示灯IO

	NVIC_EnableIRQ(SPI0_IRQn);
	SPI_IT_Config(SPI0, SPI_INT_RX,ENABLE);
	SPI_ITEA_Cmd(SPI0, ENABLE);

	/*=== 6. Enable SPI ===*/
	SPI_Cmd(SPI0, ENABLE);		
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	PIN_InitTypeDef PINX_InitStruct;
	u32 i=0;
	CSC_Init();
	InitSPI0();
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 	 	// Pin select Push Pull mode
	PINX_InitStruct.PINX_PUResistant		= PINX_PUResistant_Enable;	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed				= PINX_Speed_Low;			
	PINX_InitStruct.PINX_OUTDrive			= PINX_OUTDrive_Level1; 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider		= PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			= PINX_Inverse_Disable; 	// Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function  = 0;						 	// Pin AFS = 0
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 	// LED指示灯IO
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 	// LED指示灯IO
	
  while(1)
  {
    i++;
    if(i>=500000)
    {
			i=0;   	
			SPI_NSS = 0;  			//拉低NSS引脚
			PE13=0;
			SPI_SetTxData(SPI0, SPI_1Byte, 0x05);                               //发送1个字节的数据
			while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TCF flag	
			SPI_ClearFlag(SPI0, SPI_TCF);    // Clear flag		
			SPI_NSS = 1;   			//拉高NSS引脚
			PE13=1;					
		}
  }
}

