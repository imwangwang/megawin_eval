/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用SPI做SPI从机，并做中断接收。
 *				该demo会在接收到数据后串口打印接收值，并
 *				向主机发送一个0x50的值。
 *				
 *				
 *					
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"
#include "MG32x02z_GPIO_DRV.H"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define Dummy_Data 	0xFFFFFFFF

#define SPI_NSS 	PB0     // SPI_NSS
#define URTX URT0

/***********************************************************************************
函数名称:	void SPI0_IRQHandler (void)
功能描述:	SPI0接收中断程序
输入参数:	
返回参数:	  
*************************************************************************************/
void SPI0_IRQHandler(void)
{
	if(SPI_GetSingleFlagStatus(SPI0, SPI_RXF) == DRV_Happened)	// Wait RXF flag
	{
		uint32_t RDAT;
		PE13=0;
		RDAT = SPI_GetRxData(SPI0);                                         //获取接收到的数据
		printf("RX:0x%02X\n",RDAT);
		SPI_SetTxData(SPI0,SPI_1Byte,0x50);																	//向主机发送值
		PE13=1;
	}
}

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;

  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
		
	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);

  ProtectModuleReg(CSCprotect);
    
}


/***********************************************************************************
函数名称:	int fputc(int ch,FILE *f)
功能描述:	printf函数重定向
输入参数:	int ch,FILE *f
返回参数:	ch  
*************************************************************************************/
int fputc(int ch,FILE *f)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
	return ch;
}

/***********************************************************************************
函数名称:	void UartSendByte(int ch)
功能描述:	Uart发送函数
输入参数:	int ch
返回参数:	
*************************************************************************************/
void UartSendByte(int ch)
{
	
	URT_SetTXData(URTX,1,ch);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
	
}

/***********************************************************************************
函数名称:	void URT0_Init(void)
功能描述:	UART0初始化 
		  TXD(PB8),RXD(PB9)
		  8,n,1 115200bps@12MHz
输入参数:	
返回参数:	  
*************************************************************************************/
void URT0_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
		PIN_InitTypeDef PINX_InitStruct;

		UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);						// Enable UART0 Clock
		CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);						// Enable PortB Clock
		ProtectModuleReg(CSCprotect);
			
		//==Set GPIO init
		//PB8 PPO TX ,PB9 ODO RX
		PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
		PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
		PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
		PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
		PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
		PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_TX
		GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD at PB8

		PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
		PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_RX
		GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD at PB9

    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                	//Set PSR
    URT_BRG.URT_BaudRateCounterReload = 3;	                	//Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    	//BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            	//Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);		//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);		//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 25);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 25);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTX, ENABLE);	                            	//Enable TX
    URT_RX_Cmd(URTX, ENABLE);	                            	//Enable RX
    
    
 
    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URTX, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URTX, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URTX, 0);
    
    //=====Enable URT Interrupt=====//
    //URT_IT_Config(URTX, URT_IT_RX, ENABLE);
    //URT_ITEA_Cmd(URTX, ENABLE);
    //NVIC_EnableIRQ(URT0_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);
		
}

/***********************************************************************************
函数名称:	void InitSPI0 (void)
功能描述:	SPI从机初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void InitSPI0(void)
{  
   
	PIN_InitTypeDef PINX_InitStruct;

	/*=== 1. Enable CSC to SPI clock ===*/
	UnProtectModuleReg(CSCprotect);							  // Unprotect CSC module
	CSC_PeriphProcessClockSource_Config(CSC_SPI0_CKS, CK_APB);  // CK_SPIx_PR = CK_APB = 12MHz
	CSC_PeriphOnModeClock_Config(CSC_ON_SPI0, ENABLE);		  // Enable SPI0 module clock
	CSC_PeriphOnModeClock_Config(CSC_ON_PortB, ENABLE);		  // Enable PortB clock
	ProtectModuleReg(CSCprotect); 							  // protect CSC module

	/*=== 2. Default Initial SPI ===*/
	SPI_DeInit(SPI0);

	/*=== 3. Configure clock divider ===*/						// SPI clock = 1MHz
	SPI_Clock_Select(SPI0, SPI_CK_SPIx_PR); 					// CK_SPIx = CK_SPIx_PR
	SPI_PreDivider_Select(SPI0, SPI_PDIV_2);					// PDIV outpu = CK_SPIx /2
	SPI_Prescaler_Select(SPI0, SPI_PSC_3);						// Prescaler outpu = PDIV outpu /3
	SPI_Divider_Select(SPI0, SPI_DIV_2);						// DIV outpu = PDIV outpu /2

	/*=== 4. Configure SPI data line, mode and data size... ===*/
	SPI_DataLine_Select(SPI0, SPI_Standard);					// SPI Standard mode
	SPI_ModeAndNss_Select(SPI0, SPI_SlaveWithNss);					// Slave with nss
	SPI_NSSInputSignal_Select(SPI0,SPI_NssPin);					// Nss
	SPI_ClockPhase_Select(SPI0, SPI_LeadingEdge);				// CPHA = 0
	SPI_ClockPolarity_Select(SPI0, SPI_Low);					// CPOL = 0
	SPI_FirstBit_Select(SPI0, SPI_MSB); 						// MSB first
	SPI_DataSize_Select(SPI0, SPI_8bits);						// Data size 8bits
	SPI_SlaveModeReceivedThreshold_Select(SPI0, SPI_1Byte);     // Set SPI0 received data buffer high threshold

	/*=== 5. Config SPI0 IO ===*/
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_Digital_I; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 2;						 // Pin AFS = 2

	GPIO_PinMode_Config(PINB(0),&PINX_InitStruct); 					 // NSS setup at PB0
	GPIO_PinMode_Config(PINB(2),&PINX_InitStruct); 					 // CLK setup at PB2
	GPIO_PinMode_Config(PINB(3),&PINX_InitStruct); 					 // MOSI setup at PB3
	
  PINX_InitStruct.PINX_Mode				= PINX_Mode_PushPull_O;     // Setting pusu pull mode
  GPIO_PinMode_Config(PINB(1),&PINX_InitStruct);                      // MISO setup at PB1
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O;	 // Setting pusu pull mode
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 // D4 setup at PE13


	/*=== 6. Enable SPI ===*/
	SPI_IT_Config(SPI0, SPI_INT_RX,ENABLE);
	SPI_ITEA_Cmd(SPI0, ENABLE);
	SPI_Cmd(SPI0, ENABLE); 											 // Enable SPI 

}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O;	 // Setting Open Drain mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse

	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
	GPIO_PinMode_Config(PINE(13),&PINX_InitStruct); 					 // LED指示灯IO
	InitSPI0();	
	NVIC_EnableIRQ(SPI0_IRQn);
	URT0_Init();
	printf("hello\n");
  while(1)
  {

  }
}

