/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在SPI主机利用DMA功能一次性发送多个数据给SPI从机。
 *				
 *				
 *				注意：
 *				DMA_TCF只是DMA搬运完成，但是SPI发送完成要看SPI_TCF。
 *				
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */

#include "MG32x02z_DRV.H"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define Dummy_Data 	0xFFFFFFFF

#define SPI_NSS 	PB0     // SPI_NSS
#define URTX URT0
#define MYBINARYIMAGE2_LENGTH 6

uint8_t SendBuf[MYBINARYIMAGE2_LENGTH]={0x05,0x10,0x15,0x20,0x25,0x30};//__attribute__((at(0x20001000)));

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;

  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1
	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO

	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/
	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	

	/* Configure peripheral clock */
	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);

  ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	void InitSPI0 (void)
功能描述:	SPI主机初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void InitSPI0(void)
{  
	
	PIN_InitTypeDef PINX_InitStruct;
	
	UnProtectModuleReg(CSCprotect);							  // Unprotect CSC module
	CSC_PeriphProcessClockSource_Config(CSC_SPI0_CKS, CK_APB);  // CK_SPIx_PR = CK_APB = 12MHz
	CSC_PeriphOnModeClock_Config(CSC_ON_SPI0, ENABLE);		  // Enable SPI0 module clock
	CSC_PeriphOnModeClock_Config(CSC_ON_PortB, ENABLE);		  // Enable PortB clock
	ProtectModuleReg(CSCprotect); 							  // protect CSC module

	/*=== 2. Default Initial SPI ===*/
	SPI_DeInit(SPI0);
	
	/*=== 3. Configure clock divider ===*/						// SPI clock = 6MHz
	SPI_Clock_Select(SPI0, SPI_CK_SPIx_PR); 					// CK_SPIx = CK_SPIx_PR
	SPI_PreDivider_Select(SPI0, SPI_PDIV_1);					// PDIV outpu = CK_SPIx /1
	SPI_Prescaler_Select(SPI0, SPI_PSC_1);						// Prescaler outpu = PDIV outpu /1
	SPI_Divider_Select(SPI0, SPI_DIV_2);						// DIV outpu = PDIV outpu /2
	
	/*=== 4. Configure SPI data line, mode and data size... ===*/
	SPI_DataLine_Select(SPI0, SPI_Standard);			// SPI Standard mode 
	SPI_ModeAndNss_Select(SPI0, SPI_MasterWithNss);					// Master
	SPI_ClockPhase_Select(SPI0, SPI_LeadingEdge);				// CPHA = 0
	SPI_ClockPolarity_Select(SPI0, SPI_Low);					// CPOL = 0
	SPI_FirstBit_Select(SPI0, SPI_MSB); 						// MSB first
	SPI_DataSize_Select(SPI0, SPI_8bits);						// Data size 8bits
	
	/*=== 5. Config SPI0 IO ===*/
	PINX_InitStruct.PINX_Mode				= PINX_Mode_PushPull_O; 	// Pin select pusu pull mode
	PINX_InitStruct.PINX_PUResistant		= PINX_PUResistant_Enable;	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed				= PINX_Speed_Low;			
	PINX_InitStruct.PINX_OUTDrive			= PINX_OUTDrive_Level0; 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider		= PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			= PINX_Inverse_Disable; 	// Pin input data not inverse
	
	PINX_InitStruct.PINX_Alternate_Function = 2;						// Pin AFS = 2
	GPIO_PinMode_Config(PINB(2),&PINX_InitStruct);						// CLK setup at PB2
	
  PINX_InitStruct.PINX_Mode				= PINX_Mode_PushPull_O; 	// Pin select pusu pull mode
	PINX_InitStruct.PINX_OUTDrive			= PINX_OUTDrive_Level0; 	// Pin output drive strength 1/4
	GPIO_PinMode_Config(PINB(3),&PINX_InitStruct);						// MOSI setup at PB3

	PINX_InitStruct.PINX_Alternate_Function = 2;						// Pin AFS = 0
	GPIO_PinMode_Config(PINB(0),&PINX_InitStruct);						// NSS setup at PB0
	
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O;   //Pin select OpenDrain mode
	GPIO_PinMode_Config(PINB(1),&PINX_InitStruct);						// MISO setup at PB1

	/*=== 6. Enable SPI ===*/

	SPI_Cmd(SPI0, ENABLE);		

}

/***********************************************************************************
函数名称:	void DMA_Init (void)
功能描述:	DMA初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void DMA_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;

	 		UnProtectModuleReg(CSCprotect);
		CSC_PeriphOnModeClock_Config(CSC_ON_DMA,ENABLE);						// Enable DMA Clock
		ProtectModuleReg(CSCprotect);

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = ENABLE;
        DMATestPattern.DestDINCSel = DISABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_MEM_Read;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_SPI0_TX;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = 3;
    
        // source/destination config
        DMATestPattern.DMASourceAddr = (uint32_t *)&SendBuf;
        DMATestPattern.DMADestinationAddr = &SPI0->TDAT;
				
				
				DMA_Channel_Cmd(DMAChannel0, ENABLE);
				DMA_Base_Init(&DMATestPattern);
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	u32 i=0;
	PIN_InitTypeDef PINX_InitStruct;
	CSC_Init();
	InitSPI0();	
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
	GPIO_PinMode_Config(PINE(15),&PINX_InitStruct); 					 // D6 setup at PE15
	DMA_Init();
  while(1)
  {
    i++;
    if(i>=500000)
    {
			i=1;	
			SPI_NSS=0;
			SPI_TXDMA_Cmd(SPI0, ENABLE);
			DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
			PE15=~PE15;
			DMA_StartRequest(DMAChannel0);
			while (DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_TCF) == DRV_UnHappened);
			DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
			while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);	//DMA的TCF只是DMA搬运完成，但是SPI发送完成要看SPI TCF
			SPI_ClearFlag(SPI0, (SPI_TXF | SPI_RXF));  
			SPI_NSS=1;		
		}
  }
}




















