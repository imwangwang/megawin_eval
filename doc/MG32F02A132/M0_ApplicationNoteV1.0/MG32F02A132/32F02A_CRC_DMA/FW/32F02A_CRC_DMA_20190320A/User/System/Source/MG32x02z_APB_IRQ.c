

/**
 ******************************************************************************
 *
 * @file        MG32x02z_APB_IRQ.c
 * @brief       The APB Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/02/01
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */
 
 
// <<< Use Configuration Wizard in Context Menu >>>


// <<< end of configuration section >>>    



#include "MG32x02z_APB_DRV.h"






/**
 *******************************************************************************
 * @brief	    APB interrupt function.
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
__weak void APB_IRQ(void)
{
    
    // APB OBM1 trigger event detect flag
    if(APB_GetSingleFlagStatus(APB, APB_OBM1F) == DRV_Happened)
    {       
        // To do...
        
        APB_ClearFlag(APB, APB_OBM1F);
    }

}



