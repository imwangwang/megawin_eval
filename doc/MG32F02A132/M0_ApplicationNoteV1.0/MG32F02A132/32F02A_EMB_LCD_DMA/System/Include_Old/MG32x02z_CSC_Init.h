/**
 *******************************************************************************
 * @file        MG32x02z_CSC_Init.h
 *
 * @brief       TThe CSC initial code h file
 *
 * MG32x02z remote controller
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * #001_Hades_2018.01.31
 *  >> File rename.
 *
 @endif
 *******************************************************************************
 */

#include "MG32x02z_CSC_DRV.h"
#include "MG32x02z_MEM_DRV.h"
#include "MG32x02z_GPIO_DRV.H"


#ifndef _MG32x02z_CSC_INIT_H
/*!< _MG32x02z_CSC_INIT_H */ 
#define _MG32x02z_CSC_INIT_H


//*** <<< Use Configuration Wizard in Context Menu >>> ***
static const unsigned int CSC_InitConfig[] =
{
//  <h> Configure CSC Module

//      <o18> Enter XOSC Or External Clock Frequency 1~25000000Hz <1-25000000>

//      <o4.18> Select IHRCO <0=> 12MHz
//                           <1=> 11.0592MHz

/*      <e17.0> Enable XOSC */
//          <i> test
//          <o2.16..17> Select XOSC Gain <0=> Low Gain For 32 KHz
//                                       <1=> Highest Gain 4 ~ 25MHz
//                                       <2=> Lowest Gain For 32 KHz
/*      </e> */

//      <q4.4> Disable MCD(Missing Clock Detector)
//      <o4.22..23> Select Missing Clock Detection Duration <0=> 125us
//                                                          <1=> 250us
//                                                          <2=> 500us
//                                                          <3=> 1ms

//      <o4.10..11> Select CK_HS Source <0=> CK_IHRCO
//                                      <1=> CK_XOSC
//                                      <2=> CK_ILRCO
//                                      <3=> CK_EXT
//      <i> When Select CK_HS Source = CK_XOSC, Mz92G8z_GPIO_Init.h Wizard PC13 configutaion and PC14 configutaion  must disable.

/*      <e4.5> Configure PLL */
//      <h> Configure PLL
//          <o5.0..1> Select CK_PLLI Divider <0=> CK_HS/1
//                                           <1=> CK_HS/2
//                                           <2=> CK_HS/4
//                                           <3=> CK_HS/6
//          <o2.8> Select CK_PLL Multiplication factor <0=> CK_PLLIx16
//                                                     <1=> CK_PLLIx24
//          <o5.4..5> Select CK_PLLO Divider <0=> CK_PLL/4
//                                           <1=> CK_PLL/3
//                                           <2=> CK_PLL/2
//            
//      </h>
/*      </e> */

//      <o4.14..15> Select CK_MAIN Source <0=> CK_HS
//                                        <1=> CK_PLLI
//                                        <2=> CK_PLLO 
//      <o4.8..9> Select CK_LS Source <1=> CK_XOSC
//                                    <2=> CK_ILRCO
//                                    <3=> CK_EXT
//      <i> When Select CK_LS Source = CK_XOSC, Mz92G8z_GPIO_Init.h Wizard PC13 configutaion and PC14 configutaion  must disable.

//      <o4.16> Select CK_ST Source <0=> HCLK/8
//                                  <1=> CK_LS/2

//      <o5.16..18> Select APB Prescaler <0x00=> CK_MAIN/1
//                                       <0x01=> CK_MAIN/2
//                                       <0x02=> CK_MAIN/4
//                                       <0x03=> CK_MAIN/8
//                                       <0x04=> CK_MAIN/16
//      <o5.8..11> Select AHB Prescaler <0x00=> CK_AHB/1
//                                      <0x01=> CK_AHB/2
//                                      <0x02=> CK_AHB/4
//                                      <0x03=> CK_AHB/8
//                                      <0x04=> CK_AHB/16
//                                      <0x05=> CK_AHB/32
//                                      <0x06=> CK_AHB/64
//                                      <0x07=> CK_AHB/128
//                                      <0x08=> CK_AHB/256
//                                      <0x09=> CK_AHB/512
//      <o5.26..27> Select CK_UT Divider <0x01=> ILRCO/8
//                                       <0x00=> ILRCO/32
//                                       <0x02=> ILRCO/16
//                                       <0x03=> ILRCO/128

//      <h> Configure Peripheral On Mode Clock
//          <q7.0>  Port A
//          <q7.1>  Port B
//          <q7.2>  Port C
//          <q7.3>  Port D
//          <q7.4>  Port E
//          <q7.8>  GPL
//          <q7.12> EMB
//          <q7.15> DMA
//          <e8.0>  ADC0
//              <o14.0>  Select ADCx_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e8.2>  CMP
//              <o14.4>  Select CMP_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e8.3>  DAC
//              <o14.5>  Select DAC_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <q8.5>  RTC
//          <q8.6>  IWDT
//          <q8.7>  WWDT
//          <e8.8>  I2C0
//              <o15.0>  Select I2C0_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e8.9>  I2C1
//              <o15.2>  Select I2C1_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e8.12> SPI0
//              <o15.8>  Select SPI0_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e8.16> URT0
//              <o15.16> Select URT0_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e8.17> URT1
//              <o15.18> Select URT1_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e8.18> URT2
//              <o15.20> Select URT2_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e8.19> URT3
//              <o15.22> Select URT3_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e9.0>  TM00
//              <o16.0>  Select TM00_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e9.1>  TM01
//              <o16.2>  Select TM01_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e9.4>  TM10
//              <o16.8>  Select TM10_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e9.7>  TM16
//              <o16.14> Select TM16_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e9.8>  TM20
//              <o16.16> Select TM20_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e9.11> TM26
//              <o16.22> Select TM26_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e9.15> TM36
//              <o16.30> Select TM36_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//      </h>

//      <h> Configure Peripheral Sleep Mode Clock
//          <q10.0>  ADC0
//          <q10.2>  CMP
//          <q10.3>  DAC
//          <q10.5>  RTC
//          <q10.6>  IWDT
//          <q10.7>  WWDT
//          <q10.8>  I2C0
//          <q10.9>  I2C1
//          <q10.12> SPI0
//          <q10.16> URT0
//          <q10.17> URT1
//          <q10.18> URT2
//          <q10.19> URT3
//          <q11.0>  TM00
//          <q11.1>  TM01
//          <q11.4>  TM10
//          <q11.7>  TM16
//          <q11.8>  TM20
//          <q11.11> TM26
//          <q11.15> TM36
//          <q11.30> EMB
//      </h>

//      <h> Configure Peripheral Stop Mode Clock
//          <q12.5>  RTC
//          <q12.6>  IWDT
//      </h>

//      <e06.0> Enable ICKO
//          <o6.4..7> Select CKO <0=> CK_MAIN
//                               <1=> CK_AHB
//                               <2=> CK_APB
//                               <3=> CK_HS
//                               <4=> CK_LS
//                               <5=> CK_XOSC
//          <o6.2..3> Select ICKO Divider <0=> ICK/1
//                                        <1=> ICK/2
//                                        <2=> ICK/4
//                                        <3=> ICK/8
//      </e>
//  </h>

//*** <<< end of configuration section >>>    ***

0x00000000, /* CSC_STA 0 not use */
0x00000000, /* CSC_INT 1 not use */
0x00010100, /* CSC_PLL 2 */
0x00000000, /* CSC_KEY 3 not use */
0x00C18230, /* CSC_CR0 4 */
0x00000001, /* CSC_DIV 5 */
0x00000039, /* CSC_CKO 6 */
0x0000911F, /* CSC_AHB 7 */
0x000F13ED, /* CSC_APB0 8 */
0x00008993, /* CSC_APB1 9 */
0x00000000, /* CSC_SLP0 10 */
0x00000000, /* CSC_SLP1 11 */
0x00000000, /* CSC_STP0 12 */
0x00000000, /* CSC_STP1 13 */
0x00000000, /* CSC_CSK0 14 */
0x00000000, /* CSC_CSK1 15 */
0x00000000, /* CSC_CSK2 16 */
0x00000001, /* CSC_OTHER 17 . bit0: 0: extern clock, 1: crystal clock */
12000000,   /* XOSC_EXTCLK_FREQ 18 */
};


/**
 ******************************************************************************
 *
 * @struct      CSC_CFG_Struct
 *              CSC Configuration Struct
 *
 ******************************************************************************
 */
typedef struct
{
    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __I  uint8_t                :8;     //[7..0] 
            __IO uint8_t  PLL_MUL       :1;     //[8] CSC PLL multiplication factor select.
                                        //0 = 16 : PLL input clock x 16
                                        //1 = 24 : PLL input clock x 24
            __I  uint8_t                :7;     //[15..9] 
            __IO uint8_t  XOSC_GN       :2;     //[17..16] Gain control bits of XOSC. (The default value is loaded from CFG OR after warm reset)
                                        //0x0 = Lowest (for 32KHz crystal)
                                        //0x1 = Medium_Low
                                        //0x2 = Medium_High
                                        //0x3 = Highest
            __I  uint8_t                :6;     //[23..18] 
            __I  uint8_t                :8;     //[31..24] 
        }MBIT;
    }PLL;                               /*!< PLL        ~ Offset[0x08]  CSC OSC and PLL control register */

    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __I  uint8_t                :1;     //[0] 
            __I  uint8_t                :1;     //[1] 
            __I  uint8_t                :1;     //[2] 
            __IO uint8_t  IHRCO_EN      :1;     //[3] IHRCO circuit enable.
                                        //0 = Disable
                                        //1 = Enable
            __IO uint8_t  MCD_DIS       :1;     //[4] MCD missing clock detector circuit disable.
                                        //0 = Enable
                                        //1 = Disable
            __IO uint8_t  PLL_EN        :1;     //[5] PLL circuit enable.
                                        //0 = Disable
                                        //1 = Enable
            __I  uint8_t                :1;     //[6] 
            __I  uint8_t                :1;     //[7] 
            __IO uint8_t  LS_SEL        :2;     //[9..8] Input low speed clock source select
                                        //0x0 = Reserved
                                        //0x1 = XOSC
                                        //0x2 = ILRCO
                                        //0x3 = CK_EXT
            __IO uint8_t  HS_SEL        :2;     //[11..10] Input high speed clock source select
                                        //0x0 = IHRCO
                                        //0x1 = XOSC
                                        //0x2 = ILRCO
                                        //0x3 = CK_EXT
            __I  uint8_t                :1;     //[12] 
            __I  uint8_t                :1;     //[13] 
            __IO uint8_t  MAIN_SEL      :2;     //[15..14] System clock source select.
                                        //0x0 = CK_HS
                                        //0x1 = CK_PLLI
                                        //0x2 = CK_PLLO
                                        //0x3 = Reserved
            __IO uint8_t  ST_SEL        :1;     //[16] System tick timer external clock source select.
                                        //0 = HCLK8 : HCLK divided by 8
                                        //1 = CK_LS2 : CK_LS divided by 2
            __I  uint8_t                :1;     //[17] 
            __IO uint8_t  IHRCO_SEL     :1;     //[18] IHRCO clock frequency trimming set select.
                                        //0 = 12 : 12MHz from trimming set 0
                                        //1 = 11 : 11.059MHz from trimming set 1
            __I  uint8_t                :3;     //[21..19] 
            __IO uint8_t  MCD_SEL       :2;     //[23..22] Missing clock detection duration select.
                                        //0x0 = 125us
                                        //0x1 = 250us
                                        //0x2 = 500us
                                        //0x3 = 1ms
            __I  uint8_t                :8;     //[31..24] 
        }MBIT;
    }CR0;                               /*!< CR0        ~ Offset[0x10]  CSC clock source control register 0 */

    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __IO uint8_t  PLLI_DIV      :2;     //[1..0] PLL input clock source divider
                                        //0x0 = DIV1 : divided by 1
                                        //0x1 = DIV2 : divided by 2
                                        //0x2 = DIV4 : divided by 4
                                        //0x3 = DIV6 : divided by 6
            __I  uint8_t                :2;     //[3..2] 
            __IO uint8_t  PLLO_DIV      :2;     //[5..4] PLL output clock source divider
                                        //0x0 = DIV4 : divided by 4
                                        //0x1 = DIV3 : divided by 3
                                        //0x2 = DIV2 : divided by 2
                                        //0x3 = DIV1 : divided by 1
            __I  uint8_t                :2;     //[7..6] 
            __IO uint8_t  AHB_DIV       :4;     //[11..8] AHB clock source divider. Value 0~9 mean to divide by 1,2,4,8,16,32,64,128,256,512.
                                        //0x0 = DIV1 : divided by 1
                                        //0x1 = DIV2 : divided by 2
                                        //0x2 = DIV4 : divided by 4
                                        //0x3 = DIV8 : divided by 8
                                        //0x4 = DIV16 : divided by 16
                                        //0x5 = DIV32 : divided by 32
                                        //0x6 = DIV64 : divided by 64
                                        //0x7 = DIV128 : divided by 128
                                        //0x8 = DIV256 : divided by 256
                                        //0x9 = DIV512 : divided by 512
            __I  uint8_t                :4;     //[15..12] 
            __IO uint8_t  APB_DIV       :3;     //[18..16] APB clock source divider. Value 0~4 mean to divide by 1,2,4,8,16.
                                        //0x0 = DIV1 : divided by 1
                                        //0x1 = DIV2 : divided by 2
                                        //0x2 = DIV4 : divided by 4
                                        //0x3 = DIV8 : divided by 8
                                        //0x4 = DIV16 : divided by 16
            __I  uint8_t                :5;     //[23..19] 
            __I  uint8_t                :1;     //[24] 
            __I  uint8_t                :1;     //[25] 
            __IO uint8_t  UT_DIV        :2;     //[27..26] Unit time clock source divider.
                                        //0x0 = DIV32 : divided by 32
                                        //0x1 = DIV8 : divided by 8
                                        //0x2 = DIV16 : divided by 16
                                        //0x3 = DIV128 : divided by 128
            __I  uint8_t                :4;     //[31..28] 
        }MBIT;
    }DIV;                               /*!< DIV        ~ Offset[0x14]  CSC clock  divider register */

} CSC_CFG_Struct;


/**
 * @name	Function announce
 *   		
 */
///@{  
void CSC_Init (uint32_t* );
///@}


#endif  // _MG32x02z_CSC_INIT_H

