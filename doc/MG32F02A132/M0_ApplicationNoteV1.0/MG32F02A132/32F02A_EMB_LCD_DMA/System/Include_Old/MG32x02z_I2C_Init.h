
/**
 ******************************************************************************
 *
 * @file        MG32x02z_I2C_Init.h
 *
 * @brief       The I2C Init include file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 * 
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */


#ifndef __MG32x02z_I2C_INIT_H
#define __MG32x02z_I2C_INIT_H
#define __MG32x02z_I2C_INIT_H_VER                          0.01     /*!< File Version */



//*** <<< Use Configuration Wizard in Context Menu >>> ***
//<o0> CK_UT Clock Frequency 1~50000000Hz <1-50000000>
//<o1> CK_I2Cx_PR Clock Frequency 1~50000000Hz <1-50000000>
//<o2> TM00_TRGO Clock Frequency 1~50000000Hz <1-50000000>
#define     CK_UT           32000
#define     CK_I2Cx_PR      24000000
#define     TM00_TRGO       0



//<e0.0> I2C0 Initial Enable
//  <o2.2..3> Clock Source(Default APB) <0=> CK_I2Cx_PR <2=>TM00_TROG
//  <o2.8..10> internal clock CK_I2C_PSC prescaler 1~8 <1-8>
//  <o2.4..6> internal clock CK_I2C_DIV Divider 
//                                <1=> DIV1 : Divided by 1
//                                <1=> DIV2 : Divided by 2
//                                <2=> DIV4 : Divided by 4
//                                <3=> DIV8 : Divided by 8
//                                <4=> DIV16 : Divided by 16
//                                <5=> DIV32 : Divided by 32
//                                <6=> DIV64 : Divided by 64
//                                <7=> DIV128 : Divided by 128
//  <o4.8..12> HT : SCL high cycle time by CK_I2C_INT clock time 1-16. Default 5 <0-15>
//  <o4.0..3> LT : SCL low cycle time by CK_I2C_INT clock time 1-16. Default 4 <0-15>
//  <q3.7> general call address 0x00 recognized enable bit.
//  <e3.1> slave mode 1st slave address detect enable. 
//    <o5.1..7> slave mode 1st slave address detection request address value. 0x00~0x7F <0x00-0x7F>
//  </e>
//  <e3.2> slave mode 2nd slave address detect enable. 
//    <o5.9..15> slave mode 2nd slave address detection request address value. 0x00~0x7F <0x00-0x7F>
//  </e>
//  <e6.0> TimeOut Enable
//    <o2.12> timeout clock source select.
//                                <0=> CK_UT
//                                <1=> PSC64 (CK_I2C_PSC divided by 64)
//    <o6.2..3> timeout detection mode select
//                                <0=> SCL-low (SCL low timeout)
//                                <1=> SCL-SDA-high (both SCL and SDA high timeout for 
//                                <2=> General (general counter)
//    <o6.8..15> timeout setting value. 0~255 <0-255>
//  </e>
//  <e1.0> Interrupt Enable
//    <q1.1> Byte Mode Event Interrupt Enable
//    <q1.3> Error Event Interrupt Enable
//    <q1.4> Timeout Interrupt Enable
//    <q1.5> WakeUp Interrupt Enable
//  </e>
//  <q3.0> I2C function enable bit.
//</e>
#define     I2C0_Config0_Enable       1
#define     I2C0_Config0_INT          0x0000003B
#define     I2C0_Config0_CLK          0x00000620
#define     I2C0_Config0_CR0          0xC0000887
#define     I2C0_Config0_CR1          0x00000504
#define     I2C0_Config0_SADR         0x00006020
#define     I2C0_Config0_TMO          0x00000017



//<e0.0> I2C1 Initial Enable
//  <o2.2..3> Clock Source(Default APB) <0=> CK_I2Cx_PR <2=>TM00_TROG
//  <o2.8..10> internal clock CK_I2C_PSC prescaler 1~8 <1-8>
//  <o2.4..6> internal clock CK_I2C_DIV Divider 
//                                <1=> DIV1 : Divided by 1
//                                <1=> DIV2 : Divided by 2
//                                <2=> DIV4 : Divided by 4
//                                <3=> DIV8 : Divided by 8
//                                <4=> DIV16 : Divided by 16
//                                <5=> DIV32 : Divided by 32
//                                <6=> DIV64 : Divided by 64
//                                <7=> DIV128 : Divided by 128
//  <o4.8..12> HT : SCL high cycle time by CK_I2C_INT clock time 1-16. Default 5 <0-15>
//  <o4.0..3> LT : SCL low cycle time by CK_I2C_INT clock time 1-16. Default 4 <0-15>
//  <q3.7> general call address 0x00 recognized enable bit.
//  <e3.1> slave mode 1st slave address detect enable. 
//    <o5.1..7> slave mode 1st slave address detection request address value. 0x00~0x7F <0x00-0x7F>
//  </e>
//  <e3.2> slave mode 2nd slave address detect enable. 
//    <o5.9..15> slave mode 2nd slave address detection request address value. 0x00~0x7F <0x00-0x7F>
//  </e>
//  <e6.0> TimeOut Enable
//    <o2.12> timeout clock source select.
//                                <0=> CK_UT
//                                <1=> PSC64 (CK_I2C_PSC divided by 64)
//    <o6.2..3> timeout detection mode select
//                                <0=> SCL-low (SCL low timeout)
//                                <1=> SCL-SDA-high (both SCL and SDA high timeout for 
//                                <2=> General (general counter)
//    <o6.8..15> timeout setting value. 0~255 <0-255>
//  </e>
//  <e1.0> Interrupt Enable
//    <q1.1> Byte Mode Event Interrupt Enable
//    <q1.3> Error Event Interrupt Enable
//    <q1.4> Timeout Interrupt Enable
//    <q1.5> WakeUp Interrupt Enable
//  </e>
//  <q3.0> I2C function enable bit.
//</e>
#define     I2C1_Config0_Enable       1
#define     I2C1_Config0_INT          0x00020007
#define     I2C1_Config0_CLK          0x00000008
#define     I2C1_Config0_CR0          0xC0000801
#define     I2C1_Config0_CR1          0x00000504
#define     I2C1_Config0_SADR         0x00008040
#define     I2C1_Config0_TMO          0x00000016

//*** <<< end of configuration section >>> ***

#define     ConfigEnable    0x00000000

#ifdef __cplusplus
 extern "C" {
#endif



#include "MG32x02z_DRV.h"



/** @struct I2C_Init_TypeDef
  *  @brief  I2C Base Init structure definition
  */
typedef struct{
    uint32_t    I2C_INT;    /*!< I2C interrupt enable */
    uint32_t    I2C_CLK;    /*!< I2C clock source */
    uint32_t    I2C_CR0;    /*!< I2C control */
    uint32_t    I2C_CR1;    /*!< I2C control 1 */
    uint32_t    I2C_TMOUT;  /*!< I2C I2C timeout control */
    uint32_t    I2C_SADR;   /*!< I2C I2C slave address detect */
}I2C_Init_TypeDef;



DRV_Return I2C_Init(I2C_Struct *I2Cx, I2C_Init_TypeDef *I2C_InitStruct);
void I2C0_Init(void);
void I2C1_Init(void);

#ifdef __cplusplus
}
#endif

#endif

