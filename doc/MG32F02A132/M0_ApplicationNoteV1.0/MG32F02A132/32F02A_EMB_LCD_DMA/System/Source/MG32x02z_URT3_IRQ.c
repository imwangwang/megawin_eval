




/**
 ******************************************************************************
 *
 * @file        MG32x02z_URT3_IRQ.c
 * @brief       The demo URT IRQ C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 * 
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */







#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_URT_DRV.H"
#include "MG32x02z_GPIO_DRV.H"


#define URTX   URT3                           /*!< Deal with URT3 IRQ*/


/**
 *******************************************************************************
 * @brief	    URT3 interrupt function.
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
 __weak void URT3_IRQ(void)
 {
     uint32_t URT_Flag;
     uint32_t URT_IT_Msk;
     uint32_t data;
     
     
     URT_Flag = URT_GetITAllFlagStatus(URTX);
     URT_IT_Msk = URT_GetITStatus(URTX);
     
     if((URT_Flag & URT_STA_UGF_mask_w) && (URT_IT_Msk & URT_INT_UG_IE_mask_w))
     {
        
         if((URT_Flag & URT_STA_SADRF_mask_w) && (URT_IT_Msk & URT_INT_SADR_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URTX,URT_IT_SADR);
         }
         if((URT_Flag & URT_STA_BRTF_mask_w) && (URT_IT_Msk & URT_INT_BRT_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URTX,URT_IT_BRT);
         }
         if((URT_Flag & URT_STA_TMOF_mask_w) && (URT_IT_Msk & URT_INT_TMO_IE_mask_w))
         {
             // To do ......
             
             URT_ClearITFlag(URTX,URT_IT_TMO);
         }
         URT_ClearITFlag(URTX,URT_IT_UG);
     }
     if((URT_Flag & URT_STA_RXF_mask_w) && (URT_IT_Msk & URT_INT_RX_IE_mask_w))
     {
         data = URT_GetRXData(URTX);
         URT_SetTXData(URTX, 1, data);
         // To do ......
         
         URT_ClearITFlag(URTX,URT_IT_RX);
     }
     if((URT_Flag & URT_STA_TXF_mask_w) && (URT_IT_Msk & URT_INT_TX_IE_mask_w))
     {
         // To do ......
         
         URT_ClearITFlag(URTX,URT_IT_TX);
     }
     if((URT_Flag & URT_STA_LSF_mask_w) && (URT_IT_Msk & URT_INT_LS_IE_mask_w))
     {
         if((URT_Flag & URT_STA_IDLF_mask_w) && (URT_IT_Msk & URT_INT_IDL_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URTX,URT_IT_IDL);
         }
         if((URT_Flag & URT_STA_CTSF_mask_w) && (URT_IT_Msk & URT_INT_CTS_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URTX,URT_IT_CTS);
         }
         URT_ClearITFlag(URTX,URT_IT_LS);
     }
     if((URT_Flag & URT_STA_ERRF_mask_w) && (URT_IT_Msk & URT_INT_ERR_IE_mask_w))
     {
         
         if((URT_Flag & URT_STA_PEF_mask_w) && (URT_IT_Msk & URT_INT_PE_IE_mask_w))
         {
             //To do......
            
             URT_ClearITFlag(URTX,URT_IT_PE);
         }
         if((URT_Flag & URT_STA_ROVRF_mask_w) && (URT_IT_Msk & URT_INT_ROVR_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URTX,URT_IT_ROVR);
         }
         if((URT_Flag & URT_STA_RXTMOF_mask_w) && (URT_IT_Msk & URT_INT_RXTMO_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URTX,URT_IT_RXTMO);
         }
         if((URT_Flag & URT_STA_IDTMOF_mask_w) && (URT_IT_Msk & URT_INT_IDTMO_IE_mask_w))
         {
             //To do......
             
             URT_ClearITFlag(URTX,URT_IT_IDTMO);
         }

     }
     if((URT_Flag & URT_STA_TCF_mask_w) && (URT_IT_Msk & URT_INT_TC_IE_mask_w))
     {
         //To do......
         
         URT_ClearITFlag(URTX, URT_IT_TC);
     }
}
 

 
