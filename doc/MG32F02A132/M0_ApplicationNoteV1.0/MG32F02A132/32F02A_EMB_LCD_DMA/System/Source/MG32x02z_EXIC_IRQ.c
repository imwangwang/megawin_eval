



/**
 ******************************************************************************
 *
 * @file        MG32x02z_EXIC_IRQ.c
 * @brief       The demo EXIC IRQ C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */




#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_EXIC_DRV.H"





/**
 *******************************************************************************
 * @brief	    EXIC interrupt function.
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
__weak void EXINT0_IRQ(void)
{
    if(EXIC_GetPxTriggerAndITFlagStatus(EXIC_PA_ITF)==DRV_Happened)
    {
        // To do...
        
        
        EXIC_ClearPxTriggerITFlag(EXIC_PA_ITF,EXIC_PX_AF);
    }
    if(EXIC_GetPxTriggerOrITFlagStatus(EXIC_PA_ITF)==DRV_Happened)
    {
        // To do...
       
        EXIC_ClearPxTriggerEventFlag(EXIC_PA, EXIC_PX_AllPIN );
        EXIC_ClearPxTriggerITFlag(EXIC_PA_ITF,EXIC_PX_OF);
    }
}
 
 
 
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
__weak void EXINT1_IRQ(void)
{
    if(EXIC_GetPxTriggerAndITFlagStatus(EXIC_PB_ITF)==DRV_Happened)
    {
        // To do...
        
        
        EXIC_ClearPxTriggerITFlag(EXIC_PB_ITF,EXIC_PX_AF);
    }
    if(EXIC_GetPxTriggerOrITFlagStatus(EXIC_PB_ITF)==DRV_Happened)
    {
        // To do...
       
        EXIC_ClearPxTriggerEventFlag(EXIC_PB, EXIC_PX_AllPIN );
        EXIC_ClearPxTriggerITFlag(EXIC_PB_ITF,EXIC_PX_OF);
    }   
    
    
}
 
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
__weak void EXINT2_IRQ(void)
{
    if(EXIC_GetPxTriggerAndITFlagStatus(EXIC_PC_ITF)==DRV_Happened)
    {
        // To do...
        
        
        EXIC_ClearPxTriggerITFlag(EXIC_PC_ITF,EXIC_PX_AF);
    }
    if(EXIC_GetPxTriggerOrITFlagStatus(EXIC_PC_ITF)==DRV_Happened)
    {
        // To do...
       
        EXIC_ClearPxTriggerEventFlag(EXIC_PC, EXIC_PX_AllPIN );
        EXIC_ClearPxTriggerITFlag(EXIC_PC_ITF,EXIC_PX_OF);
    }  
    
    
}
 
 
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
__weak void EXINT3_IRQ(void)
{
    if(EXIC_GetPxTriggerAndITFlagStatus(EXIC_PD_ITF)==DRV_Happened)
    {
        // To do...
        
        
        EXIC_ClearPxTriggerITFlag(EXIC_PD_ITF,EXIC_PX_AF);
    }
    if(EXIC_GetPxTriggerOrITFlagStatus(EXIC_PD_ITF)==DRV_Happened)
    {
        // To do...
       
        EXIC_ClearPxTriggerEventFlag(EXIC_PD, EXIC_PX_AllPIN );
        EXIC_ClearPxTriggerITFlag(EXIC_PD_ITF,EXIC_PX_OF);
    }   
}


