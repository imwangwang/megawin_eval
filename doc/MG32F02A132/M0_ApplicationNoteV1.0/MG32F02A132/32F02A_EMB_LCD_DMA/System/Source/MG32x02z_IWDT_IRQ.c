/**
 ******************************************************************************
 *
 * @file        MG32x02z_IWDT_IRQ.c
 *
 * @brief       The demo code IWDT Interrupt Request C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 *******************************************************************************
 */
 
#include "MG32x02z_IWDT_DRV.h"


/**
 *******************************************************************************
 * @brief  	    IWDT module IRQ
 * @details  
 * @return	    
 * @note
 * @bug              
 *******************************************************************************
 */
__weak  void IWDT_IRQ (void)
{
    while((IWDT_GetAllFlagStatus() & IWDT_ALLF) != 0)
    {
        if(IWDT_GetSingleFlagStatus(IWDT_EW1F) == DRV_Happened)
        {
            // When early wake up 1 event happened.
            // To do...
            IWDT_ClearFlag(IWDT_EW1F);
            IWDT_RefreshCounter();
        }
        
        if(IWDT_GetSingleFlagStatus(IWDT_EW0F) == DRV_Happened)
        {
            // When early wake up 0 event happened.
            // To do...
            IWDT_ClearFlag(IWDT_EW0F);
            IWDT_RefreshCounter();
        }

        if(IWDT_GetSingleFlagStatus(IWDT_TF) == DRV_Happened)
        {
            // When timer timeout event happened.
            // To do...
            IWDT_ClearFlag(IWDT_TF);
            IWDT_RefreshCounter();
        }
    }
}



