

/**
 ******************************************************************************
 *
 * @file        MG32x02z__ChipInit.c
 * @brief       The demo ChipInit C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 * 
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */

#include "MG32x02z_ChipInit.h"
#include "MG32x02z_CSC_Init.h"
#include "MG32x02z_PW_Init.h"
#include "MG32x02z_RST_Init.h"
#include "MG32x02z_GPIO_Init.h"

/*
 *******************************************************************************
 * @brief	    Chip initial.  
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void ChipInit(void)
{

    PW_Init();
    CSC_Init((uint32_t*) &ON_CSC_InitConfig);
    RST_Init();
    GPIO_Init();
    
    //===Select initial outside Power & CSC & RST ====
    // From MG32x02z_ChipInit.h to select you need initial
    
    #if EXIC_Initial_En == 1
        EXIC_Init();
    #endif
    #if I2C0_Initial_En == 1
        I2C0_Init();
    #endif 
    #if I2C1_Initial_En == 1
        I2C1_Init();
    #endif 
    #if EMB_Initial_En == 1
        EMB_Initial();
    #endif
    #if MEME_Initial_En == 1
        MEM_Init();
    #endif
    #if GPL_Initial_En == 1
        GPL_Init();
    #endif
    #if URT_Initial_En == 1
        URT_Init();
    #endif
    #if IRQ_Initial_En == 1
        IRQ_Init();
    #endif
}









