



/**
 ******************************************************************************
 *
 * @file        MG32x02z_URT_Init.c
 * @brief       The URT Init C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/05/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *   
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 *******************************************************************************
 */

#include "MG32x02z_ChipInit.h"

#if URT_Initial_En == 1

#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_URT_Init.h"



/**
 *******************************************************************************
 * @brief	    URTx N-8-1 mode inital.
 * @details     
 * @param[in]   URTX:
 *  @arg\b      URT0: URT0 relationship control.
 *  @arg\b      URT1: URT1 relationship control.
 *  @arg\b      URT2: URT2 relationship control.
 *  @arg\b      URT3: URT3 relationship control.
 * @param[in]   BaudRate: The URTX baudrate value.
 * @return      URTx inital is whether pass or not.
 * @exception   No
 * @note        The URT initial is N-8-1.
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
DRV_Return URT_Config(URT_Struct* URTX , uint32_t URT_Freq, uint32_t BaudRate)
{
    uint32_t  TRXOverSamplingSampleNumber;
    int8_t   i,j,k;
    uint32_t Tmp;
	 
    //==========================================
    //Baudrate
    Tmp = URT_Freq / BaudRate;
    if(Tmp<6)
    {
        return(DRV_Failure);
    }
	//================TX and RX oversamplig value===================
	j = 0;
	for(i=6;i<32;i++)
	{
		k = Tmp % i;
		if(k==0)
		{
			TRXOverSamplingSampleNumber = i;
			break;
		}
		else
		{
			if((i-k)>j || (i-k)==j)
			{
				j = i - k;
				TRXOverSamplingSampleNumber = i;
			}
		}
	}
    
	URTX->CLK.W = URTX->CLK.W & (~(URT_CLK_TX_CKS_mask_w| URT_CLK_RX_CKS_mask_w | URT_CLK_CK_SEL_mask_w));
	Tmp = Tmp / (TRXOverSamplingSampleNumber);
    if(Tmp>4096)
    {
        return(DRV_Failure);
    }
    URTX->RLR.H[0] = (Tmp - 1);
    
    
    URTX->CR0.W = URTX->CR0.W & (~URT_CR0_OS_MDS_mask_w);
    TRXOverSamplingSampleNumber = TRXOverSamplingSampleNumber - 1;
    URTX->CR1.B[3] = TRXOverSamplingSampleNumber;
    URTX->CR1.B[1] = TRXOverSamplingSampleNumber;
    
    URTX->CLK.W = URTX->CLK.W | (URT_CLK_BR_MDS_mask_w | URT_CLK_BR_EN_mask_w);
    
    
    //===========================
    //1. Data is 8bit , Data order is LSB , no parity bit , stop bit is 1bit 
    //2. Data bit no inverse
    URTX->CR1.B[2] = (URT_DataLength_8 | URT_DataTyped_LSB | URT_Parity_No | URT_StopBits_1_0);
    URTX->CR1.B[0] = (URT_DataLength_8 | URT_DataTyped_LSB | URT_Parity_No | URT_StopBits_1_0);
    URTX->CR4.B[0] = URTX->CR4.B[0] & (~(URT_CR4_RDAT_INV_mask_b0|URT_CR4_TDAT_INV_mask_b0));
    
    
    //=============================
    //1. Set URT mode
    //2. Data line is 2 line
    //3. RX shadow buffer level is 1byte.
    URTX->CR0.W = (URTX->CR0.W & (~ (URT_CR0_MDS_mask_w | URT_CR0_DAT_LINE_mask_w | URT_CR0_RX_TH_mask_w)));
    
    //============================
    //1. Enable RX & TX
    //2. Enable URT 
    URTX->CR2.W = URTX->CR2.W | (URT_CR2_TX_EN_mask_w | URT_CR2_RX_EN_mask_w);                                   
    URTX->CR0.W = URTX->CR0.W | URT_CR0_EN_mask_w;                                                              
    
    
    return(DRV_Success);
}

/**
 *******************************************************************************
 * @brief	    Control URTX N-8-1 mode inital.
 * @details     
 * @return      URTx inital is whether pass or not.
 * @exception   No
 * @note        The URT initial is N-8-1.
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
DRV_Return URT_Init(void)
{
    DRV_Return Result = DRV_Success;
    
    #if URT0_INIT_EN == 1
        if(URT_Config(URT0,URT0_Freq,URT0_INIT_BD)!=DRV_Success)             // Enable URT0 and baudrate is URT0_INIT_BD ( In MG32x02z_URT_Init.h)
        {
            Result = DRV_Failure;
        }
    #endif
    #if URT1_INIT_EN == 1
        if(URT_Config(URT1,URT1_Freq,URT1_INIT_BD)!=DRV_Success)             // Enable URT1 and baudrate is URT1_INIT_BD ( In MG32x02z_URT_Init.h)
        {
            Result = DRV_Failure;
        }
    #endif
    #if URT2_INIT_EN == 1
        if(URT_Config(URT2,URT2_Freq,URT2_INIT_BD)!=DRV_Success)             // Enable URT2 and baudrate is URT2_INIT_BD ( In MG32x02z_URT_Init.h)
        {
            Result = DRV_Failure;
        }
    #endif
    #if URT3_INIT_EN == 1
        if(URT_Config(URT3,URT3_Freq,URT3_INIT_BD)!=DRV_Success)             // Enable URT3 and baudrate is URT3_INIT_BD ( In MG32x02z_URT_Init.h)
        {
            Result = DRV_Failure;
        }
    #endif
    
    return(Result);
}


#endif






