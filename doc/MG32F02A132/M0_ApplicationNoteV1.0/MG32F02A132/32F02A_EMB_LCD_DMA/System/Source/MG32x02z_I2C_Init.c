
/**
 ******************************************************************************
 *
 * @file        MG32x02z_I2C_Init.c
 * @brief       The I2C Init C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 * 
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */

#include "MG32x02z_ChipInit.h"

#if I2C0_Initial_En == 1 || I2C1_Initial_En == 1


#include "MG32x02z_I2C_Init.h"



#if I2C0_Config0_Enable
uint32_t const I2C0_Config0[] = {
    I2C0_Config0_INT,                                               //INT
    I2C0_Config0_CLK,                                               //CLK
    I2C0_Config0_CR0,                                               //CR0
    I2C0_Config0_CR1,                                               //CR1
    I2C0_Config0_TMO,                                               //TMOUT
    I2C0_Config0_SADR,                                              //SADR
};
#endif

#if I2C1_Config0_Enable
uint32_t const I2C1_Config0[] = {
    I2C1_Config0_INT,                                               //INT
    I2C1_Config0_CLK,                                               //CLK
    I2C1_Config0_CR0,                                               //CR0
    I2C1_Config0_CR1,                                               //CR1
    I2C1_Config0_TMO,                                               //TMOUT
    I2C1_Config0_SADR,                                              //SADR
};
#endif



DRV_Return I2C_Init(I2C_Struct *I2Cx, I2C_Init_TypeDef *I2C_InitStruct)
{
    I2Cx->CR0.W &= 0xFFFFFFFE;
    
    if(I2Cx == I2C0)
    {
        // Disable NVIC
        NVIC_DisableIRQ(I2C0_IRQn);

        // CSC Config
        UnProtectModuleReg(CSCprotect);
        CSC->APB0.MBIT.I2C0_EN = 1;
        ProtectModuleReg(CSCprotect);

        // I2C1 SoftWare Trigger Hardware Reset
        UnProtectModuleReg(RSTprotect);
        RST->APB0.MBIT.I2C0_EN = 1;
        __NOP();
        RST->APB0.MBIT.I2C0_EN = 0;
        ProtectModuleReg(RSTprotect);

        // Clear Flag
        I2Cx->STA.W = I2C_STA_BUFF_mask_w;


        // NVIC Config
        if((I2C_InitStruct->I2C_INT & I2C_IT_IEA) != 0)
            NVIC_EnableIRQ(I2C0_IRQn);
    }

    if(I2Cx == I2C1)
    {
        NVIC_DisableIRQ(I2Cx_IRQn);
        
        // CSC Config
        UnProtectModuleReg(CSCprotect);
        CSC->APB0.MBIT.I2C1_EN = 1;
        ProtectModuleReg(CSCprotect);

        // I2C1 SoftWare Trigger Hardware Reset
        UnProtectModuleReg(RSTprotect);
        RST->APB0.MBIT.I2C1_EN = 1;
        __NOP();
        RST->APB0.MBIT.I2C1_EN = 0;
        ProtectModuleReg(RSTprotect);

        // Clear Flag
        I2Cx->STA.W = I2C_STA_BUFF_mask_w;



        // NVIC Config
        if((I2C_InitStruct->I2C_INT & I2C_IT_IEA) != 0)
            NVIC_EnableIRQ(I2Cx_IRQn);
    }

    // Clock Config
    I2Cx->CR1.W = I2C_InitStruct->I2C_CR1;

    // Mode Config
    I2Cx->CR0.W = (I2C_InitStruct->I2C_CR0 & 0xFFFFFFFE);

    // Slave Address Config
    I2Cx->SADR.W = I2C_InitStruct->I2C_SADR;
    
    // Timeout config
    I2Cx->TMOUT.W = I2C_InitStruct->I2C_TMOUT;

    // Interrupt Config
    I2Cx->INT.W = I2C_InitStruct->I2C_INT;

    // Enable
    if((I2C_InitStruct->I2C_CR0 & 0x00000001) != 0)
        I2Cx->CR0.W |= 0x00000001;
    
    return DRV_Success;
}



void I2C0_Init(void)
{
 // DRV_Return I2C_Init(I2C_Struct *I2Cx, I2C_Init_TypeDef *I2C_InitStruct)
    I2C_Init(I2C0, ((I2C_Init_TypeDef *)(&I2C0_Config0)));
}



void I2C1_Init(void)
{
 // DRV_Return I2C_Init(I2C_Struct *I2Cx, I2C_Init_TypeDef *I2C_InitStruct)
    I2C_Init(I2C1, ((I2C_Init_TypeDef *)(&I2C1_Config0)));
}


#endif
