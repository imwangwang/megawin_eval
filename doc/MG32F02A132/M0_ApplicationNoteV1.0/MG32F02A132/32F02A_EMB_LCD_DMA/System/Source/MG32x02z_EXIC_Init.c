

/**
 ******************************************************************************
 *
 * @file        MG32x02z_EXIC_Init.c
 * @brief       The EXIC Init C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */



#include "MG32x02z_ChipInit.h"

#if EXIC_Initial_En ==  1

#include "MG32x02z__Common_DRV.h"
#include "MG32x02z_EXIC_Init.h"

#define EXIC_Px_MSK_AM_mask_w       0xFFFF0000                              /*!<Detect And Mask whether be setted or not */


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */

void EXIC_Init(void)
{   
    #if MG32x02z_ExicPAInit_EN == 1
        #if MG32x02z_ExicPAInit_Mask !=0
            EXIC->PA_TRGS.W =  ((MG32x02z_ExicPAInit_Mask | (MG32x02z_ExicPAInit_Mask >> 1))  &  MG32x02z_ExicPAInit_Mode);  
            #if MG32x02z_ExicPAInit_AINV == 1 &&   ((MG32x02z_ExicPAInit_Mask & EXIC_Px_MSK_AM_mask_w)!=0)
                EXIC_PxTriggerAndUnmatch_Cmd(EXIC_PA_IT,ENABLE)
            #endif
        #endif
        #if MG32x02z_ExicPAInit_AndOrMask !=0 
            EXIC->PA_MSK.W = MG32x02z_ExicPAInit_AndOrMask;
        #endif
    
        
        
        EXIC_ClearPxTriggerITFlag(EXIC_PA_ITF,(EXIC_PX_AF|EXIC_PX_OF));   
        EXIC_ClearPxTriggerEventFlag(EXIC_PA, 0xFFFF); 
        #if MG32x02z_ExicPAInit_INT == 1
            EXIC_PxTriggerITEA_Cmd(EXIC_PA_IT,ENABLE);
            NVIC_EnableIRQ(EXINT0_IRQn);
        #else
            EXIC_PxTriggerITEA_Cmd(EXIC_PA_IT,DISABLE);
            NVIC_DisableIRQ(EXINT0_IRQn);
        #endif    
    #endif
        
    #if MG32x02z_ExicPBInit_EN == 1
        
        #if MG32x02z_ExicPBInit_Mask !=0
            EXIC->PB_TRGS.W =  ((MG32x02z_ExicPBInit_Mask | (MG32x02z_ExicPBInit_Mask >> 1))  &  MG32x02z_ExicPBInit_Mode);
            #if MG32x02z_ExicPBInit_AINV == 1 &&   ((MG32x02z_ExicPBInit_Mask & EXIC_Px_MSK_AM_mask_w)!=0)
                EXIC_PxTriggerAndUnmatch_Cmd(EXIC_PB_IT,ENABLE)
            #endif                  
        #endif
        #if MG32x02z_ExicPBInit_AndOrMask !=0 
            EXIC->PB_MSK.W = MG32x02z_ExicPBInit_AndOrMask;
        #endif

        
        
        EXIC_ClearPxTriggerITFlag(EXIC_PB_ITF,(EXIC_PX_AF|EXIC_PX_OF));   
        EXIC_ClearPxTriggerEventFlag(EXIC_PB, 0xFFFF);   
        #if MG32x02z_ExicPBInit_INT == 1
            EXIC_PxTriggerITEA_Cmd(EXIC_PB_IT,ENABLE);
            NVIC_EnableIRQ(EXINT1_IRQn);
        #else 
            EXIC_PxTriggerITEA_Cmd(EXIC_PB_IT,DISABLE);
            NVIC_DisableIRQ(EXINT1_IRQn);
        #endif          
    #endif
    
    #if MG32x02z_ExicPCInit_EN == 1
        #if MG32x02z_ExicPCInit_Mask !=0
            EXIC->PC_TRGS.W =  ((MG32x02z_ExicPCInit_Mask | (MG32x02z_ExicPCInit_Mask >> 1))  &  MG32x02z_ExicPCInit_Mode);
            #if MG32x02z_ExicPCInit_AINV == 1 &&   ((MG32x02z_ExicPCInit_Mask & EXIC_Px_MSK_AM_mask_w)!=0)
                EXIC_PxTriggerAndUnmatch_Cmd(EXIC_PC_IT,ENABLE)
            #endif        
        #endif
        #if MG32x02z_ExicPCInit_AndOrMask !=0 
            EXIC->PC_MSK.W = MG32x02z_ExicPCInit_AndOrMask;
        #endif
        
        
        EXIC_ClearPxTriggerITFlag(EXIC_PC_ITF,(EXIC_PX_AF|EXIC_PX_OF));   
        EXIC_ClearPxTriggerEventFlag(EXIC_PC, 0xFFFF);  
        #if MG32x02z_ExicPCInit_INT == 1
            EXIC_PxTriggerITEA_Cmd(EXIC_PC_IT,ENABLE);
            NVIC_EnableIRQ(EXINT2_IRQn);
        #else
            EXIC_PxTriggerITEA_Cmd(EXIC_PC_IT,DISABLE);
            NVIC_DisableIRQ(EXINT2_IRQn);
        #endif                
    #endif    
    
    
    #if MG32x02z_ExicPDInit_EN == 1
        #if MG32x02z_ExicPDInit_Mask !=0
            EXIC->PD_TRGS.W =  ((MG32x02z_ExicPDInit_Mask | (MG32x02z_ExicPDInit_Mask >> 1))  &  MG32x02z_ExicPDInit_Mode);  
            #if MG32x02z_ExicPDInit_AINV == 1 &&   ((MG32x02z_ExicPDInit_Mask & EXIC_Px_MSK_AM_mask_w)!=0)
                EXIC_PxTriggerAndUnmatch_Cmd(EXIC_PD_IT,ENABLE)
            #endif
        #endif
        #if MG32x02z_ExicPDInit_AndOrMask !=0 
            EXIC->PD_MSK.W = MG32x02z_ExicPDInit_AndOrMask;
        #endif

        EXIC_ClearPxTriggerITFlag(EXIC_PD_ITF,(EXIC_PX_AF|EXIC_PX_OF));   
        EXIC_ClearPxTriggerEventFlag(EXIC_PD, 0xFFFF);
        #if MG32x02z_ExicPDInit_INT == 1
            EXIC_PxTriggerITEA_Cmd(EXIC_PD_IT,ENABLE);
            NVIC_EnableIRQ(EXINT3_IRQn);
        #else
            EXIC_PxTriggerITEA_Cmd(EXIC_PD_IT,DISABLE);
            NVIC_DisableIRQ(EXINT3_IRQn);
        #endif         
                
    #endif    
}


#endif








