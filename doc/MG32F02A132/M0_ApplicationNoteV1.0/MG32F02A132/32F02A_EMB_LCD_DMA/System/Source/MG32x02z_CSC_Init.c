/**
 *******************************************************************************
 *
 * @file        MG32x02z_CSC_Init.c
 *
 * @brief       The PW initial code C file
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.12
 * @date        2019/01/03
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 *******************************************************************************
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 *******************************************************************************
 */
 

#include "MG32x02z_CSC_Init.h"


/**
 *******************************************************************************
 * @brief  	    initialized CSC module.
 * @details  
 * @param[in]   CSC_CFG:
 * 	@arg\b			Use keil wizard define table index..
 * @return	    	
 * @note
 * @par         Example
 * @code
    CSC_Init ((uint32_t*) &CSC_InitConfig);
 * @endcode
 * @bug              
 *******************************************************************************
 */
void CSC_Init (uint32_t* CSC_CFG)
{
    CSC_CFG_Struct CSC_SET = {0};

    
    CSC_SET.PLL.W = CSC_CFG[2];         // Reload config data
    CSC_SET.CR0.W = CSC_CFG[4];
    CSC_SET.DIV.W = CSC_CFG[5];
    
    UnProtectModuleReg(MEMprotect);     // Setting flash wait state
    MEM_SetFlashWaitState(CSC_CFG[19]);
    ProtectModuleReg(MEMprotect);
    
    UnProtectModuleReg(CSCprotect);
    CSC->DIV.MBIT.APB_DIV = CSC_SET.DIV.MBIT.APB_DIV;   // Modify CK_APB divider
    CSC->DIV.MBIT.AHB_DIV = CSC_SET.DIV.MBIT.AHB_DIV;   // Modify CK_AHB divider
    
    CSC->CR0.MBIT.MCD_SEL = CSC_SET.CR0.MBIT.MCD_SEL;   // Missing Clock Detect configure
    CSC->CR0.MBIT.MCD_DIS = CSC_SET.CR0.MBIT.MCD_DIS;   // Setting MCD function
    
/* CK_HS selection */
    if(CSC_SET.CR0.MBIT.HS_SEL == 0)    // When CSC_SET CK_HS select IHRCO
    {
        CSC->CR0.MBIT.IHRCO_SEL = CSC_SET.CR0.MBIT.IHRCO_SEL;
        if(CSC->CR0.MBIT.IHRCO_EN == 0)
        {
            CSC_IHRCO_Cmd(ENABLE);
            while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
            CSC_ClearFlag(CSC_IHRCOF);
        } 
    }
    if(CSC_SET.CR0.MBIT.HS_SEL == 1)    // When CSC_SET CK_HS select XOSC
    {
        CSC->PLL.MBIT.XOSC_GN = CSC_SET.PLL.MBIT.XOSC_GN;
        CSC_PeriphOnModeClock_Config(CSC_ON_PortC, ENABLE);
        GPIO_PinFunction_Select(PINC(13),1);                        // Enable XOSC
        GPIO_PinFunction_Select(PINC(14),1);

        while(CSC_GetSingleFlagStatus(CSC_XOSCF) == DRV_Normal);
        CSC_ClearFlag(CSC_XOSCF);
        
//        CSC->CR0.MBIT.MCD_SEL = CSC_SET.CR0.MBIT.MCD_SEL;           // Missing Clock Detect configure
//        CSC->CR0.MBIT.MCD_DIS = CSC_SET.CR0.MBIT.MCD_DIS;           // Setting MCD function
    }
    if(CSC_SET.CR0.MBIT.HS_SEL == 2)    // When CSC_SET CK_HS select ILRCO
    {
        while(CSC_GetSingleFlagStatus(CSC_ILRCOF) == DRV_Normal);
        CSC_ClearFlag(CSC_ILRCOF);
    }
    if(CSC_SET.CR0.MBIT.HS_SEL == 3)    // When CSC_SET CK_HS select CK_EXT
    {
        CSC_PeriphOnModeClock_Config(CSC_ON_PortC, ENABLE);
        GPIO_PinFunction_Select(PINC(13),0);                        // Disable XOSC
        GPIO_PinFunction_Select(PINC(14),0);
    }
    /* Delsy 4 ILROC clock */
    CSC->CR0.MBIT.HS_SEL = CSC_SET.CR0.MBIT.HS_SEL;         // Set CK_HS


/* CK_LS */    
    if(CSC_SET.CR0.MBIT.LS_SEL == 1)    // When CSC_SET CK_LS select XOSC
    {
        if(CSC->CR0.MBIT.HS_SEL != 3)
        {
            if((PINC(13)->CR.MBIT.AFS != 1) &(PINC(14)->CR.MBIT.AFS != 1))  // if OSC is disalbe
            {
                CSC->PLL.MBIT.XOSC_GN = CSC_SET.PLL.MBIT.XOSC_GN;
                CSC_PeriphOnModeClock_Config(CSC_ON_PortC, ENABLE);
                GPIO_PinFunction_Select(PINC(13),1);                        // Enable XOSC
                GPIO_PinFunction_Select(PINC(14),1);

                while(CSC_GetSingleFlagStatus(CSC_XOSCF) == DRV_Normal);
                CSC_ClearFlag(CSC_XOSCF);
                
//                CSC->CR0.MBIT.MCD_SEL = CSC_SET.CR0.MBIT.MCD_SEL;           // Missing Clock Detect configure
//                CSC->CR0.MBIT.MCD_DIS = CSC_SET.CR0.MBIT.MCD_DIS;           // Setting MCD function
            }
        }
    }
    if(CSC_SET.CR0.MBIT.LS_SEL == 3)    // When CSC_SET CK_LS select CK_EXT
    {
        if(CSC->CR0.MBIT.HS_SEL != 1)
        {
            CSC_PeriphOnModeClock_Config(CSC_ON_PortC, ENABLE);
            GPIO_PinFunction_Select(PINC(13),0);                            // Disable XOSC
            GPIO_PinFunction_Select(PINC(14),0);
        }
    }
    CSC->CR0.MBIT.LS_SEL = CSC_SET.CR0.MBIT.LS_SEL;         // Set CK_LS
    
    
/* CK_MAIN */ 
    CSC->DIV.MBIT.PLLI_DIV = CSC_SET.DIV.MBIT.PLLI_DIV;  
    if(CSC_SET.CR0.MBIT.MAIN_SEL == 2)      // When CSC_SET SYS_SEL select CK_PLLO
    {
        CSC->PLL.MBIT.PLL_MUL = CSC_SET.PLL.MBIT.PLL_MUL;
        CSC->DIV.MBIT.PLLO_DIV = CSC_SET.DIV.MBIT.PLLO_DIV; 
        CSC->CR0.MBIT.PLL_EN = 1;
        while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
        CSC_ClearFlag(CSC_PLLF);
    }
    
    CSC->CR0.MBIT.MAIN_SEL = CSC_SET.CR0.MBIT.MAIN_SEL;     // Set CK_MAIN

    CSC->CR0.MBIT.ST_SEL= CSC_SET.CR0.MBIT.ST_SEL;          // Set CK_ST
    CSC->DIV.W = CSC_SET.DIV.W;

// Configure ICKO function
    CSC->INT.W = CSC_CFG[1];
    CSC->CKO.W = CSC_CFG[6];
    
// Configure peripheral clock 
    CSC->AHB.W = CSC_CFG[7];
    CSC->APB0.W = CSC_CFG[8];
    CSC->APB1.W = CSC_CFG[9];
    CSC->SLP0.W = CSC_CFG[10];
    CSC->SLP1.W = CSC_CFG[11];
    CSC->STP0.W = CSC_CFG[12];
    CSC->CKS0.W = CSC_CFG[14];
    CSC->CKS1.W = CSC_CFG[15];
    CSC->CKS2.W = CSC_CFG[16];
    
    if(CSC_SET.CR0.MBIT.HS_SEL != 0)    // Disable IHRCO 
    {
        CSC_IHRCO_Cmd(DISABLE);
        CSC_ClearFlag(CSC_IHRCOF);
    }
    
    ProtectModuleReg(CSCprotect);
}






