

/**
 ******************************************************************************
 *
 * @file        MG32x02z_CMP_IRQ.c
 * @brief       The CMP Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/03/28
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */
 
 
// <<< Use Configuration Wizard in Context Menu >>>


// <<< end of configuration section >>>    



#include "MG32x02z_CMP_DRV.h"






/**
 *******************************************************************************
 * @brief	    Compare interrupt function.
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
__weak void CMP_IRQ(void)
{
    
    // AC0  rising / falling edge flag
    if (CMP_GetSingleFlagStatus(CMP, AC0_RisingEdge_Flag) == DRV_Happened)
    {
        // Rising edge interrupt flag 
        // To do...
        
        CMP_ClearFlag (CMP, AC0_RisingEdge_Flag);
    }
    if (CMP_GetSingleFlagStatus(CMP, AC0_FallingEdge_Flag) == DRV_Happened)
    {
        // Falling edge interrupt flag 
        // To do...
        
        CMP_ClearFlag (CMP, AC0_FallingEdge_Flag);
    }

    // AC1  rising / falling edge flag
    if (CMP_GetSingleFlagStatus(CMP, AC1_RisingEdge_Flag) == DRV_Happened)
    {
        // Rising edge interrupt flag 
        // To do...
        
        CMP_ClearFlag (CMP, AC1_RisingEdge_Flag);
    }
    if (CMP_GetSingleFlagStatus(CMP, AC1_FallingEdge_Flag) == DRV_Happened)
    {
        // Falling edge interrupt flag 
        // To do...
        
        CMP_ClearFlag (CMP, AC1_FallingEdge_Flag);
    }
    
    // AC2  rising / falling edge flag
    if (CMP_GetSingleFlagStatus(CMP, AC2_RisingEdge_Flag) == DRV_Happened)
    {
        // Rising edge interrupt flag 
        // To do...
        
        CMP_ClearFlag (CMP, AC2_RisingEdge_Flag);
    }
    if (CMP_GetSingleFlagStatus(CMP, AC2_FallingEdge_Flag) == DRV_Happened)
    {
        // Falling edge interrupt flag 
        // To do...
        
        CMP_ClearFlag (CMP, AC2_FallingEdge_Flag);
    }
    
    // AC3  rising / falling edge flag
    if (CMP_GetSingleFlagStatus(CMP, AC3_RisingEdge_Flag) == DRV_Happened)
    {
        // Rising edge interrupt flag 
        // To do...
        
        CMP_ClearFlag (CMP, AC3_RisingEdge_Flag);
    }
    if (CMP_GetSingleFlagStatus(CMP, AC3_FallingEdge_Flag) == DRV_Happened)
    {
        // Falling edge interrupt flag 
        // To do...
        
        CMP_ClearFlag (CMP, AC3_FallingEdge_Flag);
    }
  

}



