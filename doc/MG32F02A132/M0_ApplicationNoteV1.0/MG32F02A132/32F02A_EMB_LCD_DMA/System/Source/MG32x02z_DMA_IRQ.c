

/**
 ******************************************************************************
 *
 * @file        MG32x02z_DMA_IRQ.c
 * @brief       The DMA Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/03/28
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */
 
 
// <<< Use Configuration Wizard in Context Menu >>>


// <<< end of configuration section >>>    



#include "MG32x02z_DMA_DRV.h"






/**
 *******************************************************************************
 * @brief	    DMA interrupt function.
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
__weak void DMA_IRQ(void)
{
    
    // DMA channel-0 global interrupt flag
    if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_GIF) == DRV_Happened)
    {
        // DMA channel-0 transfer complete flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_TCF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
        }
        // DMA channel-0 transfer half flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_THF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH0_THF);
        }
        // DMA channel-0 transfer error flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_ERRF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH0_ERRF);
        }
    }

    // DMA channel-1 global interrupt flag
    if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH1_GIF) == DRV_Happened)
    {
        // DMA channel-1 transfer complete flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH1_TCF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH1_TCF);
        }
        // DMA channel-1 transfer half flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH1_THF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH1_THF);
        }
        // DMA channel-1 transfer error flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH1_ERRF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH1_ERRF);
        }
    }

    // DMA channel-2 global interrupt flag
    if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH2_GIF) == DRV_Happened)
    {
        // DMA channel-2 transfer complete flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH2_TCF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH2_TCF);
        }
        // DMA channel-2 transfer half flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH2_THF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH2_THF);
        }
        // DMA channel-2 transfer error flag
        if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH2_ERRF) == DRV_Happened)
        {
            // To do...
            
            DMA_ClearFlag(DMA, DMA_FLAG_CH2_ERRF);
        }
    }
    // DMA GPL selection conflict error fla
    if(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_GPL_CEF) == DRV_Happened)
    {
        // To do...
            
        DMA_ClearFlag(DMA, DMA_FLAG_GPL_CEF);
    }
    
    

}



