

/**
 ******************************************************************************
 *
 * @file        MG32x02z_DAC_IRQ.c
 * @brief       The DAC Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/03/28
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */
 
 
// <<< Use Configuration Wizard in Context Menu >>>


// <<< end of configuration section >>>    



#include "MG32x02z_DAC_DRV.h"






/**
 *******************************************************************************
 * @brief	    DAC interrupt function.
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
__weak void DAC_IRQ(void)
{
    
    // DAC ready flag to update new data to data register
    if (DAC_GetAllFlagStatus(DAC) & Ready_Flag)
    {
        // To do...
        
        DAC_ClearFlag(DAC,Ready_Flag);
    }
    // DAC conversion underrun event flag
    if (DAC_GetAllFlagStatus(DAC) & Underrun_Flag)
    {
        // To do...
        
        DAC_ClearFlag(DAC,Underrun_Flag);
    }
    

}



