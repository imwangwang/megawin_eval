

/**
 ******************************************************************************
 *
 * @file        MG32x02z_MEM_IRQ.c
 * @brief       The MEM Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2016/10/25
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 *******************************************************************************
 */

#include "MG32x02z_MEM_IRQ.h"



/**
 *******************************************************************************
 * @brief	    MEM_IRQ
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
                MEM_IRQ();
 * @endcode     
 * @par         Modify
 *              __weak void MEM_IRQ(void)
 * @bug                 
 *******************************************************************************
 */
__weak void MEM_IRQ(void)
{


}

