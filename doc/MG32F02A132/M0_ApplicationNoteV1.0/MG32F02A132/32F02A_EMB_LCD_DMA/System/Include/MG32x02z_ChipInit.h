
/**
 ******************************************************************************
 *
 * @file        MG32x02z_ChipInit.H
 *
 * @brief       By the file select you want to function initial.
 *   
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *>>
 *--
 *--
 *>>
 *>>
 *
 @endif 
 ******************************************************************************
 */

#ifndef _MG32x02z_ChipInit_H
 
#define _MG32x02z_ChipInit_H

// <<< Use Configuration Wizard in Context Menu >>>

//<q0>EXIC Initial
#define EXIC_Initial_En   0
//<h> I2C Initial
//<q0> I2C0 Initial 
//<q1> I2C1 Initial 
#define I2C0_Initial_En   0
#define I2C1_Initial_En   0
//</h>
//<q0> EMB Initial
#define EMB_Initial_En    0
//<q0> MEM_Initial
#define MEM_Initial_En    0
//<q0> GPL Initial 
#define GPL_Initial_En    0
//<q0> URT Initial
#define URT_Initial_En    0
//<q0> IRQ Initial    
#define IRQ_Initial_En    1

// <<< end of configuration section >>>    




#if EXIC_Initial_En ==  1
    #include "MG32x02z_EXIC_Init.h"
#endif
#if IRQ_Initial_En == 1 
    #include "MG32x02z_IRQ_Init.h"
#endif
#if I2C0_Initial_En == 1 || I2C1_Initial_En == 1
    #include "MG32x02z_I2C_Init.h"
#endif
#if EMB_Initial_En == 1
    #include "MG32x02z_EMB_Init.h"
#endif
#if MEM_Initial_En == 1
    #include "MG32x02z_MEM_Init.h"
#endif
#if GPL_Initial_En == 1
    #include "MG32x02z_GPL_Init.h"
#endif
#if URT_Initial_En == 1
    #include "MG32x02z_URT_Init.h"
#endif

#endif
