/**
 *******************************************************************************
 * @file        MG32x02z_CSC_Init.h
 *
 * @brief       TThe CSC initial code h file
 *
 * MG32x02z remote controller
 * @version     V1.12
 * @date        2018/03/09
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 *******************************************************************************
 */

#include "MG32x02z_CSC_DRV.h"
#include "MG32x02z_MEM_DRV.h"
#include "MG32x02z_GPIO_DRV.H"


#ifndef _MG32x02z_CSC_INIT_H
/*!< _MG32x02z_CSC_INIT_H */ 
#define _MG32x02z_CSC_INIT_H


//*** <<< Use Configuration Wizard in Context Menu >>> ***
//  <h> Configure ON Mode CSC Module

//      <o26> Enter XOSC Or External Clock Frequency 1~25000000Hz <1-25000000>
//      <i> When use CK_HS = CK_XOSC or CK_EXT, this space must be entered at the correct frequency.
    
//      <o7.18> Select IHRCO <0=> 12MHz
//                           <1=> 11.0592MHz

/*      <e17.0> Enable XOSC */
//          <o1.16..17> Select XOSC Gain <0=> Low Gain For 32 KHz
//                                       <1=> Highest Gain 4 ~ 25MHz
//                                       <2=> Lowest Gain For 32 KHz
/*      </e> */

//      <q2.4> Disable MCD(Missing Clock Detector)
//      <o8.22..23> Select Missing Clock Detection Duration <0=> 125us
//                                                          <1=> 250us
//                                                          <2=> 500us
//                                                          <3=> 1ms

//      <o4.10..11> Select CK_HS Source <0=> CK_IHRCO
//                                      <1=> CK_XOSC
//                                      <2=> CK_ILRCO
//                                      <3=> CK_EXT
//      <i> When Select CK_HS Source = CK_XOSC, Mz92G8z_GPIO_Init.h Wizard PC13 configutaion and PC14 configutaion  must disable.

/*      <e4.5> Configure PLL */
//      <h> Configure PLL
//          <o9.0..1> Select CK_PLLI Divider <0=> CK_HS/1
//                                           <1=> CK_HS/2
//                                           <2=> CK_HS/4
//                                           <3=> CK_HS/6
//          <o0.8> Select CK_PLL Multiplication factor <0=> CK_PLLIx16
//                                                     <1=> CK_PLLIx24
//          <o10.4..5> Select CK_PLLO Divider <0=> CK_PLL/4
//                                            <1=> CK_PLL/3
//                                            <2=> CK_PLL/2
//                                            <3=> CK_PLL/1
//      </h>
/*      </e> */

//      <o5.14..15> Select CK_MAIN Source <0=> CK_HS
//                                        <1=> CK_PLLI
//                                        <2=> CK_PLLO 

//      <o3.8..9> Select CK_LS Source <1=> CK_XOSC
//                                    <2=> CK_ILRCO
//                                    <3=> CK_EXT
//      <i> When Select CK_LS Source = CK_XOSC, Mz92G8z_GPIO_Init.h Wizard PC13 configutaion and PC14 configutaion  must disable.

//      <o6.16> Select CK_ST Source <0=> HCLK/8
//                                  <1=> CK_LS/2

//      <o12.16..18> Select APB Prescaler <0x00=> CK_MAIN/1
//                                        <0x01=> CK_MAIN/2
//                                        <0x02=> CK_MAIN/4
//                                        <0x03=> CK_MAIN/8
//                                        <0x04=> CK_MAIN/16

//      <o11.8..11> Select AHB Prescaler <0x00=> CK_APB/1
//                                       <0x01=> CK_APB/2
//                                       <0x02=> CK_APB/4
//                                       <0x03=> CK_APB/8
//                                       <0x04=> CK_APB/16
//                                       <0x05=> CK_APB/32
//                                       <0x06=> CK_APB/64
//                                       <0x07=> CK_APB/128
//                                       <0x08=> CK_APB/256
//                                       <0x09=> CK_APB/512

//      <o13.26..27> Select CK_UT Divider <0x01=> ILRCO/8
//                                       <0x00=> ILRCO/32
//                                       <0x02=> ILRCO/16
//                                       <0x03=> ILRCO/128

//      <h> Configure Peripheral On Mode Clock
//          <q15.0>  Port A
//          <q15.1>  Port B
//          <q15.2>  Port C
//          <q15.3>  Port D
//          <q15.4>  Port E
//          <q15.8>  GPL
//          <q15.12> EMB
//          <q15.15> DMA
//          <e16.0>  ADC0
//              <o22.0>  Select ADCx_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e16.2>  CMP
//              <o22.4>  Select CMP_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e16.3>  DAC
//              <o22.5>  Select DAC_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <q16.5>  RTC
//          <q16.6>  IWDT
//          <q16.7>  WWDT
//          <e16.8>  I2C0
//              <o23.0>  Select I2C0_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e16.9>  I2C1
//              <o23.2>  Select I2C1_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e16.12> SPI0
//              <o23.8>  Select SPI0_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e16.16> URT0
//              <o23.16> Select URT0_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e16.17> URT1
//              <o23.18> Select URT1_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e16.18> URT2
//              <o23.20> Select URT2_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e16.19> URT3
//              <o23.22> Select URT3_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e17.0>  TM00
//              <o24.0>  Select TM00_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e17.1>  TM01
//              <o24.2>  Select TM01_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e17.4>  TM10
//              <o24.8>  Select TM10_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e17.7>  TM16
//              <o24.14> Select TM16_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e17.8>  TM20
//              <o24.16> Select TM20_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e17.11> TM26
//              <o24.22> Select TM26_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//          <e17.15> TM36
//              <o24.30> Select TM36_PR Source <0=> CK_APB <1=> CK_AHB
//          </e>
//      </h>

//      <h> Configure Peripheral Sleep Mode Clock
//          <q18.0>  ADC0
//          <q18.2>  CMP
//          <q18.3>  DAC
//          <q18.5>  RTC
//          <q18.6>  IWDT
//          <q18.7>  WWDT
//          <q18.8>  I2C0
//          <q18.9>  I2C1
//          <q18.12> SPI0
//          <q18.16> URT0
//          <q18.17> URT1
//          <q18.18> URT2
//          <q18.19> URT3
//          <q19.0>  TM00
//          <q19.1>  TM01
//          <q19.4>  TM10
//          <q19.7>  TM16
//          <q19.8>  TM20
//          <q19.11> TM26
//          <q19.15> TM36
//          <q19.30> EMB
//      </h>

//      <h> Configure Peripheral Stop Mode Clock
//          <q20.5>  RTC
//          <q20.6>  IWDT
//      </h>

//      <e014.0> Enable ICKO
//          <o14.4..7> Select CKO <0=> CK_MAIN
//                                <1=> CK_AHB
//                                <2=> CK_APB
//                                <3=> CK_HS
//                                <4=> CK_LS
//                                <5=> CK_XOSC
//          <o14.2..3> Select ICKO Divider <0=> ICK/1
//                                         <1=> ICK/2
//                                         <2=> ICK/4
//                                         <3=> ICK/8
//      </e>
//  </h>

//*** <<< end of configuration section >>>    ***

#define ON_CSC_PLL_PLL_MUL      0x00000000  // 0
#define ON_CSC_PLL_XOSC_GN      0x00010000  // 1

#define ON_CSC_CR0_MCD_DIS      0x00000010  // 2
#define ON_CSC_CR0_LS_SEL       0x00000200  // 3
#define ON_CSC_CR0_HS_SEL       0x00000000  // 4
#define ON_CSC_CR0_MAIN_SEL     0x00008000  // 5
#define ON_CSC_CR0_ST_SEL 	    0x00010000 	// 6
#define ON_CSC_CR0_IHRCO_SEL    0x00000000 	// 7
#define ON_CSC_CR0_MCD_SEL 	    0x00C00000 	// 8

#define ON_CSC_DIV_PLLI_DIV     0x00000001  // 9
#define ON_CSC_DIV_PLLO_DIV     0x00000020  // 10
#define ON_CSC_DIV_AHB_DIV      0x00000000  // 11
#define ON_CSC_DIV_APB_DIV      0x00000000  // 12
#define ON_CSC_DIV_UT_DIV       0x00000000  // 13
                                         
#define ON_CSC_CKO      0x00000000          // 14
#define ON_CSC_AHB      0x0000911F          // 15
#define ON_CSC_APB0     0x000F13ED          // 16
#define ON_CSC_APB1     0x00008993          // 17
#define ON_CSC_SLP0     0x00000000          // 18
#define ON_CSC_SLP1     0x00000000          // 19
#define ON_CSC_STP0     0x00000000          // 20
#define ON_CSC_STP1     0x00000000          // 21
#define ON_CSC_CSK0     0x00000000          // 22
#define ON_CSC_CSK1     0x00000000          // 23
#define ON_CSC_CSK2     0x00000000          // 24
#define ON_CSC_OTHER    0x00000000          // 25
#define ON_XOSC_EXTCLK_FREQ 12000000

#define ON_CSC_STA      0X00000000
#define ON_CSC_INT      0x00000000
#define ON_CSC_KEY      0x00000000

#define ON_CSC_PLL      (ON_CSC_PLL_PLL_MUL | ON_CSC_PLL_XOSC_GN)                
#define ON_CSC_CR0      (ON_CSC_CR0_MCD_DIS | ON_CSC_CR0_LS_SEL | ON_CSC_CR0_HS_SEL | ON_CSC_CR0_MAIN_SEL | ON_CSC_CR0_ST_SEL | ON_CSC_CR0_IHRCO_SEL | ON_CSC_CR0_MCD_SEL)
#define ON_CSC_DIV      (ON_CSC_DIV_PLLI_DIV | ON_CSC_DIV_PLLO_DIV | ON_CSC_DIV_AHB_DIV | ON_CSC_DIV_APB_DIV | ON_CSC_DIV_UT_DIV)


    /* Calculate CK_xxx Frequency */
// ON_CK_HS_FREQ
#if ON_CSC_CR0_HS_SEL == 0x00000000
    #if ON_CSC_CR0_IHRCO_SEL == 0x00000000
        #define ON_CK_HS_FREQ   12000000
    #else
        #define ON_CK_HS_FREQ   11059200
    #endif
#elif ON_CSC_CR0_HS_SEL == 0x00000400
    #define ON_CK_HS_FREQ   ON_XOSC_EXTCLK_FREQ
#elif ON_CSC_CR0_HS_SEL == 0x00000800
    #define ON_CK_HS_FREQ   32000
#elif ON_CSC_CR0_HS_SEL == 0x00000C00
    #define ON_CK_HS_FREQ   ON_XOSC_EXTCLK_FREQ
#endif

// ON_PLLI_DIV
#if ON_CSC_DIV_PLLI_DIV == 0x00000000
    #define ON_PLLI_DIV     1
#elif ON_CSC_DIV_PLLI_DIV == 0x00000001
    #define ON_PLLI_DIV     2
#elif ON_CSC_DIV_PLLI_DIV == 0x00000002
    #define ON_PLLI_DIV     4
#elif ON_CSC_DIV_PLLI_DIV == 0x00000003
    #define ON_PLLI_DIV     6
#endif

// ON_PLL_MUL
#if ON_CSC_PLL_PLL_MUL == 0x00000000
    #define ON_PLL_MUL      16
#elif ON_CSC_PLL_PLL_MUL == 0x00000100
    #define ON_PLL_MUL      24
#endif

// ON_PLLO_DIV
#if ON_CSC_DIV_PLLO_DIV == 0x00000000
    #define ON_PLLO_DIV     4
#elif ON_CSC_DIV_PLLO_DIV == 0x00000010
    #define ON_PLLO_DIV     3
#elif ON_CSC_DIV_PLLO_DIV == 0x00000020
    #define ON_PLLO_DIV     2
#elif ON_CSC_DIV_PLLO_DIV == 0x00000030
    #define ON_PLLO_DIV     1
#endif

// ON_CK_MAIN_FREQ
#if ON_CSC_CR0_MAIN_SEL == 0x00000000
    #define ON_CK_MAIN_FREQ     ON_CK_HS_FREQ
#elif ON_CSC_CR0_MAIN_SEL == 0x00004000
    #define ON_CK_MAIN_FREQ     ON_CK_HS_FREQ/ON_PLLI_DIV
#elif ON_CSC_CR0_MAIN_SEL == 0x00008000
    #define ON_CK_MAIN_FREQ     ON_CK_HS_FREQ /ON_PLLI_DIV *ON_PLL_MUL /ON_PLLO_DIV
#endif

// ON_CK_APB_FREQ
#if ON_CSC_DIV_APB_DIV == 0x00000000
    #define ON_CK_APB_FREQ      ON_CK_MAIN_FREQ /1
#elif ON_CSC_DIV_APB_DIV == 0x00010000
    #define ON_CK_APB_FREQ      ON_CK_MAIN_FREQ /2
#elif ON_CSC_DIV_APB_DIV == 0x00020000
    #define ON_CK_APB_FREQ      ON_CK_MAIN_FREQ /4
#elif ON_CSC_DIV_APB_DIV == 0x00030000
    #define ON_CK_APB_FREQ      ON_CK_MAIN_FREQ /8
#elif ON_CSC_DIV_APB_DIV == 0x00040000
    #define ON_CK_APB_FREQ      ON_CK_MAIN_FREQ /16
#endif

// ON_CK_AHB_FREQ
#if ON_CSC_DIV_AHB_DIV == 0x00000000
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /1
#elif ON_CSC_DIV_AHB_DIV == 0x00000100
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /2
#elif ON_CSC_DIV_AHB_DIV == 0x00000200
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /4
#elif ON_CSC_DIV_AHB_DIV == 0x00000300
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /8
#elif ON_CSC_DIV_AHB_DIV == 0x00000400
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /16
#elif ON_CSC_DIV_AHB_DIV == 0x00000500
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /32
#elif ON_CSC_DIV_AHB_DIV == 0x00000600
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /64
#elif ON_CSC_DIV_AHB_DIV == 0x00000700
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /128
#elif ON_CSC_DIV_AHB_DIV == 0x00000800
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /256
#elif ON_CSC_DIV_AHB_DIV == 0x00000900
    #define ON_CK_AHB_FREQ      ON_CK_APB_FREQ /512
#endif

// Flash wait state setting
#if ON_CK_AHB_FREQ <= 25000000
    #define FLASH_WAIT_STATE    MEM_FWAIT_ZERO
#elif ON_CK_AHB_FREQ <= 50000000
    #define FLASH_WAIT_STATE    MEM_FWAIT_ONE
#elif ON_CK_AHB_FREQ > 50000000
    #define FLASH_WAIT_STATE    MEM_FWAIT_TWO
#endif

// Check CK_APB_FREQ
#if ON_CK_APB_FREQ > 48000000
    #error "CK_APB frequency is greater than 48MHz, please let CK_APB frequency be less than or equal to 48MHz. (in MG32x02z_CSC_Init.h file)"
#endif

// Check CK_AHB_FREQ
#if ON_CK_AHB_FREQ > 48000000
    #error "CK_AHB frequency is greater than 48MHz, please let CK_AHB frequency be less than or equal to 48MHz. (in MG32x02z_CSC_Init.h file)"
#endif


static const unsigned int ON_CSC_InitConfig[] =
{
    ON_CSC_STA,     /* CSC_STA 0 not use */
    ON_CSC_INT,     /* CSC_INT 1 not use */
    ON_CSC_PLL,     /* CSC_PLL 2 */
    ON_CSC_KEY,     /* CSC_KEY 3 not use */
    ON_CSC_CR0,     /* CSC_CR0 4 */
    ON_CSC_DIV,     /* CSC_DIV 5 */
    ON_CSC_CKO,     /* CSC_CKO 6 */
    ON_CSC_AHB,     /* CSC_AHB 7 */
    ON_CSC_APB0,    /* CSC_APB0 8 */
    ON_CSC_APB1,    /* CSC_APB1 9 */
    ON_CSC_SLP0,    /* CSC_SLP0 10 */
    ON_CSC_SLP1,    /* CSC_SLP1 11 */
    ON_CSC_STP0,    /* CSC_STP0 12 */
    ON_CSC_STP1,    /* CSC_STP1 13 */
    ON_CSC_CSK0,    /* CSC_CSK0 14 */
    ON_CSC_CSK1,    /* CSC_CSK1 15 */
    ON_CSC_CSK2,    /* CSC_CSK2 16 */
    ON_CSC_OTHER,   /* CSC_OTHER 17 . bit0: 0: extern clock, 1: crystal clock */
    ON_XOSC_EXTCLK_FREQ,    /* XOSC_EXTCLK_FREQ 18 */
    FLASH_WAIT_STATE,       /* CK_APB_FREQ 19 */
};


/**
 ******************************************************************************
 *
 * @struct      CSC_CFG_Struct
 *              CSC Configuration Struct
 *
 ******************************************************************************
 */
typedef struct
{
    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __I  uint8_t                :8;     //[7..0] 
            __IO uint8_t  PLL_MUL       :1;     //[8] CSC PLL multiplication factor select.
                                        //0 = 16 : PLL input clock x 16
                                        //1 = 24 : PLL input clock x 24
            __I  uint8_t                :7;     //[15..9] 
            __IO uint8_t  XOSC_GN       :2;     //[17..16] Gain control bits of XOSC. (The default value is loaded from CFG OR after warm reset)
                                        //0x0 = Lowest (for 32KHz crystal)
                                        //0x1 = Medium_Low
                                        //0x2 = Medium_High
                                        //0x3 = Highest
            __I  uint8_t                :6;     //[23..18] 
            __I  uint8_t                :8;     //[31..24] 
        }MBIT;
    }PLL;                               /*!< PLL        ~ Offset[0x08]  CSC OSC and PLL control register */

    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __I  uint8_t                :1;     //[0] 
            __I  uint8_t                :1;     //[1] 
            __I  uint8_t                :1;     //[2] 
            __IO uint8_t  IHRCO_EN      :1;     //[3] IHRCO circuit enable.
                                        //0 = Disable
                                        //1 = Enable
            __IO uint8_t  MCD_DIS       :1;     //[4] MCD missing clock detector circuit disable.
                                        //0 = Enable
                                        //1 = Disable
            __IO uint8_t  PLL_EN        :1;     //[5] PLL circuit enable.
                                        //0 = Disable
                                        //1 = Enable
            __I  uint8_t                :1;     //[6] 
            __I  uint8_t                :1;     //[7] 
            __IO uint8_t  LS_SEL        :2;     //[9..8] Input low speed clock source select
                                        //0x0 = Reserved
                                        //0x1 = XOSC
                                        //0x2 = ILRCO
                                        //0x3 = CK_EXT
            __IO uint8_t  HS_SEL        :2;     //[11..10] Input high speed clock source select
                                        //0x0 = IHRCO
                                        //0x1 = XOSC
                                        //0x2 = ILRCO
                                        //0x3 = CK_EXT
            __I  uint8_t                :1;     //[12] 
            __I  uint8_t                :1;     //[13] 
            __IO uint8_t  MAIN_SEL      :2;     //[15..14] System clock source select.
                                        //0x0 = CK_HS
                                        //0x1 = CK_PLLI
                                        //0x2 = CK_PLLO
                                        //0x3 = Reserved
            __IO uint8_t  ST_SEL        :1;     //[16] System tick timer external clock source select.
                                        //0 = HCLK8 : HCLK divided by 8
                                        //1 = CK_LS2 : CK_LS divided by 2
            __I  uint8_t                :1;     //[17] 
            __IO uint8_t  IHRCO_SEL     :1;     //[18] IHRCO clock frequency trimming set select.
                                        //0 = 12 : 12MHz from trimming set 0
                                        //1 = 11 : 11.059MHz from trimming set 1
            __I  uint8_t                :3;     //[21..19] 
            __IO uint8_t  MCD_SEL       :2;     //[23..22] Missing clock detection duration select.
                                        //0x0 = 125us
                                        //0x1 = 250us
                                        //0x2 = 500us
                                        //0x3 = 1ms
            __I  uint8_t                :8;     //[31..24] 
        }MBIT;
    }CR0;                               /*!< CR0        ~ Offset[0x10]  CSC clock source control register 0 */

    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __IO uint8_t  PLLI_DIV      :2;     //[1..0] PLL input clock source divider
                                        //0x0 = DIV1 : divided by 1
                                        //0x1 = DIV2 : divided by 2
                                        //0x2 = DIV4 : divided by 4
                                        //0x3 = DIV6 : divided by 6
            __I  uint8_t                :2;     //[3..2] 
            __IO uint8_t  PLLO_DIV      :2;     //[5..4] PLL output clock source divider
                                        //0x0 = DIV4 : divided by 4
                                        //0x1 = DIV3 : divided by 3
                                        //0x2 = DIV2 : divided by 2
                                        //0x3 = DIV1 : divided by 1
            __I  uint8_t                :2;     //[7..6] 
            __IO uint8_t  AHB_DIV       :4;     //[11..8] AHB clock source divider. Value 0~9 mean to divide by 1,2,4,8,16,32,64,128,256,512.
                                        //0x0 = DIV1 : divided by 1
                                        //0x1 = DIV2 : divided by 2
                                        //0x2 = DIV4 : divided by 4
                                        //0x3 = DIV8 : divided by 8
                                        //0x4 = DIV16 : divided by 16
                                        //0x5 = DIV32 : divided by 32
                                        //0x6 = DIV64 : divided by 64
                                        //0x7 = DIV128 : divided by 128
                                        //0x8 = DIV256 : divided by 256
                                        //0x9 = DIV512 : divided by 512
            __I  uint8_t                :4;     //[15..12] 
            __IO uint8_t  APB_DIV       :3;     //[18..16] APB clock source divider. Value 0~4 mean to divide by 1,2,4,8,16.
                                        //0x0 = DIV1 : divided by 1
                                        //0x1 = DIV2 : divided by 2
                                        //0x2 = DIV4 : divided by 4
                                        //0x3 = DIV8 : divided by 8
                                        //0x4 = DIV16 : divided by 16
            __I  uint8_t                :5;     //[23..19] 
            __I  uint8_t                :1;     //[24] 
            __I  uint8_t                :1;     //[25] 
            __IO uint8_t  UT_DIV        :2;     //[27..26] Unit time clock source divider.
                                        //0x0 = DIV32 : divided by 32
                                        //0x1 = DIV8 : divided by 8
                                        //0x2 = DIV16 : divided by 16
                                        //0x3 = DIV128 : divided by 128
            __I  uint8_t                :4;     //[31..28] 
        }MBIT;
    }DIV;                               /*!< DIV        ~ Offset[0x14]  CSC clock  divider register */

} CSC_CFG_Struct;


/**
 * @name	Function announce
 *   		
 */
///@{  
void CSC_Init (uint32_t* );
///@}


#endif  // _MG32x02z_CSC_INIT_H

