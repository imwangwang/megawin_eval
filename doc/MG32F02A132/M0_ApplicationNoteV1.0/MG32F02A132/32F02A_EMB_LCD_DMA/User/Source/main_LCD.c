/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

//#include "MG32x02z__Common_DRV.H"
//#include "MG32x02z_EXIC_DRV.H"
//#include "MG32x02z__IRQHandler.H"
#include "MG32x02z_MID.H"

#include "MG32x02z_GPIO_Init.h"
#include "MG32x02z_IRQ_Init.h"
#include "MG32x02z_I2C_Init.h"
#include "MG32x02z_EMB_Init.h"
#include "MG32x02z_MEM_Init.h"
#include "MG32x02z_GPL_Init.h"

#include "LCD.h"
#include "ltdc.h"
#include "ugui.h"
#include "LCDDEMO_RGBLED_API.h"
#include "LCDDEMO_KEY_API.H"
#include "API_Flash.H"
#include "TYPE.h"
#include "UserDefine.h"

//#incldue "meun.h"

extern DRV_Return URT_Init(URT_Struct* URTX);


//void ChipInit(void);
//void Sample_Code(void);
//extern UG_GUI gui;

#define TextCol 80
#define TextRow 20
#define _CK_IHRCO_		0
#define _CK_XOSC_		1
#define _CK_SEL_		_CK_IHRCO_
#define SYS_CLOCK		48.000				// sysclk =48MHz
// TM00 Full Counter
#define TM00_PRE_2nd_RELOAD			(u8)((u32)(1000*SYS_CLOCK/1-1)%256)		// 1ms 
#define TM00_MAIN_RELOAD			(u8)((u32)(1000*SYS_CLOCK/1-1)/256)		// 1ms
// WAVE 数据格式
typedef struct 
{
	u8     riff[4];            //"RIFF",文件标志
	u32    fsize;              //文件大小(从下个地址开始到文件尾的总字节数)
	u8     wave[4];            //"WAVE",文件标志
}WAVE_TAG;

typedef struct 
{
	u8     fmt[4];             //"fmt ",波形格式标志 
	u32    chunsize;           //文件内部格式信息大小
	u16    wformattag;         //音频数据编码方式 1PCM
	u16    wchanles;           //声道数 1,2
	u32    dwsamplespersec;    //采样率 
	u32    dwavgbytespersec;   //波形数据传输速率（每秒平均字节数）
	u16    wblockalign;        //数据的调整数（按字节计算）
	u16    wbitspersample;     //样本数据位数
}WAVE_FMT;

typedef struct 
{
	u8     data[4];            //"data",数据标志符
	u32    datasize;           //采样数据总长度 
}WAVE_DAT;

typedef struct 
{
	WAVE_TAG   WaveTag; 
	WAVE_FMT   WaveFmt;
	WAVE_DAT   WaveDat;
	u32 DataStartAddress;
}WAVE_HEAD;


u8 WavStatus;				// Wav播放状态
enum{
	WAVE_IDLE,
	WAVE_PLAY_START,
	WAVE_PLAY_ING,
	WAVE_PLAY_PAUSE,
	WAVE_PLAY_STOP,
};

u32 WaveWaitingTime=10000;		// 自动播放Wav之间的等待时间:10000ms=10s
u32 WaveDataAddress;		// Wav数据起始地址
u32 WavePlayTime;			// Wav播放时间
u32 WaveTime;				// WAV音频时长

u8 WavInx;
u8 WavLoopEnable=0;		//自动循环播放Wav
u8 WavVolume=3;				//音量控制

void WAVE_PlayStop(void);


u8 LedTime;
u8 KeyDebugTime;

#define UART0_RX_BUF_SIZE		32

u8 Uar0RxBuf[UART0_RX_BUF_SIZE];
u8 Uart0RxIn;
u8 Uart0RxOut;

uint32_t blue=0x07E0;
uint8_t lcd_id[80];
int Begin_Time,Finish_Time;
double time;

u32 PressCount =0;

struct {
    struct{
        uint8_t Text[TextCol];
        uint8_t Font;
        uint8_t Length;
        uint16_t Color;
    }Text[TextRow];
    uint8_t ShowEnable;
    uint8_t AddIndex;
    uint16_t Accumulation;
}TextList;

void SysTick_Handler(void)
{
    //to do......
	IncTick();
	if(WavLoopEnable==1&&WaveWaitingTime>0&&WavStatus==WAVE_IDLE)
	{
		WaveWaitingTime--;
	}
		if(WavStatus==WAVE_PLAY_ING)
	{// 正在播放WAV
		WavePlayTime++;										// 播放时间加1ms			
	}
}

/***********************************************************************************
函数名称:	void TM00_Init(void)
功能描述:	TM00初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void TM00_Init(void)
{
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
 
	//==Set TM0x Clock
    UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_TM00_CKS, CK_APB);
 	CSC_PeriphOnModeClock_Config(CSC_ON_TM00,ENABLE);						// Enable TM00 Clock
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);						// Enable PortB Enable
    ProtectModuleReg(CSCprotect);
	 		
			 		

    TM_DeInit(TM00);
    
    // ----------------------------------------------------
    // 1.Set TimeBase structure parameter
    TM_TimeBase_InitStruct.TM_CounterMode = Full_Counter;					// 全计数器模式
    TM_TimeBase_InitStruct.TM_MainClockSource = TM_CK_INT;					// 主计数器源: TM_CK_INT 
    TM_TimeBase_InitStruct.TM_2ndClockSource = TM_CK_INT;					// 预分频器(2nd计数器)计数器源: TM_CK_INT
    TM_TimeBase_InitStruct.TM_MainClockDirection = TM_UpCount;				// 主计数器向上计数 
    TM_TimeBase_InitStruct.TM_2ndClockDirection = TM_UpCount;				// 预分频器(2nd计数器)向上计数

    TM_TimeBase_InitStruct.TM_Prescaler = TM00_PRE_2nd_RELOAD;				// 预分频器(2nd计数器)重载值	
    TM_TimeBase_InitStruct.TM_Period = TM00_MAIN_RELOAD;					// 主计数器重载值
    TM_TimeBase_Init(TM00, &TM_TimeBase_InitStruct);
  
	//== SET TM00 ClockOut
	TM_ClockOutSource_Select(TM00, MainCKO);
	TM_ClockOut_Cmd(TM00, ENABLE);  

	//== SET TM00 TRGO
	TM_TRGO_Select(TM00, TRGO_UEV);											// 设置TM00溢出时触发输出,用于触发DAC
	
}



/***********************************************************************************
函数名称:	void DAC0_Init(void)
功能描述:	DAC0初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void DAC0_Init(void)
{  
	    
	PIN_InitTypeDef PINX_InitStruct;
    
	

	/* Configure peripheral clock */
    UnProtectModuleReg(CSCprotect);

	CSC_PeriphProcessClockSource_Config(CSC_DAC_CKS, CK_APB);
	CSC_PeriphOnModeClock_Config(CSC_ON_DAC, ENABLE);					  // Enable DAC module clock

    ProtectModuleReg(CSCprotect);
    //==Set GPIO init 
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOA->Pin0
    //GPIO port initial is 0xFFFF
    //PB2 mode is AIO
    //PB2 function GPB2
    
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_Analog_IO; 	 	// Pin select AIO mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Disable;  	// Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function  = PB2_AF_GPB2;					// Pin AFS = GPB0
	GPIO_PinMode_Config(PINB(2),&PINX_InitStruct); 					 		// 

    
    // ------------------------------------------
    // 1.De-initial DAC
    DAC_DeInit(DAC);            
    
    // ------------------------------------------
    // 2.Select Update mode be software setting
    DAC_TriggerSource_Select(DAC, DAC_TM00_TRGO);							// DAC触发源为TM00触发输出
    DAC_TriggerEdge_Select(DAC,DAC_AcceptRisingEdge);
    
    // ------------------------------------------
    // 3.Config DAC_10BitData + DAC_RightJustified
    DAC_DataResolution_Select(DAC, DAC_10BitData);
    DAC_DataAlignment_Select(DAC, DAC_RightJustified);

    // ------------------------------------------
    // 4.set current level
    DAC_CurrentMode_Select(DAC, DAC_M2);    								// DAC_M1: ~2mA (Full scale)

    // ------------------------------------------
	// 5.Update the DAC value
    DAC_SetDAT0(DAC, 0x00);													// DAC值位512
    
    // ------------------------------------------
	// 6.Enable DAC & output
    DAC_Cmd(DAC, ENABLE);       											// 使能DAC

    
    
}


/***********************************************************************************
函数名称:	void DMA1_Init(void)
功能描述:	DMA1初始化用于DAC
输入参数:	
返回参数:	  
*************************************************************************************/
void DMA1_Init(void)
{  
    DMA_BaseInitTypeDef DMATestPattern;

    /* Configure peripheral clock */
    UnProtectModuleReg(CSCprotect);

	CSC_PeriphOnModeClock_Config(CSC_ON_DMA, ENABLE);					  // Enable DMA module clock

    ProtectModuleReg(CSCprotect);

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel1, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
    {   
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel1;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = DISABLE;
        DMATestPattern.DestDINCSel = DISABLE;
        // DMA loop mode 
        DMATestPattern.DMALoopCmd=ENABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_SPI0_RX;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_DAC0_OUT;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_2Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = 65535;
    
        // source/destination config
        //DMATestPattern.DMASourceAddr = (uint8_t *)DACTestTable;
        //DMATestPattern.DMADestinationAddr = (uint8_t *)&URTX->TDAT.B[0];
    }
    
    // ------------------------------------------------------------------------
    // Setting M2M simple parameter
    DMA_Base_Init(&DMATestPattern);
    
    
    return;
}

/*
*************************************************************************************
*  WAV Function
*
*************************************************************************************
*/
/***********************************************************************************
函数名称:	u32 WAVE_FindChunk(u32 StartAddress, u8 *ChunkBuf, u8 ChunkLength,u16 HeadMaxLength)
功能描述:	查找Chunk标志

输入参数:	u32 StartAddress: 24位起始地址 0x00000000 ~ 0xFFFFFFFF
		  u8 *ChunkBuf: chunk标志
		  u8 ChunkLength: chunk长度
		  u16 HeadMaxLength: head最大长度
返回参数: u32 chunk所在地址.  如为0,则失败
*************************************************************************************/
u32 WAVE_FindChunk(u32 StartAddress, s8 *ChunkBuf, u8 ChunkLength,u16 HeadMaxLength)
{
	u16 i;
    u8  FlashData;
    u8 x;
    
	nCS = 0;
    /* Write command */
	SPI_SetTxData(SPI0, SPI_1Byte, 0x03);
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TXF flag
    SPI_ClearFlag(SPI0, (SPI_TCF | SPI_TXF | SPI_RXF));                 // Clear TXF and RXF
	
    /* Write address */
	SPI_DataSize_Select(SPI0, SPI_24bits);
	SPI_SetTxData(SPI0, SPI_3Byte, StartAddress);
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TXF flag
    SPI_ClearFlag(SPI0, (SPI_ALLF));                                    // Clear TXF and RXF
	SPI_DataSize_Select(SPI0, SPI_8bits);

	SPI_ClearRxData(SPI0);
  
    /* Read data */
    x=0;
	for(i = 0; i < HeadMaxLength; i++)
	{
		SPI_SetTxData(SPI0, SPI_1Byte, Dummy_Data);
		while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    
		SPI_ClearFlag(SPI0, SPI_TCF | SPI_TXF | SPI_RXF);                   
        FlashData = SPI_GetRxData(SPI0);                              // Store data to buffer
		if(FlashData == ChunkBuf[x])
		{
			x++;
			if(x==ChunkLength)
			{// 找到了Chunk标志
				break;
			}
		}
		else
		{
			x=0;
		}
	}
	nCS = 1;
	if(i<HeadMaxLength)
	{
		return (StartAddress+i+1);
	}
	else
	{
		return 0x00000000;
	}
}

/***********************************************************************************
函数名称:	void WAVE_ReadChunk(u32 StartAddress, u8 *Chunk, u16 ChunkLength)
功能描述:	读chunk

输入参数:	u32 StartAddress: 24位起始地址 0x00000000 ~ 0xFFFFFFFF
		  u8 *ChunkB: chunk缓存地址
		  u8 ChunkLength: chunk长度
返回参数: 
*************************************************************************************/
void WAVE_ReadChunk(u32 StartAddress, u8 *Chunk, u16 ChunkLength)

{
	u16 i;
   
	nCS = 0;
    /* Write command */
	SPI_SetTxData(SPI0, SPI_1Byte, 0x03);
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TXF flag
    SPI_ClearFlag(SPI0, (SPI_TCF | SPI_TXF | SPI_RXF));                 // Clear TXF and RXF
	
    /* Write address */
	SPI_DataSize_Select(SPI0, SPI_24bits);
	SPI_SetTxData(SPI0, SPI_3Byte, StartAddress);
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TXF flag
    SPI_ClearFlag(SPI0, (SPI_ALLF));                                    // Clear TXF and RXF
	SPI_DataSize_Select(SPI0, SPI_8bits);

	SPI_ClearRxData(SPI0);
  
    /* Read data */
	for(i = 0; i < ChunkLength; i++)
	{
		SPI_SetTxData(SPI0, SPI_1Byte, Dummy_Data);
		while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    
		SPI_ClearFlag(SPI0, SPI_TCF | SPI_TXF | SPI_RXF);                   
        Chunk[i] = SPI_GetRxData(SPI0);                              // Store data to buffer
	}
	nCS = 1;
}

/***********************************************************************************
函数名称:	u8 WAVE_Find(u32 StartAddress,WAVE_HEAD *WaveHead)
功能描述:	读chunk

输入参数:	u32 StartAddress: 24位起始地址 0x00000000 ~ 0xFFFFFFFF
		  WAVE_HEAD *WaveHead: Wav文件头结构地址
返回参数: u8: 0->查找WAV文件头成, 其它->失败
*************************************************************************************/
u8 WAVE_Find(u32 StartAddress, WAVE_HEAD *WaveHead)
{
	
	u8 bWavFlag;
	u32 ChunkAddress;
	bWavFlag=FALSE;
	ChunkAddress=WAVE_FindChunk(StartAddress,"RIFF",4,1024);
	if(ChunkAddress!=0)
	{
		WAVE_ReadChunk(ChunkAddress-4,&WaveHead->WaveTag.riff[0],sizeof(WAVE_TAG));
		ChunkAddress=WAVE_FindChunk(StartAddress,"fmt ",4,1024);
		if(ChunkAddress!=0)
		{
			WAVE_ReadChunk(ChunkAddress-4,&WaveHead->WaveFmt.fmt[0],sizeof(WAVE_FMT));
			ChunkAddress=WAVE_FindChunk(StartAddress,"data",4,1024);
			if(ChunkAddress!=0)
			{
				WAVE_ReadChunk(ChunkAddress-4,&WaveHead->WaveDat.data[0],sizeof(WAVE_DAT));
				// 取得Wav数据起始地址
				WaveHead->DataStartAddress=ChunkAddress+4;													
				// 置读取WAV头数据成功标志
				bWavFlag=TRUE;
			}
			else
			{
			}
		}
		else
		{
		}
	}
	else
	{
	}
	return bWavFlag;
}


/***********************************************************************************
函数名称:	void WAVE_StartReadData(u32 StartAddress)
功能描述:	读数据

输入参数:	u32 StartAddress: 起始地址
返回参数: 
*************************************************************************************/
void WAVE_StartReadData(u32 StartAddress)
{
 
	nCS = 0;
    /* Write command */
	SPI_SetTxData(SPI0, SPI_1Byte, 0x03);
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TXF flag
    SPI_ClearFlag(SPI0, (SPI_TCF | SPI_TXF | SPI_RXF));                 // Clear TXF and RXF
	
    /* Write address */
	SPI_DataSize_Select(SPI0, SPI_24bits);
	SPI_SetTxData(SPI0, SPI_3Byte, StartAddress);
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TXF flag
    SPI_ClearFlag(SPI0, (SPI_ALLF));                                    // Clear TXF and RXF
	SPI_DataSize_Select(SPI0, SPI_8bits);

	SPI_ClearRxData(SPI0);
	SPI_SetTxData(SPI0, SPI_1Byte, Dummy_Data);
  
}

/***********************************************************************************
函数名称:	u32 WAVE_ReadData(u8 DataLength)
功能描述:	读数据

输入参数:	u8 DataLength: 数据长度 1,2,4
返回参数: 读到的音频数据
*************************************************************************************/
u32 WAVE_ReadData(u8 DataLength)
{
   	u8 i;
  	DWordTypeDef dwTemp;
	dwTemp.DW=0;
    /* Read data */
	for(i = 0; i < DataLength; i++)
	{
		SPI_SetTxData(SPI0, SPI_1Byte, Dummy_Data);
		while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    
		SPI_ClearFlag(SPI0, SPI_TCF | SPI_TXF | SPI_RXF);                   
        dwTemp.BUF[i] = SPI_GetRxData(SPI0);                              // Store data to buffer
	}
	return dwTemp.DW;
}

/***********************************************************************************
函数名称:	void WAVE_StopReadData(void)
功能描述:	停止读数据

输入参数:	
返回参数: 
*************************************************************************************/
void WAVE_StopReadData(void)

{
   	nCS =1;
  
}

void WAVE_Delay(void)
{
	u16 x;
	for(x=0;x<500;x++)
	{
	
	}
}


/***********************************************************************************
函数名称:	void WAVE_StartPlay(void)
功能描述:	开始播放

输入参数:	u32 StartAddress: 24位起始地址 0x00000000 ~ 0xFFFFFFFF
		  WAVE_HEAD *WaveHead: Wav文件头结构地址
返回参数: u8: 0->成功, 其它->失败
*************************************************************************************/
u8 WAVE_PlayStart(u32 StartAddress)
{
	WAVE_HEAD WavHead;
	u16 wTemp;

	if(WavStatus != WAVE_IDLE) return 0x01;
	
	if(WAVE_Find(StartAddress, &WavHead))
	{
		// 设置Wav数据起始地址
		WaveDataAddress=WavHead.DataStartAddress;
		
		// 设置播放采样率
		wTemp=(u16)((SYS_CLOCK*1000000)/WavHead.WaveFmt.dwsamplespersec);
		TM_Prescaler_Config(TM00,0,wTemp&0xFF);
		TM_Counter_Config(TM00,0,wTemp>>8);
		
		// 取得Wav播放时长单位为ms
		WaveTime=((WavHead.WaveDat.datasize)/(WavHead.WaveFmt.dwavgbytespersec/100))*10;

		wTemp=0;
		while(wTemp<512)
		{
			WAVE_Delay();
			wTemp++;
			DAC_SetDAT0(DAC,wTemp);
		}
		// 开始读Wav数据
		WAVE_StartReadData(WaveDataAddress);
		//DAC_SetDAT0(DAC, WAVE_ReadData(WavHead.WaveFmt.wblockalign));
		// 使能TM00
		TM_Timer_Cmd(TM00,ENABLE);
		// 使能DMA
		DMA_Cmd(ENABLE);
		// 使能DMA控制SPI 接收
		SPI_DMAReceive_Cmd(SPI0,ENABLE);
		// 使能DMA控制DAC
		DAC_DMA_Cmd(DAC, ENABLE);
		// 清DMA标志
		DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
		// 启动DMA通道0
		DMA_StartRequest(DMAChannel1);
		
		// 置状态为播放状态
		WavePlayTime=0;
		WavStatus=WAVE_PLAY_ING;

		return 0;
	}
	else
	{
		return 0xFE;
	}
	
}


/***********************************************************************************
函数名称:	void WAVE_PlayPause(void)
功能描述:	暂停播放

输入参数:	
返回参数: 
*************************************************************************************/
void WAVE_PlayPause(void)
{
	if(WavStatus == WAVE_PLAY_ING)
	{
		WavStatus=WAVE_PLAY_PAUSE;
		DMA_Hold_Cmd(DMAChannel0,ENABLE);
	}
}

/***********************************************************************************
函数名称:	void WAVE_PlayResume(void)
功能描述:	恢复播放

输入参数:	
返回参数: 
*************************************************************************************/
void WAVE_PlayResume(void)
{
	if(WavStatus == WAVE_PLAY_PAUSE)
	{
		WavStatus=WAVE_PLAY_ING;
		DMA_Hold_Cmd(DMAChannel0,DISABLE);
	}
}



/***********************************************************************************
函数名称:	void WAVE_PlayStop(void)
功能描述:	停止播放

输入参数:	
返回参数: 
*************************************************************************************/
void WAVE_PlayStop(void)
{
	u16 DACData;
	PE13=1;
	PE14=1;
	if((WavStatus == WAVE_PLAY_ING)||(WavStatus == WAVE_PLAY_PAUSE))
	{
		WavStatus=WAVE_IDLE;
		WAVE_StopReadData();
		DMA_Cmd(DISABLE);
		WaveWaitingTime=10000;
		DMA_Hold_Cmd(DMAChannel0,DISABLE);
		SPI_DMAReceive_Cmd(SPI0,DISABLE);
		DAC_DMA_Cmd(DAC, DISABLE);
		DACData=DAC_GetDAT0(DAC);
		while(DACData!=0)
		{
			WAVE_Delay();
			DACData--;
			DAC_SetDAT0(DAC,DACData);
		}
		
	}
}
/*
*************************************************************************************
*/ 



/*
*************************************************************************************
*  UART Function
*
*************************************************************************************
*/

#define WAV_CNT_MAX		5
u8 Wav_Count=0;
//50000:欢迎光临
const u32 WAVAddress[]={
	0x00010000,
	0x00020000,
	0x00160000,
	0x00180000,
	0x001e0000,
};

void WaveProc(void)
{
	if(WavePlayTime>=WaveTime)
	{ // 播放时间大于等于WAV时长,则停止播放
		WAVE_PlayStop();
	}
}

void WaveLoopProc(void)
{
	if(WavLoopEnable==1&&WaveWaitingTime==0)
	{
			WAVE_PlayStop();
			Delay(10);
			WaveWaitingTime=10000;
			WAVE_PlayStart(WAVAddress[Wav_Count]);
			Wav_Count++;
		  if(Wav_Count>=WAV_CNT_MAX)
			{
				Wav_Count=0;
			}
	}
}

void API_TextList_Init(void)
{
    uint8_t lx, ly;
    ly = 0;
    do{
        lx = 0;
        do{
            TextList.Text[ly].Text[lx] = 0x20;
        }while(++lx < TextCol);
        TextList.Text[ly].Color = 0;
        TextList.Text[ly].Font = 0;
        TextList.Text[ly].Length = 0;
    }while(++ly < TextRow);
    TextList.Accumulation = 0;
    TextList.AddIndex = 0;
    TextList.ShowEnable = 0;
}



void API_ShowTextList(void)
{
    uint16_t lYAxis = 0;
    uint8_t lShowIndex;
    uint8_t lShowCount = 0;
    if(TextList.ShowEnable == 0)
        return;
    LCD_Clear(BLACK);
    if(TextList.Accumulation < TextRow)
        lShowIndex = 0;
    else
        lShowIndex = TextList.AddIndex;
    do{
        LCD_ShowString(0, lYAxis, lcddev.width, 16, TextList.Text[0].Color, TextList.Text[lShowIndex].Font, TextList.Text[lShowIndex].Text);
        lShowIndex ++;
        if(lShowIndex >= TextRow)
            lShowIndex = 0;
        lYAxis += 16;
    }while(++lShowCount < TextRow);
}



void API_AddText2List(uint8_t *p, uint8_t Length, uint8_t Font, uint16_t Color)
{
    uint8_t *lpChar = p;
    uint8_t lCharCount = 0;
    do{
        TextList.Text[TextList.AddIndex].Text[lCharCount] = *lpChar;
        lpChar ++;
    }while(++lCharCount < TextCol);
    TextList.Text[TextList.AddIndex].Length = Length;
    TextList.Text[TextList.AddIndex].Font = Font;
    TextList.Text[TextList.AddIndex].Color = Color;

    TextList.AddIndex ++;
    if(TextList.AddIndex >= TextRow)
        TextList.AddIndex = 0;
    TextList.Accumulation ++;
    API_ShowTextList();
}


uint32_t TotalCount = 0;



struct{
    uint8_t Name[6];
    uint32_t Address;
    uint32_t Size;
    uint16_t Check;
}FlashIS;



union RBuffer{
    uint32_t W[512];
    uint16_t H[1024];
    uint8_t  B[2048];
}ReadDataBuffer;



void LCD_Show_Picture(uint16_t nTh)
{
    uint32_t lBaseAddr = (nTh << 4);
    uint32_t lCount;
    uint32_t lAddr;
    union{
        uint16_t H[512];
        uint8_t B[1024];
    }lH_B;
    // Read Head 
    API_Flash_MultiBytesRead(0x0000000, (uint8_t *)&TotalCount, 0x4);

    API_Flash_MultiBytesRead(lBaseAddr, (uint8_t *)&FlashIS.Name, 6);
    API_Flash_MultiBytesRead((lBaseAddr + 6), (uint8_t *)&FlashIS.Address, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xA), (uint8_t *)&FlashIS.Size, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xE), (uint8_t *)&FlashIS.Check, 2);

    LCD_SetCursor(0, 0);		//?????? 
    LCD_WriteRAM_Prepare();	    //????GRAM
    __LCD_CS_CLR();\
    __LCD_RS_SET();\

    lAddr = FlashIS.Address;
    do{
        API_Flash_MultiBytesRead(lAddr, &lH_B.B[0], 1024);
        lCount = 0;
        do{
            if(lH_B.H[lCount] == 0xFFFF)
                __DATAOUT(0x0000);
            else
                __DATAOUT(lH_B.H[lCount]);
        }while(++ lCount < 512);
        lAddr += 1024;
    }while(lAddr < (FlashIS.Address + FlashIS.Size));
    __LCD_CS_SET();
}

void DMA_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = DISABLE;
        DMATestPattern.DestDINCSel = ENABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_SPI0_RX;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_MEM_Write;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = 1024;
    
        // source/destination config
        DMATestPattern.DMASourceAddr = &SPI0->RDAT;//(uint8_t *)&RcvBuf;
//        DMATestPattern.DMADestinationAddr = (uint32_t *)0x60000000;//(uint8_t *)&RcvBuf;
				
						 // Setting M2M simple parameter

        DMA_Base_Init(&DMATestPattern);
}

void DMAU_Init(void)
{
	 DMA_BaseInitTypeDef DMATestPattern;

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
       
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = DISABLE;
        DMATestPattern.DestDINCSel = ENABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_SPI0_RX;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_MEM_Write;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = 1024;
    
        // source/destination config
        DMATestPattern.DMASourceAddr = &SPI0->RDAT;//(uint8_t *)&RcvBuf;
        DMATestPattern.DMADestinationAddr = (uint32_t *)0x60000000;//(uint8_t *)&RcvBuf;
				
						 // Setting M2M simple parameter

        DMA_Base_Init(&DMATestPattern);
}

void LCD_Show_Pictureu(uint16_t nTh)
{
    uint32_t lBaseAddr = (nTh << 4);
    uint32_t lAddr;
    union{
        uint16_t H[512];
        uint8_t B[1024];
    }lH_B;
    // Read Head 
    API_Flash_MultiBytesRead(0x0000000, (uint8_t *)&TotalCount, 0x4);

    API_Flash_MultiBytesRead(lBaseAddr, (uint8_t *)&FlashIS.Name, 6);
    API_Flash_MultiBytesRead((lBaseAddr + 6), (uint8_t *)&FlashIS.Address, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xA), (uint8_t *)&FlashIS.Size, 4);
    API_Flash_MultiBytesRead((lBaseAddr + 0xE), (uint8_t *)&FlashIS.Check, 2);
		
    LCD_SetCursor(0, 0);		//?????? 
    LCD_WriteRAM_Prepare();	    //????GRAM
    __LCD_CS_CLR();\
    __LCD_RS_SET();\
		
    lAddr = FlashIS.Address;
    do{
				DMAU_Init();
        API_Flash_MultiBytesReadu(lAddr, &lH_B.B[0], 1024);
         lAddr += 1024;
    }while(lAddr < (FlashIS.Address + FlashIS.Size));
    __LCD_CS_SET();
		DMA_Init();
}

void LCD_Show_Text(void)
{
    __LCD_BackLight_Off();
    LCD_Clear(BLACK);
    LCD_ShowString(0, 0, lcddev.width, 24, GBLUE, 24, (uint8_t *)"Megawin Company");
    LCD_ShowString(0, 24, lcddev.width, 24, BRED, 24, (uint8_t *)"ARM Cortex-M0 Series");
    LCD_ShowString(0, 48, lcddev.width, 24, YELLOW, 24, (uint8_t *)"Product MG32F02A");	
    LCD_ShowString(0, 72, lcddev.width, 24, GREEN, 24, (uint8_t *)"Show Features");
    LCD_ShowString(0, 96, lcddev.width, 24, BLUE, 24, (uint8_t *)"2018/02/17");

    LCD_ShowString(0, 144, lcddev.width, 16, WHITE, 16, (uint8_t *)"Megawin Company");	
    LCD_ShowString(0, 160, lcddev.width, 16, WHITE, 16, (uint8_t *)"ARM Cortex M0 Series");	
    LCD_ShowString(0, 176, lcddev.width, 16, WHITE, 16, (uint8_t *)"Product MG32F02A");
    LCD_ShowString(0, 192, lcddev.width, 16, WHITE, 16, (uint8_t *)"Show Features");
    LCD_ShowString(0, 208, lcddev.width, 16, WHITE, 16, (uint8_t *)"2018/02/17");

    LCD_ShowString(0, 240, lcddev.width, 12, WHITE, 12, (uint8_t *)"Megawin MG32F02A");	
    LCD_ShowString(0, 252, lcddev.width, 12, WHITE, 12, (uint8_t *)"ARM Cortex M0 Series");	
    LCD_ShowString(0, 264, lcddev.width, 12, WHITE, 12, (uint8_t *)"Product MG32F02A");
    LCD_ShowString(0, 276, lcddev.width, 12, WHITE, 12, (uint8_t *)"Show Features");
    LCD_ShowString(0, 288, lcddev.width, 12, WHITE, 12, (uint8_t *)"2018/02/17");
    __LCD_BackLight_On();
}



void LCD_Random_Text(void)
{   uint16_t lCount = 50;
    LCD_Clear(0xE71C);
    LCD_ShowString(0, 80, lcddev.width, 24, 0x001F, 24, (uint8_t *)"Megawin Company");
    LCD_ShowString(0, 104, lcddev.width, 24, 0x33E0, 24, (uint8_t *)"ARM Cortex-M0 Series");
    LCD_ShowString(0, 128, lcddev.width, 24, 0xF100, 24, (uint8_t *)"Product MG32F02A");
    Delay(1000);
    do{
        LCD_ShowString(rand() % 240, rand() % 320, lcddev.width, 24, rand() % 0xFFFF, 24, (uint8_t *)"Megawin Company");
        LCD_ShowString(rand() % 240, rand() % 320, lcddev.width, 24, rand() % 0xFFFF, 24, (uint8_t *)"ARM Cortex-M0 Series");
        LCD_ShowString(rand() % 240, rand() % 320, lcddev.width, 24, rand() % 0xFFFF, 24, (uint8_t *)"Product MG32F02A");	
    }while(-- lCount != 0);
}


void LCD_Random_Line(void)
{
    uint16_t lCount = 500;
    LCD_Clear(BLACK);
    do{
        LCD_DrawLine(rand() % 240, rand() % 320, rand() % 240, rand() % 320, rand() % 0xFFFF);
    }while(-- lCount != 0);
}

void LCD_Random_Rectangle(void)
{
    uint16_t lCount = 200;
    LCD_Clear(BLACK);
    do{
        LCD_DrawRectangle(rand() % 240, rand() % 320, rand() % 240, rand() % 320, rand() % 0xFFFF);
    }while(-- lCount != 0);
}

void LCD_Random_Fill(void)
{
    uint16_t lCount = 200;
    LCD_Clear(BLACK);
    do{
        LCD_Fill(rand() % 120, rand() % 160, rand() % 120, rand() % 160, rand() % 0xFFFF);
    }while(-- lCount != 0);
}

void LCD_Random_Circle(void)
{
    uint16_t lCount = 200;
    LCD_Clear(BLACK);
    do{
        LCD_Draw_Circle(rand() % 240, rand() % 320, rand() % 320, rand() % 0xFFFF);
    }while(-- lCount != 0);
}

void LCD_Random_Geometry(void)
{
    uint16_t lCount = 100;
    LCD_Clear(BLACK);
    do{
        LCD_DrawLine(rand() % 240, rand() % 320, rand() % 240, rand() % 320, rand() % 0xFFFF);
        LCD_DrawRectangle(rand() % 240, rand() % 320, rand() % 240, rand() % 320, rand() % 0xFFFF);
        LCD_Draw_Circle(rand() % 240, rand() % 320, rand() % 320, rand() % 0xFFFF);
    }while(-- lCount != 0);
}



typedef enum{
    LCD_Show_Logo = 1,
    LCD_Show_Text1,
//    LCD_Show_Text1 = 1,
    LCD_Show_GrayBar_1,
    LCD_Show_ColorBar1,
    LCD_Show_ColorBar2,
    LCD_Show_Line,
    LCD_Show_Rectangle,
    LCD_Show_Circle,
//    LCD_Show_Fill,
    LCD_Show_Geometry,
    LCD_Show_Text2,
    LCD_Show_Stop,
}LCD_Show_Typedef;



uint32_t            SPIFlashID;
uint32_t            ShowDelay = 0;
uint32_t            ShowDelayCapture;
MatrixKeyDef        key, oldkey;
RGB_MODE_TYPE       RGB1_Mode = RGB_Mode_SPECTRUM;
RGB_MODE_TYPE       RGB2_Mode = RGB_Mode_SPECTRUM;
RGB_MODE_TYPE       RGB3_Mode = RGB_Mode_SPECTRUM;
uint8_t             RGB1_light = 255;
uint8_t             RGB2_light = 255;
uint8_t             RGB3_light = 255;
uint8_t             DemoLoopEnable = 1;



void LCD_Show(uint8_t Template)
{
    switch(Template){
        case LCD_Show_Logo:

            SPIFlashID = MID_Flash_Read_ID();
				if((SPIFlashID == 0xFFFFFF) || (SPIFlashID == 0x00000000))
            {
                __LCD_BackLight_Off();
                LCD_Clear(BLACK);
                LCD_ShowString(0, 24, lcddev.width, 24, BRED, 24, (uint8_t *)"SPI Flash Not Ready");
                __LCD_BackLight_On();
            }
            else
						{
								LCD_Clear(BLACK);
								Begin_Time = GetTick();
								LCD_Show_Pictureu(1);
								Finish_Time = GetTick();
								time=Finish_Time-Begin_Time;
								time = 1000.0/time;
								sprintf((char *)lcd_id, "FLASH SPEED(Data from SPI):%.2lf Page/s", time);//?LCD ID???lcd_id??
								LCD_ShowString(0, 20, lcddev.width, 10, RED, 12, lcd_id);

						}

        break;
        case LCD_Show_GrayBar_1:
            LCD_GrayBar_1();
        break;
        case LCD_Show_ColorBar1:
            LCD_ColorBar_1();
        break;
        case LCD_Show_ColorBar2:
            LCD_ColorBar_2();
        break;
        case LCD_Show_Text1:
            LCD_Show_Text();
        break;
        case LCD_Show_Line:
            LCD_Random_Line();
        break;
        case LCD_Show_Rectangle:
            LCD_Random_Rectangle();
        break;
        case LCD_Show_Circle:
            LCD_Random_Circle();
        break;
//        case LCD_Show_Fill:
//            LCD_Random_Fill();
//        break;       
        case LCD_Show_Geometry:
            LCD_Random_Geometry();
        break;
        case LCD_Show_Text2:
            LCD_Random_Text();
        break;
        case LCD_Show_Stop:
        break;
    }
}



int main()
{
    LCD_Show_Typedef DemoTemplate = (LCD_Show_Typedef)2;
    LCD_Show_Typedef OldDemoTemplate = (LCD_Show_Typedef)0;
    uint8_t lcd_id[80];
    // Tick Initial
    // DRV_Return InitTick(uint32_t TickClock, uint32_t TickPriority)
    InitTick(48000000, 0);

    URT_Init(URT0);         // for Debug Information
    //URT_Init(URT2);         // for Debug Information
    printf("%c[2J",27);
    printf("%c[H",27);

    API_Flash_Init();       /* Need include to ChipInit(); */
    //printf("  SPI Flash ID %X \n\r", MID_Flash_Read_ID());    /* Read Flash ID */ // MID
    SPIFlashID = MID_Flash_Read_ID();
		PD3 = 1;								//MX25L3206 FLASH HOLD DISABLE;
		PB0 = 1;								//反相
    LCD_Init();
    API_Key_Init();
    API_RGBLED_Init();      // RGB1 / RGB2 / RGB3 Inital ( RGB1/2/3 inital is OFF)
    IRQ_Init();
		
		DMA1_Init();					//DAC DMA初始化
		TM00_Init();					//DAC DMA发声用定时
		DAC0_Init();					//DAC 初始化
    printf("Initial Ready \n\r");

		WavStatus=WAVE_IDLE;
		WavInx=0;
		
    // Setting RGB1 Color
    API_RGBColor_Select(&RGB1, 0, 255, 0);
    API_RGBColor_Select(&RGB2, 255, 0, 0);
    API_RGBColor_Select(&RGB3, 0, 0, 255);

    // Setting RGB1 Mode
    API_RGBMode_Select(&RGB1, RGB_Mode_SPECTRUM);
    API_RGBMode_Select(&RGB2, RGB_Mode_SPECTRUM);
    API_RGBMode_Select(&RGB3, RGB_Mode_SPECTRUM);

    LCD_Clear(BLACK);
    __LCD_BackLight_Off();
    LCD_ShowString(0, 0, lcddev.width, 24, GREEN, 24, (uint8_t *)"Initial Ready");
    sprintf((char *)lcd_id, "SPI Flash ID:%X", SPIFlashID);//?LCD ID???lcd_id??
    LCD_ShowString(0, 24, lcddev.width, 24, GBLUE, 24, lcd_id);
    sprintf((char *)lcd_id, "LCD ID:%04X",lcddev.id);//?LCD ID???lcd_id??
    LCD_ShowString(0, 48, lcddev.width, 24, GBLUE, 24, lcd_id);
    sprintf((char *)lcd_id, "Resolution %d x %d", lcddev.width, lcddev.height);//?LCD ID???lcd_id??
    LCD_ShowString(0, 72, lcddev.width, 24, YELLOW, 24, lcd_id);
    __LCD_BackLight_On();

    OldDemoTemplate = (LCD_Show_Typedef)0;
    DemoTemplate = (LCD_Show_Typedef)0;
    ShowDelayCapture = GetTick();
    ShowDelay = 1000;

    oldkey = NoPressKey;
    do{
				WaveProc();
				WaveLoopProc();
			if(DemoLoopEnable != 0)
        {
            if((GetTick() - ShowDelayCapture) >= ShowDelay)
            {
                DemoTemplate++;
                if(DemoTemplate >= LCD_Show_Stop)
                    DemoTemplate = (LCD_Show_Typedef)1;
                ShowDelayCapture = GetTick();
            }
        }
        if((TextList.ShowEnable == 0) && (OldDemoTemplate != DemoTemplate))
        {
            OldDemoTemplate = DemoTemplate;
            LCD_Show(DemoTemplate);
            switch(DemoTemplate){
                //case LCD_Show_Logo:
                case LCD_Show_Text1:
                case LCD_Show_GrayBar_1:
                case LCD_Show_ColorBar1:
                case LCD_Show_ColorBar2:
                    ShowDelay = 3000;
                    break;

                case LCD_Show_Line:
                case LCD_Show_Rectangle:
                case LCD_Show_Circle:
                //LCD_Show_Fill,
                case LCD_Show_Geometry:
                case LCD_Show_Text2:
                    ShowDelay = 1000;
                    break;
                case LCD_Show_Stop:
                    break;
            }
            ShowDelayCapture = GetTick();
        }

        key = API_ReadKey();
        if(oldkey == key)
            continue;

        oldkey = key;
        //printf("    Key = %d\n\r", key);

        switch(key){
            case SKey1:
                if(RGB2_Mode == RGB_Mode_SPECTRUM)
                    RGB2_Mode = RGB_Mode_OFF;
                else
                    RGB2_Mode ++;
                API_RGBMode_Select(&RGB2, RGB2_Mode);
                API_AddText2List((uint8_t *)"Left LED Mode", 20, 16, YELLOW);
                break;
            case SKey2:
                if (RGB2_light == 255)
                    RGB2_light = 0;
                else
                    if(RGB2_light > 204)
                        RGB2_light = 255;
                    else
                        RGB2_light += 51;
                RGB2_Mode = RGB_Mode_STATIC;
                API_RGBMode_Select(&RGB2, RGB_Mode_STATIC);
                API_RGBColor_Select(&RGB2, RGB2_light, 0, 0); 
                API_AddText2List((uint8_t *)"Left LED PWM +", 20, 16, YELLOW);
                break;
            case SKey3:
                if (RGB2_light == 0)
                    RGB2_light = 255;
                else
                    if(RGB2_light < 51)
                        RGB2_light = 0;
                    else
                        RGB2_light -= 51;
                RGB2_Mode = RGB_Mode_STATIC;
                API_RGBMode_Select(&RGB2, RGB_Mode_STATIC);
                API_RGBColor_Select(&RGB2, RGB2_light, 0, 0); 
                API_AddText2List((uint8_t *)"Left LED PWM -", 20, 16, YELLOW);
                break;
            case SKey4:
                DemoLoopEnable = 0;
                API_AddText2List((uint8_t *)"Escape", 20, 16, YELLOW);
                break;
            case SKey5: // Left
                DemoLoopEnable = 0;
                DemoTemplate--;
                if(DemoTemplate == (LCD_Show_Typedef)0)
                    DemoTemplate = LCD_Show_Text2;
                API_AddText2List((uint8_t *)"Left <", 20, 16, YELLOW);
                break;
            case SKey6:
								WAVE_PlayStop();						//停止播放
								WaveWaitingTime=10000;
								Wav_Count=0;
//                if(TextList.ShowEnable == 0)
//                    DemoTemplate++;
                TextList.ShowEnable = 0;
                ShowDelay = 0;
                ShowDelayCapture = GetTick();
                DemoLoopEnable = 1;

								if(WavLoopEnable==0)
								{
									WavLoopEnable=1;
									API_AddText2List((uint8_t *)"Loop Mode", 20, 16, YELLOW);
								}
								else 
								{
									WavLoopEnable=0;
									API_AddText2List((uint8_t *)"Stop Loop Mode", 20, 16, YELLOW);
								}
                break;
            case SKey7:
                if(RGB1_Mode == RGB_Mode_SPECTRUM)
                    RGB1_Mode = RGB_Mode_OFF;
                else
                    RGB1_Mode ++;
                API_RGBMode_Select(&RGB1, RGB1_Mode);
                API_AddText2List((uint8_t *)"Center LED Mode", 20, 16, YELLOW);
                break;
            case SKey8:
                if (RGB1_light == 255)
                    RGB1_light = 0;
                else
                    if(RGB1_light > 204)
                        RGB1_light = 255;
                    else
                        RGB1_light += 51;
                RGB1_Mode = RGB_Mode_STATIC;
                API_RGBMode_Select(&RGB1, RGB_Mode_STATIC);
                API_RGBColor_Select(&RGB1, 0, RGB1_light, 0);
                API_AddText2List((uint8_t *)"Center LED PWM +", 20, 16, YELLOW);
                break;
            case SKey9:
                if (RGB1_light == 0)
                    RGB1_light = 255;
                else
                    if(RGB1_light < 51)
                        RGB1_light = 0;
                    else
                        RGB1_light -= 51;
                RGB1_Mode = RGB_Mode_STATIC;
                API_RGBMode_Select(&RGB1, RGB_Mode_STATIC);
                API_RGBColor_Select(&RGB1, 0, RGB1_light, 0); 
                API_AddText2List((uint8_t *)"Center LED PWM -", 20, 16, YELLOW);
                break;
            case SKey10:// Up
                DemoLoopEnable = 0;
                API_AddText2List((uint8_t *)"Up ^", 20, 16, YELLOW);
                break;
            case SKey11://增加音量
                DemoLoopEnable = 0;
								if(WavVolume==0)
								{
									WavVolume+=1;
									API_AddText2List((uint8_t *)"Volume +", 20, 16, YELLOW);
								}
								else if(WavVolume==1)
								{
									WavVolume+=2;
									API_AddText2List((uint8_t *)"Volume +", 20, 16, YELLOW);
								}
								else if(WavVolume==3)
								{
									API_AddText2List((uint8_t *)"Already Maximum Volume ", 20, 16, YELLOW);
								}
                DAC_CurrentMode_Select(DAC, WavVolume);    								//音量加
                break;
            case SKey12://减少音量
                DemoLoopEnable = 0;
								if(WavVolume==3)
								{
									WavVolume-=2;
									API_AddText2List((uint8_t *)"Volume -", 20, 16, YELLOW);
								}
								else if(WavVolume==1)
								{
									WavVolume-=1;
									API_AddText2List((uint8_t *)"Volume -", 20, 16, YELLOW);
								}
								else if(WavVolume==0)
								{
									API_AddText2List((uint8_t *)"Already Minimum Volume ", 20, 16, YELLOW);
								}
                DAC_CurrentMode_Select(DAC, WavVolume);    								//音量减
                break;
            case PKey1:
                if(RGB3_Mode == RGB_Mode_SPECTRUM)
                    RGB3_Mode = RGB_Mode_OFF;
                else
                    RGB3_Mode ++;
                API_RGBMode_Select(&RGB3, RGB3_Mode);
                API_AddText2List((uint8_t *)"Right LED Mode", 20, 16, YELLOW);
                break;
            case PKey2:
                if (RGB3_light == 255)
                    RGB3_light = 0;
                else
                    if(RGB3_light > 204)
                        RGB3_light = 255;
                    else
                        RGB3_light += 51;
                RGB3_Mode = RGB_Mode_STATIC;
                API_RGBMode_Select(&RGB3, RGB_Mode_STATIC);
                API_RGBColor_Select(&RGB3, 0, 0, RGB3_light);
                API_AddText2List((uint8_t *)"Right LED PWM +", 20, 16, YELLOW);
                break;
            case PKey3:
                if (RGB3_light == 0)
                    RGB3_light = 255;
                else
                    if(RGB3_light < 51)
                        RGB3_light = 0;
                    else
                        RGB3_light -= 51;
                RGB3_Mode = RGB_Mode_STATIC;
                API_RGBMode_Select(&RGB3, RGB_Mode_STATIC);
                API_RGBColor_Select(&RGB3, 0, 0, RGB3_light);
                API_AddText2List((uint8_t *)"Right LED PWM -", 20, 16, YELLOW);
                break;
            case PKey4:
                DemoLoopEnable = 0;
                API_AddText2List((uint8_t *)"Return", 20, 16, YELLOW);
                break;
            case PKey5: // Right
                DemoLoopEnable = 0;
								LCD_Random_Line();
                break;
            case PKey6:
                DemoLoopEnable = 0;
								WavLoopEnable = 0;		//禁止循环语音
                TextList.ShowEnable = 1;
                API_AddText2List((uint8_t *)"Manual Mode!!", 20, 16, YELLOW);
								WAVE_PlayStop();
								Delay(10);
								WAVE_PlayStart(WAVAddress[Wav_Count]);
								Wav_Count++;
								if(Wav_Count>=WAV_CNT_MAX)
								{
									Wav_Count=0;
								}
                break;
            case NoPressKey:
            default:
                break;
        }
    }while(1);
}

