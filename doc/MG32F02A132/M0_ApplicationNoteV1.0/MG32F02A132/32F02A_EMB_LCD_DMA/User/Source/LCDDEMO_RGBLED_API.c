
/**
 ******************************************************************************
 *
 * @file        LCDDEMO_RGBLED_API.c
 *
 * @brief       This file provides firmware functions to manage the following 
 *              functionalities of the ADC peripheral:
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/02/12
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *>>
 *--
 *--
 *>>
 *>>
 *
 @endif 
 ******************************************************************************
 */

#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_GPIO_DRV.H"
#include "MG32x02z_URT_DRV.H"
#include "MG32x02z_Common_MID.h"
#include "LCDDEMO_RGBLED_API.h"





TM_HandleTypeDef    htm26, htm36;


uint8_t      *RGBX_CTR;
RGB_TYPE     RGB1;
RGB_TYPE     RGB2;
RGB_TYPE     RGB3;

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGBLED_Init(void)
{
    TM_OC_InitTypeDef   TM26PWM, TM36PWM;
    URT_BRG_TypeDef     URT_BRGStruct;
    URT_Struct*         URTX;

    // ------------------------------------------------------------------------
    // initial TM26 for PWM function mode
    htm26.Instance = TM26;
    TM_TimeBaseStruct_Init(&htm26.TimeBaseInit);
    {   // modify parameter for APB=48MHz (PWM cycle = 100 Hz)
        // prescaler = 48000000(APB clock)/256(8 bit resolution)/100(Hz) = 1875
        htm26.TimeBaseInit.TM_Prescaler = 1874;  
        htm26.TimeBaseInit.TM_Period = 255;
    }
    MID_TM_PWM_Init(&htm26);
    
    // ------------------------------------------------------------------------
    // set channel0 for 8bitx2 PWM mode (OC0H)
    {   // modify parameter
        TM26PWM.OCMode = TM_8bitx2PWM;
        TM26PWM.ReloadValue = 0x0000;       // Low byte = OC0L, High byte = OC0H
        
        TM26PWM.OCHIdleState = SET;         // OC0H idle state = 'H'   (R)
        TM26PWM.OCHInverse = ENABLE;       // without inversion
    }
    MID_TM_PWM_ConfigChannel(&htm26, &TM26PWM, MID_TM_Channel0);
    
    // ------------------------------------------------------------------------
    // set channel0 for 8bitx2 PWM mode (OC1N, OC1H)
    {   // modify parameter
        TM26PWM.OCMode = TM_8bitx2PWM;
        TM26PWM.ReloadValue = 0x0000;       // Low byte = OC1L, High byte = OC1H
        
        TM26PWM.OCxEnable = DISABLE;        // disable OC10 output
        
        TM26PWM.OCNEnable = ENABLE;         // enable OC1N output   (B)
        TM26PWM.OCNIdleState = SET;         // idle state = 'H'
        TM26PWM.OCNInverse = DISABLE;        // with inversion
        
        TM26PWM.OCHIdleState = SET;         // OC1H idle state = 'H'   (G)
        TM26PWM.OCHInverse = ENABLE;        // without inversion
    }
    MID_TM_PWM_ConfigChannel(&htm26, &TM26PWM, MID_TM_Channel1);
    
    
    
    
    
    // ------------------------------------------------------------------------
    // initial TM36 for PWM function mode
    htm36.Instance = TM36;
    TM_TimeBaseStruct_Init(&htm36.TimeBaseInit);
    {   // modify parameter for APB=48MHz (PWM cycle = 100 Hz)
        // prescaler = 48000000(APB clock)/256(8 bit resolution)/1000(Hz) = 187.5
        htm36.TimeBaseInit.TM_Prescaler = 187;  
        htm36.TimeBaseInit.TM_Period = 255;
    }
    MID_TM_PWM_Init(&htm36);
    
    // ------------------------------------------------------------------------
    // set channel0 for 8bitx2 PWM mode 
//    {   // modify parameter
//        TM36PWM.OCMode = TM_8bitx2PWM;
//        TM36PWM.ReloadValue = 0xFFFF;       // Low byte = OCxL, High byte = OCxH
//        
//        TM36PWM.OCNEnable = DISABLE;        
//        TM36PWM.OCxEnable = DISABLE;        
//    }
    {   // modify parameter
        TM36PWM.OCMode = TM_8bitx2PWM;
        TM36PWM.ReloadValue = 0xFFFF;       // Low byte = OC1L, High byte = OC1H
        
        TM36PWM.OCxEnable = ENABLE;         // disable OCx output
        TM36PWM.OCxIdleState = SET;         // idle state = 'H'
        TM36PWM.OCxInverse = DISABLE;       // with inversion
        
        TM36PWM.OCNEnable = DISABLE;        // enable OC1N output
        
        TM36PWM.OCHIdleState = SET;         // OC1H idle state = 'H'
        TM36PWM.OCHInverse = DISABLE;       // without inversion
    }

    MID_TM_PWM_ConfigChannel(&htm36, &TM36PWM, MID_TM_Channel0);
    MID_TM_PWM_ConfigChannel(&htm36, &TM36PWM, MID_TM_Channel1);
    MID_TM_PWM_ConfigChannel(&htm36, &TM36PWM, MID_TM_Channel2);
    
    MID_TM_PWM_Start_IT(&htm36, MID_TM_ChannelAll);

   
    
    
    // ------------------------------------------------------------------------
    //RGB Parameter inital
    RGB1.RGB_MODE = RGB_Mode_OFF;
    RGB2.RGB_MODE = RGB_Mode_OFF;
    RGB3.RGB_MODE = RGB_Mode_OFF;

    API_UpdateRGB3_R(0);  
    API_UpdateRGB3_G(0);      
    API_UpdateRGB3_B(0);
    
    // disable RGB LED output
    RGB1_R = RGB1_G = RGB1_B = 1;
    RGB2_R = RGB2_G = RGB2_B = 1;
    
    // ------------------------------------------------------------------------
    // enable PWM output
    MID_TM_PWM_Start(&htm26);
    MID_TM_PWM_Start(&htm36);
    
    GPIO_PinFunction_Select(PIND(11), 6);
    GPIO_PinFunction_Select(PIND(12), 7);
    GPIO_PinFunction_Select(PIND(13), 7);
    
    
    // ------------------------------------------------------------------------
    //RGB Control Timer Inital (use URT3 Timeout timer overflow
    URTX = URT3;
    //URT3 baudrate generator configuration
    URT_BRGStruct.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRGStruct.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRGStruct.URT_BaudRateCounterReload = 0;
    URT_BRGStruct.URT_PrescalerCounterReload = 7;
    URT_BaudRateGenerator_Config(URTX,&URT_BRGStruct); 
    URT_BaudRateGenerator_Cmd(URTX,ENABLE);
    //Set RX clock 
    URT_RXClockSource_Select(URTX,URT_RXClock_Internal);
    URT_RXOverSamplingSampleNumber_Select(URTX,5);
    URT_RX_Cmd(URTX,ENABLE);
    
    //Timeout timer configuration
    URT_SetTimeoutTimerCompare(URTX,10000);
    URT_TimeoutTimerClockSource_Select(URTX,URT_TMOCLK_BitTime);
    URT_TimeoutMode_Select(URTX,URT_TMOMDS_General);
    URT_TimeroutTimer_Cmd(URTX,ENABLE);
    URT_Cmd(URTX,ENABLE);
    
    
    
    URT_IT_Cmd(URTX,(URT_IT_TMO|URT_IT_UG),ENABLE);
    URT_ITEA_Cmd(URTX,ENABLE);
    NVIC_EnableIRQ(URT123_IRQn);
}


/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGBLED_SW(void)
{
    uint32_t TM36_FLAG;

    TM36_FLAG = TM_GetAllFlagStatus(TM36);
    
    // ------------------------------------------------------------------------
    // Edge Align -> RGB1 & RGB2 set to '1'
    if(TM36_FLAG & TMx_TOF)
    {
        RGB12_RGB_OFF;            
    } 
    else
    {
        /*============RGB2================*/
        if(RGB2.RGB_SWITCH==RGB_ON)
        {
            // ------------------------------------------------------------------------
            // compare2-H
            if(TM36_FLAG & TMx_CF2B)
            {
                RGB2_G_ON;
            }
            
            // compare2-L
            if(TM36_FLAG & TMx_CF2A)
            {
                RGB2_R_ON;
            }
        
            // ------------------------------------------------------------------------
            // compare1-H
            if(TM36_FLAG & TMx_CF1B)
            {
                RGB2_B_ON;
            }
        }
        else
        {
            RGB2_RGB_OFF;
        }
        /*============RGB1================*/
        if(RGB1.RGB_SWITCH==RGB_ON)
        {
            // compare1-L
            if(TM36_FLAG & TMx_CF1A)
            {
                RGB1_G_ON;        
            }   
            // ------------------------------------------------------------------------
            // compare0-H
            if(TM36_FLAG & TMx_CF0B)
            {
                RGB1_R_ON;
            }
            // compare0-L
            if(TM36_FLAG & TMx_CF0A)
            {
                RGB1_B_ON;        
            }
        }
        else
        {
            RGB1_RGB_OFF;
        }
    
    }
    TM_ClearFlag(TM36,TM36_FLAG);
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGB_Percentage_Table(uint8_t BR_STEP , uint8_t BR_BASE,uint8_t type)
{   
    uint8_t i;
    uint16_t Tmp;
    
       
    for(i=0;i<3;i++)
    {
        Tmp = BR_STEP * RGBX_CTR[RGB_COLOR_R_TABLE + i];
        
        RGBX_CTR[RGB_COLOR_R + i] = Tmp/BR_BASE;
        
        if(type!=0)
        {
            Tmp = BR_STEP * RGBX_CTR[RGB_COLOR_R_TABLE + i];
            RGBX_CTR[RGB_COLOR_R + i] = Tmp / BR_BASE;
        }
    }    
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGB_STEP(void)
{
    if(RGBX_CTR[RGB_SUB_STEP] < RGBX_CTR[RGB_SUB_STEP_CMP])
    {
        RGBX_CTR[RGB_SUB_STEP] = RGBX_CTR[RGB_SUB_STEP] + 1;
    }
    else
    {
        if(RGBX_CTR[RGB_MAIN_STEP]== RGBX_CTR[RGB_MAIN_STEP_CMP])
        {
            RGBX_CTR[RGB_MAIN_STEP] = 0;
        }
        else
        {
            RGBX_CTR[RGB_MAIN_STEP] = RGBX_CTR[RGB_MAIN_STEP] + 1;   
        }
        RGBX_CTR[RGB_SUB_STEP] = 0;
    } 
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_LED_Plusing_Drama()
{
    uint8_t Tmp;


    Tmp = RGBX_CTR[RGB_SUB_STEP] % 2;
    if(Tmp ==0)
    { 
        for(Tmp = 0; Tmp<RGB_LEN ; Tmp++)
        {
            RGBX_CTR[RGB_COLOR_R+Tmp] = 0; 
        }    
    }
    else
    {           
        RGBX_CTR[RGB_COLOR_R] = RGBX_CTR[RGB_COLOR_R_TABLE];
        RGBX_CTR[RGB_COLOR_G] = RGBX_CTR[RGB_COLOR_G_TABLE];
        RGBX_CTR[RGB_COLOR_B] = RGBX_CTR[RGB_COLOR_B_TABLE];
    }
    API_RGB_STEP();
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGB_Breathing_Drama(void)
{   
    if(RGBX_CTR[RGB_MAIN_STEP] == 0)
    {
        API_RGB_Percentage_Table(RGBX_CTR[RGB_SUB_STEP],RGBX_CTR[RGB_SUB_STEP_CMP],1);
    }
    else
    {
        API_RGB_Percentage_Table(RGBX_CTR[RGB_SUB_STEP_CMP]-RGBX_CTR[RGB_SUB_STEP],RGBX_CTR[RGB_SUB_STEP_CMP],1);
    }
    
    API_RGB_STEP();
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
#define RGB_BRIGHT     5

void API_RGB_Spectrum_Inital(void)
{   
    RGBX_CTR[RGB_COLOR_R] = 255;
    RGBX_CTR[RGB_COLOR_G] = RGB_BRIGHT;
    RGBX_CTR[RGB_COLOR_B] = 0;
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGB_Spectrum_Drama(void)
{
    API_RGB_STEP();

    if(RGBX_CTR[RGB_MAIN_STEP]==0)
    {
        if(RGBX_CTR[RGB_SUB_STEP]==1)
        {
            API_RGB_Spectrum_Inital();
        }
        RGBX_CTR[RGB_COLOR_G] = RGBX_CTR[RGB_COLOR_G] + RGB_BRIGHT;
    }
    else if(RGBX_CTR[RGB_MAIN_STEP]==1)
    {
        RGBX_CTR[RGB_COLOR_R] = RGBX_CTR[RGB_COLOR_R] - RGB_BRIGHT;
    }
    else if(RGBX_CTR[RGB_MAIN_STEP]==2)
    {
        RGBX_CTR[RGB_COLOR_B] = RGBX_CTR[RGB_COLOR_B] + RGB_BRIGHT;
    }
    else if(RGBX_CTR[RGB_MAIN_STEP]==3)
    {
        RGBX_CTR[RGB_COLOR_G] = RGBX_CTR[RGB_COLOR_G] - RGB_BRIGHT;   
    }
    else if(RGBX_CTR[RGB_MAIN_STEP]==4)
    {
        RGBX_CTR[RGB_COLOR_R] = RGBX_CTR[RGB_COLOR_R] + RGB_BRIGHT;
    }
    else if(RGBX_CTR[RGB_MAIN_STEP]==5)
    { 
        RGBX_CTR[RGB_COLOR_B] = RGBX_CTR[RGB_COLOR_B] - RGB_BRIGHT;
    }
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGBPWM_Config(RGB_TYPE* RGBX)
{
    RGBX_CTR = &(RGBX->RGB_CTR[RGB_COLOR_R_TABLE]);
    
    switch(RGBX->RGB_MODE)
    {
        case RGB_Mode_STATIC:
                                RGBX->RGB_CTR[RGB_COLOR_R] = RGBX->RGB_CTR[RGB_COLOR_R_TABLE];
                                RGBX->RGB_CTR[RGB_COLOR_G] = RGBX->RGB_CTR[RGB_COLOR_G_TABLE];
                                RGBX->RGB_CTR[RGB_COLOR_B] = RGBX->RGB_CTR[RGB_COLOR_B_TABLE];
                                break;
        case RGB_Mode_PULSING:
                                API_LED_Plusing_Drama();
                                break;
        case RGB_Mode_BREATH:
                                API_RGB_Breathing_Drama();
                                break;
        case RGB_Mode_SPECTRUM:
                                API_RGB_Spectrum_Drama();
                                break;
        case RGB_Mode_OFF:
        default:
                                RGBX->RGB_CTR[RGB_COLOR_R] = 0;
                                RGBX->RGB_CTR[RGB_COLOR_G] = 0;
                                RGBX->RGB_CTR[RGB_COLOR_B] = 0;
                                RGBX->RGB_SWITCH = RGB_OFF;
                                return;
    }
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGB_Config(void)
{  
    //-------------------------------------------------------------
    //RGB1 
    RGB1.RGB_CTR[RGB_TEMPO_CNT] = RGB1.RGB_CTR[RGB_TEMPO_CNT] + 1;
    if(RGB1.RGB_SWITCH==RGB_OFF)
    {
        RGB1.RGB_CTR[RGB_TEMPO_CNT] = 0;
    }
    else if(RGB1.RGB_CTR[RGB_TEMPO_CNT]>=RGB1.RGB_CTR[RGB_TEMPO_CNT_CMP])
    {
        API_RGBPWM_Config(&RGB1);
        RGB1.RGB_CTR[RGB_TEMPO_CNT] = 0;
        
        API_StopUpdateDutyCycle(&htm36);
        API_UpdateRGB1_R((255-RGB1.RGB_CTR[RGB_COLOR_R])+1);            
        API_UpdateRGB1_G((255-RGB1.RGB_CTR[RGB_COLOR_G])+1);      
        API_UpdateRGB1_B((255-RGB1.RGB_CTR[RGB_COLOR_B])+1);
        API_StartUpdateDutyCycle(&htm36);
    }
    //-------------------------------------------------------------
    //RGB2 
    RGB2.RGB_CTR[RGB_TEMPO_CNT] = RGB2.RGB_CTR[RGB_TEMPO_CNT] + 1;
    if(RGB2.RGB_SWITCH==RGB_OFF)
    {
        RGB2.RGB_CTR[RGB_TEMPO_CNT] = 0;
    }
    else if(RGB2.RGB_CTR[RGB_TEMPO_CNT]>=RGB2.RGB_CTR[RGB_TEMPO_CNT_CMP])
    {
        API_RGBPWM_Config(&RGB2);
        RGB2.RGB_CTR[RGB_TEMPO_CNT] = 0;
        
        API_StopUpdateDutyCycle(&htm36);
        API_UpdateRGB2_R((255-RGB2.RGB_CTR[RGB_COLOR_R])+1);            
        API_UpdateRGB2_G((255-RGB2.RGB_CTR[RGB_COLOR_G])+1);      
        API_UpdateRGB2_B((255-RGB2.RGB_CTR[RGB_COLOR_B])+1); 
        API_StartUpdateDutyCycle(&htm36);
    }
    //-------------------------------------------------------------
    //RGB3 
    RGB3.RGB_CTR[RGB_TEMPO_CNT] = RGB3.RGB_CTR[RGB_TEMPO_CNT] + 1;
    if(RGB3.RGB_SWITCH==RGB_OFF)
    {
        RGB3.RGB_CTR[RGB_TEMPO_CNT] = 0;
    }
    else if(RGB3.RGB_CTR[RGB_TEMPO_CNT]>=RGB3.RGB_CTR[RGB_TEMPO_CNT_CMP])
    {
        API_RGBPWM_Config(&RGB3);
        RGB3.RGB_CTR[RGB_TEMPO_CNT] = 0;
        
  
        API_StopUpdateDutyCycle(&htm26);
        API_UpdateRGB3_R(RGB3.RGB_CTR[RGB_COLOR_R]);  
        API_UpdateRGB3_G(RGB3.RGB_CTR[RGB_COLOR_G]);      
        API_UpdateRGB3_B(RGB3.RGB_CTR[RGB_COLOR_B]); 
        API_StartUpdateDutyCycle(&htm26);
    }
    URT_ClearITFlag(URT3,(URT_IT_TMO|URT_IT_UG));
}
/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGBMode_Select(RGB_TYPE* RGBX, RGB_MODE_TYPE RGB_Mode)
{
    uint8_t i;
    
    RGBX->RGB_SWITCH = RGB_OFF;
    RGBX->RGB_MODE = RGB_Mode;
    for(i=0;i<RGB_CTR_LEN - 3;i++)
    {
        RGBX->RGB_CTR[RGB_MAIN_STEP] = 0;
    }
    switch(RGB_Mode)
    {
        case RGB_Mode_STATIC:    
                                 RGBX->RGB_CTR[RGB_TEMPO_CNT_CMP] = 1;
                                 break;
        case RGB_Mode_PULSING:
                                 RGBX->RGB_CTR[RGB_MAIN_STEP_CMP] = 1 - 1;
                                 RGBX->RGB_CTR[RGB_SUB_STEP_CMP] =  2 - 1;
                                 RGBX->RGB_CTR[RGB_TEMPO_CNT_CMP] = 75;
                                 break;
        case RGB_Mode_BREATH:
                                 RGBX->RGB_CTR[RGB_MAIN_STEP_CMP] = 2 - 1;
                                 RGBX->RGB_CTR[RGB_SUB_STEP_CMP] =  120 - 1;
                                 RGBX->RGB_CTR[RGB_TEMPO_CNT_CMP] = 2;
                                 break;
        case RGB_Mode_SPECTRUM:
                                 RGBX->RGB_CTR[RGB_MAIN_STEP_CMP] = 6 - 1;
                                 RGBX->RGB_CTR[RGB_SUB_STEP_CMP] =  51 - 1;
                                 RGBX->RGB_CTR[RGB_TEMPO_CNT_CMP] = 3;
                                 break;
        case RGB_Mode_OFF:
        default:
                                 RGBX->RGB_MODE = RGB_Mode_OFF;
                                 if(RGB3.RGB_SWITCH == RGB_OFF)
                                 {
                                     API_StopUpdateDutyCycle(&htm26);
                                     API_UpdateRGB3_R(0);  
                                     API_UpdateRGB3_G(0);      
                                     API_UpdateRGB3_B(0); 
                                     API_StartUpdateDutyCycle(&htm26);
                                 }
                                 return;
    }
    RGBX->RGB_SWITCH = RGB_ON;
    
    if(RGB3.RGB_SWITCH==RGB_ON)
    {
        API_StartUpdateDutyCycle(&htm26);
    }
}

/**
 *******************************************************************************
 * @brief	    
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 * @bug                 
 *******************************************************************************
 */
void API_RGBColor_Select(RGB_TYPE* RGBX, uint8_t RGB_R_Param , uint8_t RGB_G_Param , uint8_t RGB_B_Param)
{
    RGBX->RGB_SWITCH = RGB_OFF;
    RGBX->RGB_CTR[RGB_COLOR_R_TABLE] = RGB_R_Param;
    RGBX->RGB_CTR[RGB_COLOR_G_TABLE] = RGB_G_Param;
    RGBX->RGB_CTR[RGB_COLOR_B_TABLE] = RGB_B_Param;
    RGBX->RGB_SWITCH = RGB_ON;
}




