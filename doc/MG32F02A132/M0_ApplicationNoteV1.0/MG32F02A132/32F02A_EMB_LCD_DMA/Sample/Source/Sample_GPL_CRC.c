/**
 ******************************************************************************
 *
 * @file        Sample_GPL_CRC.c
 *
 * @brief       This is the C code format driver file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal  
 */


#include "MG32x02z_DRV.h"

#define GPL_InputDataEndian_LITTLE      GPL_CR0_BEND_EN_disable_w
#define GPL_InputDataEndian_BIG         GPL_CR0_BEND_EN_mask_w

#define IS_GPL_InputDataEndian_MODE(MODE)     (((MODE) == GPL_InputDataEndian_LITTLE) || \
                                               ((MODE) == GPL_InputDataEndian_BIG))



#define GPL_InputDataInverse_DISABLE    GPL_CR0_IN_INV_disable_w
#define GPL_InputDataInverse_ENABLE     GPL_CR0_IN_INV_mask_w

#define IS_GPL_InputDataInverse_MODE(MODE)    (((MODE) == GPL_InputDataInverse_ENABLE) || \
                                               ((MODE) == GPL_InputDataInverse_DISABLE))


#define GPL_InputDataReverse_NONE       GPL_CR0_BREV_MDS_disable_w
#define GPL_InputDataReverse_BYTE       GPL_CR0_BREV_MDS_8bit_w
#define GPL_InputDataReverse_HALFWORD   GPL_CR0_BREV_MDS_16bit_w
#define GPL_InputDataReverse_WORD       GPL_CR0_BREV_MDS_32bit_w

#define IS_GPL_InputDataReverse_MODE(MODE)    (((MODE) == GPL_InputDataReverse_NONE) || \
                                               ((MODE) == GPL_InputDataReverse_BYTE) || \
                                               ((MODE) == GPL_InputDataReverse_HALFWORD) || \
                                               ((MODE) == GPL_InputDataReverse_WORD))



#define GPL_OutputDataReverse_NONE      GPL_CR1_CRC_BREV_disable_w
#define GPL_OutputDataReverse_BYTE      GPL_CR1_CRC_BREV_8bit_w
#define GPL_OutputDataReverse_HALFWORD  GPL_CR1_CRC_BREV_16bit_w
#define GPL_OutputDataReverse_WORD      GPL_CR1_CRC_BREV_32bit_w

#define IS_GPL_OutputDataReverse_MODE(MODE)    (((MODE) == GPL_OutputDataReverse_NONE) || \
                                                ((MODE) == GPL_OutputDataReverse_BYTE) || \
                                                ((MODE) == GPL_OutputDataReverse_HALFWORD) || \
                                                ((MODE) == GPL_OutputDataReverse_WORD))



#define GPL_CRC_Polynomial_0x1021       GPL_CR1_CRC_MDS_ccitt16_w
#define GPL_CRC_Polynomial_0x07         GPL_CR1_CRC_MDS_crc8_w
#define GPL_CRC_Polynomial_0x8005       GPL_CR1_CRC_MDS_crc16_w
#define GPL_CRC_Polynomial_0x4C11DB7    GPL_CR1_CRC_MDS_crc32_w

#define IS_GPL_CRC_Polynomial_MODE(MODE)    (((MODE) == GPL_CRC_Polynomial_0x1021) || \
                                             ((MODE) == GPL_CRC_Polynomial_0x07) || \
                                             ((MODE) == GPL_CRC_Polynomial_0x8005) || \
                                             ((MODE) == GPL_CRC_Polynomial_0x4C11DB7))



#define GPL_CRC_InputDataWidth_8bit     GPL_CR1_CRC_DSIZE_8bit_w
#define GPL_CRC_InputDataWidth_16bi     GPL_CR1_CRC_DSIZE_16bit_w
#define GPL_CRC_InputDataWidth_32bit    GPL_CR1_CRC_DSIZE_32bit_w

#define IS_GPL_CRC_InputDataWidth_MODE(MODE)    (((MODE) == GPL_CRC_InputDataWidth_8bit) || \
                                                 ((MODE) == GPL_CRC_InputDataWidth_16bi) || \
                                                 ((MODE) == GPL_CRC_InputDataWidth_32bit))



typedef struct{
    uint32_t InputDataInverse;
    uint32_t InputDataEndian;
    uint32_t InputDataReverse;
    uint32_t CRC_Polynomial;
    uint32_t CRC_InputDataWidth;
    uint32_t OutputDataReverse;
    uint32_t CRC_InitialValue;
}GPL_CRC_InitTypedef;



void GPL_CRC_Config(GPL_CRC_InitTypedef * GPL_CRC)
{
    GPL->CR1.W = 0;
    GPL->CR0.W = 0;
    GPL->DIN.W = 0;
    GPL->CR0.W = GPL_CRC->InputDataInverse | 
                 GPL_CRC->InputDataEndian | 
                 GPL_CRC->InputDataReverse;

    GPL->CRCINIT.W = GPL_CRC->CRC_InitialValue;

    GPL->CR1.W = GPL_CRC->CRC_Polynomial |
                 GPL_CRC->CRC_InputDataWidth |
                 GPL_CRC->OutputDataReverse;

    GPL->CR1.B[0] |= GPL_CR1_CRC_EN_mask_b0;    
}


const uint8_t DataPattern[] = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39};



/**
 *******************************************************************************
 * @brief       Sample_GPL_CRC8_0x07
 * @details     1. GPL Initialze
 *      \n      2. 
 *      \n      3. 
 *      \n      4. 
 *
 * @return      No
 * @exception   None
 * @note        
 * @par         Example
 * @code        
                Sample_GPL_CRC8_0x07_Normal();
 * @endcode     
 * @par         Modify
 *              void Sample_GPL_CRC8_0x07_Normal(void);
 * @bug         
 *******************************************************************************
 * @internal
 * @sign    V
 * @endinternal  
 */
void Sample_GPL_CRC8_0x07(void)
{
    uint8_t lCount;
    GPL_CRC_InitTypedef lGPL_CRC;

    // width=8 poly=0x07 init=0x00 refin=false refout=false xorout=0x00 check=0xf4 residue=0x00 name="CRC-8"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x07;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.B[0] != 0xF4)
        printf("Error CRC8 0x07 not equal is 0xF4, Now is 0x%2X Fail.\n\r", GPL->DOUT.B[0]);   

//=============================================================================
    // width=8 poly=0x07 init=0x00 refin=false refout=false xorout=0x55 check=0xa1 residue=0xac name="CRC-8/ITU"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x07;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.B[0] ^ 0x55) != 0xA1)
        printf("Error CRC8 0x07 ITU not equal is 0xA1, Now is 0x%2X Fail.\n\r", (GPL->DOUT.B[0] ^ 0x55));  

//=============================================================================
    // width=8 poly=0x07 init=0xff refin=true refout=true xorout=0x00 check=0xd0 residue=0x00 name="CRC-8/ROHC"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.CRC_InitialValue = 0xFFUL;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x07;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_BYTE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.B[0] != 0xD0)
        printf("Error CRC8 0x07 ROHC not equal is 0xD0, Now is 0x%2X Fail.\n\r", GPL->DOUT.B[0]);
}



/**
 *******************************************************************************
 * @brief       Sample_GPL_CRC16_0x8005
 * @details     1. GPL Initialze
 *      \n      2. 
 *      \n      3. 
 *      \n      4. 
 *
 * @return      No
 * @exception   None
 * @note        
 * @par         Example
 * @code        
                Sample_GPL_CRC16_0x8005();
 * @endcode     
 * @par         Modify
 *              void Sample_GPL_CRC16_0x8005(void);
 * @bug         
 *******************************************************************************
 * @internal
 * @sign    V
 * @endinternal  
 */
void Sample_GPL_CRC16_0x8005(void)
{
    uint8_t lCount;
    GPL_CRC_InitTypedef lGPL_CRC;

    // width=16 poly=0x8005 init=0x0000 refin=false refout=false xorout=0x0000 check=0xfee8 residue=0x0000 name="CRC-16/BUYPASS"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x8005;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.H[0] != 0xFEE8)
        printf("Error CRC16 0x8005 BUYPASS not equal 0xFEE8, Now is 0x%4X Fail.\n\r", GPL->DOUT.H[0]);  

//=============================================================================
    // width=16 poly=0x8005 init=0x0000 refin=true refout=true xorout=0x0000 check=0xbb3d residue=0x0000 name="ARC"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x8005;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.H[0] != 0xBB3D)
        printf("Error CRC16 0x8005 ARC not equal 0xBB3D, Now is 0x%4X Fail.\n\r", GPL->DOUT.H[0]);  

//=============================================================================
    // width=16 poly=0x8005 init=0x0000 refin=true refout=true xorout=0xffff check=0x44c2 residue=0xb001 name="CRC-16/MAXIM"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x8005;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0xFFFF) != 0x44C2)
        printf("Error CRC16 0x8005 MAXIM not equal 0x44C2, Now is 0x%4X Fail.\n\r", GPL->DOUT.H[0]);

//=============================================================================
    // width=16 poly=0x8005 init=0x800d refin=false refout=false xorout=0x0000 check=0x9ecf residue=0x0000 name="CRC-16/DDS-110"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x8005;
    lGPL_CRC.CRC_InitialValue = 0x800DUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.H[0] != 0x9ECF)
        printf("Error CRC16 0x8005 DDS-110 not equal 0x9ECF, Now is 0x%4X Fail.\n\r", GPL->DOUT.H[0]);

//=============================================================================
    // width=16 poly=0x8005 init=0xffff refin=false refout=false xorout=0x0000 check=0xaee7 residue=0x0000 name="CRC-16/CMS"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x8005;
    lGPL_CRC.CRC_InitialValue = 0xFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.H[0] != 0xAEE7)
        printf("Error CRC16 0x8005 CMS not equal 0xAEE7, Now is 0x%4X Fail.\n\r", GPL->DOUT.H[0]);

//=============================================================================
    // width=16 poly=0x8005 init=0xffff refin=true refout=true xorout=0xffff check=0xb4c8 residue=0xb001 name="CRC-16/USB"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x8005;
    lGPL_CRC.CRC_InitialValue = 0xFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0xFFFF) != 0xB4C8)
        printf("Error CRC16 0x8005 USB not equal 0xB4C8, Now is 0x%4X Fail.\n\r", GPL->DOUT.H[0]);

//=============================================================================
    // width=16 poly=0x8005 init=0xffff refin=true refout=true xorout=0x0000 check=0x4b37 residue=0x0000 name="MODBUS"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x8005;
    lGPL_CRC.CRC_InitialValue = 0xFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.H[0] != 0x4B37)
        printf("Error CRC16 0x8005 MODBUS not equal 0x4B37, Now is 0x%4X Fail.\n\r", GPL->DOUT.H[0]);
}



/**
 *******************************************************************************
 * @brief       Sample_GPL_CRC16_0x1021
 * @details     1. GPL Initialze
 *      \n      2. 
 *      \n      3. 
 *      \n      4. 
 *
 * @return      No
 * @exception   None
 * @note        
 * @par         Example
 * @code        
                Sample_GPL_CRC16_0x1021();
 * @endcode     
 * @par         Modify
 *              void Sample_GPL_CRC16_0x1021(void);
 * @bug         
 *******************************************************************************
 * @internal
 * @sign    V
 * @endinternal  
 */
void Sample_GPL_CRC16_0x1021(void)
{
    uint8_t lCount;
    GPL_CRC_InitTypedef lGPL_CRC;

//=============================================================================
    // width=16 poly=0x1021 init=0x0000 refin=false refout=false xorout=0x0000 check=0x31c3 residue=0x0000 name="XMODEM"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);
    
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.H[0] != 0x31C3)
        printf("Error CRC8 0x1021 XMODEM not equal 0x31C3, Now is 0x%4X Fail.\n\r", GPL->DOUT.H[0]);

//=============================================================================
    // width=16 poly=0x1021 init=0x0000 refin=false refout=false xorout=0xffff check=0xce3c residue=0x1d0f name="CRC-16/GSM"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0xFFFF) != 0xCE3C)
        printf("Error CRC8 0x1021 GSM not equal 0xCE3C, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0xFFFF));

//=============================================================================
    // width=16 poly=0x1021 init=0x0000 refin=true refout=true xorout=0x0000 check=0x2189 residue=0x0000 name="KERMIT"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0x0000) != 0x2189)
        printf("Error CRC8 0x1021 KERMIT not equal 0x2189, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0x0000));

//=============================================================================
    // width=16 poly=0x1021 init=0x1d0f refin=false refout=false xorout=0x0000 check=0xe5cc residue=0x0000 name="CRC-16/AUG-CCITT"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0x1D0FUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0x0000) != 0xE5CC)
        printf("Error CRC8 0x1021 AUG-CCITT not equal 0xE5CC, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0x0000));

//=============================================================================
    // width=16 poly=0x1021 init=0x89ec refin=true refout=true xorout=0x0000 check=0x26b1 residue=0x0000 name="CRC-16/TMS37157"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0x89ECUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0x0000) != 0x26B1)
        printf("Error CRC8 0x1021 TMS37157 not equal 0x26B1, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0x0000));

//=============================================================================
    // width=16 poly=0x1021 init=0xb2aa refin=true refout=true xorout=0x0000 check=0x63d0 residue=0x0000 name="CRC-16/RIELLO"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0xB2AAUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0x0000) != 0x63D0)
        printf("Error CRC8 0x1021 RIELLO not equal 0x63D0, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0x0000));

//=============================================================================
    // width=16 poly=0x1021 init=0xc6c6 refin=true refout=true xorout=0x0000 check=0xbf05 residue=0x0000 name="CRC-A"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0xC6C6UL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0x0000) != 0xBF05)
        printf("Error CRC8 0x1021 CRC-A not equal 0xBF05, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0x0000));

//=============================================================================
    // width=16 poly=0x1021 init=0xffff refin=false refout=false xorout=0x0000 check=0x29b1 residue=0x0000 name="CRC-16/CCITT-FALSE"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0xFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0x0000) != 0x29B1)
        printf("Error CRC8 0x1021 CCITT-FALSE not equal 0x29B1, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0x0000));

//=============================================================================
    // width=16 poly=0x1021 init=0xffff refin=false refout=false xorout=0xffff check=0xd64e residue=0x1d0f name="CRC-16/GENIBUS"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0xFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0xFFFF) != 0xD64E)
        printf("Error CRC8 0x1021 GENIBUS not equal 0xD64E, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0xFFFF));

//=============================================================================
    // width=16 poly=0x1021 init=0xffff refin=true refout=true xorout=0x0000 check=0x6f91 residue=0x0000 name="CRC-16/MCRF4XX"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0xFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0x0000) != 0x6F91)
        printf("Error CRC8 0x1021 MCRF4XX not equal 0x6F91, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0x0000));

//=============================================================================
    // width=16 poly=0x1021 init=0xffff refin=true refout=true xorout=0xffff check=0x906e residue=0xf0b8 name="X-25"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x1021;
    lGPL_CRC.CRC_InitialValue = 0xFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_HALFWORD;
    GPL_CRC_Config(&lGPL_CRC);
    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.H[0] ^ 0xFFFF) != 0x906E)
        printf("Error CRC8 0x1021 X-25 not equal 0x906E, Now is 0x%4X Fail.\n\r", (GPL->DOUT.H[0] ^ 0xFFFF));
}



/**
 *******************************************************************************
 * @brief       Sample_GPL_CRC32_0x04c11db7
 * @details     1. GPL Initialze
 *      \n      2. 
 *      \n      3. 
 *      \n      4. 
 *
 * @return      No
 * @exception   None
 * @note        
 * @par         Example
 * @code        
                Sample_GPL_CRC32_0x04c11db7();
 * @endcode     
 * @par         Modify
 *              void Sample_GPL_CRC32_0x04c11db7(void);
 * @bug         
 *******************************************************************************
 * @internal
 * @sign    V
 * @endinternal  
 */
void Sample_GPL_CRC32_0x04c11db7(void)
{
    uint32_t lCount;
    GPL_CRC_InitTypedef lGPL_CRC;

//=============================================================================
    // width=32 poly=0x04c11db7 init=0x00000000 refin=false refout=false xorout=0xffffffff check=0x765e7680 residue=0xc704dd7b name="CRC-32/POSIX"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x4C11DB7;
    lGPL_CRC.CRC_InitialValue = 0UL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.W ^ 0xffffffff) != 0x765e7680)
        printf("Error CRC32 0x04c11db7 POSIX not equal 0x765e7680, Now is 0x%8X Fail.\n\r", (GPL->DOUT.W ^ 0xffffffff));

//=============================================================================
    // width=32 poly=0x04c11db7 init=0xffffffff refin=false refout=false xorout=0x00000000 check=0x0376e6e7 residue=0x00000000 name="CRC-32/MPEG-2"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x4C11DB7;
    lGPL_CRC.CRC_InitialValue = 0xFFFFFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if(GPL->DOUT.W != 0x0376e6e7UL)
        printf("Error CRC32 0x04c11db7 MPEG-2 not equal 0x0376e6e7, Now is 0x%8X Fail.\n\r", GPL->DOUT.W);

//=============================================================================
    // width=32 poly=0x04c11db7 init=0xffffffff refin=true refout=true xorout=0xffffffff check=0xcbf43926 residue=0xdebb20e3 name="CRC-32"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x4C11DB7;
    lGPL_CRC.CRC_InitialValue = 0xFFFFFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_WORD;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.W ^ 0xffffffff) != 0xCBF43926UL)
        printf("Error CRC32 0x04c11db7 not equal 0xCBF43926, Now is 0x%8X Fail.\n\r", (GPL->DOUT.W ^ 0xffffffff));

//=============================================================================
    // width=32 poly=0x04c11db7 init=0xffffffff refin=false refout=false xorout=0xffffffff check=0xfc891918 residue=0xc704dd7b name="CRC-32/BZIP2"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x4C11DB7;
    lGPL_CRC.CRC_InitialValue = 0xFFFFFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_NONE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_NONE;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.W ^ 0xffffffff) != 0xFC891918UL)
        printf("Error CRC32 0x04c11db7 BZIP2 not equal 0xFC891918, Now is 0x%8X Fail.\n\r", (GPL->DOUT.W ^ 0xffffffff));

//=============================================================================
    // width=32 poly=0x04c11db7 init=0xffffffff refin=true refout=true xorout=0x00000000 check=0x340bc6d9 residue=0x00000000 name="JAMCRC"
    lGPL_CRC.InputDataInverse = GPL_InputDataInverse_DISABLE;
    lGPL_CRC.InputDataEndian = GPL_InputDataEndian_LITTLE;
    lGPL_CRC.CRC_InputDataWidth = GPL_CRC_InputDataWidth_8bit;
    lGPL_CRC.CRC_Polynomial = GPL_CRC_Polynomial_0x4C11DB7;
    lGPL_CRC.CRC_InitialValue = 0xFFFFFFFFUL;
    lGPL_CRC.InputDataReverse = GPL_InputDataReverse_BYTE;
    lGPL_CRC.OutputDataReverse = GPL_OutputDataReverse_WORD;
    GPL_CRC_Config(&lGPL_CRC);

    lCount = 0;
    do{
        GPL->DIN.B[0] = DataPattern[lCount];
    }while(++ lCount < 9);

    if((GPL->DOUT.W ^ 0x00000000) != 0x340BC6D9UL)
        printf("Error CRC32 0x04c11db7 JAMCRC not equal 0x340BC6D9, Now is 0x%8X Fail.\n\r", (GPL->DOUT.W ^ 0x00000000));
}


