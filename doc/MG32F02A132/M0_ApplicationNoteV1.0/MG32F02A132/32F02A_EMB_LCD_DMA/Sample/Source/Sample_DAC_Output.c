/**
  ******************************************************************************
 *
 * @file        Sample_DAC_Output.c
 *
 * @brief       DAC: Output Code by current level 
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/06/16
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */
#include "Sample.h"

#if Sample_Code_Macro_Select == Sample_Code_Macro_DAC && Sample_Code_DAC == Sample_Code_DAC_Output

#include "MG32x02z_DAC_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***
//  <h> DAC (current type) Output level (initial value)
//      <o0> Output Level (ratio x/1024) <0-1023>
//      <i> DAC: current type / input range 0~1023     
//  </h>
#define DAC_Output          512

//*** <<< end of configuration section >>>    ***


/**
 *******************************************************************************
 * @brief	    Set DAC output 
 * @details     1.De-initial DAC0
 *    \n        2.Select Update mode be software setting
 *    \n        3.Config DAC_10BitData + DAC_RightJustified
 *    \n        4.set current level
 *    \n        5.Update the DAC value
 *    \n        6.Enable DAC & output
 * @note        
 *******************************************************************************
 */
void Sample_DAC_Output(void)
{  
    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->DAC = Enable
    //Configure Peripheral On Mode Clock->Port B = Enable
	
    //==Set GPIO init 
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->PB2
    //GPIO port initial is 0xFFFF
    //PB2 mode is AIO
    //PB2 function GPB2
	
    // ------------------------------------------
    // 1.De-initial DAC
    DAC_DeInit(DAC);            
    
    // ------------------------------------------
    // 2.Select Update mode be software setting
    DAC_TriggerSource_Select(DAC, DAC_SoftWare);
    
    // ------------------------------------------
    // 3.Config DAC_10BitData + DAC_RightJustified
    DAC_DataResolution_Select(DAC, DAC_10BitData);
    DAC_DataAlignment_Select(DAC, DAC_RightJustified);

    // ------------------------------------------
    // 4.set current level
    DAC_CurrentMode_Select(DAC, DAC_M1);    // DAC_M1: ~1mA (Full scale)

    // ------------------------------------------
	// 5.Update the DAC value
    DAC_SetDAT0(DAC, DAC_Output);
    
    // ------------------------------------------
	// 6.Enable DAC & output
    DAC_Cmd(DAC, ENABLE);       

    return;
}


#endif

