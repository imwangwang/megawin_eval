/**
  ******************************************************************************
 *
 * @file        Sample_RTC_Init.c
 *
 * @brief       RTC sample code
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/05/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * #001_Hades_20180530
 *  >>
 *
 @endif
 *******************************************************************************
 */

#include "Sample.h"
 
#if Sample_Code_Macro_Select == Sample_Code_Macro_RTC && Sample_Code_RTC == Sample_Code_RTC_Init 

#include "MG32x02z_DRV.h"


/**
 *******************************************************************************
 * @brief  	    Sample RTC init
 * @details     1. Enable CSC to RTC clock
 *      \n      2. Configure RTC clock
 *      \n      3. Set RTC timer value
 *      \n      4. Update ALM value
 *      \n      5. Enable RTC module
 *      \n      6. Wait Alarm
 *      \n      7. Get RTC timer value
 *      \n      8. Disable RTC module
 * @return	    None	
 * @note        
 * @par         Example
 * @code
    Sample_RTC_Init();
 * @endcode
 * @bug              
 *******************************************************************************
 */
void Sample_RTC_Init (void)
{
    uint32_t RTC_CNT;

//@del{
    printf("\n\nSample_RTC_Init");                                  // Show RTC sampel code start
//@del} 
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12Mz
    //Select CK_MAIN Source = CK_HS
    //Select CK_LS Source = CK_ILRCO
    //Configure Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->RTC = Enable
    //
    //MG32x02z_GPIO_Init.H(Configuration Wizard)
    //Select GPIO Initial -> GPIOC configuration -> PC7 configuration
    //Select Pin mode = PPO
    //Select Pin alternate function = RTC_OUT
    //Other selection keep default value

//    /*=== 1. Enable CSC to RTC clock ===*/
//    UnProtectModuleReg(CSCprotect);                                 // Unprotect CSC module
//    CSC_PeriphOnModeClock_Config(CSC_ON_RTC, ENABLE);               // Enable RTC module clock
//    ProtectModuleReg(CSCprotect);                                   // protect CSC module
      
    /*=== 2. Configure RTC clock ===*/
    UnProtectModuleReg(RTCprotect);                                 // Unprotect RTC module
    RTC_CLK_Select(RTC_CK_LS);                                      // RTC clock source = CK_LS
    RTC_PreDivider_Select(RTC_PDIV_4096);                           // PDIV output = RTC clock / 4096
    RTC_Divider_Select(RTC_DIV_8);                                  // DIV output = (RTC clock / 4096) / 8
    RTC_OutputSignal_Select(RTC_PC);                                // RTC output = DIV otuput frequency
    
    /*=== 3. Set RTC timer value ===*/
    RTC_RCR_Mode_Select(RTC_RCR_MOD_ForceReload);                   // RTC switch to reload mode
    RTC_SetReladReg(0);                                             // Set reload data
    RTC_TriggerStamp_SW();                                          // Trigger reload data update to RTC timer
    while(RTC_GetSingleFlagStatus(RTC_RCRF) == DRV_UnHappened);     // Waiting reload complete
    RTC_ClearFlag(RTC_ALLF);                                        // Clear flag
    
    /*=== 4. Update ALM value ===*/
    if(RTC_GetAlarmState() == DRV_True)                             // When alarm function enable
    {
        RTC_Alarm_Cmd(DISABLE);                                     // Disable alarm function
        while(RTC_GetSingleFlagStatus(RTC_RCRF) == DRV_UnHappened); // Waiting alarm function disable  
        RTC_ClearFlag(RTC_RCRF);                                    // Clear flag RCRF
    }
    RTC_SetAlarmCompareValue(10);                                   // Set alarm compare value
    RTC_Alarm_Cmd(ENABLE);                                          // Enable Alarm function
    
    /*=== 5. Enable RTC module ===*/
    RTC_Cmd (ENABLE);                                               // Enable RTC module

    /*=== 6. Wait Alarm ===*/
    while(RTC_GetSingleFlagStatus(RTC_ALMF) == DRV_UnHappened);     // Wait Alarm happened
    RTC_ClearFlag(RTC_ALMF);                                        // Clear Alarm flag
    
    /*=== 7. Get RTC timer value ===*/
    RTC_RCR_Mode_Select(RTC_RCR_MOD_DirectlyCapture);               // RTC switch to delay capture mode
    RTC_ClearFlag(RTC_PCF | RTC_RCRF);                              // Clear flag PCF and RCRF
    RTC_TriggerStamp_SW();                                          // Trigger RTC capture timer data
    while(RTC_GetSingleFlagStatus(RTC_RCRF) == DRV_UnHappened);     // Waiting capture complete
    RTC_CNT = RTC_GetCaptureReg();                                  // Get capture register data
//@del{
    printf("\nRTC_CNT: 0x%08x", RTC_CNT);                           // Show RTC CNT
//@del}
    /*=== 8. Disable RTC module ===*/
    RTC_Cmd(DISABLE);                                               // Disable RTC module
    ProtectModuleReg(RTCprotect);                                   // Protect RTC module

}

#endif


