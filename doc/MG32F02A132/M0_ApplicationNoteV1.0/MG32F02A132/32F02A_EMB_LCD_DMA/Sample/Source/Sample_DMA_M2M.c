/**
  ******************************************************************************
 *
 * @file        Sample_DMA_M2M.c
 *
 * @brief       DMA: Memory to Memory 
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/06/16
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2016 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */

#include "MG32x02z_DMA_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***
//  <h> DMA (memory to memory)
//      <o0> Data Length , unit: byte <0-65535>
//  </h>
#define MYBINARYIMAGE2_LENGTH          40

//*** <<< end of configuration section >>>    ***

uint8_t Src[MYBINARYIMAGE2_LENGTH] __attribute__((at(0x20001000)));
uint8_t Dest[MYBINARYIMAGE2_LENGTH] __attribute__((at(0x20003800)));


/**
 *******************************************************************************
 * @brief	    Copy Source Memory to Destination Memory by DMA macro
 * @details     1.Enable DMA
 *    \n        2.Enable Channel0
 *    \n        3.Setting M2M simple parameter (only support DMA Channel 0)
 *    \n        4.clear flag
 *    \n        5.start request
 *    \n        6.until complete & clear flag
 *******************************************************************************
 */
void Sample_DMA_MEM(void)
{  
    // make sure :
    // Enable DMA clock from CSC macro (reference MG32x02z_CSC_Init.h file)
    
    DMA_BaseInitTypeDef DMATestPattern;

    // ------------------------------------------------------------------------
    // 1.Enable DMA
    DMA_Cmd(ENABLE);
    
    // ------------------------------------------------------------------------
    // 2.Enable Channel0
    DMA_Channel_Cmd(DMAChannel0, ENABLE);
    
    // ------------------------------------------------------------------------
    DMA_BaseInitStructure_Init(&DMATestPattern);
    
    // 3.initial & modify parameter
    {   
        // DMA channel select
        DMATestPattern.DMAChx = DMAChannel0;
        
        // channel x source/destination auto increase address
        DMATestPattern.SrcSINCSel = ENABLE;
        DMATestPattern.DestDINCSel = ENABLE;
        
        // DMA source peripheral config
        DMATestPattern.SrcSymSel = DMA_MEM_Read;
        
        // DMA destination peripheral config
        DMATestPattern.DestSymSel = DMA_MEM_Write;
        
        // DMA Burst size config
        DMATestPattern.BurstDataSize = DMA_BurstSize_1Byte;
        
        // DMA transfer data count initial number
        DMATestPattern.DMATransferNUM = MYBINARYIMAGE2_LENGTH;
    
        // source/destination config
        DMATestPattern.DMASourceAddr = (uint8_t *)&Src;
        DMATestPattern.DMADestinationAddr = (uint8_t *)&Dest;
    }
    
    // ------------------------------------------------------------------------
    // Setting M2M simple parameter
    DMA_Base_Init(&DMATestPattern);
    
    // ------------------------------------------------------------------------
    // 4.clear flag
    DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
    
    // ------------------------------------------------------------------------
    // 5.start request
    DMA_StartRequest(DMAChannel0);
       
    // ------------------------------------------------------------------------
    // 6.until complete & clear flag
    while (DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH0_TCF) == DRV_UnHappened);
    DMA_ClearFlag(DMA, DMA_FLAG_CH0_TCF);
    
    return;
}




