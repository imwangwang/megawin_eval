/**
 ******************************************************************************
 *
 * @file        Sample_SPI_Slave_StandardSPI.c
 *
 * @brief       SPI slave standard SPI sample code
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 *******************************************************************************
 */
 
#include "Sample.h" 
 
#if Sample_Code_Macro_Select == Sample_Code_Macro_SPI && Sample_Code_SPI == Sample_Code_SPI0_Slave 

 
#include "MG32x02z_DRV.h"

#define Dummy_Data 0xFFFFFFFF


/**
 *******************************************************************************
 * @brief  	    Sample SPI Slave Standard SPI
 * @details     1. Enable CSC to SPI clock
 *      \n      2. DeInitial SPI
 *      \n      3. Configure clock divider
 *      \n      4. Configure SPI data line, mode and data size...
 *      \n      5. Config SPI0 IO
 *      \n      6. Enable SPI 
 *      \n      7. Read data
 *      \n      8. Send data
 *      \n      9. Disable SPI
 *      \n      
 * @return	    	
 * @note        
 * @par         Example
 * @code
    Sample_SPI_SlaveStandardSPI();
 * @endcode
 *******************************************************************************
 */
void Sample_SPI_SlaveStandardSPI (void)
{
    PIN_InitTypeDef PINX_InitStruct;
    uint32_t RDAT;
    uint32_t TDAT;

    /*=== 1. Enable CSC to SPI clock ===*/
    //[A] When Use Wizard
    //Configure Peripheral On Mode Clock->SPI0 = Enable and Select SPI0_PR Source = CK_APB
    //Configure Peripheral On Mode Clock->Port B = Enable
    //[B] When Use Driver
//    UnProtectModuleReg(CSCprotect);                             // Unprotect CSC module
//    CSC_PeriphOnModeClock_Config(CSC_ON_SPI0, ENABLE);          // Enable SPI0 module clock
//    CSC_PeriphOnModeClock_Config(CSC_ON_PortB, ENABLE);		  // Enable PortB clock
//    CSC_PeriphProcessClockSource_Config(CSC_SPI0_CKS, CK_APB);  // CK_SPIx_PR = CK_APB = 12MHz
//    ProtectModuleReg(CSCprotect);                               // protect CSC module
       
    /*=== 2. DeInitial SPI ===*/
    SPI_DeInit(SPI0);
   
    /*=== 3. Configure clock divider ===*/                      // SPI clock = 1Mhz
    SPI_Clock_Select(SPI0, SPI_CK_SPIx_PR);                     // CK_SPIx = CK_SPIx_PR
    SPI_PreDivider_Select(SPI0, SPI_PDIV_2);                    // PDIV outpu = CK_SPIx /2
    SPI_Prescaler_Select(SPI0, SPI_PSC_3);                      // Prescaler outpu = PDIV outpu /3
    SPI_Divider_Select(SPI0, SPI_DIV_2);                        // DIV outpu = PDIV outpu /2
    
    /*=== 4. Configure SPI data line, mode and data size... ===*/
    SPI_DataLine_Select(SPI0, SPI_Standard);                    // SPI data line standard SPI
    SPI_ModeAndNss_Select(SPI0, SPI_Slave);                     // Slave
    SPI_ClockPhase_Select(SPI0, SPI_LeadingEdge);               // CPHA = 0
    SPI_ClockPolarity_Select(SPI0, SPI_Low);                    // CPOL = 0                        
    SPI_FirstBit_Select(SPI0, SPI_MSB);                         // MSB first
    SPI_DataSize_Select(SPI0, SPI_8bits);                       // Data size 8bits
    SPI_SlaveModeReceivedThreshold_Select(SPI0, SPI_1Byte);     // Set SPI0 received data buffer high threshold

    /*=== 5. Config SPI0 IO ===*/
    PINX_InitStruct.PINX_Mode				= PINX_Mode_Digital_I;      // Pin select digital input mode
    PINX_InitStruct.PINX_PUResistant        = PINX_PUResistant_Enable;  // Enable pull up resistor
    PINX_InitStruct.PINX_Speed              = PINX_Speed_Low;           
    PINX_InitStruct.PINX_OUTDrive           = PINX_OUTDrive_Level0;     // Pin output driver full strength.
    PINX_InitStruct.PINX_FilterDivider      = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
    PINX_InitStruct.PINX_Inverse            = PINX_Inverse_Disable;     // Pin input data not inverse
    PINX_InitStruct.PINX_Alternate_Function = 2;                        // Pin AFS = 2
    
    GPIO_PinMode_Config(PINB(0),&PINX_InitStruct);                      // NSS setup at PB0
    GPIO_PinMode_Config(PINB(2),&PINX_InitStruct);                      // CLK setup at PB2
    GPIO_PinMode_Config(PINB(3),&PINX_InitStruct);                      // MOSI setup at PB3
    
    PINX_InitStruct.PINX_Mode				= PINX_Mode_PushPull_O;     // Setting pusu pull mode
    GPIO_PinMode_Config(PINB(1),&PINX_InitStruct);                      // MISO setup at PB1
    
    /*=== 6. Enable SPI ===*/
    SPI_Cmd(SPI0, ENABLE);                                              // Enable SPI 
    
    /*=== 7. Read data ===*/
    // If you want get 1 byte, please send 1 byte 0x00.
    SPI_SetTxData(SPI0, SPI_1Byte, Dummy_Data);                               // Received 1 byte and send 1 byte the same time
    while(SPI_GetSingleFlagStatus(SPI0, SPI_RXF) == DRV_UnHappened);    // Wait RXF flag
    SPI_ClearFlag(SPI0, (SPI_TXF | SPI_RXF));                           // Clear TXF and RXF
    RDAT = SPI_GetRxData(SPI0);                                         // Get received data
    
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TCF flag
    SPI_ClearFlag(SPI0, SPI_TCF);                                       // Clear TCF
     
    /*=== 8. Send data ===*/
    TDAT = RDAT;
    SPI_SetTxData(SPI0, SPI_1Byte, TDAT);                               // Send 1 byte and received 1 byte the same time
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TXF) == DRV_UnHappened);    // Wait TXF flag
    SPI_ClearFlag(SPI0, (SPI_TXF | SPI_RXF));                           // Clear TXF and RXF
    
    while(SPI_GetSingleFlagStatus(SPI0, SPI_TCF) == DRV_UnHappened);    // Wait TCF flag
    SPI_ClearFlag(SPI0, SPI_TCF | SPI_RXF);                             // Clear TCF and RXF
    
    
    
    /*=== 9. Disable SPI ===*/
    while(SPI_GetNSSInputStatust(SPI0) == DRV_Low);                     // Wait SPI idle
    SPI_Cmd(SPI0, DISABLE);                                             // Disable SPI
}



#endif
