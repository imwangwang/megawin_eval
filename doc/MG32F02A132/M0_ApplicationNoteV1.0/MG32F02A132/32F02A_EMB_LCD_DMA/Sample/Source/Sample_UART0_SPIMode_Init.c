
/**
 ******************************************************************************
 *
 * @file        Sample_UART0_SPIMode_Init.c
 * @brief       The demo UART0'S initial to SPI mode sample C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/11/08
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */

#include "Sample.h"
 
#if Sample_Code_Macro_Select == Sample_Code_Macro_URTx && Sample_Code_URT == Sample_Code_URT0_SPI_Init 
 
#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_URT_DRV.H"

#define URTX    URT0

 /**
 *******************************************************************************
 * @brief	   Sample URT0 initial
 * @details     1. Set CSC initial.
 *     \n       2. Set GPIO initial.
 *     \n       3. Set RX/TX Baudrate.
 *     \n       4. Set data character.
 *     \n       5. Set URT0 mode.
 *     \n       6. Set Data line.
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
void Sample_URT0_SPIMode_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    ctype            URT_RecData;
    
    //==Set CSC init
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 11.0592M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->Port B/URT0 = Enable
    //Configure Peripheral On Mode Clock->URT0->Select URT0_PR Source = CK_APB(11.0592)
    
    //==Set GPIO init
    //1. MOSI Pin
    //    (1).MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->Pin8
    //    (2).GPIO port initial is 0xFFFF
    //    (3).Pin8 mode is PPO (Push pull output)
    //    (4).Pin8 pull-up resister Enable
    //    (5).Pin8 function URT0_TX
    //2. MISO Pin
    //    (1).MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->Pin9
    //    (2).GPIO port initial is 0xFFFF
    //    (3).Pin8 mode is ODO (Open drain)
    //    (4).Pin8 pull-up resister Enable
    //    (5).Pin9 function URT0_RX
    //3. SPICLK Pin
    //    (1).MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOC->Pin3
    //    (2).GPIO port initial is 0xFFFF
    //    (3).Pin8 mode is PPO (Push pull output)
    //    (4).Pin8 pull-up resister Enable
    //    (5).Pin3 function URT0_CLK
    //4. NSS Pin
    //    (1).MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->Pin10
    //    (2).GPIO port initial is 0xFFFF
    //    (3).Pin8 mode is PPO (Push pull output)
    //    (4).Pin8 pull-up resister Enable
    //    (5).Pin3 function URT0_NSS
    

    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 3;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 5;	                //Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    //BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 3);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 3);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTX, ENABLE);	                            //Enable TX
    URT_RX_Cmd(URTX, ENABLE);	                            //Enable RX
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_SYNC_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //==Set SPI Mode(CPHA = 0 , CPOL = 0)====//
    URT_CPHAMode_Select( URTX , URT_CPHA0_LeadEdge);
    URT_CPOLMode_Select( URTX , URT_CPOL0_Low );

    //=====Set Data Control=====//
    URT_TXGaudTime_Select(URTX, 0);
    
    
    //=====Enable CLK(SYSCLK)====//
    URT_CLKSignal_Cmd(URTX,ENABLE);
    
    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);
		
	
    //=====Send TX Data =======//
    URT_SetTXData(URTX,4,0x12345678);
    while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
    URT_ClearITFlag(URTX,URT_IT_TC);
    URT_RecData.W = URT_GetRXData(URTX);                                  //Receive effective data byte is equal to send data byte.
    

}


#endif



