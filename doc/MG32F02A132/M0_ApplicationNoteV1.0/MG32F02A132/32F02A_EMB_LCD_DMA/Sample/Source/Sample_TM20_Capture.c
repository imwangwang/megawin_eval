/**
  ******************************************************************************
 *
 * @file        Sample_TM20_Capture.c
 *
 * @brief       Input Capture for TM20 IC0
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "Sample.h"

#if Sample_Code_Macro_Select == Sample_Code_Macro_TM2x && Sample_Code_TM2 == Sample_Code_TM20_Capture

#include "MG32x02z_TM_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***

//  <h> simple function (IC0 from PB4) 
// 			<o0> IC0 capture type <1=> Rising to Rising edge <2=>Rising to Falling edge <3=> Falling to Falling edge <4=> Falling to Rising edge
//  </h>
#define TM20_IC0_TriggerSrc       1

//*** <<< end of configuration section >>>    ***

/**
 *******************************************************************************
 * @brief	    Capture main counter value when happen External Trigger event 
 * @details     1.initial & modify TimeBase parameter 
 *    \n        2.config overwrite mode (keep data)
 *    \n        3.config TM20 channel 0 function
 *    \n        4.config TRGI mode
 *    \n        5.clear all flag
 *    \n        6.Enable Timer  
 * @code
                uint32_t CaptureData;
                
                // initial IC0 (PB4) 
                // to do ...
                
                // config TM20 for IC0-32 bit mode (Keep capture data)
                // (Width) 'Rising to Rising edge' or 'Rising to Falling edge'
                // or 'Falling to Falling edge' or 'Falling to Rising edge'
                Sample_TM20_Capture();
                
                // wait for Capture event
                while(TM_GetSingleFlagStatus(TM20,TMx_CF0A) == DRV_UnHappened);
                
                // Get IC0 capture data (32-bits)
                CaptureData = TM_GetIC0Value(TM20);

                // clear TM20 flag
                TM_ClearFlag(TM20, TMx_CF0A | TMx_CF0B);
                
 *******************************************************************************
 */
void Sample_TM20_Capture(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
     

    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->TM20 = Enable
    //Configure Peripheral On Mode Clock->Port B = Enable
	
    //==Set GPIO init 
    //TM20_IC0 pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->Pin4
    //GPIO port initial is 0xFFFF
    //PB4 mode is ODO
    //PB4 function TM20_IC0
	

	 
    TM_DeInit(TM20);
    
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_MainClockDirection =TM_UpCount;
    TM_TimeBase_InitStruct.TM_Period = 65535; 
    TM_TimeBase_InitStruct.TM_Prescaler = 65535;
    TM_TimeBase_InitStruct.TM_CounterMode = Full_Counter;
    
    TM_TimeBase_Init(TM20, &TM_TimeBase_InitStruct);
   
    // ----------------------------------------------------
    // 2.config overwrite mode (keep data)
    TM_IC0OverWritten_Cmd(TM20, TM_Keep);
    
    // ----------------------------------------------------
    // 3.config TM20 channel 0 function 
    TM_CH0Function_Config(TM20, TM_InputCapture);
    TM_IN0Source_Select(TM20, IC0); // TM20_IN0 from IC0 (PB4)            
    
    // ----------------------------------------------------
    // 4.config TRGI mode
    TM_TriggerSource_Select(TM20,TRGI_IN0);
    
#if (TM20_IC0_TriggerSrc == 1)      // Rising to Rising edge 
    TM_TRGICounter_Select(TM20,TRGI_StartupRising);
    TM_TRGIPrescaler_Select(TM20,TRGI_StartupRising);
    TM_IN0TriggerEvent_Select(TM20, IC_RisingEdge);
    
#elif (TM20_IC0_TriggerSrc == 2)    // Rising to Falling edge 
    TM_TRGICounter_Select(TM20,TRGI_StartupRising);
    TM_TRGIPrescaler_Select(TM20,TRGI_StartupRising);
    TM_IN0TriggerEvent_Select(TM20, IC_FallingEdge);
    
#elif (TM20_IC0_TriggerSrc == 3)    // Falling to Falling edge 
    TM_TRGICounter_Select(TM20,TRGI_StartupFalling);
    TM_TRGIPrescaler_Select(TM20,TRGI_StartupFalling);
    TM_IN0TriggerEvent_Select(TM20, IC_FallingEdge);
    
#elif (TM20_IC0_TriggerSrc == 4)    // Falling to Rising edge
    TM_TRGICounter_Select(TM20,TRGI_StartupFalling);
    TM_TRGIPrescaler_Select(TM20,TRGI_StartupFalling);
    TM_IN0TriggerEvent_Select(TM20, IC_RisingEdge);
    
#endif   
    
    // ----------------------------------------------------
    // 5.clear all flag
    TM_ClearFlag(TM20, TMx_CF0A | TMx_CF0B);
    
    // ----------------------------------------------------
    // 6.enable Timer 
    TM_Timer_Cmd(TM20,ENABLE);

    return;
}
#endif

