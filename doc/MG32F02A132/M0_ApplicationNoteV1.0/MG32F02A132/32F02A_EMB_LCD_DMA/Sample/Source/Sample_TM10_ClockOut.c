/**
  ******************************************************************************
 *
 * @file        Sample_TM10_ClockOut.c
 *
 * @brief       Toggle TM10_CKO pin when TM10 counter overflow
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "Sample.h"

#if Sample_Code_Macro_Select == Sample_Code_Macro_TM1x &&  Sample_Code_TM1 == Sample_Code_TM10_CLKOUT

#include "MG32x02z_TM_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***

//  <h> ClockOut Setting (Output TM10_CKO GPIOB Pin 2)
//      <o0> pulse width (clks) (1~65536) <1-65536>
//      <o1> clock prescaler (1~65536)  <1-65536>
//      <o2> ClockOut overflow event from <1=> Main counter <0=> Prescaler counter
//  </h>
#define Simple_CKO_MainCounter     120
#define Simple_CKO_Prescaler       100
#define Simple_CKO_Source           1

//*** <<< end of configuration section >>>    ***

/**
 *******************************************************************************
 * @brief	    Toggle TM10_CKO pin when TM10 counter overflow
 * @details     1.initial & modify TimeBase parameter 
 *    \n        2.clear TOF flag
 *    \n        3.Clock Out config (initial state='1' & enable clock out) 
 *    \n        4.Start TM10
 * @note        if user wants to 1ms period and CK_TM10_PR is 12MHz.
 *              The Total clocks is 12M*1ms = 12000.
 *              User can set "clock prescaler"=100 and "pulse width"=120 .               
 *******************************************************************************
 */
void Sample_TM10_ClockOut(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
     

    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->TM10 = Enable
    //Configure Peripheral On Mode Clock->Port B = Enable
	
    //==Set GPIO init 
    //TM10_CKO pin config:
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->Pin1
    //GPIO port initial is 0xFFFF
    //Pin1 mode is PPO
    //Pin1 function TM10_CKO
	
      
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_Period = Simple_CKO_MainCounter - 1; 
    TM_TimeBase_InitStruct.TM_Prescaler = Simple_CKO_Prescaler - 1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
    
    TM_TimeBase_Init(TM10, &TM_TimeBase_InitStruct);
    
    // ----------------------------------------------------
    // 2.clear TOF flag
    TM_ClearFlag(TM10, TMx_TOF);
    
    // ----------------------------------------------------
    // 3.Clock Out config (initial state='1' & enable clock out) 
    TM_ClockOutSource_Select(TM10, (TM_CKOSrcDef) Simple_CKO_Source);
    TM_CKOOutputState_Init(TM10, SET);
    TM_ClockOut_Cmd(TM10, ENABLE);
    
    // ----------------------------------------------------
    // 4.Start TM10 
    TM_Timer_Cmd(TM10, ENABLE);

    return;
}

#endif

