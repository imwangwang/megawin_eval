/**
  ******************************************************************************
 *
 * @file        Sample_TM01_TRGO_UEV.c
 *
 * @brief       Output overflow update event by TM01
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "Sample.h"

#if Sample_Code_Macro_Select == Sample_Code_Macro_TM0x && Sample_Code_TM0 == Sample_Code_TM01_TRGO_UEV


#include "MG32x02z_TM_DRV.h"

//*** <<< Use Configuration Wizard in Context Menu >>> ***

//  <h> UEV Period Setting 
//      <o0> Clock Prescaler (1~256)  <1-256>
//      <o1> Main Counter (clks) (1~256) <1-256>
//  </h>
#define Simple_Period_Prescaler       100
#define Simple_Period_MainCounter     120

//*** <<< end of configuration section >>>    ***

/**
 *******************************************************************************
 * @brief	    Output main counter overflow event to another peripheral
 * @details     1.initial & modify TimeBase parameter 
 *    \n        2.clear TOF flag
 *    \n        3.select TRGO_UEV of TRGO 
 *    \n        4.Start TM01 
 * @note        if user wants to 1ms period and CK_TM01_PR is 12MHz.
 *              The Total clocks is 12M*1ms = 12000.
 *              User can set "clock prescaler"=100 and "pulse width"=120 .               
 *******************************************************************************
 */
void Sample_TM01_TRGO_UEV(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
     

    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->TM01 = Enable
	
    // ----------------------------------------------------
    // 1.initial TimeBase structure 
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_Period = Simple_Period_MainCounter - 1; 
    TM_TimeBase_InitStruct.TM_Prescaler = Simple_Period_Prescaler - 1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
    
    TM_TimeBase_Init(TM01, &TM_TimeBase_InitStruct);
    
    // ----------------------------------------------------
    // 2.clear TOF flag
    TM_ClearFlag(TM01, TMx_TOF);
    
    // ----------------------------------------------------
    // 3.select TRGO_UEV of TRGO 
    TM_TRGO_Select(TM01, TRGO_UEV);
    TM_UEV_Config(TM01, UEV_TOF);
    TM_UEV_Cmd(TM01, ENABLE);
    
    // ----------------------------------------------------
    // 4.Start TM01 
    TM_Timer_Cmd(TM01, ENABLE);

    return;
}

#endif

