





/**
 ******************************************************************************
 *
 * @file        Sample.c
 * @brief       The demo Sample C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */
 
#include "Sample.h"

#if Sample_Code_Macro_Select != NoSampleCode

#include "MG32x02z__Common_DRV.H"

#endif

/**
 *******************************************************************************
 * @brief	    Sample code
 * @details     From Sample.h to select you need sample code.
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
void Sample_Code(void)
{
    
    //===Select need sample code====
    //From Sample.h(Configuration Wizard) to select you need sample code
    //At same time only select one sample code
    
    //====MEM sample code====
    #if Sample_Code_Macro_Select == Sample_Code_Macro_MEM
        #if Sample_Code_MISC == Sample_Code_SysTickPrintfInit
            Sample_MEM_FlashIAPAccess();
        #endif
    //====GPIO sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_GPIO
        #if Sample_Code_GPIO == Sample_Code_GPIO_RWInit
            Sample_GPIO_RWInit();
        #endif
    
    //====EXIC sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_EXIC
        #if Sample_Code_EXIC == Sample_Code_EXIC_Init
            Sample_EXIC_Init();
        #endif
    
    //====IWDT sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_IWDT
        #if Sample_Code_IWDT == Sample_Code_IWDT_Init
            Sample_IWDT_Init();
        #endif
    
    //====WWDT sample code====    
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_WWDT
        #if Sample_Code_WWDT == Sample_Code_WWDT_Init
            Sample_WWDT_Init();
        #endif
    //====ADC sample code=====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_ADC
        #if Sample_Code_ADC == Sample_Code_ADC_CONV
            Sample_ADC_Conversion();
        #endif
        
    //====DAC sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_DAC
        #if Sample_Code_DAC == Sample_Code_DAC_Output
            Sample_DAC_Output();
        #endif
        
    //====CMP sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_CMP
        #if Sample_Code_CMP == Sample_Code_CMP_Init
            Sample_CMP_Init();
        #endif
        
    //====TM0x sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_TM0x
        #if Sample_Code_TM0 == Sample_Code_TM00_Dealy
            Sample_TM00_Delay();
        #elif Sample_Code_TM0 == Sample_Code_TM01_TRGO_UEV
            Sample_TM01_TRGO_UEV();
        #endif
        
    //====TM1x sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_TM1x
        #if  Sample_Code_TM1 == Sample_Code_TM10_CLKOUT
            Sample_TM10_ClockOut();
        #elif Sample_Code_TM1 == Sample_Code_TM16_AutoStop
            Sample_TM16_AutoStop();
        #endif
        
    //====TM2x sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_TM2x
        #if Sample_Code_TM2 == Sample_Code_TM20_Capture
            Sample_TM20_Capture();
        #elif Sample_Code_TM2 == Sample_Code_TM26_OutputCmp
            Sample_TM26_OC();
        #endif
        
    //====TM36 sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_TM3x
        #if Sample_Code_TM3 == Sample_Code_TM36_PWM
            Sample_TM36_PWM();
        #endif
    
    
    //====SPI sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_SPI
        #if Sample_Code_SPI == Sample_Code_SPI0_Slave
            Sample_SPI_SlaveStandardSPI();
        #elif Sample_Code_SPI == Sample_Code_SPI0_Master
            Sample_SPI_MasterStandardSPI();
        #endif
        
    //====URTx sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_URTx
        #if Sample_Code_URT == Sample_Code_URT0_Init
            Sample_URT0_Init();
        #elif Sample_Code_URT == Sample_Code_URT1_Init
            Sample_URT1_Init();
        #elif Sample_Code_URT == Sample_Code_URT2_Init
            Sample_URT2_Init();
        #elif Sample_Code_URT == Sample_Code_URT3_Init
            Sample_URT3_Init();
        #elif Sample_Code_URT == Sample_Code_URT0_DMA_Init
            Sample_URT0DMA_Init();
        #elif Sample_Code_URT == Sample_Code_URT0_SPI_Init
            Sample_URT0_SPIMode_Init();
        #endif
        
   
    //====I2C sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_I2Cx
        #if Sample_Code_I2C == Sample_Code_I2C0_ByteMode_Master
            Sample_I2C0_ByteMode_Master();
        #elif Sample_Code_I2C == Sample_Code_I2C1_ByteMode_Master
            Sample_I2C1_ByteMode_Master();
        #endif
    

    //====RTC sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_RTC
        #if Sample_Code_RTC == Sample_Code_RTC_Init
            Sample_RTC_Init();
        #elif Sample_Code_RTC == Sample_Code_RTC_PerpetualCalendar
            Sample_RTC_PerpetualCalendar();
        #endif
    
    
    //=====MISC sample code====
    #elif Sample_Code_Macro_Select == Sample_Code_Macro_MISC
        #if Sample_Code_MISC == Sample_Code_SysTickPrintfInit
            Sample_MISC_SysTickPrintfInitial();
        #endif
    
    #endif
}




















