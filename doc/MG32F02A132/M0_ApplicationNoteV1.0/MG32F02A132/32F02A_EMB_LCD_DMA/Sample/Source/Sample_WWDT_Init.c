/**
  ******************************************************************************
 *
 * @file        Sample_WWDT_Init.c
 *
 * @brief       WWDT sample code
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * #001_Hades_20180131
 *  >> File rename.
 *
 @endif
 *******************************************************************************
 */
#include "Sample.h"

#if Sample_Code_Macro_Select == Sample_Code_Macro_WWDT && Sample_Code_WWDT == Sample_Code_WWDT_Init


#include "MG32x02z_DRV.h"


/**
 *******************************************************************************
 * @brief  	    Sample IWDT init
 * @details     1. Enable CSC to WWDT clock
 *      \n      2. Configure WWDT clock
 *      \n      3. Setup threshold and reload value
 *      \n      4. Enable WWDT module
 *      \n      5. Check flag action
 *      \n      6. Disable WWDT module
 * @return	    None		
 * @note        
 * @par         Example
 * @code
    Sample_WWDT_Init();
 * @endcode
 * @bug              
 *******************************************************************************
 */
void Sample_WWDT_Init (void)
{

    
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12Mz
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Select CK_UT Divider = ILRCO/32
    //Configure Peripheral On Mode Clock->WWDT = Enable

//    /*=== 1. Enable CSC to WWDT clock ===*/
//    UnProtectModuleReg(CSCprotect);                                 // Unprotect CSC module
//    CSC_PeriphOnModeClock_Config(CSC_ON_WWDT, ENABLE);              // Enable WWDT module clock
//    ProtectModuleReg(CSCprotect);                                   // protect CSC module
    
    UnProtectModuleReg(WWDTprotect);                                // Unprotect WWDT module
    /*=== 2. Configure WWDT clock ===*/
    WWDT_DIV_Select(WWDT_DIV_1);                                    // DIV/1
    WWDT_PDIV_Select(WWDT_PDIV_1);                                  // PDIV/1
    WWDT_CLK_Select(WWDT_CK_UT);                                    // WWDT = CK_UT
    
    /*=== 3. Setup threshold and reload value ===*/
    WWDT_SetReloadReg(0x3FF);                                       // Reload register = 0x3FF
    WWDT_SetWindowThreshold(0x1FF);                                 // Window threshold = 0x1FF
    WWDT_SetWarningThreshold(0x0FF);                                // Waring threshold = 0x0FF

    /*=== 4. Enable WWDT module ===*/
    WWDT_Cmd(ENABLE);                                               // Enable WWDT module
    ProtectModuleReg(WWDTprotect);                                  // Protect WWDT module
    
    /*=== 5. Check flag action ===*/
    WWDT_RefreshCounter();                                          // Trigger WINF happened
    while(WWDT_GetSingleFlagStatus(WWDT_WINF) == DRV_UnHappened);   // Wait WINF happened
    WWDT_ClearFlag(WWDT_WINF);                                      // Clear WINF flag
    
    while(WWDT_GetSingleFlagStatus(WWDT_WRNF) == DRV_UnHappened);   // Wait WRNF happened
    WWDT_ClearFlag(WWDT_WRNF);                                      // Clear WRNF flag
    
    while(WWDT_GetSingleFlagStatus(WWDT_TF) == DRV_UnHappened);     // Wait TF happened
    WWDT_ClearFlag(WWDT_TF);                                        // Clear WRNF flag

    /*=== 6. Disable WWDT module ===*/
    UnProtectModuleReg(WWDTprotect);                                // Unprotect WWDT module
    WWDT_Cmd(DISABLE);                                              // Disable WWDT module
    ProtectModuleReg(WWDTprotect);                                  // Unprotect WWDT module

}


//@del{
/**
 *******************************************************************************
 * @brief  	    WWDT sample code
 * @details  
 * @return	    	
 * @note        
 * @par         Example
 * @code
    WWDT_Sample();
 * @endcode
 * @bug              
 *******************************************************************************
 */
void Sample_WWDT (void)
{

    // Remember enable MG32x02z_CSC_Init.h Configure Peripheral On Mode Clock -> WWDT
    UnProtectModuleReg(CSCprotect);                         // Unprotect CSC module
    CSC_PeriphOnModeClock_Config(CSC_ON_WWDT, ENABLE);      // Enable WWDT module clock
    ProtectModuleReg(CSCprotect);                           // protect CSC module
    
    // Interrupt Configure
    UnProtectModuleReg(WWDTprotect);                        // Unprotect IWDT module
    WWDT_IT_Config(WWDT_INT_WRN, ENABLE);                   // Enable WWDT counter warning can trigger interrupt
    WWDT_IT_Config(WWDT_INT_WIN, ENABLE);                   // Enable WWDT window can trigger interrupt
    WWDT_IT_Config(WWDT_INT_TF, ENABLE);                    // Enable WWDT timer timeout can trigger interrupt
    NVIC_EnableIRQ(WWDT_IRQn);                              // Enable NVIC WWDT
    
    // Setup clock divider
    WWDT_DIV_Select(WWDT_DIV_1);                            // DIV/1
    WWDT_PDIV_Select(WWDT_PDIV_1);                          // PDIV/1
    WWDT_CLK_Select(WWDT_CK_UT);                            // WWDT = CK_UT
    
    // Setup threshold and reload value
    WWDT_SetReloadReg(0x3FF);                               // Reload register = 0x3FF
    WWDT_SetWindowThreshold(0x1FF);                         // Window threshold = 0x1FF
    WWDT_SetWarningThreshold(0x0FF);                        // Waring threshold = 0x0FF
    
    // Setup WWDT keep work in SLEEP mode
    UnProtectModuleReg(CSCprotect);
    CSC_PeriphSleepModeClock_Config(CSC_SLP_WWDT, ENABLE);
    ProtectModuleReg(CSCprotect);                           // Protect CSC module
    
    // Setup WWDT reset trigger source
    WWDT_RstEvent_Config(WWDT_RSTW, ENABLE);                // Enable WWDT WWDT reload counter out of window reset generation
    WWDT_RstEvent_Config(WWDT_RSTF, ENABLE);                // Enable WWDT timer underflow reset generation
    
    // Setup WWDT reset source trigger cold reset, warm reset or others 
//    UnProtectModuleReg(RSTprotect);                         // Protect RST module
//    RST_CRstSource_Config(RST_WWDT_CE, ENABLE);             // Enable IWDT event cold reset trigger source.
//    RST_WRstSource_Config(RST_WWDT_WE, ENABLE);             // Enable IWDT event cold reset trigger source.
//    ProtectModuleReg(RSTprotect);                           // Protect RST module

    // Enable IWDT module
    WWDT_Cmd(ENABLE);
    
    WWDT_RefreshCounter();          // Trigger WINF
    
    ProtectModuleReg(WWDTprotect);  // Protect IWDT module

}
//@del}

#endif
