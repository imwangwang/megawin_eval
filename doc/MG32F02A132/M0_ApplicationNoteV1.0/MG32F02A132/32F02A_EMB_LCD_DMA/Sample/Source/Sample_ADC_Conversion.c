/**
  ******************************************************************************
 *
 * @file        Sample_ADC_Conversion.c
 *
 * @brief       Analog conversion to Digital (ADC)
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2017/07/19
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
* @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */
#include "Sample.h"

#if Sample_Code_Macro_Select == Sample_Code_Macro_ADC && Sample_Code_ADC == Sample_Code_ADC_CONV

#include "MG32x02z_ADC_DRV.h"

/**
 *******************************************************************************
 * @brief	    Conversion PA4 analog input voltage to Digital value depend VREF 
 * @details     1.Config ADC base parameter  
 *    \n        2.Enable ADC 
 *    \n        3.Config ADC Mode
 *    \n        4.Clear all flag
 *    \n        5.Start Calibration 
 *    \n        6.Select PA4 Channel be converted
 *    \n        7.Trigger Source select and Start conversion
 *    \n        8.until E1CNVF & clear flag
 * @return      Result of ADC conversion data
 * @note        The period = (prescaler*main counter)/12MHz (CK_TM36_PR=12MHz) (s)
 *              User can update duty cycle by replace CCxB register.
 *******************************************************************************
 */
uint16_t Sample_ADC_Conversion(void)
{  
	
    ADC_InitTypeDef ADC_Base;
    
    
    // make sure :
	
    //===Set CSC init====
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 12M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->ADC = Enable
    //Configure Peripheral On Mode Clock->Port A = Enable 
	
    //==Set GPIO init 
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOA->Pin4
    //GPIO port initial is 0xFFFF
    //Pin4 mode is AIO
    //Pin4 function GPA4
    
    // ------------------------------------------------------------------------
    // 1.Config ADC base parameter    
    ADC_BaseStructure_Init(&ADC_Base);
    {   // modify parameter
        ADC_Base.ADCMainClockSelect = ADC_CKADC;
            ADC_Base.ADC_IntCK_Div = ADC_IntDIV4;   // for internal clock divider
    
        // ADC data alignment mode (Right or Left)
        ADC_Base.ADC_DataAlign = ADC_RightJustified;
        
        // ADC conversion resolution 8, 10 or 12 bit
        ADC_Base.ADC_ResolutionSel = ADC_12BitData;
        
        // ADC overwritten data or keep data
        ADC_Base.ADC_DataOverrunEvent = ADC_DataOverWritten;
        
    }
    ADC_Base_Init(ADC0, &ADC_Base);
    
    // ------------------------------------------------------------------------
	// 2.Enable ADC 
    ADC_Cmd(ADC0, ENABLE);
    
    // ------------------------------------------------------------------------
    // 3.Config ADC Mode
    ADC_ConversionMode_Select(ADC0, ADCMode);   // one-shot
    ADC_PGA_Cmd(ADC0, DISABLE);                 // Disable PGA
    ADC_SingleDifferentMode_Select(ADC0, ADC_SingleMode);  // Single Mode  
    
    // ------------------------------------------------------------------------
    // 4.Clear all flag
    ADC_ClearFlag(ADC0, 0xFFFFFFFF);

    // ------------------------------------------------------------------------
    // 5.Start Calibration 
    ADC_StartCalibration(ADC0, ENABLE);
    
    // ------------------------------------------------------------------------
	// 6.Select Exnternal Channel (PA4)
    ADC_ExternalChannel_Select(ADC0, ADC_ExtAIN4);

    // ------------------------------------------------------------------------
	// 7.Trigger Source select and Start conversion
    ADC_TriggerSource_Select(ADC0, ADC_START);
    ADC_SoftwareConversion_Cmd(ADC0, ENABLE);
	
    // ------------------------------------------------------------------------
	// 8.until E1CNVF & clear flag
    while(ADC_GetSingleFlagStatus(ADC0, ADC_E1CNVF) == DRV_UnHappened);
    ADC_ClearFlag(ADC0, ADC_E1CNVF);
   
    return (uint16_t) ADC_GetDAT0Data(ADC0);
}

#endif


