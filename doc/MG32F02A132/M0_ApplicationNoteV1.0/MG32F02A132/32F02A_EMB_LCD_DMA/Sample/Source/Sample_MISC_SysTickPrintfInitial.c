/**
 ******************************************************************************
 *
 * @file        Sample_MISC_SysTickPrintfInitial.c
 *
 * @brief       This is the C code format driver file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/05/29
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal  
 */

#include "Sample.h"

#if Sample_Code_Macro_Select == Sample_Code_Macro_MISC && Sample_Code_MISC == Sample_Code_SysTickPrintfInit

#include "MG32x02z_DRV.h"

extern void Sample_URT0_Init(void);



/**
 *******************************************************************************
 * @brief  	    fputc
 * @details     Write transmit data and wait transfor complet.
 * @return	    	
 * @note        
 * @par         Example
 * @code
 * @endcode
 * @bug              
 *******************************************************************************
 */
//int fputc(int ch, FILE *f)
//{
//    URT_SetTXData(URT0, 1, ch);
//    while(URT_GetITSingleFlagStatus(URT0, URT_IT_TC) == DRV_UnHappened);
//    URT_ClearITFlag(URT0, URT_IT_TC);
//    return ch;
//}



/**
 *******************************************************************************
 * @brief  	    fputc
 * @details     Check transmit data buffer empty and Write transmit data.
 * @return	    	
 * @note        
 * @par         Example
 * @code
 * @endcode
 * @bug              
 *******************************************************************************
 */
volatile uint8_t gURT0_First = 0;
int fputc(int ch, FILE *f)
{
    if(gURT0_First == 0)
        gURT0_First = 1;
    else
        while(URT_GetITSingleFlagStatus(URT0, URT_IT_TC) == DRV_UnHappened);
    URT_ClearITFlag(URT0, URT_IT_TC);
    URT_SetTXData(URT0, 1, ch);
    return ch;
}



/**
 *******************************************************************************
 * @brief  	    fgetc
 * @details     Check receive data buffer non empty and read received data.
 * @return	    	
 * @note        
 * @par         Example
 * @code
 * @endcode
 * @bug              
 *******************************************************************************
 */
int fgetc(FILE *f)
{
    while(URT_GetITSingleFlagStatus(URT0, URT_IT_RX) == DRV_UnHappened);
	return URT0->RDAT.H[0];
}



/**
 *******************************************************************************
 * @brief  	    Sample_MISC_SysTickPrintfInitial
 * @details     Initial SysTick is 1ms, and UART0 Baudrate 115200 8N1
 * @return	    	
 * @note        
 * @par         Example
 * @code
				Sample_MISC_SysTickPrintfInitial();
 * @endcode
 * @bug              
 *******************************************************************************
 */
void Sample_MISC_SysTickPrintfInitial(void)
{
    InitTick(12000000UL, 0); // 1ms Tick
    Sample_URT0_Init(); // Baudrate 115200
    printf("Ready\n\r");
}

#endif
