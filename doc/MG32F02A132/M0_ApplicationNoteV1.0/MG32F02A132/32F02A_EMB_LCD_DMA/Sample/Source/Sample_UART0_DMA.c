

/**
 ******************************************************************************
 *
 * @file        Sample_UART0_DMA.c
 * @brief       The demo UART0'S DMA initial sample C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/06/26
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History:
 * --
 * --
 * >>
 * >>
 *
 @endif
 *******************************************************************************
 */
 
#include "Sample.h"
 
#if Sample_Code_Macro_Select == Sample_Code_Macro_URTx && Sample_Code_URT == Sample_Code_URT0_DMA_Init 

#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_URT_DRV.H"
#include "MG32x02z_DMA_DRV.H"

#define URTX    URT0

 /**
 *******************************************************************************
 * @brief	   
 * @details    
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
#define URT_TOTAL_DATA_LEN    256*4 
uint8_t URT_TX_TABLE[URT_TOTAL_DATA_LEN]; 
uint8_t URT_RX_TABLE[URT_TOTAL_DATA_LEN]; 
 
void Sample_URT0DMA_Init(void)
{
    URT_BRG_TypeDef              URT_BRG;
    URT_Data_TypeDef             DataDef;
    DMA_BaseInitTypeDef          DMAPattern;
    DMA_DestinationRequestDef    URTTX_DMA = DMA_URT0_TX;
    DMA_SourcenRequestDef        URTRX_DMA = DMA_URT0_RX;
    
    //==Set CSC init
    //MG32x02z_CSC_Init.h(Configuration Wizard)
    //Select CK_HS source = CK_IHRCO
    //Select IHRCO = 11.0592M
    //Select CK_MAIN Source = CK_HS
    //Configure PLL->Select APB Prescaler = CK_MAIN/1
    //Configure Peripheral On Mode Clock->Port B/URT0 = Enable
    //Configure Peripheral On Mode Clock->URT0->Select URT0_PR Source = CK_APB(11.0592)
    
    //==Set GPIO init
    //MG32x02z_GPIO_Init.h(Configuration Wizard)->Use GPIOB->Pin8/9
    //GPIO port initial is 0xFFFF
    //Pin8 mode is PPO/Pin9 mode is ODO
    //Pin8/9 pull-up resister Enable
    //Pin8/9 function URT0_TX/RX
    
    
    URT_TX_Cmd(URTX, DISABLE);	 
    URT_RX_Cmd(URTX, DISABLE);
    URT_ClearTXData(URTX);
    URT_ClearRXData(URTX);
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 3;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 5;	                //Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    //BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 3);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 3);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);


    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_DataInverse = DISABLE;
    DataDef.URT_TX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URTX, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URTX, URT_IDLEMode_No);
    URT_TXGaudTime_Select(URTX, 0);
    
    
    //====Set DMA========
    DMA_Cmd(DISABLE);
    DMA_Cmd(ENABLE);
    
    //======TX DMA=========
    DMA_Channel_Cmd(DMAChannel2, ENABLE);                           // DMA channel2 for URTX TX
    DMA_BaseInitStructure_Init(&DMAPattern);
    DMAPattern.DMAChx         = DMAChannel2;
    DMAPattern.DMALoopCmd     = DISABLE;                        
    DMAPattern.SrcSINCSel     = ENABLE;                             // MEM address Increase enable
    DMAPattern.SrcSymSel      = DMA_MEM_Read;                       // DMA source from MEM
    DMAPattern.DestSymSel     = URTTX_DMA;                          // DMA destination is to URT_TX
    DMAPattern.BurstDataSize  = DMA_BurstSize_1Byte;                // DMA data size is 1byte
    DMAPattern.DMATransferNUM = URT_TOTAL_DATA_LEN;                 // Transmit total size 
    DMAPattern.DMASourceAddr  = (uint8_t *)&URT_TX_TABLE;           // Transmit data address.
    DMA_Base_Init(&DMAPattern);
    
    URT_TX_Cmd(URTX,DISABLE);                                       // Before enable URT_TX DMA have to disable URT_TX_EN
    URT_TXDMA_Cmd(URTX,ENABLE);                                     // Enable URT TX DMA.
    URT_TX_Cmd(URTX,ENABLE);                                        // Enable URT TX_EN.
    
    //=======RX DMA==========
    DMA_Channel_Cmd(DMAChannel1, ENABLE);                           // DMA channel1 for URTX TX
    DMA_BaseInitStructure_Init(&DMAPattern);
    DMAPattern.DMAChx             = DMAChannel1;
    DMAPattern.DMALoopCmd         = DISABLE;
    DMAPattern.DestDINCSel        = ENABLE;                         // MEM address Increase enable
    DMAPattern.SrcSymSel          = URTRX_DMA;                      // DMA source from URT RX
    DMAPattern.DestSymSel         = DMA_MEM_Wrtie;                  // DMA destination is to MEM 
    DMAPattern.BurstDataSize      = DMA_BurstSize_1Byte;            // DMA data size is 1byte
    DMAPattern.DMATransferNUM     = URT_TOTAL_DATA_LEN;             // Transmit total size  
    DMAPattern.DMADestinationAddr = (uint8_t *)&URT_RX_TABLE;       // Receive MEM address.
    DMA_Base_Init(&DMAPattern);
    
    URT_RX_Cmd(URTX,DISABLE);                                       // Before enable URT_RX DMA have to disable URT_RX_EN
    URT_RXDMA_Cmd(URTX,ENABLE);                                     // Enable URT RX DMA.
    URT_RX_Cmd(URTX,ENABLE);                                        // Enable URT RX_EN.
    

    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);
		
        
	DMA_ClearFlag(DMA,0xFFFFFFFF);                 // Clear DMA flag
    DMA_StartRequest(DMAChannel1);                 // Start URT0 RX DMA
    DMA_StartRequest(DMAChannel2);                 // Start URT0 TX DMA
    
    
    while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);             //Wait DMA TX finish.
    while(DMA_GetSingleFlagStatus(DMA, DMA_FLAG_CH1_TCF) == DRV_UnHappened);      //Wati DMA RX finish.
    
    
}



#endif















