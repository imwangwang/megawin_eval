/**
 ******************************************************************************
 *
 * @file        Sample_I2C_ByteMode.h
 *
 * @brief       Sample I2C Byte Mode Transfer Head File 
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2019/01/18
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2019 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE
 * Modify History:
 * #0.01_HPJ_20190118 //bugNum_Authour_Date
 * >> Bug1 description
 * -- Bug1 sub-description
 * --
 @endif
 ******************************************************************************
 * @internal
 * @sign
 * @endinternal  
 */ 



#ifndef __MG32x02z_I2C_ByteMode_H
#define __MG32x02z_I2C_ByteMode_H
//#define __MG32x02z_I2C_ByteMode_H_VER                            0.01

#ifdef __cplusplus
 extern "C" {
#endif



#include "MG32x02z_DRV.h"



/**
 * @name	I2C Initialze TypeDef
 *          I2C Initialze TypeDef
 */ 
///@{
typedef struct
{
        uint32_t            CK_I2C_PR_Frequency;
        uint32_t            SCL_Clock;
        uint8_t             OwnAddress1;
}Sample_I2C_InitTypeDef;
///@}


/** @defgroup I2C_Exported_Types I2C Exported Types
  * @{
  */

/**
 * @name	I2C Control TypeDef
 *          I2C Control TypeDef
 */ 
///@{
typedef enum{
    Sample_I2C_XferNext_Mask        = 0x00000003,
    Sample_I2C_XferNext_RepeatStart = 0x00000001,
    Sample_I2C_XferNext_STOP        = 0x00000002,
    Sample_I2C_XferNext_StopStart   = 0x00000003,
}Sample_I2C_XferNextTypeDef;

typedef union{
    uint32_t W;
    uint16_t H[2];
    uint8_t  B[4];
    struct{
        uint32_t XferNext           :2;
        uint32_t NACKNext           :1;
        uint32_t                    :29;
    }MBit;
}Sample_I2C_ControlTypeDef;
///@}



/**
 * @name	I2C State TypeDef
 *          I2C State TypeDef
 */ 
///@{
#define Sample_I2C_State_Done               (uint32_t)0x00000001
#define Sample_I2C_State_BusError           (uint32_t)0x00000002
#define Sample_I2C_State_ArbitrationLost    (uint32_t)0x00000004
#define Sample_I2C_State_NACK               (uint32_t)0x00000008
#define Sample_I2C_State_Length             (uint32_t)0x00000010



typedef union{
    uint32_t W;
    uint16_t H[2];
    uint8_t  B[4];
    struct{
        uint32_t Done               :1;
        uint32_t BusError           :1;
        uint32_t ArbitrationLost    :1;
        uint32_t NACK               :1;
        uint32_t Length             :1;
        uint32_t                    :27;
    }MBit;
}Sample_I2C_StateTypeDef;
///@}



/**
 * @name	I2C Handle TypeDef
 *          I2C Handle TypeDef
 */ 
///@{
typedef struct{
    __IO I2C_Struct                 *Instance;
    __IO Sample_I2C_InitTypeDef     Init;
    __IO uint8_t                    *pData;
    __IO Sample_I2C_StateTypeDef    State;
    __IO Sample_I2C_ControlTypeDef  Control;
    __IO uint16_t                   Count;
    __IO uint16_t                   Length;
    __IO uint8_t                    SlaveAddress;
}Sample_I2C_HandleTypeDef;
///@}

/**
  * @}
  */



void Sample_I2C0_ByteMode(void);
void Sample_I2C1_ByteMode(void);

DRV_Return Sample_I2C_DeInit(Sample_I2C_HandleTypeDef *SI2C);
DRV_Return Sample_I2C_ByteMode_Init(Sample_I2C_HandleTypeDef *SI2C);

/******* Blocking mode: Polling */
DRV_Return Sample_I2C_IsDeviceReady(Sample_I2C_HandleTypeDef *SI2C, uint16_t DevAddress, uint32_t Trials);

DRV_Return Sample_I2C_ByteMode_MasterTransmit(Sample_I2C_HandleTypeDef *SI2C, uint16_t SlaveAddress, uint8_t *pData, uint16_t Lenth);
DRV_Return Sample_I2C_ByteMode_MasterReceive(Sample_I2C_HandleTypeDef *SI2C, uint16_t SlaveAddress, uint8_t *pData, uint16_t Lenth);
DRV_Return Sample_I2C_ByteMode_SlaveReceive(Sample_I2C_HandleTypeDef *SI2C, uint8_t *pData, uint16_t Lenth);
DRV_Return Sample_I2C_ByteMode_SlaveTransmit(Sample_I2C_HandleTypeDef *SI2C, uint8_t *pData, uint16_t Lenth);

void Sample_I2C_ByteMode_Handle(Sample_I2C_HandleTypeDef *Sample_I2C);



#ifdef __cplusplus
}
#endif

#endif


