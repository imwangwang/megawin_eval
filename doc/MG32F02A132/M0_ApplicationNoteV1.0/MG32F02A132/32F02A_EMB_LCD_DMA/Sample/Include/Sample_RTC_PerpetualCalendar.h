/**
 ******************************************************************************
 *
 * @file        Sample_RTC_PerpetualCalendar.h
 *
 * @brief       This file contains all the functions prototypes for the RTC  
 *              perpetual calendar firmware library.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/08/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 
 *Modify History: 
 *--
 *--
 *>>
 *>>
 *
 @endif 
 ******************************************************************************
 */ 

#ifndef _MG32x02z_PerpetualCalendar_H

/*!< _MG32x02z_ADC_DRV_H */ 
#define _MG32x02z_PerpetualCalendar_H


#include "MG32x02z.H"
#include "MG32x02z__Common_DRV.H"
#include "MG32x02z_RTC_DRV.h"


/** 
 * @enum		_MonthDef
 * @brief		declare month 
 */        
typedef enum
{
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
    
} _MonthDef;

/** 
 * @enum		_WeekendDef
 * @brief		declare weekend 
 */        
typedef enum
{
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
} _WeekendDef;

/** 
 * @enum		_isLeapYearDef
 * @brief		check this year for leap year 
 */        
typedef enum
{
    LeapYear,
    NotLeapYear,
} _isLeapYearDef;


/** 
 * @struct  _PerpetualCalendarDef
 * @brief   declare struct for Perpetual Calendar 
 */        
typedef struct 				
{ 
    uint32_t year;          /*!< Specifies the year */
    
    _MonthDef month;        /*!< Specifies the month. Please reference _MonthDef enum.*/                         
                                              
    uint8_t day;            /*!< Specifies the day number of month. */    
    
    uint8_t hour;           /*!< Specifies the hour of 24 hours a day */
    
    uint8_t minute;         /*!< Specifies minute */
                                                                                                    
    uint8_t second;         /*!< Specifies second */   
    
    _WeekendDef weekend;    /*!< Specifies weekend. Please reference _WeekendDef enum. */   

} _PerpetualCalendarDef;   


/**
 * @name	Function announce
 *   		
 */ 
///@{  
_isLeapYearDef isLeapYear ( _PerpetualCalendarDef *MG_Calendar);
void FillYMD ( _PerpetualCalendarDef *MG_Calendar, uint32_t days );
void getWeekday(_PerpetualCalendarDef *MG_Calendar);
DRV_Return Sample_RTC_PerpetualCalendar_Init(_PerpetualCalendarDef *MG_Calendar);
DRV_Return Sample_RTC_CaptureConvert(_PerpetualCalendarDef *MG_Calendar);
///@}


#endif
