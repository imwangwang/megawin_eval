/**
 *******************************************************************************
 *
 * @file        API_Flash.c
 *
 * @brief       Application for serial flash 
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2018/02/21
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 @if HIDE
 * Modify History: 
 * #001_Hades_
 *  >> 
 *      -- 
 *      -- 
 @endif
 *******************************************************************************
 */
#include "MG32x02z_SPI_MID.h"


#ifndef _API_Flash_H
/*!< _API_Flash_H */ 
#define _API_Flash_H


/**
 * @name	Function announce
 *   		
 */ 
///@{ 
void API_Flash_Init (void);

void API_Flash_Erase (uint32_t Address, uint32_t Length);

void API_Flash_MultiBytesWrite (uint32_t Address, uint8_t *DataSource, uint32_t Length);
void API_Flash_1ByteWrite (uint32_t Address, uint8_t Dat);

void API_Flash_MultiBytesRead (uint32_t Address, uint8_t *BufferAddreass, uint32_t Length);
void API_Flash_MultiBytesReadu (uint32_t Address, uint8_t *BufferAddreass, uint32_t Length);
uint8_t API_Flash_1ByteRead (uint32_t Address);

///@}  


#endif  // _API_Flash_H

