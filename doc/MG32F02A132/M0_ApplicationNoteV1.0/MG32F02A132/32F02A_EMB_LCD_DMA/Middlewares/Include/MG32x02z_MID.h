/**
 ******************************************************************************
 *
 * @file        MG32x02z_DRV.h
 *
 * @brief       This is the C Code Include format example file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 @if HIDE 

 @endif 
 ******************************************************************************
 */ 

#ifndef _MG32x02z_MID_H
#define _MG32x02z_MID_H
#define _MG32x02z_MID_H_VER                            0.01           /*!<MG32x02z Driver ver.*/

#ifdef __cplusplus
extern "C" {
#endif

#include "MG32x02z_SPI_MID.h"
#include "MG32x02z_TM_MID.h"
#include "MG32x02z_ADC_MID.h"



#ifdef __cplusplus
}
#endif

#endif

