/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				此demo旨在利用TM36输出两路PWM波形到PB8
 *				和PD1。
 *				此demo会在PB8和PD1输出PWM波形，且两个IO口
 *				输出的波形为互补型带死区的波形，用户可设置
 *				TM_SetDeadTime()的第二个参数设置死区时间。
 *				
 *				
 *				注意：
 *				TIM中仅TM36支持使用死区控制。
 *				死区必须是互补型输出才可以使用。
 *
 *
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */


#include "MG32x02z_DRV.H"
#include <stdio.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define TM36_PrescalerCounter_Range 120
#define TM36_MainCounter_Range      1000

//if user wants to delay 1ms and CK_TM00_PR is 12MHz.
//The Total clocks is 12M*1ms = 12000.
//User can set "clock prescaler"=100 and "pulse width"=120 .   

/***********************************************************************************
函数名称:	void Sample_TM36_PWM (void)
功能描述:	TM36初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void Sample_TM36_PWM(void)
{  

		TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;  
		PIN_InitTypeDef PINX_InitStruct;

		UnProtectModuleReg(CSCprotect);
		CSC_PeriphProcessClockSource_Config(CSC_TM36_CKS, CK_APB);
		CSC_PeriphOnModeClock_Config(CSC_ON_TM36, ENABLE);					  // Enable TIM36 module clock
		CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);					  // Enable Port B clock
		CSC_PeriphOnModeClock_Config(CSC_ON_PortD,ENABLE);					  // Enable Port D clock
		ProtectModuleReg(CSCprotect);

		PINX_InitStruct.PINX_Mode 		   = PINX_Mode_PushPull_O;				  		// Pin select Push Pull mode
		PINX_InitStruct.PINX_PUResistant	   = PINX_PUResistant_Enable;			  // Enable pull up resistor
		PINX_InitStruct.PINX_Speed		   = PINX_Speed_Low;						 
		PINX_InitStruct.PINX_OUTDrive 	   = PINX_OUTDrive_Level0;				   // Pin output driver full strength.
		PINX_InitStruct.PINX_FilterDivider   = PINX_FilterDivider_Bypass; 		   // Pin input deglitch filter clock divider bypass
		PINX_InitStruct.PINX_Inverse		   = PINX_Inverse_Disable;				   // Pin input data not inverse  
		  
		PINX_InitStruct.PINX_Alternate_Function = 6;								  //AFS = TM36OC01
		GPIO_PinMode_Config(PINB(8),&PINX_InitStruct);				  
		
		PINX_InitStruct.PINX_Alternate_Function = 6;									//AFS =  TM36OC2N
		GPIO_PinMode_Config(PIND(1),&PINX_InitStruct);	  

		
		TM_DeInit(TM36);
		TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
		TM_TimeBase_InitStruct.TM_MainClockDirection =TM_UpCount;
		TM_TimeBase_InitStruct.TM_Period = TM36_MainCounter_Range - 1; 
		TM_TimeBase_InitStruct.TM_Prescaler = TM36_PrescalerCounter_Range - 1;
		TM_TimeBase_InitStruct.TM_CounterMode = Cascade;
		
		TM_TimeBase_Init(TM36, &TM_TimeBase_InitStruct);
		TM_CH0Function_Config(TM36, TM_16bitPWMDTG);		//模式设置为带死区的PWM
		TM_CH2Function_Config(TM36, TM_16bitPWMDTG);
	   
		TM_OC01Output_Cmd(TM36,ENABLE);    //  TM36_OC01  
		TM_InverseOC0z_Cmd(TM36, DISABLE);
		TM_OC0zOutputState_Init(TM36, CLR);   
		TM_SetCC0A(TM36,500);        
		TM_SetCC0B(TM36,500);	
		TM_SetDeadZoneClockDivider(TM36,TM_DTDIV8);      //DeadTime DIV
		TM_SetDeadTime(TM36,200);                     //DeadTime count
		
		TM_OC2Output_Cmd(TM36,ENABLE);     //  TM36_OC2 
		TM_InverseOC2_Cmd(TM36, DISABLE);
		TM_OC2OutputState_Init(TM36, CLR);    
		TM_OC2NOutput_Cmd(TM36, ENABLE);           //DeadTime should be used with OCN
		TM_InverseOC2N_Cmd(TM36, DISABLE);
		TM_SetCC2A(TM36,300);        
		TM_SetCC2B(TM36,300);

		TM_AlignmentMode_Select(TM36, TM_EdgeAlign);				//边沿对齐模式
		TM_ClearFlag(TM36, TMx_CF0A | TMx_CF1A | TMx_CF2A |TMx_CF3A);
		TM_Timer_Cmd(TM36,ENABLE);
	}

/***********************************************************************************
函数名称:	void CSC_Init (void)
功能描述:	系统时钟初始化
输入参数:	
返回参数:	  
*************************************************************************************/
void CSC_Init (void)
{
	CSC_PLL_TyprDef CSC_PLL_CFG;
	
  UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
  MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
  ProtectModuleReg(MEMprotect);

  UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	
	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO
	
	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
	
	/* Configure peripheral clock */

  ProtectModuleReg(CSCprotect);
    
}

/***********************************************************************************
函数名称:	int main()
功能描述:	主程序
输入参数:	
返回参数:	  
*************************************************************************************/
int main()
{
	CSC_Init();
	Sample_TM36_PWM();	

	while(1)
	{
	}	

}

