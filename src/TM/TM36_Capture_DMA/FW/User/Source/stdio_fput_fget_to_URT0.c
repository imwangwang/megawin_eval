#include "stdio.h"
#include "string.h"
#include "Mx92G8z_DRV.h"



volatile uint8_t gURT0_First = 0;

int fputc(int ch, FILE *f)
{
    if(gURT0_First == 0)
        gURT0_First = 1;
    else
        while(URT_GetITSingleFlagStatus(URT0, URT_IT_TC) == DRV_UnHappened);
    
    URT_ClearITFlag(URT0, URT_IT_TC);
    URT_SetTXData(URT0, 1, ch);
    return ch;
}



int fgetc(FILE *f)
{
    int ch;
    while(URT0->STA.MBIT.RXF == 0);
    ch = UART_RXData1BYTE_Read(URT0);
    return ch;
}

