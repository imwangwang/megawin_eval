/**
 ******************************************************************************
 *
 * @file        main.C
 * @brief       MG32x02z demo main c Code. 
 *
 * @par         Project
 *              MG32x02z
 *				��demoּ������TM26���PWM���ε�PE13
 *				��PE14��
 *				��demo����PE13��PE14���PWM���Σ��û�
 *				������TM_SetCCnA��TM_SetCCnB�ĵڶ�������
 *				����ռ�ձȡ�
 *				
 *				ע�⣺
 *
 *
 *
 *				
 * @version     
 * @date        
 * @author      
 * @copyright   
 *             
 *
 ******************************************************************************* 
 * @par Disclaimer

 *******************************************************************************
 */

#include "MG32x02z_DRV.H"
#include <stdio.h>
#include "cmsis_os.h"
#include "terminal.h"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#define URTX URT0
#define URTY URT1

#define TM10_PRE_2nd_RELOAD			(u16)((u32)(1*1000*12.00/1-1)%65536)
#define TM10_MAIN_RELOAD			(u16)((u32)(1*1000*12.00/1-1)/65536)

//if user wants to set PWM period 1ms and CK_TM00_PR is 12MHz.
//The Total clocks is 12M*1ms = 12000.
//User can set "clock prescaler"=100 and "pulse width"=120 .   
#define TM26_PrescalerCounter_Range 120
#define TM26_MainCounter_Range      100

#define TM20_PrescalerCounter_Range 12
#define TM20_MainCounter_Range      1000


typedef enum
{
  THREAD_1 = 0,
  THREAD_2,
  THREAD_3
} Thread_TypeDef;
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
osThreadId localThread1Handle, localThread2Handle, localThread3Handle;
uint32_t timeCountOffset = 0;
double frequency = 0;
float targetFreq = 60.0f;

uint32_t timeCountOffset2 = 0;
double frequency2 = 0;

volatile uint64_t SysTickCtr = 0;
char dbgbuffer[256];

uint32_t output;

float P = 0.2f, I = 0.01f, D = 1.0f;

/* Private function prototypes -----------------------------------------------*/
static void terminalThread1(void const *argument);
static void pidThread2(void const *argument);
static void localThread3(void const *argument);

static uint8_t pwm_set_cmd(void);
static uint8_t pid_set_KID(void);


static uint8_t pid_set_KID(void) {
    float lp, li, ld;

    if (CLI_IsArgFlag("-p")) {
        if (CLI_GetArgFloatByFlag("-p", &lp)) {
            CLI_Printf("\r\nset P arguments: %.2f\r\n", lp);
            P = lp;
        }
    }

    if (CLI_IsArgFlag("-i")) {
        if (CLI_GetArgFloatByFlag("-i", &li)) {
            CLI_Printf("\r\nset I arguments: %.2f\r\n", li);
            I = li;
        }
    }

    if (CLI_IsArgFlag("-d")) {
        if (CLI_GetArgFloatByFlag("-d", &ld)) {
            CLI_Printf("\r\nset D arguments: %.2f\r\n", ld);
            D = ld;
        }
    }

    return TE_OK;
}

static uint8_t pwm_set_cmd(void) {
	uint32_t i = 0x01;
    uint32_t c = 0;

    if (!CLI_IsArgFlag("-i")) {
        CLI_Printf("invalid arguments, -i specify which fan to be controlled\r\n");
        return TE_OK;
    }

    c = CLI_GetArgDec(0);
    
    CLI_GetArgHexByFlag("-i", &i);

    if (i == 0) {
        TM_SetCC0A(TM26, c % 100);
        TM_SetCC0B(TM26, c % 100);
    }
    else if (i == 1) {
        TM_SetCC1A(TM26, c % 100);
        TM_SetCC1B(TM26, c % 100);
    }
    else {
    	CLI_Printf("\r\ni: 0x%08X\r\nc: %d", (int) i, (int) c);
    }

	return TE_OK;
}


void myCmdInit(void)
{
	CLI_AddCmd("pwmset", pwm_set_cmd, 1, 0, "pwmset - set specific fan dutycycle");
	CLI_AddCmd("pidsetkid", pid_set_KID, 1, 0, "pidsetk - set pid K,I,D factor");
}


void _reset_fcn()
{
	NVIC_SystemReset();
}

void TUSART_PutChar(char c)
{
	URT_SetTXData(URTX,1,c);
	while(URT_GetITSingleFlagStatus(URTX,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTX,URT_IT_TC);
}

void TUSART_Print(const char* str)
{
	while(str != NULL) {
		TUSART_PutChar(*str);

		if (*str == '\0')
			break;

		str++;
	}
}

void URT0_IRQHandler(void) 
{
    TC_Result_e ret;
    
    if (URT_GetITSingleFlagStatus(URTX, URT_IT_RX) == DRV_Happened) {
        ret = CLI_EnterChar(URT_GetRXShiftBufferData(URTX));

        if (ret == TC_Enter && localThread1Handle != 0) {
            BaseType_t xHigherPriorityTaskWoken;
            
            xHigherPriorityTaskWoken = pdFALSE;
            xTaskNotifyFromISR(localThread1Handle, 0x01, eSetBits, &xHigherPriorityTaskWoken);
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
        URT_ClearITFlag(URTX, URT_IT_RX);
    }
}



void SysTick_Handler(void)
{
    osSystickHandler();
    SysTickCtr++;
}

void TM10_IRQHandler(void)
{
	if (TM_GetSingleFlagStatus(TM10, TMx_TOF) == DRV_Happened)	   // Main Timer overflow flag
	{
		TM_ClearFlag (TM10, TMx_TOF);
	}
}

void TM10_Init(void)
{	
	TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
	 
	//==Set TM1x Clock
	UnProtectModuleReg(CSCprotect);
	CSC_PeriphProcessClockSource_Config(CSC_TM10_CKS, CK_APB);
	CSC_PeriphOnModeClock_Config(CSC_ON_TM10,ENABLE);						// Enable TM10 Clock
	ProtectModuleReg(CSCprotect);					
		
	TM_DeInit(TM10);
			
	// ----------------------------------------------------
	// Set TimeBase structure parameter
	TM_TimeBase_InitStruct.TM_CounterMode = Full_Counter;					// È«¼ÆÊýÆ÷Ä£Ê½
	TM_TimeBase_InitStruct.TM_MainClockSource = TM_CK_INT;					// Ö÷¼ÆÊýÆ÷Ô´: (È«¼ÆÊýÄ£Ê½ÏÂ,ÎÞÐèÉèÖÃ)
	TM_TimeBase_InitStruct.TM_2ndClockSource = TM_CK_INT;					// Ô¤·ÖÆµÆ÷(2nd¼ÆÊýÆ÷)¼ÆÊýÆ÷Ô´: TM_CK_INT
	TM_TimeBase_InitStruct.TM_MainClockDirection = TM_UpCount;				// Ö÷¼ÆÊýÆ÷ÏòÉÏ¼ÆÊý 
	TM_TimeBase_InitStruct.TM_2ndClockDirection = TM_UpCount;				// Ô¤·ÖÆµÆ÷(2nd¼ÆÊýÆ÷)ÏòÉÏ¼ÆÊý
		
	TM_TimeBase_InitStruct.TM_Prescaler = TM10_PRE_2nd_RELOAD;				// Ô¤·ÖÆµÆ÷(2nd¼ÆÊýÆ÷)ÖØÔØÖµ	
	TM_TimeBase_InitStruct.TM_Period = TM10_MAIN_RELOAD;					// Ö÷¼ÆÊýÆ÷ÖØÔØÖµ
	TM_TimeBase_Init(TM10, &TM_TimeBase_InitStruct);
		 
	//== SET TM10 ClockOut
	TM_ClockOutSource_Select(TM10, MainCKO);
	TM_ClockOut_Cmd(TM10, ENABLE);	

	// ----------------------------------------------------
	// 2.clear TOF flag
	TM_ClearFlag(TM10, TMx_TOF);
			
	// ----------------------------------------------------
	// 3.Enable TM10/TM16 interrupt 
	TM_IT_Config(TM10,TMx_TIE_IE,ENABLE);									// Ö÷¼ÆÊýÆ÷Òç³öÖÐ¶ÏÊ¹ÄÜ
		
	TM_ITEA_Cmd(TM10,ENABLE);

	// ----------------------------------------------------
	// Timer enable
	TM_Timer_Cmd(TM10,ENABLE);
		
	// ----------------------------------------------------
	// NVIC Enable TM10/TM16
	NVIC_EnableIRQ(TM10_IRQn); 	
}

void TM20_IRQHandler(void)
{
    uint16_t timeCount1 = 0, timeCount2 = 0;
    uint16_t timeCount3 = 0, timeCount4 = 0;
    static uint32_t timeCountSave = 0, timeCountSave2 = 0;
    uint32_t timeCountNow = 0, timeCountNow2 = 0;
    
	if (TM_GetSingleFlagStatus(TM20, TMx_CF0A) == DRV_Happened)	   // Main Timer overflow flag
	{
        TM_ClearFlag(TM20, TMx_CF0A);
        timeCount1 = TM_GetCC0A(TM20);
        timeCount2 = TM_GetCC0B(TM20);
        
        timeCountNow = timeCount2 | (timeCount1 << 16);
        
        if (timeCountSave != 0) {
            if (timeCountNow > timeCountSave)
                timeCountOffset = timeCountNow - timeCountSave;
            else {
                timeCountOffset =  (uint32_t)( -1 ) - timeCountSave + timeCountNow;
            }
            
            frequency = (double)12000000.00 / (double)timeCountOffset;
        }
        
        timeCountSave = timeCountNow;
	}
    
	if (TM_GetSingleFlagStatus(TM20, TMx_CF1A) == DRV_Happened)	   // Main Timer overflow flag
	{
        TM_ClearFlag(TM20, TMx_CF1A);
        
        timeCount3 = TM_GetCC1A(TM20);
        timeCount4 = TM_GetCC1B(TM20);
        
        timeCountNow2 = timeCount4 | (timeCount3 << 16);
        
        if (timeCountSave2 != 0) {
            if (timeCountNow2 > timeCountSave2)
                timeCountOffset2 = timeCountNow2 - timeCountSave2;
            else {
                timeCountOffset2 =  (uint32_t)( -1 ) - timeCountSave2 + timeCountNow2;
            }
            
            frequency2 = (double)12000000.00 / (double)timeCountOffset2;
        }
        
        timeCountSave2 = timeCountNow2;
	}
    
    TM_ClearFlag(TM20, TM_GetAllFlagStatus(TM20));
}

void TM20_Capture_Init(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
    PIN_InitTypeDef PINX_InitStruct; 
    UnProtectModuleReg(CSCprotect);
    CSC_PeriphProcessClockSource_Config(CSC_TM20_CKS, CK_APB);
    CSC_PeriphOnModeClock_Config(CSC_ON_TM20,ENABLE);						// Enable TM20 Clock
    CSC_PeriphOnModeClock_Config(CSC_ON_PortD,ENABLE);						// Enable PortD Clock
    ProtectModuleReg(CSCprotect);

    //==Set GPIO init
    PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 								// Pin select OpenDrain mode
    PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  				// Enable pull up resistor
    PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
    PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 						// Pin output driver full strength.
    PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;			// Pin input deglitch filter clock divider bypass
    PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 						// Pin input data not inverse
    
    PINX_InitStruct.PINX_Alternate_Function  = 6;														// Pin AFS = TIM20 IC
    GPIO_PinMode_Config(PIND(14),&PINX_InitStruct); 		  										// Set PD14 to TIM20 IC
    GPIO_PinMode_Config(PIND(15),&PINX_InitStruct); 		  										// Set PD14 to TIM20 IC
    
    TM_DeInit(TM20);
    
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_MainClockDirection = TM_UpCount;
    
//    TM_TimeBase_InitStruct.TM_Period = TM20_MainCounter_Range - 1; 
//    TM_TimeBase_InitStruct.TM_Prescaler = TM20_PrescalerCounter_Range - 1;
    
    TM_TimeBase_InitStruct.TM_Period = 0xFFFF; 
    TM_TimeBase_InitStruct.TM_Prescaler = 0;
    TM_TimeBase_InitStruct.TM_CounterMode = Full_Counter;
    
    TM_TimeBase_Init(TM20, &TM_TimeBase_InitStruct);
   
    // ----------------------------------------------------
    // 2.config overwrite mode (keep data)
    TM_IC0OverWritten_Cmd(TM20, TM_Keep);
    TM_IC1OverWritten_Cmd(TM20, TM_Keep);
    
    // ----------------------------------------------------
    // 3.config TM36 channel 0 function 
    TM_CH0Function_Config(TM20, TM_InputCapture);
    TM_IN0Source_Select(TM20, IC0);      // TM36_IN0 from IC0 (PD14)            

    TM_CH1Function_Config(TM20, TM_InputCapture);
    TM_IN1Source_Select(TM20, IC0);      // TM36_IN0 from IC0 (PD14)            
    
    // ----------------------------------------------------
    // 4.config TRGI mode
//    TM_TriggerSource_Select(TM20, TRGI_IN0);

    TM_IN0TriggerEvent_Select(TM20, IC_RisingEdge);
    TM_IN1TriggerEvent_Select(TM20, IC_RisingEdge);
    
    TM_Counter_Cmd(TM20, ENABLE);
    TM_Prescaler_Cmd(TM20, ENABLE);
    
    
//    TM_TRGICounter_Select(TM20,TRGI_StartupRising);
//    TM_TRGIPrescaler_Select(TM20,TRGI_StartupRising);


#if (TM36_IC0_TriggerSrc == 1)      // Rising to Rising edge 
    TM_IN0TriggerEvent_Select(TM36, IC_RisingEdge);
		TM_Counter_Cmd(TM36, ENABLE);
	  TM_Prescaler_Cmd(TM36, ENABLE);
    
#elif (TM36_IC0_TriggerSrc == 2)    // Rising to Falling edge 
    TM_TRGICounter_Select(TM36,TRGI_StartupRising);
    TM_TRGIPrescaler_Select(TM36,TRGI_StartupRising);
    TM_IN0TriggerEvent_Select(TM36, IC_FallingEdge);
    
#elif (TM36_IC0_TriggerSrc == 3)    // Falling to Falling edge 
    TM_TRGICounter_Select(TM36,TRGI_StartupFalling);
    TM_TRGIPrescaler_Select(TM36,TRGI_StartupFalling);
    TM_IN0TriggerEvent_Select(TM36, IC_FallingEdge);
    
#elif (TM36_IC0_TriggerSrc == 4)    // Falling to Rising edge
    TM_TRGICounter_Select(TM36,TRGI_StartupFalling);
    TM_TRGIPrescaler_Select(TM36,TRGI_StartupFalling);
    TM_IN0TriggerEvent_Select(TM36, IC_RisingEdge);
		
#elif (TM36_IC0_TriggerSrc == 5)    // Rising to Dual edge
    TM_TRGICounter_Select(TM36,TRGI_StartupRising);
    TM_TRGIPrescaler_Select(TM36,TRGI_StartupRising);
    TM_IN0TriggerEvent_Select(TM36, IC_DualEdge);
				
#endif   
    
    // ----------------------------------------------------
    // 5.clear all flag
    TM_ClearFlag(TM20, TMx_CF0A | TMx_CF0B | TMx_CF1A | TMx_CF1B);
    
    TM_IT_Config(TM20, TMx_CC0_IE | TMx_CC1_IE, ENABLE);
    
    TM_ITEA_Cmd(TM20,ENABLE);
    
    // ----------------------------------------------------
    // 6.enable Timer 
    TM_Timer_Cmd(TM20,ENABLE);
    
    NVIC_EnableIRQ(TM20_IRQn); 
    
}

void Sample_TM26_PWM(void)
{  
    TM_TimeBaseInitTypeDef TM_TimeBase_InitStruct;
    PIN_InitTypeDef PINX_InitStruct;
    
    UnProtectModuleReg(CSCprotect);
    CSC_PeriphProcessClockSource_Config(CSC_TM26_CKS, CK_APB);
    CSC_PeriphOnModeClock_Config(CSC_ON_TM26, ENABLE);					  // Enable TIM26 module clock
    CSC_PeriphOnModeClock_Config(CSC_ON_PortD,ENABLE);					  // Enable Port D clock
    ProtectModuleReg(CSCprotect);

    TM_DeInit(TM26);

    //==Set GPIO init
    PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 								// Pin select Push Pull mode
    PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  				// Enable pull up resistor
    PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
    PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 						// Pin output driver full strength.
    PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;			// Pin input deglitch filter clock divider bypass
    PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 						// Pin input data not inverse

    PINX_InitStruct.PINX_Alternate_Function  = 6;														// Pin AFS = TIM26 OC
    GPIO_PinMode_Config(PIND(5),&PINX_InitStruct); 		  										// Set PD5 to TIM26 OC11
    GPIO_PinMode_Config(PIND(9),&PINX_InitStruct); 		  										// Set PD9 to TIM26 OC12
    
    // ----------------------------------------------------
    // 1.TimeBase structure initial
    TM_TimeBaseStruct_Init(&TM_TimeBase_InitStruct);
    
    // modify parameter
    TM_TimeBase_InitStruct.TM_MainClockDirection = TM_UpCount;
    TM_TimeBase_InitStruct.TM_Period = TM26_MainCounter_Range-1; 
    TM_TimeBase_InitStruct.TM_Prescaler = TM26_PrescalerCounter_Range-1;
    TM_TimeBase_InitStruct.TM_CounterMode = Cascade;   
    TM_TimeBase_Init(TM26, &TM_TimeBase_InitStruct);
    
    TM_CH0Function_Config(TM26, TM_16bitPWM);					//PWMģʽ
    // ----------------------------------------------------
    // 3.Enable TM26 channel 1 Output (just output TM26_OC11)
    TM_OC01Output_Cmd(TM26,ENABLE);    
    TM_InverseOC0z_Cmd(TM26, DISABLE);
    TM_OC0zOutputState_Init(TM26, CLR);
    
    TM_SetCC0A(TM26, 0);        
    TM_SetCC0B(TM26, 0);		// reload value when overflow

    // ----------------------------------------------------
    // 2.config TM26 channel 1 function 
    TM_CH1Function_Config(TM26, TM_16bitPWM);					//PWMģʽ
    // ----------------------------------------------------
    // 3.Enable TM26 channel 1 Output (just output TM26_OC11)
    TM_OC11Output_Cmd(TM26,ENABLE);    
    TM_InverseOC1z_Cmd(TM26, DISABLE);
    TM_OC1zOutputState_Init(TM26, CLR);
    
    // ----------------------------------------------------
    // 4.����ռ�ձ�(Duty cycle %) for PWM channel0.(�趨��ΧֵΪ1~MainCounter_Range��ֵ��

    TM_SetCC1A(TM26, 0);        
    TM_SetCC1B(TM26, 0);		// reload value when overflow
    
    // ----------------------------------------------------
    // 9.select Edge Align
    TM_AlignmentMode_Select(TM26, TM_EdgeAlign);    
    // ----------------------------------------------------
    // 10.clear flag
    TM_ClearFlag(TM26, TMx_CF0A | TMx_CF1A | TMx_CF2A);   
    // ----------------------------------------------------
    // 11.Timer enable
    TM_Timer_Cmd(TM26,ENABLE);

}

void Sample_URT1_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    PIN_InitTypeDef PINX_InitStruct;

    UnProtectModuleReg(CSCprotect);
    CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
    CSC_PeriphOnModeClock_Config(CSC_ON_UART1,ENABLE);
    CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);	
    ProtectModuleReg(CSCprotect);

    PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
    PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
    PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
    PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
    PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
    PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
    PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_TX
    GPIO_PinMode_Config(PINE(2),&PINX_InitStruct); 					 		// TXD at PE2

    PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
    PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_RX
    GPIO_PinMode_Config(PINE(3),&PINX_InitStruct); 					 		// RXD at PE3  
	
	
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 3;	                //Set RLR
    URT_BaudRateGenerator_Config(URTY, &URT_BRG);		    //BR115200 = f(CK_URTY)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTY, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTY, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTY, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTY, 25);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTY, 25);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTY, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTY, ENABLE);	                            //Enable TX
    URT_RX_Cmd(URTY, ENABLE);	                            //Enable RX
    
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTY, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTY, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTY, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URTY, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URTY, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URTY, 0);

#if 0
    //=====Enable URT Interrupt=====//
    URT_IT_Config(URTY, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URTY, ENABLE);
    NVIC_EnableIRQ(URT1_IRQn);
#endif
    //=====Enable URT=====//
    URT_Cmd(URTY, ENABLE);
		
	//==See MG32x02z_URT1_IRQ.c when interrupt in
}


void Sample_URT0_Init(void)
{
    URT_BRG_TypeDef  URT_BRG;
    URT_Data_TypeDef DataDef;
    PIN_InitTypeDef PINX_InitStruct;

    UnProtectModuleReg(CSCprotect);
    CSC_PeriphProcessClockSource_Config(CSC_UART0_CKS, CK_APB);
    CSC_PeriphOnModeClock_Config(CSC_ON_UART0,ENABLE);
    CSC_PeriphOnModeClock_Config(CSC_ON_PortB,ENABLE);	
    ProtectModuleReg(CSCprotect);

    PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 	// Pin select Push Pull mode
    PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  	// Enable pull up resistor
    PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
    PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 	// Pin output driver full strength.
    PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;	// Pin input deglitch filter clock divider bypass
    PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 	// Pin input data not inverse
    PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_TX
    GPIO_PinMode_Config(PINB(8),&PINX_InitStruct); 					 		// TXD at PB8

    PINX_InitStruct.PINX_Mode				 = PINX_Mode_OpenDrain_O; 		// Pin select Open Drain mode
    PINX_InitStruct.PINX_Alternate_Function  = 3;				// Pin AFS = URT0_RX
    GPIO_PinMode_Config(PINB(9),&PINX_InitStruct); 					 		// RXD at PB9  
	
	
    
    //=====Set Clock=====//
    //---Set BaudRate---//
    URT_BRG.URT_InteranlClockSource = URT_BDClock_PROC;
    URT_BRG.URT_BaudRateMode = URT_BDMode_Separated;
    URT_BRG.URT_PrescalerCounterReload = 0;	                //Set PSR
    URT_BRG.URT_BaudRateCounterReload = 3;	                //Set RLR
    URT_BaudRateGenerator_Config(URTX, &URT_BRG);		    //BR115200 = f(CK_URTx)/(PSR+1)/(RLR+1)/(OS_NUM+1)
    URT_BaudRateGenerator_Cmd(URTX, ENABLE);	            //Enable BaudRateGenerator
    //---TX/RX Clock---//
    URT_TXClockSource_Select(URTX, URT_TXClock_Internal);	//URT_TX use BaudRateGenerator
    URT_RXClockSource_Select(URTX, URT_RXClock_Internal);	//URT_RX use BaudRateGenerator
    URT_TXOverSamplingSampleNumber_Select(URTX, 25);	        //Set TX OS_NUM
    URT_RXOverSamplingSampleNumber_Select(URTX, 25);	        //Set RX OS_NUM
    URT_RXOverSamplingMode_Select(URTX, URT_RXSMP_3TIME);
    URT_TX_Cmd(URTX, ENABLE);	                            //Enable TX
    URT_RX_Cmd(URTX, ENABLE);	                            //Enable RX
    
    

    //=====Set Mode=====//
    //---Set Data character config---//
    DataDef.URT_TX_DataLength  = URT_DataLength_8;
    DataDef.URT_RX_DataLength  = URT_DataLength_8;
    DataDef.URT_TX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_RX_DataOrder   = URT_DataTyped_LSB;
    DataDef.URT_TX_Parity      = URT_Parity_No;
    DataDef.URT_RX_Parity      = URT_Parity_No;
    DataDef.URT_TX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_RX_StopBits    = URT_StopBits_1_0;
    DataDef.URT_TX_DataInverse = DISABLE;
    DataDef.URT_RX_DataInverse = DISABLE;
    URT_DataCharacter_Config(URTX, &DataDef);
    //---Set Mode Select---//
    URT_Mode_Select(URTX, URT_URT_mode);
    //---Set DataLine Select---//
    URT_DataLine_Select(URTX, URT_DataLine_2);
    
    //=====Set Error Control=====//
    // to do...
    
    //=====Set Bus Status Detect Control=====//
    // to do...
    
    //=====Set Data Control=====//
    URT_RXShadowBufferThreshold_Select(URTX, URT_RXTH_1BYTE);
    URT_IdlehandleMode_Select(URTX, URT_IDLEMode_No);
    URT_TXGuardTime_Select(URTX, 0);
    
    //=====Enable URT Interrupt=====//
    URT_IT_Config(URTX, URT_IT_RX, ENABLE);
    URT_ITEA_Cmd(URTX, ENABLE);
    NVIC_EnableIRQ(URT0_IRQn);

    //=====Enable URT=====//
    URT_Cmd(URTX, ENABLE);
		
	//==See MG32x02z_URT0_IRQ.c when interrupt in
}

int fputc(int ch,FILE *f)
{
	
	URT_SetTXData(URTY,1,ch);
	while(URT_GetITSingleFlagStatus(URTY,URT_IT_TC)==DRV_UnHappened);
	URT_ClearITFlag(URTY,URT_IT_TC);
	
	return ch;
}

void CSC_Init (void)
{
    CSC_PLL_TyprDef CSC_PLL_CFG;
    
    UnProtectModuleReg(MEMprotect);     	// Setting flash wait state
    MEM_SetFlashWaitState(MEM_FWAIT_ONE);	// 50MHz> Sysclk >=25MHz
    ProtectModuleReg(MEMprotect);

    UnProtectModuleReg(CSCprotect);
	CSC_CK_APB_Divider_Select(APB_DIV_1);	// Modify CK_APB divider	APB=CK_MAIN/1
	CSC_CK_AHB_Divider_Select(AHB_DIV_1);	// Modify CK_AHB divider	AHB=APB/1

	/* CK_HS selection */
	CSC_IHRCO_Select(IHRCO_12MHz);			// IHRCO Sel 12MHz
	CSC_IHRCO_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_IHRCOF) == DRV_Normal);
	CSC_ClearFlag(CSC_IHRCOF);
	CSC_CK_HS_Select(HS_CK_IHRCO);			// CK_HS select IHRCO
	
	/* PLL */
	/**********************************************************/
	CSC_PLL_CFG.InputDivider=PLLI_DIV_2;	// 12M/2=6M
	CSC_PLL_CFG.Multiplication=PLLIx16;		// 6M*16=96M
	CSC_PLL_CFG.OutputDivider=PLLO_DIV_2;	// PLLO=96M/2=48M
	CSC_PLL_Config(&CSC_PLL_CFG);
	CSC_PLL_Cmd(ENABLE);
	while(CSC_GetSingleFlagStatus(CSC_PLLF) == DRV_Normal);
	CSC_ClearFlag(CSC_PLLF);
	/**********************************************************/

	
	/* CK_MAIN */ 
	CSC_CK_MAIN_Select(MAIN_CK_HS);	
    
	/* Configure peripheral clock */
 	CSC_PeriphOnModeClock_Config(CSC_ON_PortE,ENABLE);
		
	/* Configure peripheral clock */
 	ProtectModuleReg(CSCprotect);
    
}

/**
  * @brief  Toggle LED2 thread 1
  * @param  thread not used
  * @retval None
  */
static void terminalThread1(void const *argument)
{
    PIN_InitTypeDef PINX_InitStruct;
    (void) argument;
	uint8_t xResult;
	uint32_t ulNotifiedValue;
    
	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
    GPIO_PinMode_Config(PINE(15),&PINX_InitStruct);

    /* Turn on LED2 to indicate Thread1 is ready */
    PE15 = 0x00;

    for (;;)
    {
		xResult = xTaskNotifyWait(0,    /* clear bits on entry. */
                           0xFFFFFFFF,        /* Dont Clear all bits on exit. */
                           &ulNotifiedValue, /* Stores the notified value. */
                           portMAX_DELAY );
		if (xResult != pdPASS) {
		   printf("debug: Thread 1 xTaskNotifyWait fatal failure\r\n");
		   break;
		}

        CLI_Execute();
    }
}

/**
  * @brief  Toggle LED2 thread 2
  * @param  argument not used
  * @retval None
  */
static void pidThread2(void const *argument)
{
    PIN_InitTypeDef PINX_InitStruct;
    (void) argument;
    float dT = 100.0f;
    float error = 0.0f, integral = 0.0f, derivative = 0.0f, preError = 0.0f;

	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
    GPIO_PinMode_Config(PINE(14),&PINX_InitStruct);

    /* Turn on LED2 */
    PE14 = 0;

    for (;;)
    {
        float loutput;
        
        osDelay((uint32_t)dT);

        PE14 =~ PE14;

        error = targetFreq - frequency;

        integral = integral + (error * dT);

        derivative = (error - preError) / dT;

        loutput = (P * error) + (I * integral) + (D * derivative);
        
        if (loutput <= 0)
            output = 0;
        else
            output = (uint32_t)loutput;

        preError = error; 

        TM_SetCC0A(TM26, output % 100);
        TM_SetCC0B(TM26, output % 100);
    }
}

/**
  * @brief  Toggle LED2 thread 2
  * @param  argument not used
  * @retval None
  */
static void localThread3(void const *argument)
{
    PIN_InitTypeDef PINX_InitStruct;
    uint32_t count;
    (void) argument;

	PINX_InitStruct.PINX_Mode				 = PINX_Mode_PushPull_O; 	 // Pin select digital input mode
	PINX_InitStruct.PINX_PUResistant		 = PINX_PUResistant_Enable;  // Enable pull up resistor
	PINX_InitStruct.PINX_Speed 			 	 = PINX_Speed_Low;			 
	PINX_InitStruct.PINX_OUTDrive			 = PINX_OUTDrive_Level0;	 // Pin output driver full strength.
	PINX_InitStruct.PINX_FilterDivider 	 	 = PINX_FilterDivider_Bypass;// Pin input deglitch filter clock divider bypass
	PINX_InitStruct.PINX_Inverse			 = PINX_Inverse_Disable;	 // Pin input data not inverse
	PINX_InitStruct.PINX_Alternate_Function = 0;						 // Pin AFS = 0
    GPIO_PinMode_Config(PINE(13),&PINX_InitStruct);

    for (;;)
    {
        count = osKernelSysTick() + 5000;

        /* Turn on LED2 */
        PE13 = 0x00;

        while (count > osKernelSysTick())
        {
          /* Toggle LED2 every 250ms*/
          osDelay(250);
          PE13=~PE13;
          printf("debug: freqency1: %.2f, freqency2: %.2f, output: %d\r\n", frequency, frequency2, output);
        }

        /* Turn off LED2 */
        PE13 = 0xFF;

        /* Resume Thread 1 */
    }
}

int main()
{
	CSC_Init();
    InitTick(12000000,0);
    
	Sample_URT0_Init();
    Sample_URT1_Init();
//    TM10_Init();
    
    Sample_TM26_PWM();
    
    TM20_Capture_Init();
    
	CLI_Init(TDC_Time);

    myCmdInit();
    
    printf("main: system initialize completed\r\n");
    
    /* Thread 1 definition */
    osThreadDef(THREAD_1, terminalThread1, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 3);

    /* Thread 2 definition */
    osThreadDef(THREAD_2, pidThread2, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 2);

    /* Thread 3 definition */
    osThreadDef(THREAD_3, localThread3, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
    
    /* Start thread 1 */
    localThread1Handle = osThreadCreate(osThread(THREAD_1), NULL);

    /* Start thread 2 */
    localThread2Handle = osThreadCreate(osThread(THREAD_2), NULL);  
    
    /* Start thread 3 */
    localThread3Handle = osThreadCreate(osThread(THREAD_3), NULL);  
    
    /* Start scheduler */
    osKernelStart();

    /* We should never get here as control is now taken by the scheduler */
    for (;;);
    
//	Sample_TM26_PWM();
//  while(1);
		
}
