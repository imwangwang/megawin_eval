


/**
 ******************************************************************************
 *
 * @file        MG32x02z__IRQHandler.H
 *
 * @brief       This file contains all the functions prototypes for the IRQHandler
 *              firmware library.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/07/07
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 */ 

#ifndef __MG32x02z_IRQHANDLER_H
#define __MG32x02z_IRQHANDLER_H
#define __MG32x02z_IRQHANDLER_H_VER  0.01               /*!< File Version */



#ifdef __cplusplus
 extern "C" {
#endif


extern void HardFault_IRQ(void);
extern void WWDT_IRQ(void);
extern void IWDT_IRQ(void);
extern void PW_IRQ(void);
extern void RTC_IRQ(void);
extern void CSC_IRQ(void);
extern void APB_IRQ(void);
extern void MEM_IRQ(void);
extern void EMB_IRQ(void);
extern void EXINT0_IRQ(void);
extern void EXINT1_IRQ(void);
extern void EXINT2_IRQ(void);
extern void EXINT3_IRQ(void);
extern void CMP_IRQ(void);
extern void DMA_IRQ(void);
extern void ADC_IRQ(void);
extern void DAC_IRQ(void);
extern void TM00_IRQ(void);
extern void TM01_IRQ(void);
extern void TM10_IRQ(void);
extern void TM16_IRQ(void);
extern void TM20_IRQ(void);
extern void TM26_IRQ(void);
extern void TM36_IRQ(void);
extern void URT0_IRQ(void);
extern void URT1_IRQ(void);
extern void URT2_IRQ(void);
extern void URT3_IRQ(void);
extern void SPI0_IRQ(void);
extern void I2C0_IRQ(void);
extern void I2C1_IRQ(void);
extern void RTC_IRQ(void);
extern void WWDT_IRQ(void);


#ifdef __cplusplus
}
#endif

#endif


