

/**
 ******************************************************************************
 *
 * @file        MG32x02z_EXIC_DRV.H
 *
 * @brief       This file contains all the functions prototypes for the EXIC
 *              firmware library.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/30
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *
 ******************************************************************************
 * @par 		Disclaimer 
 *		The Demo software is provided "AS IS"  without any warranty, either 
 *		expressed or implied, including, but not limited to, the implied warranties 
 *		of merchantability and fitness for a particular purpose.  The author will 
 *		not be liable for any special, incidental, consequential or indirect 
 *		damages due to loss of data or any other reason. 
 *		These statements agree with the world wide and local dictated laws about 
 *		authorship and violence against these laws. 
 ******************************************************************************
 ******************************************************************************
 */

#ifndef __MG32x02z_EXIC_DRV_H
#define __MG32x02z_EXIC_DRV_H


#include "MG32x02z__Common_DRV.h"
#include "MG32x02z_EXIC.h" 


/** 
 * @struct	EXIC_PX_Struct
 * @brief	External PX trigger control struct.
 */
typedef struct
{
    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __IO uint8_t  PA0_PF        :1;     /*!< [0] EXIC pin input interrupt pending flag x for external input interrupt pin of PX*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA1_PF        :1;     /*!< [1] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA2_PF        :1;     /*!< [2] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA3_PF        :1;     /*!< [3] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA4_PF        :1;     /*!< [4] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA5_PF        :1;     /*!< [5] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA6_PF        :1;     /*!< [6] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA7_PF        :1;     /*!< [7] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA8_PF        :1;     /*!< [8] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA9_PF        :1;     /*!< [9] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA10_PF       :1;     /*!< [10] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA11_PF       :1;     /*!< [11] Refer to EXIC_PX0_PF. */ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA12_PF       :1;     /*!< [12] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA13_PF       :1;     /*!< [13] Refer to EXIC_PX0_PF.*/ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA14_PF       :1;     /*!< [14] Refer to EXIC_PX0_PF. */ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __IO uint8_t  PA15_PF       :1;     /*!< [15] Refer to EXIC_PX0_PF. */ 
                                                /*!< 0 = Normal : No event occurred */ 
                                                /*!< 1 = Happened : Event happened */ 
            __I  uint16_t               :16;    /*!< [31..16] */ 
        }MBIT;
    }PF;                                        /*!< PX_PF      ~ Offset[0x20]  EXIC PX input interrupt pending flag register */                           

    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __IO uint8_t  PA0_TRGS      :2;     /*!<[1..0] External interrupt pin edge/level trigger event select */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA1_TRGS      :2;     /*!<[3..2] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA2_TRGS      :2;     /*!<[5..4] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA3_TRGS      :2;     /*!<[7..6] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA4_TRGS      :2;     /*!<[9..8] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA5_TRGS      :2;     /*!<[11..10] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA6_TRGS      :2;     /*!<[13..12] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA7_TRGS      :2;     /*!<[15..14] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA8_TRGS      :2;     /*!<[17..16] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA9_TRGS      :2;     /*!<[19..18] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA10_TRGS     :2;     /*!<[21..20] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA11_TRGS     :2;     /*!<[23..22] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA12_TRGS     :2;     /*!<[25..24] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA13_TRGS     :2;     /*!<[27..26] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA14_TRGS     :2;     /*!<[29..28] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
            __IO uint8_t  PA15_TRGS     :2;     /*!<[31..30] Refer to EXIC_PX0_TRGS. */ 
                                                /*!<0x0 = No : No updated flag */ 
                                                /*!<0x1 = Level */ 
                                                /*!<0x2 = Edge */ 
                                                /*!<0x3 = Dual-edge */ 
        }MBIT;
    }TRGS;                                      /*!< PX_TRGS    ~ Offset[0x24]  EXIC PX Pad input trigger select register */        

    union
    {
        __IO  uint32_t  W;
        __IO  uint16_t  H[2];
        __IO  uint8_t   B[4];
        struct
        {
            __IO uint8_t  PA0_OM        :1;     /*!<[0] External interrupt pin of PX OR mask bit x */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA1_OM        :1;     /*!<[1] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA2_OM        :1;     /*!<[2] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA3_OM        :1;     /*!<[3] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA4_OM        :1;     /*!<[4] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA5_OM        :1;     /*!<[5] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA6_OM        :1;     /*!<[6] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA7_OM        :1;     /*!<[7] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA8_OM        :1;     /*!<[8] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA9_OM        :1;     /*!<[9] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA10_OM       :1;     /*!<[10] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA11_OM       :1;     /*!<[11] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA12_OM       :1;     /*!<[12] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA13_OM       :1;     /*!<[13] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA14_OM       :1;     /*!<[14] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA15_OM       :1;     /*!<[15] Refer to EXIC_PX0_OM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA0_AM        :1;     /*!<[16] External interrupt pin of PX AND mask bit x */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable  */
            __IO uint8_t  PA1_AM        :1;     /*!<[17] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA2_AM        :1;     /*!<[18] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA3_AM        :1;     /*!<[19] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA4_AM        :1;     /*!<[20] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA5_AM        :1;     /*!<[21] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA6_AM        :1;     /*!<[22] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA7_AM        :1;     /*!<[23] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA8_AM        :1;     /*!<[24] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA9_AM        :1;     /*!<[25] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA10_AM       :1;     /*!<[26] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA11_AM       :1;     /*!<[27] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA12_AM       :1;     /*!<[28] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA13_AM       :1;     /*!<[29] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA14_AM       :1;     /*!<[30] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable */
            __IO uint8_t  PA15_AM       :1;     /*!<[31] Refer to EXIC_PX0_AM. */
                                                /*!<0 = Disable (Mask) */
                                                /*!<1 = Enable    */
                                        
        }MBIT;                                  
    }MSK;                                       /*!< PX_MSK     ~ Offset[0x28]  EXIC PX AOI Mask register */
}EXIC_PX_Struct;

///@{
#define EXIC_PA_Base    ((uint32_t)(0x50000000+0x20))   /*!< EXIC PA Register Control Definitions base address*/
#define EXIC_PB_Base    ((uint32_t)(0x50000000+0x30))   /*!< EXIC PB Register Control Definitions base address*/
#define EXIC_PC_Base    ((uint32_t)(0x50000000+0x40))   /*!< EXIC PC Register Control Definitions bass address*/
#define EXIC_PD_Base    ((uint32_t)(0x50000000+0x50))   /*!< EXIC PD Register Control Definitions bass address*/


#define EXIC_PA         ((EXIC_PX_Struct*) EXIC_PA_Base)    /*!< EXIC PA Register Control Definitions */
#define EXIC_PB         ((EXIC_PX_Struct*) EXIC_PB_Base)    /*!< EXIC PB Register Control Definitions */
#define EXIC_PC         ((EXIC_PX_Struct*) EXIC_PC_Base)    /*!< EXIC PC Register Control Definitions */
#define EXIC_PD         ((EXIC_PX_Struct*) EXIC_PD_Base)    /*!< EXIC PD Register Control Definitions */



#define EXIC_PA_IT          1                                                           /*!< External trigger interrupt flag Control Definitions */
#define EXIC_PB_IT          2                                                           /*!< External trigger interrupt flag Control Definitions */
#define EXIC_PC_IT          4                                                           /*!< External trigger interrupt flag Control Definitions */
#define EXIC_PD_IT          8                                                           /*!< External trigger interrupt flag Control Definitions */
 
#define EXIC_PX_AF          EXIC_STA_PA_AF_mask_b0              /*!< Byte of PX_AF mask */                
#define EXIC_PX_OF          EXIC_STA_PA_OF_mask_b0              /*!< Byte of PX_OF mask */                  
#define EXIC_PX_ITF_Mask    EXIC_PX_AF | EXIC_PX_OF             /*!< Byte of EXIC PX interrupt flag mask */
#define EXIC_PX_ITF_Shift   4

#define EXIC_TRGS_PIN0      0x00000001      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN1      0x00000004      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN2      0x00000010      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN3      0x00000040      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN4      0x00000100      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN5      0x00000400      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN6      0x00001000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN7      0x00004000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN8      0x00010000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN9      0x00040000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN10     0x00100000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN11     0x00400000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN12     0x01000000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN13     0x04000000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN14     0x10000000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_PIN15     0x40000000      /*!< External trigger mode of pin Control Definitions */
#define EXIC_TRGS_ALL       0x55555555      /*!< External trigger mode of pin Control Definitions */
                              
#define EXIC_PX_PIN0        0x0001          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN1        0x0002          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN2        0x0004          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN3        0x0008          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN4        0x0010          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN5        0x0020          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN6        0x0040          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN7        0x0080          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN8        0x0100          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN9        0x0200          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN10       0x0400          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN11       0x0800          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN12       0x1000          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN13       0x2000          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN14       0x4000          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_PIN15       0x8000          /*!< External trigger Pin Control Definitions */
#define EXIC_PX_AllPIN      0xFFFF          /*!< External trigger Pin Control Definitions */

#define EXIC_ID_ADR_Base    0x50000060
#define EXIC_ID_MAX         32
///@}


/*! @enum   EXIC_TRGSMode_Typedef
    @brief  External pin edge / level trigger event select definitions.
*/
typedef enum
{
	No_UpData_Flag  =   0,                  /*!< No external trgger function */
    Level           =   1,                  /*!< External trigger event is level*/ 
    Edge            =   2,                  /*!< External trigger event is edge*/
    Dual_edge       =   3                   /*!< External trigger event is dual edge*/
}EXIC_TRGSMode_Typedef;                              


/*! @enum   EXIC_ITFlag_Typdef
    @brief  Read / Clear interrupt flag of Port select definitions.
*/
typedef enum
{
    EXIC_PA_ITF    = 0,                     /*!< PortA*/                                               
    EXIC_PB_ITF    = 1,                     /*!< PortB*/
    EXIC_PC_ITF    = 2,                     /*!< PortC*/                    
    EXIC_PD_ITF    = 3,                     /*!< PortD*/
}EXIC_ITFlag_Typdef;

#if !defined(MG32F02A132) && !defined(MG32F02A072) && !defined(MA862)
/** 
 * @enum	EXIC_NMIMUX_Typedef
 * @brief	Control NIM trigger source select.
 */
typedef enum
{
    EXIC_NMI_WWDT       = 0x00000000 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from WWDT */
    EXIC_NMI_SYS        = 0x00000400 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from SYS */
    EXIC_NMI_EXIN0      = 0x00000C00 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from EXINT0 */
    EXIC_NMI_EXIN1      = 0x00001000 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from EXINT1 */
    EXIC_NMI_EXIN2      = 0x00001400 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from EXINT2 */
    EXIC_NMI_EXIN3      = 0x00001800 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from EXINT3 */
    EXIC_NMI_COMP       = 0x00001C00 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from COMP */
    EXIC_NMI_DMA        = 0x00002000 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from DMA */
    EXIC_NMI_ADC        = 0x00002800 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from ADC */
    EXIC_NMI_DAC        = 0x00002C00 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from DAC */
    EXIC_NMI_TM00       = 0x00003000 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from TM00 */
    EXIC_NMI_TM10       = 0x00003400 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from TM10 */
    EXIC_NMI_TM16       = 0x00003800 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from TM16 */
    EXIC_NMI_TM20       = 0x00003C00 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from TM20 */
    EXIC_NMI_TM26       = 0x00004000 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from TM26 */
    EXIC_NMI_TM36       = 0x00004400 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from TM36 */
    EXIC_NMI_URT0       = 0x00005000 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from URT0 */
    EXIC_NMI_URT123     = 0x00005400 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from URT123 */
    EXIC_NMI_SPI0       = 0x00006000 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from SPI0 */
    EXIC_NMI_I2C0       = 0x00007000 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from I2C0 */
    EXIC_NMI_I2Cx       = 0x00007400 | EXIC_CR0_NMI_SEL_mask_w,    /*!< Trgger NMI source from I2Cx */
    EXIC_NMI_NMI        = 0x00000000,
}EXIC_NMIMUX_Typedef;
#endif

/** 
 * @struct	EXIC_TRGSTypeDef
 * @brief	Control external pin event flag.
 */
typedef struct	
{
	uint32_t                  	EXIC_Pin;           /*!< Select external trigger pin.*/
	EXIC_TRGSMode_Typedef       EXIC_TRGS_Mode;     /*!< Select external trigger mode.*/
}EXIC_TRGSTypeDef;





void EXIC_PxTriggerITEA_Cmd(uint32_t EXIC_PX_IT, FunctionalState EXIC_IT_State);
uint16_t EXIC_GetPxAllTriggerEventFlagStatus(EXIC_PX_Struct* EXIC_PX);
uint8_t EXIC_GetPxAllTriggerITFlagStatus( EXIC_ITFlag_Typdef EXIC_PX_ITFlag);
DRV_Return EXIC_GetPinxTriggerEventSingleFlagStatus(EXIC_PX_Struct* EXIC_PX , uint16_t EXIC_Pin_PF);
DRV_Return EXIC_GetPxTriggerAndITFlagStatus( EXIC_ITFlag_Typdef EXIC_PX_ITFlag);
DRV_Return EXIC_GetPxTriggerOrITFlagStatus( EXIC_ITFlag_Typdef EXIC_PX_ITFlag);
void EXIC_ClearPxTriggerEventFlag(EXIC_PX_Struct* EXIC_PX , uint16_t EXIC_Pin_PF);
void EXIC_ClearPxTriggerITFlag( EXIC_ITFlag_Typdef EXIC_PX , uint8_t EXIC_PX_ITFlag);
DRV_Return EXIC_PxTriggerMode_Select(EXIC_PX_Struct* EXIC_PX ,EXIC_TRGSTypeDef* EXIC_PX_TRGS );
void EXIC_PxTriggerAndMask_Select(EXIC_PX_Struct* EXIC_PX , uint16_t  EXIC_MSK_PIN);
void EXIC_PxTriggerOrMask_Select(EXIC_PX_Struct* EXIC_PX , uint16_t  EXIC_MSK_PIN);
void EXIC_PxTriggerAndUnmatch_Cmd(uint32_t EXIC_PX_IT, FunctionalState PX_unMatch_Cmd);
uint8_t EXIC_GetITSourceID(IRQn_Type ITSourceNum);
void EXIC_RXEV_Cmd(FunctionalState RXEV_State);
void EXIC_NMI_Cmd(FunctionalState NMI_State);
void EXIC_NMITrigger_SW(void);
#if !defined(MG32F02A132) && !defined(MG32F02A072) && !defined(MA862)
    void EXIC_NMI_Select(EXIC_NMIMUX_Typedef);
#endif


#endif








