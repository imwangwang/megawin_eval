/**
 ******************************************************************************
 *
 * @file        MG32x02z_RTC_IRQ.c
 *
 * @brief       The demo code RTC Interrupt Request C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */
 
#include "MG32x02z_RTC_DRV.h"


/**
 *******************************************************************************
 * @brief  	    RTC module reset event service
 * @details  
 * @return	    
 * @note
 *******************************************************************************
 */
void RTC_IRQ (void)
{
    
    
    if(RTC_GetSingleFlagStatus(RTC_RCRF) == DRV_Happened)
    {
        // When RTC_RCR register reload finished, software capture finished or RTC_ALM register value update allowed.
        // To do...
        RTC_ClearFlag(RTC_RCRF);
    }
    
    if(RTC_GetSingleFlagStatus(RTC_TOF) == DRV_Happened)
    {
        // When RTC timer overflow.
        // To do...
        RTC_ClearFlag(RTC_TOF);
    }
    
    if(RTC_GetSingleFlagStatus(RTC_TSF) == DRV_Happened)
    {
        // When RTC time stamp.
        // To do...
        RTC_ClearFlag(RTC_TSF);
    }
    
    if(RTC_GetSingleFlagStatus(RTC_PCF) == DRV_Happened)
    {
        // When RTC periodic interrupt.
        // To do...
        RTC_ClearFlag(RTC_PCF);
    }
    
    if(RTC_GetSingleFlagStatus(RTC_ALMF) == DRV_Happened)
    {
        // When RTC alarm matched.
        // To do...
        RTC_ClearFlag(RTC_ALMF);
    }
    

}


