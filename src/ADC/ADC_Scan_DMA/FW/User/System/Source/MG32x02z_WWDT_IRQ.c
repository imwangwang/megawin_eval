/**
 ******************************************************************************
 *
 * @file        MG32x02z_WWDT_IRQ.c
 *
 * @brief       The demo code WWDT Interrupt Request C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.10
 * @date        2018/01/31
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2018 Megawin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par         Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */ 

#include "MG32x02z_WWDT_DRV.h"


/**
 *******************************************************************************
 * @brief  	    WWDT module reset event service
 * @details  
 * @return	    
 * @note
 *******************************************************************************
 */
void WWDT_IRQ (void)
{

    
    if(WWDT_GetSingleFlagStatus(WWDT_WINF) == DRV_Happened)
    {
        // When WWDT counter refreshing and value over the window compare threshold condition.
        // To do...
        
        WWDT_ClearFlag(WWDT_WINF);
    }
    
    if(WWDT_GetSingleFlagStatus(WWDT_WRNF) == DRV_Happened)
    {
        // When WWDT counter warning.
        // To do...
        WWDT_ClearFlag(WWDT_WRNF);
    }
        
    if(WWDT_GetSingleFlagStatus(WWDT_TF) == DRV_Happened)
    {
        // When WWDT timer timeout interrupt.
        // To do...
        WWDT_ClearFlag(WWDT_TF);
    }
}



