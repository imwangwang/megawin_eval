

/**
 ******************************************************************************
 *
 * @file        MG32x02z_I2C_IRQ.c
 * @brief       The I2C Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2016/10/25
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */

#include "MG32x02z_I2C_IRQ.h"



/**
 *******************************************************************************
 * @brief	    I2C WakeUp Handler
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
                I2C_WakeUp_Handler(I2C0);
                I2C_WakeUp_Handler(I2C1);
 * @endcode     
 * @par         Modify
 *              __weak void I2C_WakeUp_Handler(I2C_Struct* I2Cx)
 *******************************************************************************
 */
__weak void I2C_WakeUp_Handler(I2C_Struct* I2Cx)
{

}



/**
 *******************************************************************************
 * @brief	    I2C Timeout Handler
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
                I2C_Timeout_Handler(I2C0)
                I2C_Timeout_Handler(I2C1)
 * @endcode
 * @par         Modify
 *              __weak void I2C_Timeout_Handler(I2C_Struct* I2Cx)
 *******************************************************************************
 */
__weak void I2C_Timeout_Handler(I2C_Struct* I2Cx)
{
    if(I2C_GetFlagStatus(I2Cx, I2C_FLAG_TMOUTF))
    {
        // I2C0 time-out detect.
        // to do..
        I2C_ClearFlag(I2Cx, I2C_FLAG_TMOUTF);
    }
}



/**
 *******************************************************************************
 * @brief	    I2C ByteMode Event Handler
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
                I2C_ByteMode_Event_Handler(I2C0);
                I2C_ByteMode_Event_Handler(I2C1);
 * @endcode
 * @par         Modify
 *              __weak void I2C_ByteMode_Event_Handler(I2C_Struct* I2Cx)
 *******************************************************************************
 */
__weak void I2C_ByteMode_Event_Handler(I2C_Struct* I2Cx)
{
    if(I2C_GetFlagStatus(I2Cx, I2C_FLAG_EVENTF))
    {
        // I2C status event interrupt.
        // to do..
        switch(I2C_GetEventCode(I2Cx)){
            case 0x00:  // Bus error during MASTER or selected slave modes, due to an illegal START or STOP condition. State 0x00 can also occur when interference causes the I2C block to enter an undefined state.
                    // to do..
                break;

            case 0x08:  // A START condition has been transmitted.
                    // to do..
                break;

            case 0x10:  // A repeated START condition has been transmitted.
                    // to do..
                break;

            case 0x18:  // SLA+W has been transmitted; ACK has been received.
                    // to do..
                break;

            case 0x20:  // SLA+W has been transmitted; NOT ACK has been received.
                    // to do..
                break;

            case 0x28:  // Data byte in DAT has been transmitted; ACK has been received.
                    // to do..
                break;

            case 0x30:  // Data byte in DAT has been transmitted; NOT ACK has been received.
                    // to do..
                break;

            case 0x38:  // Arbitration lost in SLA+R/W or Data bytes.
                    // to do..
                break;

            case 0x40:  // SLA+R has been transmitted; ACK has been received.
                    // to do..
                break;

            case 0x48:  // SLA+R has been transmitted; NOT ACK has been received.
                    // to do..
                break;

            case 0x50:  // Data byte has been received; ACK has been returned.
                    // to do..
                break;

            case 0x58:  // Data byte has been received; NOT ACK has been returned.
                    // to do..
                break;

            case 0x60:  // Own SLA+W has been received; ACK has been returned.
                    // to do..
                break;

            case 0x68:  // Arbitration lost in SLA+R/W as master; Own SLA+W has been received, ACK returned.
                    // to do..
                break;

            case 0x70:  // General Call address (0x00) has been received; ACK has been returned.
                    // to do..
                break;

            case 0x78:  // Arbitration lost in SLA+R/W as master; General Call address has been received, ACK has been returned.
                    // to do..
                break;

            case 0x80:  // Previously addressed with own SLA address; DATA has been received; ACK has been returned.
                    // to do..
                break;

            case 0x88:  // Previously addressed with own SLA; DATA byte has been received; NOT ACK has been returned.
                    // to do..
                break;

            case 0x90:  // Previously addressed with General Call; DATA byte has been received; ACK has been returned.
                    // to do..
                break;

            case 0x98:  // Previously addressed with General Call; DATA byte has been received; NOT ACK has been returned.
                    // to do..
                break;

            case 0xA0:  // A STOP condition or condition has been received while still addressed as Slave Receiver or Slave Transmitter.
                    // to do..
                break;

            case 0xA8:  // Own SLA+R has been received; ACK has been returned.
                    // to do..
                break;

            case 0xB0:  // Arbitration lost in SLA+R/W as master; Own SLA+R has been received, ACK has been returned.
                    // to do..
                break;

            case 0xB8:  // Data byte in DAT has been transmitted; ACK has been received.
                    // to do..
                break;

            case 0xC0:  // Data byte in DAT has been transmitted; NOT ACK has been received.
                    // to do..
                break;

            case 0xC8:  // Last data byte in DAT has been transmitted (AA = 0); ACK has been received.
                    // to do..
                break;

            case 0xF8:  // No relevant state information available; Bus is released ; EVENTF = 0 and no interrupt asserted.(Default)
                    // to do..
                break;
        }
        // to do..
        I2C_ClearFlag(I2Cx, I2C_FLAG_EVENTF);
    }
}



/**
 *******************************************************************************
 * @brief	    I2C0_IRQ
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
                I2C0_IRQ();
 * @endcode     
 * @par         Modify
 *              __weak void I2C0_IRQ(void)
 *******************************************************************************
 */
__weak void I2C0_IRQ(void)
{
    I2C_WakeUp_Handler(I2C0);

    I2C_Timeout_Handler(I2C0);

    I2C_ByteMode_Event_Handler(I2C0);

}


/**
 *******************************************************************************
 * @brief	    I2C1_IRQ
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
                I2C1_IRQ();
 * @endcode     
 * @par         Modify
 *              __weak void I2C0_IRQ(void)
 *******************************************************************************
 */
__weak void I2C1_IRQ(void)
{
    I2C_WakeUp_Handler(I2C1);

    I2C_Timeout_Handler(I2C1);

    I2C_ByteMode_Event_Handler(I2C1);
}





