

/**
 ******************************************************************************
 *
 * @file        MG32x02z_TM01_IRQ.c
 * @brief       The TM01 Interrupt Request Handler C file.
 *
 * @par         Project
 *              MG32x02z
 * @version     V1.00
 * @date        2017/03/28
 * @author      Megawin Software Center
 * @copyright   Copyright (c) 2017 MegaWin Technology Co., Ltd.
 *              All rights reserved.
 *  
 ******************************************************************************* 
 * @par Disclaimer
 * The Demo software is provided "AS IS" without any warranty, either
 * expressed or implied, including, but not limited to, the implied warranties
 * of merchantability and fitness for a particular purpose. The author will
 * not be liable for any special, incidental, consequential or indirect
 * damages due to loss of data or any other reason.
 * These statements agree with the world wide and local dictated laws about
 * authorship and violence against these laws.
 *******************************************************************************
 */ 
 
// <<< Use Configuration Wizard in Context Menu >>>


// <<< end of configuration section >>>    



#include "MG32x02z_TM_DRV.h"






/**
 *******************************************************************************
 * @brief	    TM01 interrupt function. 
 * @details     
 * @return      
 * @exception   No
 * @note
 * @par         Example
 * @code
 * @endcode
 *******************************************************************************
 */
__weak void TM01_IRQ(void)
{

    
    // 2nd / Main counter
    if (TM_GetSingleFlagStatus(TM01, TMx_TUF2) == DRV_Happened)
    {
        // 2nd Timer underflow flag
        // To do...
        
        TM_ClearFlag (TM01, TMx_TUF2);
    }
    if (TM_GetSingleFlagStatus(TM01, TMx_TOF2) == DRV_Happened)
    {
        // 2nd Timer overflow flag
        // To do...
        
        TM_ClearFlag (TM01, TMx_TOF2);
    }
    if (TM_GetSingleFlagStatus(TM01, TMx_TOF) == DRV_Happened)
    {
        // Main Timer overflow flag
        // To do...
        
        TM_ClearFlag (TM01, TMx_TOF);
    }

    // external trigger
    if (TM_GetSingleFlagStatus(TM01, TMx_EXF) == DRV_Happened)
    {
        // Timer external trigger flag
        // To do...
        
        TM_ClearFlag (TM01, TMx_EXF);
    }
    

}



